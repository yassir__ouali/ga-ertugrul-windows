﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Svelto.Tasks.ParallelTaskCollection
struct ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA;
// Svelto.Tasks.SerialTaskCollection
struct SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A;
// System.Collections.Generic.Dictionary`2<System.String,Tayr.TPrefabsConf>
struct Dictionary_2_t83BA0CF1FD9248B190489DADB47FBF726F846EA3;
// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299;
// System.Collections.Generic.Dictionary`2<Tayr.AnimationType,System.Type>
struct Dictionary_2_t055F14A9CCA952EF0DCF118E732AAD5F426E0206;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,Tayr.ObjectPool`1<UnityEngine.GameObject>>
struct Dictionary_2_t14628031B0235DCDE817C9CF9685AE79944DE191;
// System.Collections.Generic.Dictionary`2<UnityEngine.SystemLanguage,System.String[]>
struct Dictionary_2_tD72C2CDDAA3A34F41790E7B0309A2CB21D2C2654;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<Tayr.NodeCacheData>
struct List_1_t9394DDF96FA4463DFBB26378113201C873489995;
// System.Collections.Generic.List`1<Tayr.PatchAssetData>
struct List_1_t77284B88D10BB4C1F3F82DD37A8D645EC21AA396;
// System.Collections.Generic.List`1<Tayr.TListItem>
struct List_1_t2F608C9C1F2DE3FCCC09DBBB84D6C0B85A093212;
// System.IO.FileStream
struct FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// Tayr.AssetBundleLoaderTask
struct AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD;
// Tayr.DepotFileLoaderTask
struct DepotFileLoaderTask_t8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32;
// Tayr.DownloadFileTask
struct DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F;
// Tayr.GameObjectPool
struct GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221;
// Tayr.ILibrary
struct ILibrary_tCBD3DE1F66AD92DAB1112B8018EBA4A4BD23767C;
// Tayr.INode
struct INode_tD44544594CD9341F4835D560B81E4A956C1D1DC7;
// Tayr.INodeAnimationHandler
struct INodeAnimationHandler_t7A50F84EDDDD8CA150788E8B2A8D0F06E5ABF200;
// Tayr.ITask
struct ITask_tAC251208C63C7CDF3F8E273685FE0DBBD809E3D5;
// Tayr.ITaskContainer
struct ITaskContainer_t2FD3049836F2CE1D1D36D26BE4E990C439E97B3E;
// Tayr.ITaskRunner
struct ITaskRunner_tB1CAE8EDCFC7F57D1F9FCE9C61BE7F3B8BB1389B;
// Tayr.IntroFinishEvent
struct IntroFinishEvent_t41434E95E4BB9D37CE53CCD6322D8B8D1DCD87EC;
// Tayr.JsonConfigurator
struct JsonConfigurator_t4300FF6941F6A5F99CB53A04D924CFEAC2DD5B91;
// Tayr.LoadCSVLocalizationTask
struct LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78;
// Tayr.LoadHashFileTask
struct LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552;
// Tayr.LocalFileLoaderTask
struct LocalFileLoaderTask_t4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD;
// Tayr.LocalizationLoadedEvent
struct LocalizationLoadedEvent_t006A316DE326820292279E7ED43DEE916CCCE570;
// Tayr.MappingInitializer
struct MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A;
// Tayr.MissingAssetsGroup
struct MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1;
// Tayr.MultiLookupLoaderTask
struct MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A;
// Tayr.NodeAnimator
struct NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F;
// Tayr.NodeSystem
struct NodeSystem_t28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE;
// Tayr.OnProgress
struct OnProgress_tADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4;
// Tayr.OnTaskCompleted`1<System.Byte[]>
struct OnTaskCompleted_1_t1BAF2992632965DF893A9E1E7A3F31439A3CD18F;
// Tayr.OnTaskCompleted`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct OnTaskCompleted_1_tBBB582D5B9BFD572A3E5CA228F8B03A453171F07;
// Tayr.OnTaskCompleted`1<System.Int32>
struct OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE;
// Tayr.OnTaskCompleted`1<System.String>
struct OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74;
// Tayr.OnTaskCompleted`1<Tayr.ILibrary>
struct OnTaskCompleted_1_t5259FE6B7912622DF08844AF6D84DA0F3F5EE3FB;
// Tayr.OnTaskCompleted`1<Tayr.MissingAssetsGroup>
struct OnTaskCompleted_1_tB39638D949906BF08283F9CE0C05C80116F19ED4;
// Tayr.OutroFinishEvent
struct OutroFinishEvent_t6379D6DE5405EBB1F4C2160A23FD9AF6EC019F38;
// Tayr.ParallelTask
struct ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717;
// Tayr.PatchingTask
struct PatchingTask_tAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD;
// Tayr.ResourceLibrary
struct ResourceLibrary_t9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54;
// Tayr.RetryNode
struct RetryNode_tA2D5659E521764F41C793319C5DD5D9E8F3EE04C;
// Tayr.SceneLibrary
struct SceneLibrary_tD4A336537A5F417E9F29493F769735C1EDFBCFDF;
// Tayr.SerialTask
struct SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A;
// Tayr.ServerFileLoaderTask
struct ServerFileLoaderTask_t95314098EBA8E90891F1D8C5F19C0DC49D532F92;
// Tayr.StreamingAssetFileLoaderTask
struct StreamingAssetFileLoaderTask_t766339DE3AEAB244DF39F9C963074DCCAEA2BA43;
// Tayr.TLocalizationSystem
struct TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B;
// Tayr.TSoundSystem
struct TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511;
// Tayr.TSystemsManager
struct TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462;
// Tayr.TUpdaterSystem
struct TUpdaterSystem_t69A618B692764BD9B69DD0829A3D8DDFEC7AFA2E;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.AssetBundle
struct AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;
// Zenject.DisposableManager
struct DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ARABICFIXER_T2221BB251023D6FC85CF202B56FB036FF7627ED3_H
#define ARABICFIXER_T2221BB251023D6FC85CF202B56FB036FF7627ED3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ArabicSupport.ArabicFixer
struct  ArabicFixer_t2221BB251023D6FC85CF202B56FB036FF7627ED3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARABICFIXER_T2221BB251023D6FC85CF202B56FB036FF7627ED3_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ADSCONSTANTS_T069750BD8A0D24B8EC924103D0A3A031F3EDD71F_H
#define ADSCONSTANTS_T069750BD8A0D24B8EC924103D0A3A031F3EDD71F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.AdsConstants
struct  AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F  : public RuntimeObject
{
public:

public:
};

struct AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields
{
public:
	// System.String Tayr.AdsConstants::ANDROID_INTERSTITIAL_TEST_ID
	String_t* ___ANDROID_INTERSTITIAL_TEST_ID_0;
	// System.String Tayr.AdsConstants::ANDROID_BANNER_TEST_ID
	String_t* ___ANDROID_BANNER_TEST_ID_1;
	// System.String Tayr.AdsConstants::ANDROID_TEST_APP_ID
	String_t* ___ANDROID_TEST_APP_ID_2;
	// System.String Tayr.AdsConstants::IOS_INTERSTITIAL_TEST_ID
	String_t* ___IOS_INTERSTITIAL_TEST_ID_3;
	// System.String Tayr.AdsConstants::IOS_BANNER_TEST_ID
	String_t* ___IOS_BANNER_TEST_ID_4;
	// System.String Tayr.AdsConstants::IOS_TEST_APP_ID
	String_t* ___IOS_TEST_APP_ID_5;

public:
	inline static int32_t get_offset_of_ANDROID_INTERSTITIAL_TEST_ID_0() { return static_cast<int32_t>(offsetof(AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields, ___ANDROID_INTERSTITIAL_TEST_ID_0)); }
	inline String_t* get_ANDROID_INTERSTITIAL_TEST_ID_0() const { return ___ANDROID_INTERSTITIAL_TEST_ID_0; }
	inline String_t** get_address_of_ANDROID_INTERSTITIAL_TEST_ID_0() { return &___ANDROID_INTERSTITIAL_TEST_ID_0; }
	inline void set_ANDROID_INTERSTITIAL_TEST_ID_0(String_t* value)
	{
		___ANDROID_INTERSTITIAL_TEST_ID_0 = value;
		Il2CppCodeGenWriteBarrier((&___ANDROID_INTERSTITIAL_TEST_ID_0), value);
	}

	inline static int32_t get_offset_of_ANDROID_BANNER_TEST_ID_1() { return static_cast<int32_t>(offsetof(AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields, ___ANDROID_BANNER_TEST_ID_1)); }
	inline String_t* get_ANDROID_BANNER_TEST_ID_1() const { return ___ANDROID_BANNER_TEST_ID_1; }
	inline String_t** get_address_of_ANDROID_BANNER_TEST_ID_1() { return &___ANDROID_BANNER_TEST_ID_1; }
	inline void set_ANDROID_BANNER_TEST_ID_1(String_t* value)
	{
		___ANDROID_BANNER_TEST_ID_1 = value;
		Il2CppCodeGenWriteBarrier((&___ANDROID_BANNER_TEST_ID_1), value);
	}

	inline static int32_t get_offset_of_ANDROID_TEST_APP_ID_2() { return static_cast<int32_t>(offsetof(AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields, ___ANDROID_TEST_APP_ID_2)); }
	inline String_t* get_ANDROID_TEST_APP_ID_2() const { return ___ANDROID_TEST_APP_ID_2; }
	inline String_t** get_address_of_ANDROID_TEST_APP_ID_2() { return &___ANDROID_TEST_APP_ID_2; }
	inline void set_ANDROID_TEST_APP_ID_2(String_t* value)
	{
		___ANDROID_TEST_APP_ID_2 = value;
		Il2CppCodeGenWriteBarrier((&___ANDROID_TEST_APP_ID_2), value);
	}

	inline static int32_t get_offset_of_IOS_INTERSTITIAL_TEST_ID_3() { return static_cast<int32_t>(offsetof(AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields, ___IOS_INTERSTITIAL_TEST_ID_3)); }
	inline String_t* get_IOS_INTERSTITIAL_TEST_ID_3() const { return ___IOS_INTERSTITIAL_TEST_ID_3; }
	inline String_t** get_address_of_IOS_INTERSTITIAL_TEST_ID_3() { return &___IOS_INTERSTITIAL_TEST_ID_3; }
	inline void set_IOS_INTERSTITIAL_TEST_ID_3(String_t* value)
	{
		___IOS_INTERSTITIAL_TEST_ID_3 = value;
		Il2CppCodeGenWriteBarrier((&___IOS_INTERSTITIAL_TEST_ID_3), value);
	}

	inline static int32_t get_offset_of_IOS_BANNER_TEST_ID_4() { return static_cast<int32_t>(offsetof(AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields, ___IOS_BANNER_TEST_ID_4)); }
	inline String_t* get_IOS_BANNER_TEST_ID_4() const { return ___IOS_BANNER_TEST_ID_4; }
	inline String_t** get_address_of_IOS_BANNER_TEST_ID_4() { return &___IOS_BANNER_TEST_ID_4; }
	inline void set_IOS_BANNER_TEST_ID_4(String_t* value)
	{
		___IOS_BANNER_TEST_ID_4 = value;
		Il2CppCodeGenWriteBarrier((&___IOS_BANNER_TEST_ID_4), value);
	}

	inline static int32_t get_offset_of_IOS_TEST_APP_ID_5() { return static_cast<int32_t>(offsetof(AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields, ___IOS_TEST_APP_ID_5)); }
	inline String_t* get_IOS_TEST_APP_ID_5() const { return ___IOS_TEST_APP_ID_5; }
	inline String_t** get_address_of_IOS_TEST_APP_ID_5() { return &___IOS_TEST_APP_ID_5; }
	inline void set_IOS_TEST_APP_ID_5(String_t* value)
	{
		___IOS_TEST_APP_ID_5 = value;
		Il2CppCodeGenWriteBarrier((&___IOS_TEST_APP_ID_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADSCONSTANTS_T069750BD8A0D24B8EC924103D0A3A031F3EDD71F_H
#ifndef ADSFACTORY_T491A846A1985B6E666BD20787FFFD63BBFE6EE2F_H
#define ADSFACTORY_T491A846A1985B6E666BD20787FFFD63BBFE6EE2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.AdsFactory
struct  AdsFactory_t491A846A1985B6E666BD20787FFFD63BBFE6EE2F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADSFACTORY_T491A846A1985B6E666BD20787FFFD63BBFE6EE2F_H
#ifndef ANIMATOREXTENSION_TB48EDA5E01669A4C7BD7C1FBBF3C4AE32D2B62B1_H
#define ANIMATOREXTENSION_TB48EDA5E01669A4C7BD7C1FBBF3C4AE32D2B62B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.AnimatorExtension
struct  AnimatorExtension_tB48EDA5E01669A4C7BD7C1FBBF3C4AE32D2B62B1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOREXTENSION_TB48EDA5E01669A4C7BD7C1FBBF3C4AE32D2B62B1_H
#ifndef ANIMATORHANDLER_TB8713610DD7F2D5A30CBBE4EC356E4FE1C1CD66E_H
#define ANIMATORHANDLER_TB8713610DD7F2D5A30CBBE4EC356E4FE1C1CD66E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.AnimatorHandler
struct  AnimatorHandler_tB8713610DD7F2D5A30CBBE4EC356E4FE1C1CD66E  : public RuntimeObject
{
public:
	// UnityEngine.Animator Tayr.AnimatorHandler::_animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ____animator_0;

public:
	inline static int32_t get_offset_of__animator_0() { return static_cast<int32_t>(offsetof(AnimatorHandler_tB8713610DD7F2D5A30CBBE4EC356E4FE1C1CD66E, ____animator_0)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get__animator_0() const { return ____animator_0; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of__animator_0() { return &____animator_0; }
	inline void set__animator_0(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		____animator_0 = value;
		Il2CppCodeGenWriteBarrier((&____animator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORHANDLER_TB8713610DD7F2D5A30CBBE4EC356E4FE1C1CD66E_H
#ifndef U3CTASKU3ED__4_T8D82C67FA91BC73B95A260D81FCD18A6BD22C4CC_H
#define U3CTASKU3ED__4_T8D82C67FA91BC73B95A260D81FCD18A6BD22C4CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.AssetBundleLoaderTask_<Task>d__4
struct  U3CTaskU3Ed__4_t8D82C67FA91BC73B95A260D81FCD18A6BD22C4CC  : public RuntimeObject
{
public:
	// System.Int32 Tayr.AssetBundleLoaderTask_<Task>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.AssetBundleLoaderTask_<Task>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.AssetBundleLoaderTask Tayr.AssetBundleLoaderTask_<Task>d__4::<>4__this
	AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * ___U3CU3E4__this_2;
	// Tayr.MultiLookupLoaderTask Tayr.AssetBundleLoaderTask_<Task>d__4::<loader>5__2
	MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * ___U3CloaderU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__4_t8D82C67FA91BC73B95A260D81FCD18A6BD22C4CC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__4_t8D82C67FA91BC73B95A260D81FCD18A6BD22C4CC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__4_t8D82C67FA91BC73B95A260D81FCD18A6BD22C4CC, ___U3CU3E4__this_2)); }
	inline AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CloaderU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__4_t8D82C67FA91BC73B95A260D81FCD18A6BD22C4CC, ___U3CloaderU3E5__2_3)); }
	inline MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * get_U3CloaderU3E5__2_3() const { return ___U3CloaderU3E5__2_3; }
	inline MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A ** get_address_of_U3CloaderU3E5__2_3() { return &___U3CloaderU3E5__2_3; }
	inline void set_U3CloaderU3E5__2_3(MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * value)
	{
		___U3CloaderU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloaderU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__4_T8D82C67FA91BC73B95A260D81FCD18A6BD22C4CC_H
#ifndef ASSETBUNDLESLIBRARY_T9B34940E0C2AF8549D39827846C36516EC514DDD_H
#define ASSETBUNDLESLIBRARY_T9B34940E0C2AF8549D39827846C36516EC514DDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.AssetBundlesLibrary
struct  AssetBundlesLibrary_t9B34940E0C2AF8549D39827846C36516EC514DDD  : public RuntimeObject
{
public:
	// UnityEngine.AssetBundle Tayr.AssetBundlesLibrary::_bundle
	AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * ____bundle_0;
	// Tayr.GameObjectPool Tayr.AssetBundlesLibrary::_poolManager
	GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * ____poolManager_1;

public:
	inline static int32_t get_offset_of__bundle_0() { return static_cast<int32_t>(offsetof(AssetBundlesLibrary_t9B34940E0C2AF8549D39827846C36516EC514DDD, ____bundle_0)); }
	inline AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * get__bundle_0() const { return ____bundle_0; }
	inline AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 ** get_address_of__bundle_0() { return &____bundle_0; }
	inline void set__bundle_0(AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * value)
	{
		____bundle_0 = value;
		Il2CppCodeGenWriteBarrier((&____bundle_0), value);
	}

	inline static int32_t get_offset_of__poolManager_1() { return static_cast<int32_t>(offsetof(AssetBundlesLibrary_t9B34940E0C2AF8549D39827846C36516EC514DDD, ____poolManager_1)); }
	inline GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * get__poolManager_1() const { return ____poolManager_1; }
	inline GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 ** get_address_of__poolManager_1() { return &____poolManager_1; }
	inline void set__poolManager_1(GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * value)
	{
		____poolManager_1 = value;
		Il2CppCodeGenWriteBarrier((&____poolManager_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLESLIBRARY_T9B34940E0C2AF8549D39827846C36516EC514DDD_H
#ifndef BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#define BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTSystem
struct  BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8  : public RuntimeObject
{
public:
	// Zenject.DisposableManager Tayr.BasicTSystem::_disposableManager
	DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * ____disposableManager_0;
	// Tayr.TSystemsManager Tayr.BasicTSystem::_systemsManager
	TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * ____systemsManager_1;
	// System.Boolean Tayr.BasicTSystem::<IsInitialized>k__BackingField
	bool ___U3CIsInitializedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__disposableManager_0() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ____disposableManager_0)); }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * get__disposableManager_0() const { return ____disposableManager_0; }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC ** get_address_of__disposableManager_0() { return &____disposableManager_0; }
	inline void set__disposableManager_0(DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * value)
	{
		____disposableManager_0 = value;
		Il2CppCodeGenWriteBarrier((&____disposableManager_0), value);
	}

	inline static int32_t get_offset_of__systemsManager_1() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ____systemsManager_1)); }
	inline TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * get__systemsManager_1() const { return ____systemsManager_1; }
	inline TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 ** get_address_of__systemsManager_1() { return &____systemsManager_1; }
	inline void set__systemsManager_1(TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * value)
	{
		____systemsManager_1 = value;
		Il2CppCodeGenWriteBarrier((&____systemsManager_1), value);
	}

	inline static int32_t get_offset_of_U3CIsInitializedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ___U3CIsInitializedU3Ek__BackingField_2)); }
	inline bool get_U3CIsInitializedU3Ek__BackingField_2() const { return ___U3CIsInitializedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsInitializedU3Ek__BackingField_2() { return &___U3CIsInitializedU3Ek__BackingField_2; }
	inline void set_U3CIsInitializedU3Ek__BackingField_2(bool value)
	{
		___U3CIsInitializedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#ifndef BASICTASK_1_T2103EEFF091DD4A943C23C8C77591716114AFEF3_H
#define BASICTASK_1_T2103EEFF091DD4A943C23C8C77591716114AFEF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTask`1<System.Byte[]>
struct  BasicTask_1_t2103EEFF091DD4A943C23C8C77591716114AFEF3  : public RuntimeObject
{
public:
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEvent
	OnTaskCompleted_1_t1BAF2992632965DF893A9E1E7A3F31439A3CD18F * ____onTaskCompletedEvent_0;
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEventInternal
	OnTaskCompleted_1_t1BAF2992632965DF893A9E1E7A3F31439A3CD18F * ____onTaskCompletedEventInternal_1;
	// T Tayr.BasicTask`1::Result
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___Result_2;
	// System.Boolean Tayr.BasicTask`1::_isDone
	bool ____isDone_3;
	// Tayr.ITaskRunner Tayr.BasicTask`1::_taskRunner
	RuntimeObject* ____taskRunner_4;
	// Tayr.ITaskContainer Tayr.BasicTask`1::_taskContainer
	RuntimeObject* ____taskContainer_5;
	// System.Int32 Tayr.BasicTask`1::TaskContainerIndex
	int32_t ___TaskContainerIndex_6;

public:
	inline static int32_t get_offset_of__onTaskCompletedEvent_0() { return static_cast<int32_t>(offsetof(BasicTask_1_t2103EEFF091DD4A943C23C8C77591716114AFEF3, ____onTaskCompletedEvent_0)); }
	inline OnTaskCompleted_1_t1BAF2992632965DF893A9E1E7A3F31439A3CD18F * get__onTaskCompletedEvent_0() const { return ____onTaskCompletedEvent_0; }
	inline OnTaskCompleted_1_t1BAF2992632965DF893A9E1E7A3F31439A3CD18F ** get_address_of__onTaskCompletedEvent_0() { return &____onTaskCompletedEvent_0; }
	inline void set__onTaskCompletedEvent_0(OnTaskCompleted_1_t1BAF2992632965DF893A9E1E7A3F31439A3CD18F * value)
	{
		____onTaskCompletedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEvent_0), value);
	}

	inline static int32_t get_offset_of__onTaskCompletedEventInternal_1() { return static_cast<int32_t>(offsetof(BasicTask_1_t2103EEFF091DD4A943C23C8C77591716114AFEF3, ____onTaskCompletedEventInternal_1)); }
	inline OnTaskCompleted_1_t1BAF2992632965DF893A9E1E7A3F31439A3CD18F * get__onTaskCompletedEventInternal_1() const { return ____onTaskCompletedEventInternal_1; }
	inline OnTaskCompleted_1_t1BAF2992632965DF893A9E1E7A3F31439A3CD18F ** get_address_of__onTaskCompletedEventInternal_1() { return &____onTaskCompletedEventInternal_1; }
	inline void set__onTaskCompletedEventInternal_1(OnTaskCompleted_1_t1BAF2992632965DF893A9E1E7A3F31439A3CD18F * value)
	{
		____onTaskCompletedEventInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEventInternal_1), value);
	}

	inline static int32_t get_offset_of_Result_2() { return static_cast<int32_t>(offsetof(BasicTask_1_t2103EEFF091DD4A943C23C8C77591716114AFEF3, ___Result_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_Result_2() const { return ___Result_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_Result_2() { return &___Result_2; }
	inline void set_Result_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___Result_2 = value;
		Il2CppCodeGenWriteBarrier((&___Result_2), value);
	}

	inline static int32_t get_offset_of__isDone_3() { return static_cast<int32_t>(offsetof(BasicTask_1_t2103EEFF091DD4A943C23C8C77591716114AFEF3, ____isDone_3)); }
	inline bool get__isDone_3() const { return ____isDone_3; }
	inline bool* get_address_of__isDone_3() { return &____isDone_3; }
	inline void set__isDone_3(bool value)
	{
		____isDone_3 = value;
	}

	inline static int32_t get_offset_of__taskRunner_4() { return static_cast<int32_t>(offsetof(BasicTask_1_t2103EEFF091DD4A943C23C8C77591716114AFEF3, ____taskRunner_4)); }
	inline RuntimeObject* get__taskRunner_4() const { return ____taskRunner_4; }
	inline RuntimeObject** get_address_of__taskRunner_4() { return &____taskRunner_4; }
	inline void set__taskRunner_4(RuntimeObject* value)
	{
		____taskRunner_4 = value;
		Il2CppCodeGenWriteBarrier((&____taskRunner_4), value);
	}

	inline static int32_t get_offset_of__taskContainer_5() { return static_cast<int32_t>(offsetof(BasicTask_1_t2103EEFF091DD4A943C23C8C77591716114AFEF3, ____taskContainer_5)); }
	inline RuntimeObject* get__taskContainer_5() const { return ____taskContainer_5; }
	inline RuntimeObject** get_address_of__taskContainer_5() { return &____taskContainer_5; }
	inline void set__taskContainer_5(RuntimeObject* value)
	{
		____taskContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____taskContainer_5), value);
	}

	inline static int32_t get_offset_of_TaskContainerIndex_6() { return static_cast<int32_t>(offsetof(BasicTask_1_t2103EEFF091DD4A943C23C8C77591716114AFEF3, ___TaskContainerIndex_6)); }
	inline int32_t get_TaskContainerIndex_6() const { return ___TaskContainerIndex_6; }
	inline int32_t* get_address_of_TaskContainerIndex_6() { return &___TaskContainerIndex_6; }
	inline void set_TaskContainerIndex_6(int32_t value)
	{
		___TaskContainerIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTASK_1_T2103EEFF091DD4A943C23C8C77591716114AFEF3_H
#ifndef BASICTASK_1_T441E728836BB86EF4FACC39258BA9F3AE70D4C99_H
#define BASICTASK_1_T441E728836BB86EF4FACC39258BA9F3AE70D4C99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTask`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>
struct  BasicTask_1_t441E728836BB86EF4FACC39258BA9F3AE70D4C99  : public RuntimeObject
{
public:
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEvent
	OnTaskCompleted_1_tBBB582D5B9BFD572A3E5CA228F8B03A453171F07 * ____onTaskCompletedEvent_0;
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEventInternal
	OnTaskCompleted_1_tBBB582D5B9BFD572A3E5CA228F8B03A453171F07 * ____onTaskCompletedEventInternal_1;
	// T Tayr.BasicTask`1::Result
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___Result_2;
	// System.Boolean Tayr.BasicTask`1::_isDone
	bool ____isDone_3;
	// Tayr.ITaskRunner Tayr.BasicTask`1::_taskRunner
	RuntimeObject* ____taskRunner_4;
	// Tayr.ITaskContainer Tayr.BasicTask`1::_taskContainer
	RuntimeObject* ____taskContainer_5;
	// System.Int32 Tayr.BasicTask`1::TaskContainerIndex
	int32_t ___TaskContainerIndex_6;

public:
	inline static int32_t get_offset_of__onTaskCompletedEvent_0() { return static_cast<int32_t>(offsetof(BasicTask_1_t441E728836BB86EF4FACC39258BA9F3AE70D4C99, ____onTaskCompletedEvent_0)); }
	inline OnTaskCompleted_1_tBBB582D5B9BFD572A3E5CA228F8B03A453171F07 * get__onTaskCompletedEvent_0() const { return ____onTaskCompletedEvent_0; }
	inline OnTaskCompleted_1_tBBB582D5B9BFD572A3E5CA228F8B03A453171F07 ** get_address_of__onTaskCompletedEvent_0() { return &____onTaskCompletedEvent_0; }
	inline void set__onTaskCompletedEvent_0(OnTaskCompleted_1_tBBB582D5B9BFD572A3E5CA228F8B03A453171F07 * value)
	{
		____onTaskCompletedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEvent_0), value);
	}

	inline static int32_t get_offset_of__onTaskCompletedEventInternal_1() { return static_cast<int32_t>(offsetof(BasicTask_1_t441E728836BB86EF4FACC39258BA9F3AE70D4C99, ____onTaskCompletedEventInternal_1)); }
	inline OnTaskCompleted_1_tBBB582D5B9BFD572A3E5CA228F8B03A453171F07 * get__onTaskCompletedEventInternal_1() const { return ____onTaskCompletedEventInternal_1; }
	inline OnTaskCompleted_1_tBBB582D5B9BFD572A3E5CA228F8B03A453171F07 ** get_address_of__onTaskCompletedEventInternal_1() { return &____onTaskCompletedEventInternal_1; }
	inline void set__onTaskCompletedEventInternal_1(OnTaskCompleted_1_tBBB582D5B9BFD572A3E5CA228F8B03A453171F07 * value)
	{
		____onTaskCompletedEventInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEventInternal_1), value);
	}

	inline static int32_t get_offset_of_Result_2() { return static_cast<int32_t>(offsetof(BasicTask_1_t441E728836BB86EF4FACC39258BA9F3AE70D4C99, ___Result_2)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_Result_2() const { return ___Result_2; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_Result_2() { return &___Result_2; }
	inline void set_Result_2(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___Result_2 = value;
		Il2CppCodeGenWriteBarrier((&___Result_2), value);
	}

	inline static int32_t get_offset_of__isDone_3() { return static_cast<int32_t>(offsetof(BasicTask_1_t441E728836BB86EF4FACC39258BA9F3AE70D4C99, ____isDone_3)); }
	inline bool get__isDone_3() const { return ____isDone_3; }
	inline bool* get_address_of__isDone_3() { return &____isDone_3; }
	inline void set__isDone_3(bool value)
	{
		____isDone_3 = value;
	}

	inline static int32_t get_offset_of__taskRunner_4() { return static_cast<int32_t>(offsetof(BasicTask_1_t441E728836BB86EF4FACC39258BA9F3AE70D4C99, ____taskRunner_4)); }
	inline RuntimeObject* get__taskRunner_4() const { return ____taskRunner_4; }
	inline RuntimeObject** get_address_of__taskRunner_4() { return &____taskRunner_4; }
	inline void set__taskRunner_4(RuntimeObject* value)
	{
		____taskRunner_4 = value;
		Il2CppCodeGenWriteBarrier((&____taskRunner_4), value);
	}

	inline static int32_t get_offset_of__taskContainer_5() { return static_cast<int32_t>(offsetof(BasicTask_1_t441E728836BB86EF4FACC39258BA9F3AE70D4C99, ____taskContainer_5)); }
	inline RuntimeObject* get__taskContainer_5() const { return ____taskContainer_5; }
	inline RuntimeObject** get_address_of__taskContainer_5() { return &____taskContainer_5; }
	inline void set__taskContainer_5(RuntimeObject* value)
	{
		____taskContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____taskContainer_5), value);
	}

	inline static int32_t get_offset_of_TaskContainerIndex_6() { return static_cast<int32_t>(offsetof(BasicTask_1_t441E728836BB86EF4FACC39258BA9F3AE70D4C99, ___TaskContainerIndex_6)); }
	inline int32_t get_TaskContainerIndex_6() const { return ___TaskContainerIndex_6; }
	inline int32_t* get_address_of_TaskContainerIndex_6() { return &___TaskContainerIndex_6; }
	inline void set_TaskContainerIndex_6(int32_t value)
	{
		___TaskContainerIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTASK_1_T441E728836BB86EF4FACC39258BA9F3AE70D4C99_H
#ifndef BASICTASK_1_TA96B338B10A9FF438D49FEEF0A97378798D99F57_H
#define BASICTASK_1_TA96B338B10A9FF438D49FEEF0A97378798D99F57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTask`1<System.Int32>
struct  BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57  : public RuntimeObject
{
public:
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEvent
	OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * ____onTaskCompletedEvent_0;
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEventInternal
	OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * ____onTaskCompletedEventInternal_1;
	// T Tayr.BasicTask`1::Result
	int32_t ___Result_2;
	// System.Boolean Tayr.BasicTask`1::_isDone
	bool ____isDone_3;
	// Tayr.ITaskRunner Tayr.BasicTask`1::_taskRunner
	RuntimeObject* ____taskRunner_4;
	// Tayr.ITaskContainer Tayr.BasicTask`1::_taskContainer
	RuntimeObject* ____taskContainer_5;
	// System.Int32 Tayr.BasicTask`1::TaskContainerIndex
	int32_t ___TaskContainerIndex_6;

public:
	inline static int32_t get_offset_of__onTaskCompletedEvent_0() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____onTaskCompletedEvent_0)); }
	inline OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * get__onTaskCompletedEvent_0() const { return ____onTaskCompletedEvent_0; }
	inline OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE ** get_address_of__onTaskCompletedEvent_0() { return &____onTaskCompletedEvent_0; }
	inline void set__onTaskCompletedEvent_0(OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * value)
	{
		____onTaskCompletedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEvent_0), value);
	}

	inline static int32_t get_offset_of__onTaskCompletedEventInternal_1() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____onTaskCompletedEventInternal_1)); }
	inline OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * get__onTaskCompletedEventInternal_1() const { return ____onTaskCompletedEventInternal_1; }
	inline OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE ** get_address_of__onTaskCompletedEventInternal_1() { return &____onTaskCompletedEventInternal_1; }
	inline void set__onTaskCompletedEventInternal_1(OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * value)
	{
		____onTaskCompletedEventInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEventInternal_1), value);
	}

	inline static int32_t get_offset_of_Result_2() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ___Result_2)); }
	inline int32_t get_Result_2() const { return ___Result_2; }
	inline int32_t* get_address_of_Result_2() { return &___Result_2; }
	inline void set_Result_2(int32_t value)
	{
		___Result_2 = value;
	}

	inline static int32_t get_offset_of__isDone_3() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____isDone_3)); }
	inline bool get__isDone_3() const { return ____isDone_3; }
	inline bool* get_address_of__isDone_3() { return &____isDone_3; }
	inline void set__isDone_3(bool value)
	{
		____isDone_3 = value;
	}

	inline static int32_t get_offset_of__taskRunner_4() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____taskRunner_4)); }
	inline RuntimeObject* get__taskRunner_4() const { return ____taskRunner_4; }
	inline RuntimeObject** get_address_of__taskRunner_4() { return &____taskRunner_4; }
	inline void set__taskRunner_4(RuntimeObject* value)
	{
		____taskRunner_4 = value;
		Il2CppCodeGenWriteBarrier((&____taskRunner_4), value);
	}

	inline static int32_t get_offset_of__taskContainer_5() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____taskContainer_5)); }
	inline RuntimeObject* get__taskContainer_5() const { return ____taskContainer_5; }
	inline RuntimeObject** get_address_of__taskContainer_5() { return &____taskContainer_5; }
	inline void set__taskContainer_5(RuntimeObject* value)
	{
		____taskContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____taskContainer_5), value);
	}

	inline static int32_t get_offset_of_TaskContainerIndex_6() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ___TaskContainerIndex_6)); }
	inline int32_t get_TaskContainerIndex_6() const { return ___TaskContainerIndex_6; }
	inline int32_t* get_address_of_TaskContainerIndex_6() { return &___TaskContainerIndex_6; }
	inline void set_TaskContainerIndex_6(int32_t value)
	{
		___TaskContainerIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTASK_1_TA96B338B10A9FF438D49FEEF0A97378798D99F57_H
#ifndef BASICTASK_1_TE70B7733E99882370633F0BDBE9D6208EB03B77E_H
#define BASICTASK_1_TE70B7733E99882370633F0BDBE9D6208EB03B77E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTask`1<System.String>
struct  BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E  : public RuntimeObject
{
public:
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEvent
	OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 * ____onTaskCompletedEvent_0;
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEventInternal
	OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 * ____onTaskCompletedEventInternal_1;
	// T Tayr.BasicTask`1::Result
	String_t* ___Result_2;
	// System.Boolean Tayr.BasicTask`1::_isDone
	bool ____isDone_3;
	// Tayr.ITaskRunner Tayr.BasicTask`1::_taskRunner
	RuntimeObject* ____taskRunner_4;
	// Tayr.ITaskContainer Tayr.BasicTask`1::_taskContainer
	RuntimeObject* ____taskContainer_5;
	// System.Int32 Tayr.BasicTask`1::TaskContainerIndex
	int32_t ___TaskContainerIndex_6;

public:
	inline static int32_t get_offset_of__onTaskCompletedEvent_0() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ____onTaskCompletedEvent_0)); }
	inline OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 * get__onTaskCompletedEvent_0() const { return ____onTaskCompletedEvent_0; }
	inline OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 ** get_address_of__onTaskCompletedEvent_0() { return &____onTaskCompletedEvent_0; }
	inline void set__onTaskCompletedEvent_0(OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 * value)
	{
		____onTaskCompletedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEvent_0), value);
	}

	inline static int32_t get_offset_of__onTaskCompletedEventInternal_1() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ____onTaskCompletedEventInternal_1)); }
	inline OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 * get__onTaskCompletedEventInternal_1() const { return ____onTaskCompletedEventInternal_1; }
	inline OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 ** get_address_of__onTaskCompletedEventInternal_1() { return &____onTaskCompletedEventInternal_1; }
	inline void set__onTaskCompletedEventInternal_1(OnTaskCompleted_1_tF855A48AFF64D869CA45995B6E9A47AC3C074E74 * value)
	{
		____onTaskCompletedEventInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEventInternal_1), value);
	}

	inline static int32_t get_offset_of_Result_2() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ___Result_2)); }
	inline String_t* get_Result_2() const { return ___Result_2; }
	inline String_t** get_address_of_Result_2() { return &___Result_2; }
	inline void set_Result_2(String_t* value)
	{
		___Result_2 = value;
		Il2CppCodeGenWriteBarrier((&___Result_2), value);
	}

	inline static int32_t get_offset_of__isDone_3() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ____isDone_3)); }
	inline bool get__isDone_3() const { return ____isDone_3; }
	inline bool* get_address_of__isDone_3() { return &____isDone_3; }
	inline void set__isDone_3(bool value)
	{
		____isDone_3 = value;
	}

	inline static int32_t get_offset_of__taskRunner_4() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ____taskRunner_4)); }
	inline RuntimeObject* get__taskRunner_4() const { return ____taskRunner_4; }
	inline RuntimeObject** get_address_of__taskRunner_4() { return &____taskRunner_4; }
	inline void set__taskRunner_4(RuntimeObject* value)
	{
		____taskRunner_4 = value;
		Il2CppCodeGenWriteBarrier((&____taskRunner_4), value);
	}

	inline static int32_t get_offset_of__taskContainer_5() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ____taskContainer_5)); }
	inline RuntimeObject* get__taskContainer_5() const { return ____taskContainer_5; }
	inline RuntimeObject** get_address_of__taskContainer_5() { return &____taskContainer_5; }
	inline void set__taskContainer_5(RuntimeObject* value)
	{
		____taskContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____taskContainer_5), value);
	}

	inline static int32_t get_offset_of_TaskContainerIndex_6() { return static_cast<int32_t>(offsetof(BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E, ___TaskContainerIndex_6)); }
	inline int32_t get_TaskContainerIndex_6() const { return ___TaskContainerIndex_6; }
	inline int32_t* get_address_of_TaskContainerIndex_6() { return &___TaskContainerIndex_6; }
	inline void set_TaskContainerIndex_6(int32_t value)
	{
		___TaskContainerIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTASK_1_TE70B7733E99882370633F0BDBE9D6208EB03B77E_H
#ifndef BASICTASK_1_T7F49147173E4A1AC129308B42CDD067972B5B75A_H
#define BASICTASK_1_T7F49147173E4A1AC129308B42CDD067972B5B75A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTask`1<Tayr.ILibrary>
struct  BasicTask_1_t7F49147173E4A1AC129308B42CDD067972B5B75A  : public RuntimeObject
{
public:
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEvent
	OnTaskCompleted_1_t5259FE6B7912622DF08844AF6D84DA0F3F5EE3FB * ____onTaskCompletedEvent_0;
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEventInternal
	OnTaskCompleted_1_t5259FE6B7912622DF08844AF6D84DA0F3F5EE3FB * ____onTaskCompletedEventInternal_1;
	// T Tayr.BasicTask`1::Result
	RuntimeObject* ___Result_2;
	// System.Boolean Tayr.BasicTask`1::_isDone
	bool ____isDone_3;
	// Tayr.ITaskRunner Tayr.BasicTask`1::_taskRunner
	RuntimeObject* ____taskRunner_4;
	// Tayr.ITaskContainer Tayr.BasicTask`1::_taskContainer
	RuntimeObject* ____taskContainer_5;
	// System.Int32 Tayr.BasicTask`1::TaskContainerIndex
	int32_t ___TaskContainerIndex_6;

public:
	inline static int32_t get_offset_of__onTaskCompletedEvent_0() { return static_cast<int32_t>(offsetof(BasicTask_1_t7F49147173E4A1AC129308B42CDD067972B5B75A, ____onTaskCompletedEvent_0)); }
	inline OnTaskCompleted_1_t5259FE6B7912622DF08844AF6D84DA0F3F5EE3FB * get__onTaskCompletedEvent_0() const { return ____onTaskCompletedEvent_0; }
	inline OnTaskCompleted_1_t5259FE6B7912622DF08844AF6D84DA0F3F5EE3FB ** get_address_of__onTaskCompletedEvent_0() { return &____onTaskCompletedEvent_0; }
	inline void set__onTaskCompletedEvent_0(OnTaskCompleted_1_t5259FE6B7912622DF08844AF6D84DA0F3F5EE3FB * value)
	{
		____onTaskCompletedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEvent_0), value);
	}

	inline static int32_t get_offset_of__onTaskCompletedEventInternal_1() { return static_cast<int32_t>(offsetof(BasicTask_1_t7F49147173E4A1AC129308B42CDD067972B5B75A, ____onTaskCompletedEventInternal_1)); }
	inline OnTaskCompleted_1_t5259FE6B7912622DF08844AF6D84DA0F3F5EE3FB * get__onTaskCompletedEventInternal_1() const { return ____onTaskCompletedEventInternal_1; }
	inline OnTaskCompleted_1_t5259FE6B7912622DF08844AF6D84DA0F3F5EE3FB ** get_address_of__onTaskCompletedEventInternal_1() { return &____onTaskCompletedEventInternal_1; }
	inline void set__onTaskCompletedEventInternal_1(OnTaskCompleted_1_t5259FE6B7912622DF08844AF6D84DA0F3F5EE3FB * value)
	{
		____onTaskCompletedEventInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEventInternal_1), value);
	}

	inline static int32_t get_offset_of_Result_2() { return static_cast<int32_t>(offsetof(BasicTask_1_t7F49147173E4A1AC129308B42CDD067972B5B75A, ___Result_2)); }
	inline RuntimeObject* get_Result_2() const { return ___Result_2; }
	inline RuntimeObject** get_address_of_Result_2() { return &___Result_2; }
	inline void set_Result_2(RuntimeObject* value)
	{
		___Result_2 = value;
		Il2CppCodeGenWriteBarrier((&___Result_2), value);
	}

	inline static int32_t get_offset_of__isDone_3() { return static_cast<int32_t>(offsetof(BasicTask_1_t7F49147173E4A1AC129308B42CDD067972B5B75A, ____isDone_3)); }
	inline bool get__isDone_3() const { return ____isDone_3; }
	inline bool* get_address_of__isDone_3() { return &____isDone_3; }
	inline void set__isDone_3(bool value)
	{
		____isDone_3 = value;
	}

	inline static int32_t get_offset_of__taskRunner_4() { return static_cast<int32_t>(offsetof(BasicTask_1_t7F49147173E4A1AC129308B42CDD067972B5B75A, ____taskRunner_4)); }
	inline RuntimeObject* get__taskRunner_4() const { return ____taskRunner_4; }
	inline RuntimeObject** get_address_of__taskRunner_4() { return &____taskRunner_4; }
	inline void set__taskRunner_4(RuntimeObject* value)
	{
		____taskRunner_4 = value;
		Il2CppCodeGenWriteBarrier((&____taskRunner_4), value);
	}

	inline static int32_t get_offset_of__taskContainer_5() { return static_cast<int32_t>(offsetof(BasicTask_1_t7F49147173E4A1AC129308B42CDD067972B5B75A, ____taskContainer_5)); }
	inline RuntimeObject* get__taskContainer_5() const { return ____taskContainer_5; }
	inline RuntimeObject** get_address_of__taskContainer_5() { return &____taskContainer_5; }
	inline void set__taskContainer_5(RuntimeObject* value)
	{
		____taskContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____taskContainer_5), value);
	}

	inline static int32_t get_offset_of_TaskContainerIndex_6() { return static_cast<int32_t>(offsetof(BasicTask_1_t7F49147173E4A1AC129308B42CDD067972B5B75A, ___TaskContainerIndex_6)); }
	inline int32_t get_TaskContainerIndex_6() const { return ___TaskContainerIndex_6; }
	inline int32_t* get_address_of_TaskContainerIndex_6() { return &___TaskContainerIndex_6; }
	inline void set_TaskContainerIndex_6(int32_t value)
	{
		___TaskContainerIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTASK_1_T7F49147173E4A1AC129308B42CDD067972B5B75A_H
#ifndef BASICTASK_1_T061E76CF1C2670C446FFAC160877B71F63E09121_H
#define BASICTASK_1_T061E76CF1C2670C446FFAC160877B71F63E09121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTask`1<Tayr.MissingAssetsGroup>
struct  BasicTask_1_t061E76CF1C2670C446FFAC160877B71F63E09121  : public RuntimeObject
{
public:
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEvent
	OnTaskCompleted_1_tB39638D949906BF08283F9CE0C05C80116F19ED4 * ____onTaskCompletedEvent_0;
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEventInternal
	OnTaskCompleted_1_tB39638D949906BF08283F9CE0C05C80116F19ED4 * ____onTaskCompletedEventInternal_1;
	// T Tayr.BasicTask`1::Result
	MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1 * ___Result_2;
	// System.Boolean Tayr.BasicTask`1::_isDone
	bool ____isDone_3;
	// Tayr.ITaskRunner Tayr.BasicTask`1::_taskRunner
	RuntimeObject* ____taskRunner_4;
	// Tayr.ITaskContainer Tayr.BasicTask`1::_taskContainer
	RuntimeObject* ____taskContainer_5;
	// System.Int32 Tayr.BasicTask`1::TaskContainerIndex
	int32_t ___TaskContainerIndex_6;

public:
	inline static int32_t get_offset_of__onTaskCompletedEvent_0() { return static_cast<int32_t>(offsetof(BasicTask_1_t061E76CF1C2670C446FFAC160877B71F63E09121, ____onTaskCompletedEvent_0)); }
	inline OnTaskCompleted_1_tB39638D949906BF08283F9CE0C05C80116F19ED4 * get__onTaskCompletedEvent_0() const { return ____onTaskCompletedEvent_0; }
	inline OnTaskCompleted_1_tB39638D949906BF08283F9CE0C05C80116F19ED4 ** get_address_of__onTaskCompletedEvent_0() { return &____onTaskCompletedEvent_0; }
	inline void set__onTaskCompletedEvent_0(OnTaskCompleted_1_tB39638D949906BF08283F9CE0C05C80116F19ED4 * value)
	{
		____onTaskCompletedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEvent_0), value);
	}

	inline static int32_t get_offset_of__onTaskCompletedEventInternal_1() { return static_cast<int32_t>(offsetof(BasicTask_1_t061E76CF1C2670C446FFAC160877B71F63E09121, ____onTaskCompletedEventInternal_1)); }
	inline OnTaskCompleted_1_tB39638D949906BF08283F9CE0C05C80116F19ED4 * get__onTaskCompletedEventInternal_1() const { return ____onTaskCompletedEventInternal_1; }
	inline OnTaskCompleted_1_tB39638D949906BF08283F9CE0C05C80116F19ED4 ** get_address_of__onTaskCompletedEventInternal_1() { return &____onTaskCompletedEventInternal_1; }
	inline void set__onTaskCompletedEventInternal_1(OnTaskCompleted_1_tB39638D949906BF08283F9CE0C05C80116F19ED4 * value)
	{
		____onTaskCompletedEventInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEventInternal_1), value);
	}

	inline static int32_t get_offset_of_Result_2() { return static_cast<int32_t>(offsetof(BasicTask_1_t061E76CF1C2670C446FFAC160877B71F63E09121, ___Result_2)); }
	inline MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1 * get_Result_2() const { return ___Result_2; }
	inline MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1 ** get_address_of_Result_2() { return &___Result_2; }
	inline void set_Result_2(MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1 * value)
	{
		___Result_2 = value;
		Il2CppCodeGenWriteBarrier((&___Result_2), value);
	}

	inline static int32_t get_offset_of__isDone_3() { return static_cast<int32_t>(offsetof(BasicTask_1_t061E76CF1C2670C446FFAC160877B71F63E09121, ____isDone_3)); }
	inline bool get__isDone_3() const { return ____isDone_3; }
	inline bool* get_address_of__isDone_3() { return &____isDone_3; }
	inline void set__isDone_3(bool value)
	{
		____isDone_3 = value;
	}

	inline static int32_t get_offset_of__taskRunner_4() { return static_cast<int32_t>(offsetof(BasicTask_1_t061E76CF1C2670C446FFAC160877B71F63E09121, ____taskRunner_4)); }
	inline RuntimeObject* get__taskRunner_4() const { return ____taskRunner_4; }
	inline RuntimeObject** get_address_of__taskRunner_4() { return &____taskRunner_4; }
	inline void set__taskRunner_4(RuntimeObject* value)
	{
		____taskRunner_4 = value;
		Il2CppCodeGenWriteBarrier((&____taskRunner_4), value);
	}

	inline static int32_t get_offset_of__taskContainer_5() { return static_cast<int32_t>(offsetof(BasicTask_1_t061E76CF1C2670C446FFAC160877B71F63E09121, ____taskContainer_5)); }
	inline RuntimeObject* get__taskContainer_5() const { return ____taskContainer_5; }
	inline RuntimeObject** get_address_of__taskContainer_5() { return &____taskContainer_5; }
	inline void set__taskContainer_5(RuntimeObject* value)
	{
		____taskContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____taskContainer_5), value);
	}

	inline static int32_t get_offset_of_TaskContainerIndex_6() { return static_cast<int32_t>(offsetof(BasicTask_1_t061E76CF1C2670C446FFAC160877B71F63E09121, ___TaskContainerIndex_6)); }
	inline int32_t get_TaskContainerIndex_6() const { return ___TaskContainerIndex_6; }
	inline int32_t* get_address_of_TaskContainerIndex_6() { return &___TaskContainerIndex_6; }
	inline void set_TaskContainerIndex_6(int32_t value)
	{
		___TaskContainerIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTASK_1_T061E76CF1C2670C446FFAC160877B71F63E09121_H
#ifndef COMMANDFACTORY_T3B1CC884776C46DF4E4D4E404A1904C730C9EE81_H
#define COMMANDFACTORY_T3B1CC884776C46DF4E4D4E404A1904C730C9EE81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.CommandFactory
struct  CommandFactory_t3B1CC884776C46DF4E4D4E404A1904C730C9EE81  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDFACTORY_T3B1CC884776C46DF4E4D4E404A1904C730C9EE81_H
#ifndef COROUTINERUNNER_T55D9FE1CD110034DE5497E8A93A4F8E55AAE7097_H
#define COROUTINERUNNER_T55D9FE1CD110034DE5497E8A93A4F8E55AAE7097_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.CoroutineRunner
struct  CoroutineRunner_t55D9FE1CD110034DE5497E8A93A4F8E55AAE7097  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COROUTINERUNNER_T55D9FE1CD110034DE5497E8A93A4F8E55AAE7097_H
#ifndef DOTWEENHANDLER_T6DF9AE6CF73A4B646F43FECC59BF28142A8E0939_H
#define DOTWEENHANDLER_T6DF9AE6CF73A4B646F43FECC59BF28142A8E0939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.DOTweenHandler
struct  DOTweenHandler_t6DF9AE6CF73A4B646F43FECC59BF28142A8E0939  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENHANDLER_T6DF9AE6CF73A4B646F43FECC59BF28142A8E0939_H
#ifndef U3CTASKU3ED__2_TB76CA1F55FF199E00748C1353CA3B0905774C2D9_H
#define U3CTASKU3ED__2_TB76CA1F55FF199E00748C1353CA3B0905774C2D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.DepotFileLoaderTask_<Task>d__2
struct  U3CTaskU3Ed__2_tB76CA1F55FF199E00748C1353CA3B0905774C2D9  : public RuntimeObject
{
public:
	// System.Int32 Tayr.DepotFileLoaderTask_<Task>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.DepotFileLoaderTask_<Task>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.DepotFileLoaderTask Tayr.DepotFileLoaderTask_<Task>d__2::<>4__this
	DepotFileLoaderTask_t8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tB76CA1F55FF199E00748C1353CA3B0905774C2D9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tB76CA1F55FF199E00748C1353CA3B0905774C2D9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tB76CA1F55FF199E00748C1353CA3B0905774C2D9, ___U3CU3E4__this_2)); }
	inline DepotFileLoaderTask_t8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DepotFileLoaderTask_t8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DepotFileLoaderTask_t8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__2_TB76CA1F55FF199E00748C1353CA3B0905774C2D9_H
#ifndef DICORE_TF6CFCD21895475F9ECC5DB7FDB06093B89E55061_H
#define DICORE_TF6CFCD21895475F9ECC5DB7FDB06093B89E55061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.DiCore
struct  DiCore_tF6CFCD21895475F9ECC5DB7FDB06093B89E55061  : public RuntimeObject
{
public:

public:
};

struct DiCore_tF6CFCD21895475F9ECC5DB7FDB06093B89E55061_StaticFields
{
public:
	// Zenject.DiContainer Tayr.DiCore::Container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ___Container_1;

public:
	inline static int32_t get_offset_of_Container_1() { return static_cast<int32_t>(offsetof(DiCore_tF6CFCD21895475F9ECC5DB7FDB06093B89E55061_StaticFields, ___Container_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get_Container_1() const { return ___Container_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of_Container_1() { return &___Container_1; }
	inline void set_Container_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		___Container_1 = value;
		Il2CppCodeGenWriteBarrier((&___Container_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICORE_TF6CFCD21895475F9ECC5DB7FDB06093B89E55061_H
#ifndef U3CTASKU3ED__3_TA83839BA6DD6AC4B9DA308F04018E0331D810A4B_H
#define U3CTASKU3ED__3_TA83839BA6DD6AC4B9DA308F04018E0331D810A4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.DownloadFileTask_<Task>d__3
struct  U3CTaskU3Ed__3_tA83839BA6DD6AC4B9DA308F04018E0331D810A4B  : public RuntimeObject
{
public:
	// System.Int32 Tayr.DownloadFileTask_<Task>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.DownloadFileTask_<Task>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.DownloadFileTask Tayr.DownloadFileTask_<Task>d__3::<>4__this
	DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F * ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest Tayr.DownloadFileTask_<Task>d__3::<www>5__2
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwwwU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__3_tA83839BA6DD6AC4B9DA308F04018E0331D810A4B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__3_tA83839BA6DD6AC4B9DA308F04018E0331D810A4B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__3_tA83839BA6DD6AC4B9DA308F04018E0331D810A4B, ___U3CU3E4__this_2)); }
	inline DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__3_tA83839BA6DD6AC4B9DA308F04018E0331D810A4B, ___U3CwwwU3E5__2_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwwwU3E5__2_3() const { return ___U3CwwwU3E5__2_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwwwU3E5__2_3() { return &___U3CwwwU3E5__2_3; }
	inline void set_U3CwwwU3E5__2_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwwwU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__3_TA83839BA6DD6AC4B9DA308F04018E0331D810A4B_H
#ifndef FILEUTILS_T6B19A7B46ACB859C0A3D613EA39CD6376CCC1E00_H
#define FILEUTILS_T6B19A7B46ACB859C0A3D613EA39CD6376CCC1E00_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.FileUtils
struct  FileUtils_t6B19A7B46ACB859C0A3D613EA39CD6376CCC1E00  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEUTILS_T6B19A7B46ACB859C0A3D613EA39CD6376CCC1E00_H
#ifndef GAMEOBJECTEXTENSION_TDC2B83D8A14FDEEFBD564E75B618672A8F2069D4_H
#define GAMEOBJECTEXTENSION_TDC2B83D8A14FDEEFBD564E75B618672A8F2069D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.GameObjectExtension
struct  GameObjectExtension_tDC2B83D8A14FDEEFBD564E75B618672A8F2069D4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEXTENSION_TDC2B83D8A14FDEEFBD564E75B618672A8F2069D4_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_TBAE365A26C3DEAF200733A94B49C1DE99A051A34_H
#define U3CU3EC__DISPLAYCLASS8_0_TBAE365A26C3DEAF200733A94B49C1DE99A051A34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.GameObjectPool_<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tBAE365A26C3DEAF200733A94B49C1DE99A051A34  : public RuntimeObject
{
public:
	// Tayr.GameObjectPool Tayr.GameObjectPool_<>c__DisplayClass8_0::<>4__this
	GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * ___U3CU3E4__this_0;
	// UnityEngine.GameObject Tayr.GameObjectPool_<>c__DisplayClass8_0::prefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___prefab_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tBAE365A26C3DEAF200733A94B49C1DE99A051A34, ___U3CU3E4__this_0)); }
	inline GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_prefab_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tBAE365A26C3DEAF200733A94B49C1DE99A051A34, ___prefab_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_prefab_1() const { return ___prefab_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_prefab_1() { return &___prefab_1; }
	inline void set_prefab_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_TBAE365A26C3DEAF200733A94B49C1DE99A051A34_H
#ifndef JSONCONFIGURATOR_T4300FF6941F6A5F99CB53A04D924CFEAC2DD5B91_H
#define JSONCONFIGURATOR_T4300FF6941F6A5F99CB53A04D924CFEAC2DD5B91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.JsonConfigurator
struct  JsonConfigurator_t4300FF6941F6A5F99CB53A04D924CFEAC2DD5B91  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONFIGURATOR_T4300FF6941F6A5F99CB53A04D924CFEAC2DD5B91_H
#ifndef U3CTASKU3ED__7_TB45D5E1AB2605BD2133466C0A5325A7326ACBADD_H
#define U3CTASKU3ED__7_TB45D5E1AB2605BD2133466C0A5325A7326ACBADD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.LoadCSVLocalizationTask_<Task>d__7
struct  U3CTaskU3Ed__7_tB45D5E1AB2605BD2133466C0A5325A7326ACBADD  : public RuntimeObject
{
public:
	// System.Int32 Tayr.LoadCSVLocalizationTask_<Task>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.LoadCSVLocalizationTask_<Task>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.LoadCSVLocalizationTask Tayr.LoadCSVLocalizationTask_<Task>d__7::<>4__this
	LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78 * ___U3CU3E4__this_2;
	// Tayr.MultiLookupLoaderTask Tayr.LoadCSVLocalizationTask_<Task>d__7::<loaderTask>5__2
	MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * ___U3CloaderTaskU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__7_tB45D5E1AB2605BD2133466C0A5325A7326ACBADD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__7_tB45D5E1AB2605BD2133466C0A5325A7326ACBADD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__7_tB45D5E1AB2605BD2133466C0A5325A7326ACBADD, ___U3CU3E4__this_2)); }
	inline LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CloaderTaskU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__7_tB45D5E1AB2605BD2133466C0A5325A7326ACBADD, ___U3CloaderTaskU3E5__2_3)); }
	inline MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * get_U3CloaderTaskU3E5__2_3() const { return ___U3CloaderTaskU3E5__2_3; }
	inline MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A ** get_address_of_U3CloaderTaskU3E5__2_3() { return &___U3CloaderTaskU3E5__2_3; }
	inline void set_U3CloaderTaskU3E5__2_3(MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * value)
	{
		___U3CloaderTaskU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloaderTaskU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__7_TB45D5E1AB2605BD2133466C0A5325A7326ACBADD_H
#ifndef U3CTASKU3ED__5_TF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0_H
#define U3CTASKU3ED__5_TF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.LoadHashFileTask_<Task>d__5
struct  U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0  : public RuntimeObject
{
public:
	// System.Int32 Tayr.LoadHashFileTask_<Task>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.LoadHashFileTask_<Task>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.LoadHashFileTask Tayr.LoadHashFileTask_<Task>d__5::<>4__this
	LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552 * ___U3CU3E4__this_2;
	// Tayr.MultiLookupLoaderTask Tayr.LoadHashFileTask_<Task>d__5::<multiLookupLoader>5__2
	MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * ___U3CmultiLookupLoaderU3E5__2_3;
	// Tayr.DownloadFileTask Tayr.LoadHashFileTask_<Task>d__5::<downloadFileTask>5__3
	DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F * ___U3CdownloadFileTaskU3E5__3_4;
	// Tayr.MultiLookupLoaderTask Tayr.LoadHashFileTask_<Task>d__5::<multiLookupLoaderNew>5__4
	MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * ___U3CmultiLookupLoaderNewU3E5__4_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0, ___U3CU3E4__this_2)); }
	inline LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CmultiLookupLoaderU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0, ___U3CmultiLookupLoaderU3E5__2_3)); }
	inline MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * get_U3CmultiLookupLoaderU3E5__2_3() const { return ___U3CmultiLookupLoaderU3E5__2_3; }
	inline MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A ** get_address_of_U3CmultiLookupLoaderU3E5__2_3() { return &___U3CmultiLookupLoaderU3E5__2_3; }
	inline void set_U3CmultiLookupLoaderU3E5__2_3(MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * value)
	{
		___U3CmultiLookupLoaderU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmultiLookupLoaderU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CdownloadFileTaskU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0, ___U3CdownloadFileTaskU3E5__3_4)); }
	inline DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F * get_U3CdownloadFileTaskU3E5__3_4() const { return ___U3CdownloadFileTaskU3E5__3_4; }
	inline DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F ** get_address_of_U3CdownloadFileTaskU3E5__3_4() { return &___U3CdownloadFileTaskU3E5__3_4; }
	inline void set_U3CdownloadFileTaskU3E5__3_4(DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F * value)
	{
		___U3CdownloadFileTaskU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdownloadFileTaskU3E5__3_4), value);
	}

	inline static int32_t get_offset_of_U3CmultiLookupLoaderNewU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0, ___U3CmultiLookupLoaderNewU3E5__4_5)); }
	inline MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * get_U3CmultiLookupLoaderNewU3E5__4_5() const { return ___U3CmultiLookupLoaderNewU3E5__4_5; }
	inline MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A ** get_address_of_U3CmultiLookupLoaderNewU3E5__4_5() { return &___U3CmultiLookupLoaderNewU3E5__4_5; }
	inline void set_U3CmultiLookupLoaderNewU3E5__4_5(MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * value)
	{
		___U3CmultiLookupLoaderNewU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmultiLookupLoaderNewU3E5__4_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__5_TF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0_H
#ifndef U3CTASKU3ED__2_T6054B9CF056BDC9A4E1A15DD7F3187A12F8927F8_H
#define U3CTASKU3ED__2_T6054B9CF056BDC9A4E1A15DD7F3187A12F8927F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.LocalFileLoaderTask_<Task>d__2
struct  U3CTaskU3Ed__2_t6054B9CF056BDC9A4E1A15DD7F3187A12F8927F8  : public RuntimeObject
{
public:
	// System.Int32 Tayr.LocalFileLoaderTask_<Task>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.LocalFileLoaderTask_<Task>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.LocalFileLoaderTask Tayr.LocalFileLoaderTask_<Task>d__2::<>4__this
	LocalFileLoaderTask_t4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t6054B9CF056BDC9A4E1A15DD7F3187A12F8927F8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t6054B9CF056BDC9A4E1A15DD7F3187A12F8927F8, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t6054B9CF056BDC9A4E1A15DD7F3187A12F8927F8, ___U3CU3E4__this_2)); }
	inline LocalFileLoaderTask_t4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LocalFileLoaderTask_t4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LocalFileLoaderTask_t4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__2_T6054B9CF056BDC9A4E1A15DD7F3187A12F8927F8_H
#ifndef MAPMODEL_T73414FA47D931828C3029249838AA32F48E39C84_H
#define MAPMODEL_T73414FA47D931828C3029249838AA32F48E39C84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.MapModel
struct  MapModel_t73414FA47D931828C3029249838AA32F48E39C84  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> Tayr.MapModel::Groups
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___Groups_0;

public:
	inline static int32_t get_offset_of_Groups_0() { return static_cast<int32_t>(offsetof(MapModel_t73414FA47D931828C3029249838AA32F48E39C84, ___Groups_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_Groups_0() const { return ___Groups_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_Groups_0() { return &___Groups_0; }
	inline void set_Groups_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___Groups_0 = value;
		Il2CppCodeGenWriteBarrier((&___Groups_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPMODEL_T73414FA47D931828C3029249838AA32F48E39C84_H
#ifndef MAPPINGINITIALIZER_T2D92363C6AC21A6B38A61CC24ACEDADB1302C53A_H
#define MAPPINGINITIALIZER_T2D92363C6AC21A6B38A61CC24ACEDADB1302C53A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.MappingInitializer
struct  MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> Tayr.MappingInitializer::AliasComponentsMapping
	Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * ___AliasComponentsMapping_0;
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> Tayr.MappingInitializer::ComponentsAliasMapping
	Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 * ___ComponentsAliasMapping_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> Tayr.MappingInitializer::SystemContainerMapping
	Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 * ___SystemContainerMapping_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Tayr.MappingInitializer::AliasContainersMapping
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___AliasContainersMapping_3;

public:
	inline static int32_t get_offset_of_AliasComponentsMapping_0() { return static_cast<int32_t>(offsetof(MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A, ___AliasComponentsMapping_0)); }
	inline Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * get_AliasComponentsMapping_0() const { return ___AliasComponentsMapping_0; }
	inline Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A ** get_address_of_AliasComponentsMapping_0() { return &___AliasComponentsMapping_0; }
	inline void set_AliasComponentsMapping_0(Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * value)
	{
		___AliasComponentsMapping_0 = value;
		Il2CppCodeGenWriteBarrier((&___AliasComponentsMapping_0), value);
	}

	inline static int32_t get_offset_of_ComponentsAliasMapping_1() { return static_cast<int32_t>(offsetof(MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A, ___ComponentsAliasMapping_1)); }
	inline Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 * get_ComponentsAliasMapping_1() const { return ___ComponentsAliasMapping_1; }
	inline Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 ** get_address_of_ComponentsAliasMapping_1() { return &___ComponentsAliasMapping_1; }
	inline void set_ComponentsAliasMapping_1(Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 * value)
	{
		___ComponentsAliasMapping_1 = value;
		Il2CppCodeGenWriteBarrier((&___ComponentsAliasMapping_1), value);
	}

	inline static int32_t get_offset_of_SystemContainerMapping_2() { return static_cast<int32_t>(offsetof(MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A, ___SystemContainerMapping_2)); }
	inline Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 * get_SystemContainerMapping_2() const { return ___SystemContainerMapping_2; }
	inline Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 ** get_address_of_SystemContainerMapping_2() { return &___SystemContainerMapping_2; }
	inline void set_SystemContainerMapping_2(Dictionary_2_t325762390EC6A243BD7A037A815D001885C77299 * value)
	{
		___SystemContainerMapping_2 = value;
		Il2CppCodeGenWriteBarrier((&___SystemContainerMapping_2), value);
	}

	inline static int32_t get_offset_of_AliasContainersMapping_3() { return static_cast<int32_t>(offsetof(MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A, ___AliasContainersMapping_3)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_AliasContainersMapping_3() const { return ___AliasContainersMapping_3; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_AliasContainersMapping_3() { return &___AliasContainersMapping_3; }
	inline void set_AliasContainersMapping_3(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___AliasContainersMapping_3 = value;
		Il2CppCodeGenWriteBarrier((&___AliasContainersMapping_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPPINGINITIALIZER_T2D92363C6AC21A6B38A61CC24ACEDADB1302C53A_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_TB31B2942C7B81602E2E78F47C741C09F5448970F_H
#define U3CU3EC__DISPLAYCLASS4_0_TB31B2942C7B81602E2E78F47C741C09F5448970F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.MappingInitializer_<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_tB31B2942C7B81602E2E78F47C741C09F5448970F  : public RuntimeObject
{
public:
	// System.String Tayr.MappingInitializer_<>c__DisplayClass4_0::assemblyName
	String_t* ___assemblyName_0;

public:
	inline static int32_t get_offset_of_assemblyName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tB31B2942C7B81602E2E78F47C741C09F5448970F, ___assemblyName_0)); }
	inline String_t* get_assemblyName_0() const { return ___assemblyName_0; }
	inline String_t** get_address_of_assemblyName_0() { return &___assemblyName_0; }
	inline void set_assemblyName_0(String_t* value)
	{
		___assemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_TB31B2942C7B81602E2E78F47C741C09F5448970F_H
#ifndef MISSINGASSETSGROUP_T4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1_H
#define MISSINGASSETSGROUP_T4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.MissingAssetsGroup
struct  MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Tayr.PatchAssetData> Tayr.MissingAssetsGroup::Assets
	List_1_t77284B88D10BB4C1F3F82DD37A8D645EC21AA396 * ___Assets_0;
	// System.Int64 Tayr.MissingAssetsGroup::RequiredSize
	int64_t ___RequiredSize_1;

public:
	inline static int32_t get_offset_of_Assets_0() { return static_cast<int32_t>(offsetof(MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1, ___Assets_0)); }
	inline List_1_t77284B88D10BB4C1F3F82DD37A8D645EC21AA396 * get_Assets_0() const { return ___Assets_0; }
	inline List_1_t77284B88D10BB4C1F3F82DD37A8D645EC21AA396 ** get_address_of_Assets_0() { return &___Assets_0; }
	inline void set_Assets_0(List_1_t77284B88D10BB4C1F3F82DD37A8D645EC21AA396 * value)
	{
		___Assets_0 = value;
		Il2CppCodeGenWriteBarrier((&___Assets_0), value);
	}

	inline static int32_t get_offset_of_RequiredSize_1() { return static_cast<int32_t>(offsetof(MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1, ___RequiredSize_1)); }
	inline int64_t get_RequiredSize_1() const { return ___RequiredSize_1; }
	inline int64_t* get_address_of_RequiredSize_1() { return &___RequiredSize_1; }
	inline void set_RequiredSize_1(int64_t value)
	{
		___RequiredSize_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGASSETSGROUP_T4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1_H
#ifndef MONOBEHAVIOREXTENSION_TBEB0A1CBE2C514439C15AD58ED0A45DA97420E0D_H
#define MONOBEHAVIOREXTENSION_TBEB0A1CBE2C514439C15AD58ED0A45DA97420E0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.MonoBehaviorExtension
struct  MonoBehaviorExtension_tBEB0A1CBE2C514439C15AD58ED0A45DA97420E0D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOREXTENSION_TBEB0A1CBE2C514439C15AD58ED0A45DA97420E0D_H
#ifndef U3CTASKU3ED__2_T3D43A0CB6ECBD878CFD598A7FE68F390593927A7_H
#define U3CTASKU3ED__2_T3D43A0CB6ECBD878CFD598A7FE68F390593927A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.MultiLookupLoaderTask_<Task>d__2
struct  U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7  : public RuntimeObject
{
public:
	// System.Int32 Tayr.MultiLookupLoaderTask_<Task>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.MultiLookupLoaderTask_<Task>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.MultiLookupLoaderTask Tayr.MultiLookupLoaderTask_<Task>d__2::<>4__this
	MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * ___U3CU3E4__this_2;
	// Tayr.LocalFileLoaderTask Tayr.MultiLookupLoaderTask_<Task>d__2::<loadLocalDataTask>5__2
	LocalFileLoaderTask_t4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD * ___U3CloadLocalDataTaskU3E5__2_3;
	// Tayr.StreamingAssetFileLoaderTask Tayr.MultiLookupLoaderTask_<Task>d__2::<loadStreamingAssetTask>5__3
	StreamingAssetFileLoaderTask_t766339DE3AEAB244DF39F9C963074DCCAEA2BA43 * ___U3CloadStreamingAssetTaskU3E5__3_4;
	// Tayr.ServerFileLoaderTask Tayr.MultiLookupLoaderTask_<Task>d__2::<loadServerDataTask>5__4
	ServerFileLoaderTask_t95314098EBA8E90891F1D8C5F19C0DC49D532F92 * ___U3CloadServerDataTaskU3E5__4_5;
	// Tayr.DepotFileLoaderTask Tayr.MultiLookupLoaderTask_<Task>d__2::<loadDepotDataTask>5__5
	DepotFileLoaderTask_t8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32 * ___U3CloadDepotDataTaskU3E5__5_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7, ___U3CU3E4__this_2)); }
	inline MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CloadLocalDataTaskU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7, ___U3CloadLocalDataTaskU3E5__2_3)); }
	inline LocalFileLoaderTask_t4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD * get_U3CloadLocalDataTaskU3E5__2_3() const { return ___U3CloadLocalDataTaskU3E5__2_3; }
	inline LocalFileLoaderTask_t4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD ** get_address_of_U3CloadLocalDataTaskU3E5__2_3() { return &___U3CloadLocalDataTaskU3E5__2_3; }
	inline void set_U3CloadLocalDataTaskU3E5__2_3(LocalFileLoaderTask_t4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD * value)
	{
		___U3CloadLocalDataTaskU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadLocalDataTaskU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CloadStreamingAssetTaskU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7, ___U3CloadStreamingAssetTaskU3E5__3_4)); }
	inline StreamingAssetFileLoaderTask_t766339DE3AEAB244DF39F9C963074DCCAEA2BA43 * get_U3CloadStreamingAssetTaskU3E5__3_4() const { return ___U3CloadStreamingAssetTaskU3E5__3_4; }
	inline StreamingAssetFileLoaderTask_t766339DE3AEAB244DF39F9C963074DCCAEA2BA43 ** get_address_of_U3CloadStreamingAssetTaskU3E5__3_4() { return &___U3CloadStreamingAssetTaskU3E5__3_4; }
	inline void set_U3CloadStreamingAssetTaskU3E5__3_4(StreamingAssetFileLoaderTask_t766339DE3AEAB244DF39F9C963074DCCAEA2BA43 * value)
	{
		___U3CloadStreamingAssetTaskU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadStreamingAssetTaskU3E5__3_4), value);
	}

	inline static int32_t get_offset_of_U3CloadServerDataTaskU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7, ___U3CloadServerDataTaskU3E5__4_5)); }
	inline ServerFileLoaderTask_t95314098EBA8E90891F1D8C5F19C0DC49D532F92 * get_U3CloadServerDataTaskU3E5__4_5() const { return ___U3CloadServerDataTaskU3E5__4_5; }
	inline ServerFileLoaderTask_t95314098EBA8E90891F1D8C5F19C0DC49D532F92 ** get_address_of_U3CloadServerDataTaskU3E5__4_5() { return &___U3CloadServerDataTaskU3E5__4_5; }
	inline void set_U3CloadServerDataTaskU3E5__4_5(ServerFileLoaderTask_t95314098EBA8E90891F1D8C5F19C0DC49D532F92 * value)
	{
		___U3CloadServerDataTaskU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadServerDataTaskU3E5__4_5), value);
	}

	inline static int32_t get_offset_of_U3CloadDepotDataTaskU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7, ___U3CloadDepotDataTaskU3E5__5_6)); }
	inline DepotFileLoaderTask_t8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32 * get_U3CloadDepotDataTaskU3E5__5_6() const { return ___U3CloadDepotDataTaskU3E5__5_6; }
	inline DepotFileLoaderTask_t8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32 ** get_address_of_U3CloadDepotDataTaskU3E5__5_6() { return &___U3CloadDepotDataTaskU3E5__5_6; }
	inline void set_U3CloadDepotDataTaskU3E5__5_6(DepotFileLoaderTask_t8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32 * value)
	{
		___U3CloadDepotDataTaskU3E5__5_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloadDepotDataTaskU3E5__5_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__2_T3D43A0CB6ECBD878CFD598A7FE68F390593927A7_H
#ifndef MULTITHREADRUNNER_T7E19EA5A8B0723D35CF76C72ADD4ED3701E10D40_H
#define MULTITHREADRUNNER_T7E19EA5A8B0723D35CF76C72ADD4ED3701E10D40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.MultiThreadRunner
struct  MultiThreadRunner_t7E19EA5A8B0723D35CF76C72ADD4ED3701E10D40  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTITHREADRUNNER_T7E19EA5A8B0723D35CF76C72ADD4ED3701E10D40_H
#ifndef NODECACHEDATA_T7817FA13D174980D397D0B532F2C3CACE43C3D7F_H
#define NODECACHEDATA_T7817FA13D174980D397D0B532F2C3CACE43C3D7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.NodeCacheData
struct  NodeCacheData_t7817FA13D174980D397D0B532F2C3CACE43C3D7F  : public RuntimeObject
{
public:
	// Tayr.INode Tayr.NodeCacheData::Node
	RuntimeObject* ___Node_0;
	// Tayr.ILibrary Tayr.NodeCacheData::Library
	RuntimeObject* ___Library_1;
	// System.String Tayr.NodeCacheData::Layer
	String_t* ___Layer_2;

public:
	inline static int32_t get_offset_of_Node_0() { return static_cast<int32_t>(offsetof(NodeCacheData_t7817FA13D174980D397D0B532F2C3CACE43C3D7F, ___Node_0)); }
	inline RuntimeObject* get_Node_0() const { return ___Node_0; }
	inline RuntimeObject** get_address_of_Node_0() { return &___Node_0; }
	inline void set_Node_0(RuntimeObject* value)
	{
		___Node_0 = value;
		Il2CppCodeGenWriteBarrier((&___Node_0), value);
	}

	inline static int32_t get_offset_of_Library_1() { return static_cast<int32_t>(offsetof(NodeCacheData_t7817FA13D174980D397D0B532F2C3CACE43C3D7F, ___Library_1)); }
	inline RuntimeObject* get_Library_1() const { return ___Library_1; }
	inline RuntimeObject** get_address_of_Library_1() { return &___Library_1; }
	inline void set_Library_1(RuntimeObject* value)
	{
		___Library_1 = value;
		Il2CppCodeGenWriteBarrier((&___Library_1), value);
	}

	inline static int32_t get_offset_of_Layer_2() { return static_cast<int32_t>(offsetof(NodeCacheData_t7817FA13D174980D397D0B532F2C3CACE43C3D7F, ___Layer_2)); }
	inline String_t* get_Layer_2() const { return ___Layer_2; }
	inline String_t** get_address_of_Layer_2() { return &___Layer_2; }
	inline void set_Layer_2(String_t* value)
	{
		___Layer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Layer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODECACHEDATA_T7817FA13D174980D397D0B532F2C3CACE43C3D7F_H
#ifndef NODEHELPER_TE40D988E7C5679FF18AE5191FBF524A9DBFEF945_H
#define NODEHELPER_TE40D988E7C5679FF18AE5191FBF524A9DBFEF945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.NodeHelper
struct  NodeHelper_tE40D988E7C5679FF18AE5191FBF524A9DBFEF945  : public RuntimeObject
{
public:

public:
};

struct NodeHelper_tE40D988E7C5679FF18AE5191FBF524A9DBFEF945_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Tayr.AnimationType,System.Type> Tayr.NodeHelper::AnimationTypeToHandler
	Dictionary_2_t055F14A9CCA952EF0DCF118E732AAD5F426E0206 * ___AnimationTypeToHandler_0;

public:
	inline static int32_t get_offset_of_AnimationTypeToHandler_0() { return static_cast<int32_t>(offsetof(NodeHelper_tE40D988E7C5679FF18AE5191FBF524A9DBFEF945_StaticFields, ___AnimationTypeToHandler_0)); }
	inline Dictionary_2_t055F14A9CCA952EF0DCF118E732AAD5F426E0206 * get_AnimationTypeToHandler_0() const { return ___AnimationTypeToHandler_0; }
	inline Dictionary_2_t055F14A9CCA952EF0DCF118E732AAD5F426E0206 ** get_address_of_AnimationTypeToHandler_0() { return &___AnimationTypeToHandler_0; }
	inline void set_AnimationTypeToHandler_0(Dictionary_2_t055F14A9CCA952EF0DCF118E732AAD5F426E0206 * value)
	{
		___AnimationTypeToHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationTypeToHandler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEHELPER_TE40D988E7C5679FF18AE5191FBF524A9DBFEF945_H
#ifndef NODESYSTEM_T28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE_H
#define NODESYSTEM_T28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.NodeSystem
struct  NodeSystem_t28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Tayr.NodeCacheData> Tayr.NodeSystem::_activeNodes
	List_1_t9394DDF96FA4463DFBB26378113201C873489995 * ____activeNodes_0;
	// UnityEngine.Transform Tayr.NodeSystem::_nodeParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____nodeParent_1;
	// UnityEngine.GameObject Tayr.NodeSystem::_root
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____root_2;

public:
	inline static int32_t get_offset_of__activeNodes_0() { return static_cast<int32_t>(offsetof(NodeSystem_t28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE, ____activeNodes_0)); }
	inline List_1_t9394DDF96FA4463DFBB26378113201C873489995 * get__activeNodes_0() const { return ____activeNodes_0; }
	inline List_1_t9394DDF96FA4463DFBB26378113201C873489995 ** get_address_of__activeNodes_0() { return &____activeNodes_0; }
	inline void set__activeNodes_0(List_1_t9394DDF96FA4463DFBB26378113201C873489995 * value)
	{
		____activeNodes_0 = value;
		Il2CppCodeGenWriteBarrier((&____activeNodes_0), value);
	}

	inline static int32_t get_offset_of__nodeParent_1() { return static_cast<int32_t>(offsetof(NodeSystem_t28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE, ____nodeParent_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__nodeParent_1() const { return ____nodeParent_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__nodeParent_1() { return &____nodeParent_1; }
	inline void set__nodeParent_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____nodeParent_1 = value;
		Il2CppCodeGenWriteBarrier((&____nodeParent_1), value);
	}

	inline static int32_t get_offset_of__root_2() { return static_cast<int32_t>(offsetof(NodeSystem_t28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE, ____root_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__root_2() const { return ____root_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__root_2() { return &____root_2; }
	inline void set__root_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____root_2 = value;
		Il2CppCodeGenWriteBarrier((&____root_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODESYSTEM_T28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE_H
#ifndef U3CU3EC__DISPLAYCLASS9_0_T4834D8551E6AD576D26116518514BBEBF63BFDCE_H
#define U3CU3EC__DISPLAYCLASS9_0_T4834D8551E6AD576D26116518514BBEBF63BFDCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.NodeSystem_<>c__DisplayClass9_0
struct  U3CU3Ec__DisplayClass9_0_t4834D8551E6AD576D26116518514BBEBF63BFDCE  : public RuntimeObject
{
public:
	// Tayr.INode Tayr.NodeSystem_<>c__DisplayClass9_0::node
	RuntimeObject* ___node_0;

public:
	inline static int32_t get_offset_of_node_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t4834D8551E6AD576D26116518514BBEBF63BFDCE, ___node_0)); }
	inline RuntimeObject* get_node_0() const { return ___node_0; }
	inline RuntimeObject** get_address_of_node_0() { return &___node_0; }
	inline void set_node_0(RuntimeObject* value)
	{
		___node_0 = value;
		Il2CppCodeGenWriteBarrier((&___node_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS9_0_T4834D8551E6AD576D26116518514BBEBF63BFDCE_H
#ifndef U3CTASKU3ED__9_T2CED0FE067EBE3426586277B00A03E2C506FEC94_H
#define U3CTASKU3ED__9_T2CED0FE067EBE3426586277B00A03E2C506FEC94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.ParallelTask_<Task>d__9
struct  U3CTaskU3Ed__9_t2CED0FE067EBE3426586277B00A03E2C506FEC94  : public RuntimeObject
{
public:
	// System.Int32 Tayr.ParallelTask_<Task>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.ParallelTask_<Task>d__9::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.ParallelTask Tayr.ParallelTask_<Task>d__9::<>4__this
	ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__9_t2CED0FE067EBE3426586277B00A03E2C506FEC94, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__9_t2CED0FE067EBE3426586277B00A03E2C506FEC94, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__9_t2CED0FE067EBE3426586277B00A03E2C506FEC94, ___U3CU3E4__this_2)); }
	inline ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__9_T2CED0FE067EBE3426586277B00A03E2C506FEC94_H
#ifndef PATCHASSETDATA_TDB6643A7B2F28765976DE6C425B0F4A91B1BB180_H
#define PATCHASSETDATA_TDB6643A7B2F28765976DE6C425B0F4A91B1BB180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.PatchAssetData
struct  PatchAssetData_tDB6643A7B2F28765976DE6C425B0F4A91B1BB180  : public RuntimeObject
{
public:
	// System.String Tayr.PatchAssetData::Hash
	String_t* ___Hash_0;
	// System.Int64 Tayr.PatchAssetData::Size
	int64_t ___Size_1;
	// System.String Tayr.PatchAssetData::Path
	String_t* ___Path_2;

public:
	inline static int32_t get_offset_of_Hash_0() { return static_cast<int32_t>(offsetof(PatchAssetData_tDB6643A7B2F28765976DE6C425B0F4A91B1BB180, ___Hash_0)); }
	inline String_t* get_Hash_0() const { return ___Hash_0; }
	inline String_t** get_address_of_Hash_0() { return &___Hash_0; }
	inline void set_Hash_0(String_t* value)
	{
		___Hash_0 = value;
		Il2CppCodeGenWriteBarrier((&___Hash_0), value);
	}

	inline static int32_t get_offset_of_Size_1() { return static_cast<int32_t>(offsetof(PatchAssetData_tDB6643A7B2F28765976DE6C425B0F4A91B1BB180, ___Size_1)); }
	inline int64_t get_Size_1() const { return ___Size_1; }
	inline int64_t* get_address_of_Size_1() { return &___Size_1; }
	inline void set_Size_1(int64_t value)
	{
		___Size_1 = value;
	}

	inline static int32_t get_offset_of_Path_2() { return static_cast<int32_t>(offsetof(PatchAssetData_tDB6643A7B2F28765976DE6C425B0F4A91B1BB180, ___Path_2)); }
	inline String_t* get_Path_2() const { return ___Path_2; }
	inline String_t** get_address_of_Path_2() { return &___Path_2; }
	inline void set_Path_2(String_t* value)
	{
		___Path_2 = value;
		Il2CppCodeGenWriteBarrier((&___Path_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATCHASSETDATA_TDB6643A7B2F28765976DE6C425B0F4A91B1BB180_H
#ifndef U3CTASKU3ED__4_TE2B745D070540DDCC7B10373F94D421131774EF0_H
#define U3CTASKU3ED__4_TE2B745D070540DDCC7B10373F94D421131774EF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.PatchingTask_<Task>d__4
struct  U3CTaskU3Ed__4_tE2B745D070540DDCC7B10373F94D421131774EF0  : public RuntimeObject
{
public:
	// System.Int32 Tayr.PatchingTask_<Task>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.PatchingTask_<Task>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.PatchingTask Tayr.PatchingTask_<Task>d__4::<>4__this
	PatchingTask_tAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__4_tE2B745D070540DDCC7B10373F94D421131774EF0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__4_tE2B745D070540DDCC7B10373F94D421131774EF0, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__4_tE2B745D070540DDCC7B10373F94D421131774EF0, ___U3CU3E4__this_2)); }
	inline PatchingTask_tAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PatchingTask_tAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PatchingTask_tAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__4_TE2B745D070540DDCC7B10373F94D421131774EF0_H
#ifndef PATCHINGUTILS_T174B783D1A666D540042010C3848D704DBC1E8DF_H
#define PATCHINGUTILS_T174B783D1A666D540042010C3848D704DBC1E8DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.PatchingUtils
struct  PatchingUtils_t174B783D1A666D540042010C3848D704DBC1E8DF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATCHINGUTILS_T174B783D1A666D540042010C3848D704DBC1E8DF_H
#ifndef RESOURCELIBRARY_T9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54_H
#define RESOURCELIBRARY_T9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.ResourceLibrary
struct  ResourceLibrary_t9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Tayr.ResourceLibrary::_resourcesDictionary
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ____resourcesDictionary_0;
	// Tayr.GameObjectPool Tayr.ResourceLibrary::_poolManager
	GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * ____poolManager_1;

public:
	inline static int32_t get_offset_of__resourcesDictionary_0() { return static_cast<int32_t>(offsetof(ResourceLibrary_t9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54, ____resourcesDictionary_0)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get__resourcesDictionary_0() const { return ____resourcesDictionary_0; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of__resourcesDictionary_0() { return &____resourcesDictionary_0; }
	inline void set__resourcesDictionary_0(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		____resourcesDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&____resourcesDictionary_0), value);
	}

	inline static int32_t get_offset_of__poolManager_1() { return static_cast<int32_t>(offsetof(ResourceLibrary_t9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54, ____poolManager_1)); }
	inline GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * get__poolManager_1() const { return ____poolManager_1; }
	inline GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 ** get_address_of__poolManager_1() { return &____poolManager_1; }
	inline void set__poolManager_1(GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * value)
	{
		____poolManager_1 = value;
		Il2CppCodeGenWriteBarrier((&____poolManager_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCELIBRARY_T9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54_H
#ifndef SCENELIBRARY_TD4A336537A5F417E9F29493F769735C1EDFBCFDF_H
#define SCENELIBRARY_TD4A336537A5F417E9F29493F769735C1EDFBCFDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.SceneLibrary
struct  SceneLibrary_tD4A336537A5F417E9F29493F769735C1EDFBCFDF  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Tayr.SceneLibrary::_sceneDictionary
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ____sceneDictionary_0;

public:
	inline static int32_t get_offset_of__sceneDictionary_0() { return static_cast<int32_t>(offsetof(SceneLibrary_tD4A336537A5F417E9F29493F769735C1EDFBCFDF, ____sceneDictionary_0)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get__sceneDictionary_0() const { return ____sceneDictionary_0; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of__sceneDictionary_0() { return &____sceneDictionary_0; }
	inline void set__sceneDictionary_0(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		____sceneDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&____sceneDictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENELIBRARY_TD4A336537A5F417E9F29493F769735C1EDFBCFDF_H
#ifndef U3CTASKU3ED__6_T4A3DA8D7E927EFFF4F0F1F28094A1D78CAD24ADF_H
#define U3CTASKU3ED__6_T4A3DA8D7E927EFFF4F0F1F28094A1D78CAD24ADF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.SerialTask_<Task>d__6
struct  U3CTaskU3Ed__6_t4A3DA8D7E927EFFF4F0F1F28094A1D78CAD24ADF  : public RuntimeObject
{
public:
	// System.Int32 Tayr.SerialTask_<Task>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.SerialTask_<Task>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.SerialTask Tayr.SerialTask_<Task>d__6::<>4__this
	SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__6_t4A3DA8D7E927EFFF4F0F1F28094A1D78CAD24ADF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__6_t4A3DA8D7E927EFFF4F0F1F28094A1D78CAD24ADF, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__6_t4A3DA8D7E927EFFF4F0F1F28094A1D78CAD24ADF, ___U3CU3E4__this_2)); }
	inline SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__6_T4A3DA8D7E927EFFF4F0F1F28094A1D78CAD24ADF_H
#ifndef U3CTASKU3ED__2_TD77D3A24C3EF9C9E011921FB03E4D184C2784B46_H
#define U3CTASKU3ED__2_TD77D3A24C3EF9C9E011921FB03E4D184C2784B46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.ServerFileLoaderTask_<Task>d__2
struct  U3CTaskU3Ed__2_tD77D3A24C3EF9C9E011921FB03E4D184C2784B46  : public RuntimeObject
{
public:
	// System.Int32 Tayr.ServerFileLoaderTask_<Task>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.ServerFileLoaderTask_<Task>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.ServerFileLoaderTask Tayr.ServerFileLoaderTask_<Task>d__2::<>4__this
	ServerFileLoaderTask_t95314098EBA8E90891F1D8C5F19C0DC49D532F92 * ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest Tayr.ServerFileLoaderTask_<Task>d__2::<www>5__2
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwwwU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tD77D3A24C3EF9C9E011921FB03E4D184C2784B46, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tD77D3A24C3EF9C9E011921FB03E4D184C2784B46, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tD77D3A24C3EF9C9E011921FB03E4D184C2784B46, ___U3CU3E4__this_2)); }
	inline ServerFileLoaderTask_t95314098EBA8E90891F1D8C5F19C0DC49D532F92 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ServerFileLoaderTask_t95314098EBA8E90891F1D8C5F19C0DC49D532F92 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ServerFileLoaderTask_t95314098EBA8E90891F1D8C5F19C0DC49D532F92 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_tD77D3A24C3EF9C9E011921FB03E4D184C2784B46, ___U3CwwwU3E5__2_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwwwU3E5__2_3() const { return ___U3CwwwU3E5__2_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwwwU3E5__2_3() { return &___U3CwwwU3E5__2_3; }
	inline void set_U3CwwwU3E5__2_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwwwU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__2_TD77D3A24C3EF9C9E011921FB03E4D184C2784B46_H
#ifndef U3CTASKU3ED__2_T996C5019828C38A607088ED0DBCDC342B7B3688C_H
#define U3CTASKU3ED__2_T996C5019828C38A607088ED0DBCDC342B7B3688C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.StreamingAssetFileLoaderTask_<Task>d__2
struct  U3CTaskU3Ed__2_t996C5019828C38A607088ED0DBCDC342B7B3688C  : public RuntimeObject
{
public:
	// System.Int32 Tayr.StreamingAssetFileLoaderTask_<Task>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.StreamingAssetFileLoaderTask_<Task>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.StreamingAssetFileLoaderTask Tayr.StreamingAssetFileLoaderTask_<Task>d__2::<>4__this
	StreamingAssetFileLoaderTask_t766339DE3AEAB244DF39F9C963074DCCAEA2BA43 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t996C5019828C38A607088ED0DBCDC342B7B3688C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t996C5019828C38A607088ED0DBCDC342B7B3688C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t996C5019828C38A607088ED0DBCDC342B7B3688C, ___U3CU3E4__this_2)); }
	inline StreamingAssetFileLoaderTask_t766339DE3AEAB244DF39F9C963074DCCAEA2BA43 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline StreamingAssetFileLoaderTask_t766339DE3AEAB244DF39F9C963074DCCAEA2BA43 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(StreamingAssetFileLoaderTask_t766339DE3AEAB244DF39F9C963074DCCAEA2BA43 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__2_T996C5019828C38A607088ED0DBCDC342B7B3688C_H
#ifndef TALERT_T04CBB10626DE07CD76206287C32FA769FB757063_H
#define TALERT_T04CBB10626DE07CD76206287C32FA769FB757063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TAlert
struct  TAlert_t04CBB10626DE07CD76206287C32FA769FB757063  : public RuntimeObject
{
public:

public:
};

struct TAlert_t04CBB10626DE07CD76206287C32FA769FB757063_StaticFields
{
public:
	// Tayr.RetryNode Tayr.TAlert::_retryNode
	RetryNode_tA2D5659E521764F41C793319C5DD5D9E8F3EE04C * ____retryNode_0;
	// System.Type Tayr.TAlert::_nodeType
	Type_t * ____nodeType_1;
	// Tayr.ILibrary Tayr.TAlert::_library
	RuntimeObject* ____library_2;
	// System.Object Tayr.TAlert::_data
	RuntimeObject * ____data_3;

public:
	inline static int32_t get_offset_of__retryNode_0() { return static_cast<int32_t>(offsetof(TAlert_t04CBB10626DE07CD76206287C32FA769FB757063_StaticFields, ____retryNode_0)); }
	inline RetryNode_tA2D5659E521764F41C793319C5DD5D9E8F3EE04C * get__retryNode_0() const { return ____retryNode_0; }
	inline RetryNode_tA2D5659E521764F41C793319C5DD5D9E8F3EE04C ** get_address_of__retryNode_0() { return &____retryNode_0; }
	inline void set__retryNode_0(RetryNode_tA2D5659E521764F41C793319C5DD5D9E8F3EE04C * value)
	{
		____retryNode_0 = value;
		Il2CppCodeGenWriteBarrier((&____retryNode_0), value);
	}

	inline static int32_t get_offset_of__nodeType_1() { return static_cast<int32_t>(offsetof(TAlert_t04CBB10626DE07CD76206287C32FA769FB757063_StaticFields, ____nodeType_1)); }
	inline Type_t * get__nodeType_1() const { return ____nodeType_1; }
	inline Type_t ** get_address_of__nodeType_1() { return &____nodeType_1; }
	inline void set__nodeType_1(Type_t * value)
	{
		____nodeType_1 = value;
		Il2CppCodeGenWriteBarrier((&____nodeType_1), value);
	}

	inline static int32_t get_offset_of__library_2() { return static_cast<int32_t>(offsetof(TAlert_t04CBB10626DE07CD76206287C32FA769FB757063_StaticFields, ____library_2)); }
	inline RuntimeObject* get__library_2() const { return ____library_2; }
	inline RuntimeObject** get_address_of__library_2() { return &____library_2; }
	inline void set__library_2(RuntimeObject* value)
	{
		____library_2 = value;
		Il2CppCodeGenWriteBarrier((&____library_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(TAlert_t04CBB10626DE07CD76206287C32FA769FB757063_StaticFields, ____data_3)); }
	inline RuntimeObject * get__data_3() const { return ____data_3; }
	inline RuntimeObject ** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject * value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TALERT_T04CBB10626DE07CD76206287C32FA769FB757063_H
#ifndef TDYNAMICLISTDATA_T634C7C07588B155397EB9DB5A3825AC657E12369_H
#define TDYNAMICLISTDATA_T634C7C07588B155397EB9DB5A3825AC657E12369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TDynamicListData
struct  TDynamicListData_t634C7C07588B155397EB9DB5A3825AC657E12369  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Tayr.TDynamicListData::Prefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Prefab_0;

public:
	inline static int32_t get_offset_of_Prefab_0() { return static_cast<int32_t>(offsetof(TDynamicListData_t634C7C07588B155397EB9DB5A3825AC657E12369, ___Prefab_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Prefab_0() const { return ___Prefab_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Prefab_0() { return &___Prefab_0; }
	inline void set_Prefab_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Prefab_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TDYNAMICLISTDATA_T634C7C07588B155397EB9DB5A3825AC657E12369_H
#ifndef TLOCALIZATIONUTILS_T520027CB2848DEA38DB70D0DEDB4CDC9F8BC64FC_H
#define TLOCALIZATIONUTILS_T520027CB2848DEA38DB70D0DEDB4CDC9F8BC64FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TLocalizationUtils
struct  TLocalizationUtils_t520027CB2848DEA38DB70D0DEDB4CDC9F8BC64FC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLOCALIZATIONUTILS_T520027CB2848DEA38DB70D0DEDB4CDC9F8BC64FC_H
#ifndef TPATHRESOLVER_TFB0A83A4198D6F6D4DB90CE21D1ECA7212882796_H
#define TPATHRESOLVER_TFB0A83A4198D6F6D4DB90CE21D1ECA7212882796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TPathResolver
struct  TPathResolver_tFB0A83A4198D6F6D4DB90CE21D1ECA7212882796  : public RuntimeObject
{
public:

public:
};

struct TPathResolver_tFB0A83A4198D6F6D4DB90CE21D1ECA7212882796_StaticFields
{
public:
	// System.String Tayr.TPathResolver::Server
	String_t* ___Server_0;
	// System.String Tayr.TPathResolver::Depot
	String_t* ___Depot_1;
	// System.String Tayr.TPathResolver::LocalizationPath
	String_t* ___LocalizationPath_2;

public:
	inline static int32_t get_offset_of_Server_0() { return static_cast<int32_t>(offsetof(TPathResolver_tFB0A83A4198D6F6D4DB90CE21D1ECA7212882796_StaticFields, ___Server_0)); }
	inline String_t* get_Server_0() const { return ___Server_0; }
	inline String_t** get_address_of_Server_0() { return &___Server_0; }
	inline void set_Server_0(String_t* value)
	{
		___Server_0 = value;
		Il2CppCodeGenWriteBarrier((&___Server_0), value);
	}

	inline static int32_t get_offset_of_Depot_1() { return static_cast<int32_t>(offsetof(TPathResolver_tFB0A83A4198D6F6D4DB90CE21D1ECA7212882796_StaticFields, ___Depot_1)); }
	inline String_t* get_Depot_1() const { return ___Depot_1; }
	inline String_t** get_address_of_Depot_1() { return &___Depot_1; }
	inline void set_Depot_1(String_t* value)
	{
		___Depot_1 = value;
		Il2CppCodeGenWriteBarrier((&___Depot_1), value);
	}

	inline static int32_t get_offset_of_LocalizationPath_2() { return static_cast<int32_t>(offsetof(TPathResolver_tFB0A83A4198D6F6D4DB90CE21D1ECA7212882796_StaticFields, ___LocalizationPath_2)); }
	inline String_t* get_LocalizationPath_2() const { return ___LocalizationPath_2; }
	inline String_t** get_address_of_LocalizationPath_2() { return &___LocalizationPath_2; }
	inline void set_LocalizationPath_2(String_t* value)
	{
		___LocalizationPath_2 = value;
		Il2CppCodeGenWriteBarrier((&___LocalizationPath_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TPATHRESOLVER_TFB0A83A4198D6F6D4DB90CE21D1ECA7212882796_H
#ifndef U3CFADEMUSICU3ED__13_T386F09790D7AEC7D217DD9CE193C9FFDD02757FB_H
#define U3CFADEMUSICU3ED__13_T386F09790D7AEC7D217DD9CE193C9FFDD02757FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TSoundSystem_<FadeMusic>d__13
struct  U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB  : public RuntimeObject
{
public:
	// System.Int32 Tayr.TSoundSystem_<FadeMusic>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.TSoundSystem_<FadeMusic>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.TSoundSystem Tayr.TSoundSystem_<FadeMusic>d__13::<>4__this
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ___U3CU3E4__this_2;
	// System.Single Tayr.TSoundSystem_<FadeMusic>d__13::duration
	float ___duration_3;
	// UnityEngine.AudioClip Tayr.TSoundSystem_<FadeMusic>d__13::clip
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___clip_4;
	// System.Single Tayr.TSoundSystem_<FadeMusic>d__13::<currentTime>5__2
	float ___U3CcurrentTimeU3E5__2_5;
	// System.Single Tayr.TSoundSystem_<FadeMusic>d__13::<start>5__3
	float ___U3CstartU3E5__3_6;
	// System.Single Tayr.TSoundSystem_<FadeMusic>d__13::<d>5__4
	float ___U3CdU3E5__4_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB, ___U3CU3E4__this_2)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_clip_4() { return static_cast<int32_t>(offsetof(U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB, ___clip_4)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_clip_4() const { return ___clip_4; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_clip_4() { return &___clip_4; }
	inline void set_clip_4(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___clip_4 = value;
		Il2CppCodeGenWriteBarrier((&___clip_4), value);
	}

	inline static int32_t get_offset_of_U3CcurrentTimeU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB, ___U3CcurrentTimeU3E5__2_5)); }
	inline float get_U3CcurrentTimeU3E5__2_5() const { return ___U3CcurrentTimeU3E5__2_5; }
	inline float* get_address_of_U3CcurrentTimeU3E5__2_5() { return &___U3CcurrentTimeU3E5__2_5; }
	inline void set_U3CcurrentTimeU3E5__2_5(float value)
	{
		___U3CcurrentTimeU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CstartU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB, ___U3CstartU3E5__3_6)); }
	inline float get_U3CstartU3E5__3_6() const { return ___U3CstartU3E5__3_6; }
	inline float* get_address_of_U3CstartU3E5__3_6() { return &___U3CstartU3E5__3_6; }
	inline void set_U3CstartU3E5__3_6(float value)
	{
		___U3CstartU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CdU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB, ___U3CdU3E5__4_7)); }
	inline float get_U3CdU3E5__4_7() const { return ___U3CdU3E5__4_7; }
	inline float* get_address_of_U3CdU3E5__4_7() { return &___U3CdU3E5__4_7; }
	inline void set_U3CdU3E5__4_7(float value)
	{
		___U3CdU3E5__4_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEMUSICU3ED__13_T386F09790D7AEC7D217DD9CE193C9FFDD02757FB_H
#ifndef U3CSTARTFADEMUSICU3ED__12_TAC22FF2F55608187E484F4902347D30ACD206251_H
#define U3CSTARTFADEMUSICU3ED__12_TAC22FF2F55608187E484F4902347D30ACD206251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TSoundSystem_<StartFadeMusic>d__12
struct  U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251  : public RuntimeObject
{
public:
	// System.Int32 Tayr.TSoundSystem_<StartFadeMusic>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Tayr.TSoundSystem_<StartFadeMusic>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Tayr.TSoundSystem Tayr.TSoundSystem_<StartFadeMusic>d__12::<>4__this
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ___U3CU3E4__this_2;
	// System.Single Tayr.TSoundSystem_<StartFadeMusic>d__12::targetVolume
	float ___targetVolume_3;
	// System.Single Tayr.TSoundSystem_<StartFadeMusic>d__12::duration
	float ___duration_4;
	// System.Single Tayr.TSoundSystem_<StartFadeMusic>d__12::<currentTime>5__2
	float ___U3CcurrentTimeU3E5__2_5;
	// System.Single Tayr.TSoundSystem_<StartFadeMusic>d__12::<start>5__3
	float ___U3CstartU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251, ___U3CU3E4__this_2)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_targetVolume_3() { return static_cast<int32_t>(offsetof(U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251, ___targetVolume_3)); }
	inline float get_targetVolume_3() const { return ___targetVolume_3; }
	inline float* get_address_of_targetVolume_3() { return &___targetVolume_3; }
	inline void set_targetVolume_3(float value)
	{
		___targetVolume_3 = value;
	}

	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentTimeU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251, ___U3CcurrentTimeU3E5__2_5)); }
	inline float get_U3CcurrentTimeU3E5__2_5() const { return ___U3CcurrentTimeU3E5__2_5; }
	inline float* get_address_of_U3CcurrentTimeU3E5__2_5() { return &___U3CcurrentTimeU3E5__2_5; }
	inline void set_U3CcurrentTimeU3E5__2_5(float value)
	{
		___U3CcurrentTimeU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CstartU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251, ___U3CstartU3E5__3_6)); }
	inline float get_U3CstartU3E5__3_6() const { return ___U3CstartU3E5__3_6; }
	inline float* get_address_of_U3CstartU3E5__3_6() { return &___U3CstartU3E5__3_6; }
	inline void set_U3CstartU3E5__3_6(float value)
	{
		___U3CstartU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTFADEMUSICU3ED__12_TAC22FF2F55608187E484F4902347D30ACD206251_H
#ifndef TWAITFOREVENT_T48111D8D7D335240707B5CB2F1EA686871299A05_H
#define TWAITFOREVENT_T48111D8D7D335240707B5CB2F1EA686871299A05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TWaitForEvent
struct  TWaitForEvent_t48111D8D7D335240707B5CB2F1EA686871299A05  : public RuntimeObject
{
public:
	// System.Boolean Tayr.TWaitForEvent::_isInvoked
	bool ____isInvoked_0;

public:
	inline static int32_t get_offset_of__isInvoked_0() { return static_cast<int32_t>(offsetof(TWaitForEvent_t48111D8D7D335240707B5CB2F1EA686871299A05, ____isInvoked_0)); }
	inline bool get__isInvoked_0() const { return ____isInvoked_0; }
	inline bool* get_address_of__isInvoked_0() { return &____isInvoked_0; }
	inline void set__isInvoked_0(bool value)
	{
		____isInvoked_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWAITFOREVENT_T48111D8D7D335240707B5CB2F1EA686871299A05_H
#ifndef TAYRINITIALIZER_TF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_H
#define TAYRINITIALIZER_TF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TayrInitializer
struct  TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659  : public RuntimeObject
{
public:

public:
};

struct TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields
{
public:
	// Tayr.JsonConfigurator Tayr.TayrInitializer::JsonConfigurator
	JsonConfigurator_t4300FF6941F6A5F99CB53A04D924CFEAC2DD5B91 * ___JsonConfigurator_0;
	// Tayr.TLocalizationSystem Tayr.TayrInitializer::TLocalizationSystem
	TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B * ___TLocalizationSystem_1;
	// Tayr.MappingInitializer Tayr.TayrInitializer::MappingInitilizer
	MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A * ___MappingInitilizer_2;
	// Tayr.ResourceLibrary Tayr.TayrInitializer::ResourceLibrary
	ResourceLibrary_t9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54 * ___ResourceLibrary_3;
	// Tayr.SceneLibrary Tayr.TayrInitializer::SceneLibrary
	SceneLibrary_tD4A336537A5F417E9F29493F769735C1EDFBCFDF * ___SceneLibrary_4;
	// Tayr.GameObjectPool Tayr.TayrInitializer::PoolManager
	GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * ___PoolManager_5;
	// Tayr.NodeSystem Tayr.TayrInitializer::NodeSystem
	NodeSystem_t28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE * ___NodeSystem_6;
	// Tayr.TSystemsManager Tayr.TayrInitializer::TSystemManager
	TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * ___TSystemManager_7;
	// Tayr.TUpdaterSystem Tayr.TayrInitializer::TUpdaterSystem
	TUpdaterSystem_t69A618B692764BD9B69DD0829A3D8DDFEC7AFA2E * ___TUpdaterSystem_8;
	// System.Collections.Generic.Dictionary`2<System.String,Tayr.TPrefabsConf> Tayr.TayrInitializer::ConfKeys
	Dictionary_2_t83BA0CF1FD9248B190489DADB47FBF726F846EA3 * ___ConfKeys_9;

public:
	inline static int32_t get_offset_of_JsonConfigurator_0() { return static_cast<int32_t>(offsetof(TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields, ___JsonConfigurator_0)); }
	inline JsonConfigurator_t4300FF6941F6A5F99CB53A04D924CFEAC2DD5B91 * get_JsonConfigurator_0() const { return ___JsonConfigurator_0; }
	inline JsonConfigurator_t4300FF6941F6A5F99CB53A04D924CFEAC2DD5B91 ** get_address_of_JsonConfigurator_0() { return &___JsonConfigurator_0; }
	inline void set_JsonConfigurator_0(JsonConfigurator_t4300FF6941F6A5F99CB53A04D924CFEAC2DD5B91 * value)
	{
		___JsonConfigurator_0 = value;
		Il2CppCodeGenWriteBarrier((&___JsonConfigurator_0), value);
	}

	inline static int32_t get_offset_of_TLocalizationSystem_1() { return static_cast<int32_t>(offsetof(TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields, ___TLocalizationSystem_1)); }
	inline TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B * get_TLocalizationSystem_1() const { return ___TLocalizationSystem_1; }
	inline TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B ** get_address_of_TLocalizationSystem_1() { return &___TLocalizationSystem_1; }
	inline void set_TLocalizationSystem_1(TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B * value)
	{
		___TLocalizationSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___TLocalizationSystem_1), value);
	}

	inline static int32_t get_offset_of_MappingInitilizer_2() { return static_cast<int32_t>(offsetof(TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields, ___MappingInitilizer_2)); }
	inline MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A * get_MappingInitilizer_2() const { return ___MappingInitilizer_2; }
	inline MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A ** get_address_of_MappingInitilizer_2() { return &___MappingInitilizer_2; }
	inline void set_MappingInitilizer_2(MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A * value)
	{
		___MappingInitilizer_2 = value;
		Il2CppCodeGenWriteBarrier((&___MappingInitilizer_2), value);
	}

	inline static int32_t get_offset_of_ResourceLibrary_3() { return static_cast<int32_t>(offsetof(TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields, ___ResourceLibrary_3)); }
	inline ResourceLibrary_t9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54 * get_ResourceLibrary_3() const { return ___ResourceLibrary_3; }
	inline ResourceLibrary_t9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54 ** get_address_of_ResourceLibrary_3() { return &___ResourceLibrary_3; }
	inline void set_ResourceLibrary_3(ResourceLibrary_t9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54 * value)
	{
		___ResourceLibrary_3 = value;
		Il2CppCodeGenWriteBarrier((&___ResourceLibrary_3), value);
	}

	inline static int32_t get_offset_of_SceneLibrary_4() { return static_cast<int32_t>(offsetof(TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields, ___SceneLibrary_4)); }
	inline SceneLibrary_tD4A336537A5F417E9F29493F769735C1EDFBCFDF * get_SceneLibrary_4() const { return ___SceneLibrary_4; }
	inline SceneLibrary_tD4A336537A5F417E9F29493F769735C1EDFBCFDF ** get_address_of_SceneLibrary_4() { return &___SceneLibrary_4; }
	inline void set_SceneLibrary_4(SceneLibrary_tD4A336537A5F417E9F29493F769735C1EDFBCFDF * value)
	{
		___SceneLibrary_4 = value;
		Il2CppCodeGenWriteBarrier((&___SceneLibrary_4), value);
	}

	inline static int32_t get_offset_of_PoolManager_5() { return static_cast<int32_t>(offsetof(TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields, ___PoolManager_5)); }
	inline GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * get_PoolManager_5() const { return ___PoolManager_5; }
	inline GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 ** get_address_of_PoolManager_5() { return &___PoolManager_5; }
	inline void set_PoolManager_5(GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * value)
	{
		___PoolManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___PoolManager_5), value);
	}

	inline static int32_t get_offset_of_NodeSystem_6() { return static_cast<int32_t>(offsetof(TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields, ___NodeSystem_6)); }
	inline NodeSystem_t28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE * get_NodeSystem_6() const { return ___NodeSystem_6; }
	inline NodeSystem_t28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE ** get_address_of_NodeSystem_6() { return &___NodeSystem_6; }
	inline void set_NodeSystem_6(NodeSystem_t28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE * value)
	{
		___NodeSystem_6 = value;
		Il2CppCodeGenWriteBarrier((&___NodeSystem_6), value);
	}

	inline static int32_t get_offset_of_TSystemManager_7() { return static_cast<int32_t>(offsetof(TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields, ___TSystemManager_7)); }
	inline TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * get_TSystemManager_7() const { return ___TSystemManager_7; }
	inline TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 ** get_address_of_TSystemManager_7() { return &___TSystemManager_7; }
	inline void set_TSystemManager_7(TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * value)
	{
		___TSystemManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___TSystemManager_7), value);
	}

	inline static int32_t get_offset_of_TUpdaterSystem_8() { return static_cast<int32_t>(offsetof(TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields, ___TUpdaterSystem_8)); }
	inline TUpdaterSystem_t69A618B692764BD9B69DD0829A3D8DDFEC7AFA2E * get_TUpdaterSystem_8() const { return ___TUpdaterSystem_8; }
	inline TUpdaterSystem_t69A618B692764BD9B69DD0829A3D8DDFEC7AFA2E ** get_address_of_TUpdaterSystem_8() { return &___TUpdaterSystem_8; }
	inline void set_TUpdaterSystem_8(TUpdaterSystem_t69A618B692764BD9B69DD0829A3D8DDFEC7AFA2E * value)
	{
		___TUpdaterSystem_8 = value;
		Il2CppCodeGenWriteBarrier((&___TUpdaterSystem_8), value);
	}

	inline static int32_t get_offset_of_ConfKeys_9() { return static_cast<int32_t>(offsetof(TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields, ___ConfKeys_9)); }
	inline Dictionary_2_t83BA0CF1FD9248B190489DADB47FBF726F846EA3 * get_ConfKeys_9() const { return ___ConfKeys_9; }
	inline Dictionary_2_t83BA0CF1FD9248B190489DADB47FBF726F846EA3 ** get_address_of_ConfKeys_9() { return &___ConfKeys_9; }
	inline void set_ConfKeys_9(Dictionary_2_t83BA0CF1FD9248B190489DADB47FBF726F846EA3 * value)
	{
		___ConfKeys_9 = value;
		Il2CppCodeGenWriteBarrier((&___ConfKeys_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAYRINITIALIZER_TF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_H
#ifndef TRANSFORMEXTENSION_TD01D1E420ACCDB0E3AB58F7852434B8A0061CF81_H
#define TRANSFORMEXTENSION_TD01D1E420ACCDB0E3AB58F7852434B8A0061CF81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TransformExtension
struct  TransformExtension_tD01D1E420ACCDB0E3AB58F7852434B8A0061CF81  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMEXTENSION_TD01D1E420ACCDB0E3AB58F7852434B8A0061CF81_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ASSETBUNDLELOADERTASK_T285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD_H
#define ASSETBUNDLELOADERTASK_T285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.AssetBundleLoaderTask
struct  AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD  : public BasicTask_1_t7F49147173E4A1AC129308B42CDD067972B5B75A
{
public:
	// System.String Tayr.AssetBundleLoaderTask::_path
	String_t* ____path_7;
	// Zenject.DiContainer Tayr.AssetBundleLoaderTask::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_8;
	// System.String Tayr.AssetBundleLoaderTask::_injectTag
	String_t* ____injectTag_9;

public:
	inline static int32_t get_offset_of__path_7() { return static_cast<int32_t>(offsetof(AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD, ____path_7)); }
	inline String_t* get__path_7() const { return ____path_7; }
	inline String_t** get_address_of__path_7() { return &____path_7; }
	inline void set__path_7(String_t* value)
	{
		____path_7 = value;
		Il2CppCodeGenWriteBarrier((&____path_7), value);
	}

	inline static int32_t get_offset_of__diContainer_8() { return static_cast<int32_t>(offsetof(AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD, ____diContainer_8)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_8() const { return ____diContainer_8; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_8() { return &____diContainer_8; }
	inline void set__diContainer_8(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_8 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_8), value);
	}

	inline static int32_t get_offset_of__injectTag_9() { return static_cast<int32_t>(offsetof(AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD, ____injectTag_9)); }
	inline String_t* get__injectTag_9() const { return ____injectTag_9; }
	inline String_t** get_address_of__injectTag_9() { return &____injectTag_9; }
	inline void set__injectTag_9(String_t* value)
	{
		____injectTag_9 = value;
		Il2CppCodeGenWriteBarrier((&____injectTag_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLELOADERTASK_T285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD_H
#ifndef DEPOTFILELOADERTASK_T8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32_H
#define DEPOTFILELOADERTASK_T8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.DepotFileLoaderTask
struct  DepotFileLoaderTask_t8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32  : public BasicTask_1_t2103EEFF091DD4A943C23C8C77591716114AFEF3
{
public:
	// System.String Tayr.DepotFileLoaderTask::_path
	String_t* ____path_7;

public:
	inline static int32_t get_offset_of__path_7() { return static_cast<int32_t>(offsetof(DepotFileLoaderTask_t8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32, ____path_7)); }
	inline String_t* get__path_7() const { return ____path_7; }
	inline String_t** get_address_of__path_7() { return &____path_7; }
	inline void set__path_7(String_t* value)
	{
		____path_7 = value;
		Il2CppCodeGenWriteBarrier((&____path_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPOTFILELOADERTASK_T8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32_H
#ifndef DOWNLOADFILETASK_T10719E6DB76BF03F2FAE4428B512AF5C4375104F_H
#define DOWNLOADFILETASK_T10719E6DB76BF03F2FAE4428B512AF5C4375104F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.DownloadFileTask
struct  DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F  : public BasicTask_1_tE70B7733E99882370633F0BDBE9D6208EB03B77E
{
public:
	// System.String Tayr.DownloadFileTask::_url
	String_t* ____url_7;
	// System.String Tayr.DownloadFileTask::_destination
	String_t* ____destination_8;

public:
	inline static int32_t get_offset_of__url_7() { return static_cast<int32_t>(offsetof(DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F, ____url_7)); }
	inline String_t* get__url_7() const { return ____url_7; }
	inline String_t** get_address_of__url_7() { return &____url_7; }
	inline void set__url_7(String_t* value)
	{
		____url_7 = value;
		Il2CppCodeGenWriteBarrier((&____url_7), value);
	}

	inline static int32_t get_offset_of__destination_8() { return static_cast<int32_t>(offsetof(DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F, ____destination_8)); }
	inline String_t* get__destination_8() const { return ____destination_8; }
	inline String_t** get_address_of__destination_8() { return &____destination_8; }
	inline void set__destination_8(String_t* value)
	{
		____destination_8 = value;
		Il2CppCodeGenWriteBarrier((&____destination_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADFILETASK_T10719E6DB76BF03F2FAE4428B512AF5C4375104F_H
#ifndef LOADHASHFILETASK_T7696E228DD71017700D9DE906D61BA9676A81552_H
#define LOADHASHFILETASK_T7696E228DD71017700D9DE906D61BA9676A81552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.LoadHashFileTask
struct  LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552  : public BasicTask_1_t061E76CF1C2670C446FFAC160877B71F63E09121
{
public:
	// Tayr.MissingAssetsGroup Tayr.LoadHashFileTask::_missingContent
	MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1 * ____missingContent_7;
	// System.String Tayr.LoadHashFileTask::_destination
	String_t* ____destination_8;
	// System.String Tayr.LoadHashFileTask::_url
	String_t* ____url_9;
	// System.Collections.Generic.List`1<System.String> Tayr.LoadHashFileTask::_availableContent
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____availableContent_10;

public:
	inline static int32_t get_offset_of__missingContent_7() { return static_cast<int32_t>(offsetof(LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552, ____missingContent_7)); }
	inline MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1 * get__missingContent_7() const { return ____missingContent_7; }
	inline MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1 ** get_address_of__missingContent_7() { return &____missingContent_7; }
	inline void set__missingContent_7(MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1 * value)
	{
		____missingContent_7 = value;
		Il2CppCodeGenWriteBarrier((&____missingContent_7), value);
	}

	inline static int32_t get_offset_of__destination_8() { return static_cast<int32_t>(offsetof(LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552, ____destination_8)); }
	inline String_t* get__destination_8() const { return ____destination_8; }
	inline String_t** get_address_of__destination_8() { return &____destination_8; }
	inline void set__destination_8(String_t* value)
	{
		____destination_8 = value;
		Il2CppCodeGenWriteBarrier((&____destination_8), value);
	}

	inline static int32_t get_offset_of__url_9() { return static_cast<int32_t>(offsetof(LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552, ____url_9)); }
	inline String_t* get__url_9() const { return ____url_9; }
	inline String_t** get_address_of__url_9() { return &____url_9; }
	inline void set__url_9(String_t* value)
	{
		____url_9 = value;
		Il2CppCodeGenWriteBarrier((&____url_9), value);
	}

	inline static int32_t get_offset_of__availableContent_10() { return static_cast<int32_t>(offsetof(LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552, ____availableContent_10)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__availableContent_10() const { return ____availableContent_10; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__availableContent_10() { return &____availableContent_10; }
	inline void set__availableContent_10(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____availableContent_10 = value;
		Il2CppCodeGenWriteBarrier((&____availableContent_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADHASHFILETASK_T7696E228DD71017700D9DE906D61BA9676A81552_H
#ifndef LOCALFILELOADERTASK_T4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD_H
#define LOCALFILELOADERTASK_T4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.LocalFileLoaderTask
struct  LocalFileLoaderTask_t4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD  : public BasicTask_1_t2103EEFF091DD4A943C23C8C77591716114AFEF3
{
public:
	// System.String Tayr.LocalFileLoaderTask::_path
	String_t* ____path_7;

public:
	inline static int32_t get_offset_of__path_7() { return static_cast<int32_t>(offsetof(LocalFileLoaderTask_t4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD, ____path_7)); }
	inline String_t* get__path_7() const { return ____path_7; }
	inline String_t** get_address_of__path_7() { return &____path_7; }
	inline void set__path_7(String_t* value)
	{
		____path_7 = value;
		Il2CppCodeGenWriteBarrier((&____path_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALFILELOADERTASK_T4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD_H
#ifndef MULTILOOKUPLOADERTASK_T59636CCC00F213A387F9851130F5691B44EA7B8A_H
#define MULTILOOKUPLOADERTASK_T59636CCC00F213A387F9851130F5691B44EA7B8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.MultiLookupLoaderTask
struct  MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A  : public BasicTask_1_t2103EEFF091DD4A943C23C8C77591716114AFEF3
{
public:
	// System.String Tayr.MultiLookupLoaderTask::_path
	String_t* ____path_7;

public:
	inline static int32_t get_offset_of__path_7() { return static_cast<int32_t>(offsetof(MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A, ____path_7)); }
	inline String_t* get__path_7() const { return ____path_7; }
	inline String_t** get_address_of__path_7() { return &____path_7; }
	inline void set__path_7(String_t* value)
	{
		____path_7 = value;
		Il2CppCodeGenWriteBarrier((&____path_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTILOOKUPLOADERTASK_T59636CCC00F213A387F9851130F5691B44EA7B8A_H
#ifndef PARALLELTASK_TED959C990B1D6B0E41F2ED292B4AD6AE34939717_H
#define PARALLELTASK_TED959C990B1D6B0E41F2ED292B4AD6AE34939717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.ParallelTask
struct  ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717  : public BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57
{
public:
	// Svelto.Tasks.ParallelTaskCollection Tayr.ParallelTask::_parallelTaskCollection
	ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA * ____parallelTaskCollection_7;
	// Tayr.OnProgress Tayr.ParallelTask::_onProgressEvent
	OnProgress_tADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4 * ____onProgressEvent_8;
	// System.Int32 Tayr.ParallelTask::_childTaskCounter
	int32_t ____childTaskCounter_9;
	// System.Single Tayr.ParallelTask::_childTaskFinished
	float ____childTaskFinished_10;

public:
	inline static int32_t get_offset_of__parallelTaskCollection_7() { return static_cast<int32_t>(offsetof(ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717, ____parallelTaskCollection_7)); }
	inline ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA * get__parallelTaskCollection_7() const { return ____parallelTaskCollection_7; }
	inline ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA ** get_address_of__parallelTaskCollection_7() { return &____parallelTaskCollection_7; }
	inline void set__parallelTaskCollection_7(ParallelTaskCollection_t8DFC5A6DAFD6AFB617F77CF0767758AF060049DA * value)
	{
		____parallelTaskCollection_7 = value;
		Il2CppCodeGenWriteBarrier((&____parallelTaskCollection_7), value);
	}

	inline static int32_t get_offset_of__onProgressEvent_8() { return static_cast<int32_t>(offsetof(ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717, ____onProgressEvent_8)); }
	inline OnProgress_tADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4 * get__onProgressEvent_8() const { return ____onProgressEvent_8; }
	inline OnProgress_tADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4 ** get_address_of__onProgressEvent_8() { return &____onProgressEvent_8; }
	inline void set__onProgressEvent_8(OnProgress_tADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4 * value)
	{
		____onProgressEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&____onProgressEvent_8), value);
	}

	inline static int32_t get_offset_of__childTaskCounter_9() { return static_cast<int32_t>(offsetof(ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717, ____childTaskCounter_9)); }
	inline int32_t get__childTaskCounter_9() const { return ____childTaskCounter_9; }
	inline int32_t* get_address_of__childTaskCounter_9() { return &____childTaskCounter_9; }
	inline void set__childTaskCounter_9(int32_t value)
	{
		____childTaskCounter_9 = value;
	}

	inline static int32_t get_offset_of__childTaskFinished_10() { return static_cast<int32_t>(offsetof(ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717, ____childTaskFinished_10)); }
	inline float get__childTaskFinished_10() const { return ____childTaskFinished_10; }
	inline float* get_address_of__childTaskFinished_10() { return &____childTaskFinished_10; }
	inline void set__childTaskFinished_10(float value)
	{
		____childTaskFinished_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARALLELTASK_TED959C990B1D6B0E41F2ED292B4AD6AE34939717_H
#ifndef PATCHINGTASK_TAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD_H
#define PATCHINGTASK_TAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.PatchingTask
struct  PatchingTask_tAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD  : public BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57
{
public:
	// System.String Tayr.PatchingTask::_patchFile
	String_t* ____patchFile_7;
	// Tayr.ITask Tayr.PatchingTask::_patchingTask
	RuntimeObject* ____patchingTask_8;
	// System.String Tayr.PatchingTask::_serverUrl
	String_t* ____serverUrl_9;

public:
	inline static int32_t get_offset_of__patchFile_7() { return static_cast<int32_t>(offsetof(PatchingTask_tAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD, ____patchFile_7)); }
	inline String_t* get__patchFile_7() const { return ____patchFile_7; }
	inline String_t** get_address_of__patchFile_7() { return &____patchFile_7; }
	inline void set__patchFile_7(String_t* value)
	{
		____patchFile_7 = value;
		Il2CppCodeGenWriteBarrier((&____patchFile_7), value);
	}

	inline static int32_t get_offset_of__patchingTask_8() { return static_cast<int32_t>(offsetof(PatchingTask_tAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD, ____patchingTask_8)); }
	inline RuntimeObject* get__patchingTask_8() const { return ____patchingTask_8; }
	inline RuntimeObject** get_address_of__patchingTask_8() { return &____patchingTask_8; }
	inline void set__patchingTask_8(RuntimeObject* value)
	{
		____patchingTask_8 = value;
		Il2CppCodeGenWriteBarrier((&____patchingTask_8), value);
	}

	inline static int32_t get_offset_of__serverUrl_9() { return static_cast<int32_t>(offsetof(PatchingTask_tAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD, ____serverUrl_9)); }
	inline String_t* get__serverUrl_9() const { return ____serverUrl_9; }
	inline String_t** get_address_of__serverUrl_9() { return &____serverUrl_9; }
	inline void set__serverUrl_9(String_t* value)
	{
		____serverUrl_9 = value;
		Il2CppCodeGenWriteBarrier((&____serverUrl_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATCHINGTASK_TAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD_H
#ifndef SERIALTASK_TF1A918D9502AC12F7FFC785540520BB122C5B04A_H
#define SERIALTASK_TF1A918D9502AC12F7FFC785540520BB122C5B04A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.SerialTask
struct  SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A  : public BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57
{
public:
	// Svelto.Tasks.SerialTaskCollection Tayr.SerialTask::_serialTask
	SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7 * ____serialTask_7;
	// Tayr.OnProgress Tayr.SerialTask::_onProgressEvent
	OnProgress_tADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4 * ____onProgressEvent_8;
	// System.Int32 Tayr.SerialTask::_childTaskCounter
	int32_t ____childTaskCounter_9;
	// System.Single Tayr.SerialTask::_childTaskFinished
	float ____childTaskFinished_10;

public:
	inline static int32_t get_offset_of__serialTask_7() { return static_cast<int32_t>(offsetof(SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A, ____serialTask_7)); }
	inline SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7 * get__serialTask_7() const { return ____serialTask_7; }
	inline SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7 ** get_address_of__serialTask_7() { return &____serialTask_7; }
	inline void set__serialTask_7(SerialTaskCollection_t31E5E0CD4A5BAA251A396D5B51F12E1FB6B68BE7 * value)
	{
		____serialTask_7 = value;
		Il2CppCodeGenWriteBarrier((&____serialTask_7), value);
	}

	inline static int32_t get_offset_of__onProgressEvent_8() { return static_cast<int32_t>(offsetof(SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A, ____onProgressEvent_8)); }
	inline OnProgress_tADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4 * get__onProgressEvent_8() const { return ____onProgressEvent_8; }
	inline OnProgress_tADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4 ** get_address_of__onProgressEvent_8() { return &____onProgressEvent_8; }
	inline void set__onProgressEvent_8(OnProgress_tADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4 * value)
	{
		____onProgressEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&____onProgressEvent_8), value);
	}

	inline static int32_t get_offset_of__childTaskCounter_9() { return static_cast<int32_t>(offsetof(SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A, ____childTaskCounter_9)); }
	inline int32_t get__childTaskCounter_9() const { return ____childTaskCounter_9; }
	inline int32_t* get_address_of__childTaskCounter_9() { return &____childTaskCounter_9; }
	inline void set__childTaskCounter_9(int32_t value)
	{
		____childTaskCounter_9 = value;
	}

	inline static int32_t get_offset_of__childTaskFinished_10() { return static_cast<int32_t>(offsetof(SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A, ____childTaskFinished_10)); }
	inline float get__childTaskFinished_10() const { return ____childTaskFinished_10; }
	inline float* get_address_of__childTaskFinished_10() { return &____childTaskFinished_10; }
	inline void set__childTaskFinished_10(float value)
	{
		____childTaskFinished_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALTASK_TF1A918D9502AC12F7FFC785540520BB122C5B04A_H
#ifndef SERVERFILELOADERTASK_T95314098EBA8E90891F1D8C5F19C0DC49D532F92_H
#define SERVERFILELOADERTASK_T95314098EBA8E90891F1D8C5F19C0DC49D532F92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.ServerFileLoaderTask
struct  ServerFileLoaderTask_t95314098EBA8E90891F1D8C5F19C0DC49D532F92  : public BasicTask_1_t2103EEFF091DD4A943C23C8C77591716114AFEF3
{
public:
	// System.String Tayr.ServerFileLoaderTask::_path
	String_t* ____path_7;

public:
	inline static int32_t get_offset_of__path_7() { return static_cast<int32_t>(offsetof(ServerFileLoaderTask_t95314098EBA8E90891F1D8C5F19C0DC49D532F92, ____path_7)); }
	inline String_t* get__path_7() const { return ____path_7; }
	inline String_t** get_address_of__path_7() { return &____path_7; }
	inline void set__path_7(String_t* value)
	{
		____path_7 = value;
		Il2CppCodeGenWriteBarrier((&____path_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERFILELOADERTASK_T95314098EBA8E90891F1D8C5F19C0DC49D532F92_H
#ifndef STREAMINGASSETFILELOADERTASK_T766339DE3AEAB244DF39F9C963074DCCAEA2BA43_H
#define STREAMINGASSETFILELOADERTASK_T766339DE3AEAB244DF39F9C963074DCCAEA2BA43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.StreamingAssetFileLoaderTask
struct  StreamingAssetFileLoaderTask_t766339DE3AEAB244DF39F9C963074DCCAEA2BA43  : public BasicTask_1_t2103EEFF091DD4A943C23C8C77591716114AFEF3
{
public:
	// System.String Tayr.StreamingAssetFileLoaderTask::_path
	String_t* ____path_7;

public:
	inline static int32_t get_offset_of__path_7() { return static_cast<int32_t>(offsetof(StreamingAssetFileLoaderTask_t766339DE3AEAB244DF39F9C963074DCCAEA2BA43, ____path_7)); }
	inline String_t* get__path_7() const { return ____path_7; }
	inline String_t** get_address_of__path_7() { return &____path_7; }
	inline void set__path_7(String_t* value)
	{
		____path_7 = value;
		Il2CppCodeGenWriteBarrier((&____path_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGASSETFILELOADERTASK_T766339DE3AEAB244DF39F9C963074DCCAEA2BA43_H
#ifndef TALIASATTRIBUTE_T1E4D6E59DCFE4E3379DF65A2A8FAE97AFD9F793B_H
#define TALIASATTRIBUTE_T1E4D6E59DCFE4E3379DF65A2A8FAE97AFD9F793B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TAliasAttribute
struct  TAliasAttribute_t1E4D6E59DCFE4E3379DF65A2A8FAE97AFD9F793B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Tayr.TAliasAttribute::Alias
	String_t* ___Alias_0;
	// System.String Tayr.TAliasAttribute::Container
	String_t* ___Container_1;

public:
	inline static int32_t get_offset_of_Alias_0() { return static_cast<int32_t>(offsetof(TAliasAttribute_t1E4D6E59DCFE4E3379DF65A2A8FAE97AFD9F793B, ___Alias_0)); }
	inline String_t* get_Alias_0() const { return ___Alias_0; }
	inline String_t** get_address_of_Alias_0() { return &___Alias_0; }
	inline void set_Alias_0(String_t* value)
	{
		___Alias_0 = value;
		Il2CppCodeGenWriteBarrier((&___Alias_0), value);
	}

	inline static int32_t get_offset_of_Container_1() { return static_cast<int32_t>(offsetof(TAliasAttribute_t1E4D6E59DCFE4E3379DF65A2A8FAE97AFD9F793B, ___Container_1)); }
	inline String_t* get_Container_1() const { return ___Container_1; }
	inline String_t** get_address_of_Container_1() { return &___Container_1; }
	inline void set_Container_1(String_t* value)
	{
		___Container_1 = value;
		Il2CppCodeGenWriteBarrier((&___Container_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TALIASATTRIBUTE_T1E4D6E59DCFE4E3379DF65A2A8FAE97AFD9F793B_H
#ifndef TINJECTATTRIBUTE_T18D76B14BB15005A0008C7A1000F8299403127A3_H
#define TINJECTATTRIBUTE_T18D76B14BB15005A0008C7A1000F8299403127A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TInjectAttribute
struct  TInjectAttribute_t18D76B14BB15005A0008C7A1000F8299403127A3  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Tayr.TInjectAttribute::Tag
	String_t* ___Tag_0;

public:
	inline static int32_t get_offset_of_Tag_0() { return static_cast<int32_t>(offsetof(TInjectAttribute_t18D76B14BB15005A0008C7A1000F8299403127A3, ___Tag_0)); }
	inline String_t* get_Tag_0() const { return ___Tag_0; }
	inline String_t** get_address_of_Tag_0() { return &___Tag_0; }
	inline void set_Tag_0(String_t* value)
	{
		___Tag_0 = value;
		Il2CppCodeGenWriteBarrier((&___Tag_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TINJECTATTRIBUTE_T18D76B14BB15005A0008C7A1000F8299403127A3_H
#ifndef TSOUNDSYSTEM_T26E4FEC20D24CB5940F67189D1210E5A6454B511_H
#define TSOUNDSYSTEM_T26E4FEC20D24CB5940F67189D1210E5A6454B511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TSoundSystem
struct  TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511  : public BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8
{
public:
	// UnityEngine.AudioSource Tayr.TSoundSystem::FxSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___FxSource_3;
	// UnityEngine.AudioSource Tayr.TSoundSystem::MusicSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___MusicSource_4;

public:
	inline static int32_t get_offset_of_FxSource_3() { return static_cast<int32_t>(offsetof(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511, ___FxSource_3)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_FxSource_3() const { return ___FxSource_3; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_FxSource_3() { return &___FxSource_3; }
	inline void set_FxSource_3(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___FxSource_3 = value;
		Il2CppCodeGenWriteBarrier((&___FxSource_3), value);
	}

	inline static int32_t get_offset_of_MusicSource_4() { return static_cast<int32_t>(offsetof(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511, ___MusicSource_4)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_MusicSource_4() const { return ___MusicSource_4; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_MusicSource_4() { return &___MusicSource_4; }
	inline void set_MusicSource_4(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___MusicSource_4 = value;
		Il2CppCodeGenWriteBarrier((&___MusicSource_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TSOUNDSYSTEM_T26E4FEC20D24CB5940F67189D1210E5A6454B511_H
#ifndef UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#define UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifndef UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#define UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Single>
struct  UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T7839A0014FFD3A212A87547A44A7719D6549ED87_H
#ifndef UNITYEVENT_1_TECD0EC93EF224355126EAB0946F19A4FFA4136AD_H
#define UNITYEVENT_1_TECD0EC93EF224355126EAB0946F19A4FFA4136AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<UnityEngine.SystemLanguage>
struct  UnityEvent_1_tECD0EC93EF224355126EAB0946F19A4FFA4136AD  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tECD0EC93EF224355126EAB0946F19A4FFA4136AD, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_TECD0EC93EF224355126EAB0946F19A4FFA4136AD_H
#ifndef ADSMODE_TE42BCF91B16C0E62CECE6FACA149D0E8644971C2_H
#define ADSMODE_TE42BCF91B16C0E62CECE6FACA149D0E8644971C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.AdsMode
struct  AdsMode_tE42BCF91B16C0E62CECE6FACA149D0E8644971C2 
{
public:
	// System.Int32 Tayr.AdsMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdsMode_tE42BCF91B16C0E62CECE6FACA149D0E8644971C2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADSMODE_TE42BCF91B16C0E62CECE6FACA149D0E8644971C2_H
#ifndef ADSPLATFORM_T00535346A3EF196AB057005CE8A8C14A65DA4415_H
#define ADSPLATFORM_T00535346A3EF196AB057005CE8A8C14A65DA4415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.AdsPlatform
struct  AdsPlatform_t00535346A3EF196AB057005CE8A8C14A65DA4415 
{
public:
	// System.Int32 Tayr.AdsPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdsPlatform_t00535346A3EF196AB057005CE8A8C14A65DA4415, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADSPLATFORM_T00535346A3EF196AB057005CE8A8C14A65DA4415_H
#ifndef ANIMATIONTYPE_TBF4164D39BF991485B289847B900CB568CA6992B_H
#define ANIMATIONTYPE_TBF4164D39BF991485B289847B900CB568CA6992B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.AnimationType
struct  AnimationType_tBF4164D39BF991485B289847B900CB568CA6992B 
{
public:
	// System.Int32 Tayr.AnimationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimationType_tBF4164D39BF991485B289847B900CB568CA6992B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONTYPE_TBF4164D39BF991485B289847B900CB568CA6992B_H
#ifndef INTROFINISHEVENT_T41434E95E4BB9D37CE53CCD6322D8B8D1DCD87EC_H
#define INTROFINISHEVENT_T41434E95E4BB9D37CE53CCD6322D8B8D1DCD87EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.IntroFinishEvent
struct  IntroFinishEvent_t41434E95E4BB9D37CE53CCD6322D8B8D1DCD87EC  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTROFINISHEVENT_T41434E95E4BB9D37CE53CCD6322D8B8D1DCD87EC_H
#ifndef LOCALIZATIONLOADEDEVENT_T006A316DE326820292279E7ED43DEE916CCCE570_H
#define LOCALIZATIONLOADEDEVENT_T006A316DE326820292279E7ED43DEE916CCCE570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.LocalizationLoadedEvent
struct  LocalizationLoadedEvent_t006A316DE326820292279E7ED43DEE916CCCE570  : public UnityEvent_1_tECD0EC93EF224355126EAB0946F19A4FFA4136AD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZATIONLOADEDEVENT_T006A316DE326820292279E7ED43DEE916CCCE570_H
#ifndef ONPROGRESS_TADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4_H
#define ONPROGRESS_TADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.OnProgress
struct  OnProgress_tADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4  : public UnityEvent_1_t7839A0014FFD3A212A87547A44A7719D6549ED87
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONPROGRESS_TADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4_H
#ifndef OUTROFINISHEVENT_T6379D6DE5405EBB1F4C2160A23FD9AF6EC019F38_H
#define OUTROFINISHEVENT_T6379D6DE5405EBB1F4C2160A23FD9AF6EC019F38_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.OutroFinishEvent
struct  OutroFinishEvent_t6379D6DE5405EBB1F4C2160A23FD9AF6EC019F38  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTROFINISHEVENT_T6379D6DE5405EBB1F4C2160A23FD9AF6EC019F38_H
#ifndef DOWNLOADHANDLER_T4A7802ADC97024B469C87FA454B6973951980EE9_H
#define DOWNLOADHANDLER_T4A7802ADC97024B469C87FA454B6973951980EE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandler
struct  DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // DOWNLOADHANDLER_T4A7802ADC97024B469C87FA454B6973951980EE9_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef SYSTEMLANGUAGE_TDFC6112B5AB6A51D92EFFA0FD9BE2F35E7359ED0_H
#define SYSTEMLANGUAGE_TDFC6112B5AB6A51D92EFFA0FD9BE2F35E7359ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SystemLanguage
struct  SystemLanguage_tDFC6112B5AB6A51D92EFFA0FD9BE2F35E7359ED0 
{
public:
	// System.Int32 UnityEngine.SystemLanguage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SystemLanguage_tDFC6112B5AB6A51D92EFFA0FD9BE2F35E7359ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMLANGUAGE_TDFC6112B5AB6A51D92EFFA0FD9BE2F35E7359ED0_H
#ifndef ADSSETTINGSMODEL_TA300D75BC22011B877706A18F8D0D1BA9AF33E03_H
#define ADSSETTINGSMODEL_TA300D75BC22011B877706A18F8D0D1BA9AF33E03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.AdsSettingsModel
struct  AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03  : public RuntimeObject
{
public:
	// Tayr.AdsMode Tayr.AdsSettingsModel::Mode
	int32_t ___Mode_0;
	// System.String Tayr.AdsSettingsModel::AppId
	String_t* ___AppId_1;
	// System.String Tayr.AdsSettingsModel::BannerId
	String_t* ___BannerId_2;
	// System.String Tayr.AdsSettingsModel::InterstitialId
	String_t* ___InterstitialId_3;
	// System.String Tayr.AdsSettingsModel::VideoId
	String_t* ___VideoId_4;
	// System.String Tayr.AdsSettingsModel::TestDeviceId
	String_t* ___TestDeviceId_5;

public:
	inline static int32_t get_offset_of_Mode_0() { return static_cast<int32_t>(offsetof(AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03, ___Mode_0)); }
	inline int32_t get_Mode_0() const { return ___Mode_0; }
	inline int32_t* get_address_of_Mode_0() { return &___Mode_0; }
	inline void set_Mode_0(int32_t value)
	{
		___Mode_0 = value;
	}

	inline static int32_t get_offset_of_AppId_1() { return static_cast<int32_t>(offsetof(AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03, ___AppId_1)); }
	inline String_t* get_AppId_1() const { return ___AppId_1; }
	inline String_t** get_address_of_AppId_1() { return &___AppId_1; }
	inline void set_AppId_1(String_t* value)
	{
		___AppId_1 = value;
		Il2CppCodeGenWriteBarrier((&___AppId_1), value);
	}

	inline static int32_t get_offset_of_BannerId_2() { return static_cast<int32_t>(offsetof(AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03, ___BannerId_2)); }
	inline String_t* get_BannerId_2() const { return ___BannerId_2; }
	inline String_t** get_address_of_BannerId_2() { return &___BannerId_2; }
	inline void set_BannerId_2(String_t* value)
	{
		___BannerId_2 = value;
		Il2CppCodeGenWriteBarrier((&___BannerId_2), value);
	}

	inline static int32_t get_offset_of_InterstitialId_3() { return static_cast<int32_t>(offsetof(AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03, ___InterstitialId_3)); }
	inline String_t* get_InterstitialId_3() const { return ___InterstitialId_3; }
	inline String_t** get_address_of_InterstitialId_3() { return &___InterstitialId_3; }
	inline void set_InterstitialId_3(String_t* value)
	{
		___InterstitialId_3 = value;
		Il2CppCodeGenWriteBarrier((&___InterstitialId_3), value);
	}

	inline static int32_t get_offset_of_VideoId_4() { return static_cast<int32_t>(offsetof(AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03, ___VideoId_4)); }
	inline String_t* get_VideoId_4() const { return ___VideoId_4; }
	inline String_t** get_address_of_VideoId_4() { return &___VideoId_4; }
	inline void set_VideoId_4(String_t* value)
	{
		___VideoId_4 = value;
		Il2CppCodeGenWriteBarrier((&___VideoId_4), value);
	}

	inline static int32_t get_offset_of_TestDeviceId_5() { return static_cast<int32_t>(offsetof(AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03, ___TestDeviceId_5)); }
	inline String_t* get_TestDeviceId_5() const { return ___TestDeviceId_5; }
	inline String_t** get_address_of_TestDeviceId_5() { return &___TestDeviceId_5; }
	inline void set_TestDeviceId_5(String_t* value)
	{
		___TestDeviceId_5 = value;
		Il2CppCodeGenWriteBarrier((&___TestDeviceId_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADSSETTINGSMODEL_TA300D75BC22011B877706A18F8D0D1BA9AF33E03_H
#ifndef LOADCSVLOCALIZATIONTASK_T54FD8578E5033272852E9BCD334207B43D341E78_H
#define LOADCSVLOCALIZATIONTASK_T54FD8578E5033272852E9BCD334207B43D341E78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.LoadCSVLocalizationTask
struct  LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78  : public BasicTask_1_t441E728836BB86EF4FACC39258BA9F3AE70D4C99
{
public:
	// System.Char[] Tayr.LoadCSVLocalizationTask::NEW_LINE
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___NEW_LINE_7;
	// System.String Tayr.LoadCSVLocalizationTask::SEPARATOR
	String_t* ___SEPARATOR_8;
	// System.String Tayr.LoadCSVLocalizationTask::COMMENT
	String_t* ___COMMENT_9;
	// System.String Tayr.LoadCSVLocalizationTask::INTENDED_NEW_LINE
	String_t* ___INTENDED_NEW_LINE_10;
	// UnityEngine.SystemLanguage Tayr.LoadCSVLocalizationTask::_language
	int32_t ____language_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Tayr.LoadCSVLocalizationTask::_translations
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ____translations_12;

public:
	inline static int32_t get_offset_of_NEW_LINE_7() { return static_cast<int32_t>(offsetof(LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78, ___NEW_LINE_7)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_NEW_LINE_7() const { return ___NEW_LINE_7; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_NEW_LINE_7() { return &___NEW_LINE_7; }
	inline void set_NEW_LINE_7(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___NEW_LINE_7 = value;
		Il2CppCodeGenWriteBarrier((&___NEW_LINE_7), value);
	}

	inline static int32_t get_offset_of_SEPARATOR_8() { return static_cast<int32_t>(offsetof(LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78, ___SEPARATOR_8)); }
	inline String_t* get_SEPARATOR_8() const { return ___SEPARATOR_8; }
	inline String_t** get_address_of_SEPARATOR_8() { return &___SEPARATOR_8; }
	inline void set_SEPARATOR_8(String_t* value)
	{
		___SEPARATOR_8 = value;
		Il2CppCodeGenWriteBarrier((&___SEPARATOR_8), value);
	}

	inline static int32_t get_offset_of_COMMENT_9() { return static_cast<int32_t>(offsetof(LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78, ___COMMENT_9)); }
	inline String_t* get_COMMENT_9() const { return ___COMMENT_9; }
	inline String_t** get_address_of_COMMENT_9() { return &___COMMENT_9; }
	inline void set_COMMENT_9(String_t* value)
	{
		___COMMENT_9 = value;
		Il2CppCodeGenWriteBarrier((&___COMMENT_9), value);
	}

	inline static int32_t get_offset_of_INTENDED_NEW_LINE_10() { return static_cast<int32_t>(offsetof(LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78, ___INTENDED_NEW_LINE_10)); }
	inline String_t* get_INTENDED_NEW_LINE_10() const { return ___INTENDED_NEW_LINE_10; }
	inline String_t** get_address_of_INTENDED_NEW_LINE_10() { return &___INTENDED_NEW_LINE_10; }
	inline void set_INTENDED_NEW_LINE_10(String_t* value)
	{
		___INTENDED_NEW_LINE_10 = value;
		Il2CppCodeGenWriteBarrier((&___INTENDED_NEW_LINE_10), value);
	}

	inline static int32_t get_offset_of__language_11() { return static_cast<int32_t>(offsetof(LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78, ____language_11)); }
	inline int32_t get__language_11() const { return ____language_11; }
	inline int32_t* get_address_of__language_11() { return &____language_11; }
	inline void set__language_11(int32_t value)
	{
		____language_11 = value;
	}

	inline static int32_t get_offset_of__translations_12() { return static_cast<int32_t>(offsetof(LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78, ____translations_12)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get__translations_12() const { return ____translations_12; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of__translations_12() { return &____translations_12; }
	inline void set__translations_12(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		____translations_12 = value;
		Il2CppCodeGenWriteBarrier((&____translations_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADCSVLOCALIZATIONTASK_T54FD8578E5033272852E9BCD334207B43D341E78_H
#ifndef TLOCALIZATIONSYSTEM_TA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B_H
#define TLOCALIZATIONSYSTEM_TA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TLocalizationSystem
struct  TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B  : public RuntimeObject
{
public:
	// Tayr.LocalizationLoadedEvent Tayr.TLocalizationSystem::_onLocalizationLoaded
	LocalizationLoadedEvent_t006A316DE326820292279E7ED43DEE916CCCE570 * ____onLocalizationLoaded_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Tayr.TLocalizationSystem::_translation
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ____translation_1;
	// System.Collections.Generic.Dictionary`2<UnityEngine.SystemLanguage,System.String[]> Tayr.TLocalizationSystem::_presetLanguages
	Dictionary_2_tD72C2CDDAA3A34F41790E7B0309A2CB21D2C2654 * ____presetLanguages_2;
	// UnityEngine.SystemLanguage Tayr.TLocalizationSystem::_currentLanguage
	int32_t ____currentLanguage_3;
	// System.Boolean Tayr.TLocalizationSystem::_localizationLoaded
	bool ____localizationLoaded_4;

public:
	inline static int32_t get_offset_of__onLocalizationLoaded_0() { return static_cast<int32_t>(offsetof(TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B, ____onLocalizationLoaded_0)); }
	inline LocalizationLoadedEvent_t006A316DE326820292279E7ED43DEE916CCCE570 * get__onLocalizationLoaded_0() const { return ____onLocalizationLoaded_0; }
	inline LocalizationLoadedEvent_t006A316DE326820292279E7ED43DEE916CCCE570 ** get_address_of__onLocalizationLoaded_0() { return &____onLocalizationLoaded_0; }
	inline void set__onLocalizationLoaded_0(LocalizationLoadedEvent_t006A316DE326820292279E7ED43DEE916CCCE570 * value)
	{
		____onLocalizationLoaded_0 = value;
		Il2CppCodeGenWriteBarrier((&____onLocalizationLoaded_0), value);
	}

	inline static int32_t get_offset_of__translation_1() { return static_cast<int32_t>(offsetof(TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B, ____translation_1)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get__translation_1() const { return ____translation_1; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of__translation_1() { return &____translation_1; }
	inline void set__translation_1(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		____translation_1 = value;
		Il2CppCodeGenWriteBarrier((&____translation_1), value);
	}

	inline static int32_t get_offset_of__presetLanguages_2() { return static_cast<int32_t>(offsetof(TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B, ____presetLanguages_2)); }
	inline Dictionary_2_tD72C2CDDAA3A34F41790E7B0309A2CB21D2C2654 * get__presetLanguages_2() const { return ____presetLanguages_2; }
	inline Dictionary_2_tD72C2CDDAA3A34F41790E7B0309A2CB21D2C2654 ** get_address_of__presetLanguages_2() { return &____presetLanguages_2; }
	inline void set__presetLanguages_2(Dictionary_2_tD72C2CDDAA3A34F41790E7B0309A2CB21D2C2654 * value)
	{
		____presetLanguages_2 = value;
		Il2CppCodeGenWriteBarrier((&____presetLanguages_2), value);
	}

	inline static int32_t get_offset_of__currentLanguage_3() { return static_cast<int32_t>(offsetof(TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B, ____currentLanguage_3)); }
	inline int32_t get__currentLanguage_3() const { return ____currentLanguage_3; }
	inline int32_t* get_address_of__currentLanguage_3() { return &____currentLanguage_3; }
	inline void set__currentLanguage_3(int32_t value)
	{
		____currentLanguage_3 = value;
	}

	inline static int32_t get_offset_of__localizationLoaded_4() { return static_cast<int32_t>(offsetof(TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B, ____localizationLoaded_4)); }
	inline bool get__localizationLoaded_4() const { return ____localizationLoaded_4; }
	inline bool* get_address_of__localizationLoaded_4() { return &____localizationLoaded_4; }
	inline void set__localizationLoaded_4(bool value)
	{
		____localizationLoaded_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLOCALIZATIONSYSTEM_TA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef DOWNLOADHANDLERSCRIPT_T04E40C9E41AB0DE0863F4EDA30A9F17B8BC8B5CF_H
#define DOWNLOADHANDLERSCRIPT_T04E40C9E41AB0DE0863F4EDA30A9F17B8BC8B5CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Networking.DownloadHandlerScript
struct  DownloadHandlerScript_t04E40C9E41AB0DE0863F4EDA30A9F17B8BC8B5CF  : public DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerScript
struct DownloadHandlerScript_t04E40C9E41AB0DE0863F4EDA30A9F17B8BC8B5CF_marshaled_pinvoke : public DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerScript
struct DownloadHandlerScript_t04E40C9E41AB0DE0863F4EDA30A9F17B8BC8B5CF_marshaled_com : public DownloadHandler_t4A7802ADC97024B469C87FA454B6973951980EE9_marshaled_com
{
};
#endif // DOWNLOADHANDLERSCRIPT_T04E40C9E41AB0DE0863F4EDA30A9F17B8BC8B5CF_H
#ifndef DOWNLOADHANDLERFILE_T606AFD19457AD43034A4A76A17852055CF260D88_H
#define DOWNLOADHANDLERFILE_T606AFD19457AD43034A4A76A17852055CF260D88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.DownloadHandlerFile
struct  DownloadHandlerFile_t606AFD19457AD43034A4A76A17852055CF260D88  : public DownloadHandlerScript_t04E40C9E41AB0DE0863F4EDA30A9F17B8BC8B5CF
{
public:
	// System.Int32 Tayr.DownloadHandlerFile::_contentLength
	int32_t ____contentLength_1;
	// System.Int32 Tayr.DownloadHandlerFile::_received
	int32_t ____received_2;
	// System.IO.FileStream Tayr.DownloadHandlerFile::_stream
	FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * ____stream_3;

public:
	inline static int32_t get_offset_of__contentLength_1() { return static_cast<int32_t>(offsetof(DownloadHandlerFile_t606AFD19457AD43034A4A76A17852055CF260D88, ____contentLength_1)); }
	inline int32_t get__contentLength_1() const { return ____contentLength_1; }
	inline int32_t* get_address_of__contentLength_1() { return &____contentLength_1; }
	inline void set__contentLength_1(int32_t value)
	{
		____contentLength_1 = value;
	}

	inline static int32_t get_offset_of__received_2() { return static_cast<int32_t>(offsetof(DownloadHandlerFile_t606AFD19457AD43034A4A76A17852055CF260D88, ____received_2)); }
	inline int32_t get__received_2() const { return ____received_2; }
	inline int32_t* get_address_of__received_2() { return &____received_2; }
	inline void set__received_2(int32_t value)
	{
		____received_2 = value;
	}

	inline static int32_t get_offset_of__stream_3() { return static_cast<int32_t>(offsetof(DownloadHandlerFile_t606AFD19457AD43034A4A76A17852055CF260D88, ____stream_3)); }
	inline FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * get__stream_3() const { return ____stream_3; }
	inline FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 ** get_address_of__stream_3() { return &____stream_3; }
	inline void set__stream_3(FileStream_tA770BF9AF0906644D43C81B962C7DBC3BC79A418 * value)
	{
		____stream_3 = value;
		Il2CppCodeGenWriteBarrier((&____stream_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADHANDLERFILE_T606AFD19457AD43034A4A76A17852055CF260D88_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#define TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TMonoBehaviour
struct  TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#ifndef BASICNODE_1_T31F5A99F9379D18DE55F22D14196E346D7C1FADB_H
#define BASICNODE_1_T31F5A99F9379D18DE55F22D14196E346D7C1FADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicNode`1<System.String>
struct  BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// T Tayr.BasicNode`1::Data
	String_t* ___Data_4;
	// Tayr.INodeAnimationHandler Tayr.BasicNode`1::_nodeAnimationHandler
	RuntimeObject* ____nodeAnimationHandler_5;
	// Tayr.NodeAnimator Tayr.BasicNode`1::_nodeAnimator
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * ____nodeAnimator_6;

public:
	inline static int32_t get_offset_of_Data_4() { return static_cast<int32_t>(offsetof(BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB, ___Data_4)); }
	inline String_t* get_Data_4() const { return ___Data_4; }
	inline String_t** get_address_of_Data_4() { return &___Data_4; }
	inline void set_Data_4(String_t* value)
	{
		___Data_4 = value;
		Il2CppCodeGenWriteBarrier((&___Data_4), value);
	}

	inline static int32_t get_offset_of__nodeAnimationHandler_5() { return static_cast<int32_t>(offsetof(BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB, ____nodeAnimationHandler_5)); }
	inline RuntimeObject* get__nodeAnimationHandler_5() const { return ____nodeAnimationHandler_5; }
	inline RuntimeObject** get_address_of__nodeAnimationHandler_5() { return &____nodeAnimationHandler_5; }
	inline void set__nodeAnimationHandler_5(RuntimeObject* value)
	{
		____nodeAnimationHandler_5 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimationHandler_5), value);
	}

	inline static int32_t get_offset_of__nodeAnimator_6() { return static_cast<int32_t>(offsetof(BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB, ____nodeAnimator_6)); }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * get__nodeAnimator_6() const { return ____nodeAnimator_6; }
	inline NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F ** get_address_of__nodeAnimator_6() { return &____nodeAnimator_6; }
	inline void set__nodeAnimator_6(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F * value)
	{
		____nodeAnimator_6 = value;
		Il2CppCodeGenWriteBarrier((&____nodeAnimator_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICNODE_1_T31F5A99F9379D18DE55F22D14196E346D7C1FADB_H
#ifndef GAMEOBJECTPOOL_TB195E5FFA2B13603B867994317CE6FE9B1122221_H
#define GAMEOBJECTPOOL_TB195E5FFA2B13603B867994317CE6FE9B1122221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.GameObjectPool
struct  GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// System.Boolean Tayr.GameObjectPool::logStatus
	bool ___logStatus_4;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,Tayr.ObjectPool`1<UnityEngine.GameObject>> Tayr.GameObjectPool::prefabLookup
	Dictionary_2_t14628031B0235DCDE817C9CF9685AE79944DE191 * ___prefabLookup_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,Tayr.ObjectPool`1<UnityEngine.GameObject>> Tayr.GameObjectPool::instanceLookup
	Dictionary_2_t14628031B0235DCDE817C9CF9685AE79944DE191 * ___instanceLookup_6;
	// System.Boolean Tayr.GameObjectPool::dirty
	bool ___dirty_7;
	// Zenject.DiContainer Tayr.GameObjectPool::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_8;

public:
	inline static int32_t get_offset_of_logStatus_4() { return static_cast<int32_t>(offsetof(GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221, ___logStatus_4)); }
	inline bool get_logStatus_4() const { return ___logStatus_4; }
	inline bool* get_address_of_logStatus_4() { return &___logStatus_4; }
	inline void set_logStatus_4(bool value)
	{
		___logStatus_4 = value;
	}

	inline static int32_t get_offset_of_prefabLookup_5() { return static_cast<int32_t>(offsetof(GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221, ___prefabLookup_5)); }
	inline Dictionary_2_t14628031B0235DCDE817C9CF9685AE79944DE191 * get_prefabLookup_5() const { return ___prefabLookup_5; }
	inline Dictionary_2_t14628031B0235DCDE817C9CF9685AE79944DE191 ** get_address_of_prefabLookup_5() { return &___prefabLookup_5; }
	inline void set_prefabLookup_5(Dictionary_2_t14628031B0235DCDE817C9CF9685AE79944DE191 * value)
	{
		___prefabLookup_5 = value;
		Il2CppCodeGenWriteBarrier((&___prefabLookup_5), value);
	}

	inline static int32_t get_offset_of_instanceLookup_6() { return static_cast<int32_t>(offsetof(GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221, ___instanceLookup_6)); }
	inline Dictionary_2_t14628031B0235DCDE817C9CF9685AE79944DE191 * get_instanceLookup_6() const { return ___instanceLookup_6; }
	inline Dictionary_2_t14628031B0235DCDE817C9CF9685AE79944DE191 ** get_address_of_instanceLookup_6() { return &___instanceLookup_6; }
	inline void set_instanceLookup_6(Dictionary_2_t14628031B0235DCDE817C9CF9685AE79944DE191 * value)
	{
		___instanceLookup_6 = value;
		Il2CppCodeGenWriteBarrier((&___instanceLookup_6), value);
	}

	inline static int32_t get_offset_of_dirty_7() { return static_cast<int32_t>(offsetof(GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221, ___dirty_7)); }
	inline bool get_dirty_7() const { return ___dirty_7; }
	inline bool* get_address_of_dirty_7() { return &___dirty_7; }
	inline void set_dirty_7(bool value)
	{
		___dirty_7 = value;
	}

	inline static int32_t get_offset_of__container_8() { return static_cast<int32_t>(offsetof(GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221, ____container_8)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_8() const { return ____container_8; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_8() { return &____container_8; }
	inline void set__container_8(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_8 = value;
		Il2CppCodeGenWriteBarrier((&____container_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTPOOL_TB195E5FFA2B13603B867994317CE6FE9B1122221_H
#ifndef NODEANIMATOR_T5E49B23D617EB48B77766FF9E73F76F43F57813F_H
#define NODEANIMATOR_T5E49B23D617EB48B77766FF9E73F76F43F57813F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.NodeAnimator
struct  NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// Tayr.AnimationType Tayr.NodeAnimator::AnimationType
	int32_t ___AnimationType_4;
	// System.String Tayr.NodeAnimator::Intro
	String_t* ___Intro_5;
	// System.String Tayr.NodeAnimator::Outro
	String_t* ___Outro_6;
	// Tayr.IntroFinishEvent Tayr.NodeAnimator::IntroFinishEvent
	IntroFinishEvent_t41434E95E4BB9D37CE53CCD6322D8B8D1DCD87EC * ___IntroFinishEvent_7;
	// Tayr.OutroFinishEvent Tayr.NodeAnimator::OutroFinishEvent
	OutroFinishEvent_t6379D6DE5405EBB1F4C2160A23FD9AF6EC019F38 * ___OutroFinishEvent_8;

public:
	inline static int32_t get_offset_of_AnimationType_4() { return static_cast<int32_t>(offsetof(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F, ___AnimationType_4)); }
	inline int32_t get_AnimationType_4() const { return ___AnimationType_4; }
	inline int32_t* get_address_of_AnimationType_4() { return &___AnimationType_4; }
	inline void set_AnimationType_4(int32_t value)
	{
		___AnimationType_4 = value;
	}

	inline static int32_t get_offset_of_Intro_5() { return static_cast<int32_t>(offsetof(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F, ___Intro_5)); }
	inline String_t* get_Intro_5() const { return ___Intro_5; }
	inline String_t** get_address_of_Intro_5() { return &___Intro_5; }
	inline void set_Intro_5(String_t* value)
	{
		___Intro_5 = value;
		Il2CppCodeGenWriteBarrier((&___Intro_5), value);
	}

	inline static int32_t get_offset_of_Outro_6() { return static_cast<int32_t>(offsetof(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F, ___Outro_6)); }
	inline String_t* get_Outro_6() const { return ___Outro_6; }
	inline String_t** get_address_of_Outro_6() { return &___Outro_6; }
	inline void set_Outro_6(String_t* value)
	{
		___Outro_6 = value;
		Il2CppCodeGenWriteBarrier((&___Outro_6), value);
	}

	inline static int32_t get_offset_of_IntroFinishEvent_7() { return static_cast<int32_t>(offsetof(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F, ___IntroFinishEvent_7)); }
	inline IntroFinishEvent_t41434E95E4BB9D37CE53CCD6322D8B8D1DCD87EC * get_IntroFinishEvent_7() const { return ___IntroFinishEvent_7; }
	inline IntroFinishEvent_t41434E95E4BB9D37CE53CCD6322D8B8D1DCD87EC ** get_address_of_IntroFinishEvent_7() { return &___IntroFinishEvent_7; }
	inline void set_IntroFinishEvent_7(IntroFinishEvent_t41434E95E4BB9D37CE53CCD6322D8B8D1DCD87EC * value)
	{
		___IntroFinishEvent_7 = value;
		Il2CppCodeGenWriteBarrier((&___IntroFinishEvent_7), value);
	}

	inline static int32_t get_offset_of_OutroFinishEvent_8() { return static_cast<int32_t>(offsetof(NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F, ___OutroFinishEvent_8)); }
	inline OutroFinishEvent_t6379D6DE5405EBB1F4C2160A23FD9AF6EC019F38 * get_OutroFinishEvent_8() const { return ___OutroFinishEvent_8; }
	inline OutroFinishEvent_t6379D6DE5405EBB1F4C2160A23FD9AF6EC019F38 ** get_address_of_OutroFinishEvent_8() { return &___OutroFinishEvent_8; }
	inline void set_OutroFinishEvent_8(OutroFinishEvent_t6379D6DE5405EBB1F4C2160A23FD9AF6EC019F38 * value)
	{
		___OutroFinishEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___OutroFinishEvent_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEANIMATOR_T5E49B23D617EB48B77766FF9E73F76F43F57813F_H
#ifndef TDYNAMICLIST_TF4B70F991EE13A3CB137902A92EC1A729C06DB19_H
#define TDYNAMICLIST_TF4B70F991EE13A3CB137902A92EC1A729C06DB19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TDynamicList
struct  TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// Tayr.GameObjectPool Tayr.TDynamicList::_gameObjectPool
	GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * ____gameObjectPool_4;
	// UnityEngine.Transform Tayr.TDynamicList::ContentParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___ContentParent_5;
	// System.Collections.Generic.List`1<Tayr.TListItem> Tayr.TDynamicList::_items
	List_1_t2F608C9C1F2DE3FCCC09DBBB84D6C0B85A093212 * ____items_6;

public:
	inline static int32_t get_offset_of__gameObjectPool_4() { return static_cast<int32_t>(offsetof(TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19, ____gameObjectPool_4)); }
	inline GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * get__gameObjectPool_4() const { return ____gameObjectPool_4; }
	inline GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 ** get_address_of__gameObjectPool_4() { return &____gameObjectPool_4; }
	inline void set__gameObjectPool_4(GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221 * value)
	{
		____gameObjectPool_4 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectPool_4), value);
	}

	inline static int32_t get_offset_of_ContentParent_5() { return static_cast<int32_t>(offsetof(TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19, ___ContentParent_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_ContentParent_5() const { return ___ContentParent_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_ContentParent_5() { return &___ContentParent_5; }
	inline void set_ContentParent_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___ContentParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___ContentParent_5), value);
	}

	inline static int32_t get_offset_of__items_6() { return static_cast<int32_t>(offsetof(TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19, ____items_6)); }
	inline List_1_t2F608C9C1F2DE3FCCC09DBBB84D6C0B85A093212 * get__items_6() const { return ____items_6; }
	inline List_1_t2F608C9C1F2DE3FCCC09DBBB84D6C0B85A093212 ** get_address_of__items_6() { return &____items_6; }
	inline void set__items_6(List_1_t2F608C9C1F2DE3FCCC09DBBB84D6C0B85A093212 * value)
	{
		____items_6 = value;
		Il2CppCodeGenWriteBarrier((&____items_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TDYNAMICLIST_TF4B70F991EE13A3CB137902A92EC1A729C06DB19_H
#ifndef TLIST_TB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292_H
#define TLIST_TB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TList
struct  TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// UnityEngine.GameObject Tayr.TList::ItemsPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ItemsPrefab_4;
	// UnityEngine.Transform Tayr.TList::ContentParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___ContentParent_5;
	// System.Collections.Generic.List`1<Tayr.TListItem> Tayr.TList::_items
	List_1_t2F608C9C1F2DE3FCCC09DBBB84D6C0B85A093212 * ____items_6;

public:
	inline static int32_t get_offset_of_ItemsPrefab_4() { return static_cast<int32_t>(offsetof(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292, ___ItemsPrefab_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ItemsPrefab_4() const { return ___ItemsPrefab_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ItemsPrefab_4() { return &___ItemsPrefab_4; }
	inline void set_ItemsPrefab_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ItemsPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___ItemsPrefab_4), value);
	}

	inline static int32_t get_offset_of_ContentParent_5() { return static_cast<int32_t>(offsetof(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292, ___ContentParent_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_ContentParent_5() const { return ___ContentParent_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_ContentParent_5() { return &___ContentParent_5; }
	inline void set_ContentParent_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___ContentParent_5 = value;
		Il2CppCodeGenWriteBarrier((&___ContentParent_5), value);
	}

	inline static int32_t get_offset_of__items_6() { return static_cast<int32_t>(offsetof(TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292, ____items_6)); }
	inline List_1_t2F608C9C1F2DE3FCCC09DBBB84D6C0B85A093212 * get__items_6() const { return ____items_6; }
	inline List_1_t2F608C9C1F2DE3FCCC09DBBB84D6C0B85A093212 ** get_address_of__items_6() { return &____items_6; }
	inline void set__items_6(List_1_t2F608C9C1F2DE3FCCC09DBBB84D6C0B85A093212 * value)
	{
		____items_6 = value;
		Il2CppCodeGenWriteBarrier((&____items_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLIST_TB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292_H
#ifndef RETRYNODE_TA2D5659E521764F41C793319C5DD5D9E8F3EE04C_H
#define RETRYNODE_TA2D5659E521764F41C793319C5DD5D9E8F3EE04C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.RetryNode
struct  RetryNode_tA2D5659E521764F41C793319C5DD5D9E8F3EE04C  : public BasicNode_1_t31F5A99F9379D18DE55F22D14196E346D7C1FADB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETRYNODE_TA2D5659E521764F41C793319C5DD5D9E8F3EE04C_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7900 = { sizeof (ArabicFixer_t2221BB251023D6FC85CF202B56FB036FF7627ED3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7901 = { sizeof (AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F), -1, sizeof(AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7901[6] = 
{
	AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields::get_offset_of_ANDROID_INTERSTITIAL_TEST_ID_0(),
	AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields::get_offset_of_ANDROID_BANNER_TEST_ID_1(),
	AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields::get_offset_of_ANDROID_TEST_APP_ID_2(),
	AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields::get_offset_of_IOS_INTERSTITIAL_TEST_ID_3(),
	AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields::get_offset_of_IOS_BANNER_TEST_ID_4(),
	AdsConstants_t069750BD8A0D24B8EC924103D0A3A031F3EDD71F_StaticFields::get_offset_of_IOS_TEST_APP_ID_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7902 = { sizeof (AdsPlatform_t00535346A3EF196AB057005CE8A8C14A65DA4415)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7902[3] = 
{
	AdsPlatform_t00535346A3EF196AB057005CE8A8C14A65DA4415::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7903 = { sizeof (AdsMode_tE42BCF91B16C0E62CECE6FACA149D0E8644971C2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7903[3] = 
{
	AdsMode_tE42BCF91B16C0E62CECE6FACA149D0E8644971C2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7904 = { sizeof (AdsFactory_t491A846A1985B6E666BD20787FFFD63BBFE6EE2F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7905 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7906 = { sizeof (AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7906[6] = 
{
	AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03::get_offset_of_Mode_0(),
	AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03::get_offset_of_AppId_1(),
	AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03::get_offset_of_BannerId_2(),
	AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03::get_offset_of_InterstitialId_3(),
	AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03::get_offset_of_VideoId_4(),
	AdsSettingsModel_tA300D75BC22011B877706A18F8D0D1BA9AF33E03::get_offset_of_TestDeviceId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7907 = { sizeof (TAliasAttribute_t1E4D6E59DCFE4E3379DF65A2A8FAE97AFD9F793B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7907[2] = 
{
	TAliasAttribute_t1E4D6E59DCFE4E3379DF65A2A8FAE97AFD9F793B::get_offset_of_Alias_0(),
	TAliasAttribute_t1E4D6E59DCFE4E3379DF65A2A8FAE97AFD9F793B::get_offset_of_Container_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7908 = { sizeof (TInjectAttribute_t18D76B14BB15005A0008C7A1000F8299403127A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7908[1] = 
{
	TInjectAttribute_t18D76B14BB15005A0008C7A1000F8299403127A3::get_offset_of_Tag_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7909 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7910 = { sizeof (CommandFactory_t3B1CC884776C46DF4E4D4E404A1904C730C9EE81), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7911 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7911[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7912 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7912[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7913 = { sizeof (MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7913[4] = 
{
	MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A::get_offset_of_AliasComponentsMapping_0(),
	MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A::get_offset_of_ComponentsAliasMapping_1(),
	MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A::get_offset_of_SystemContainerMapping_2(),
	MappingInitializer_t2D92363C6AC21A6B38A61CC24ACEDADB1302C53A::get_offset_of_AliasContainersMapping_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7914 = { sizeof (U3CU3Ec__DisplayClass4_0_tB31B2942C7B81602E2E78F47C741C09F5448970F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7914[1] = 
{
	U3CU3Ec__DisplayClass4_0_tB31B2942C7B81602E2E78F47C741C09F5448970F::get_offset_of_assemblyName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7915 = { sizeof (JsonConfigurator_t4300FF6941F6A5F99CB53A04D924CFEAC2DD5B91), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7916 = { sizeof (DiCore_tF6CFCD21895475F9ECC5DB7FDB06093B89E55061), -1, sizeof(DiCore_tF6CFCD21895475F9ECC5DB7FDB06093B89E55061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7916[2] = 
{
	0,
	DiCore_tF6CFCD21895475F9ECC5DB7FDB06093B89E55061_StaticFields::get_offset_of_Container_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7917 = { sizeof (AnimatorExtension_tB48EDA5E01669A4C7BD7C1FBBF3C4AE32D2B62B1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7918 = { sizeof (GameObjectExtension_tDC2B83D8A14FDEEFBD564E75B618672A8F2069D4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7919 = { sizeof (MonoBehaviorExtension_tBEB0A1CBE2C514439C15AD58ED0A45DA97420E0D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7920 = { sizeof (TransformExtension_tD01D1E420ACCDB0E3AB58F7852434B8A0061CF81), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7921 = { sizeof (TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659), -1, sizeof(TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7921[10] = 
{
	TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields::get_offset_of_JsonConfigurator_0(),
	TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields::get_offset_of_TLocalizationSystem_1(),
	TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields::get_offset_of_MappingInitilizer_2(),
	TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields::get_offset_of_ResourceLibrary_3(),
	TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields::get_offset_of_SceneLibrary_4(),
	TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields::get_offset_of_PoolManager_5(),
	TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields::get_offset_of_NodeSystem_6(),
	TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields::get_offset_of_TSystemManager_7(),
	TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields::get_offset_of_TUpdaterSystem_8(),
	TayrInitializer_tF32B0F730F96A7B8BA68F3B45FE35325C8CCC659_StaticFields::get_offset_of_ConfKeys_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7922 = { sizeof (FileUtils_t6B19A7B46ACB859C0A3D613EA39CD6376CCC1E00), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7923 = { sizeof (DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7923[2] = 
{
	DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F::get_offset_of__url_7(),
	DownloadFileTask_t10719E6DB76BF03F2FAE4428B512AF5C4375104F::get_offset_of__destination_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7924 = { sizeof (U3CTaskU3Ed__3_tA83839BA6DD6AC4B9DA308F04018E0331D810A4B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7924[4] = 
{
	U3CTaskU3Ed__3_tA83839BA6DD6AC4B9DA308F04018E0331D810A4B::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__3_tA83839BA6DD6AC4B9DA308F04018E0331D810A4B::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__3_tA83839BA6DD6AC4B9DA308F04018E0331D810A4B::get_offset_of_U3CU3E4__this_2(),
	U3CTaskU3Ed__3_tA83839BA6DD6AC4B9DA308F04018E0331D810A4B::get_offset_of_U3CwwwU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7925 = { sizeof (TPathResolver_tFB0A83A4198D6F6D4DB90CE21D1ECA7212882796), -1, sizeof(TPathResolver_tFB0A83A4198D6F6D4DB90CE21D1ECA7212882796_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7925[3] = 
{
	TPathResolver_tFB0A83A4198D6F6D4DB90CE21D1ECA7212882796_StaticFields::get_offset_of_Server_0(),
	TPathResolver_tFB0A83A4198D6F6D4DB90CE21D1ECA7212882796_StaticFields::get_offset_of_Depot_1(),
	TPathResolver_tFB0A83A4198D6F6D4DB90CE21D1ECA7212882796_StaticFields::get_offset_of_LocalizationPath_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7926 = { sizeof (AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7926[3] = 
{
	AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD::get_offset_of__path_7(),
	AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD::get_offset_of__diContainer_8(),
	AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD::get_offset_of__injectTag_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7927 = { sizeof (U3CTaskU3Ed__4_t8D82C67FA91BC73B95A260D81FCD18A6BD22C4CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7927[4] = 
{
	U3CTaskU3Ed__4_t8D82C67FA91BC73B95A260D81FCD18A6BD22C4CC::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__4_t8D82C67FA91BC73B95A260D81FCD18A6BD22C4CC::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__4_t8D82C67FA91BC73B95A260D81FCD18A6BD22C4CC::get_offset_of_U3CU3E4__this_2(),
	U3CTaskU3Ed__4_t8D82C67FA91BC73B95A260D81FCD18A6BD22C4CC::get_offset_of_U3CloaderU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7928 = { sizeof (AssetBundlesLibrary_t9B34940E0C2AF8549D39827846C36516EC514DDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7928[2] = 
{
	AssetBundlesLibrary_t9B34940E0C2AF8549D39827846C36516EC514DDD::get_offset_of__bundle_0(),
	AssetBundlesLibrary_t9B34940E0C2AF8549D39827846C36516EC514DDD::get_offset_of__poolManager_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7929 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7930 = { sizeof (ResourceLibrary_t9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7930[2] = 
{
	ResourceLibrary_t9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54::get_offset_of__resourcesDictionary_0(),
	ResourceLibrary_t9B2CFC877F2C3CA0A72D5FA1324B6EABEA5E5B54::get_offset_of__poolManager_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7931 = { sizeof (SceneLibrary_tD4A336537A5F417E9F29493F769735C1EDFBCFDF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7931[1] = 
{
	SceneLibrary_tD4A336537A5F417E9F29493F769735C1EDFBCFDF::get_offset_of__sceneDictionary_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7932 = { sizeof (LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7932[6] = 
{
	LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78::get_offset_of_NEW_LINE_7(),
	LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78::get_offset_of_SEPARATOR_8(),
	LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78::get_offset_of_COMMENT_9(),
	LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78::get_offset_of_INTENDED_NEW_LINE_10(),
	LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78::get_offset_of__language_11(),
	LoadCSVLocalizationTask_t54FD8578E5033272852E9BCD334207B43D341E78::get_offset_of__translations_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7933 = { sizeof (U3CTaskU3Ed__7_tB45D5E1AB2605BD2133466C0A5325A7326ACBADD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7933[4] = 
{
	U3CTaskU3Ed__7_tB45D5E1AB2605BD2133466C0A5325A7326ACBADD::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__7_tB45D5E1AB2605BD2133466C0A5325A7326ACBADD::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__7_tB45D5E1AB2605BD2133466C0A5325A7326ACBADD::get_offset_of_U3CU3E4__this_2(),
	U3CTaskU3Ed__7_tB45D5E1AB2605BD2133466C0A5325A7326ACBADD::get_offset_of_U3CloaderTaskU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7934 = { sizeof (TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7934[5] = 
{
	TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B::get_offset_of__onLocalizationLoaded_0(),
	TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B::get_offset_of__translation_1(),
	TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B::get_offset_of__presetLanguages_2(),
	TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B::get_offset_of__currentLanguage_3(),
	TLocalizationSystem_tA20FC05431CAB5EF92DDA121E29C1540A6DBCE1B::get_offset_of__localizationLoaded_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7935 = { sizeof (LocalizationLoadedEvent_t006A316DE326820292279E7ED43DEE916CCCE570), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7936 = { sizeof (TLocalizationUtils_t520027CB2848DEA38DB70D0DEDB4CDC9F8BC64FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7937 = { sizeof (DepotFileLoaderTask_t8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7937[1] = 
{
	DepotFileLoaderTask_t8DDCB6D8C0B61F0549B80E3F3FBC9D210AE67A32::get_offset_of__path_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7938 = { sizeof (U3CTaskU3Ed__2_tB76CA1F55FF199E00748C1353CA3B0905774C2D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7938[3] = 
{
	U3CTaskU3Ed__2_tB76CA1F55FF199E00748C1353CA3B0905774C2D9::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__2_tB76CA1F55FF199E00748C1353CA3B0905774C2D9::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__2_tB76CA1F55FF199E00748C1353CA3B0905774C2D9::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7939 = { sizeof (LocalFileLoaderTask_t4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7939[1] = 
{
	LocalFileLoaderTask_t4DD31D2B90A0EDE20BB70AE18E4D86609F7178DD::get_offset_of__path_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7940 = { sizeof (U3CTaskU3Ed__2_t6054B9CF056BDC9A4E1A15DD7F3187A12F8927F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7940[3] = 
{
	U3CTaskU3Ed__2_t6054B9CF056BDC9A4E1A15DD7F3187A12F8927F8::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__2_t6054B9CF056BDC9A4E1A15DD7F3187A12F8927F8::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__2_t6054B9CF056BDC9A4E1A15DD7F3187A12F8927F8::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7941 = { sizeof (MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7941[1] = 
{
	MultiLookupLoaderTask_t59636CCC00F213A387F9851130F5691B44EA7B8A::get_offset_of__path_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7942 = { sizeof (U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7942[7] = 
{
	U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7::get_offset_of_U3CU3E4__this_2(),
	U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7::get_offset_of_U3CloadLocalDataTaskU3E5__2_3(),
	U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7::get_offset_of_U3CloadStreamingAssetTaskU3E5__3_4(),
	U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7::get_offset_of_U3CloadServerDataTaskU3E5__4_5(),
	U3CTaskU3Ed__2_t3D43A0CB6ECBD878CFD598A7FE68F390593927A7::get_offset_of_U3CloadDepotDataTaskU3E5__5_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7943 = { sizeof (ServerFileLoaderTask_t95314098EBA8E90891F1D8C5F19C0DC49D532F92), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7943[1] = 
{
	ServerFileLoaderTask_t95314098EBA8E90891F1D8C5F19C0DC49D532F92::get_offset_of__path_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7944 = { sizeof (U3CTaskU3Ed__2_tD77D3A24C3EF9C9E011921FB03E4D184C2784B46), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7944[4] = 
{
	U3CTaskU3Ed__2_tD77D3A24C3EF9C9E011921FB03E4D184C2784B46::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__2_tD77D3A24C3EF9C9E011921FB03E4D184C2784B46::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__2_tD77D3A24C3EF9C9E011921FB03E4D184C2784B46::get_offset_of_U3CU3E4__this_2(),
	U3CTaskU3Ed__2_tD77D3A24C3EF9C9E011921FB03E4D184C2784B46::get_offset_of_U3CwwwU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7945 = { sizeof (StreamingAssetFileLoaderTask_t766339DE3AEAB244DF39F9C963074DCCAEA2BA43), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7945[1] = 
{
	StreamingAssetFileLoaderTask_t766339DE3AEAB244DF39F9C963074DCCAEA2BA43::get_offset_of__path_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7946 = { sizeof (U3CTaskU3Ed__2_t996C5019828C38A607088ED0DBCDC342B7B3688C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7946[3] = 
{
	U3CTaskU3Ed__2_t996C5019828C38A607088ED0DBCDC342B7B3688C::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__2_t996C5019828C38A607088ED0DBCDC342B7B3688C::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__2_t996C5019828C38A607088ED0DBCDC342B7B3688C::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7947 = { sizeof (AnimatorHandler_tB8713610DD7F2D5A30CBBE4EC356E4FE1C1CD66E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7947[1] = 
{
	AnimatorHandler_tB8713610DD7F2D5A30CBBE4EC356E4FE1C1CD66E::get_offset_of__animator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7948 = { sizeof (DOTweenHandler_t6DF9AE6CF73A4B646F43FECC59BF28142A8E0939), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7949 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7950 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7951 = { sizeof (NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7951[5] = 
{
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F::get_offset_of_AnimationType_4(),
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F::get_offset_of_Intro_5(),
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F::get_offset_of_Outro_6(),
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F::get_offset_of_IntroFinishEvent_7(),
	NodeAnimator_t5E49B23D617EB48B77766FF9E73F76F43F57813F::get_offset_of_OutroFinishEvent_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7952 = { sizeof (IntroFinishEvent_t41434E95E4BB9D37CE53CCD6322D8B8D1DCD87EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7953 = { sizeof (OutroFinishEvent_t6379D6DE5405EBB1F4C2160A23FD9AF6EC019F38), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7954 = { sizeof (NodeHelper_tE40D988E7C5679FF18AE5191FBF524A9DBFEF945), -1, sizeof(NodeHelper_tE40D988E7C5679FF18AE5191FBF524A9DBFEF945_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7954[1] = 
{
	NodeHelper_tE40D988E7C5679FF18AE5191FBF524A9DBFEF945_StaticFields::get_offset_of_AnimationTypeToHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7955 = { sizeof (AnimationType_tBF4164D39BF991485B289847B900CB568CA6992B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7955[3] = 
{
	AnimationType_tBF4164D39BF991485B289847B900CB568CA6992B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7956 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7956[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7957 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7958 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7958[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7959 = { sizeof (NodeSystem_t28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7959[3] = 
{
	NodeSystem_t28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE::get_offset_of__activeNodes_0(),
	NodeSystem_t28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE::get_offset_of__nodeParent_1(),
	NodeSystem_t28EF08625C3EE4F84BAA3C7BA1660E70AD1394AE::get_offset_of__root_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7960 = { sizeof (U3CU3Ec__DisplayClass9_0_t4834D8551E6AD576D26116518514BBEBF63BFDCE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7960[1] = 
{
	U3CU3Ec__DisplayClass9_0_t4834D8551E6AD576D26116518514BBEBF63BFDCE::get_offset_of_node_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7961 = { sizeof (NodeCacheData_t7817FA13D174980D397D0B532F2C3CACE43C3D7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7961[3] = 
{
	NodeCacheData_t7817FA13D174980D397D0B532F2C3CACE43C3D7F::get_offset_of_Node_0(),
	NodeCacheData_t7817FA13D174980D397D0B532F2C3CACE43C3D7F::get_offset_of_Library_1(),
	NodeCacheData_t7817FA13D174980D397D0B532F2C3CACE43C3D7F::get_offset_of_Layer_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7962 = { sizeof (DownloadHandlerFile_t606AFD19457AD43034A4A76A17852055CF260D88), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7962[3] = 
{
	DownloadHandlerFile_t606AFD19457AD43034A4A76A17852055CF260D88::get_offset_of__contentLength_1(),
	DownloadHandlerFile_t606AFD19457AD43034A4A76A17852055CF260D88::get_offset_of__received_2(),
	DownloadHandlerFile_t606AFD19457AD43034A4A76A17852055CF260D88::get_offset_of__stream_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7963 = { sizeof (LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7963[4] = 
{
	LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552::get_offset_of__missingContent_7(),
	LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552::get_offset_of__destination_8(),
	LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552::get_offset_of__url_9(),
	LoadHashFileTask_t7696E228DD71017700D9DE906D61BA9676A81552::get_offset_of__availableContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7964 = { sizeof (U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7964[6] = 
{
	U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0::get_offset_of_U3CU3E4__this_2(),
	U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0::get_offset_of_U3CmultiLookupLoaderU3E5__2_3(),
	U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0::get_offset_of_U3CdownloadFileTaskU3E5__3_4(),
	U3CTaskU3Ed__5_tF4E74CFC6CE3AED1C5BCAD7C4DF18F81A4572DA0::get_offset_of_U3CmultiLookupLoaderNewU3E5__4_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7965 = { sizeof (MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7965[2] = 
{
	MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1::get_offset_of_Assets_0(),
	MissingAssetsGroup_t4A39A3CD5679D4D8E682BA729CA0E2C6DB334AA1::get_offset_of_RequiredSize_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7966 = { sizeof (PatchAssetData_tDB6643A7B2F28765976DE6C425B0F4A91B1BB180), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7966[3] = 
{
	PatchAssetData_tDB6643A7B2F28765976DE6C425B0F4A91B1BB180::get_offset_of_Hash_0(),
	PatchAssetData_tDB6643A7B2F28765976DE6C425B0F4A91B1BB180::get_offset_of_Size_1(),
	PatchAssetData_tDB6643A7B2F28765976DE6C425B0F4A91B1BB180::get_offset_of_Path_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7967 = { sizeof (MapModel_t73414FA47D931828C3029249838AA32F48E39C84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7967[1] = 
{
	MapModel_t73414FA47D931828C3029249838AA32F48E39C84::get_offset_of_Groups_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7968 = { sizeof (PatchingTask_tAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7968[3] = 
{
	PatchingTask_tAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD::get_offset_of__patchFile_7(),
	PatchingTask_tAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD::get_offset_of__patchingTask_8(),
	PatchingTask_tAE0B0BA2CD0E51857D6D77A9F445C7FC376B47AD::get_offset_of__serverUrl_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7969 = { sizeof (U3CTaskU3Ed__4_tE2B745D070540DDCC7B10373F94D421131774EF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7969[3] = 
{
	U3CTaskU3Ed__4_tE2B745D070540DDCC7B10373F94D421131774EF0::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__4_tE2B745D070540DDCC7B10373F94D421131774EF0::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__4_tE2B745D070540DDCC7B10373F94D421131774EF0::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7970 = { sizeof (PatchingUtils_t174B783D1A666D540042010C3848D704DBC1E8DF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7971 = { sizeof (GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7971[5] = 
{
	GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221::get_offset_of_logStatus_4(),
	GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221::get_offset_of_prefabLookup_5(),
	GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221::get_offset_of_instanceLookup_6(),
	GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221::get_offset_of_dirty_7(),
	GameObjectPool_tB195E5FFA2B13603B867994317CE6FE9B1122221::get_offset_of__container_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7972 = { sizeof (U3CU3Ec__DisplayClass8_0_tBAE365A26C3DEAF200733A94B49C1DE99A051A34), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7972[2] = 
{
	U3CU3Ec__DisplayClass8_0_tBAE365A26C3DEAF200733A94B49C1DE99A051A34::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass8_0_tBAE365A26C3DEAF200733A94B49C1DE99A051A34::get_offset_of_prefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7973 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7973[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7974 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7974[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7975 = { sizeof (TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7975[2] = 
{
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511::get_offset_of_FxSource_3(),
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511::get_offset_of_MusicSource_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7976 = { sizeof (U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7976[7] = 
{
	U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251::get_offset_of_U3CU3E1__state_0(),
	U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251::get_offset_of_U3CU3E2__current_1(),
	U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251::get_offset_of_U3CU3E4__this_2(),
	U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251::get_offset_of_targetVolume_3(),
	U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251::get_offset_of_duration_4(),
	U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251::get_offset_of_U3CcurrentTimeU3E5__2_5(),
	U3CStartFadeMusicU3Ed__12_tAC22FF2F55608187E484F4902347D30ACD206251::get_offset_of_U3CstartU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7977 = { sizeof (U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7977[8] = 
{
	U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB::get_offset_of_U3CU3E1__state_0(),
	U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB::get_offset_of_U3CU3E2__current_1(),
	U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB::get_offset_of_U3CU3E4__this_2(),
	U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB::get_offset_of_duration_3(),
	U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB::get_offset_of_clip_4(),
	U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB::get_offset_of_U3CcurrentTimeU3E5__2_5(),
	U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB::get_offset_of_U3CstartU3E5__3_6(),
	U3CFadeMusicU3Ed__13_t386F09790D7AEC7D217DD9CE193C9FFDD02757FB::get_offset_of_U3CdU3E5__4_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7978 = { sizeof (ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7978[4] = 
{
	ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717::get_offset_of__parallelTaskCollection_7(),
	ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717::get_offset_of__onProgressEvent_8(),
	ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717::get_offset_of__childTaskCounter_9(),
	ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717::get_offset_of__childTaskFinished_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7979 = { sizeof (U3CTaskU3Ed__9_t2CED0FE067EBE3426586277B00A03E2C506FEC94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7979[3] = 
{
	U3CTaskU3Ed__9_t2CED0FE067EBE3426586277B00A03E2C506FEC94::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__9_t2CED0FE067EBE3426586277B00A03E2C506FEC94::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__9_t2CED0FE067EBE3426586277B00A03E2C506FEC94::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7980 = { sizeof (SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7980[4] = 
{
	SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A::get_offset_of__serialTask_7(),
	SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A::get_offset_of__onProgressEvent_8(),
	SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A::get_offset_of__childTaskCounter_9(),
	SerialTask_tF1A918D9502AC12F7FFC785540520BB122C5B04A::get_offset_of__childTaskFinished_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7981 = { sizeof (U3CTaskU3Ed__6_t4A3DA8D7E927EFFF4F0F1F28094A1D78CAD24ADF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7981[3] = 
{
	U3CTaskU3Ed__6_t4A3DA8D7E927EFFF4F0F1F28094A1D78CAD24ADF::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__6_t4A3DA8D7E927EFFF4F0F1F28094A1D78CAD24ADF::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__6_t4A3DA8D7E927EFFF4F0F1F28094A1D78CAD24ADF::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7982 = { sizeof (OnProgress_tADC03F8ED177DE6DDF9FFFAAE7FE52A93CB78CF4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7983 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7984 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7985 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7986 = { sizeof (CoroutineRunner_t55D9FE1CD110034DE5497E8A93A4F8E55AAE7097), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7987 = { sizeof (MultiThreadRunner_t7E19EA5A8B0723D35CF76C72ADD4ED3701E10D40), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7988 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7988[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7989 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7990 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7990[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7991 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7991[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7992 = { sizeof (RetryNode_tA2D5659E521764F41C793319C5DD5D9E8F3EE04C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7993 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7993[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7994 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7994[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7995 = { sizeof (TAlert_t04CBB10626DE07CD76206287C32FA769FB757063), -1, sizeof(TAlert_t04CBB10626DE07CD76206287C32FA769FB757063_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7995[4] = 
{
	TAlert_t04CBB10626DE07CD76206287C32FA769FB757063_StaticFields::get_offset_of__retryNode_0(),
	TAlert_t04CBB10626DE07CD76206287C32FA769FB757063_StaticFields::get_offset_of__nodeType_1(),
	TAlert_t04CBB10626DE07CD76206287C32FA769FB757063_StaticFields::get_offset_of__library_2(),
	TAlert_t04CBB10626DE07CD76206287C32FA769FB757063_StaticFields::get_offset_of__data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7996 = { sizeof (TWaitForEvent_t48111D8D7D335240707B5CB2F1EA686871299A05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7996[1] = 
{
	TWaitForEvent_t48111D8D7D335240707B5CB2F1EA686871299A05::get_offset_of__isInvoked_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7997 = { sizeof (TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7997[3] = 
{
	TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19::get_offset_of__gameObjectPool_4(),
	TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19::get_offset_of_ContentParent_5(),
	TDynamicList_tF4B70F991EE13A3CB137902A92EC1A729C06DB19::get_offset_of__items_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7998 = { sizeof (TDynamicListData_t634C7C07588B155397EB9DB5A3825AC657E12369), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7998[1] = 
{
	TDynamicListData_t634C7C07588B155397EB9DB5A3825AC657E12369::get_offset_of_Prefab_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7999 = { sizeof (TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7999[3] = 
{
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292::get_offset_of_ItemsPrefab_4(),
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292::get_offset_of_ContentParent_5(),
	TList_tB67CEA10085B72B7C69ACE7FCF01FCC38E6EF292::get_offset_of__items_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
