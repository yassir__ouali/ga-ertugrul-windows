﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Org.BouncyCastle.Asn1.Asn1Set
struct Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7;
// Org.BouncyCastle.Asn1.X509.BasicConstraints
struct BasicConstraints_t199BCC0E7A6950B11A098A0422FDEB8E73E2380E;
// Org.BouncyCastle.Asn1.X509.X509CertificateStructure
struct X509CertificateStructure_tEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1;
// Org.BouncyCastle.Crypto.Prng.IRandomGenerator
struct IRandomGenerator_tEFF6415248F4D37A24017AB5D51691A43740A6CB;
// Org.BouncyCastle.Utilities.Encoders.IEncoder
struct IEncoder_t083765E943BBC1EB8C76C2CE959AF7D90766707A;
// Org.BouncyCastle.Utilities.Zlib.Adler32
struct Adler32_t820E77D81E138AD13711AC3662B7DC9EDD1C9F97;
// Org.BouncyCastle.Utilities.Zlib.Deflate
struct Deflate_tA360BF189910682C62915B83E8F061F816C886E5;
// Org.BouncyCastle.Utilities.Zlib.Deflate/Config[]
struct ConfigU5BU5D_t697F913CAB0DC9EF0657413DB5866E4C38F6C032;
// Org.BouncyCastle.Utilities.Zlib.InfBlocks
struct InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4;
// Org.BouncyCastle.Utilities.Zlib.InfCodes
struct InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743;
// Org.BouncyCastle.Utilities.Zlib.InfTree
struct InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2;
// Org.BouncyCastle.Utilities.Zlib.Inflate
struct Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180;
// Org.BouncyCastle.Utilities.Zlib.StaticTree
struct StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5;
// Org.BouncyCastle.Utilities.Zlib.Tree
struct Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D;
// Org.BouncyCastle.Utilities.Zlib.ZStream
struct ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB;
// Org.BouncyCastle.X509.PemParser
struct PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerable
struct IEnumerable_tD74549CEA1AA48E768382B94FEACBB07E2E3FA2C;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Globalization.CompareInfo
struct CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Type
struct Type_t;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_TBE4A2C03D239132B99D8F2C5938A2EC7220D741C_H
#define U3CMODULEU3E_TBE4A2C03D239132B99D8F2C5938A2EC7220D741C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tBE4A2C03D239132B99D8F2C5938A2EC7220D741C 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TBE4A2C03D239132B99D8F2C5938A2EC7220D741C_H
#ifndef U3CMODULEU3E_T93AF16331CBD41D5C3147B00C1826A972C35DD7D_H
#define U3CMODULEU3E_T93AF16331CBD41D5C3147B00C1826A972C35DD7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t93AF16331CBD41D5C3147B00C1826A972C35DD7D 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T93AF16331CBD41D5C3147B00C1826A972C35DD7D_H
#ifndef U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#define U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TB308A2384DEB86F8845A4E61970976B8944B5DC4_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef NAT192_TAE48D7B0E4353214D3C4BBD54850ED08F4EC2628_H
#define NAT192_TAE48D7B0E4353214D3C4BBD54850ED08F4EC2628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Nat192
struct  Nat192_tAE48D7B0E4353214D3C4BBD54850ED08F4EC2628  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT192_TAE48D7B0E4353214D3C4BBD54850ED08F4EC2628_H
#ifndef NAT224_TA44CD0B1DA6238650B0A01CF4EDCD876AA9CAE9F_H
#define NAT224_TA44CD0B1DA6238650B0A01CF4EDCD876AA9CAE9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Nat224
struct  Nat224_tA44CD0B1DA6238650B0A01CF4EDCD876AA9CAE9F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT224_TA44CD0B1DA6238650B0A01CF4EDCD876AA9CAE9F_H
#ifndef NAT256_TAF0A99BB5510054032153DB7A0E80C5E435F4252_H
#define NAT256_TAF0A99BB5510054032153DB7A0E80C5E435F4252_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Nat256
struct  Nat256_tAF0A99BB5510054032153DB7A0E80C5E435F4252  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT256_TAF0A99BB5510054032153DB7A0E80C5E435F4252_H
#ifndef NAT320_T0D3A378BFCD60326B30640B307530B994EA2316E_H
#define NAT320_T0D3A378BFCD60326B30640B307530B994EA2316E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Nat320
struct  Nat320_t0D3A378BFCD60326B30640B307530B994EA2316E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT320_T0D3A378BFCD60326B30640B307530B994EA2316E_H
#ifndef NAT384_TC26A95526871DA324E89D5057FC751D2941B3020_H
#define NAT384_TC26A95526871DA324E89D5057FC751D2941B3020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Nat384
struct  Nat384_tC26A95526871DA324E89D5057FC751D2941B3020  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT384_TC26A95526871DA324E89D5057FC751D2941B3020_H
#ifndef NAT448_T74497AC85F8C2E5987378EB4D68E63902F6B2CF0_H
#define NAT448_T74497AC85F8C2E5987378EB4D68E63902F6B2CF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Nat448
struct  Nat448_t74497AC85F8C2E5987378EB4D68E63902F6B2CF0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT448_T74497AC85F8C2E5987378EB4D68E63902F6B2CF0_H
#ifndef NAT512_T877FE82CD2D8E4D386B601D14807DBC65073E860_H
#define NAT512_T877FE82CD2D8E4D386B601D14807DBC65073E860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Nat512
struct  Nat512_t877FE82CD2D8E4D386B601D14807DBC65073E860  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT512_T877FE82CD2D8E4D386B601D14807DBC65073E860_H
#ifndef NAT576_TA562984E4FFA99F0EDF81A437AE8B8FA8F5358CF_H
#define NAT576_TA562984E4FFA99F0EDF81A437AE8B8FA8F5358CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.Raw.Nat576
struct  Nat576_tA562984E4FFA99F0EDF81A437AE8B8FA8F5358CF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAT576_TA562984E4FFA99F0EDF81A437AE8B8FA8F5358CF_H
#ifndef DIGESTUTILITIES_T55BF65B32A85C65E66B412315C65A9CB4390CF56_H
#define DIGESTUTILITIES_T55BF65B32A85C65E66B412315C65A9CB4390CF56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Security.DigestUtilities
struct  DigestUtilities_t55BF65B32A85C65E66B412315C65A9CB4390CF56  : public RuntimeObject
{
public:

public:
};

struct DigestUtilities_t55BF65B32A85C65E66B412315C65A9CB4390CF56_StaticFields
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Security.DigestUtilities::algorithms
	RuntimeObject* ___algorithms_0;
	// System.Collections.IDictionary Org.BouncyCastle.Security.DigestUtilities::oids
	RuntimeObject* ___oids_1;

public:
	inline static int32_t get_offset_of_algorithms_0() { return static_cast<int32_t>(offsetof(DigestUtilities_t55BF65B32A85C65E66B412315C65A9CB4390CF56_StaticFields, ___algorithms_0)); }
	inline RuntimeObject* get_algorithms_0() const { return ___algorithms_0; }
	inline RuntimeObject** get_address_of_algorithms_0() { return &___algorithms_0; }
	inline void set_algorithms_0(RuntimeObject* value)
	{
		___algorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_0), value);
	}

	inline static int32_t get_offset_of_oids_1() { return static_cast<int32_t>(offsetof(DigestUtilities_t55BF65B32A85C65E66B412315C65A9CB4390CF56_StaticFields, ___oids_1)); }
	inline RuntimeObject* get_oids_1() const { return ___oids_1; }
	inline RuntimeObject** get_address_of_oids_1() { return &___oids_1; }
	inline void set_oids_1(RuntimeObject* value)
	{
		___oids_1 = value;
		Il2CppCodeGenWriteBarrier((&___oids_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTUTILITIES_T55BF65B32A85C65E66B412315C65A9CB4390CF56_H
#ifndef MACUTILITIES_TD27296E701E6EE768F49104A5FA7E42EE372AFFA_H
#define MACUTILITIES_TD27296E701E6EE768F49104A5FA7E42EE372AFFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Security.MacUtilities
struct  MacUtilities_tD27296E701E6EE768F49104A5FA7E42EE372AFFA  : public RuntimeObject
{
public:

public:
};

struct MacUtilities_tD27296E701E6EE768F49104A5FA7E42EE372AFFA_StaticFields
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Security.MacUtilities::algorithms
	RuntimeObject* ___algorithms_0;

public:
	inline static int32_t get_offset_of_algorithms_0() { return static_cast<int32_t>(offsetof(MacUtilities_tD27296E701E6EE768F49104A5FA7E42EE372AFFA_StaticFields, ___algorithms_0)); }
	inline RuntimeObject* get_algorithms_0() const { return ___algorithms_0; }
	inline RuntimeObject** get_address_of_algorithms_0() { return &___algorithms_0; }
	inline void set_algorithms_0(RuntimeObject* value)
	{
		___algorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MACUTILITIES_TD27296E701E6EE768F49104A5FA7E42EE372AFFA_H
#ifndef PUBLICKEYFACTORY_TC68C0AE99ACFD7B1242CE490A66330A2F8C35172_H
#define PUBLICKEYFACTORY_TC68C0AE99ACFD7B1242CE490A66330A2F8C35172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Security.PublicKeyFactory
struct  PublicKeyFactory_tC68C0AE99ACFD7B1242CE490A66330A2F8C35172  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUBLICKEYFACTORY_TC68C0AE99ACFD7B1242CE490A66330A2F8C35172_H
#ifndef SIGNERUTILITIES_T316BEDC8F9DED4FA92BB8447C5DABF873B9F7D28_H
#define SIGNERUTILITIES_T316BEDC8F9DED4FA92BB8447C5DABF873B9F7D28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Security.SignerUtilities
struct  SignerUtilities_t316BEDC8F9DED4FA92BB8447C5DABF873B9F7D28  : public RuntimeObject
{
public:

public:
};

struct SignerUtilities_t316BEDC8F9DED4FA92BB8447C5DABF873B9F7D28_StaticFields
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Security.SignerUtilities::algorithms
	RuntimeObject* ___algorithms_0;
	// System.Collections.IDictionary Org.BouncyCastle.Security.SignerUtilities::oids
	RuntimeObject* ___oids_1;

public:
	inline static int32_t get_offset_of_algorithms_0() { return static_cast<int32_t>(offsetof(SignerUtilities_t316BEDC8F9DED4FA92BB8447C5DABF873B9F7D28_StaticFields, ___algorithms_0)); }
	inline RuntimeObject* get_algorithms_0() const { return ___algorithms_0; }
	inline RuntimeObject** get_address_of_algorithms_0() { return &___algorithms_0; }
	inline void set_algorithms_0(RuntimeObject* value)
	{
		___algorithms_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_0), value);
	}

	inline static int32_t get_offset_of_oids_1() { return static_cast<int32_t>(offsetof(SignerUtilities_t316BEDC8F9DED4FA92BB8447C5DABF873B9F7D28_StaticFields, ___oids_1)); }
	inline RuntimeObject* get_oids_1() const { return ___oids_1; }
	inline RuntimeObject** get_address_of_oids_1() { return &___oids_1; }
	inline void set_oids_1(RuntimeObject* value)
	{
		___oids_1 = value;
		Il2CppCodeGenWriteBarrier((&___oids_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERUTILITIES_T316BEDC8F9DED4FA92BB8447C5DABF873B9F7D28_H
#ifndef ARRAYS_T63735DA71DBD670D3F511132B0BBE4DE7C525AAF_H
#define ARRAYS_T63735DA71DBD670D3F511132B0BBE4DE7C525AAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Arrays
struct  Arrays_t63735DA71DBD670D3F511132B0BBE4DE7C525AAF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYS_T63735DA71DBD670D3F511132B0BBE4DE7C525AAF_H
#ifndef BIGINTEGERS_TAF2E32490FD837FC4E6890B161025FCF3834B96A_H
#define BIGINTEGERS_TAF2E32490FD837FC4E6890B161025FCF3834B96A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.BigIntegers
struct  BigIntegers_tAF2E32490FD837FC4E6890B161025FCF3834B96A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGINTEGERS_TAF2E32490FD837FC4E6890B161025FCF3834B96A_H
#ifndef COLLECTIONUTILITIES_T73DD2B74FBF3824DB87A35CE64B96D45E7D68420_H
#define COLLECTIONUTILITIES_T73DD2B74FBF3824DB87A35CE64B96D45E7D68420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Collections.CollectionUtilities
struct  CollectionUtilities_t73DD2B74FBF3824DB87A35CE64B96D45E7D68420  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONUTILITIES_T73DD2B74FBF3824DB87A35CE64B96D45E7D68420_H
#ifndef ENUMERABLEPROXY_TB7FC60E6BF21CA2EE047E3FAA1E411E1DE91C84E_H
#define ENUMERABLEPROXY_TB7FC60E6BF21CA2EE047E3FAA1E411E1DE91C84E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Collections.EnumerableProxy
struct  EnumerableProxy_tB7FC60E6BF21CA2EE047E3FAA1E411E1DE91C84E  : public RuntimeObject
{
public:
	// System.Collections.IEnumerable Org.BouncyCastle.Utilities.Collections.EnumerableProxy::inner
	RuntimeObject* ___inner_0;

public:
	inline static int32_t get_offset_of_inner_0() { return static_cast<int32_t>(offsetof(EnumerableProxy_tB7FC60E6BF21CA2EE047E3FAA1E411E1DE91C84E, ___inner_0)); }
	inline RuntimeObject* get_inner_0() const { return ___inner_0; }
	inline RuntimeObject** get_address_of_inner_0() { return &___inner_0; }
	inline void set_inner_0(RuntimeObject* value)
	{
		___inner_0 = value;
		Il2CppCodeGenWriteBarrier((&___inner_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERABLEPROXY_TB7FC60E6BF21CA2EE047E3FAA1E411E1DE91C84E_H
#ifndef BASE64_TF9625F882314C6CE3CFF6C14CFAC9974D9ACBACF_H
#define BASE64_TF9625F882314C6CE3CFF6C14CFAC9974D9ACBACF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Encoders.Base64
struct  Base64_tF9625F882314C6CE3CFF6C14CFAC9974D9ACBACF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64_TF9625F882314C6CE3CFF6C14CFAC9974D9ACBACF_H
#ifndef HEX_TEF946DC3EA341835053763B600C2B26C0E375EAD_H
#define HEX_TEF946DC3EA341835053763B600C2B26C0E375EAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Encoders.Hex
struct  Hex_tEF946DC3EA341835053763B600C2B26C0E375EAD  : public RuntimeObject
{
public:

public:
};

struct Hex_tEF946DC3EA341835053763B600C2B26C0E375EAD_StaticFields
{
public:
	// Org.BouncyCastle.Utilities.Encoders.IEncoder Org.BouncyCastle.Utilities.Encoders.Hex::encoder
	RuntimeObject* ___encoder_0;

public:
	inline static int32_t get_offset_of_encoder_0() { return static_cast<int32_t>(offsetof(Hex_tEF946DC3EA341835053763B600C2B26C0E375EAD_StaticFields, ___encoder_0)); }
	inline RuntimeObject* get_encoder_0() const { return ___encoder_0; }
	inline RuntimeObject** get_address_of_encoder_0() { return &___encoder_0; }
	inline void set_encoder_0(RuntimeObject* value)
	{
		___encoder_0 = value;
		Il2CppCodeGenWriteBarrier((&___encoder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEX_TEF946DC3EA341835053763B600C2B26C0E375EAD_H
#ifndef HEXENCODER_T01DC94DCC847A783F93BA4866482C2E22E946192_H
#define HEXENCODER_T01DC94DCC847A783F93BA4866482C2E22E946192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Encoders.HexEncoder
struct  HexEncoder_t01DC94DCC847A783F93BA4866482C2E22E946192  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Utilities.Encoders.HexEncoder::encodingTable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___encodingTable_0;
	// System.Byte[] Org.BouncyCastle.Utilities.Encoders.HexEncoder::decodingTable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___decodingTable_1;

public:
	inline static int32_t get_offset_of_encodingTable_0() { return static_cast<int32_t>(offsetof(HexEncoder_t01DC94DCC847A783F93BA4866482C2E22E946192, ___encodingTable_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_encodingTable_0() const { return ___encodingTable_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_encodingTable_0() { return &___encodingTable_0; }
	inline void set_encodingTable_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___encodingTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___encodingTable_0), value);
	}

	inline static int32_t get_offset_of_decodingTable_1() { return static_cast<int32_t>(offsetof(HexEncoder_t01DC94DCC847A783F93BA4866482C2E22E946192, ___decodingTable_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_decodingTable_1() const { return ___decodingTable_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_decodingTable_1() { return &___decodingTable_1; }
	inline void set_decodingTable_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___decodingTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___decodingTable_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEXENCODER_T01DC94DCC847A783F93BA4866482C2E22E946192_H
#ifndef ENUMS_TA03EC3F31800DE4E15A42CEF7F64F6859124C392_H
#define ENUMS_TA03EC3F31800DE4E15A42CEF7F64F6859124C392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Enums
struct  Enums_tA03EC3F31800DE4E15A42CEF7F64F6859124C392  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMS_TA03EC3F31800DE4E15A42CEF7F64F6859124C392_H
#ifndef STREAMS_TFFA2017D63B0F62D5084C134EB05893F950E7851_H
#define STREAMS_TFFA2017D63B0F62D5084C134EB05893F950E7851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.IO.Streams
struct  Streams_tFFA2017D63B0F62D5084C134EB05893F950E7851  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMS_TFFA2017D63B0F62D5084C134EB05893F950E7851_H
#ifndef INTEGERS_T7C9F57668BA0D3F43F0516CC405C69B05577ADD0_H
#define INTEGERS_T7C9F57668BA0D3F43F0516CC405C69B05577ADD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Integers
struct  Integers_t7C9F57668BA0D3F43F0516CC405C69B05577ADD0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEGERS_T7C9F57668BA0D3F43F0516CC405C69B05577ADD0_H
#ifndef PLATFORM_T0DACA1EE9C17BB52DA4EECD7506573F6D4FEC7E4_H
#define PLATFORM_T0DACA1EE9C17BB52DA4EECD7506573F6D4FEC7E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Platform
struct  Platform_t0DACA1EE9C17BB52DA4EECD7506573F6D4FEC7E4  : public RuntimeObject
{
public:

public:
};

struct Platform_t0DACA1EE9C17BB52DA4EECD7506573F6D4FEC7E4_StaticFields
{
public:
	// System.Globalization.CompareInfo Org.BouncyCastle.Utilities.Platform::InvariantCompareInfo
	CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * ___InvariantCompareInfo_0;
	// System.String Org.BouncyCastle.Utilities.Platform::NewLine
	String_t* ___NewLine_1;

public:
	inline static int32_t get_offset_of_InvariantCompareInfo_0() { return static_cast<int32_t>(offsetof(Platform_t0DACA1EE9C17BB52DA4EECD7506573F6D4FEC7E4_StaticFields, ___InvariantCompareInfo_0)); }
	inline CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * get_InvariantCompareInfo_0() const { return ___InvariantCompareInfo_0; }
	inline CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 ** get_address_of_InvariantCompareInfo_0() { return &___InvariantCompareInfo_0; }
	inline void set_InvariantCompareInfo_0(CompareInfo_tB9A071DBC11AC00AF2EA2066D0C2AE1DCB1865D1 * value)
	{
		___InvariantCompareInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___InvariantCompareInfo_0), value);
	}

	inline static int32_t get_offset_of_NewLine_1() { return static_cast<int32_t>(offsetof(Platform_t0DACA1EE9C17BB52DA4EECD7506573F6D4FEC7E4_StaticFields, ___NewLine_1)); }
	inline String_t* get_NewLine_1() const { return ___NewLine_1; }
	inline String_t** get_address_of_NewLine_1() { return &___NewLine_1; }
	inline void set_NewLine_1(String_t* value)
	{
		___NewLine_1 = value;
		Il2CppCodeGenWriteBarrier((&___NewLine_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORM_T0DACA1EE9C17BB52DA4EECD7506573F6D4FEC7E4_H
#ifndef STRINGS_T28AD15CF2250A7FBB9E5140D3D3740C67F4C55F4_H
#define STRINGS_T28AD15CF2250A7FBB9E5140D3D3740C67F4C55F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Strings
struct  Strings_t28AD15CF2250A7FBB9E5140D3D3740C67F4C55F4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGS_T28AD15CF2250A7FBB9E5140D3D3740C67F4C55F4_H
#ifndef TIMES_TD93A13784662FC42F65483C22AAB50A79154E925_H
#define TIMES_TD93A13784662FC42F65483C22AAB50A79154E925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Times
struct  Times_tD93A13784662FC42F65483C22AAB50A79154E925  : public RuntimeObject
{
public:

public:
};

struct Times_tD93A13784662FC42F65483C22AAB50A79154E925_StaticFields
{
public:
	// System.Int64 Org.BouncyCastle.Utilities.Times::NanosecondsPerTick
	int64_t ___NanosecondsPerTick_0;

public:
	inline static int32_t get_offset_of_NanosecondsPerTick_0() { return static_cast<int32_t>(offsetof(Times_tD93A13784662FC42F65483C22AAB50A79154E925_StaticFields, ___NanosecondsPerTick_0)); }
	inline int64_t get_NanosecondsPerTick_0() const { return ___NanosecondsPerTick_0; }
	inline int64_t* get_address_of_NanosecondsPerTick_0() { return &___NanosecondsPerTick_0; }
	inline void set_NanosecondsPerTick_0(int64_t value)
	{
		___NanosecondsPerTick_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMES_TD93A13784662FC42F65483C22AAB50A79154E925_H
#ifndef ADLER32_T820E77D81E138AD13711AC3662B7DC9EDD1C9F97_H
#define ADLER32_T820E77D81E138AD13711AC3662B7DC9EDD1C9F97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Zlib.Adler32
struct  Adler32_t820E77D81E138AD13711AC3662B7DC9EDD1C9F97  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADLER32_T820E77D81E138AD13711AC3662B7DC9EDD1C9F97_H
#ifndef DEFLATE_TA360BF189910682C62915B83E8F061F816C886E5_H
#define DEFLATE_TA360BF189910682C62915B83E8F061F816C886E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Zlib.Deflate
struct  Deflate_tA360BF189910682C62915B83E8F061F816C886E5  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Utilities.Zlib.ZStream Org.BouncyCastle.Utilities.Zlib.Deflate::strm
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * ___strm_2;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::status
	int32_t ___status_3;
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.Deflate::pending_buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pending_buf_4;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::pending_buf_size
	int32_t ___pending_buf_size_5;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::pending_out
	int32_t ___pending_out_6;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::pending
	int32_t ___pending_7;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::noheader
	int32_t ___noheader_8;
	// System.Byte Org.BouncyCastle.Utilities.Zlib.Deflate::data_type
	uint8_t ___data_type_9;
	// System.Byte Org.BouncyCastle.Utilities.Zlib.Deflate::method
	uint8_t ___method_10;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::last_flush
	int32_t ___last_flush_11;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::w_size
	int32_t ___w_size_12;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::w_bits
	int32_t ___w_bits_13;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::w_mask
	int32_t ___w_mask_14;
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.Deflate::window
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___window_15;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::window_size
	int32_t ___window_size_16;
	// System.Int16[] Org.BouncyCastle.Utilities.Zlib.Deflate::prev
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___prev_17;
	// System.Int16[] Org.BouncyCastle.Utilities.Zlib.Deflate::head
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___head_18;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::ins_h
	int32_t ___ins_h_19;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::hash_size
	int32_t ___hash_size_20;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::hash_bits
	int32_t ___hash_bits_21;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::hash_mask
	int32_t ___hash_mask_22;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::hash_shift
	int32_t ___hash_shift_23;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::block_start
	int32_t ___block_start_24;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::match_length
	int32_t ___match_length_25;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::prev_match
	int32_t ___prev_match_26;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::match_available
	int32_t ___match_available_27;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::strstart
	int32_t ___strstart_28;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::match_start
	int32_t ___match_start_29;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::lookahead
	int32_t ___lookahead_30;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::prev_length
	int32_t ___prev_length_31;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::max_chain_length
	int32_t ___max_chain_length_32;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::max_lazy_match
	int32_t ___max_lazy_match_33;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::level
	int32_t ___level_34;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::strategy
	int32_t ___strategy_35;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::good_match
	int32_t ___good_match_36;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::nice_match
	int32_t ___nice_match_37;
	// System.Int16[] Org.BouncyCastle.Utilities.Zlib.Deflate::dyn_ltree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___dyn_ltree_38;
	// System.Int16[] Org.BouncyCastle.Utilities.Zlib.Deflate::dyn_dtree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___dyn_dtree_39;
	// System.Int16[] Org.BouncyCastle.Utilities.Zlib.Deflate::bl_tree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___bl_tree_40;
	// Org.BouncyCastle.Utilities.Zlib.Tree Org.BouncyCastle.Utilities.Zlib.Deflate::l_desc
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D * ___l_desc_41;
	// Org.BouncyCastle.Utilities.Zlib.Tree Org.BouncyCastle.Utilities.Zlib.Deflate::d_desc
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D * ___d_desc_42;
	// Org.BouncyCastle.Utilities.Zlib.Tree Org.BouncyCastle.Utilities.Zlib.Deflate::bl_desc
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D * ___bl_desc_43;
	// System.Int16[] Org.BouncyCastle.Utilities.Zlib.Deflate::bl_count
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___bl_count_44;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.Deflate::heap
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___heap_45;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::heap_len
	int32_t ___heap_len_46;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::heap_max
	int32_t ___heap_max_47;
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.Deflate::depth
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___depth_48;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::l_buf
	int32_t ___l_buf_49;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::lit_bufsize
	int32_t ___lit_bufsize_50;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::last_lit
	int32_t ___last_lit_51;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::d_buf
	int32_t ___d_buf_52;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::opt_len
	int32_t ___opt_len_53;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::static_len
	int32_t ___static_len_54;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::matches
	int32_t ___matches_55;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::last_eob_len
	int32_t ___last_eob_len_56;
	// System.UInt32 Org.BouncyCastle.Utilities.Zlib.Deflate::bi_buf
	uint32_t ___bi_buf_57;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate::bi_valid
	int32_t ___bi_valid_58;

public:
	inline static int32_t get_offset_of_strm_2() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___strm_2)); }
	inline ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * get_strm_2() const { return ___strm_2; }
	inline ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB ** get_address_of_strm_2() { return &___strm_2; }
	inline void set_strm_2(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * value)
	{
		___strm_2 = value;
		Il2CppCodeGenWriteBarrier((&___strm_2), value);
	}

	inline static int32_t get_offset_of_status_3() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___status_3)); }
	inline int32_t get_status_3() const { return ___status_3; }
	inline int32_t* get_address_of_status_3() { return &___status_3; }
	inline void set_status_3(int32_t value)
	{
		___status_3 = value;
	}

	inline static int32_t get_offset_of_pending_buf_4() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___pending_buf_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_pending_buf_4() const { return ___pending_buf_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_pending_buf_4() { return &___pending_buf_4; }
	inline void set_pending_buf_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___pending_buf_4 = value;
		Il2CppCodeGenWriteBarrier((&___pending_buf_4), value);
	}

	inline static int32_t get_offset_of_pending_buf_size_5() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___pending_buf_size_5)); }
	inline int32_t get_pending_buf_size_5() const { return ___pending_buf_size_5; }
	inline int32_t* get_address_of_pending_buf_size_5() { return &___pending_buf_size_5; }
	inline void set_pending_buf_size_5(int32_t value)
	{
		___pending_buf_size_5 = value;
	}

	inline static int32_t get_offset_of_pending_out_6() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___pending_out_6)); }
	inline int32_t get_pending_out_6() const { return ___pending_out_6; }
	inline int32_t* get_address_of_pending_out_6() { return &___pending_out_6; }
	inline void set_pending_out_6(int32_t value)
	{
		___pending_out_6 = value;
	}

	inline static int32_t get_offset_of_pending_7() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___pending_7)); }
	inline int32_t get_pending_7() const { return ___pending_7; }
	inline int32_t* get_address_of_pending_7() { return &___pending_7; }
	inline void set_pending_7(int32_t value)
	{
		___pending_7 = value;
	}

	inline static int32_t get_offset_of_noheader_8() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___noheader_8)); }
	inline int32_t get_noheader_8() const { return ___noheader_8; }
	inline int32_t* get_address_of_noheader_8() { return &___noheader_8; }
	inline void set_noheader_8(int32_t value)
	{
		___noheader_8 = value;
	}

	inline static int32_t get_offset_of_data_type_9() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___data_type_9)); }
	inline uint8_t get_data_type_9() const { return ___data_type_9; }
	inline uint8_t* get_address_of_data_type_9() { return &___data_type_9; }
	inline void set_data_type_9(uint8_t value)
	{
		___data_type_9 = value;
	}

	inline static int32_t get_offset_of_method_10() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___method_10)); }
	inline uint8_t get_method_10() const { return ___method_10; }
	inline uint8_t* get_address_of_method_10() { return &___method_10; }
	inline void set_method_10(uint8_t value)
	{
		___method_10 = value;
	}

	inline static int32_t get_offset_of_last_flush_11() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___last_flush_11)); }
	inline int32_t get_last_flush_11() const { return ___last_flush_11; }
	inline int32_t* get_address_of_last_flush_11() { return &___last_flush_11; }
	inline void set_last_flush_11(int32_t value)
	{
		___last_flush_11 = value;
	}

	inline static int32_t get_offset_of_w_size_12() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___w_size_12)); }
	inline int32_t get_w_size_12() const { return ___w_size_12; }
	inline int32_t* get_address_of_w_size_12() { return &___w_size_12; }
	inline void set_w_size_12(int32_t value)
	{
		___w_size_12 = value;
	}

	inline static int32_t get_offset_of_w_bits_13() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___w_bits_13)); }
	inline int32_t get_w_bits_13() const { return ___w_bits_13; }
	inline int32_t* get_address_of_w_bits_13() { return &___w_bits_13; }
	inline void set_w_bits_13(int32_t value)
	{
		___w_bits_13 = value;
	}

	inline static int32_t get_offset_of_w_mask_14() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___w_mask_14)); }
	inline int32_t get_w_mask_14() const { return ___w_mask_14; }
	inline int32_t* get_address_of_w_mask_14() { return &___w_mask_14; }
	inline void set_w_mask_14(int32_t value)
	{
		___w_mask_14 = value;
	}

	inline static int32_t get_offset_of_window_15() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___window_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_window_15() const { return ___window_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_window_15() { return &___window_15; }
	inline void set_window_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___window_15 = value;
		Il2CppCodeGenWriteBarrier((&___window_15), value);
	}

	inline static int32_t get_offset_of_window_size_16() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___window_size_16)); }
	inline int32_t get_window_size_16() const { return ___window_size_16; }
	inline int32_t* get_address_of_window_size_16() { return &___window_size_16; }
	inline void set_window_size_16(int32_t value)
	{
		___window_size_16 = value;
	}

	inline static int32_t get_offset_of_prev_17() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___prev_17)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_prev_17() const { return ___prev_17; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_prev_17() { return &___prev_17; }
	inline void set_prev_17(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___prev_17 = value;
		Il2CppCodeGenWriteBarrier((&___prev_17), value);
	}

	inline static int32_t get_offset_of_head_18() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___head_18)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_head_18() const { return ___head_18; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_head_18() { return &___head_18; }
	inline void set_head_18(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___head_18 = value;
		Il2CppCodeGenWriteBarrier((&___head_18), value);
	}

	inline static int32_t get_offset_of_ins_h_19() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___ins_h_19)); }
	inline int32_t get_ins_h_19() const { return ___ins_h_19; }
	inline int32_t* get_address_of_ins_h_19() { return &___ins_h_19; }
	inline void set_ins_h_19(int32_t value)
	{
		___ins_h_19 = value;
	}

	inline static int32_t get_offset_of_hash_size_20() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___hash_size_20)); }
	inline int32_t get_hash_size_20() const { return ___hash_size_20; }
	inline int32_t* get_address_of_hash_size_20() { return &___hash_size_20; }
	inline void set_hash_size_20(int32_t value)
	{
		___hash_size_20 = value;
	}

	inline static int32_t get_offset_of_hash_bits_21() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___hash_bits_21)); }
	inline int32_t get_hash_bits_21() const { return ___hash_bits_21; }
	inline int32_t* get_address_of_hash_bits_21() { return &___hash_bits_21; }
	inline void set_hash_bits_21(int32_t value)
	{
		___hash_bits_21 = value;
	}

	inline static int32_t get_offset_of_hash_mask_22() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___hash_mask_22)); }
	inline int32_t get_hash_mask_22() const { return ___hash_mask_22; }
	inline int32_t* get_address_of_hash_mask_22() { return &___hash_mask_22; }
	inline void set_hash_mask_22(int32_t value)
	{
		___hash_mask_22 = value;
	}

	inline static int32_t get_offset_of_hash_shift_23() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___hash_shift_23)); }
	inline int32_t get_hash_shift_23() const { return ___hash_shift_23; }
	inline int32_t* get_address_of_hash_shift_23() { return &___hash_shift_23; }
	inline void set_hash_shift_23(int32_t value)
	{
		___hash_shift_23 = value;
	}

	inline static int32_t get_offset_of_block_start_24() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___block_start_24)); }
	inline int32_t get_block_start_24() const { return ___block_start_24; }
	inline int32_t* get_address_of_block_start_24() { return &___block_start_24; }
	inline void set_block_start_24(int32_t value)
	{
		___block_start_24 = value;
	}

	inline static int32_t get_offset_of_match_length_25() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___match_length_25)); }
	inline int32_t get_match_length_25() const { return ___match_length_25; }
	inline int32_t* get_address_of_match_length_25() { return &___match_length_25; }
	inline void set_match_length_25(int32_t value)
	{
		___match_length_25 = value;
	}

	inline static int32_t get_offset_of_prev_match_26() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___prev_match_26)); }
	inline int32_t get_prev_match_26() const { return ___prev_match_26; }
	inline int32_t* get_address_of_prev_match_26() { return &___prev_match_26; }
	inline void set_prev_match_26(int32_t value)
	{
		___prev_match_26 = value;
	}

	inline static int32_t get_offset_of_match_available_27() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___match_available_27)); }
	inline int32_t get_match_available_27() const { return ___match_available_27; }
	inline int32_t* get_address_of_match_available_27() { return &___match_available_27; }
	inline void set_match_available_27(int32_t value)
	{
		___match_available_27 = value;
	}

	inline static int32_t get_offset_of_strstart_28() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___strstart_28)); }
	inline int32_t get_strstart_28() const { return ___strstart_28; }
	inline int32_t* get_address_of_strstart_28() { return &___strstart_28; }
	inline void set_strstart_28(int32_t value)
	{
		___strstart_28 = value;
	}

	inline static int32_t get_offset_of_match_start_29() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___match_start_29)); }
	inline int32_t get_match_start_29() const { return ___match_start_29; }
	inline int32_t* get_address_of_match_start_29() { return &___match_start_29; }
	inline void set_match_start_29(int32_t value)
	{
		___match_start_29 = value;
	}

	inline static int32_t get_offset_of_lookahead_30() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___lookahead_30)); }
	inline int32_t get_lookahead_30() const { return ___lookahead_30; }
	inline int32_t* get_address_of_lookahead_30() { return &___lookahead_30; }
	inline void set_lookahead_30(int32_t value)
	{
		___lookahead_30 = value;
	}

	inline static int32_t get_offset_of_prev_length_31() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___prev_length_31)); }
	inline int32_t get_prev_length_31() const { return ___prev_length_31; }
	inline int32_t* get_address_of_prev_length_31() { return &___prev_length_31; }
	inline void set_prev_length_31(int32_t value)
	{
		___prev_length_31 = value;
	}

	inline static int32_t get_offset_of_max_chain_length_32() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___max_chain_length_32)); }
	inline int32_t get_max_chain_length_32() const { return ___max_chain_length_32; }
	inline int32_t* get_address_of_max_chain_length_32() { return &___max_chain_length_32; }
	inline void set_max_chain_length_32(int32_t value)
	{
		___max_chain_length_32 = value;
	}

	inline static int32_t get_offset_of_max_lazy_match_33() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___max_lazy_match_33)); }
	inline int32_t get_max_lazy_match_33() const { return ___max_lazy_match_33; }
	inline int32_t* get_address_of_max_lazy_match_33() { return &___max_lazy_match_33; }
	inline void set_max_lazy_match_33(int32_t value)
	{
		___max_lazy_match_33 = value;
	}

	inline static int32_t get_offset_of_level_34() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___level_34)); }
	inline int32_t get_level_34() const { return ___level_34; }
	inline int32_t* get_address_of_level_34() { return &___level_34; }
	inline void set_level_34(int32_t value)
	{
		___level_34 = value;
	}

	inline static int32_t get_offset_of_strategy_35() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___strategy_35)); }
	inline int32_t get_strategy_35() const { return ___strategy_35; }
	inline int32_t* get_address_of_strategy_35() { return &___strategy_35; }
	inline void set_strategy_35(int32_t value)
	{
		___strategy_35 = value;
	}

	inline static int32_t get_offset_of_good_match_36() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___good_match_36)); }
	inline int32_t get_good_match_36() const { return ___good_match_36; }
	inline int32_t* get_address_of_good_match_36() { return &___good_match_36; }
	inline void set_good_match_36(int32_t value)
	{
		___good_match_36 = value;
	}

	inline static int32_t get_offset_of_nice_match_37() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___nice_match_37)); }
	inline int32_t get_nice_match_37() const { return ___nice_match_37; }
	inline int32_t* get_address_of_nice_match_37() { return &___nice_match_37; }
	inline void set_nice_match_37(int32_t value)
	{
		___nice_match_37 = value;
	}

	inline static int32_t get_offset_of_dyn_ltree_38() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___dyn_ltree_38)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_dyn_ltree_38() const { return ___dyn_ltree_38; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_dyn_ltree_38() { return &___dyn_ltree_38; }
	inline void set_dyn_ltree_38(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___dyn_ltree_38 = value;
		Il2CppCodeGenWriteBarrier((&___dyn_ltree_38), value);
	}

	inline static int32_t get_offset_of_dyn_dtree_39() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___dyn_dtree_39)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_dyn_dtree_39() const { return ___dyn_dtree_39; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_dyn_dtree_39() { return &___dyn_dtree_39; }
	inline void set_dyn_dtree_39(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___dyn_dtree_39 = value;
		Il2CppCodeGenWriteBarrier((&___dyn_dtree_39), value);
	}

	inline static int32_t get_offset_of_bl_tree_40() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___bl_tree_40)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_bl_tree_40() const { return ___bl_tree_40; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_bl_tree_40() { return &___bl_tree_40; }
	inline void set_bl_tree_40(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___bl_tree_40 = value;
		Il2CppCodeGenWriteBarrier((&___bl_tree_40), value);
	}

	inline static int32_t get_offset_of_l_desc_41() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___l_desc_41)); }
	inline Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D * get_l_desc_41() const { return ___l_desc_41; }
	inline Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D ** get_address_of_l_desc_41() { return &___l_desc_41; }
	inline void set_l_desc_41(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D * value)
	{
		___l_desc_41 = value;
		Il2CppCodeGenWriteBarrier((&___l_desc_41), value);
	}

	inline static int32_t get_offset_of_d_desc_42() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___d_desc_42)); }
	inline Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D * get_d_desc_42() const { return ___d_desc_42; }
	inline Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D ** get_address_of_d_desc_42() { return &___d_desc_42; }
	inline void set_d_desc_42(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D * value)
	{
		___d_desc_42 = value;
		Il2CppCodeGenWriteBarrier((&___d_desc_42), value);
	}

	inline static int32_t get_offset_of_bl_desc_43() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___bl_desc_43)); }
	inline Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D * get_bl_desc_43() const { return ___bl_desc_43; }
	inline Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D ** get_address_of_bl_desc_43() { return &___bl_desc_43; }
	inline void set_bl_desc_43(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D * value)
	{
		___bl_desc_43 = value;
		Il2CppCodeGenWriteBarrier((&___bl_desc_43), value);
	}

	inline static int32_t get_offset_of_bl_count_44() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___bl_count_44)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_bl_count_44() const { return ___bl_count_44; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_bl_count_44() { return &___bl_count_44; }
	inline void set_bl_count_44(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___bl_count_44 = value;
		Il2CppCodeGenWriteBarrier((&___bl_count_44), value);
	}

	inline static int32_t get_offset_of_heap_45() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___heap_45)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_heap_45() const { return ___heap_45; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_heap_45() { return &___heap_45; }
	inline void set_heap_45(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___heap_45 = value;
		Il2CppCodeGenWriteBarrier((&___heap_45), value);
	}

	inline static int32_t get_offset_of_heap_len_46() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___heap_len_46)); }
	inline int32_t get_heap_len_46() const { return ___heap_len_46; }
	inline int32_t* get_address_of_heap_len_46() { return &___heap_len_46; }
	inline void set_heap_len_46(int32_t value)
	{
		___heap_len_46 = value;
	}

	inline static int32_t get_offset_of_heap_max_47() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___heap_max_47)); }
	inline int32_t get_heap_max_47() const { return ___heap_max_47; }
	inline int32_t* get_address_of_heap_max_47() { return &___heap_max_47; }
	inline void set_heap_max_47(int32_t value)
	{
		___heap_max_47 = value;
	}

	inline static int32_t get_offset_of_depth_48() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___depth_48)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_depth_48() const { return ___depth_48; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_depth_48() { return &___depth_48; }
	inline void set_depth_48(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___depth_48 = value;
		Il2CppCodeGenWriteBarrier((&___depth_48), value);
	}

	inline static int32_t get_offset_of_l_buf_49() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___l_buf_49)); }
	inline int32_t get_l_buf_49() const { return ___l_buf_49; }
	inline int32_t* get_address_of_l_buf_49() { return &___l_buf_49; }
	inline void set_l_buf_49(int32_t value)
	{
		___l_buf_49 = value;
	}

	inline static int32_t get_offset_of_lit_bufsize_50() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___lit_bufsize_50)); }
	inline int32_t get_lit_bufsize_50() const { return ___lit_bufsize_50; }
	inline int32_t* get_address_of_lit_bufsize_50() { return &___lit_bufsize_50; }
	inline void set_lit_bufsize_50(int32_t value)
	{
		___lit_bufsize_50 = value;
	}

	inline static int32_t get_offset_of_last_lit_51() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___last_lit_51)); }
	inline int32_t get_last_lit_51() const { return ___last_lit_51; }
	inline int32_t* get_address_of_last_lit_51() { return &___last_lit_51; }
	inline void set_last_lit_51(int32_t value)
	{
		___last_lit_51 = value;
	}

	inline static int32_t get_offset_of_d_buf_52() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___d_buf_52)); }
	inline int32_t get_d_buf_52() const { return ___d_buf_52; }
	inline int32_t* get_address_of_d_buf_52() { return &___d_buf_52; }
	inline void set_d_buf_52(int32_t value)
	{
		___d_buf_52 = value;
	}

	inline static int32_t get_offset_of_opt_len_53() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___opt_len_53)); }
	inline int32_t get_opt_len_53() const { return ___opt_len_53; }
	inline int32_t* get_address_of_opt_len_53() { return &___opt_len_53; }
	inline void set_opt_len_53(int32_t value)
	{
		___opt_len_53 = value;
	}

	inline static int32_t get_offset_of_static_len_54() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___static_len_54)); }
	inline int32_t get_static_len_54() const { return ___static_len_54; }
	inline int32_t* get_address_of_static_len_54() { return &___static_len_54; }
	inline void set_static_len_54(int32_t value)
	{
		___static_len_54 = value;
	}

	inline static int32_t get_offset_of_matches_55() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___matches_55)); }
	inline int32_t get_matches_55() const { return ___matches_55; }
	inline int32_t* get_address_of_matches_55() { return &___matches_55; }
	inline void set_matches_55(int32_t value)
	{
		___matches_55 = value;
	}

	inline static int32_t get_offset_of_last_eob_len_56() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___last_eob_len_56)); }
	inline int32_t get_last_eob_len_56() const { return ___last_eob_len_56; }
	inline int32_t* get_address_of_last_eob_len_56() { return &___last_eob_len_56; }
	inline void set_last_eob_len_56(int32_t value)
	{
		___last_eob_len_56 = value;
	}

	inline static int32_t get_offset_of_bi_buf_57() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___bi_buf_57)); }
	inline uint32_t get_bi_buf_57() const { return ___bi_buf_57; }
	inline uint32_t* get_address_of_bi_buf_57() { return &___bi_buf_57; }
	inline void set_bi_buf_57(uint32_t value)
	{
		___bi_buf_57 = value;
	}

	inline static int32_t get_offset_of_bi_valid_58() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5, ___bi_valid_58)); }
	inline int32_t get_bi_valid_58() const { return ___bi_valid_58; }
	inline int32_t* get_address_of_bi_valid_58() { return &___bi_valid_58; }
	inline void set_bi_valid_58(int32_t value)
	{
		___bi_valid_58 = value;
	}
};

struct Deflate_tA360BF189910682C62915B83E8F061F816C886E5_StaticFields
{
public:
	// Org.BouncyCastle.Utilities.Zlib.Deflate_Config[] Org.BouncyCastle.Utilities.Zlib.Deflate::config_table
	ConfigU5BU5D_t697F913CAB0DC9EF0657413DB5866E4C38F6C032* ___config_table_0;
	// System.String[] Org.BouncyCastle.Utilities.Zlib.Deflate::z_errmsg
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___z_errmsg_1;

public:
	inline static int32_t get_offset_of_config_table_0() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5_StaticFields, ___config_table_0)); }
	inline ConfigU5BU5D_t697F913CAB0DC9EF0657413DB5866E4C38F6C032* get_config_table_0() const { return ___config_table_0; }
	inline ConfigU5BU5D_t697F913CAB0DC9EF0657413DB5866E4C38F6C032** get_address_of_config_table_0() { return &___config_table_0; }
	inline void set_config_table_0(ConfigU5BU5D_t697F913CAB0DC9EF0657413DB5866E4C38F6C032* value)
	{
		___config_table_0 = value;
		Il2CppCodeGenWriteBarrier((&___config_table_0), value);
	}

	inline static int32_t get_offset_of_z_errmsg_1() { return static_cast<int32_t>(offsetof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5_StaticFields, ___z_errmsg_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_z_errmsg_1() const { return ___z_errmsg_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_z_errmsg_1() { return &___z_errmsg_1; }
	inline void set_z_errmsg_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___z_errmsg_1 = value;
		Il2CppCodeGenWriteBarrier((&___z_errmsg_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATE_TA360BF189910682C62915B83E8F061F816C886E5_H
#ifndef CONFIG_TBE4BEDAB07B6FBC6A133D3D33E04A3381079E203_H
#define CONFIG_TBE4BEDAB07B6FBC6A133D3D33E04A3381079E203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Zlib.Deflate_Config
struct  Config_tBE4BEDAB07B6FBC6A133D3D33E04A3381079E203  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate_Config::good_length
	int32_t ___good_length_0;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate_Config::max_lazy
	int32_t ___max_lazy_1;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate_Config::nice_length
	int32_t ___nice_length_2;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate_Config::max_chain
	int32_t ___max_chain_3;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Deflate_Config::func
	int32_t ___func_4;

public:
	inline static int32_t get_offset_of_good_length_0() { return static_cast<int32_t>(offsetof(Config_tBE4BEDAB07B6FBC6A133D3D33E04A3381079E203, ___good_length_0)); }
	inline int32_t get_good_length_0() const { return ___good_length_0; }
	inline int32_t* get_address_of_good_length_0() { return &___good_length_0; }
	inline void set_good_length_0(int32_t value)
	{
		___good_length_0 = value;
	}

	inline static int32_t get_offset_of_max_lazy_1() { return static_cast<int32_t>(offsetof(Config_tBE4BEDAB07B6FBC6A133D3D33E04A3381079E203, ___max_lazy_1)); }
	inline int32_t get_max_lazy_1() const { return ___max_lazy_1; }
	inline int32_t* get_address_of_max_lazy_1() { return &___max_lazy_1; }
	inline void set_max_lazy_1(int32_t value)
	{
		___max_lazy_1 = value;
	}

	inline static int32_t get_offset_of_nice_length_2() { return static_cast<int32_t>(offsetof(Config_tBE4BEDAB07B6FBC6A133D3D33E04A3381079E203, ___nice_length_2)); }
	inline int32_t get_nice_length_2() const { return ___nice_length_2; }
	inline int32_t* get_address_of_nice_length_2() { return &___nice_length_2; }
	inline void set_nice_length_2(int32_t value)
	{
		___nice_length_2 = value;
	}

	inline static int32_t get_offset_of_max_chain_3() { return static_cast<int32_t>(offsetof(Config_tBE4BEDAB07B6FBC6A133D3D33E04A3381079E203, ___max_chain_3)); }
	inline int32_t get_max_chain_3() const { return ___max_chain_3; }
	inline int32_t* get_address_of_max_chain_3() { return &___max_chain_3; }
	inline void set_max_chain_3(int32_t value)
	{
		___max_chain_3 = value;
	}

	inline static int32_t get_offset_of_func_4() { return static_cast<int32_t>(offsetof(Config_tBE4BEDAB07B6FBC6A133D3D33E04A3381079E203, ___func_4)); }
	inline int32_t get_func_4() const { return ___func_4; }
	inline int32_t* get_address_of_func_4() { return &___func_4; }
	inline void set_func_4(int32_t value)
	{
		___func_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIG_TBE4BEDAB07B6FBC6A133D3D33E04A3381079E203_H
#ifndef INFBLOCKS_T3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4_H
#define INFBLOCKS_T3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Zlib.InfBlocks
struct  InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfBlocks::mode
	int32_t ___mode_2;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfBlocks::left
	int32_t ___left_3;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfBlocks::table
	int32_t ___table_4;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfBlocks::index
	int32_t ___index_5;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfBlocks::blens
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___blens_6;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfBlocks::bb
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___bb_7;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfBlocks::tb
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___tb_8;
	// Org.BouncyCastle.Utilities.Zlib.InfCodes Org.BouncyCastle.Utilities.Zlib.InfBlocks::codes
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743 * ___codes_9;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfBlocks::last
	int32_t ___last_10;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfBlocks::bitk
	int32_t ___bitk_11;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfBlocks::bitb
	int32_t ___bitb_12;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfBlocks::hufts
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___hufts_13;
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.InfBlocks::window
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___window_14;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfBlocks::end
	int32_t ___end_15;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfBlocks::read
	int32_t ___read_16;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfBlocks::write
	int32_t ___write_17;
	// System.Object Org.BouncyCastle.Utilities.Zlib.InfBlocks::checkfn
	RuntimeObject * ___checkfn_18;
	// System.Int64 Org.BouncyCastle.Utilities.Zlib.InfBlocks::check
	int64_t ___check_19;
	// Org.BouncyCastle.Utilities.Zlib.InfTree Org.BouncyCastle.Utilities.Zlib.InfBlocks::inftree
	InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2 * ___inftree_20;

public:
	inline static int32_t get_offset_of_mode_2() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___mode_2)); }
	inline int32_t get_mode_2() const { return ___mode_2; }
	inline int32_t* get_address_of_mode_2() { return &___mode_2; }
	inline void set_mode_2(int32_t value)
	{
		___mode_2 = value;
	}

	inline static int32_t get_offset_of_left_3() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___left_3)); }
	inline int32_t get_left_3() const { return ___left_3; }
	inline int32_t* get_address_of_left_3() { return &___left_3; }
	inline void set_left_3(int32_t value)
	{
		___left_3 = value;
	}

	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___table_4)); }
	inline int32_t get_table_4() const { return ___table_4; }
	inline int32_t* get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(int32_t value)
	{
		___table_4 = value;
	}

	inline static int32_t get_offset_of_index_5() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___index_5)); }
	inline int32_t get_index_5() const { return ___index_5; }
	inline int32_t* get_address_of_index_5() { return &___index_5; }
	inline void set_index_5(int32_t value)
	{
		___index_5 = value;
	}

	inline static int32_t get_offset_of_blens_6() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___blens_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_blens_6() const { return ___blens_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_blens_6() { return &___blens_6; }
	inline void set_blens_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___blens_6 = value;
		Il2CppCodeGenWriteBarrier((&___blens_6), value);
	}

	inline static int32_t get_offset_of_bb_7() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___bb_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_bb_7() const { return ___bb_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_bb_7() { return &___bb_7; }
	inline void set_bb_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___bb_7 = value;
		Il2CppCodeGenWriteBarrier((&___bb_7), value);
	}

	inline static int32_t get_offset_of_tb_8() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___tb_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_tb_8() const { return ___tb_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_tb_8() { return &___tb_8; }
	inline void set_tb_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___tb_8 = value;
		Il2CppCodeGenWriteBarrier((&___tb_8), value);
	}

	inline static int32_t get_offset_of_codes_9() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___codes_9)); }
	inline InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743 * get_codes_9() const { return ___codes_9; }
	inline InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743 ** get_address_of_codes_9() { return &___codes_9; }
	inline void set_codes_9(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743 * value)
	{
		___codes_9 = value;
		Il2CppCodeGenWriteBarrier((&___codes_9), value);
	}

	inline static int32_t get_offset_of_last_10() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___last_10)); }
	inline int32_t get_last_10() const { return ___last_10; }
	inline int32_t* get_address_of_last_10() { return &___last_10; }
	inline void set_last_10(int32_t value)
	{
		___last_10 = value;
	}

	inline static int32_t get_offset_of_bitk_11() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___bitk_11)); }
	inline int32_t get_bitk_11() const { return ___bitk_11; }
	inline int32_t* get_address_of_bitk_11() { return &___bitk_11; }
	inline void set_bitk_11(int32_t value)
	{
		___bitk_11 = value;
	}

	inline static int32_t get_offset_of_bitb_12() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___bitb_12)); }
	inline int32_t get_bitb_12() const { return ___bitb_12; }
	inline int32_t* get_address_of_bitb_12() { return &___bitb_12; }
	inline void set_bitb_12(int32_t value)
	{
		___bitb_12 = value;
	}

	inline static int32_t get_offset_of_hufts_13() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___hufts_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_hufts_13() const { return ___hufts_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_hufts_13() { return &___hufts_13; }
	inline void set_hufts_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___hufts_13 = value;
		Il2CppCodeGenWriteBarrier((&___hufts_13), value);
	}

	inline static int32_t get_offset_of_window_14() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___window_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_window_14() const { return ___window_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_window_14() { return &___window_14; }
	inline void set_window_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___window_14 = value;
		Il2CppCodeGenWriteBarrier((&___window_14), value);
	}

	inline static int32_t get_offset_of_end_15() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___end_15)); }
	inline int32_t get_end_15() const { return ___end_15; }
	inline int32_t* get_address_of_end_15() { return &___end_15; }
	inline void set_end_15(int32_t value)
	{
		___end_15 = value;
	}

	inline static int32_t get_offset_of_read_16() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___read_16)); }
	inline int32_t get_read_16() const { return ___read_16; }
	inline int32_t* get_address_of_read_16() { return &___read_16; }
	inline void set_read_16(int32_t value)
	{
		___read_16 = value;
	}

	inline static int32_t get_offset_of_write_17() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___write_17)); }
	inline int32_t get_write_17() const { return ___write_17; }
	inline int32_t* get_address_of_write_17() { return &___write_17; }
	inline void set_write_17(int32_t value)
	{
		___write_17 = value;
	}

	inline static int32_t get_offset_of_checkfn_18() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___checkfn_18)); }
	inline RuntimeObject * get_checkfn_18() const { return ___checkfn_18; }
	inline RuntimeObject ** get_address_of_checkfn_18() { return &___checkfn_18; }
	inline void set_checkfn_18(RuntimeObject * value)
	{
		___checkfn_18 = value;
		Il2CppCodeGenWriteBarrier((&___checkfn_18), value);
	}

	inline static int32_t get_offset_of_check_19() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___check_19)); }
	inline int64_t get_check_19() const { return ___check_19; }
	inline int64_t* get_address_of_check_19() { return &___check_19; }
	inline void set_check_19(int64_t value)
	{
		___check_19 = value;
	}

	inline static int32_t get_offset_of_inftree_20() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4, ___inftree_20)); }
	inline InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2 * get_inftree_20() const { return ___inftree_20; }
	inline InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2 ** get_address_of_inftree_20() { return &___inftree_20; }
	inline void set_inftree_20(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2 * value)
	{
		___inftree_20 = value;
		Il2CppCodeGenWriteBarrier((&___inftree_20), value);
	}
};

struct InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4_StaticFields
{
public:
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfBlocks::inflate_mask
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___inflate_mask_0;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfBlocks::border
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___border_1;

public:
	inline static int32_t get_offset_of_inflate_mask_0() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4_StaticFields, ___inflate_mask_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_inflate_mask_0() const { return ___inflate_mask_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_inflate_mask_0() { return &___inflate_mask_0; }
	inline void set_inflate_mask_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___inflate_mask_0 = value;
		Il2CppCodeGenWriteBarrier((&___inflate_mask_0), value);
	}

	inline static int32_t get_offset_of_border_1() { return static_cast<int32_t>(offsetof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4_StaticFields, ___border_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_border_1() const { return ___border_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_border_1() { return &___border_1; }
	inline void set_border_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___border_1 = value;
		Il2CppCodeGenWriteBarrier((&___border_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFBLOCKS_T3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4_H
#ifndef INFCODES_TBC425671E2190CB31E6C3230877ABC11F7031743_H
#define INFCODES_TBC425671E2190CB31E6C3230877ABC11F7031743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Zlib.InfCodes
struct  InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfCodes::mode
	int32_t ___mode_1;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfCodes::len
	int32_t ___len_2;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfCodes::tree
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___tree_3;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfCodes::tree_index
	int32_t ___tree_index_4;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfCodes::need
	int32_t ___need_5;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfCodes::lit
	int32_t ___lit_6;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfCodes::get
	int32_t ___get_7;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfCodes::dist
	int32_t ___dist_8;
	// System.Byte Org.BouncyCastle.Utilities.Zlib.InfCodes::lbits
	uint8_t ___lbits_9;
	// System.Byte Org.BouncyCastle.Utilities.Zlib.InfCodes::dbits
	uint8_t ___dbits_10;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfCodes::ltree
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ltree_11;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfCodes::ltree_index
	int32_t ___ltree_index_12;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfCodes::dtree
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___dtree_13;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.InfCodes::dtree_index
	int32_t ___dtree_index_14;

public:
	inline static int32_t get_offset_of_mode_1() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___mode_1)); }
	inline int32_t get_mode_1() const { return ___mode_1; }
	inline int32_t* get_address_of_mode_1() { return &___mode_1; }
	inline void set_mode_1(int32_t value)
	{
		___mode_1 = value;
	}

	inline static int32_t get_offset_of_len_2() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___len_2)); }
	inline int32_t get_len_2() const { return ___len_2; }
	inline int32_t* get_address_of_len_2() { return &___len_2; }
	inline void set_len_2(int32_t value)
	{
		___len_2 = value;
	}

	inline static int32_t get_offset_of_tree_3() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___tree_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_tree_3() const { return ___tree_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_tree_3() { return &___tree_3; }
	inline void set_tree_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___tree_3 = value;
		Il2CppCodeGenWriteBarrier((&___tree_3), value);
	}

	inline static int32_t get_offset_of_tree_index_4() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___tree_index_4)); }
	inline int32_t get_tree_index_4() const { return ___tree_index_4; }
	inline int32_t* get_address_of_tree_index_4() { return &___tree_index_4; }
	inline void set_tree_index_4(int32_t value)
	{
		___tree_index_4 = value;
	}

	inline static int32_t get_offset_of_need_5() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___need_5)); }
	inline int32_t get_need_5() const { return ___need_5; }
	inline int32_t* get_address_of_need_5() { return &___need_5; }
	inline void set_need_5(int32_t value)
	{
		___need_5 = value;
	}

	inline static int32_t get_offset_of_lit_6() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___lit_6)); }
	inline int32_t get_lit_6() const { return ___lit_6; }
	inline int32_t* get_address_of_lit_6() { return &___lit_6; }
	inline void set_lit_6(int32_t value)
	{
		___lit_6 = value;
	}

	inline static int32_t get_offset_of_get_7() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___get_7)); }
	inline int32_t get_get_7() const { return ___get_7; }
	inline int32_t* get_address_of_get_7() { return &___get_7; }
	inline void set_get_7(int32_t value)
	{
		___get_7 = value;
	}

	inline static int32_t get_offset_of_dist_8() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___dist_8)); }
	inline int32_t get_dist_8() const { return ___dist_8; }
	inline int32_t* get_address_of_dist_8() { return &___dist_8; }
	inline void set_dist_8(int32_t value)
	{
		___dist_8 = value;
	}

	inline static int32_t get_offset_of_lbits_9() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___lbits_9)); }
	inline uint8_t get_lbits_9() const { return ___lbits_9; }
	inline uint8_t* get_address_of_lbits_9() { return &___lbits_9; }
	inline void set_lbits_9(uint8_t value)
	{
		___lbits_9 = value;
	}

	inline static int32_t get_offset_of_dbits_10() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___dbits_10)); }
	inline uint8_t get_dbits_10() const { return ___dbits_10; }
	inline uint8_t* get_address_of_dbits_10() { return &___dbits_10; }
	inline void set_dbits_10(uint8_t value)
	{
		___dbits_10 = value;
	}

	inline static int32_t get_offset_of_ltree_11() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___ltree_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ltree_11() const { return ___ltree_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ltree_11() { return &___ltree_11; }
	inline void set_ltree_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ltree_11 = value;
		Il2CppCodeGenWriteBarrier((&___ltree_11), value);
	}

	inline static int32_t get_offset_of_ltree_index_12() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___ltree_index_12)); }
	inline int32_t get_ltree_index_12() const { return ___ltree_index_12; }
	inline int32_t* get_address_of_ltree_index_12() { return &___ltree_index_12; }
	inline void set_ltree_index_12(int32_t value)
	{
		___ltree_index_12 = value;
	}

	inline static int32_t get_offset_of_dtree_13() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___dtree_13)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_dtree_13() const { return ___dtree_13; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_dtree_13() { return &___dtree_13; }
	inline void set_dtree_13(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___dtree_13 = value;
		Il2CppCodeGenWriteBarrier((&___dtree_13), value);
	}

	inline static int32_t get_offset_of_dtree_index_14() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743, ___dtree_index_14)); }
	inline int32_t get_dtree_index_14() const { return ___dtree_index_14; }
	inline int32_t* get_address_of_dtree_index_14() { return &___dtree_index_14; }
	inline void set_dtree_index_14(int32_t value)
	{
		___dtree_index_14 = value;
	}
};

struct InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743_StaticFields
{
public:
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfCodes::inflate_mask
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___inflate_mask_0;

public:
	inline static int32_t get_offset_of_inflate_mask_0() { return static_cast<int32_t>(offsetof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743_StaticFields, ___inflate_mask_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_inflate_mask_0() const { return ___inflate_mask_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_inflate_mask_0() { return &___inflate_mask_0; }
	inline void set_inflate_mask_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___inflate_mask_0 = value;
		Il2CppCodeGenWriteBarrier((&___inflate_mask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFCODES_TBC425671E2190CB31E6C3230877ABC11F7031743_H
#ifndef INFTREE_T5B928E492CC48FB71F79556EA6E70A8CB9A400B2_H
#define INFTREE_T5B928E492CC48FB71F79556EA6E70A8CB9A400B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Zlib.InfTree
struct  InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2  : public RuntimeObject
{
public:
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfTree::hn
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___hn_6;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfTree::v
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___v_7;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfTree::c
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___c_8;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfTree::r
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___r_9;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfTree::u
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___u_10;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfTree::x
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___x_11;

public:
	inline static int32_t get_offset_of_hn_6() { return static_cast<int32_t>(offsetof(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2, ___hn_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_hn_6() const { return ___hn_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_hn_6() { return &___hn_6; }
	inline void set_hn_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___hn_6 = value;
		Il2CppCodeGenWriteBarrier((&___hn_6), value);
	}

	inline static int32_t get_offset_of_v_7() { return static_cast<int32_t>(offsetof(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2, ___v_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_v_7() const { return ___v_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_v_7() { return &___v_7; }
	inline void set_v_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___v_7 = value;
		Il2CppCodeGenWriteBarrier((&___v_7), value);
	}

	inline static int32_t get_offset_of_c_8() { return static_cast<int32_t>(offsetof(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2, ___c_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_c_8() const { return ___c_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_c_8() { return &___c_8; }
	inline void set_c_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___c_8 = value;
		Il2CppCodeGenWriteBarrier((&___c_8), value);
	}

	inline static int32_t get_offset_of_r_9() { return static_cast<int32_t>(offsetof(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2, ___r_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_r_9() const { return ___r_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_r_9() { return &___r_9; }
	inline void set_r_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___r_9 = value;
		Il2CppCodeGenWriteBarrier((&___r_9), value);
	}

	inline static int32_t get_offset_of_u_10() { return static_cast<int32_t>(offsetof(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2, ___u_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_u_10() const { return ___u_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_u_10() { return &___u_10; }
	inline void set_u_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___u_10 = value;
		Il2CppCodeGenWriteBarrier((&___u_10), value);
	}

	inline static int32_t get_offset_of_x_11() { return static_cast<int32_t>(offsetof(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2, ___x_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_x_11() const { return ___x_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_x_11() { return &___x_11; }
	inline void set_x_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___x_11 = value;
		Il2CppCodeGenWriteBarrier((&___x_11), value);
	}
};

struct InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields
{
public:
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfTree::fixed_tl
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___fixed_tl_0;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfTree::fixed_td
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___fixed_td_1;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfTree::cplens
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cplens_2;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfTree::cplext
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cplext_3;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfTree::cpdist
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cpdist_4;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.InfTree::cpdext
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___cpdext_5;

public:
	inline static int32_t get_offset_of_fixed_tl_0() { return static_cast<int32_t>(offsetof(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields, ___fixed_tl_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_fixed_tl_0() const { return ___fixed_tl_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_fixed_tl_0() { return &___fixed_tl_0; }
	inline void set_fixed_tl_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___fixed_tl_0 = value;
		Il2CppCodeGenWriteBarrier((&___fixed_tl_0), value);
	}

	inline static int32_t get_offset_of_fixed_td_1() { return static_cast<int32_t>(offsetof(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields, ___fixed_td_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_fixed_td_1() const { return ___fixed_td_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_fixed_td_1() { return &___fixed_td_1; }
	inline void set_fixed_td_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___fixed_td_1 = value;
		Il2CppCodeGenWriteBarrier((&___fixed_td_1), value);
	}

	inline static int32_t get_offset_of_cplens_2() { return static_cast<int32_t>(offsetof(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields, ___cplens_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cplens_2() const { return ___cplens_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cplens_2() { return &___cplens_2; }
	inline void set_cplens_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cplens_2 = value;
		Il2CppCodeGenWriteBarrier((&___cplens_2), value);
	}

	inline static int32_t get_offset_of_cplext_3() { return static_cast<int32_t>(offsetof(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields, ___cplext_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cplext_3() const { return ___cplext_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cplext_3() { return &___cplext_3; }
	inline void set_cplext_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cplext_3 = value;
		Il2CppCodeGenWriteBarrier((&___cplext_3), value);
	}

	inline static int32_t get_offset_of_cpdist_4() { return static_cast<int32_t>(offsetof(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields, ___cpdist_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cpdist_4() const { return ___cpdist_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cpdist_4() { return &___cpdist_4; }
	inline void set_cpdist_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cpdist_4 = value;
		Il2CppCodeGenWriteBarrier((&___cpdist_4), value);
	}

	inline static int32_t get_offset_of_cpdext_5() { return static_cast<int32_t>(offsetof(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields, ___cpdext_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_cpdext_5() const { return ___cpdext_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_cpdext_5() { return &___cpdext_5; }
	inline void set_cpdext_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___cpdext_5 = value;
		Il2CppCodeGenWriteBarrier((&___cpdext_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFTREE_T5B928E492CC48FB71F79556EA6E70A8CB9A400B2_H
#ifndef INFLATE_TAA980C6AB931D08786B9F940FDC5EE875427E180_H
#define INFLATE_TAA980C6AB931D08786B9F940FDC5EE875427E180_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Zlib.Inflate
struct  Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Inflate::mode
	int32_t ___mode_0;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Inflate::method
	int32_t ___method_1;
	// System.Int64[] Org.BouncyCastle.Utilities.Zlib.Inflate::was
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___was_2;
	// System.Int64 Org.BouncyCastle.Utilities.Zlib.Inflate::need
	int64_t ___need_3;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Inflate::marker
	int32_t ___marker_4;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Inflate::nowrap
	int32_t ___nowrap_5;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Inflate::wbits
	int32_t ___wbits_6;
	// Org.BouncyCastle.Utilities.Zlib.InfBlocks Org.BouncyCastle.Utilities.Zlib.Inflate::blocks
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4 * ___blocks_7;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180, ___method_1)); }
	inline int32_t get_method_1() const { return ___method_1; }
	inline int32_t* get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(int32_t value)
	{
		___method_1 = value;
	}

	inline static int32_t get_offset_of_was_2() { return static_cast<int32_t>(offsetof(Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180, ___was_2)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_was_2() const { return ___was_2; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_was_2() { return &___was_2; }
	inline void set_was_2(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___was_2 = value;
		Il2CppCodeGenWriteBarrier((&___was_2), value);
	}

	inline static int32_t get_offset_of_need_3() { return static_cast<int32_t>(offsetof(Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180, ___need_3)); }
	inline int64_t get_need_3() const { return ___need_3; }
	inline int64_t* get_address_of_need_3() { return &___need_3; }
	inline void set_need_3(int64_t value)
	{
		___need_3 = value;
	}

	inline static int32_t get_offset_of_marker_4() { return static_cast<int32_t>(offsetof(Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180, ___marker_4)); }
	inline int32_t get_marker_4() const { return ___marker_4; }
	inline int32_t* get_address_of_marker_4() { return &___marker_4; }
	inline void set_marker_4(int32_t value)
	{
		___marker_4 = value;
	}

	inline static int32_t get_offset_of_nowrap_5() { return static_cast<int32_t>(offsetof(Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180, ___nowrap_5)); }
	inline int32_t get_nowrap_5() const { return ___nowrap_5; }
	inline int32_t* get_address_of_nowrap_5() { return &___nowrap_5; }
	inline void set_nowrap_5(int32_t value)
	{
		___nowrap_5 = value;
	}

	inline static int32_t get_offset_of_wbits_6() { return static_cast<int32_t>(offsetof(Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180, ___wbits_6)); }
	inline int32_t get_wbits_6() const { return ___wbits_6; }
	inline int32_t* get_address_of_wbits_6() { return &___wbits_6; }
	inline void set_wbits_6(int32_t value)
	{
		___wbits_6 = value;
	}

	inline static int32_t get_offset_of_blocks_7() { return static_cast<int32_t>(offsetof(Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180, ___blocks_7)); }
	inline InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4 * get_blocks_7() const { return ___blocks_7; }
	inline InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4 ** get_address_of_blocks_7() { return &___blocks_7; }
	inline void set_blocks_7(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4 * value)
	{
		___blocks_7 = value;
		Il2CppCodeGenWriteBarrier((&___blocks_7), value);
	}
};

struct Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180_StaticFields
{
public:
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.Inflate::mark
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mark_8;

public:
	inline static int32_t get_offset_of_mark_8() { return static_cast<int32_t>(offsetof(Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180_StaticFields, ___mark_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mark_8() const { return ___mark_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mark_8() { return &___mark_8; }
	inline void set_mark_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mark_8 = value;
		Il2CppCodeGenWriteBarrier((&___mark_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFLATE_TAA980C6AB931D08786B9F940FDC5EE875427E180_H
#ifndef STATICTREE_T15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_H
#define STATICTREE_T15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Zlib.StaticTree
struct  StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5  : public RuntimeObject
{
public:
	// System.Int16[] Org.BouncyCastle.Utilities.Zlib.StaticTree::static_tree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___static_tree_5;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.StaticTree::extra_bits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___extra_bits_6;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.StaticTree::extra_base
	int32_t ___extra_base_7;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.StaticTree::elems
	int32_t ___elems_8;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.StaticTree::max_length
	int32_t ___max_length_9;

public:
	inline static int32_t get_offset_of_static_tree_5() { return static_cast<int32_t>(offsetof(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5, ___static_tree_5)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_static_tree_5() const { return ___static_tree_5; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_static_tree_5() { return &___static_tree_5; }
	inline void set_static_tree_5(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___static_tree_5 = value;
		Il2CppCodeGenWriteBarrier((&___static_tree_5), value);
	}

	inline static int32_t get_offset_of_extra_bits_6() { return static_cast<int32_t>(offsetof(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5, ___extra_bits_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_extra_bits_6() const { return ___extra_bits_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_extra_bits_6() { return &___extra_bits_6; }
	inline void set_extra_bits_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___extra_bits_6 = value;
		Il2CppCodeGenWriteBarrier((&___extra_bits_6), value);
	}

	inline static int32_t get_offset_of_extra_base_7() { return static_cast<int32_t>(offsetof(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5, ___extra_base_7)); }
	inline int32_t get_extra_base_7() const { return ___extra_base_7; }
	inline int32_t* get_address_of_extra_base_7() { return &___extra_base_7; }
	inline void set_extra_base_7(int32_t value)
	{
		___extra_base_7 = value;
	}

	inline static int32_t get_offset_of_elems_8() { return static_cast<int32_t>(offsetof(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5, ___elems_8)); }
	inline int32_t get_elems_8() const { return ___elems_8; }
	inline int32_t* get_address_of_elems_8() { return &___elems_8; }
	inline void set_elems_8(int32_t value)
	{
		___elems_8 = value;
	}

	inline static int32_t get_offset_of_max_length_9() { return static_cast<int32_t>(offsetof(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5, ___max_length_9)); }
	inline int32_t get_max_length_9() const { return ___max_length_9; }
	inline int32_t* get_address_of_max_length_9() { return &___max_length_9; }
	inline void set_max_length_9(int32_t value)
	{
		___max_length_9 = value;
	}
};

struct StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_StaticFields
{
public:
	// System.Int16[] Org.BouncyCastle.Utilities.Zlib.StaticTree::static_ltree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___static_ltree_0;
	// System.Int16[] Org.BouncyCastle.Utilities.Zlib.StaticTree::static_dtree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___static_dtree_1;
	// Org.BouncyCastle.Utilities.Zlib.StaticTree Org.BouncyCastle.Utilities.Zlib.StaticTree::static_l_desc
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 * ___static_l_desc_2;
	// Org.BouncyCastle.Utilities.Zlib.StaticTree Org.BouncyCastle.Utilities.Zlib.StaticTree::static_d_desc
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 * ___static_d_desc_3;
	// Org.BouncyCastle.Utilities.Zlib.StaticTree Org.BouncyCastle.Utilities.Zlib.StaticTree::static_bl_desc
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 * ___static_bl_desc_4;

public:
	inline static int32_t get_offset_of_static_ltree_0() { return static_cast<int32_t>(offsetof(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_StaticFields, ___static_ltree_0)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_static_ltree_0() const { return ___static_ltree_0; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_static_ltree_0() { return &___static_ltree_0; }
	inline void set_static_ltree_0(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___static_ltree_0 = value;
		Il2CppCodeGenWriteBarrier((&___static_ltree_0), value);
	}

	inline static int32_t get_offset_of_static_dtree_1() { return static_cast<int32_t>(offsetof(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_StaticFields, ___static_dtree_1)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_static_dtree_1() const { return ___static_dtree_1; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_static_dtree_1() { return &___static_dtree_1; }
	inline void set_static_dtree_1(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___static_dtree_1 = value;
		Il2CppCodeGenWriteBarrier((&___static_dtree_1), value);
	}

	inline static int32_t get_offset_of_static_l_desc_2() { return static_cast<int32_t>(offsetof(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_StaticFields, ___static_l_desc_2)); }
	inline StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 * get_static_l_desc_2() const { return ___static_l_desc_2; }
	inline StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 ** get_address_of_static_l_desc_2() { return &___static_l_desc_2; }
	inline void set_static_l_desc_2(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 * value)
	{
		___static_l_desc_2 = value;
		Il2CppCodeGenWriteBarrier((&___static_l_desc_2), value);
	}

	inline static int32_t get_offset_of_static_d_desc_3() { return static_cast<int32_t>(offsetof(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_StaticFields, ___static_d_desc_3)); }
	inline StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 * get_static_d_desc_3() const { return ___static_d_desc_3; }
	inline StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 ** get_address_of_static_d_desc_3() { return &___static_d_desc_3; }
	inline void set_static_d_desc_3(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 * value)
	{
		___static_d_desc_3 = value;
		Il2CppCodeGenWriteBarrier((&___static_d_desc_3), value);
	}

	inline static int32_t get_offset_of_static_bl_desc_4() { return static_cast<int32_t>(offsetof(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_StaticFields, ___static_bl_desc_4)); }
	inline StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 * get_static_bl_desc_4() const { return ___static_bl_desc_4; }
	inline StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 ** get_address_of_static_bl_desc_4() { return &___static_bl_desc_4; }
	inline void set_static_bl_desc_4(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 * value)
	{
		___static_bl_desc_4 = value;
		Il2CppCodeGenWriteBarrier((&___static_bl_desc_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICTREE_T15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_H
#ifndef TREE_T5D1AEE8FF0C0886844A468224840118DD973FD6D_H
#define TREE_T5D1AEE8FF0C0886844A468224840118DD973FD6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Zlib.Tree
struct  Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D  : public RuntimeObject
{
public:
	// System.Int16[] Org.BouncyCastle.Utilities.Zlib.Tree::dyn_tree
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___dyn_tree_8;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.Tree::max_code
	int32_t ___max_code_9;
	// Org.BouncyCastle.Utilities.Zlib.StaticTree Org.BouncyCastle.Utilities.Zlib.Tree::stat_desc
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 * ___stat_desc_10;

public:
	inline static int32_t get_offset_of_dyn_tree_8() { return static_cast<int32_t>(offsetof(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D, ___dyn_tree_8)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_dyn_tree_8() const { return ___dyn_tree_8; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_dyn_tree_8() { return &___dyn_tree_8; }
	inline void set_dyn_tree_8(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___dyn_tree_8 = value;
		Il2CppCodeGenWriteBarrier((&___dyn_tree_8), value);
	}

	inline static int32_t get_offset_of_max_code_9() { return static_cast<int32_t>(offsetof(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D, ___max_code_9)); }
	inline int32_t get_max_code_9() const { return ___max_code_9; }
	inline int32_t* get_address_of_max_code_9() { return &___max_code_9; }
	inline void set_max_code_9(int32_t value)
	{
		___max_code_9 = value;
	}

	inline static int32_t get_offset_of_stat_desc_10() { return static_cast<int32_t>(offsetof(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D, ___stat_desc_10)); }
	inline StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 * get_stat_desc_10() const { return ___stat_desc_10; }
	inline StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 ** get_address_of_stat_desc_10() { return &___stat_desc_10; }
	inline void set_stat_desc_10(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5 * value)
	{
		___stat_desc_10 = value;
		Il2CppCodeGenWriteBarrier((&___stat_desc_10), value);
	}
};

struct Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields
{
public:
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.Tree::extra_lbits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___extra_lbits_0;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.Tree::extra_dbits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___extra_dbits_1;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.Tree::extra_blbits
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___extra_blbits_2;
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.Tree::bl_order
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bl_order_3;
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.Tree::_dist_code
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____dist_code_4;
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.Tree::_length_code
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____length_code_5;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.Tree::base_length
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___base_length_6;
	// System.Int32[] Org.BouncyCastle.Utilities.Zlib.Tree::base_dist
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___base_dist_7;

public:
	inline static int32_t get_offset_of_extra_lbits_0() { return static_cast<int32_t>(offsetof(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields, ___extra_lbits_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_extra_lbits_0() const { return ___extra_lbits_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_extra_lbits_0() { return &___extra_lbits_0; }
	inline void set_extra_lbits_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___extra_lbits_0 = value;
		Il2CppCodeGenWriteBarrier((&___extra_lbits_0), value);
	}

	inline static int32_t get_offset_of_extra_dbits_1() { return static_cast<int32_t>(offsetof(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields, ___extra_dbits_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_extra_dbits_1() const { return ___extra_dbits_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_extra_dbits_1() { return &___extra_dbits_1; }
	inline void set_extra_dbits_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___extra_dbits_1 = value;
		Il2CppCodeGenWriteBarrier((&___extra_dbits_1), value);
	}

	inline static int32_t get_offset_of_extra_blbits_2() { return static_cast<int32_t>(offsetof(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields, ___extra_blbits_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_extra_blbits_2() const { return ___extra_blbits_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_extra_blbits_2() { return &___extra_blbits_2; }
	inline void set_extra_blbits_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___extra_blbits_2 = value;
		Il2CppCodeGenWriteBarrier((&___extra_blbits_2), value);
	}

	inline static int32_t get_offset_of_bl_order_3() { return static_cast<int32_t>(offsetof(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields, ___bl_order_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bl_order_3() const { return ___bl_order_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bl_order_3() { return &___bl_order_3; }
	inline void set_bl_order_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bl_order_3 = value;
		Il2CppCodeGenWriteBarrier((&___bl_order_3), value);
	}

	inline static int32_t get_offset_of__dist_code_4() { return static_cast<int32_t>(offsetof(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields, ____dist_code_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__dist_code_4() const { return ____dist_code_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__dist_code_4() { return &____dist_code_4; }
	inline void set__dist_code_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____dist_code_4 = value;
		Il2CppCodeGenWriteBarrier((&____dist_code_4), value);
	}

	inline static int32_t get_offset_of__length_code_5() { return static_cast<int32_t>(offsetof(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields, ____length_code_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__length_code_5() const { return ____length_code_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__length_code_5() { return &____length_code_5; }
	inline void set__length_code_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____length_code_5 = value;
		Il2CppCodeGenWriteBarrier((&____length_code_5), value);
	}

	inline static int32_t get_offset_of_base_length_6() { return static_cast<int32_t>(offsetof(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields, ___base_length_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_base_length_6() const { return ___base_length_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_base_length_6() { return &___base_length_6; }
	inline void set_base_length_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___base_length_6 = value;
		Il2CppCodeGenWriteBarrier((&___base_length_6), value);
	}

	inline static int32_t get_offset_of_base_dist_7() { return static_cast<int32_t>(offsetof(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields, ___base_dist_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_base_dist_7() const { return ___base_dist_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_base_dist_7() { return &___base_dist_7; }
	inline void set_base_dist_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___base_dist_7 = value;
		Il2CppCodeGenWriteBarrier((&___base_dist_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TREE_T5D1AEE8FF0C0886844A468224840118DD973FD6D_H
#ifndef ZSTREAM_T0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB_H
#define ZSTREAM_T0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Zlib.ZStream
struct  ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.ZStream::next_in
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___next_in_0;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.ZStream::next_in_index
	int32_t ___next_in_index_1;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.ZStream::avail_in
	int32_t ___avail_in_2;
	// System.Int64 Org.BouncyCastle.Utilities.Zlib.ZStream::total_in
	int64_t ___total_in_3;
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.ZStream::next_out
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___next_out_4;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.ZStream::next_out_index
	int32_t ___next_out_index_5;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.ZStream::avail_out
	int32_t ___avail_out_6;
	// System.Int64 Org.BouncyCastle.Utilities.Zlib.ZStream::total_out
	int64_t ___total_out_7;
	// System.String Org.BouncyCastle.Utilities.Zlib.ZStream::msg
	String_t* ___msg_8;
	// Org.BouncyCastle.Utilities.Zlib.Deflate Org.BouncyCastle.Utilities.Zlib.ZStream::dstate
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5 * ___dstate_9;
	// Org.BouncyCastle.Utilities.Zlib.Inflate Org.BouncyCastle.Utilities.Zlib.ZStream::istate
	Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180 * ___istate_10;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.ZStream::data_type
	int32_t ___data_type_11;
	// System.Int64 Org.BouncyCastle.Utilities.Zlib.ZStream::adler
	int64_t ___adler_12;
	// Org.BouncyCastle.Utilities.Zlib.Adler32 Org.BouncyCastle.Utilities.Zlib.ZStream::_adler
	Adler32_t820E77D81E138AD13711AC3662B7DC9EDD1C9F97 * ____adler_13;

public:
	inline static int32_t get_offset_of_next_in_0() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ___next_in_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_next_in_0() const { return ___next_in_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_next_in_0() { return &___next_in_0; }
	inline void set_next_in_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___next_in_0 = value;
		Il2CppCodeGenWriteBarrier((&___next_in_0), value);
	}

	inline static int32_t get_offset_of_next_in_index_1() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ___next_in_index_1)); }
	inline int32_t get_next_in_index_1() const { return ___next_in_index_1; }
	inline int32_t* get_address_of_next_in_index_1() { return &___next_in_index_1; }
	inline void set_next_in_index_1(int32_t value)
	{
		___next_in_index_1 = value;
	}

	inline static int32_t get_offset_of_avail_in_2() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ___avail_in_2)); }
	inline int32_t get_avail_in_2() const { return ___avail_in_2; }
	inline int32_t* get_address_of_avail_in_2() { return &___avail_in_2; }
	inline void set_avail_in_2(int32_t value)
	{
		___avail_in_2 = value;
	}

	inline static int32_t get_offset_of_total_in_3() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ___total_in_3)); }
	inline int64_t get_total_in_3() const { return ___total_in_3; }
	inline int64_t* get_address_of_total_in_3() { return &___total_in_3; }
	inline void set_total_in_3(int64_t value)
	{
		___total_in_3 = value;
	}

	inline static int32_t get_offset_of_next_out_4() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ___next_out_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_next_out_4() const { return ___next_out_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_next_out_4() { return &___next_out_4; }
	inline void set_next_out_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___next_out_4 = value;
		Il2CppCodeGenWriteBarrier((&___next_out_4), value);
	}

	inline static int32_t get_offset_of_next_out_index_5() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ___next_out_index_5)); }
	inline int32_t get_next_out_index_5() const { return ___next_out_index_5; }
	inline int32_t* get_address_of_next_out_index_5() { return &___next_out_index_5; }
	inline void set_next_out_index_5(int32_t value)
	{
		___next_out_index_5 = value;
	}

	inline static int32_t get_offset_of_avail_out_6() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ___avail_out_6)); }
	inline int32_t get_avail_out_6() const { return ___avail_out_6; }
	inline int32_t* get_address_of_avail_out_6() { return &___avail_out_6; }
	inline void set_avail_out_6(int32_t value)
	{
		___avail_out_6 = value;
	}

	inline static int32_t get_offset_of_total_out_7() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ___total_out_7)); }
	inline int64_t get_total_out_7() const { return ___total_out_7; }
	inline int64_t* get_address_of_total_out_7() { return &___total_out_7; }
	inline void set_total_out_7(int64_t value)
	{
		___total_out_7 = value;
	}

	inline static int32_t get_offset_of_msg_8() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ___msg_8)); }
	inline String_t* get_msg_8() const { return ___msg_8; }
	inline String_t** get_address_of_msg_8() { return &___msg_8; }
	inline void set_msg_8(String_t* value)
	{
		___msg_8 = value;
		Il2CppCodeGenWriteBarrier((&___msg_8), value);
	}

	inline static int32_t get_offset_of_dstate_9() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ___dstate_9)); }
	inline Deflate_tA360BF189910682C62915B83E8F061F816C886E5 * get_dstate_9() const { return ___dstate_9; }
	inline Deflate_tA360BF189910682C62915B83E8F061F816C886E5 ** get_address_of_dstate_9() { return &___dstate_9; }
	inline void set_dstate_9(Deflate_tA360BF189910682C62915B83E8F061F816C886E5 * value)
	{
		___dstate_9 = value;
		Il2CppCodeGenWriteBarrier((&___dstate_9), value);
	}

	inline static int32_t get_offset_of_istate_10() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ___istate_10)); }
	inline Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180 * get_istate_10() const { return ___istate_10; }
	inline Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180 ** get_address_of_istate_10() { return &___istate_10; }
	inline void set_istate_10(Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180 * value)
	{
		___istate_10 = value;
		Il2CppCodeGenWriteBarrier((&___istate_10), value);
	}

	inline static int32_t get_offset_of_data_type_11() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ___data_type_11)); }
	inline int32_t get_data_type_11() const { return ___data_type_11; }
	inline int32_t* get_address_of_data_type_11() { return &___data_type_11; }
	inline void set_data_type_11(int32_t value)
	{
		___data_type_11 = value;
	}

	inline static int32_t get_offset_of_adler_12() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ___adler_12)); }
	inline int64_t get_adler_12() const { return ___adler_12; }
	inline int64_t* get_address_of_adler_12() { return &___adler_12; }
	inline void set_adler_12(int64_t value)
	{
		___adler_12 = value;
	}

	inline static int32_t get_offset_of__adler_13() { return static_cast<int32_t>(offsetof(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB, ____adler_13)); }
	inline Adler32_t820E77D81E138AD13711AC3662B7DC9EDD1C9F97 * get__adler_13() const { return ____adler_13; }
	inline Adler32_t820E77D81E138AD13711AC3662B7DC9EDD1C9F97 ** get_address_of__adler_13() { return &____adler_13; }
	inline void set__adler_13(Adler32_t820E77D81E138AD13711AC3662B7DC9EDD1C9F97 * value)
	{
		____adler_13 = value;
		Il2CppCodeGenWriteBarrier((&____adler_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZSTREAM_T0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB_H
#ifndef X509EXTENSIONUTILITIES_TDABB31F5554EFA536E8D697EFC52F35CC9EED5D8_H
#define X509EXTENSIONUTILITIES_TDABB31F5554EFA536E8D697EFC52F35CC9EED5D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.X509.Extension.X509ExtensionUtilities
struct  X509ExtensionUtilities_tDABB31F5554EFA536E8D697EFC52F35CC9EED5D8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONUTILITIES_TDABB31F5554EFA536E8D697EFC52F35CC9EED5D8_H
#ifndef PEMPARSER_TC3625C27825A6A5C2F2F29B4E6386841272C433D_H
#define PEMPARSER_TC3625C27825A6A5C2F2F29B4E6386841272C433D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.X509.PemParser
struct  PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D  : public RuntimeObject
{
public:
	// System.String Org.BouncyCastle.X509.PemParser::_header1
	String_t* ____header1_0;
	// System.String Org.BouncyCastle.X509.PemParser::_header2
	String_t* ____header2_1;
	// System.String Org.BouncyCastle.X509.PemParser::_footer1
	String_t* ____footer1_2;
	// System.String Org.BouncyCastle.X509.PemParser::_footer2
	String_t* ____footer2_3;

public:
	inline static int32_t get_offset_of__header1_0() { return static_cast<int32_t>(offsetof(PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D, ____header1_0)); }
	inline String_t* get__header1_0() const { return ____header1_0; }
	inline String_t** get_address_of__header1_0() { return &____header1_0; }
	inline void set__header1_0(String_t* value)
	{
		____header1_0 = value;
		Il2CppCodeGenWriteBarrier((&____header1_0), value);
	}

	inline static int32_t get_offset_of__header2_1() { return static_cast<int32_t>(offsetof(PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D, ____header2_1)); }
	inline String_t* get__header2_1() const { return ____header2_1; }
	inline String_t** get_address_of__header2_1() { return &____header2_1; }
	inline void set__header2_1(String_t* value)
	{
		____header2_1 = value;
		Il2CppCodeGenWriteBarrier((&____header2_1), value);
	}

	inline static int32_t get_offset_of__footer1_2() { return static_cast<int32_t>(offsetof(PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D, ____footer1_2)); }
	inline String_t* get__footer1_2() const { return ____footer1_2; }
	inline String_t** get_address_of__footer1_2() { return &____footer1_2; }
	inline void set__footer1_2(String_t* value)
	{
		____footer1_2 = value;
		Il2CppCodeGenWriteBarrier((&____footer1_2), value);
	}

	inline static int32_t get_offset_of__footer2_3() { return static_cast<int32_t>(offsetof(PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D, ____footer2_3)); }
	inline String_t* get__footer2_3() const { return ____footer2_3; }
	inline String_t** get_address_of__footer2_3() { return &____footer2_3; }
	inline void set__footer2_3(String_t* value)
	{
		____footer2_3 = value;
		Il2CppCodeGenWriteBarrier((&____footer2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PEMPARSER_TC3625C27825A6A5C2F2F29B4E6386841272C433D_H
#ifndef X509CERTIFICATEPARSER_TB1C6ACC492089BDFD07811CD6CFE1C1B7C128010_H
#define X509CERTIFICATEPARSER_TB1C6ACC492089BDFD07811CD6CFE1C1B7C128010_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.X509.X509CertificateParser
struct  X509CertificateParser_tB1C6ACC492089BDFD07811CD6CFE1C1B7C128010  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Asn1.Asn1Set Org.BouncyCastle.X509.X509CertificateParser::sData
	Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * ___sData_1;
	// System.Int32 Org.BouncyCastle.X509.X509CertificateParser::sDataObjectCount
	int32_t ___sDataObjectCount_2;
	// System.IO.Stream Org.BouncyCastle.X509.X509CertificateParser::currentStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___currentStream_3;

public:
	inline static int32_t get_offset_of_sData_1() { return static_cast<int32_t>(offsetof(X509CertificateParser_tB1C6ACC492089BDFD07811CD6CFE1C1B7C128010, ___sData_1)); }
	inline Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * get_sData_1() const { return ___sData_1; }
	inline Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 ** get_address_of_sData_1() { return &___sData_1; }
	inline void set_sData_1(Asn1Set_tD51325409687C84A095F7692B251BDE783A1A4A7 * value)
	{
		___sData_1 = value;
		Il2CppCodeGenWriteBarrier((&___sData_1), value);
	}

	inline static int32_t get_offset_of_sDataObjectCount_2() { return static_cast<int32_t>(offsetof(X509CertificateParser_tB1C6ACC492089BDFD07811CD6CFE1C1B7C128010, ___sDataObjectCount_2)); }
	inline int32_t get_sDataObjectCount_2() const { return ___sDataObjectCount_2; }
	inline int32_t* get_address_of_sDataObjectCount_2() { return &___sDataObjectCount_2; }
	inline void set_sDataObjectCount_2(int32_t value)
	{
		___sDataObjectCount_2 = value;
	}

	inline static int32_t get_offset_of_currentStream_3() { return static_cast<int32_t>(offsetof(X509CertificateParser_tB1C6ACC492089BDFD07811CD6CFE1C1B7C128010, ___currentStream_3)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_currentStream_3() const { return ___currentStream_3; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_currentStream_3() { return &___currentStream_3; }
	inline void set_currentStream_3(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___currentStream_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentStream_3), value);
	}
};

struct X509CertificateParser_tB1C6ACC492089BDFD07811CD6CFE1C1B7C128010_StaticFields
{
public:
	// Org.BouncyCastle.X509.PemParser Org.BouncyCastle.X509.X509CertificateParser::PemCertParser
	PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D * ___PemCertParser_0;

public:
	inline static int32_t get_offset_of_PemCertParser_0() { return static_cast<int32_t>(offsetof(X509CertificateParser_tB1C6ACC492089BDFD07811CD6CFE1C1B7C128010_StaticFields, ___PemCertParser_0)); }
	inline PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D * get_PemCertParser_0() const { return ___PemCertParser_0; }
	inline PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D ** get_address_of_PemCertParser_0() { return &___PemCertParser_0; }
	inline void set_PemCertParser_0(PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D * value)
	{
		___PemCertParser_0 = value;
		Il2CppCodeGenWriteBarrier((&___PemCertParser_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEPARSER_TB1C6ACC492089BDFD07811CD6CFE1C1B7C128010_H
#ifndef X509EXTENSIONBASE_TF6546E0F40D0D08B41EAE795CB9E14BCC4A6E57C_H
#define X509EXTENSIONBASE_TF6546E0F40D0D08B41EAE795CB9E14BCC4A6E57C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.X509.X509ExtensionBase
struct  X509ExtensionBase_tF6546E0F40D0D08B41EAE795CB9E14BCC4A6E57C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONBASE_TF6546E0F40D0D08B41EAE795CB9E14BCC4A6E57C_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef RANDOM_T18A28484F67EFA289C256F508A5C71D9E6DEE09F_H
#define RANDOM_T18A28484F67EFA289C256F508A5C71D9E6DEE09F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Random
struct  Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F  : public RuntimeObject
{
public:
	// System.Int32 System.Random::inext
	int32_t ___inext_0;
	// System.Int32 System.Random::inextp
	int32_t ___inextp_1;
	// System.Int32[] System.Random::SeedArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___SeedArray_2;

public:
	inline static int32_t get_offset_of_inext_0() { return static_cast<int32_t>(offsetof(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F, ___inext_0)); }
	inline int32_t get_inext_0() const { return ___inext_0; }
	inline int32_t* get_address_of_inext_0() { return &___inext_0; }
	inline void set_inext_0(int32_t value)
	{
		___inext_0 = value;
	}

	inline static int32_t get_offset_of_inextp_1() { return static_cast<int32_t>(offsetof(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F, ___inextp_1)); }
	inline int32_t get_inextp_1() const { return ___inextp_1; }
	inline int32_t* get_address_of_inextp_1() { return &___inextp_1; }
	inline void set_inextp_1(int32_t value)
	{
		___inextp_1 = value;
	}

	inline static int32_t get_offset_of_SeedArray_2() { return static_cast<int32_t>(offsetof(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F, ___SeedArray_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_SeedArray_2() const { return ___SeedArray_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_SeedArray_2() { return &___SeedArray_2; }
	inline void set_SeedArray_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___SeedArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___SeedArray_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOM_T18A28484F67EFA289C256F508A5C71D9E6DEE09F_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef U24ARRAYTYPEU3D1024_T4D19AA19C09B428FA06442BCB087DC6143DF6214_H
#define U24ARRAYTYPEU3D1024_T4D19AA19C09B428FA06442BCB087DC6143DF6214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D1024
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214__padding[1024];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D1024_T4D19AA19C09B428FA06442BCB087DC6143DF6214_H
#ifndef U24ARRAYTYPEU3D1152_T26C03C6F9BF348D112C4FCF00BB0EC8807B477FF_H
#define U24ARRAYTYPEU3D1152_T26C03C6F9BF348D112C4FCF00BB0EC8807B477FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D1152
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D1152_t26C03C6F9BF348D112C4FCF00BB0EC8807B477FF 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D1152_t26C03C6F9BF348D112C4FCF00BB0EC8807B477FF__padding[1152];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D1152_T26C03C6F9BF348D112C4FCF00BB0EC8807B477FF_H
#ifndef U24ARRAYTYPEU3D116_T3D3A23342468E840802FEE05074D0A8F384077CF_H
#define U24ARRAYTYPEU3D116_T3D3A23342468E840802FEE05074D0A8F384077CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D116
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D116_t3D3A23342468E840802FEE05074D0A8F384077CF 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D116_t3D3A23342468E840802FEE05074D0A8F384077CF__padding[116];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D116_T3D3A23342468E840802FEE05074D0A8F384077CF_H
#ifndef U24ARRAYTYPEU3D12_TF4E301A88C1FC5AE3406635020A6E699649A8F33_H
#define U24ARRAYTYPEU3D12_TF4E301A88C1FC5AE3406635020A6E699649A8F33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_TF4E301A88C1FC5AE3406635020A6E699649A8F33_H
#ifndef U24ARRAYTYPEU3D120_TF3BCD6357BAE42661C2208DCCAC9E84DE70B972B_H
#define U24ARRAYTYPEU3D120_TF3BCD6357BAE42661C2208DCCAC9E84DE70B972B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D120
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B__padding[120];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D120_TF3BCD6357BAE42661C2208DCCAC9E84DE70B972B_H
#ifndef U24ARRAYTYPEU3D124_TC53BBB2379F45A189B7240430D076326E331393F_H
#define U24ARRAYTYPEU3D124_TC53BBB2379F45A189B7240430D076326E331393F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D124
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D124_tC53BBB2379F45A189B7240430D076326E331393F 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D124_tC53BBB2379F45A189B7240430D076326E331393F__padding[124];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D124_TC53BBB2379F45A189B7240430D076326E331393F_H
#ifndef U24ARRAYTYPEU3D128_T86B7B65AEE45C88C60349903E5CAC9606602016D_H
#define U24ARRAYTYPEU3D128_T86B7B65AEE45C88C60349903E5CAC9606602016D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D128
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D__padding[128];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D128_T86B7B65AEE45C88C60349903E5CAC9606602016D_H
#ifndef U24ARRAYTYPEU3D16_T4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326_H
#define U24ARRAYTYPEU3D16_T4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D16
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326__padding[16];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D16_T4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326_H
#ifndef U24ARRAYTYPEU3D20_T3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3_H
#define U24ARRAYTYPEU3D20_T3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D20
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3__padding[20];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D20_T3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3_H
#ifndef U24ARRAYTYPEU3D2048_TAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99_H
#define U24ARRAYTYPEU3D2048_TAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D2048
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99__padding[2048];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D2048_TAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99_H
#ifndef U24ARRAYTYPEU3D24_TF140EF8176ADB359E38E7CC2EDD04F7543567723_H
#define U24ARRAYTYPEU3D24_TF140EF8176ADB359E38E7CC2EDD04F7543567723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_TF140EF8176ADB359E38E7CC2EDD04F7543567723_H
#ifndef U24ARRAYTYPEU3D256_T12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D_H
#define U24ARRAYTYPEU3D256_T12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D256
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D__padding[256];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D256_T12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D_H
#ifndef U24ARRAYTYPEU3D28_T610D95459FE7AE34493ABD63FB9BE516837C8D3F_H
#define U24ARRAYTYPEU3D28_T610D95459FE7AE34493ABD63FB9BE516837C8D3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D28
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F__padding[28];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D28_T610D95459FE7AE34493ABD63FB9BE516837C8D3F_H
#ifndef U24ARRAYTYPEU3D32_T6E9BEF962F0E02C51005F006E292E7FF25C2BC99_H
#define U24ARRAYTYPEU3D32_T6E9BEF962F0E02C51005F006E292E7FF25C2BC99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D32
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99__padding[32];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D32_T6E9BEF962F0E02C51005F006E292E7FF25C2BC99_H
#ifndef U24ARRAYTYPEU3D36_T9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B_H
#define U24ARRAYTYPEU3D36_T9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D36
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D36_t9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D36_t9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B__padding[36];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D36_T9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B_H
#ifndef U24ARRAYTYPEU3D384_T2196F8B9AC0E7C9D619BA635541A7E6C3D3A6891_H
#define U24ARRAYTYPEU3D384_T2196F8B9AC0E7C9D619BA635541A7E6C3D3A6891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D384
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D384_t2196F8B9AC0E7C9D619BA635541A7E6C3D3A6891 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D384_t2196F8B9AC0E7C9D619BA635541A7E6C3D3A6891__padding[384];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D384_T2196F8B9AC0E7C9D619BA635541A7E6C3D3A6891_H
#ifndef U24ARRAYTYPEU3D4_T5ED5745FD13269D462C2D8E11D552E593CE2BEC9_H
#define U24ARRAYTYPEU3D4_T5ED5745FD13269D462C2D8E11D552E593CE2BEC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D4
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9__padding[4];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D4_T5ED5745FD13269D462C2D8E11D552E593CE2BEC9_H
#ifndef U24ARRAYTYPEU3D40_T30FEF54F62CBC9541955162200901918B05295E8_H
#define U24ARRAYTYPEU3D40_T30FEF54F62CBC9541955162200901918B05295E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D40
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8__padding[40];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D40_T30FEF54F62CBC9541955162200901918B05295E8_H
#ifndef U24ARRAYTYPEU3D4096_TE76E581E9D7FCAFF3FDB2A1A2F45C247994B04A5_H
#define U24ARRAYTYPEU3D4096_TE76E581E9D7FCAFF3FDB2A1A2F45C247994B04A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D4096
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D4096_tE76E581E9D7FCAFF3FDB2A1A2F45C247994B04A5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D4096_tE76E581E9D7FCAFF3FDB2A1A2F45C247994B04A5__padding[4096];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D4096_TE76E581E9D7FCAFF3FDB2A1A2F45C247994B04A5_H
#ifndef U24ARRAYTYPEU3D44_T156A45D15629E5CD523F90BAE6DB67B5ACBE097F_H
#define U24ARRAYTYPEU3D44_T156A45D15629E5CD523F90BAE6DB67B5ACBE097F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D44
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D44_t156A45D15629E5CD523F90BAE6DB67B5ACBE097F 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D44_t156A45D15629E5CD523F90BAE6DB67B5ACBE097F__padding[44];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D44_T156A45D15629E5CD523F90BAE6DB67B5ACBE097F_H
#ifndef U24ARRAYTYPEU3D48_T915AF8EF4B514884B68AD099BA24A5315C9B12A3_H
#define U24ARRAYTYPEU3D48_T915AF8EF4B514884B68AD099BA24A5315C9B12A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D48
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3__padding[48];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D48_T915AF8EF4B514884B68AD099BA24A5315C9B12A3_H
#ifndef U24ARRAYTYPEU3D512_T09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6_H
#define U24ARRAYTYPEU3D512_T09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D512
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6__padding[512];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D512_T09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6_H
#ifndef U24ARRAYTYPEU3D56_TB87BF1665D7F5C1366CBF2E3952F7D2EA1526803_H
#define U24ARRAYTYPEU3D56_TB87BF1665D7F5C1366CBF2E3952F7D2EA1526803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D56
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803__padding[56];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D56_TB87BF1665D7F5C1366CBF2E3952F7D2EA1526803_H
#ifndef U24ARRAYTYPEU3D60_TD3F93F1391B60ED8B4F85262374C3FBC6BB2F4CF_H
#define U24ARRAYTYPEU3D60_TD3F93F1391B60ED8B4F85262374C3FBC6BB2F4CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D60
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D60_tD3F93F1391B60ED8B4F85262374C3FBC6BB2F4CF 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D60_tD3F93F1391B60ED8B4F85262374C3FBC6BB2F4CF__padding[60];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D60_TD3F93F1391B60ED8B4F85262374C3FBC6BB2F4CF_H
#ifndef U24ARRAYTYPEU3D6144_T0FA5B30A17373E7B9445630D8FD58FD781E5DA6E_H
#define U24ARRAYTYPEU3D6144_T0FA5B30A17373E7B9445630D8FD58FD781E5DA6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D6144
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D6144_t0FA5B30A17373E7B9445630D8FD58FD781E5DA6E 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D6144_t0FA5B30A17373E7B9445630D8FD58FD781E5DA6E__padding[6144];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D6144_T0FA5B30A17373E7B9445630D8FD58FD781E5DA6E_H
#ifndef U24ARRAYTYPEU3D64_T68B6801159918258824F70A65B7BC8C186A89D05_H
#define U24ARRAYTYPEU3D64_T68B6801159918258824F70A65B7BC8C186A89D05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D64
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05__padding[64];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D64_T68B6801159918258824F70A65B7BC8C186A89D05_H
#ifndef U24ARRAYTYPEU3D640_T2FF86352B1300B97F0AC45DBCF28BFFF74F29242_H
#define U24ARRAYTYPEU3D640_T2FF86352B1300B97F0AC45DBCF28BFFF74F29242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D640
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D640_t2FF86352B1300B97F0AC45DBCF28BFFF74F29242 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D640_t2FF86352B1300B97F0AC45DBCF28BFFF74F29242__padding[640];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D640_T2FF86352B1300B97F0AC45DBCF28BFFF74F29242_H
#ifndef U24ARRAYTYPEU3D68_T01417E058C89CBFE48692EC7117BEC72107FAEA5_H
#define U24ARRAYTYPEU3D68_T01417E058C89CBFE48692EC7117BEC72107FAEA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D68
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5__padding[68];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D68_T01417E058C89CBFE48692EC7117BEC72107FAEA5_H
#ifndef U24ARRAYTYPEU3D72_T2220FD4A9A26B7F357806CAF46D79E53E4F11D64_H
#define U24ARRAYTYPEU3D72_T2220FD4A9A26B7F357806CAF46D79E53E4F11D64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D72
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D72_t2220FD4A9A26B7F357806CAF46D79E53E4F11D64 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D72_t2220FD4A9A26B7F357806CAF46D79E53E4F11D64__padding[72];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D72_T2220FD4A9A26B7F357806CAF46D79E53E4F11D64_H
#ifndef U24ARRAYTYPEU3D76_TA66D1F76A98126C3A8C881FB7A1C3D6647924CC3_H
#define U24ARRAYTYPEU3D76_TA66D1F76A98126C3A8C881FB7A1C3D6647924CC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D76
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D76_tA66D1F76A98126C3A8C881FB7A1C3D6647924CC3 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D76_tA66D1F76A98126C3A8C881FB7A1C3D6647924CC3__padding[76];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D76_TA66D1F76A98126C3A8C881FB7A1C3D6647924CC3_H
#ifndef U24ARRAYTYPEU3D8_T164480F6E3D5CD24BED4D07818C671A634F568EA_H
#define U24ARRAYTYPEU3D8_T164480F6E3D5CD24BED4D07818C671A634F568EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D8
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D8_t164480F6E3D5CD24BED4D07818C671A634F568EA 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D8_t164480F6E3D5CD24BED4D07818C671A634F568EA__padding[8];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D8_T164480F6E3D5CD24BED4D07818C671A634F568EA_H
#ifndef U24ARRAYTYPEU3D96_T5C15B6692DC57B36771160CFA909A1E1B6A9E4A4_H
#define U24ARRAYTYPEU3D96_T5C15B6692DC57B36771160CFA909A1E1B6A9E4A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D96
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D96_t5C15B6692DC57B36771160CFA909A1E1B6A9E4A4 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D96_t5C15B6692DC57B36771160CFA909A1E1B6A9E4A4__padding[96];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D96_T5C15B6692DC57B36771160CFA909A1E1B6A9E4A4_H
#ifndef GENERALSECURITYEXCEPTION_T070EA34F73C23E41F54F84969CBCE09BC9BCA880_H
#define GENERALSECURITYEXCEPTION_T070EA34F73C23E41F54F84969CBCE09BC9BCA880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Security.GeneralSecurityException
struct  GeneralSecurityException_t070EA34F73C23E41F54F84969CBCE09BC9BCA880  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALSECURITYEXCEPTION_T070EA34F73C23E41F54F84969CBCE09BC9BCA880_H
#ifndef SECURERANDOM_T0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0_H
#define SECURERANDOM_T0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Security.SecureRandom
struct  SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0  : public Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F
{
public:
	// Org.BouncyCastle.Crypto.Prng.IRandomGenerator Org.BouncyCastle.Security.SecureRandom::generator
	RuntimeObject* ___generator_5;

public:
	inline static int32_t get_offset_of_generator_5() { return static_cast<int32_t>(offsetof(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0, ___generator_5)); }
	inline RuntimeObject* get_generator_5() const { return ___generator_5; }
	inline RuntimeObject** get_address_of_generator_5() { return &___generator_5; }
	inline void set_generator_5(RuntimeObject* value)
	{
		___generator_5 = value;
		Il2CppCodeGenWriteBarrier((&___generator_5), value);
	}
};

struct SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0_StaticFields
{
public:
	// System.Int64 Org.BouncyCastle.Security.SecureRandom::counter
	int64_t ___counter_3;
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Security.SecureRandom::master
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___master_4;
	// System.Double Org.BouncyCastle.Security.SecureRandom::DoubleScale
	double ___DoubleScale_6;

public:
	inline static int32_t get_offset_of_counter_3() { return static_cast<int32_t>(offsetof(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0_StaticFields, ___counter_3)); }
	inline int64_t get_counter_3() const { return ___counter_3; }
	inline int64_t* get_address_of_counter_3() { return &___counter_3; }
	inline void set_counter_3(int64_t value)
	{
		___counter_3 = value;
	}

	inline static int32_t get_offset_of_master_4() { return static_cast<int32_t>(offsetof(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0_StaticFields, ___master_4)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_master_4() const { return ___master_4; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_master_4() { return &___master_4; }
	inline void set_master_4(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___master_4 = value;
		Il2CppCodeGenWriteBarrier((&___master_4), value);
	}

	inline static int32_t get_offset_of_DoubleScale_6() { return static_cast<int32_t>(offsetof(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0_StaticFields, ___DoubleScale_6)); }
	inline double get_DoubleScale_6() const { return ___DoubleScale_6; }
	inline double* get_address_of_DoubleScale_6() { return &___DoubleScale_6; }
	inline void set_DoubleScale_6(double value)
	{
		___DoubleScale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURERANDOM_T0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0_H
#ifndef SECURITYUTILITYEXCEPTION_T1A9B40C730C498CCADC4BAF7EE932E09E0354089_H
#define SECURITYUTILITYEXCEPTION_T1A9B40C730C498CCADC4BAF7EE932E09E0354089_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Security.SecurityUtilityException
struct  SecurityUtilityException_t1A9B40C730C498CCADC4BAF7EE932E09E0354089  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYUTILITYEXCEPTION_T1A9B40C730C498CCADC4BAF7EE932E09E0354089_H
#ifndef X509CERTIFICATE_T9350C69721F95FA663C0FD6ED97CDF09B8037786_H
#define X509CERTIFICATE_T9350C69721F95FA663C0FD6ED97CDF09B8037786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.X509.X509Certificate
struct  X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786  : public X509ExtensionBase_tF6546E0F40D0D08B41EAE795CB9E14BCC4A6E57C
{
public:
	// Org.BouncyCastle.Asn1.X509.X509CertificateStructure Org.BouncyCastle.X509.X509Certificate::c
	X509CertificateStructure_tEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1 * ___c_0;
	// Org.BouncyCastle.Asn1.X509.BasicConstraints Org.BouncyCastle.X509.X509Certificate::basicConstraints
	BasicConstraints_t199BCC0E7A6950B11A098A0422FDEB8E73E2380E * ___basicConstraints_1;
	// System.Boolean[] Org.BouncyCastle.X509.X509Certificate::keyUsage
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___keyUsage_2;
	// System.Boolean Org.BouncyCastle.X509.X509Certificate::hashValueSet
	bool ___hashValueSet_3;
	// System.Int32 Org.BouncyCastle.X509.X509Certificate::hashValue
	int32_t ___hashValue_4;

public:
	inline static int32_t get_offset_of_c_0() { return static_cast<int32_t>(offsetof(X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786, ___c_0)); }
	inline X509CertificateStructure_tEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1 * get_c_0() const { return ___c_0; }
	inline X509CertificateStructure_tEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1 ** get_address_of_c_0() { return &___c_0; }
	inline void set_c_0(X509CertificateStructure_tEF05F4D8F02C2BC366AC06A3E2BBDB4C35D54FE1 * value)
	{
		___c_0 = value;
		Il2CppCodeGenWriteBarrier((&___c_0), value);
	}

	inline static int32_t get_offset_of_basicConstraints_1() { return static_cast<int32_t>(offsetof(X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786, ___basicConstraints_1)); }
	inline BasicConstraints_t199BCC0E7A6950B11A098A0422FDEB8E73E2380E * get_basicConstraints_1() const { return ___basicConstraints_1; }
	inline BasicConstraints_t199BCC0E7A6950B11A098A0422FDEB8E73E2380E ** get_address_of_basicConstraints_1() { return &___basicConstraints_1; }
	inline void set_basicConstraints_1(BasicConstraints_t199BCC0E7A6950B11A098A0422FDEB8E73E2380E * value)
	{
		___basicConstraints_1 = value;
		Il2CppCodeGenWriteBarrier((&___basicConstraints_1), value);
	}

	inline static int32_t get_offset_of_keyUsage_2() { return static_cast<int32_t>(offsetof(X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786, ___keyUsage_2)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_keyUsage_2() const { return ___keyUsage_2; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_keyUsage_2() { return &___keyUsage_2; }
	inline void set_keyUsage_2(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___keyUsage_2 = value;
		Il2CppCodeGenWriteBarrier((&___keyUsage_2), value);
	}

	inline static int32_t get_offset_of_hashValueSet_3() { return static_cast<int32_t>(offsetof(X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786, ___hashValueSet_3)); }
	inline bool get_hashValueSet_3() const { return ___hashValueSet_3; }
	inline bool* get_address_of_hashValueSet_3() { return &___hashValueSet_3; }
	inline void set_hashValueSet_3(bool value)
	{
		___hashValueSet_3 = value;
	}

	inline static int32_t get_offset_of_hashValue_4() { return static_cast<int32_t>(offsetof(X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786, ___hashValue_4)); }
	inline int32_t get_hashValue_4() const { return ___hashValue_4; }
	inline int32_t* get_address_of_hashValue_4() { return &___hashValue_4; }
	inline void set_hashValue_4(int32_t value)
	{
		___hashValue_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE_T9350C69721F95FA663C0FD6ED97CDF09B8037786_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef DATACONTRACTATTRIBUTE_TAD58D5877BD04EADB56BB4AEDDE342C73F032FC5_H
#define DATACONTRACTATTRIBUTE_TAD58D5877BD04EADB56BB4AEDDE342C73F032FC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.DataContractAttribute
struct  DataContractAttribute_tAD58D5877BD04EADB56BB4AEDDE342C73F032FC5  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.Runtime.Serialization.DataContractAttribute::isReference
	bool ___isReference_0;

public:
	inline static int32_t get_offset_of_isReference_0() { return static_cast<int32_t>(offsetof(DataContractAttribute_tAD58D5877BD04EADB56BB4AEDDE342C73F032FC5, ___isReference_0)); }
	inline bool get_isReference_0() const { return ___isReference_0; }
	inline bool* get_address_of_isReference_0() { return &___isReference_0; }
	inline void set_isReference_0(bool value)
	{
		___isReference_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATACONTRACTATTRIBUTE_TAD58D5877BD04EADB56BB4AEDDE342C73F032FC5_H
#ifndef DATAMEMBERATTRIBUTE_TC865433FEC93FFD46D6F3E4BB28F262C9EE40525_H
#define DATAMEMBERATTRIBUTE_TC865433FEC93FFD46D6F3E4BB28F262C9EE40525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.DataMemberAttribute
struct  DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Runtime.Serialization.DataMemberAttribute::name
	String_t* ___name_0;
	// System.Int32 System.Runtime.Serialization.DataMemberAttribute::order
	int32_t ___order_1;
	// System.Boolean System.Runtime.Serialization.DataMemberAttribute::isRequired
	bool ___isRequired_2;
	// System.Boolean System.Runtime.Serialization.DataMemberAttribute::emitDefaultValue
	bool ___emitDefaultValue_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_isRequired_2() { return static_cast<int32_t>(offsetof(DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525, ___isRequired_2)); }
	inline bool get_isRequired_2() const { return ___isRequired_2; }
	inline bool* get_address_of_isRequired_2() { return &___isRequired_2; }
	inline void set_isRequired_2(bool value)
	{
		___isRequired_2 = value;
	}

	inline static int32_t get_offset_of_emitDefaultValue_3() { return static_cast<int32_t>(offsetof(DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525, ___emitDefaultValue_3)); }
	inline bool get_emitDefaultValue_3() const { return ___emitDefaultValue_3; }
	inline bool* get_address_of_emitDefaultValue_3() { return &___emitDefaultValue_3; }
	inline void set_emitDefaultValue_3(bool value)
	{
		___emitDefaultValue_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAMEMBERATTRIBUTE_TC865433FEC93FFD46D6F3E4BB28F262C9EE40525_H
#ifndef ENUMMEMBERATTRIBUTE_T115D80337B2C8222158FC46345EA100EEB63B32D_H
#define ENUMMEMBERATTRIBUTE_T115D80337B2C8222158FC46345EA100EEB63B32D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.EnumMemberAttribute
struct  EnumMemberAttribute_t115D80337B2C8222158FC46345EA100EEB63B32D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.Runtime.Serialization.EnumMemberAttribute::value
	String_t* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(EnumMemberAttribute_t115D80337B2C8222158FC46345EA100EEB63B32D, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMMEMBERATTRIBUTE_T115D80337B2C8222158FC46345EA100EEB63B32D_H
#ifndef KNOWNTYPEATTRIBUTE_TFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E_H
#define KNOWNTYPEATTRIBUTE_TFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.KnownTypeAttribute
struct  KnownTypeAttribute_tFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type System.Runtime.Serialization.KnownTypeAttribute::type
	Type_t * ___type_0;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(KnownTypeAttribute_tFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KNOWNTYPEATTRIBUTE_TFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields
{
public:
	// <PrivateImplementationDetails>_U24ArrayTypeU3D32 <PrivateImplementationDetails>::U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84
	U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  ___U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D32 <PrivateImplementationDetails>::U24fieldU2DCC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63
	U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  ___U24fieldU2DCC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_1;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D640 <PrivateImplementationDetails>::U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291
	U24ArrayTypeU3D640_t2FF86352B1300B97F0AC45DBCF28BFFF74F29242  ___U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291_2;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2DD2C5BAE967587C6F3D9F2C4551911E0575A1101F
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2DD2C5BAE967587C6F3D9F2C4551911E0575A1101F_3;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2_4;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D2048 <PrivateImplementationDetails>::U24fieldU2DA0FABB8173BA247898A9FA267D0CE05500B667A0
	U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99  ___U24fieldU2DA0FABB8173BA247898A9FA267D0CE05500B667A0_5;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D2048 <PrivateImplementationDetails>::U24fieldU2D467C6758F235D3193618192A64129CBB602C9067
	U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99  ___U24fieldU2D467C6758F235D3193618192A64129CBB602C9067_6;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D2048 <PrivateImplementationDetails>::U24fieldU2D1648F737A4CFFDA4E6C83A3D742109BF9DBC2446
	U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99  ___U24fieldU2D1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_7;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D2048 <PrivateImplementationDetails>::U24fieldU2DC079C42AC966756C902EC38C4D7989F3C20D3625
	U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99  ___U24fieldU2DC079C42AC966756C902EC38C4D7989F3C20D3625_8;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D1024 <PrivateImplementationDetails>::U24fieldU2D840B3A53AAF3595FDF3313D46FFD246A7EA6E89E
	U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  ___U24fieldU2D840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_9;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_10;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939_11;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D32 <PrivateImplementationDetails>::U24fieldU2DF83C7B497B14628C1D0DB3FA2FD89BD5C0D5138A
	U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  ___U24fieldU2DF83C7B497B14628C1D0DB3FA2FD89BD5C0D5138A_12;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D1024 <PrivateImplementationDetails>::U24fieldU2DB68637EF60D499620B99E336C59E4865FFC4C5D7
	U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  ___U24fieldU2DB68637EF60D499620B99E336C59E4865FFC4C5D7_13;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D1024 <PrivateImplementationDetails>::U24fieldU2D1AFB455399A50580CF1039188ABA6BE82F309543
	U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  ___U24fieldU2D1AFB455399A50580CF1039188ABA6BE82F309543_14;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D48 <PrivateImplementationDetails>::U24fieldU2DAE843E1C1136C908565A6D4E04E8564B69465B3B
	U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  ___U24fieldU2DAE843E1C1136C908565A6D4E04E8564B69465B3B_15;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D1024 <PrivateImplementationDetails>::U24fieldU2DB1108EE6609DB783B2EC606B3BFDD544A7D4C2B3
	U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  ___U24fieldU2DB1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_16;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D1024 <PrivateImplementationDetails>::U24fieldU2D14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD
	U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  ___U24fieldU2D14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_17;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D1024 <PrivateImplementationDetails>::U24fieldU2DB718C95C87C6B65DFA29D58A10442CEC9EBBDF1F
	U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  ___U24fieldU2DB718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_18;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D1024 <PrivateImplementationDetails>::U24fieldU2D124B1C35B19149213F8F7D40AA8E0ABA15DD70EC
	U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  ___U24fieldU2D124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_19;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D16 <PrivateImplementationDetails>::U24fieldU2D4FFC8339E09825A68B861995F9C660EB11DBF13D
	U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  ___U24fieldU2D4FFC8339E09825A68B861995F9C660EB11DBF13D_20;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D96 <PrivateImplementationDetails>::U24fieldU2DC105B70BED997DB5D36E1D2E84C1EFCB445A428C
	U24ArrayTypeU3D96_t5C15B6692DC57B36771160CFA909A1E1B6A9E4A4  ___U24fieldU2DC105B70BED997DB5D36E1D2E84C1EFCB445A428C_21;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D56 <PrivateImplementationDetails>::U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82
	U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803  ___U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_22;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D16 <PrivateImplementationDetails>::U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D
	U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  ___U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_23;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D48 <PrivateImplementationDetails>::U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283
	U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  ___U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283_24;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2D04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2D04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_25;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2D3D6EB645BC212077C1B37A3A32CA2A62F7B39018
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2D3D6EB645BC212077C1B37A3A32CA2A62F7B39018_26;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2D821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2D821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_27;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2D64354464C9074B5BB4369689AAA131961CD1EF19
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2D64354464C9074B5BB4369689AAA131961CD1EF19_28;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2D3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2D3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_29;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2D6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2D6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_30;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2DFC9EEBC457831129D4AF4FF84333B481F4BED60E
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2DFC9EEBC457831129D4AF4FF84333B481F4BED60E_31;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2D4C44594E2C603D85EC6195B1A7A6C5876CBB58E2
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2D4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_32;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D128 <PrivateImplementationDetails>::U24fieldU2DD3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13
	U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  ___U24fieldU2DD3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_33;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D128 <PrivateImplementationDetails>::U24fieldU2DCC53D7FE00E6AC1385AF09521629229467BCCC86
	U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  ___U24fieldU2DCC53D7FE00E6AC1385AF09521629229467BCCC86_34;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D128 <PrivateImplementationDetails>::U24fieldU2D2BA9E4B370D477F8C7FE286262D7ADC69CAF290E
	U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  ___U24fieldU2D2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_35;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D128 <PrivateImplementationDetails>::U24fieldU2D1005BA20F99323E3F050E781BB81D1A4479AB037
	U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  ___U24fieldU2D1005BA20F99323E3F050E781BB81D1A4479AB037_36;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D128 <PrivateImplementationDetails>::U24fieldU2DE17D18DCD8392C99D47823F8CB9F43896D115FB8
	U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  ___U24fieldU2DE17D18DCD8392C99D47823F8CB9F43896D115FB8_37;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D128 <PrivateImplementationDetails>::U24fieldU2D7CFF7A50C8F8981091791CDB210243E8F465BC80
	U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  ___U24fieldU2D7CFF7A50C8F8981091791CDB210243E8F465BC80_38;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D128 <PrivateImplementationDetails>::U24fieldU2D3A38ADC6BCFB84DE23160C1E50212DACBCD25A11
	U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  ___U24fieldU2D3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_39;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D1024 <PrivateImplementationDetails>::U24fieldU2DD9642D3FF9879EC5C4BB28AE7001CEE3D43956AB
	U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  ___U24fieldU2DD9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_40;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D1024 <PrivateImplementationDetails>::U24fieldU2D8C7FEE53346CDB1B119FCAD8D605F476400A03CE
	U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  ___U24fieldU2D8C7FEE53346CDB1B119FCAD8D605F476400A03CE_41;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D1024 <PrivateImplementationDetails>::U24fieldU2DEEDBCB52C67688DE5F5FD9209E8A25BC786A2430
	U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  ___U24fieldU2DEEDBCB52C67688DE5F5FD9209E8A25BC786A2430_42;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D1024 <PrivateImplementationDetails>::U24fieldU2DF8AB5CA414AD9084F3E8B8D887217E6DFC32C62C
	U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  ___U24fieldU2DF8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_43;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D64 <PrivateImplementationDetails>::U24fieldU2D641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776
	U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05  ___U24fieldU2D641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_44;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D60 <PrivateImplementationDetails>::U24fieldU2D55BCCC431843040DD1427CB495B2FD469C4D4D20
	U24ArrayTypeU3D60_tD3F93F1391B60ED8B4F85262374C3FBC6BB2F4CF  ___U24fieldU2D55BCCC431843040DD1427CB495B2FD469C4D4D20_45;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D8 <PrivateImplementationDetails>::U24fieldU2D3C3A13E15C81AD114ECBA973C706DCD985BF18D6
	U24ArrayTypeU3D8_t164480F6E3D5CD24BED4D07818C671A634F568EA  ___U24fieldU2D3C3A13E15C81AD114ECBA973C706DCD985BF18D6_46;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D4 <PrivateImplementationDetails>::U24fieldU2D07B35CC0531C638EA1581B73756B3A11272C1D91
	U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9  ___U24fieldU2D07B35CC0531C638EA1581B73756B3A11272C1D91_47;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D4 <PrivateImplementationDetails>::U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277
	U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9  ___U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277_48;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D4 <PrivateImplementationDetails>::U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C
	U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9  ___U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_49;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D32 <PrivateImplementationDetails>::U24fieldU2DEE0F12B14397A7DF4588BEA8AA9B022754F4DA1B
	U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  ___U24fieldU2DEE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_50;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D20 <PrivateImplementationDetails>::U24fieldU2DD7231C06B1D6276752359120E26EAE206A7F74F9
	U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  ___U24fieldU2DD7231C06B1D6276752359120E26EAE206A7F74F9_51;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D20 <PrivateImplementationDetails>::U24fieldU2DAA7973F07CDE1E6AA10B6970B0072D05F38F0AB2
	U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  ___U24fieldU2DAA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_52;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D16 <PrivateImplementationDetails>::U24fieldU2D2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD
	U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  ___U24fieldU2D2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_53;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D16 <PrivateImplementationDetails>::U24fieldU2D6277CE8FE3A9156D3455749B453AC88191D3C6D6
	U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  ___U24fieldU2D6277CE8FE3A9156D3455749B453AC88191D3C6D6_54;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D16 <PrivateImplementationDetails>::U24fieldU2D9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738
	U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  ___U24fieldU2D9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_55;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D16 <PrivateImplementationDetails>::U24fieldU2D714A94F3805E05CA6C00F9A46489427ABEB25D60
	U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  ___U24fieldU2D714A94F3805E05CA6C00F9A46489427ABEB25D60_56;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D16 <PrivateImplementationDetails>::U24fieldU2DF128744756EEB38C3EAD4A7E8536EC5D3FA430FF
	U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  ___U24fieldU2DF128744756EEB38C3EAD4A7E8536EC5D3FA430FF_57;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D16 <PrivateImplementationDetails>::U24fieldU2D5AB421AC76CECB8E84025172585CB97DE8BECD65
	U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  ___U24fieldU2D5AB421AC76CECB8E84025172585CB97DE8BECD65_58;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D16 <PrivateImplementationDetails>::U24fieldU2D6BBD3A22A185224EE0EBAB0784455E9E245376B7
	U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  ___U24fieldU2D6BBD3A22A185224EE0EBAB0784455E9E245376B7_59;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D7B22115C45C6AD570AFBAB744FA1058DF97CDBC1
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_60;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D460C77B94933562DE2E0E5B4FD72B431DFAEB5B6
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_61;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DCE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DCE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_62;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DA471AF3330805980C7041F978D3CFF8838054E14
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DA471AF3330805980C7041F978D3CFF8838054E14_63;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DC0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DC0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_64;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DAD4075598ACA56EC39C5E575771BBB0CFBCE24EE
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DAD4075598ACA56EC39C5E575771BBB0CFBCE24EE_65;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D9AEFA90D8E67EBAE069B4B6C071A8E867B108B31
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_66;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DA5981DCAA364B0DC9E0385D893A31C2022364075
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DA5981DCAA364B0DC9E0385D893A31C2022364075_67;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D4435D44E1091E6624ED6B6E4FA3C9A8C5C996098
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_68;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DF5AEFD834ADB72DAA720930140E9ECC087FCF389
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DF5AEFD834ADB72DAA720930140E9ECC087FCF389_69;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_70;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D1A43D7FEEED03520E11C4A8996F149705D99C6BB
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D1A43D7FEEED03520E11C4A8996F149705D99C6BB_71;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D4B411385A36907D25D8088AE39AB6AAFA46B0642
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D4B411385A36907D25D8088AE39AB6AAFA46B0642_72;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D29F7A0217340B5682E7DDF98ADAD952E2A360E65
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D29F7A0217340B5682E7DDF98ADAD952E2A360E65_73;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D68178023585F1F782745740AA583CDC778DB31B3
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D68178023585F1F782745740AA583CDC778DB31B3_74;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DE53E13AFB95C5C24DF50875117B7DDCE12937B2E
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DE53E13AFB95C5C24DF50875117B7DDCE12937B2E_75;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D6F39BC29A161CAE5394821B1FDE160EB5229AE71
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D6F39BC29A161CAE5394821B1FDE160EB5229AE71_76;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DD99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DD99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_77;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D50AA269217736906D8469B9191F420DC6B13A36A
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D50AA269217736906D8469B9191F420DC6B13A36A_78;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DC3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DC3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_79;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DB997A0149EBF3CDD050D72AE1784E375A413B128
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DB997A0149EBF3CDD050D72AE1784E375A413B128_80;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_81;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_82;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DAD19F20EECB80A2079F504CB928A26FDE10E8C47
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DAD19F20EECB80A2079F504CB928A26FDE10E8C47_83;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DD129FBC67222EA7D73E90E51E4DCFCA8C7497D67
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DD129FBC67222EA7D73E90E51E4DCFCA8C7497D67_84;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DB3A60EC240A886DA5AFD600CC73AE12514A881E8
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DB3A60EC240A886DA5AFD600CC73AE12514A881E8_85;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D512F92F4041B190727A330E2A6CC39E5D9EA06E6
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D512F92F4041B190727A330E2A6CC39E5D9EA06E6_86;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DAD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DAD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_87;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D03D9F1A66AED1E059B1609A09E435B708A88C8B8
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D03D9F1A66AED1E059B1609A09E435B708A88C8B8_88;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D26853A2C322BBAD5BBD886C60A32BBBCFE847F00
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D26853A2C322BBAD5BBD886C60A32BBBCFE847F00_89;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DD31171F7904EB3247DD4834E43B47B1E2DCB97AC
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DD31171F7904EB3247DD4834E43B47B1E2DCB97AC_90;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D5EF7F909EFC731E811E21521A43A80FB5AC0B229
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D5EF7F909EFC731E811E21521A43A80FB5AC0B229_91;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D372040F482ABADADF58EF0C31A6A8BE386AF8A50
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D372040F482ABADADF58EF0C31A6A8BE386AF8A50_92;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D7878E9E7126B2BDF365429C31842AE1903CD2DFF
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D7878E9E7126B2BDF365429C31842AE1903CD2DFF_93;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D882888781BC0DC17021FB4F11BA783038C83B313
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D882888781BC0DC17021FB4F11BA783038C83B313_94;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DF3BF6F581A24C57F2FFF3D2FD3290FD102BB8566
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DF3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_95;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DC2FEEB3C521ADDD49A534A0876BA97FF5894476E
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DC2FEEB3C521ADDD49A534A0876BA97FF5894476E_96;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DC991C784E7697AD0F91A159F03727BF4621A5AB8
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DC991C784E7697AD0F91A159F03727BF4621A5AB8_97;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DD860D5BD327368D1D4174620FE2E4A91FE9AADEC
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DD860D5BD327368D1D4174620FE2E4A91FE9AADEC_98;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DA696E1EE8632C559732B81052E4D2993B8783877
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DA696E1EE8632C559732B81052E4D2993B8783877_99;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D672385C1D6A6C84A1AC3588540B09C4AE3B87DDC
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_100;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_101;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D42C3E89412F11AA94E57C09EFB4B2B415C1AAB58
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_102;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DC2D514B39C8DFA25365195A0759A5AE28D9F2A87
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DC2D514B39C8DFA25365195A0759A5AE28D9F2A87_103;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D613CFAEE025A3AF3C6D13DEB22E298C1925C31B5
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_104;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DB5E8BA68953A5283AD953094F0F391FA4502A3FA
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DB5E8BA68953A5283AD953094F0F391FA4502A3FA_105;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D35E6464339FFAE0D3777B12A371F82D2D1F668CA
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D35E6464339FFAE0D3777B12A371F82D2D1F668CA_106;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D6DEB7F74818574642B0B824B9C08B366C962A360
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D6DEB7F74818574642B0B824B9C08B366C962A360_107;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DBEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DBEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_108;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D5D41C56232C500092E99AC044D3C5C442B1C834F
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D5D41C56232C500092E99AC044D3C5C442B1C834F_109;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DF63639527E877A2CBCB26FFD41D4A59470BFF8C8
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DF63639527E877A2CBCB26FFD41D4A59470BFF8C8_110;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DA72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DA72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_111;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DC4B266E68FA20D0D222D86ADAD31EBB55118CD21
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DC4B266E68FA20D0D222D86ADAD31EBB55118CD21_112;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2D6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_113;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2DC95356610D5583976B69017BED7048EB50121B90
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2DC95356610D5583976B69017BED7048EB50121B90_114;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D32 <PrivateImplementationDetails>::U24fieldU2DA4313AAA146ACFCA88681B7BFC3D644005F3792B
	U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  ___U24fieldU2DA4313AAA146ACFCA88681B7BFC3D644005F3792B_115;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D512 <PrivateImplementationDetails>::U24fieldU2DCA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99
	U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6  ___U24fieldU2DCA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_116;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D512 <PrivateImplementationDetails>::U24fieldU2D239B59488F1CE7EBE225785FDC22A8E3102A2E82
	U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6  ___U24fieldU2D239B59488F1CE7EBE225785FDC22A8E3102A2E82_117;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D1024 <PrivateImplementationDetails>::U24fieldU2D310EFB639F3C7677A2A82B54EEED1124ED69E9A3
	U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  ___U24fieldU2D310EFB639F3C7677A2A82B54EEED1124ED69E9A3_118;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D512 <PrivateImplementationDetails>::U24fieldU2D428007959831954B0C2DCFAF9DD641D629B00DBF
	U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6  ___U24fieldU2D428007959831954B0C2DCFAF9DD641D629B00DBF_119;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D4096 <PrivateImplementationDetails>::U24fieldU2DFA5B1C8B2F287078ED719C15595DB729BDB85911
	U24ArrayTypeU3D4096_tE76E581E9D7FCAFF3FDB2A1A2F45C247994B04A5  ___U24fieldU2DFA5B1C8B2F287078ED719C15595DB729BDB85911_120;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D32 <PrivateImplementationDetails>::U24fieldU2D86B0F85AC13B58F88DEFFD8FD6EE095438B98F10
	U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  ___U24fieldU2D86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_121;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D64 <PrivateImplementationDetails>::U24fieldU2DEAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9
	U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05  ___U24fieldU2DEAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_122;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D32 <PrivateImplementationDetails>::U24fieldU2D6AAC0DB543C50F09E879F5B9F757319773564CE1
	U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  ___U24fieldU2D6AAC0DB543C50F09E879F5B9F757319773564CE1_123;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D16 <PrivateImplementationDetails>::U24fieldU2DFAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05
	U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  ___U24fieldU2DFAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_124;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D32 <PrivateImplementationDetails>::U24fieldU2D068B2E17352B5B9FF693CAE83421B679E0342A5C
	U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  ___U24fieldU2D068B2E17352B5B9FF693CAE83421B679E0342A5C_125;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D32 <PrivateImplementationDetails>::U24fieldU2DF2842E2653F6C5E55959B5EC5E07ABAFC0191FB0
	U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  ___U24fieldU2DF2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_126;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D20 <PrivateImplementationDetails>::U24fieldU2D35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD
	U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  ___U24fieldU2D35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_127;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D40 <PrivateImplementationDetails>::U24fieldU2D310FB325A3EA3EA527B55C2F08544D1CB92C19F4
	U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8  ___U24fieldU2D310FB325A3EA3EA527B55C2F08544D1CB92C19F4_128;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D28 <PrivateImplementationDetails>::U24fieldU2DC132685022CE310ACFD3A883E0A57033A482A959
	U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  ___U24fieldU2DC132685022CE310ACFD3A883E0A57033A482A959_129;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D20 <PrivateImplementationDetails>::U24fieldU2D27FED0F92A97C41B08D3115553BBDC064F417B6E
	U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  ___U24fieldU2D27FED0F92A97C41B08D3115553BBDC064F417B6E_130;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D40 <PrivateImplementationDetails>::U24fieldU2D60A08108A32C9D3F263B2F42095A2694B7C1C1EF
	U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8  ___U24fieldU2D60A08108A32C9D3F263B2F42095A2694B7C1C1EF_131;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D28 <PrivateImplementationDetails>::U24fieldU2D32ECB35FF8400B4E56FF5E09588FB20DD60350E7
	U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  ___U24fieldU2D32ECB35FF8400B4E56FF5E09588FB20DD60350E7_132;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D24 <PrivateImplementationDetails>::U24fieldU2DFB3C663794DD23F500825FF78450D198FE338938
	U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  ___U24fieldU2DFB3C663794DD23F500825FF78450D198FE338938_133;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D48 <PrivateImplementationDetails>::U24fieldU2DC5515C87D04DC0D00C7984096F5E35B4944C1CB6
	U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  ___U24fieldU2DC5515C87D04DC0D00C7984096F5E35B4944C1CB6_134;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D32 <PrivateImplementationDetails>::U24fieldU2D8330271815E046D369E0B1F7673D308739FDCC07
	U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  ___U24fieldU2D8330271815E046D369E0B1F7673D308739FDCC07_135;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D24 <PrivateImplementationDetails>::U24fieldU2DD6898715AE96BC2F82A7BBA76E2BFC7100E282C3
	U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  ___U24fieldU2DD6898715AE96BC2F82A7BBA76E2BFC7100E282C3_136;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D48 <PrivateImplementationDetails>::U24fieldU2DA53306F44DF494A019EA1487807B59CA336BF024
	U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  ___U24fieldU2DA53306F44DF494A019EA1487807B59CA336BF024_137;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D36 <PrivateImplementationDetails>::U24fieldU2D0982B1B45B764F2694ABC3DE57204AC898651429
	U24ArrayTypeU3D36_t9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B  ___U24fieldU2D0982B1B45B764F2694ABC3DE57204AC898651429_138;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D28 <PrivateImplementationDetails>::U24fieldU2D126589410FF9CA1510B9950BF0E79E5BFD60000B
	U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  ___U24fieldU2D126589410FF9CA1510B9950BF0E79E5BFD60000B_139;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D56 <PrivateImplementationDetails>::U24fieldU2DE647D32D165F3510693DF9787DC98E0A0B63C5C2
	U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803  ___U24fieldU2DE647D32D165F3510693DF9787DC98E0A0B63C5C2_140;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D36 <PrivateImplementationDetails>::U24fieldU2D66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47
	U24ArrayTypeU3D36_t9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B  ___U24fieldU2D66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_141;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D28 <PrivateImplementationDetails>::U24fieldU2D059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0
	U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  ___U24fieldU2D059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_142;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D28 <PrivateImplementationDetails>::U24fieldU2D1E3842329C5294DBE1DF588A77C68B35C6AF83BF
	U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  ___U24fieldU2D1E3842329C5294DBE1DF588A77C68B35C6AF83BF_143;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D56 <PrivateImplementationDetails>::U24fieldU2D8C9BE3C02B5604C5CBF6A03E8032549588A6ED54
	U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803  ___U24fieldU2D8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_144;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D44 <PrivateImplementationDetails>::U24fieldU2DEBE167F7962841FA83451C9C1663416D69AA5294
	U24ArrayTypeU3D44_t156A45D15629E5CD523F90BAE6DB67B5ACBE097F  ___U24fieldU2DEBE167F7962841FA83451C9C1663416D69AA5294_145;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D32 <PrivateImplementationDetails>::U24fieldU2D2CABEB86D5B3D362822AF2E5D136A10A17AD85DA
	U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  ___U24fieldU2D2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_146;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D64 <PrivateImplementationDetails>::U24fieldU2D79A213B796D2AD7A89C2071B0732B78207F5CE01
	U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05  ___U24fieldU2D79A213B796D2AD7A89C2071B0732B78207F5CE01_147;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D40 <PrivateImplementationDetails>::U24fieldU2DBE8E2513259482B6F307AC07F23F5D9FB4841EAA
	U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8  ___U24fieldU2DBE8E2513259482B6F307AC07F23F5D9FB4841EAA_148;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D32 <PrivateImplementationDetails>::U24fieldU2D083DE622A9A685DC50D8D5653CB388A41343C8EC
	U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  ___U24fieldU2D083DE622A9A685DC50D8D5653CB388A41343C8EC_149;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D64 <PrivateImplementationDetails>::U24fieldU2D214F93D9222D60794CE1EA0A10389885C5CA9824
	U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05  ___U24fieldU2D214F93D9222D60794CE1EA0A10389885C5CA9824_150;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D48 <PrivateImplementationDetails>::U24fieldU2D095B351FE2104237B032546280C98C9804D331C5
	U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  ___U24fieldU2D095B351FE2104237B032546280C98C9804D331C5_151;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D96 <PrivateImplementationDetails>::U24fieldU2DCE39574ADC95015A9B5E0475EB65EE8F32353FD4
	U24ArrayTypeU3D96_t5C15B6692DC57B36771160CFA909A1E1B6A9E4A4  ___U24fieldU2DCE39574ADC95015A9B5E0475EB65EE8F32353FD4_152;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D68 <PrivateImplementationDetails>::U24fieldU2DEF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B
	U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5  ___U24fieldU2DEF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_153;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D68 <PrivateImplementationDetails>::U24fieldU2D96ADC3934F8492C827987DFEE3B4DD4EF1738E78
	U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5  ___U24fieldU2D96ADC3934F8492C827987DFEE3B4DD4EF1738E78_154;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D24 <PrivateImplementationDetails>::U24fieldU2DCBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965
	U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  ___U24fieldU2DCBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_155;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D24 <PrivateImplementationDetails>::U24fieldU2DBCE617693C33CE2C76FE00F449CA910E4C6E117E
	U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  ___U24fieldU2DBCE617693C33CE2C76FE00F449CA910E4C6E117E_156;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2DE0219F11D9EECC43022AA94967780250AC270D4B
	U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  ___U24fieldU2DE0219F11D9EECC43022AA94967780250AC270D4B_157;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D40 <PrivateImplementationDetails>::U24fieldU2D5E7F55149EC07597C76E6E3CD9F62274214061E6
	U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8  ___U24fieldU2D5E7F55149EC07597C76E6E3CD9F62274214061E6_158;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D72 <PrivateImplementationDetails>::U24fieldU2D0D0825E62E82DBEBFAD598623694129548E24C9C
	U24ArrayTypeU3D72_t2220FD4A9A26B7F357806CAF46D79E53E4F11D64  ___U24fieldU2D0D0825E62E82DBEBFAD598623694129548E24C9C_159;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D24 <PrivateImplementationDetails>::U24fieldU2D17E54FCA28103DF892BBB946DDEED4B061D7F5C7
	U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  ___U24fieldU2D17E54FCA28103DF892BBB946DDEED4B061D7F5C7_160;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D16 <PrivateImplementationDetails>::U24fieldU2DFE5567E8D769550852182CDF69D74BB16DFF8E29
	U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  ___U24fieldU2DFE5567E8D769550852182CDF69D74BB16DFF8E29_161;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D68 <PrivateImplementationDetails>::U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF
	U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5  ___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_162;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D76 <PrivateImplementationDetails>::U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38
	U24ArrayTypeU3D76_tA66D1F76A98126C3A8C881FB7A1C3D6647924CC3  ___U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_163;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D6144 <PrivateImplementationDetails>::U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD
	U24ArrayTypeU3D6144_t0FA5B30A17373E7B9445630D8FD58FD781E5DA6E  ___U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_164;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D384 <PrivateImplementationDetails>::U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401
	U24ArrayTypeU3D384_t2196F8B9AC0E7C9D619BA635541A7E6C3D3A6891  ___U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_165;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D124 <PrivateImplementationDetails>::U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E
	U24ArrayTypeU3D124_tC53BBB2379F45A189B7240430D076326E331393F  ___U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_166;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D124 <PrivateImplementationDetails>::U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A
	U24ArrayTypeU3D124_tC53BBB2379F45A189B7240430D076326E331393F  ___U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_167;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D120 <PrivateImplementationDetails>::U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F
	U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B  ___U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_168;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D120 <PrivateImplementationDetails>::U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F
	U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B  ___U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_169;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D1152 <PrivateImplementationDetails>::U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB
	U24ArrayTypeU3D1152_t26C03C6F9BF348D112C4FCF00BB0EC8807B477FF  ___U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_170;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D120 <PrivateImplementationDetails>::U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014
	U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B  ___U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_171;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D116 <PrivateImplementationDetails>::U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA
	U24ArrayTypeU3D116_t3D3A23342468E840802FEE05074D0A8F384077CF  ___U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_172;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D76 <PrivateImplementationDetails>::U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8
	U24ArrayTypeU3D76_tA66D1F76A98126C3A8C881FB7A1C3D6647924CC3  ___U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_173;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D20 <PrivateImplementationDetails>::U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4
	U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  ___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_174;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D512 <PrivateImplementationDetails>::U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3
	U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6  ___U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_175;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D256 <PrivateImplementationDetails>::U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6
	U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  ___U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_176;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D116 <PrivateImplementationDetails>::U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71
	U24ArrayTypeU3D116_t3D3A23342468E840802FEE05074D0A8F384077CF  ___U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_177;
	// <PrivateImplementationDetails>_U24ArrayTypeU3D120 <PrivateImplementationDetails>::U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1
	U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B  ___U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_178;

public:
	inline static int32_t get_offset_of_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0)); }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  get_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0() const { return ___U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0; }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 * get_address_of_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0() { return &___U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0; }
	inline void set_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0(U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  value)
	{
		___U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DCC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_1)); }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  get_U24fieldU2DCC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_1() const { return ___U24fieldU2DCC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_1; }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 * get_address_of_U24fieldU2DCC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_1() { return &___U24fieldU2DCC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_1; }
	inline void set_U24fieldU2DCC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_1(U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  value)
	{
		___U24fieldU2DCC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_1 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291_2)); }
	inline U24ArrayTypeU3D640_t2FF86352B1300B97F0AC45DBCF28BFFF74F29242  get_U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291_2() const { return ___U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291_2; }
	inline U24ArrayTypeU3D640_t2FF86352B1300B97F0AC45DBCF28BFFF74F29242 * get_address_of_U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291_2() { return &___U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291_2; }
	inline void set_U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291_2(U24ArrayTypeU3D640_t2FF86352B1300B97F0AC45DBCF28BFFF74F29242  value)
	{
		___U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291_2 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD2C5BAE967587C6F3D9F2C4551911E0575A1101F_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DD2C5BAE967587C6F3D9F2C4551911E0575A1101F_3)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2DD2C5BAE967587C6F3D9F2C4551911E0575A1101F_3() const { return ___U24fieldU2DD2C5BAE967587C6F3D9F2C4551911E0575A1101F_3; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2DD2C5BAE967587C6F3D9F2C4551911E0575A1101F_3() { return &___U24fieldU2DD2C5BAE967587C6F3D9F2C4551911E0575A1101F_3; }
	inline void set_U24fieldU2DD2C5BAE967587C6F3D9F2C4551911E0575A1101F_3(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2DD2C5BAE967587C6F3D9F2C4551911E0575A1101F_3 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2_4)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2_4() const { return ___U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2_4; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2_4() { return &___U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2_4; }
	inline void set_U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2_4(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2_4 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA0FABB8173BA247898A9FA267D0CE05500B667A0_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DA0FABB8173BA247898A9FA267D0CE05500B667A0_5)); }
	inline U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99  get_U24fieldU2DA0FABB8173BA247898A9FA267D0CE05500B667A0_5() const { return ___U24fieldU2DA0FABB8173BA247898A9FA267D0CE05500B667A0_5; }
	inline U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99 * get_address_of_U24fieldU2DA0FABB8173BA247898A9FA267D0CE05500B667A0_5() { return &___U24fieldU2DA0FABB8173BA247898A9FA267D0CE05500B667A0_5; }
	inline void set_U24fieldU2DA0FABB8173BA247898A9FA267D0CE05500B667A0_5(U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99  value)
	{
		___U24fieldU2DA0FABB8173BA247898A9FA267D0CE05500B667A0_5 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D467C6758F235D3193618192A64129CBB602C9067_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D467C6758F235D3193618192A64129CBB602C9067_6)); }
	inline U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99  get_U24fieldU2D467C6758F235D3193618192A64129CBB602C9067_6() const { return ___U24fieldU2D467C6758F235D3193618192A64129CBB602C9067_6; }
	inline U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99 * get_address_of_U24fieldU2D467C6758F235D3193618192A64129CBB602C9067_6() { return &___U24fieldU2D467C6758F235D3193618192A64129CBB602C9067_6; }
	inline void set_U24fieldU2D467C6758F235D3193618192A64129CBB602C9067_6(U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99  value)
	{
		___U24fieldU2D467C6758F235D3193618192A64129CBB602C9067_6 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_7)); }
	inline U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99  get_U24fieldU2D1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_7() const { return ___U24fieldU2D1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_7; }
	inline U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99 * get_address_of_U24fieldU2D1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_7() { return &___U24fieldU2D1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_7; }
	inline void set_U24fieldU2D1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_7(U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99  value)
	{
		___U24fieldU2D1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_7 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC079C42AC966756C902EC38C4D7989F3C20D3625_8() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DC079C42AC966756C902EC38C4D7989F3C20D3625_8)); }
	inline U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99  get_U24fieldU2DC079C42AC966756C902EC38C4D7989F3C20D3625_8() const { return ___U24fieldU2DC079C42AC966756C902EC38C4D7989F3C20D3625_8; }
	inline U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99 * get_address_of_U24fieldU2DC079C42AC966756C902EC38C4D7989F3C20D3625_8() { return &___U24fieldU2DC079C42AC966756C902EC38C4D7989F3C20D3625_8; }
	inline void set_U24fieldU2DC079C42AC966756C902EC38C4D7989F3C20D3625_8(U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99  value)
	{
		___U24fieldU2DC079C42AC966756C902EC38C4D7989F3C20D3625_8 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_9() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_9)); }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  get_U24fieldU2D840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_9() const { return ___U24fieldU2D840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_9; }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 * get_address_of_U24fieldU2D840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_9() { return &___U24fieldU2D840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_9; }
	inline void set_U24fieldU2D840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_9(U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  value)
	{
		___U24fieldU2D840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_9 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_10() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_10)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_10() const { return ___U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_10; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_10() { return &___U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_10; }
	inline void set_U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_10(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_10 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939_11() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939_11)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939_11() const { return ___U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939_11; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939_11() { return &___U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939_11; }
	inline void set_U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939_11(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939_11 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF83C7B497B14628C1D0DB3FA2FD89BD5C0D5138A_12() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DF83C7B497B14628C1D0DB3FA2FD89BD5C0D5138A_12)); }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  get_U24fieldU2DF83C7B497B14628C1D0DB3FA2FD89BD5C0D5138A_12() const { return ___U24fieldU2DF83C7B497B14628C1D0DB3FA2FD89BD5C0D5138A_12; }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 * get_address_of_U24fieldU2DF83C7B497B14628C1D0DB3FA2FD89BD5C0D5138A_12() { return &___U24fieldU2DF83C7B497B14628C1D0DB3FA2FD89BD5C0D5138A_12; }
	inline void set_U24fieldU2DF83C7B497B14628C1D0DB3FA2FD89BD5C0D5138A_12(U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  value)
	{
		___U24fieldU2DF83C7B497B14628C1D0DB3FA2FD89BD5C0D5138A_12 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB68637EF60D499620B99E336C59E4865FFC4C5D7_13() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DB68637EF60D499620B99E336C59E4865FFC4C5D7_13)); }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  get_U24fieldU2DB68637EF60D499620B99E336C59E4865FFC4C5D7_13() const { return ___U24fieldU2DB68637EF60D499620B99E336C59E4865FFC4C5D7_13; }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 * get_address_of_U24fieldU2DB68637EF60D499620B99E336C59E4865FFC4C5D7_13() { return &___U24fieldU2DB68637EF60D499620B99E336C59E4865FFC4C5D7_13; }
	inline void set_U24fieldU2DB68637EF60D499620B99E336C59E4865FFC4C5D7_13(U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  value)
	{
		___U24fieldU2DB68637EF60D499620B99E336C59E4865FFC4C5D7_13 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1AFB455399A50580CF1039188ABA6BE82F309543_14() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D1AFB455399A50580CF1039188ABA6BE82F309543_14)); }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  get_U24fieldU2D1AFB455399A50580CF1039188ABA6BE82F309543_14() const { return ___U24fieldU2D1AFB455399A50580CF1039188ABA6BE82F309543_14; }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 * get_address_of_U24fieldU2D1AFB455399A50580CF1039188ABA6BE82F309543_14() { return &___U24fieldU2D1AFB455399A50580CF1039188ABA6BE82F309543_14; }
	inline void set_U24fieldU2D1AFB455399A50580CF1039188ABA6BE82F309543_14(U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  value)
	{
		___U24fieldU2D1AFB455399A50580CF1039188ABA6BE82F309543_14 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAE843E1C1136C908565A6D4E04E8564B69465B3B_15() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DAE843E1C1136C908565A6D4E04E8564B69465B3B_15)); }
	inline U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  get_U24fieldU2DAE843E1C1136C908565A6D4E04E8564B69465B3B_15() const { return ___U24fieldU2DAE843E1C1136C908565A6D4E04E8564B69465B3B_15; }
	inline U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3 * get_address_of_U24fieldU2DAE843E1C1136C908565A6D4E04E8564B69465B3B_15() { return &___U24fieldU2DAE843E1C1136C908565A6D4E04E8564B69465B3B_15; }
	inline void set_U24fieldU2DAE843E1C1136C908565A6D4E04E8564B69465B3B_15(U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  value)
	{
		___U24fieldU2DAE843E1C1136C908565A6D4E04E8564B69465B3B_15 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_16() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DB1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_16)); }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  get_U24fieldU2DB1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_16() const { return ___U24fieldU2DB1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_16; }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 * get_address_of_U24fieldU2DB1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_16() { return &___U24fieldU2DB1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_16; }
	inline void set_U24fieldU2DB1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_16(U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  value)
	{
		___U24fieldU2DB1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_16 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_17() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_17)); }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  get_U24fieldU2D14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_17() const { return ___U24fieldU2D14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_17; }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 * get_address_of_U24fieldU2D14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_17() { return &___U24fieldU2D14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_17; }
	inline void set_U24fieldU2D14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_17(U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  value)
	{
		___U24fieldU2D14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_17 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_18() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DB718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_18)); }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  get_U24fieldU2DB718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_18() const { return ___U24fieldU2DB718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_18; }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 * get_address_of_U24fieldU2DB718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_18() { return &___U24fieldU2DB718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_18; }
	inline void set_U24fieldU2DB718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_18(U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  value)
	{
		___U24fieldU2DB718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_18 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_19() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_19)); }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  get_U24fieldU2D124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_19() const { return ___U24fieldU2D124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_19; }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 * get_address_of_U24fieldU2D124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_19() { return &___U24fieldU2D124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_19; }
	inline void set_U24fieldU2D124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_19(U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  value)
	{
		___U24fieldU2D124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_19 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D4FFC8339E09825A68B861995F9C660EB11DBF13D_20() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D4FFC8339E09825A68B861995F9C660EB11DBF13D_20)); }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  get_U24fieldU2D4FFC8339E09825A68B861995F9C660EB11DBF13D_20() const { return ___U24fieldU2D4FFC8339E09825A68B861995F9C660EB11DBF13D_20; }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326 * get_address_of_U24fieldU2D4FFC8339E09825A68B861995F9C660EB11DBF13D_20() { return &___U24fieldU2D4FFC8339E09825A68B861995F9C660EB11DBF13D_20; }
	inline void set_U24fieldU2D4FFC8339E09825A68B861995F9C660EB11DBF13D_20(U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  value)
	{
		___U24fieldU2D4FFC8339E09825A68B861995F9C660EB11DBF13D_20 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC105B70BED997DB5D36E1D2E84C1EFCB445A428C_21() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DC105B70BED997DB5D36E1D2E84C1EFCB445A428C_21)); }
	inline U24ArrayTypeU3D96_t5C15B6692DC57B36771160CFA909A1E1B6A9E4A4  get_U24fieldU2DC105B70BED997DB5D36E1D2E84C1EFCB445A428C_21() const { return ___U24fieldU2DC105B70BED997DB5D36E1D2E84C1EFCB445A428C_21; }
	inline U24ArrayTypeU3D96_t5C15B6692DC57B36771160CFA909A1E1B6A9E4A4 * get_address_of_U24fieldU2DC105B70BED997DB5D36E1D2E84C1EFCB445A428C_21() { return &___U24fieldU2DC105B70BED997DB5D36E1D2E84C1EFCB445A428C_21; }
	inline void set_U24fieldU2DC105B70BED997DB5D36E1D2E84C1EFCB445A428C_21(U24ArrayTypeU3D96_t5C15B6692DC57B36771160CFA909A1E1B6A9E4A4  value)
	{
		___U24fieldU2DC105B70BED997DB5D36E1D2E84C1EFCB445A428C_21 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_22() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_22)); }
	inline U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803  get_U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_22() const { return ___U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_22; }
	inline U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803 * get_address_of_U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_22() { return &___U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_22; }
	inline void set_U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_22(U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803  value)
	{
		___U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_22 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_23() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_23)); }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  get_U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_23() const { return ___U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_23; }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326 * get_address_of_U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_23() { return &___U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_23; }
	inline void set_U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_23(U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  value)
	{
		___U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_23 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283_24() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283_24)); }
	inline U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  get_U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283_24() const { return ___U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283_24; }
	inline U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3 * get_address_of_U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283_24() { return &___U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283_24; }
	inline void set_U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283_24(U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  value)
	{
		___U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283_24 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_25() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_25)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2D04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_25() const { return ___U24fieldU2D04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_25; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2D04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_25() { return &___U24fieldU2D04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_25; }
	inline void set_U24fieldU2D04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_25(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2D04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_25 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3D6EB645BC212077C1B37A3A32CA2A62F7B39018_26() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D3D6EB645BC212077C1B37A3A32CA2A62F7B39018_26)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2D3D6EB645BC212077C1B37A3A32CA2A62F7B39018_26() const { return ___U24fieldU2D3D6EB645BC212077C1B37A3A32CA2A62F7B39018_26; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2D3D6EB645BC212077C1B37A3A32CA2A62F7B39018_26() { return &___U24fieldU2D3D6EB645BC212077C1B37A3A32CA2A62F7B39018_26; }
	inline void set_U24fieldU2D3D6EB645BC212077C1B37A3A32CA2A62F7B39018_26(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2D3D6EB645BC212077C1B37A3A32CA2A62F7B39018_26 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_27() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_27)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2D821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_27() const { return ___U24fieldU2D821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_27; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2D821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_27() { return &___U24fieldU2D821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_27; }
	inline void set_U24fieldU2D821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_27(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2D821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_27 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D64354464C9074B5BB4369689AAA131961CD1EF19_28() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D64354464C9074B5BB4369689AAA131961CD1EF19_28)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2D64354464C9074B5BB4369689AAA131961CD1EF19_28() const { return ___U24fieldU2D64354464C9074B5BB4369689AAA131961CD1EF19_28; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2D64354464C9074B5BB4369689AAA131961CD1EF19_28() { return &___U24fieldU2D64354464C9074B5BB4369689AAA131961CD1EF19_28; }
	inline void set_U24fieldU2D64354464C9074B5BB4369689AAA131961CD1EF19_28(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2D64354464C9074B5BB4369689AAA131961CD1EF19_28 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_29() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_29)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2D3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_29() const { return ___U24fieldU2D3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_29; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2D3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_29() { return &___U24fieldU2D3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_29; }
	inline void set_U24fieldU2D3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_29(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2D3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_29 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_30() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_30)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2D6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_30() const { return ___U24fieldU2D6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_30; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2D6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_30() { return &___U24fieldU2D6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_30; }
	inline void set_U24fieldU2D6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_30(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2D6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_30 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DFC9EEBC457831129D4AF4FF84333B481F4BED60E_31() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DFC9EEBC457831129D4AF4FF84333B481F4BED60E_31)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2DFC9EEBC457831129D4AF4FF84333B481F4BED60E_31() const { return ___U24fieldU2DFC9EEBC457831129D4AF4FF84333B481F4BED60E_31; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2DFC9EEBC457831129D4AF4FF84333B481F4BED60E_31() { return &___U24fieldU2DFC9EEBC457831129D4AF4FF84333B481F4BED60E_31; }
	inline void set_U24fieldU2DFC9EEBC457831129D4AF4FF84333B481F4BED60E_31(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2DFC9EEBC457831129D4AF4FF84333B481F4BED60E_31 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_32() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_32)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2D4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_32() const { return ___U24fieldU2D4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_32; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2D4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_32() { return &___U24fieldU2D4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_32; }
	inline void set_U24fieldU2D4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_32(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2D4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_32 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_33() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DD3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_33)); }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  get_U24fieldU2DD3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_33() const { return ___U24fieldU2DD3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_33; }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D * get_address_of_U24fieldU2DD3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_33() { return &___U24fieldU2DD3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_33; }
	inline void set_U24fieldU2DD3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_33(U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  value)
	{
		___U24fieldU2DD3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_33 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCC53D7FE00E6AC1385AF09521629229467BCCC86_34() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DCC53D7FE00E6AC1385AF09521629229467BCCC86_34)); }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  get_U24fieldU2DCC53D7FE00E6AC1385AF09521629229467BCCC86_34() const { return ___U24fieldU2DCC53D7FE00E6AC1385AF09521629229467BCCC86_34; }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D * get_address_of_U24fieldU2DCC53D7FE00E6AC1385AF09521629229467BCCC86_34() { return &___U24fieldU2DCC53D7FE00E6AC1385AF09521629229467BCCC86_34; }
	inline void set_U24fieldU2DCC53D7FE00E6AC1385AF09521629229467BCCC86_34(U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  value)
	{
		___U24fieldU2DCC53D7FE00E6AC1385AF09521629229467BCCC86_34 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_35() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_35)); }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  get_U24fieldU2D2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_35() const { return ___U24fieldU2D2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_35; }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D * get_address_of_U24fieldU2D2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_35() { return &___U24fieldU2D2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_35; }
	inline void set_U24fieldU2D2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_35(U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  value)
	{
		___U24fieldU2D2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_35 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1005BA20F99323E3F050E781BB81D1A4479AB037_36() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D1005BA20F99323E3F050E781BB81D1A4479AB037_36)); }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  get_U24fieldU2D1005BA20F99323E3F050E781BB81D1A4479AB037_36() const { return ___U24fieldU2D1005BA20F99323E3F050E781BB81D1A4479AB037_36; }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D * get_address_of_U24fieldU2D1005BA20F99323E3F050E781BB81D1A4479AB037_36() { return &___U24fieldU2D1005BA20F99323E3F050E781BB81D1A4479AB037_36; }
	inline void set_U24fieldU2D1005BA20F99323E3F050E781BB81D1A4479AB037_36(U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  value)
	{
		___U24fieldU2D1005BA20F99323E3F050E781BB81D1A4479AB037_36 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE17D18DCD8392C99D47823F8CB9F43896D115FB8_37() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DE17D18DCD8392C99D47823F8CB9F43896D115FB8_37)); }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  get_U24fieldU2DE17D18DCD8392C99D47823F8CB9F43896D115FB8_37() const { return ___U24fieldU2DE17D18DCD8392C99D47823F8CB9F43896D115FB8_37; }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D * get_address_of_U24fieldU2DE17D18DCD8392C99D47823F8CB9F43896D115FB8_37() { return &___U24fieldU2DE17D18DCD8392C99D47823F8CB9F43896D115FB8_37; }
	inline void set_U24fieldU2DE17D18DCD8392C99D47823F8CB9F43896D115FB8_37(U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  value)
	{
		___U24fieldU2DE17D18DCD8392C99D47823F8CB9F43896D115FB8_37 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7CFF7A50C8F8981091791CDB210243E8F465BC80_38() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D7CFF7A50C8F8981091791CDB210243E8F465BC80_38)); }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  get_U24fieldU2D7CFF7A50C8F8981091791CDB210243E8F465BC80_38() const { return ___U24fieldU2D7CFF7A50C8F8981091791CDB210243E8F465BC80_38; }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D * get_address_of_U24fieldU2D7CFF7A50C8F8981091791CDB210243E8F465BC80_38() { return &___U24fieldU2D7CFF7A50C8F8981091791CDB210243E8F465BC80_38; }
	inline void set_U24fieldU2D7CFF7A50C8F8981091791CDB210243E8F465BC80_38(U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  value)
	{
		___U24fieldU2D7CFF7A50C8F8981091791CDB210243E8F465BC80_38 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_39() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_39)); }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  get_U24fieldU2D3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_39() const { return ___U24fieldU2D3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_39; }
	inline U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D * get_address_of_U24fieldU2D3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_39() { return &___U24fieldU2D3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_39; }
	inline void set_U24fieldU2D3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_39(U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D  value)
	{
		___U24fieldU2D3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_39 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_40() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DD9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_40)); }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  get_U24fieldU2DD9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_40() const { return ___U24fieldU2DD9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_40; }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 * get_address_of_U24fieldU2DD9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_40() { return &___U24fieldU2DD9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_40; }
	inline void set_U24fieldU2DD9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_40(U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  value)
	{
		___U24fieldU2DD9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_40 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8C7FEE53346CDB1B119FCAD8D605F476400A03CE_41() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D8C7FEE53346CDB1B119FCAD8D605F476400A03CE_41)); }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  get_U24fieldU2D8C7FEE53346CDB1B119FCAD8D605F476400A03CE_41() const { return ___U24fieldU2D8C7FEE53346CDB1B119FCAD8D605F476400A03CE_41; }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 * get_address_of_U24fieldU2D8C7FEE53346CDB1B119FCAD8D605F476400A03CE_41() { return &___U24fieldU2D8C7FEE53346CDB1B119FCAD8D605F476400A03CE_41; }
	inline void set_U24fieldU2D8C7FEE53346CDB1B119FCAD8D605F476400A03CE_41(U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  value)
	{
		___U24fieldU2D8C7FEE53346CDB1B119FCAD8D605F476400A03CE_41 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DEEDBCB52C67688DE5F5FD9209E8A25BC786A2430_42() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DEEDBCB52C67688DE5F5FD9209E8A25BC786A2430_42)); }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  get_U24fieldU2DEEDBCB52C67688DE5F5FD9209E8A25BC786A2430_42() const { return ___U24fieldU2DEEDBCB52C67688DE5F5FD9209E8A25BC786A2430_42; }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 * get_address_of_U24fieldU2DEEDBCB52C67688DE5F5FD9209E8A25BC786A2430_42() { return &___U24fieldU2DEEDBCB52C67688DE5F5FD9209E8A25BC786A2430_42; }
	inline void set_U24fieldU2DEEDBCB52C67688DE5F5FD9209E8A25BC786A2430_42(U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  value)
	{
		___U24fieldU2DEEDBCB52C67688DE5F5FD9209E8A25BC786A2430_42 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_43() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DF8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_43)); }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  get_U24fieldU2DF8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_43() const { return ___U24fieldU2DF8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_43; }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 * get_address_of_U24fieldU2DF8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_43() { return &___U24fieldU2DF8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_43; }
	inline void set_U24fieldU2DF8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_43(U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  value)
	{
		___U24fieldU2DF8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_43 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_44() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_44)); }
	inline U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05  get_U24fieldU2D641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_44() const { return ___U24fieldU2D641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_44; }
	inline U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05 * get_address_of_U24fieldU2D641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_44() { return &___U24fieldU2D641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_44; }
	inline void set_U24fieldU2D641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_44(U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05  value)
	{
		___U24fieldU2D641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_44 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D55BCCC431843040DD1427CB495B2FD469C4D4D20_45() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D55BCCC431843040DD1427CB495B2FD469C4D4D20_45)); }
	inline U24ArrayTypeU3D60_tD3F93F1391B60ED8B4F85262374C3FBC6BB2F4CF  get_U24fieldU2D55BCCC431843040DD1427CB495B2FD469C4D4D20_45() const { return ___U24fieldU2D55BCCC431843040DD1427CB495B2FD469C4D4D20_45; }
	inline U24ArrayTypeU3D60_tD3F93F1391B60ED8B4F85262374C3FBC6BB2F4CF * get_address_of_U24fieldU2D55BCCC431843040DD1427CB495B2FD469C4D4D20_45() { return &___U24fieldU2D55BCCC431843040DD1427CB495B2FD469C4D4D20_45; }
	inline void set_U24fieldU2D55BCCC431843040DD1427CB495B2FD469C4D4D20_45(U24ArrayTypeU3D60_tD3F93F1391B60ED8B4F85262374C3FBC6BB2F4CF  value)
	{
		___U24fieldU2D55BCCC431843040DD1427CB495B2FD469C4D4D20_45 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3C3A13E15C81AD114ECBA973C706DCD985BF18D6_46() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D3C3A13E15C81AD114ECBA973C706DCD985BF18D6_46)); }
	inline U24ArrayTypeU3D8_t164480F6E3D5CD24BED4D07818C671A634F568EA  get_U24fieldU2D3C3A13E15C81AD114ECBA973C706DCD985BF18D6_46() const { return ___U24fieldU2D3C3A13E15C81AD114ECBA973C706DCD985BF18D6_46; }
	inline U24ArrayTypeU3D8_t164480F6E3D5CD24BED4D07818C671A634F568EA * get_address_of_U24fieldU2D3C3A13E15C81AD114ECBA973C706DCD985BF18D6_46() { return &___U24fieldU2D3C3A13E15C81AD114ECBA973C706DCD985BF18D6_46; }
	inline void set_U24fieldU2D3C3A13E15C81AD114ECBA973C706DCD985BF18D6_46(U24ArrayTypeU3D8_t164480F6E3D5CD24BED4D07818C671A634F568EA  value)
	{
		___U24fieldU2D3C3A13E15C81AD114ECBA973C706DCD985BF18D6_46 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D07B35CC0531C638EA1581B73756B3A11272C1D91_47() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D07B35CC0531C638EA1581B73756B3A11272C1D91_47)); }
	inline U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9  get_U24fieldU2D07B35CC0531C638EA1581B73756B3A11272C1D91_47() const { return ___U24fieldU2D07B35CC0531C638EA1581B73756B3A11272C1D91_47; }
	inline U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9 * get_address_of_U24fieldU2D07B35CC0531C638EA1581B73756B3A11272C1D91_47() { return &___U24fieldU2D07B35CC0531C638EA1581B73756B3A11272C1D91_47; }
	inline void set_U24fieldU2D07B35CC0531C638EA1581B73756B3A11272C1D91_47(U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9  value)
	{
		___U24fieldU2D07B35CC0531C638EA1581B73756B3A11272C1D91_47 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277_48() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277_48)); }
	inline U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9  get_U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277_48() const { return ___U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277_48; }
	inline U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9 * get_address_of_U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277_48() { return &___U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277_48; }
	inline void set_U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277_48(U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9  value)
	{
		___U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277_48 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_49() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_49)); }
	inline U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9  get_U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_49() const { return ___U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_49; }
	inline U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9 * get_address_of_U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_49() { return &___U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_49; }
	inline void set_U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_49(U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9  value)
	{
		___U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_49 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DEE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_50() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DEE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_50)); }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  get_U24fieldU2DEE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_50() const { return ___U24fieldU2DEE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_50; }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 * get_address_of_U24fieldU2DEE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_50() { return &___U24fieldU2DEE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_50; }
	inline void set_U24fieldU2DEE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_50(U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  value)
	{
		___U24fieldU2DEE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_50 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD7231C06B1D6276752359120E26EAE206A7F74F9_51() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DD7231C06B1D6276752359120E26EAE206A7F74F9_51)); }
	inline U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  get_U24fieldU2DD7231C06B1D6276752359120E26EAE206A7F74F9_51() const { return ___U24fieldU2DD7231C06B1D6276752359120E26EAE206A7F74F9_51; }
	inline U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3 * get_address_of_U24fieldU2DD7231C06B1D6276752359120E26EAE206A7F74F9_51() { return &___U24fieldU2DD7231C06B1D6276752359120E26EAE206A7F74F9_51; }
	inline void set_U24fieldU2DD7231C06B1D6276752359120E26EAE206A7F74F9_51(U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  value)
	{
		___U24fieldU2DD7231C06B1D6276752359120E26EAE206A7F74F9_51 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_52() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DAA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_52)); }
	inline U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  get_U24fieldU2DAA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_52() const { return ___U24fieldU2DAA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_52; }
	inline U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3 * get_address_of_U24fieldU2DAA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_52() { return &___U24fieldU2DAA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_52; }
	inline void set_U24fieldU2DAA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_52(U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  value)
	{
		___U24fieldU2DAA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_52 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_53() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_53)); }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  get_U24fieldU2D2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_53() const { return ___U24fieldU2D2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_53; }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326 * get_address_of_U24fieldU2D2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_53() { return &___U24fieldU2D2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_53; }
	inline void set_U24fieldU2D2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_53(U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  value)
	{
		___U24fieldU2D2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_53 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6277CE8FE3A9156D3455749B453AC88191D3C6D6_54() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D6277CE8FE3A9156D3455749B453AC88191D3C6D6_54)); }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  get_U24fieldU2D6277CE8FE3A9156D3455749B453AC88191D3C6D6_54() const { return ___U24fieldU2D6277CE8FE3A9156D3455749B453AC88191D3C6D6_54; }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326 * get_address_of_U24fieldU2D6277CE8FE3A9156D3455749B453AC88191D3C6D6_54() { return &___U24fieldU2D6277CE8FE3A9156D3455749B453AC88191D3C6D6_54; }
	inline void set_U24fieldU2D6277CE8FE3A9156D3455749B453AC88191D3C6D6_54(U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  value)
	{
		___U24fieldU2D6277CE8FE3A9156D3455749B453AC88191D3C6D6_54 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_55() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_55)); }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  get_U24fieldU2D9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_55() const { return ___U24fieldU2D9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_55; }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326 * get_address_of_U24fieldU2D9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_55() { return &___U24fieldU2D9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_55; }
	inline void set_U24fieldU2D9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_55(U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  value)
	{
		___U24fieldU2D9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_55 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D714A94F3805E05CA6C00F9A46489427ABEB25D60_56() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D714A94F3805E05CA6C00F9A46489427ABEB25D60_56)); }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  get_U24fieldU2D714A94F3805E05CA6C00F9A46489427ABEB25D60_56() const { return ___U24fieldU2D714A94F3805E05CA6C00F9A46489427ABEB25D60_56; }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326 * get_address_of_U24fieldU2D714A94F3805E05CA6C00F9A46489427ABEB25D60_56() { return &___U24fieldU2D714A94F3805E05CA6C00F9A46489427ABEB25D60_56; }
	inline void set_U24fieldU2D714A94F3805E05CA6C00F9A46489427ABEB25D60_56(U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  value)
	{
		___U24fieldU2D714A94F3805E05CA6C00F9A46489427ABEB25D60_56 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF128744756EEB38C3EAD4A7E8536EC5D3FA430FF_57() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DF128744756EEB38C3EAD4A7E8536EC5D3FA430FF_57)); }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  get_U24fieldU2DF128744756EEB38C3EAD4A7E8536EC5D3FA430FF_57() const { return ___U24fieldU2DF128744756EEB38C3EAD4A7E8536EC5D3FA430FF_57; }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326 * get_address_of_U24fieldU2DF128744756EEB38C3EAD4A7E8536EC5D3FA430FF_57() { return &___U24fieldU2DF128744756EEB38C3EAD4A7E8536EC5D3FA430FF_57; }
	inline void set_U24fieldU2DF128744756EEB38C3EAD4A7E8536EC5D3FA430FF_57(U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  value)
	{
		___U24fieldU2DF128744756EEB38C3EAD4A7E8536EC5D3FA430FF_57 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5AB421AC76CECB8E84025172585CB97DE8BECD65_58() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D5AB421AC76CECB8E84025172585CB97DE8BECD65_58)); }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  get_U24fieldU2D5AB421AC76CECB8E84025172585CB97DE8BECD65_58() const { return ___U24fieldU2D5AB421AC76CECB8E84025172585CB97DE8BECD65_58; }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326 * get_address_of_U24fieldU2D5AB421AC76CECB8E84025172585CB97DE8BECD65_58() { return &___U24fieldU2D5AB421AC76CECB8E84025172585CB97DE8BECD65_58; }
	inline void set_U24fieldU2D5AB421AC76CECB8E84025172585CB97DE8BECD65_58(U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  value)
	{
		___U24fieldU2D5AB421AC76CECB8E84025172585CB97DE8BECD65_58 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6BBD3A22A185224EE0EBAB0784455E9E245376B7_59() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D6BBD3A22A185224EE0EBAB0784455E9E245376B7_59)); }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  get_U24fieldU2D6BBD3A22A185224EE0EBAB0784455E9E245376B7_59() const { return ___U24fieldU2D6BBD3A22A185224EE0EBAB0784455E9E245376B7_59; }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326 * get_address_of_U24fieldU2D6BBD3A22A185224EE0EBAB0784455E9E245376B7_59() { return &___U24fieldU2D6BBD3A22A185224EE0EBAB0784455E9E245376B7_59; }
	inline void set_U24fieldU2D6BBD3A22A185224EE0EBAB0784455E9E245376B7_59(U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  value)
	{
		___U24fieldU2D6BBD3A22A185224EE0EBAB0784455E9E245376B7_59 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_60() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_60)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_60() const { return ___U24fieldU2D7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_60; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_60() { return &___U24fieldU2D7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_60; }
	inline void set_U24fieldU2D7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_60(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_60 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_61() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_61)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_61() const { return ___U24fieldU2D460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_61; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_61() { return &___U24fieldU2D460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_61; }
	inline void set_U24fieldU2D460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_61(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_61 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_62() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DCE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_62)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DCE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_62() const { return ___U24fieldU2DCE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_62; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DCE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_62() { return &___U24fieldU2DCE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_62; }
	inline void set_U24fieldU2DCE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_62(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DCE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_62 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA471AF3330805980C7041F978D3CFF8838054E14_63() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DA471AF3330805980C7041F978D3CFF8838054E14_63)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DA471AF3330805980C7041F978D3CFF8838054E14_63() const { return ___U24fieldU2DA471AF3330805980C7041F978D3CFF8838054E14_63; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DA471AF3330805980C7041F978D3CFF8838054E14_63() { return &___U24fieldU2DA471AF3330805980C7041F978D3CFF8838054E14_63; }
	inline void set_U24fieldU2DA471AF3330805980C7041F978D3CFF8838054E14_63(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DA471AF3330805980C7041F978D3CFF8838054E14_63 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_64() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DC0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_64)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DC0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_64() const { return ___U24fieldU2DC0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_64; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DC0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_64() { return &___U24fieldU2DC0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_64; }
	inline void set_U24fieldU2DC0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_64(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DC0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_64 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAD4075598ACA56EC39C5E575771BBB0CFBCE24EE_65() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DAD4075598ACA56EC39C5E575771BBB0CFBCE24EE_65)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DAD4075598ACA56EC39C5E575771BBB0CFBCE24EE_65() const { return ___U24fieldU2DAD4075598ACA56EC39C5E575771BBB0CFBCE24EE_65; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DAD4075598ACA56EC39C5E575771BBB0CFBCE24EE_65() { return &___U24fieldU2DAD4075598ACA56EC39C5E575771BBB0CFBCE24EE_65; }
	inline void set_U24fieldU2DAD4075598ACA56EC39C5E575771BBB0CFBCE24EE_65(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DAD4075598ACA56EC39C5E575771BBB0CFBCE24EE_65 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_66() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_66)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_66() const { return ___U24fieldU2D9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_66; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_66() { return &___U24fieldU2D9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_66; }
	inline void set_U24fieldU2D9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_66(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_66 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA5981DCAA364B0DC9E0385D893A31C2022364075_67() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DA5981DCAA364B0DC9E0385D893A31C2022364075_67)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DA5981DCAA364B0DC9E0385D893A31C2022364075_67() const { return ___U24fieldU2DA5981DCAA364B0DC9E0385D893A31C2022364075_67; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DA5981DCAA364B0DC9E0385D893A31C2022364075_67() { return &___U24fieldU2DA5981DCAA364B0DC9E0385D893A31C2022364075_67; }
	inline void set_U24fieldU2DA5981DCAA364B0DC9E0385D893A31C2022364075_67(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DA5981DCAA364B0DC9E0385D893A31C2022364075_67 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_68() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_68)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_68() const { return ___U24fieldU2D4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_68; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_68() { return &___U24fieldU2D4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_68; }
	inline void set_U24fieldU2D4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_68(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_68 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF5AEFD834ADB72DAA720930140E9ECC087FCF389_69() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DF5AEFD834ADB72DAA720930140E9ECC087FCF389_69)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DF5AEFD834ADB72DAA720930140E9ECC087FCF389_69() const { return ___U24fieldU2DF5AEFD834ADB72DAA720930140E9ECC087FCF389_69; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DF5AEFD834ADB72DAA720930140E9ECC087FCF389_69() { return &___U24fieldU2DF5AEFD834ADB72DAA720930140E9ECC087FCF389_69; }
	inline void set_U24fieldU2DF5AEFD834ADB72DAA720930140E9ECC087FCF389_69(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DF5AEFD834ADB72DAA720930140E9ECC087FCF389_69 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_70() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_70)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_70() const { return ___U24fieldU2D0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_70; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_70() { return &___U24fieldU2D0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_70; }
	inline void set_U24fieldU2D0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_70(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_70 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1A43D7FEEED03520E11C4A8996F149705D99C6BB_71() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D1A43D7FEEED03520E11C4A8996F149705D99C6BB_71)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D1A43D7FEEED03520E11C4A8996F149705D99C6BB_71() const { return ___U24fieldU2D1A43D7FEEED03520E11C4A8996F149705D99C6BB_71; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D1A43D7FEEED03520E11C4A8996F149705D99C6BB_71() { return &___U24fieldU2D1A43D7FEEED03520E11C4A8996F149705D99C6BB_71; }
	inline void set_U24fieldU2D1A43D7FEEED03520E11C4A8996F149705D99C6BB_71(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D1A43D7FEEED03520E11C4A8996F149705D99C6BB_71 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D4B411385A36907D25D8088AE39AB6AAFA46B0642_72() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D4B411385A36907D25D8088AE39AB6AAFA46B0642_72)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D4B411385A36907D25D8088AE39AB6AAFA46B0642_72() const { return ___U24fieldU2D4B411385A36907D25D8088AE39AB6AAFA46B0642_72; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D4B411385A36907D25D8088AE39AB6AAFA46B0642_72() { return &___U24fieldU2D4B411385A36907D25D8088AE39AB6AAFA46B0642_72; }
	inline void set_U24fieldU2D4B411385A36907D25D8088AE39AB6AAFA46B0642_72(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D4B411385A36907D25D8088AE39AB6AAFA46B0642_72 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D29F7A0217340B5682E7DDF98ADAD952E2A360E65_73() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D29F7A0217340B5682E7DDF98ADAD952E2A360E65_73)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D29F7A0217340B5682E7DDF98ADAD952E2A360E65_73() const { return ___U24fieldU2D29F7A0217340B5682E7DDF98ADAD952E2A360E65_73; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D29F7A0217340B5682E7DDF98ADAD952E2A360E65_73() { return &___U24fieldU2D29F7A0217340B5682E7DDF98ADAD952E2A360E65_73; }
	inline void set_U24fieldU2D29F7A0217340B5682E7DDF98ADAD952E2A360E65_73(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D29F7A0217340B5682E7DDF98ADAD952E2A360E65_73 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D68178023585F1F782745740AA583CDC778DB31B3_74() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D68178023585F1F782745740AA583CDC778DB31B3_74)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D68178023585F1F782745740AA583CDC778DB31B3_74() const { return ___U24fieldU2D68178023585F1F782745740AA583CDC778DB31B3_74; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D68178023585F1F782745740AA583CDC778DB31B3_74() { return &___U24fieldU2D68178023585F1F782745740AA583CDC778DB31B3_74; }
	inline void set_U24fieldU2D68178023585F1F782745740AA583CDC778DB31B3_74(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D68178023585F1F782745740AA583CDC778DB31B3_74 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE53E13AFB95C5C24DF50875117B7DDCE12937B2E_75() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DE53E13AFB95C5C24DF50875117B7DDCE12937B2E_75)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DE53E13AFB95C5C24DF50875117B7DDCE12937B2E_75() const { return ___U24fieldU2DE53E13AFB95C5C24DF50875117B7DDCE12937B2E_75; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DE53E13AFB95C5C24DF50875117B7DDCE12937B2E_75() { return &___U24fieldU2DE53E13AFB95C5C24DF50875117B7DDCE12937B2E_75; }
	inline void set_U24fieldU2DE53E13AFB95C5C24DF50875117B7DDCE12937B2E_75(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DE53E13AFB95C5C24DF50875117B7DDCE12937B2E_75 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6F39BC29A161CAE5394821B1FDE160EB5229AE71_76() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D6F39BC29A161CAE5394821B1FDE160EB5229AE71_76)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D6F39BC29A161CAE5394821B1FDE160EB5229AE71_76() const { return ___U24fieldU2D6F39BC29A161CAE5394821B1FDE160EB5229AE71_76; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D6F39BC29A161CAE5394821B1FDE160EB5229AE71_76() { return &___U24fieldU2D6F39BC29A161CAE5394821B1FDE160EB5229AE71_76; }
	inline void set_U24fieldU2D6F39BC29A161CAE5394821B1FDE160EB5229AE71_76(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D6F39BC29A161CAE5394821B1FDE160EB5229AE71_76 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_77() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DD99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_77)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DD99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_77() const { return ___U24fieldU2DD99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_77; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DD99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_77() { return &___U24fieldU2DD99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_77; }
	inline void set_U24fieldU2DD99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_77(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DD99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_77 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D50AA269217736906D8469B9191F420DC6B13A36A_78() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D50AA269217736906D8469B9191F420DC6B13A36A_78)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D50AA269217736906D8469B9191F420DC6B13A36A_78() const { return ___U24fieldU2D50AA269217736906D8469B9191F420DC6B13A36A_78; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D50AA269217736906D8469B9191F420DC6B13A36A_78() { return &___U24fieldU2D50AA269217736906D8469B9191F420DC6B13A36A_78; }
	inline void set_U24fieldU2D50AA269217736906D8469B9191F420DC6B13A36A_78(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D50AA269217736906D8469B9191F420DC6B13A36A_78 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_79() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DC3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_79)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DC3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_79() const { return ___U24fieldU2DC3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_79; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DC3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_79() { return &___U24fieldU2DC3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_79; }
	inline void set_U24fieldU2DC3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_79(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DC3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_79 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB997A0149EBF3CDD050D72AE1784E375A413B128_80() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DB997A0149EBF3CDD050D72AE1784E375A413B128_80)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DB997A0149EBF3CDD050D72AE1784E375A413B128_80() const { return ___U24fieldU2DB997A0149EBF3CDD050D72AE1784E375A413B128_80; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DB997A0149EBF3CDD050D72AE1784E375A413B128_80() { return &___U24fieldU2DB997A0149EBF3CDD050D72AE1784E375A413B128_80; }
	inline void set_U24fieldU2DB997A0149EBF3CDD050D72AE1784E375A413B128_80(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DB997A0149EBF3CDD050D72AE1784E375A413B128_80 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_81() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_81)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_81() const { return ___U24fieldU2D9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_81; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_81() { return &___U24fieldU2D9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_81; }
	inline void set_U24fieldU2D9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_81(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_81 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_82() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_82)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_82() const { return ___U24fieldU2D9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_82; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_82() { return &___U24fieldU2D9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_82; }
	inline void set_U24fieldU2D9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_82(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_82 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAD19F20EECB80A2079F504CB928A26FDE10E8C47_83() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DAD19F20EECB80A2079F504CB928A26FDE10E8C47_83)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DAD19F20EECB80A2079F504CB928A26FDE10E8C47_83() const { return ___U24fieldU2DAD19F20EECB80A2079F504CB928A26FDE10E8C47_83; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DAD19F20EECB80A2079F504CB928A26FDE10E8C47_83() { return &___U24fieldU2DAD19F20EECB80A2079F504CB928A26FDE10E8C47_83; }
	inline void set_U24fieldU2DAD19F20EECB80A2079F504CB928A26FDE10E8C47_83(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DAD19F20EECB80A2079F504CB928A26FDE10E8C47_83 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD129FBC67222EA7D73E90E51E4DCFCA8C7497D67_84() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DD129FBC67222EA7D73E90E51E4DCFCA8C7497D67_84)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DD129FBC67222EA7D73E90E51E4DCFCA8C7497D67_84() const { return ___U24fieldU2DD129FBC67222EA7D73E90E51E4DCFCA8C7497D67_84; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DD129FBC67222EA7D73E90E51E4DCFCA8C7497D67_84() { return &___U24fieldU2DD129FBC67222EA7D73E90E51E4DCFCA8C7497D67_84; }
	inline void set_U24fieldU2DD129FBC67222EA7D73E90E51E4DCFCA8C7497D67_84(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DD129FBC67222EA7D73E90E51E4DCFCA8C7497D67_84 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB3A60EC240A886DA5AFD600CC73AE12514A881E8_85() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DB3A60EC240A886DA5AFD600CC73AE12514A881E8_85)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DB3A60EC240A886DA5AFD600CC73AE12514A881E8_85() const { return ___U24fieldU2DB3A60EC240A886DA5AFD600CC73AE12514A881E8_85; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DB3A60EC240A886DA5AFD600CC73AE12514A881E8_85() { return &___U24fieldU2DB3A60EC240A886DA5AFD600CC73AE12514A881E8_85; }
	inline void set_U24fieldU2DB3A60EC240A886DA5AFD600CC73AE12514A881E8_85(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DB3A60EC240A886DA5AFD600CC73AE12514A881E8_85 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D512F92F4041B190727A330E2A6CC39E5D9EA06E6_86() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D512F92F4041B190727A330E2A6CC39E5D9EA06E6_86)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D512F92F4041B190727A330E2A6CC39E5D9EA06E6_86() const { return ___U24fieldU2D512F92F4041B190727A330E2A6CC39E5D9EA06E6_86; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D512F92F4041B190727A330E2A6CC39E5D9EA06E6_86() { return &___U24fieldU2D512F92F4041B190727A330E2A6CC39E5D9EA06E6_86; }
	inline void set_U24fieldU2D512F92F4041B190727A330E2A6CC39E5D9EA06E6_86(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D512F92F4041B190727A330E2A6CC39E5D9EA06E6_86 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_87() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DAD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_87)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DAD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_87() const { return ___U24fieldU2DAD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_87; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DAD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_87() { return &___U24fieldU2DAD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_87; }
	inline void set_U24fieldU2DAD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_87(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DAD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_87 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D03D9F1A66AED1E059B1609A09E435B708A88C8B8_88() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D03D9F1A66AED1E059B1609A09E435B708A88C8B8_88)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D03D9F1A66AED1E059B1609A09E435B708A88C8B8_88() const { return ___U24fieldU2D03D9F1A66AED1E059B1609A09E435B708A88C8B8_88; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D03D9F1A66AED1E059B1609A09E435B708A88C8B8_88() { return &___U24fieldU2D03D9F1A66AED1E059B1609A09E435B708A88C8B8_88; }
	inline void set_U24fieldU2D03D9F1A66AED1E059B1609A09E435B708A88C8B8_88(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D03D9F1A66AED1E059B1609A09E435B708A88C8B8_88 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D26853A2C322BBAD5BBD886C60A32BBBCFE847F00_89() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D26853A2C322BBAD5BBD886C60A32BBBCFE847F00_89)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D26853A2C322BBAD5BBD886C60A32BBBCFE847F00_89() const { return ___U24fieldU2D26853A2C322BBAD5BBD886C60A32BBBCFE847F00_89; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D26853A2C322BBAD5BBD886C60A32BBBCFE847F00_89() { return &___U24fieldU2D26853A2C322BBAD5BBD886C60A32BBBCFE847F00_89; }
	inline void set_U24fieldU2D26853A2C322BBAD5BBD886C60A32BBBCFE847F00_89(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D26853A2C322BBAD5BBD886C60A32BBBCFE847F00_89 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD31171F7904EB3247DD4834E43B47B1E2DCB97AC_90() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DD31171F7904EB3247DD4834E43B47B1E2DCB97AC_90)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DD31171F7904EB3247DD4834E43B47B1E2DCB97AC_90() const { return ___U24fieldU2DD31171F7904EB3247DD4834E43B47B1E2DCB97AC_90; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DD31171F7904EB3247DD4834E43B47B1E2DCB97AC_90() { return &___U24fieldU2DD31171F7904EB3247DD4834E43B47B1E2DCB97AC_90; }
	inline void set_U24fieldU2DD31171F7904EB3247DD4834E43B47B1E2DCB97AC_90(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DD31171F7904EB3247DD4834E43B47B1E2DCB97AC_90 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5EF7F909EFC731E811E21521A43A80FB5AC0B229_91() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D5EF7F909EFC731E811E21521A43A80FB5AC0B229_91)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D5EF7F909EFC731E811E21521A43A80FB5AC0B229_91() const { return ___U24fieldU2D5EF7F909EFC731E811E21521A43A80FB5AC0B229_91; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D5EF7F909EFC731E811E21521A43A80FB5AC0B229_91() { return &___U24fieldU2D5EF7F909EFC731E811E21521A43A80FB5AC0B229_91; }
	inline void set_U24fieldU2D5EF7F909EFC731E811E21521A43A80FB5AC0B229_91(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D5EF7F909EFC731E811E21521A43A80FB5AC0B229_91 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D372040F482ABADADF58EF0C31A6A8BE386AF8A50_92() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D372040F482ABADADF58EF0C31A6A8BE386AF8A50_92)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D372040F482ABADADF58EF0C31A6A8BE386AF8A50_92() const { return ___U24fieldU2D372040F482ABADADF58EF0C31A6A8BE386AF8A50_92; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D372040F482ABADADF58EF0C31A6A8BE386AF8A50_92() { return &___U24fieldU2D372040F482ABADADF58EF0C31A6A8BE386AF8A50_92; }
	inline void set_U24fieldU2D372040F482ABADADF58EF0C31A6A8BE386AF8A50_92(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D372040F482ABADADF58EF0C31A6A8BE386AF8A50_92 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D7878E9E7126B2BDF365429C31842AE1903CD2DFF_93() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D7878E9E7126B2BDF365429C31842AE1903CD2DFF_93)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D7878E9E7126B2BDF365429C31842AE1903CD2DFF_93() const { return ___U24fieldU2D7878E9E7126B2BDF365429C31842AE1903CD2DFF_93; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D7878E9E7126B2BDF365429C31842AE1903CD2DFF_93() { return &___U24fieldU2D7878E9E7126B2BDF365429C31842AE1903CD2DFF_93; }
	inline void set_U24fieldU2D7878E9E7126B2BDF365429C31842AE1903CD2DFF_93(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D7878E9E7126B2BDF365429C31842AE1903CD2DFF_93 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D882888781BC0DC17021FB4F11BA783038C83B313_94() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D882888781BC0DC17021FB4F11BA783038C83B313_94)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D882888781BC0DC17021FB4F11BA783038C83B313_94() const { return ___U24fieldU2D882888781BC0DC17021FB4F11BA783038C83B313_94; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D882888781BC0DC17021FB4F11BA783038C83B313_94() { return &___U24fieldU2D882888781BC0DC17021FB4F11BA783038C83B313_94; }
	inline void set_U24fieldU2D882888781BC0DC17021FB4F11BA783038C83B313_94(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D882888781BC0DC17021FB4F11BA783038C83B313_94 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_95() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DF3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_95)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DF3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_95() const { return ___U24fieldU2DF3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_95; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DF3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_95() { return &___U24fieldU2DF3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_95; }
	inline void set_U24fieldU2DF3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_95(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DF3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_95 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC2FEEB3C521ADDD49A534A0876BA97FF5894476E_96() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DC2FEEB3C521ADDD49A534A0876BA97FF5894476E_96)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DC2FEEB3C521ADDD49A534A0876BA97FF5894476E_96() const { return ___U24fieldU2DC2FEEB3C521ADDD49A534A0876BA97FF5894476E_96; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DC2FEEB3C521ADDD49A534A0876BA97FF5894476E_96() { return &___U24fieldU2DC2FEEB3C521ADDD49A534A0876BA97FF5894476E_96; }
	inline void set_U24fieldU2DC2FEEB3C521ADDD49A534A0876BA97FF5894476E_96(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DC2FEEB3C521ADDD49A534A0876BA97FF5894476E_96 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC991C784E7697AD0F91A159F03727BF4621A5AB8_97() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DC991C784E7697AD0F91A159F03727BF4621A5AB8_97)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DC991C784E7697AD0F91A159F03727BF4621A5AB8_97() const { return ___U24fieldU2DC991C784E7697AD0F91A159F03727BF4621A5AB8_97; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DC991C784E7697AD0F91A159F03727BF4621A5AB8_97() { return &___U24fieldU2DC991C784E7697AD0F91A159F03727BF4621A5AB8_97; }
	inline void set_U24fieldU2DC991C784E7697AD0F91A159F03727BF4621A5AB8_97(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DC991C784E7697AD0F91A159F03727BF4621A5AB8_97 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD860D5BD327368D1D4174620FE2E4A91FE9AADEC_98() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DD860D5BD327368D1D4174620FE2E4A91FE9AADEC_98)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DD860D5BD327368D1D4174620FE2E4A91FE9AADEC_98() const { return ___U24fieldU2DD860D5BD327368D1D4174620FE2E4A91FE9AADEC_98; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DD860D5BD327368D1D4174620FE2E4A91FE9AADEC_98() { return &___U24fieldU2DD860D5BD327368D1D4174620FE2E4A91FE9AADEC_98; }
	inline void set_U24fieldU2DD860D5BD327368D1D4174620FE2E4A91FE9AADEC_98(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DD860D5BD327368D1D4174620FE2E4A91FE9AADEC_98 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA696E1EE8632C559732B81052E4D2993B8783877_99() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DA696E1EE8632C559732B81052E4D2993B8783877_99)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DA696E1EE8632C559732B81052E4D2993B8783877_99() const { return ___U24fieldU2DA696E1EE8632C559732B81052E4D2993B8783877_99; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DA696E1EE8632C559732B81052E4D2993B8783877_99() { return &___U24fieldU2DA696E1EE8632C559732B81052E4D2993B8783877_99; }
	inline void set_U24fieldU2DA696E1EE8632C559732B81052E4D2993B8783877_99(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DA696E1EE8632C559732B81052E4D2993B8783877_99 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_100() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_100)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_100() const { return ___U24fieldU2D672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_100; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_100() { return &___U24fieldU2D672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_100; }
	inline void set_U24fieldU2D672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_100(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_100 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_101() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_101)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_101() const { return ___U24fieldU2D71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_101; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_101() { return &___U24fieldU2D71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_101; }
	inline void set_U24fieldU2D71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_101(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_101 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_102() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_102)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_102() const { return ___U24fieldU2D42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_102; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_102() { return &___U24fieldU2D42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_102; }
	inline void set_U24fieldU2D42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_102(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_102 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC2D514B39C8DFA25365195A0759A5AE28D9F2A87_103() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DC2D514B39C8DFA25365195A0759A5AE28D9F2A87_103)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DC2D514B39C8DFA25365195A0759A5AE28D9F2A87_103() const { return ___U24fieldU2DC2D514B39C8DFA25365195A0759A5AE28D9F2A87_103; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DC2D514B39C8DFA25365195A0759A5AE28D9F2A87_103() { return &___U24fieldU2DC2D514B39C8DFA25365195A0759A5AE28D9F2A87_103; }
	inline void set_U24fieldU2DC2D514B39C8DFA25365195A0759A5AE28D9F2A87_103(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DC2D514B39C8DFA25365195A0759A5AE28D9F2A87_103 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_104() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_104)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_104() const { return ___U24fieldU2D613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_104; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_104() { return &___U24fieldU2D613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_104; }
	inline void set_U24fieldU2D613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_104(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_104 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DB5E8BA68953A5283AD953094F0F391FA4502A3FA_105() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DB5E8BA68953A5283AD953094F0F391FA4502A3FA_105)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DB5E8BA68953A5283AD953094F0F391FA4502A3FA_105() const { return ___U24fieldU2DB5E8BA68953A5283AD953094F0F391FA4502A3FA_105; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DB5E8BA68953A5283AD953094F0F391FA4502A3FA_105() { return &___U24fieldU2DB5E8BA68953A5283AD953094F0F391FA4502A3FA_105; }
	inline void set_U24fieldU2DB5E8BA68953A5283AD953094F0F391FA4502A3FA_105(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DB5E8BA68953A5283AD953094F0F391FA4502A3FA_105 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D35E6464339FFAE0D3777B12A371F82D2D1F668CA_106() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D35E6464339FFAE0D3777B12A371F82D2D1F668CA_106)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D35E6464339FFAE0D3777B12A371F82D2D1F668CA_106() const { return ___U24fieldU2D35E6464339FFAE0D3777B12A371F82D2D1F668CA_106; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D35E6464339FFAE0D3777B12A371F82D2D1F668CA_106() { return &___U24fieldU2D35E6464339FFAE0D3777B12A371F82D2D1F668CA_106; }
	inline void set_U24fieldU2D35E6464339FFAE0D3777B12A371F82D2D1F668CA_106(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D35E6464339FFAE0D3777B12A371F82D2D1F668CA_106 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6DEB7F74818574642B0B824B9C08B366C962A360_107() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D6DEB7F74818574642B0B824B9C08B366C962A360_107)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D6DEB7F74818574642B0B824B9C08B366C962A360_107() const { return ___U24fieldU2D6DEB7F74818574642B0B824B9C08B366C962A360_107; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D6DEB7F74818574642B0B824B9C08B366C962A360_107() { return &___U24fieldU2D6DEB7F74818574642B0B824B9C08B366C962A360_107; }
	inline void set_U24fieldU2D6DEB7F74818574642B0B824B9C08B366C962A360_107(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D6DEB7F74818574642B0B824B9C08B366C962A360_107 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DBEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_108() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DBEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_108)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DBEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_108() const { return ___U24fieldU2DBEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_108; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DBEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_108() { return &___U24fieldU2DBEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_108; }
	inline void set_U24fieldU2DBEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_108(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DBEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_108 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5D41C56232C500092E99AC044D3C5C442B1C834F_109() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D5D41C56232C500092E99AC044D3C5C442B1C834F_109)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D5D41C56232C500092E99AC044D3C5C442B1C834F_109() const { return ___U24fieldU2D5D41C56232C500092E99AC044D3C5C442B1C834F_109; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D5D41C56232C500092E99AC044D3C5C442B1C834F_109() { return &___U24fieldU2D5D41C56232C500092E99AC044D3C5C442B1C834F_109; }
	inline void set_U24fieldU2D5D41C56232C500092E99AC044D3C5C442B1C834F_109(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D5D41C56232C500092E99AC044D3C5C442B1C834F_109 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF63639527E877A2CBCB26FFD41D4A59470BFF8C8_110() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DF63639527E877A2CBCB26FFD41D4A59470BFF8C8_110)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DF63639527E877A2CBCB26FFD41D4A59470BFF8C8_110() const { return ___U24fieldU2DF63639527E877A2CBCB26FFD41D4A59470BFF8C8_110; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DF63639527E877A2CBCB26FFD41D4A59470BFF8C8_110() { return &___U24fieldU2DF63639527E877A2CBCB26FFD41D4A59470BFF8C8_110; }
	inline void set_U24fieldU2DF63639527E877A2CBCB26FFD41D4A59470BFF8C8_110(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DF63639527E877A2CBCB26FFD41D4A59470BFF8C8_110 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_111() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DA72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_111)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DA72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_111() const { return ___U24fieldU2DA72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_111; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DA72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_111() { return &___U24fieldU2DA72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_111; }
	inline void set_U24fieldU2DA72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_111(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DA72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_111 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC4B266E68FA20D0D222D86ADAD31EBB55118CD21_112() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DC4B266E68FA20D0D222D86ADAD31EBB55118CD21_112)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DC4B266E68FA20D0D222D86ADAD31EBB55118CD21_112() const { return ___U24fieldU2DC4B266E68FA20D0D222D86ADAD31EBB55118CD21_112; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DC4B266E68FA20D0D222D86ADAD31EBB55118CD21_112() { return &___U24fieldU2DC4B266E68FA20D0D222D86ADAD31EBB55118CD21_112; }
	inline void set_U24fieldU2DC4B266E68FA20D0D222D86ADAD31EBB55118CD21_112(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DC4B266E68FA20D0D222D86ADAD31EBB55118CD21_112 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_113() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_113)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2D6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_113() const { return ___U24fieldU2D6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_113; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2D6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_113() { return &___U24fieldU2D6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_113; }
	inline void set_U24fieldU2D6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_113(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2D6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_113 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC95356610D5583976B69017BED7048EB50121B90_114() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DC95356610D5583976B69017BED7048EB50121B90_114)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2DC95356610D5583976B69017BED7048EB50121B90_114() const { return ___U24fieldU2DC95356610D5583976B69017BED7048EB50121B90_114; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2DC95356610D5583976B69017BED7048EB50121B90_114() { return &___U24fieldU2DC95356610D5583976B69017BED7048EB50121B90_114; }
	inline void set_U24fieldU2DC95356610D5583976B69017BED7048EB50121B90_114(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2DC95356610D5583976B69017BED7048EB50121B90_114 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA4313AAA146ACFCA88681B7BFC3D644005F3792B_115() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DA4313AAA146ACFCA88681B7BFC3D644005F3792B_115)); }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  get_U24fieldU2DA4313AAA146ACFCA88681B7BFC3D644005F3792B_115() const { return ___U24fieldU2DA4313AAA146ACFCA88681B7BFC3D644005F3792B_115; }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 * get_address_of_U24fieldU2DA4313AAA146ACFCA88681B7BFC3D644005F3792B_115() { return &___U24fieldU2DA4313AAA146ACFCA88681B7BFC3D644005F3792B_115; }
	inline void set_U24fieldU2DA4313AAA146ACFCA88681B7BFC3D644005F3792B_115(U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  value)
	{
		___U24fieldU2DA4313AAA146ACFCA88681B7BFC3D644005F3792B_115 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_116() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DCA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_116)); }
	inline U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6  get_U24fieldU2DCA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_116() const { return ___U24fieldU2DCA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_116; }
	inline U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6 * get_address_of_U24fieldU2DCA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_116() { return &___U24fieldU2DCA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_116; }
	inline void set_U24fieldU2DCA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_116(U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6  value)
	{
		___U24fieldU2DCA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_116 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D239B59488F1CE7EBE225785FDC22A8E3102A2E82_117() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D239B59488F1CE7EBE225785FDC22A8E3102A2E82_117)); }
	inline U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6  get_U24fieldU2D239B59488F1CE7EBE225785FDC22A8E3102A2E82_117() const { return ___U24fieldU2D239B59488F1CE7EBE225785FDC22A8E3102A2E82_117; }
	inline U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6 * get_address_of_U24fieldU2D239B59488F1CE7EBE225785FDC22A8E3102A2E82_117() { return &___U24fieldU2D239B59488F1CE7EBE225785FDC22A8E3102A2E82_117; }
	inline void set_U24fieldU2D239B59488F1CE7EBE225785FDC22A8E3102A2E82_117(U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6  value)
	{
		___U24fieldU2D239B59488F1CE7EBE225785FDC22A8E3102A2E82_117 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D310EFB639F3C7677A2A82B54EEED1124ED69E9A3_118() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D310EFB639F3C7677A2A82B54EEED1124ED69E9A3_118)); }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  get_U24fieldU2D310EFB639F3C7677A2A82B54EEED1124ED69E9A3_118() const { return ___U24fieldU2D310EFB639F3C7677A2A82B54EEED1124ED69E9A3_118; }
	inline U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 * get_address_of_U24fieldU2D310EFB639F3C7677A2A82B54EEED1124ED69E9A3_118() { return &___U24fieldU2D310EFB639F3C7677A2A82B54EEED1124ED69E9A3_118; }
	inline void set_U24fieldU2D310EFB639F3C7677A2A82B54EEED1124ED69E9A3_118(U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214  value)
	{
		___U24fieldU2D310EFB639F3C7677A2A82B54EEED1124ED69E9A3_118 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D428007959831954B0C2DCFAF9DD641D629B00DBF_119() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D428007959831954B0C2DCFAF9DD641D629B00DBF_119)); }
	inline U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6  get_U24fieldU2D428007959831954B0C2DCFAF9DD641D629B00DBF_119() const { return ___U24fieldU2D428007959831954B0C2DCFAF9DD641D629B00DBF_119; }
	inline U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6 * get_address_of_U24fieldU2D428007959831954B0C2DCFAF9DD641D629B00DBF_119() { return &___U24fieldU2D428007959831954B0C2DCFAF9DD641D629B00DBF_119; }
	inline void set_U24fieldU2D428007959831954B0C2DCFAF9DD641D629B00DBF_119(U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6  value)
	{
		___U24fieldU2D428007959831954B0C2DCFAF9DD641D629B00DBF_119 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DFA5B1C8B2F287078ED719C15595DB729BDB85911_120() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DFA5B1C8B2F287078ED719C15595DB729BDB85911_120)); }
	inline U24ArrayTypeU3D4096_tE76E581E9D7FCAFF3FDB2A1A2F45C247994B04A5  get_U24fieldU2DFA5B1C8B2F287078ED719C15595DB729BDB85911_120() const { return ___U24fieldU2DFA5B1C8B2F287078ED719C15595DB729BDB85911_120; }
	inline U24ArrayTypeU3D4096_tE76E581E9D7FCAFF3FDB2A1A2F45C247994B04A5 * get_address_of_U24fieldU2DFA5B1C8B2F287078ED719C15595DB729BDB85911_120() { return &___U24fieldU2DFA5B1C8B2F287078ED719C15595DB729BDB85911_120; }
	inline void set_U24fieldU2DFA5B1C8B2F287078ED719C15595DB729BDB85911_120(U24ArrayTypeU3D4096_tE76E581E9D7FCAFF3FDB2A1A2F45C247994B04A5  value)
	{
		___U24fieldU2DFA5B1C8B2F287078ED719C15595DB729BDB85911_120 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_121() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_121)); }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  get_U24fieldU2D86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_121() const { return ___U24fieldU2D86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_121; }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 * get_address_of_U24fieldU2D86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_121() { return &___U24fieldU2D86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_121; }
	inline void set_U24fieldU2D86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_121(U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  value)
	{
		___U24fieldU2D86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_121 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DEAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_122() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DEAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_122)); }
	inline U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05  get_U24fieldU2DEAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_122() const { return ___U24fieldU2DEAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_122; }
	inline U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05 * get_address_of_U24fieldU2DEAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_122() { return &___U24fieldU2DEAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_122; }
	inline void set_U24fieldU2DEAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_122(U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05  value)
	{
		___U24fieldU2DEAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_122 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6AAC0DB543C50F09E879F5B9F757319773564CE1_123() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D6AAC0DB543C50F09E879F5B9F757319773564CE1_123)); }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  get_U24fieldU2D6AAC0DB543C50F09E879F5B9F757319773564CE1_123() const { return ___U24fieldU2D6AAC0DB543C50F09E879F5B9F757319773564CE1_123; }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 * get_address_of_U24fieldU2D6AAC0DB543C50F09E879F5B9F757319773564CE1_123() { return &___U24fieldU2D6AAC0DB543C50F09E879F5B9F757319773564CE1_123; }
	inline void set_U24fieldU2D6AAC0DB543C50F09E879F5B9F757319773564CE1_123(U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  value)
	{
		___U24fieldU2D6AAC0DB543C50F09E879F5B9F757319773564CE1_123 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DFAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_124() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DFAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_124)); }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  get_U24fieldU2DFAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_124() const { return ___U24fieldU2DFAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_124; }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326 * get_address_of_U24fieldU2DFAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_124() { return &___U24fieldU2DFAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_124; }
	inline void set_U24fieldU2DFAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_124(U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  value)
	{
		___U24fieldU2DFAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_124 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D068B2E17352B5B9FF693CAE83421B679E0342A5C_125() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D068B2E17352B5B9FF693CAE83421B679E0342A5C_125)); }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  get_U24fieldU2D068B2E17352B5B9FF693CAE83421B679E0342A5C_125() const { return ___U24fieldU2D068B2E17352B5B9FF693CAE83421B679E0342A5C_125; }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 * get_address_of_U24fieldU2D068B2E17352B5B9FF693CAE83421B679E0342A5C_125() { return &___U24fieldU2D068B2E17352B5B9FF693CAE83421B679E0342A5C_125; }
	inline void set_U24fieldU2D068B2E17352B5B9FF693CAE83421B679E0342A5C_125(U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  value)
	{
		___U24fieldU2D068B2E17352B5B9FF693CAE83421B679E0342A5C_125 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_126() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DF2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_126)); }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  get_U24fieldU2DF2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_126() const { return ___U24fieldU2DF2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_126; }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 * get_address_of_U24fieldU2DF2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_126() { return &___U24fieldU2DF2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_126; }
	inline void set_U24fieldU2DF2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_126(U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  value)
	{
		___U24fieldU2DF2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_126 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_127() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_127)); }
	inline U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  get_U24fieldU2D35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_127() const { return ___U24fieldU2D35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_127; }
	inline U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3 * get_address_of_U24fieldU2D35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_127() { return &___U24fieldU2D35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_127; }
	inline void set_U24fieldU2D35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_127(U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  value)
	{
		___U24fieldU2D35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_127 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D310FB325A3EA3EA527B55C2F08544D1CB92C19F4_128() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D310FB325A3EA3EA527B55C2F08544D1CB92C19F4_128)); }
	inline U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8  get_U24fieldU2D310FB325A3EA3EA527B55C2F08544D1CB92C19F4_128() const { return ___U24fieldU2D310FB325A3EA3EA527B55C2F08544D1CB92C19F4_128; }
	inline U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8 * get_address_of_U24fieldU2D310FB325A3EA3EA527B55C2F08544D1CB92C19F4_128() { return &___U24fieldU2D310FB325A3EA3EA527B55C2F08544D1CB92C19F4_128; }
	inline void set_U24fieldU2D310FB325A3EA3EA527B55C2F08544D1CB92C19F4_128(U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8  value)
	{
		___U24fieldU2D310FB325A3EA3EA527B55C2F08544D1CB92C19F4_128 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC132685022CE310ACFD3A883E0A57033A482A959_129() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DC132685022CE310ACFD3A883E0A57033A482A959_129)); }
	inline U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  get_U24fieldU2DC132685022CE310ACFD3A883E0A57033A482A959_129() const { return ___U24fieldU2DC132685022CE310ACFD3A883E0A57033A482A959_129; }
	inline U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F * get_address_of_U24fieldU2DC132685022CE310ACFD3A883E0A57033A482A959_129() { return &___U24fieldU2DC132685022CE310ACFD3A883E0A57033A482A959_129; }
	inline void set_U24fieldU2DC132685022CE310ACFD3A883E0A57033A482A959_129(U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  value)
	{
		___U24fieldU2DC132685022CE310ACFD3A883E0A57033A482A959_129 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D27FED0F92A97C41B08D3115553BBDC064F417B6E_130() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D27FED0F92A97C41B08D3115553BBDC064F417B6E_130)); }
	inline U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  get_U24fieldU2D27FED0F92A97C41B08D3115553BBDC064F417B6E_130() const { return ___U24fieldU2D27FED0F92A97C41B08D3115553BBDC064F417B6E_130; }
	inline U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3 * get_address_of_U24fieldU2D27FED0F92A97C41B08D3115553BBDC064F417B6E_130() { return &___U24fieldU2D27FED0F92A97C41B08D3115553BBDC064F417B6E_130; }
	inline void set_U24fieldU2D27FED0F92A97C41B08D3115553BBDC064F417B6E_130(U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  value)
	{
		___U24fieldU2D27FED0F92A97C41B08D3115553BBDC064F417B6E_130 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D60A08108A32C9D3F263B2F42095A2694B7C1C1EF_131() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D60A08108A32C9D3F263B2F42095A2694B7C1C1EF_131)); }
	inline U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8  get_U24fieldU2D60A08108A32C9D3F263B2F42095A2694B7C1C1EF_131() const { return ___U24fieldU2D60A08108A32C9D3F263B2F42095A2694B7C1C1EF_131; }
	inline U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8 * get_address_of_U24fieldU2D60A08108A32C9D3F263B2F42095A2694B7C1C1EF_131() { return &___U24fieldU2D60A08108A32C9D3F263B2F42095A2694B7C1C1EF_131; }
	inline void set_U24fieldU2D60A08108A32C9D3F263B2F42095A2694B7C1C1EF_131(U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8  value)
	{
		___U24fieldU2D60A08108A32C9D3F263B2F42095A2694B7C1C1EF_131 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D32ECB35FF8400B4E56FF5E09588FB20DD60350E7_132() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D32ECB35FF8400B4E56FF5E09588FB20DD60350E7_132)); }
	inline U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  get_U24fieldU2D32ECB35FF8400B4E56FF5E09588FB20DD60350E7_132() const { return ___U24fieldU2D32ECB35FF8400B4E56FF5E09588FB20DD60350E7_132; }
	inline U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F * get_address_of_U24fieldU2D32ECB35FF8400B4E56FF5E09588FB20DD60350E7_132() { return &___U24fieldU2D32ECB35FF8400B4E56FF5E09588FB20DD60350E7_132; }
	inline void set_U24fieldU2D32ECB35FF8400B4E56FF5E09588FB20DD60350E7_132(U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  value)
	{
		___U24fieldU2D32ECB35FF8400B4E56FF5E09588FB20DD60350E7_132 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DFB3C663794DD23F500825FF78450D198FE338938_133() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DFB3C663794DD23F500825FF78450D198FE338938_133)); }
	inline U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  get_U24fieldU2DFB3C663794DD23F500825FF78450D198FE338938_133() const { return ___U24fieldU2DFB3C663794DD23F500825FF78450D198FE338938_133; }
	inline U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723 * get_address_of_U24fieldU2DFB3C663794DD23F500825FF78450D198FE338938_133() { return &___U24fieldU2DFB3C663794DD23F500825FF78450D198FE338938_133; }
	inline void set_U24fieldU2DFB3C663794DD23F500825FF78450D198FE338938_133(U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  value)
	{
		___U24fieldU2DFB3C663794DD23F500825FF78450D198FE338938_133 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC5515C87D04DC0D00C7984096F5E35B4944C1CB6_134() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DC5515C87D04DC0D00C7984096F5E35B4944C1CB6_134)); }
	inline U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  get_U24fieldU2DC5515C87D04DC0D00C7984096F5E35B4944C1CB6_134() const { return ___U24fieldU2DC5515C87D04DC0D00C7984096F5E35B4944C1CB6_134; }
	inline U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3 * get_address_of_U24fieldU2DC5515C87D04DC0D00C7984096F5E35B4944C1CB6_134() { return &___U24fieldU2DC5515C87D04DC0D00C7984096F5E35B4944C1CB6_134; }
	inline void set_U24fieldU2DC5515C87D04DC0D00C7984096F5E35B4944C1CB6_134(U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  value)
	{
		___U24fieldU2DC5515C87D04DC0D00C7984096F5E35B4944C1CB6_134 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8330271815E046D369E0B1F7673D308739FDCC07_135() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D8330271815E046D369E0B1F7673D308739FDCC07_135)); }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  get_U24fieldU2D8330271815E046D369E0B1F7673D308739FDCC07_135() const { return ___U24fieldU2D8330271815E046D369E0B1F7673D308739FDCC07_135; }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 * get_address_of_U24fieldU2D8330271815E046D369E0B1F7673D308739FDCC07_135() { return &___U24fieldU2D8330271815E046D369E0B1F7673D308739FDCC07_135; }
	inline void set_U24fieldU2D8330271815E046D369E0B1F7673D308739FDCC07_135(U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  value)
	{
		___U24fieldU2D8330271815E046D369E0B1F7673D308739FDCC07_135 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD6898715AE96BC2F82A7BBA76E2BFC7100E282C3_136() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DD6898715AE96BC2F82A7BBA76E2BFC7100E282C3_136)); }
	inline U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  get_U24fieldU2DD6898715AE96BC2F82A7BBA76E2BFC7100E282C3_136() const { return ___U24fieldU2DD6898715AE96BC2F82A7BBA76E2BFC7100E282C3_136; }
	inline U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723 * get_address_of_U24fieldU2DD6898715AE96BC2F82A7BBA76E2BFC7100E282C3_136() { return &___U24fieldU2DD6898715AE96BC2F82A7BBA76E2BFC7100E282C3_136; }
	inline void set_U24fieldU2DD6898715AE96BC2F82A7BBA76E2BFC7100E282C3_136(U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  value)
	{
		___U24fieldU2DD6898715AE96BC2F82A7BBA76E2BFC7100E282C3_136 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA53306F44DF494A019EA1487807B59CA336BF024_137() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DA53306F44DF494A019EA1487807B59CA336BF024_137)); }
	inline U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  get_U24fieldU2DA53306F44DF494A019EA1487807B59CA336BF024_137() const { return ___U24fieldU2DA53306F44DF494A019EA1487807B59CA336BF024_137; }
	inline U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3 * get_address_of_U24fieldU2DA53306F44DF494A019EA1487807B59CA336BF024_137() { return &___U24fieldU2DA53306F44DF494A019EA1487807B59CA336BF024_137; }
	inline void set_U24fieldU2DA53306F44DF494A019EA1487807B59CA336BF024_137(U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  value)
	{
		___U24fieldU2DA53306F44DF494A019EA1487807B59CA336BF024_137 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D0982B1B45B764F2694ABC3DE57204AC898651429_138() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D0982B1B45B764F2694ABC3DE57204AC898651429_138)); }
	inline U24ArrayTypeU3D36_t9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B  get_U24fieldU2D0982B1B45B764F2694ABC3DE57204AC898651429_138() const { return ___U24fieldU2D0982B1B45B764F2694ABC3DE57204AC898651429_138; }
	inline U24ArrayTypeU3D36_t9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B * get_address_of_U24fieldU2D0982B1B45B764F2694ABC3DE57204AC898651429_138() { return &___U24fieldU2D0982B1B45B764F2694ABC3DE57204AC898651429_138; }
	inline void set_U24fieldU2D0982B1B45B764F2694ABC3DE57204AC898651429_138(U24ArrayTypeU3D36_t9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B  value)
	{
		___U24fieldU2D0982B1B45B764F2694ABC3DE57204AC898651429_138 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D126589410FF9CA1510B9950BF0E79E5BFD60000B_139() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D126589410FF9CA1510B9950BF0E79E5BFD60000B_139)); }
	inline U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  get_U24fieldU2D126589410FF9CA1510B9950BF0E79E5BFD60000B_139() const { return ___U24fieldU2D126589410FF9CA1510B9950BF0E79E5BFD60000B_139; }
	inline U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F * get_address_of_U24fieldU2D126589410FF9CA1510B9950BF0E79E5BFD60000B_139() { return &___U24fieldU2D126589410FF9CA1510B9950BF0E79E5BFD60000B_139; }
	inline void set_U24fieldU2D126589410FF9CA1510B9950BF0E79E5BFD60000B_139(U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  value)
	{
		___U24fieldU2D126589410FF9CA1510B9950BF0E79E5BFD60000B_139 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE647D32D165F3510693DF9787DC98E0A0B63C5C2_140() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DE647D32D165F3510693DF9787DC98E0A0B63C5C2_140)); }
	inline U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803  get_U24fieldU2DE647D32D165F3510693DF9787DC98E0A0B63C5C2_140() const { return ___U24fieldU2DE647D32D165F3510693DF9787DC98E0A0B63C5C2_140; }
	inline U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803 * get_address_of_U24fieldU2DE647D32D165F3510693DF9787DC98E0A0B63C5C2_140() { return &___U24fieldU2DE647D32D165F3510693DF9787DC98E0A0B63C5C2_140; }
	inline void set_U24fieldU2DE647D32D165F3510693DF9787DC98E0A0B63C5C2_140(U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803  value)
	{
		___U24fieldU2DE647D32D165F3510693DF9787DC98E0A0B63C5C2_140 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_141() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_141)); }
	inline U24ArrayTypeU3D36_t9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B  get_U24fieldU2D66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_141() const { return ___U24fieldU2D66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_141; }
	inline U24ArrayTypeU3D36_t9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B * get_address_of_U24fieldU2D66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_141() { return &___U24fieldU2D66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_141; }
	inline void set_U24fieldU2D66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_141(U24ArrayTypeU3D36_t9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B  value)
	{
		___U24fieldU2D66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_141 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_142() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_142)); }
	inline U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  get_U24fieldU2D059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_142() const { return ___U24fieldU2D059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_142; }
	inline U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F * get_address_of_U24fieldU2D059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_142() { return &___U24fieldU2D059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_142; }
	inline void set_U24fieldU2D059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_142(U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  value)
	{
		___U24fieldU2D059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_142 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1E3842329C5294DBE1DF588A77C68B35C6AF83BF_143() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D1E3842329C5294DBE1DF588A77C68B35C6AF83BF_143)); }
	inline U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  get_U24fieldU2D1E3842329C5294DBE1DF588A77C68B35C6AF83BF_143() const { return ___U24fieldU2D1E3842329C5294DBE1DF588A77C68B35C6AF83BF_143; }
	inline U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F * get_address_of_U24fieldU2D1E3842329C5294DBE1DF588A77C68B35C6AF83BF_143() { return &___U24fieldU2D1E3842329C5294DBE1DF588A77C68B35C6AF83BF_143; }
	inline void set_U24fieldU2D1E3842329C5294DBE1DF588A77C68B35C6AF83BF_143(U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F  value)
	{
		___U24fieldU2D1E3842329C5294DBE1DF588A77C68B35C6AF83BF_143 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_144() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_144)); }
	inline U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803  get_U24fieldU2D8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_144() const { return ___U24fieldU2D8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_144; }
	inline U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803 * get_address_of_U24fieldU2D8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_144() { return &___U24fieldU2D8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_144; }
	inline void set_U24fieldU2D8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_144(U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803  value)
	{
		___U24fieldU2D8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_144 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DEBE167F7962841FA83451C9C1663416D69AA5294_145() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DEBE167F7962841FA83451C9C1663416D69AA5294_145)); }
	inline U24ArrayTypeU3D44_t156A45D15629E5CD523F90BAE6DB67B5ACBE097F  get_U24fieldU2DEBE167F7962841FA83451C9C1663416D69AA5294_145() const { return ___U24fieldU2DEBE167F7962841FA83451C9C1663416D69AA5294_145; }
	inline U24ArrayTypeU3D44_t156A45D15629E5CD523F90BAE6DB67B5ACBE097F * get_address_of_U24fieldU2DEBE167F7962841FA83451C9C1663416D69AA5294_145() { return &___U24fieldU2DEBE167F7962841FA83451C9C1663416D69AA5294_145; }
	inline void set_U24fieldU2DEBE167F7962841FA83451C9C1663416D69AA5294_145(U24ArrayTypeU3D44_t156A45D15629E5CD523F90BAE6DB67B5ACBE097F  value)
	{
		___U24fieldU2DEBE167F7962841FA83451C9C1663416D69AA5294_145 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_146() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_146)); }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  get_U24fieldU2D2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_146() const { return ___U24fieldU2D2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_146; }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 * get_address_of_U24fieldU2D2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_146() { return &___U24fieldU2D2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_146; }
	inline void set_U24fieldU2D2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_146(U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  value)
	{
		___U24fieldU2D2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_146 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D79A213B796D2AD7A89C2071B0732B78207F5CE01_147() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D79A213B796D2AD7A89C2071B0732B78207F5CE01_147)); }
	inline U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05  get_U24fieldU2D79A213B796D2AD7A89C2071B0732B78207F5CE01_147() const { return ___U24fieldU2D79A213B796D2AD7A89C2071B0732B78207F5CE01_147; }
	inline U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05 * get_address_of_U24fieldU2D79A213B796D2AD7A89C2071B0732B78207F5CE01_147() { return &___U24fieldU2D79A213B796D2AD7A89C2071B0732B78207F5CE01_147; }
	inline void set_U24fieldU2D79A213B796D2AD7A89C2071B0732B78207F5CE01_147(U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05  value)
	{
		___U24fieldU2D79A213B796D2AD7A89C2071B0732B78207F5CE01_147 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DBE8E2513259482B6F307AC07F23F5D9FB4841EAA_148() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DBE8E2513259482B6F307AC07F23F5D9FB4841EAA_148)); }
	inline U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8  get_U24fieldU2DBE8E2513259482B6F307AC07F23F5D9FB4841EAA_148() const { return ___U24fieldU2DBE8E2513259482B6F307AC07F23F5D9FB4841EAA_148; }
	inline U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8 * get_address_of_U24fieldU2DBE8E2513259482B6F307AC07F23F5D9FB4841EAA_148() { return &___U24fieldU2DBE8E2513259482B6F307AC07F23F5D9FB4841EAA_148; }
	inline void set_U24fieldU2DBE8E2513259482B6F307AC07F23F5D9FB4841EAA_148(U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8  value)
	{
		___U24fieldU2DBE8E2513259482B6F307AC07F23F5D9FB4841EAA_148 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D083DE622A9A685DC50D8D5653CB388A41343C8EC_149() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D083DE622A9A685DC50D8D5653CB388A41343C8EC_149)); }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  get_U24fieldU2D083DE622A9A685DC50D8D5653CB388A41343C8EC_149() const { return ___U24fieldU2D083DE622A9A685DC50D8D5653CB388A41343C8EC_149; }
	inline U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 * get_address_of_U24fieldU2D083DE622A9A685DC50D8D5653CB388A41343C8EC_149() { return &___U24fieldU2D083DE622A9A685DC50D8D5653CB388A41343C8EC_149; }
	inline void set_U24fieldU2D083DE622A9A685DC50D8D5653CB388A41343C8EC_149(U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99  value)
	{
		___U24fieldU2D083DE622A9A685DC50D8D5653CB388A41343C8EC_149 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D214F93D9222D60794CE1EA0A10389885C5CA9824_150() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D214F93D9222D60794CE1EA0A10389885C5CA9824_150)); }
	inline U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05  get_U24fieldU2D214F93D9222D60794CE1EA0A10389885C5CA9824_150() const { return ___U24fieldU2D214F93D9222D60794CE1EA0A10389885C5CA9824_150; }
	inline U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05 * get_address_of_U24fieldU2D214F93D9222D60794CE1EA0A10389885C5CA9824_150() { return &___U24fieldU2D214F93D9222D60794CE1EA0A10389885C5CA9824_150; }
	inline void set_U24fieldU2D214F93D9222D60794CE1EA0A10389885C5CA9824_150(U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05  value)
	{
		___U24fieldU2D214F93D9222D60794CE1EA0A10389885C5CA9824_150 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D095B351FE2104237B032546280C98C9804D331C5_151() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D095B351FE2104237B032546280C98C9804D331C5_151)); }
	inline U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  get_U24fieldU2D095B351FE2104237B032546280C98C9804D331C5_151() const { return ___U24fieldU2D095B351FE2104237B032546280C98C9804D331C5_151; }
	inline U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3 * get_address_of_U24fieldU2D095B351FE2104237B032546280C98C9804D331C5_151() { return &___U24fieldU2D095B351FE2104237B032546280C98C9804D331C5_151; }
	inline void set_U24fieldU2D095B351FE2104237B032546280C98C9804D331C5_151(U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3  value)
	{
		___U24fieldU2D095B351FE2104237B032546280C98C9804D331C5_151 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCE39574ADC95015A9B5E0475EB65EE8F32353FD4_152() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DCE39574ADC95015A9B5E0475EB65EE8F32353FD4_152)); }
	inline U24ArrayTypeU3D96_t5C15B6692DC57B36771160CFA909A1E1B6A9E4A4  get_U24fieldU2DCE39574ADC95015A9B5E0475EB65EE8F32353FD4_152() const { return ___U24fieldU2DCE39574ADC95015A9B5E0475EB65EE8F32353FD4_152; }
	inline U24ArrayTypeU3D96_t5C15B6692DC57B36771160CFA909A1E1B6A9E4A4 * get_address_of_U24fieldU2DCE39574ADC95015A9B5E0475EB65EE8F32353FD4_152() { return &___U24fieldU2DCE39574ADC95015A9B5E0475EB65EE8F32353FD4_152; }
	inline void set_U24fieldU2DCE39574ADC95015A9B5E0475EB65EE8F32353FD4_152(U24ArrayTypeU3D96_t5C15B6692DC57B36771160CFA909A1E1B6A9E4A4  value)
	{
		___U24fieldU2DCE39574ADC95015A9B5E0475EB65EE8F32353FD4_152 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DEF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_153() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DEF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_153)); }
	inline U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5  get_U24fieldU2DEF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_153() const { return ___U24fieldU2DEF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_153; }
	inline U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5 * get_address_of_U24fieldU2DEF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_153() { return &___U24fieldU2DEF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_153; }
	inline void set_U24fieldU2DEF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_153(U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5  value)
	{
		___U24fieldU2DEF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_153 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D96ADC3934F8492C827987DFEE3B4DD4EF1738E78_154() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D96ADC3934F8492C827987DFEE3B4DD4EF1738E78_154)); }
	inline U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5  get_U24fieldU2D96ADC3934F8492C827987DFEE3B4DD4EF1738E78_154() const { return ___U24fieldU2D96ADC3934F8492C827987DFEE3B4DD4EF1738E78_154; }
	inline U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5 * get_address_of_U24fieldU2D96ADC3934F8492C827987DFEE3B4DD4EF1738E78_154() { return &___U24fieldU2D96ADC3934F8492C827987DFEE3B4DD4EF1738E78_154; }
	inline void set_U24fieldU2D96ADC3934F8492C827987DFEE3B4DD4EF1738E78_154(U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5  value)
	{
		___U24fieldU2D96ADC3934F8492C827987DFEE3B4DD4EF1738E78_154 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DCBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_155() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DCBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_155)); }
	inline U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  get_U24fieldU2DCBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_155() const { return ___U24fieldU2DCBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_155; }
	inline U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723 * get_address_of_U24fieldU2DCBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_155() { return &___U24fieldU2DCBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_155; }
	inline void set_U24fieldU2DCBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_155(U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  value)
	{
		___U24fieldU2DCBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_155 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DBCE617693C33CE2C76FE00F449CA910E4C6E117E_156() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DBCE617693C33CE2C76FE00F449CA910E4C6E117E_156)); }
	inline U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  get_U24fieldU2DBCE617693C33CE2C76FE00F449CA910E4C6E117E_156() const { return ___U24fieldU2DBCE617693C33CE2C76FE00F449CA910E4C6E117E_156; }
	inline U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723 * get_address_of_U24fieldU2DBCE617693C33CE2C76FE00F449CA910E4C6E117E_156() { return &___U24fieldU2DBCE617693C33CE2C76FE00F449CA910E4C6E117E_156; }
	inline void set_U24fieldU2DBCE617693C33CE2C76FE00F449CA910E4C6E117E_156(U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  value)
	{
		___U24fieldU2DBCE617693C33CE2C76FE00F449CA910E4C6E117E_156 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DE0219F11D9EECC43022AA94967780250AC270D4B_157() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DE0219F11D9EECC43022AA94967780250AC270D4B_157)); }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  get_U24fieldU2DE0219F11D9EECC43022AA94967780250AC270D4B_157() const { return ___U24fieldU2DE0219F11D9EECC43022AA94967780250AC270D4B_157; }
	inline U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 * get_address_of_U24fieldU2DE0219F11D9EECC43022AA94967780250AC270D4B_157() { return &___U24fieldU2DE0219F11D9EECC43022AA94967780250AC270D4B_157; }
	inline void set_U24fieldU2DE0219F11D9EECC43022AA94967780250AC270D4B_157(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33  value)
	{
		___U24fieldU2DE0219F11D9EECC43022AA94967780250AC270D4B_157 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5E7F55149EC07597C76E6E3CD9F62274214061E6_158() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D5E7F55149EC07597C76E6E3CD9F62274214061E6_158)); }
	inline U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8  get_U24fieldU2D5E7F55149EC07597C76E6E3CD9F62274214061E6_158() const { return ___U24fieldU2D5E7F55149EC07597C76E6E3CD9F62274214061E6_158; }
	inline U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8 * get_address_of_U24fieldU2D5E7F55149EC07597C76E6E3CD9F62274214061E6_158() { return &___U24fieldU2D5E7F55149EC07597C76E6E3CD9F62274214061E6_158; }
	inline void set_U24fieldU2D5E7F55149EC07597C76E6E3CD9F62274214061E6_158(U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8  value)
	{
		___U24fieldU2D5E7F55149EC07597C76E6E3CD9F62274214061E6_158 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D0D0825E62E82DBEBFAD598623694129548E24C9C_159() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D0D0825E62E82DBEBFAD598623694129548E24C9C_159)); }
	inline U24ArrayTypeU3D72_t2220FD4A9A26B7F357806CAF46D79E53E4F11D64  get_U24fieldU2D0D0825E62E82DBEBFAD598623694129548E24C9C_159() const { return ___U24fieldU2D0D0825E62E82DBEBFAD598623694129548E24C9C_159; }
	inline U24ArrayTypeU3D72_t2220FD4A9A26B7F357806CAF46D79E53E4F11D64 * get_address_of_U24fieldU2D0D0825E62E82DBEBFAD598623694129548E24C9C_159() { return &___U24fieldU2D0D0825E62E82DBEBFAD598623694129548E24C9C_159; }
	inline void set_U24fieldU2D0D0825E62E82DBEBFAD598623694129548E24C9C_159(U24ArrayTypeU3D72_t2220FD4A9A26B7F357806CAF46D79E53E4F11D64  value)
	{
		___U24fieldU2D0D0825E62E82DBEBFAD598623694129548E24C9C_159 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D17E54FCA28103DF892BBB946DDEED4B061D7F5C7_160() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D17E54FCA28103DF892BBB946DDEED4B061D7F5C7_160)); }
	inline U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  get_U24fieldU2D17E54FCA28103DF892BBB946DDEED4B061D7F5C7_160() const { return ___U24fieldU2D17E54FCA28103DF892BBB946DDEED4B061D7F5C7_160; }
	inline U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723 * get_address_of_U24fieldU2D17E54FCA28103DF892BBB946DDEED4B061D7F5C7_160() { return &___U24fieldU2D17E54FCA28103DF892BBB946DDEED4B061D7F5C7_160; }
	inline void set_U24fieldU2D17E54FCA28103DF892BBB946DDEED4B061D7F5C7_160(U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723  value)
	{
		___U24fieldU2D17E54FCA28103DF892BBB946DDEED4B061D7F5C7_160 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DFE5567E8D769550852182CDF69D74BB16DFF8E29_161() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DFE5567E8D769550852182CDF69D74BB16DFF8E29_161)); }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  get_U24fieldU2DFE5567E8D769550852182CDF69D74BB16DFF8E29_161() const { return ___U24fieldU2DFE5567E8D769550852182CDF69D74BB16DFF8E29_161; }
	inline U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326 * get_address_of_U24fieldU2DFE5567E8D769550852182CDF69D74BB16DFF8E29_161() { return &___U24fieldU2DFE5567E8D769550852182CDF69D74BB16DFF8E29_161; }
	inline void set_U24fieldU2DFE5567E8D769550852182CDF69D74BB16DFF8E29_161(U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326  value)
	{
		___U24fieldU2DFE5567E8D769550852182CDF69D74BB16DFF8E29_161 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_162() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_162)); }
	inline U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5  get_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_162() const { return ___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_162; }
	inline U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5 * get_address_of_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_162() { return &___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_162; }
	inline void set_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_162(U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5  value)
	{
		___U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_162 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_163() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_163)); }
	inline U24ArrayTypeU3D76_tA66D1F76A98126C3A8C881FB7A1C3D6647924CC3  get_U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_163() const { return ___U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_163; }
	inline U24ArrayTypeU3D76_tA66D1F76A98126C3A8C881FB7A1C3D6647924CC3 * get_address_of_U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_163() { return &___U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_163; }
	inline void set_U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_163(U24ArrayTypeU3D76_tA66D1F76A98126C3A8C881FB7A1C3D6647924CC3  value)
	{
		___U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_163 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_164() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_164)); }
	inline U24ArrayTypeU3D6144_t0FA5B30A17373E7B9445630D8FD58FD781E5DA6E  get_U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_164() const { return ___U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_164; }
	inline U24ArrayTypeU3D6144_t0FA5B30A17373E7B9445630D8FD58FD781E5DA6E * get_address_of_U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_164() { return &___U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_164; }
	inline void set_U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_164(U24ArrayTypeU3D6144_t0FA5B30A17373E7B9445630D8FD58FD781E5DA6E  value)
	{
		___U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_164 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_165() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_165)); }
	inline U24ArrayTypeU3D384_t2196F8B9AC0E7C9D619BA635541A7E6C3D3A6891  get_U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_165() const { return ___U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_165; }
	inline U24ArrayTypeU3D384_t2196F8B9AC0E7C9D619BA635541A7E6C3D3A6891 * get_address_of_U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_165() { return &___U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_165; }
	inline void set_U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_165(U24ArrayTypeU3D384_t2196F8B9AC0E7C9D619BA635541A7E6C3D3A6891  value)
	{
		___U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_165 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_166() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_166)); }
	inline U24ArrayTypeU3D124_tC53BBB2379F45A189B7240430D076326E331393F  get_U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_166() const { return ___U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_166; }
	inline U24ArrayTypeU3D124_tC53BBB2379F45A189B7240430D076326E331393F * get_address_of_U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_166() { return &___U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_166; }
	inline void set_U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_166(U24ArrayTypeU3D124_tC53BBB2379F45A189B7240430D076326E331393F  value)
	{
		___U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_166 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_167() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_167)); }
	inline U24ArrayTypeU3D124_tC53BBB2379F45A189B7240430D076326E331393F  get_U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_167() const { return ___U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_167; }
	inline U24ArrayTypeU3D124_tC53BBB2379F45A189B7240430D076326E331393F * get_address_of_U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_167() { return &___U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_167; }
	inline void set_U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_167(U24ArrayTypeU3D124_tC53BBB2379F45A189B7240430D076326E331393F  value)
	{
		___U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_167 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_168() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_168)); }
	inline U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B  get_U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_168() const { return ___U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_168; }
	inline U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B * get_address_of_U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_168() { return &___U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_168; }
	inline void set_U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_168(U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B  value)
	{
		___U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_168 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_169() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_169)); }
	inline U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B  get_U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_169() const { return ___U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_169; }
	inline U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B * get_address_of_U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_169() { return &___U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_169; }
	inline void set_U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_169(U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B  value)
	{
		___U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_169 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_170() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_170)); }
	inline U24ArrayTypeU3D1152_t26C03C6F9BF348D112C4FCF00BB0EC8807B477FF  get_U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_170() const { return ___U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_170; }
	inline U24ArrayTypeU3D1152_t26C03C6F9BF348D112C4FCF00BB0EC8807B477FF * get_address_of_U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_170() { return &___U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_170; }
	inline void set_U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_170(U24ArrayTypeU3D1152_t26C03C6F9BF348D112C4FCF00BB0EC8807B477FF  value)
	{
		___U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_170 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_171() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_171)); }
	inline U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B  get_U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_171() const { return ___U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_171; }
	inline U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B * get_address_of_U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_171() { return &___U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_171; }
	inline void set_U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_171(U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B  value)
	{
		___U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_171 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_172() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_172)); }
	inline U24ArrayTypeU3D116_t3D3A23342468E840802FEE05074D0A8F384077CF  get_U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_172() const { return ___U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_172; }
	inline U24ArrayTypeU3D116_t3D3A23342468E840802FEE05074D0A8F384077CF * get_address_of_U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_172() { return &___U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_172; }
	inline void set_U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_172(U24ArrayTypeU3D116_t3D3A23342468E840802FEE05074D0A8F384077CF  value)
	{
		___U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_172 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_173() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_173)); }
	inline U24ArrayTypeU3D76_tA66D1F76A98126C3A8C881FB7A1C3D6647924CC3  get_U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_173() const { return ___U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_173; }
	inline U24ArrayTypeU3D76_tA66D1F76A98126C3A8C881FB7A1C3D6647924CC3 * get_address_of_U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_173() { return &___U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_173; }
	inline void set_U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_173(U24ArrayTypeU3D76_tA66D1F76A98126C3A8C881FB7A1C3D6647924CC3  value)
	{
		___U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_173 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_174() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_174)); }
	inline U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  get_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_174() const { return ___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_174; }
	inline U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3 * get_address_of_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_174() { return &___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_174; }
	inline void set_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_174(U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3  value)
	{
		___U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_174 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_175() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_175)); }
	inline U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6  get_U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_175() const { return ___U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_175; }
	inline U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6 * get_address_of_U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_175() { return &___U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_175; }
	inline void set_U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_175(U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6  value)
	{
		___U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_175 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_176() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_176)); }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  get_U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_176() const { return ___U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_176; }
	inline U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D * get_address_of_U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_176() { return &___U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_176; }
	inline void set_U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_176(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D  value)
	{
		___U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_176 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_177() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_177)); }
	inline U24ArrayTypeU3D116_t3D3A23342468E840802FEE05074D0A8F384077CF  get_U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_177() const { return ___U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_177; }
	inline U24ArrayTypeU3D116_t3D3A23342468E840802FEE05074D0A8F384077CF * get_address_of_U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_177() { return &___U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_177; }
	inline void set_U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_177(U24ArrayTypeU3D116_t3D3A23342468E840802FEE05074D0A8F384077CF  value)
	{
		___U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_177 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_178() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields, ___U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_178)); }
	inline U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B  get_U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_178() const { return ___U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_178; }
	inline U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B * get_address_of_U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_178() { return &___U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_178; }
	inline void set_U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_178(U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B  value)
	{
		___U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_178 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_H
#ifndef CERTIFICATEEXCEPTION_T4F698F8F521570F8F7C7AC8447C61B408D8665C4_H
#define CERTIFICATEEXCEPTION_T4F698F8F521570F8F7C7AC8447C61B408D8665C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Security.Certificates.CertificateException
struct  CertificateException_t4F698F8F521570F8F7C7AC8447C61B408D8665C4  : public GeneralSecurityException_t070EA34F73C23E41F54F84969CBCE09BC9BCA880
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEEXCEPTION_T4F698F8F521570F8F7C7AC8447C61B408D8665C4_H
#ifndef DIGESTALGORITHM_TDBD0CFCAD3C556A9844EF400A7EC9D847F2B11F8_H
#define DIGESTALGORITHM_TDBD0CFCAD3C556A9844EF400A7EC9D847F2B11F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Security.DigestUtilities_DigestAlgorithm
struct  DigestAlgorithm_tDBD0CFCAD3C556A9844EF400A7EC9D847F2B11F8 
{
public:
	// System.Int32 Org.BouncyCastle.Security.DigestUtilities_DigestAlgorithm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DigestAlgorithm_tDBD0CFCAD3C556A9844EF400A7EC9D847F2B11F8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTALGORITHM_TDBD0CFCAD3C556A9844EF400A7EC9D847F2B11F8_H
#ifndef KEYEXCEPTION_T6BD78CFAC566FFB1A572483FD70874960C7C5CE8_H
#define KEYEXCEPTION_T6BD78CFAC566FFB1A572483FD70874960C7C5CE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Security.KeyException
struct  KeyException_t6BD78CFAC566FFB1A572483FD70874960C7C5CE8  : public GeneralSecurityException_t070EA34F73C23E41F54F84969CBCE09BC9BCA880
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEXCEPTION_T6BD78CFAC566FFB1A572483FD70874960C7C5CE8_H
#ifndef DATETIMEUTILITIES_T8BC9D1BA2717327FB81FFFEBF24074D322EAAD49_H
#define DATETIMEUTILITIES_T8BC9D1BA2717327FB81FFFEBF24074D322EAAD49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Date.DateTimeUtilities
struct  DateTimeUtilities_t8BC9D1BA2717327FB81FFFEBF24074D322EAAD49  : public RuntimeObject
{
public:

public:
};

struct DateTimeUtilities_t8BC9D1BA2717327FB81FFFEBF24074D322EAAD49_StaticFields
{
public:
	// System.DateTime Org.BouncyCastle.Utilities.Date.DateTimeUtilities::UnixEpoch
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___UnixEpoch_0;

public:
	inline static int32_t get_offset_of_UnixEpoch_0() { return static_cast<int32_t>(offsetof(DateTimeUtilities_t8BC9D1BA2717327FB81FFFEBF24074D322EAAD49_StaticFields, ___UnixEpoch_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_UnixEpoch_0() const { return ___UnixEpoch_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_UnixEpoch_0() { return &___UnixEpoch_0; }
	inline void set_UnixEpoch_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___UnixEpoch_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEUTILITIES_T8BC9D1BA2717327FB81FFFEBF24074D322EAAD49_H
#ifndef BASEINPUTSTREAM_T391EE4FC5A02C738BAA41F88320C83E7C99061A7_H
#define BASEINPUTSTREAM_T391EE4FC5A02C738BAA41F88320C83E7C99061A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.IO.BaseInputStream
struct  BaseInputStream_t391EE4FC5A02C738BAA41F88320C83E7C99061A7  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean Org.BouncyCastle.Utilities.IO.BaseInputStream::closed
	bool ___closed_5;

public:
	inline static int32_t get_offset_of_closed_5() { return static_cast<int32_t>(offsetof(BaseInputStream_t391EE4FC5A02C738BAA41F88320C83E7C99061A7, ___closed_5)); }
	inline bool get_closed_5() const { return ___closed_5; }
	inline bool* get_address_of_closed_5() { return &___closed_5; }
	inline void set_closed_5(bool value)
	{
		___closed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTSTREAM_T391EE4FC5A02C738BAA41F88320C83E7C99061A7_H
#ifndef BASEOUTPUTSTREAM_TA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832_H
#define BASEOUTPUTSTREAM_TA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.IO.BaseOutputStream
struct  BaseOutputStream_tA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean Org.BouncyCastle.Utilities.IO.BaseOutputStream::closed
	bool ___closed_5;

public:
	inline static int32_t get_offset_of_closed_5() { return static_cast<int32_t>(offsetof(BaseOutputStream_tA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832, ___closed_5)); }
	inline bool get_closed_5() const { return ___closed_5; }
	inline bool* get_address_of_closed_5() { return &___closed_5; }
	inline void set_closed_5(bool value)
	{
		___closed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEOUTPUTSTREAM_TA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832_H
#ifndef FILTERSTREAM_T88E5588F96DA875764C392A00134F11FFAEFF567_H
#define FILTERSTREAM_T88E5588F96DA875764C392A00134F11FFAEFF567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.IO.FilterStream
struct  FilterStream_t88E5588F96DA875764C392A00134F11FFAEFF567  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.IO.Stream Org.BouncyCastle.Utilities.IO.FilterStream::s
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___s_5;

public:
	inline static int32_t get_offset_of_s_5() { return static_cast<int32_t>(offsetof(FilterStream_t88E5588F96DA875764C392A00134F11FFAEFF567, ___s_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_s_5() const { return ___s_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_s_5() { return &___s_5; }
	inline void set_s_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___s_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILTERSTREAM_T88E5588F96DA875764C392A00134F11FFAEFF567_H
#ifndef ZOUTPUTSTREAM_TD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425_H
#define ZOUTPUTSTREAM_TD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Zlib.ZOutputStream
struct  ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// Org.BouncyCastle.Utilities.Zlib.ZStream Org.BouncyCastle.Utilities.Zlib.ZOutputStream::z
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * ___z_5;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.ZOutputStream::flushLevel
	int32_t ___flushLevel_6;
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.ZOutputStream::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_7;
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.ZOutputStream::buf1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf1_8;
	// System.Boolean Org.BouncyCastle.Utilities.Zlib.ZOutputStream::compress
	bool ___compress_9;
	// System.IO.Stream Org.BouncyCastle.Utilities.Zlib.ZOutputStream::output
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___output_10;
	// System.Boolean Org.BouncyCastle.Utilities.Zlib.ZOutputStream::closed
	bool ___closed_11;

public:
	inline static int32_t get_offset_of_z_5() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___z_5)); }
	inline ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * get_z_5() const { return ___z_5; }
	inline ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB ** get_address_of_z_5() { return &___z_5; }
	inline void set_z_5(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * value)
	{
		___z_5 = value;
		Il2CppCodeGenWriteBarrier((&___z_5), value);
	}

	inline static int32_t get_offset_of_flushLevel_6() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___flushLevel_6)); }
	inline int32_t get_flushLevel_6() const { return ___flushLevel_6; }
	inline int32_t* get_address_of_flushLevel_6() { return &___flushLevel_6; }
	inline void set_flushLevel_6(int32_t value)
	{
		___flushLevel_6 = value;
	}

	inline static int32_t get_offset_of_buf_7() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___buf_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_7() const { return ___buf_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_7() { return &___buf_7; }
	inline void set_buf_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_7 = value;
		Il2CppCodeGenWriteBarrier((&___buf_7), value);
	}

	inline static int32_t get_offset_of_buf1_8() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___buf1_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf1_8() const { return ___buf1_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf1_8() { return &___buf1_8; }
	inline void set_buf1_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf1_8 = value;
		Il2CppCodeGenWriteBarrier((&___buf1_8), value);
	}

	inline static int32_t get_offset_of_compress_9() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___compress_9)); }
	inline bool get_compress_9() const { return ___compress_9; }
	inline bool* get_address_of_compress_9() { return &___compress_9; }
	inline void set_compress_9(bool value)
	{
		___compress_9 = value;
	}

	inline static int32_t get_offset_of_output_10() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___output_10)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_output_10() const { return ___output_10; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_output_10() { return &___output_10; }
	inline void set_output_10(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___output_10 = value;
		Il2CppCodeGenWriteBarrier((&___output_10), value);
	}

	inline static int32_t get_offset_of_closed_11() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___closed_11)); }
	inline bool get_closed_11() const { return ___closed_11; }
	inline bool* get_address_of_closed_11() { return &___closed_11; }
	inline void set_closed_11(bool value)
	{
		___closed_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOUTPUTSTREAM_TD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425_H
#ifndef INVALIDCASTEXCEPTION_T91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA_H
#define INVALIDCASTEXCEPTION_T91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidCastException
struct  InvalidCastException_t91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDCASTEXCEPTION_T91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA_H
#ifndef CERTIFICATEPARSINGEXCEPTION_T6C8F0481E64C19E90013D4461EB7B8BD8E2E591C_H
#define CERTIFICATEPARSINGEXCEPTION_T6C8F0481E64C19E90013D4461EB7B8BD8E2E591C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Security.Certificates.CertificateParsingException
struct  CertificateParsingException_t6C8F0481E64C19E90013D4461EB7B8BD8E2E591C  : public CertificateException_t4F698F8F521570F8F7C7AC8447C61B408D8665C4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEPARSINGEXCEPTION_T6C8F0481E64C19E90013D4461EB7B8BD8E2E591C_H
#ifndef INVALIDKEYEXCEPTION_TA114E16EF741C300A60BC69D8A856B6AAB286FB0_H
#define INVALIDKEYEXCEPTION_TA114E16EF741C300A60BC69D8A856B6AAB286FB0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Security.InvalidKeyException
struct  InvalidKeyException_tA114E16EF741C300A60BC69D8A856B6AAB286FB0  : public KeyException_t6BD78CFAC566FFB1A572483FD70874960C7C5CE8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDKEYEXCEPTION_TA114E16EF741C300A60BC69D8A856B6AAB286FB0_H
#ifndef INVALIDPARAMETEREXCEPTION_T3AED9F79BAF414D5430A9855CF9C0F286C7D7023_H
#define INVALIDPARAMETEREXCEPTION_T3AED9F79BAF414D5430A9855CF9C0F286C7D7023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Security.InvalidParameterException
struct  InvalidParameterException_t3AED9F79BAF414D5430A9855CF9C0F286C7D7023  : public KeyException_t6BD78CFAC566FFB1A572483FD70874960C7C5CE8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDPARAMETEREXCEPTION_T3AED9F79BAF414D5430A9855CF9C0F286C7D7023_H
#ifndef PUSHBACKSTREAM_TABFBB14946E8D4AE29AEDDF1710C4313A3A1E98B_H
#define PUSHBACKSTREAM_TABFBB14946E8D4AE29AEDDF1710C4313A3A1E98B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.IO.PushbackStream
struct  PushbackStream_tABFBB14946E8D4AE29AEDDF1710C4313A3A1E98B  : public FilterStream_t88E5588F96DA875764C392A00134F11FFAEFF567
{
public:
	// System.Int32 Org.BouncyCastle.Utilities.IO.PushbackStream::buf
	int32_t ___buf_6;

public:
	inline static int32_t get_offset_of_buf_6() { return static_cast<int32_t>(offsetof(PushbackStream_tABFBB14946E8D4AE29AEDDF1710C4313A3A1E98B, ___buf_6)); }
	inline int32_t get_buf_6() const { return ___buf_6; }
	inline int32_t* get_address_of_buf_6() { return &___buf_6; }
	inline void set_buf_6(int32_t value)
	{
		___buf_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUSHBACKSTREAM_TABFBB14946E8D4AE29AEDDF1710C4313A3A1E98B_H
#ifndef TEEINPUTSTREAM_T2094653A76A98A92BF98E40AD972D6E8B2751675_H
#define TEEINPUTSTREAM_T2094653A76A98A92BF98E40AD972D6E8B2751675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.IO.TeeInputStream
struct  TeeInputStream_t2094653A76A98A92BF98E40AD972D6E8B2751675  : public BaseInputStream_t391EE4FC5A02C738BAA41F88320C83E7C99061A7
{
public:
	// System.IO.Stream Org.BouncyCastle.Utilities.IO.TeeInputStream::input
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___input_6;
	// System.IO.Stream Org.BouncyCastle.Utilities.IO.TeeInputStream::tee
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___tee_7;

public:
	inline static int32_t get_offset_of_input_6() { return static_cast<int32_t>(offsetof(TeeInputStream_t2094653A76A98A92BF98E40AD972D6E8B2751675, ___input_6)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_input_6() const { return ___input_6; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_input_6() { return &___input_6; }
	inline void set_input_6(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___input_6 = value;
		Il2CppCodeGenWriteBarrier((&___input_6), value);
	}

	inline static int32_t get_offset_of_tee_7() { return static_cast<int32_t>(offsetof(TeeInputStream_t2094653A76A98A92BF98E40AD972D6E8B2751675, ___tee_7)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_tee_7() const { return ___tee_7; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_tee_7() { return &___tee_7; }
	inline void set_tee_7(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___tee_7 = value;
		Il2CppCodeGenWriteBarrier((&___tee_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEEINPUTSTREAM_T2094653A76A98A92BF98E40AD972D6E8B2751675_H
#ifndef MEMOABLERESETEXCEPTION_TB05FA707B0D9A62F83D1FA10762C822163A60CDC_H
#define MEMOABLERESETEXCEPTION_TB05FA707B0D9A62F83D1FA10762C822163A60CDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.MemoableResetException
struct  MemoableResetException_tB05FA707B0D9A62F83D1FA10762C822163A60CDC  : public InvalidCastException_t91DF9E7D7FCCDA6C562CB4A9A18903E016680FDA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMOABLERESETEXCEPTION_TB05FA707B0D9A62F83D1FA10762C822163A60CDC_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5400 = { sizeof (Nat192_tAE48D7B0E4353214D3C4BBD54850ED08F4EC2628), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5401 = { sizeof (Nat224_tA44CD0B1DA6238650B0A01CF4EDCD876AA9CAE9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5402 = { sizeof (Nat256_tAF0A99BB5510054032153DB7A0E80C5E435F4252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5403 = { sizeof (Nat320_t0D3A378BFCD60326B30640B307530B994EA2316E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5404 = { sizeof (Nat384_tC26A95526871DA324E89D5057FC751D2941B3020), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5405 = { sizeof (Nat448_t74497AC85F8C2E5987378EB4D68E63902F6B2CF0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5406 = { sizeof (Nat512_t877FE82CD2D8E4D386B601D14807DBC65073E860), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5407 = { sizeof (Nat576_tA562984E4FFA99F0EDF81A437AE8B8FA8F5358CF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5408 = { sizeof (DigestUtilities_t55BF65B32A85C65E66B412315C65A9CB4390CF56), -1, sizeof(DigestUtilities_t55BF65B32A85C65E66B412315C65A9CB4390CF56_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5408[2] = 
{
	DigestUtilities_t55BF65B32A85C65E66B412315C65A9CB4390CF56_StaticFields::get_offset_of_algorithms_0(),
	DigestUtilities_t55BF65B32A85C65E66B412315C65A9CB4390CF56_StaticFields::get_offset_of_oids_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5409 = { sizeof (DigestAlgorithm_tDBD0CFCAD3C556A9844EF400A7EC9D847F2B11F8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5409[29] = 
{
	DigestAlgorithm_tDBD0CFCAD3C556A9844EF400A7EC9D847F2B11F8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5410 = { sizeof (GeneralSecurityException_t070EA34F73C23E41F54F84969CBCE09BC9BCA880), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5411 = { sizeof (InvalidKeyException_tA114E16EF741C300A60BC69D8A856B6AAB286FB0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5412 = { sizeof (InvalidParameterException_t3AED9F79BAF414D5430A9855CF9C0F286C7D7023), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5413 = { sizeof (KeyException_t6BD78CFAC566FFB1A572483FD70874960C7C5CE8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5414 = { sizeof (MacUtilities_tD27296E701E6EE768F49104A5FA7E42EE372AFFA), -1, sizeof(MacUtilities_tD27296E701E6EE768F49104A5FA7E42EE372AFFA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5414[1] = 
{
	MacUtilities_tD27296E701E6EE768F49104A5FA7E42EE372AFFA_StaticFields::get_offset_of_algorithms_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5415 = { sizeof (PublicKeyFactory_tC68C0AE99ACFD7B1242CE490A66330A2F8C35172), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5416 = { sizeof (SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0), -1, sizeof(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5416[4] = 
{
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0_StaticFields::get_offset_of_counter_3(),
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0_StaticFields::get_offset_of_master_4(),
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0::get_offset_of_generator_5(),
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0_StaticFields::get_offset_of_DoubleScale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5417 = { sizeof (SecurityUtilityException_t1A9B40C730C498CCADC4BAF7EE932E09E0354089), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5418 = { sizeof (SignerUtilities_t316BEDC8F9DED4FA92BB8447C5DABF873B9F7D28), -1, sizeof(SignerUtilities_t316BEDC8F9DED4FA92BB8447C5DABF873B9F7D28_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5418[2] = 
{
	SignerUtilities_t316BEDC8F9DED4FA92BB8447C5DABF873B9F7D28_StaticFields::get_offset_of_algorithms_0(),
	SignerUtilities_t316BEDC8F9DED4FA92BB8447C5DABF873B9F7D28_StaticFields::get_offset_of_oids_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5419 = { sizeof (CertificateException_t4F698F8F521570F8F7C7AC8447C61B408D8665C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5420 = { sizeof (CertificateParsingException_t6C8F0481E64C19E90013D4461EB7B8BD8E2E591C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5421 = { sizeof (Arrays_t63735DA71DBD670D3F511132B0BBE4DE7C525AAF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5422 = { sizeof (BigIntegers_tAF2E32490FD837FC4E6890B161025FCF3834B96A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5423 = { sizeof (Enums_tA03EC3F31800DE4E15A42CEF7F64F6859124C392), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5424 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5425 = { sizeof (Integers_t7C9F57668BA0D3F43F0516CC405C69B05577ADD0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5426 = { sizeof (MemoableResetException_tB05FA707B0D9A62F83D1FA10762C822163A60CDC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5427 = { sizeof (Platform_t0DACA1EE9C17BB52DA4EECD7506573F6D4FEC7E4), -1, sizeof(Platform_t0DACA1EE9C17BB52DA4EECD7506573F6D4FEC7E4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5427[2] = 
{
	Platform_t0DACA1EE9C17BB52DA4EECD7506573F6D4FEC7E4_StaticFields::get_offset_of_InvariantCompareInfo_0(),
	Platform_t0DACA1EE9C17BB52DA4EECD7506573F6D4FEC7E4_StaticFields::get_offset_of_NewLine_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5428 = { sizeof (Strings_t28AD15CF2250A7FBB9E5140D3D3740C67F4C55F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5429 = { sizeof (Times_tD93A13784662FC42F65483C22AAB50A79154E925), -1, sizeof(Times_tD93A13784662FC42F65483C22AAB50A79154E925_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5429[1] = 
{
	Times_tD93A13784662FC42F65483C22AAB50A79154E925_StaticFields::get_offset_of_NanosecondsPerTick_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5430 = { sizeof (CollectionUtilities_t73DD2B74FBF3824DB87A35CE64B96D45E7D68420), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5431 = { sizeof (EnumerableProxy_tB7FC60E6BF21CA2EE047E3FAA1E411E1DE91C84E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5431[1] = 
{
	EnumerableProxy_tB7FC60E6BF21CA2EE047E3FAA1E411E1DE91C84E::get_offset_of_inner_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5432 = { sizeof (DateTimeUtilities_t8BC9D1BA2717327FB81FFFEBF24074D322EAAD49), -1, sizeof(DateTimeUtilities_t8BC9D1BA2717327FB81FFFEBF24074D322EAAD49_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5432[1] = 
{
	DateTimeUtilities_t8BC9D1BA2717327FB81FFFEBF24074D322EAAD49_StaticFields::get_offset_of_UnixEpoch_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5433 = { sizeof (Base64_tF9625F882314C6CE3CFF6C14CFAC9974D9ACBACF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5434 = { sizeof (Hex_tEF946DC3EA341835053763B600C2B26C0E375EAD), -1, sizeof(Hex_tEF946DC3EA341835053763B600C2B26C0E375EAD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5434[1] = 
{
	Hex_tEF946DC3EA341835053763B600C2B26C0E375EAD_StaticFields::get_offset_of_encoder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5435 = { sizeof (HexEncoder_t01DC94DCC847A783F93BA4866482C2E22E946192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5435[2] = 
{
	HexEncoder_t01DC94DCC847A783F93BA4866482C2E22E946192::get_offset_of_encodingTable_0(),
	HexEncoder_t01DC94DCC847A783F93BA4866482C2E22E946192::get_offset_of_decodingTable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5436 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5437 = { sizeof (BaseInputStream_t391EE4FC5A02C738BAA41F88320C83E7C99061A7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5437[1] = 
{
	BaseInputStream_t391EE4FC5A02C738BAA41F88320C83E7C99061A7::get_offset_of_closed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5438 = { sizeof (BaseOutputStream_tA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5438[1] = 
{
	BaseOutputStream_tA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832::get_offset_of_closed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5439 = { sizeof (FilterStream_t88E5588F96DA875764C392A00134F11FFAEFF567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5439[1] = 
{
	FilterStream_t88E5588F96DA875764C392A00134F11FFAEFF567::get_offset_of_s_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5440 = { sizeof (PushbackStream_tABFBB14946E8D4AE29AEDDF1710C4313A3A1E98B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5440[1] = 
{
	PushbackStream_tABFBB14946E8D4AE29AEDDF1710C4313A3A1E98B::get_offset_of_buf_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5441 = { sizeof (Streams_tFFA2017D63B0F62D5084C134EB05893F950E7851), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5442 = { sizeof (TeeInputStream_t2094653A76A98A92BF98E40AD972D6E8B2751675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5442[2] = 
{
	TeeInputStream_t2094653A76A98A92BF98E40AD972D6E8B2751675::get_offset_of_input_6(),
	TeeInputStream_t2094653A76A98A92BF98E40AD972D6E8B2751675::get_offset_of_tee_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5443 = { sizeof (Adler32_t820E77D81E138AD13711AC3662B7DC9EDD1C9F97), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5444 = { sizeof (Deflate_tA360BF189910682C62915B83E8F061F816C886E5), -1, sizeof(Deflate_tA360BF189910682C62915B83E8F061F816C886E5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5444[59] = 
{
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5_StaticFields::get_offset_of_config_table_0(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5_StaticFields::get_offset_of_z_errmsg_1(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_strm_2(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_status_3(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_pending_buf_4(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_pending_buf_size_5(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_pending_out_6(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_pending_7(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_noheader_8(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_data_type_9(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_method_10(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_last_flush_11(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_w_size_12(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_w_bits_13(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_w_mask_14(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_window_15(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_window_size_16(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_prev_17(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_head_18(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_ins_h_19(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_hash_size_20(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_hash_bits_21(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_hash_mask_22(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_hash_shift_23(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_block_start_24(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_match_length_25(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_prev_match_26(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_match_available_27(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_strstart_28(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_match_start_29(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_lookahead_30(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_prev_length_31(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_max_chain_length_32(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_max_lazy_match_33(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_level_34(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_strategy_35(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_good_match_36(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_nice_match_37(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_dyn_ltree_38(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_dyn_dtree_39(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_bl_tree_40(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_l_desc_41(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_d_desc_42(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_bl_desc_43(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_bl_count_44(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_heap_45(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_heap_len_46(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_heap_max_47(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_depth_48(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_l_buf_49(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_lit_bufsize_50(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_last_lit_51(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_d_buf_52(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_opt_len_53(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_static_len_54(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_matches_55(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_last_eob_len_56(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_bi_buf_57(),
	Deflate_tA360BF189910682C62915B83E8F061F816C886E5::get_offset_of_bi_valid_58(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5445 = { sizeof (Config_tBE4BEDAB07B6FBC6A133D3D33E04A3381079E203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5445[5] = 
{
	Config_tBE4BEDAB07B6FBC6A133D3D33E04A3381079E203::get_offset_of_good_length_0(),
	Config_tBE4BEDAB07B6FBC6A133D3D33E04A3381079E203::get_offset_of_max_lazy_1(),
	Config_tBE4BEDAB07B6FBC6A133D3D33E04A3381079E203::get_offset_of_nice_length_2(),
	Config_tBE4BEDAB07B6FBC6A133D3D33E04A3381079E203::get_offset_of_max_chain_3(),
	Config_tBE4BEDAB07B6FBC6A133D3D33E04A3381079E203::get_offset_of_func_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5446 = { sizeof (InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4), -1, sizeof(InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5446[21] = 
{
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4_StaticFields::get_offset_of_inflate_mask_0(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4_StaticFields::get_offset_of_border_1(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_mode_2(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_left_3(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_table_4(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_index_5(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_blens_6(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_bb_7(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_tb_8(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_codes_9(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_last_10(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_bitk_11(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_bitb_12(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_hufts_13(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_window_14(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_end_15(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_read_16(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_write_17(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_checkfn_18(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_check_19(),
	InfBlocks_t3776F9F3F5DC03C5E5405FAF46F1C54652A9A4D4::get_offset_of_inftree_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5447 = { sizeof (InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743), -1, sizeof(InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5447[15] = 
{
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743_StaticFields::get_offset_of_inflate_mask_0(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_mode_1(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_len_2(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_tree_3(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_tree_index_4(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_need_5(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_lit_6(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_get_7(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_dist_8(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_lbits_9(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_dbits_10(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_ltree_11(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_ltree_index_12(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_dtree_13(),
	InfCodes_tBC425671E2190CB31E6C3230877ABC11F7031743::get_offset_of_dtree_index_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5448 = { sizeof (InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2), -1, sizeof(InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5448[12] = 
{
	InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields::get_offset_of_fixed_tl_0(),
	InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields::get_offset_of_fixed_td_1(),
	InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields::get_offset_of_cplens_2(),
	InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields::get_offset_of_cplext_3(),
	InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields::get_offset_of_cpdist_4(),
	InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2_StaticFields::get_offset_of_cpdext_5(),
	InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2::get_offset_of_hn_6(),
	InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2::get_offset_of_v_7(),
	InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2::get_offset_of_c_8(),
	InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2::get_offset_of_r_9(),
	InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2::get_offset_of_u_10(),
	InfTree_t5B928E492CC48FB71F79556EA6E70A8CB9A400B2::get_offset_of_x_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5449 = { sizeof (Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180), -1, sizeof(Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5449[9] = 
{
	Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180::get_offset_of_mode_0(),
	Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180::get_offset_of_method_1(),
	Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180::get_offset_of_was_2(),
	Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180::get_offset_of_need_3(),
	Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180::get_offset_of_marker_4(),
	Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180::get_offset_of_nowrap_5(),
	Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180::get_offset_of_wbits_6(),
	Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180::get_offset_of_blocks_7(),
	Inflate_tAA980C6AB931D08786B9F940FDC5EE875427E180_StaticFields::get_offset_of_mark_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5450 = { sizeof (StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5), -1, sizeof(StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5450[10] = 
{
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_StaticFields::get_offset_of_static_ltree_0(),
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_StaticFields::get_offset_of_static_dtree_1(),
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_StaticFields::get_offset_of_static_l_desc_2(),
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_StaticFields::get_offset_of_static_d_desc_3(),
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5_StaticFields::get_offset_of_static_bl_desc_4(),
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5::get_offset_of_static_tree_5(),
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5::get_offset_of_extra_bits_6(),
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5::get_offset_of_extra_base_7(),
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5::get_offset_of_elems_8(),
	StaticTree_t15906B219F5C1963EA3DFDBE8EE64E665B4DAED5::get_offset_of_max_length_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5451 = { sizeof (Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D), -1, sizeof(Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5451[11] = 
{
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields::get_offset_of_extra_lbits_0(),
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields::get_offset_of_extra_dbits_1(),
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields::get_offset_of_extra_blbits_2(),
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields::get_offset_of_bl_order_3(),
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields::get_offset_of__dist_code_4(),
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields::get_offset_of__length_code_5(),
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields::get_offset_of_base_length_6(),
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D_StaticFields::get_offset_of_base_dist_7(),
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D::get_offset_of_dyn_tree_8(),
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D::get_offset_of_max_code_9(),
	Tree_t5D1AEE8FF0C0886844A468224840118DD973FD6D::get_offset_of_stat_desc_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5452 = { sizeof (ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5452[7] = 
{
	ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425::get_offset_of_z_5(),
	ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425::get_offset_of_flushLevel_6(),
	ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425::get_offset_of_buf_7(),
	ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425::get_offset_of_buf1_8(),
	ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425::get_offset_of_compress_9(),
	ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425::get_offset_of_output_10(),
	ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425::get_offset_of_closed_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5453 = { sizeof (ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5453[14] = 
{
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of_next_in_0(),
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of_next_in_index_1(),
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of_avail_in_2(),
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of_total_in_3(),
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of_next_out_4(),
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of_next_out_index_5(),
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of_avail_out_6(),
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of_total_out_7(),
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of_msg_8(),
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of_dstate_9(),
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of_istate_10(),
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of_data_type_11(),
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of_adler_12(),
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB::get_offset_of__adler_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5454 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5455 = { sizeof (PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5455[4] = 
{
	PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D::get_offset_of__header1_0(),
	PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D::get_offset_of__header2_1(),
	PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D::get_offset_of__footer1_2(),
	PemParser_tC3625C27825A6A5C2F2F29B4E6386841272C433D::get_offset_of__footer2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5456 = { sizeof (X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5456[5] = 
{
	X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786::get_offset_of_c_0(),
	X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786::get_offset_of_basicConstraints_1(),
	X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786::get_offset_of_keyUsage_2(),
	X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786::get_offset_of_hashValueSet_3(),
	X509Certificate_t9350C69721F95FA663C0FD6ED97CDF09B8037786::get_offset_of_hashValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5457 = { sizeof (X509CertificateParser_tB1C6ACC492089BDFD07811CD6CFE1C1B7C128010), -1, sizeof(X509CertificateParser_tB1C6ACC492089BDFD07811CD6CFE1C1B7C128010_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5457[4] = 
{
	X509CertificateParser_tB1C6ACC492089BDFD07811CD6CFE1C1B7C128010_StaticFields::get_offset_of_PemCertParser_0(),
	X509CertificateParser_tB1C6ACC492089BDFD07811CD6CFE1C1B7C128010::get_offset_of_sData_1(),
	X509CertificateParser_tB1C6ACC492089BDFD07811CD6CFE1C1B7C128010::get_offset_of_sDataObjectCount_2(),
	X509CertificateParser_tB1C6ACC492089BDFD07811CD6CFE1C1B7C128010::get_offset_of_currentStream_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5458 = { sizeof (X509ExtensionBase_tF6546E0F40D0D08B41EAE795CB9E14BCC4A6E57C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5459 = { sizeof (X509ExtensionUtilities_tDABB31F5554EFA536E8D697EFC52F35CC9EED5D8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5460 = { sizeof (U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE), -1, sizeof(U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5460[179] = 
{
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DCC5B8B6FA17DA57B26C3ACA4DA8B2A477C609D63_1(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D90A0542282A011472F94E97CEAE59F8B3B1A3291_2(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DD2C5BAE967587C6F3D9F2C4551911E0575A1101F_3(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D433175D38B13FFE177FDD661A309F1B528B3F6E2_4(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DA0FABB8173BA247898A9FA267D0CE05500B667A0_5(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D467C6758F235D3193618192A64129CBB602C9067_6(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D1648F737A4CFFDA4E6C83A3D742109BF9DBC2446_7(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DC079C42AC966756C902EC38C4D7989F3C20D3625_8(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D840B3A53AAF3595FDF3313D46FFD246A7EA6E89E_9(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D8F22C9ECE1331718CBD268A9BBFD2F5E451441E3_10(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D0C4110BC17D746F018F47B49E0EB0D6590F69939_11(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DF83C7B497B14628C1D0DB3FA2FD89BD5C0D5138A_12(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DB68637EF60D499620B99E336C59E4865FFC4C5D7_13(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D1AFB455399A50580CF1039188ABA6BE82F309543_14(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DAE843E1C1136C908565A6D4E04E8564B69465B3B_15(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DB1108EE6609DB783B2EC606B3BFDD544A7D4C2B3_16(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D14F6FAB1B4065EBADBBA4A3661ADE689FF444EDD_17(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DB718C95C87C6B65DFA29D58A10442CEC9EBBDF1F_18(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D124B1C35B19149213F8F7D40AA8E0ABA15DD70EC_19(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D4FFC8339E09825A68B861995F9C660EB11DBF13D_20(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DC105B70BED997DB5D36E1D2E84C1EFCB445A428C_21(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DDC2B830D8CD59AD6A4E4332D21CA0DCA2821AD82_22(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DB4FBD02AAB5B16E0F4BD858DA5D9E348F3CE501D_23(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D62BAB0F245E66C3EB982CF5A7015F0A7C3382283_24(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D04B9E6B1364BBA4125AE2866E09C57C9E8CD6DA6_25(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D3D6EB645BC212077C1B37A3A32CA2A62F7B39018_26(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D821D1E62CD072AE9EC73D238C7DE19C5C5F3A7D8_27(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D64354464C9074B5BB4369689AAA131961CD1EF19_28(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D3F653EBF39CFCB2FD7FF335DC96E82F3CDFDF0C7_29(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D6D7292FF64ED402BBFFF5E7534C0980BEBF0EEB1_30(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DFC9EEBC457831129D4AF4FF84333B481F4BED60E_31(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D4C44594E2C603D85EC6195B1A7A6C5876CBB58E2_32(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DD3BA31FA2132E3CD69D057D38B3E3ACF0A6C8A13_33(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DCC53D7FE00E6AC1385AF09521629229467BCCC86_34(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D2BA9E4B370D477F8C7FE286262D7ADC69CAF290E_35(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D1005BA20F99323E3F050E781BB81D1A4479AB037_36(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DE17D18DCD8392C99D47823F8CB9F43896D115FB8_37(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D7CFF7A50C8F8981091791CDB210243E8F465BC80_38(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D3A38ADC6BCFB84DE23160C1E50212DACBCD25A11_39(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DD9642D3FF9879EC5C4BB28AE7001CEE3D43956AB_40(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D8C7FEE53346CDB1B119FCAD8D605F476400A03CE_41(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DEEDBCB52C67688DE5F5FD9209E8A25BC786A2430_42(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DF8AB5CA414AD9084F3E8B8D887217E6DFC32C62C_43(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D641D0D9E9CE6DDAC3F12536417A6A64C8DDD4776_44(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D55BCCC431843040DD1427CB495B2FD469C4D4D20_45(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D3C3A13E15C81AD114ECBA973C706DCD985BF18D6_46(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D07B35CC0531C638EA1581B73756B3A11272C1D91_47(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D31D8729F7377B44017C0A2395A582C9CA4163277_48(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D1397D288BDA63B8D4B5F30CFFB9FF5A965AA7A1C_49(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DEE0F12B14397A7DF4588BEA8AA9B022754F4DA1B_50(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DD7231C06B1D6276752359120E26EAE206A7F74F9_51(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DAA7973F07CDE1E6AA10B6970B0072D05F38F0AB2_52(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D2FCBB2FC4E672ED0607DD7827BA69B7E6C9EB6BD_53(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D6277CE8FE3A9156D3455749B453AC88191D3C6D6_54(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D9FC36EB698A900B5D2EF5E3B1ABA28CB6A217738_55(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D714A94F3805E05CA6C00F9A46489427ABEB25D60_56(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DF128744756EEB38C3EAD4A7E8536EC5D3FA430FF_57(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D5AB421AC76CECB8E84025172585CB97DE8BECD65_58(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D6BBD3A22A185224EE0EBAB0784455E9E245376B7_59(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D7B22115C45C6AD570AFBAB744FA1058DF97CDBC1_60(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D460C77B94933562DE2E0E5B4FD72B431DFAEB5B6_61(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DCE2B1D9E3E16F8A9B2ADFD296846C7C91AB27B86_62(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DA471AF3330805980C7041F978D3CFF8838054E14_63(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DC0F40F05DBD4B7C8F77A1A55F3C5B9525FB5E124_64(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DAD4075598ACA56EC39C5E575771BBB0CFBCE24EE_65(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D9AEFA90D8E67EBAE069B4B6C071A8E867B108B31_66(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DA5981DCAA364B0DC9E0385D893A31C2022364075_67(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D4435D44E1091E6624ED6B6E4FA3C9A8C5C996098_68(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DF5AEFD834ADB72DAA720930140E9ECC087FCF389_69(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D0D4B2069E2C1085C37B7AD86C9D0C59E4CED879B_70(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D1A43D7FEEED03520E11C4A8996F149705D99C6BB_71(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D4B411385A36907D25D8088AE39AB6AAFA46B0642_72(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D29F7A0217340B5682E7DDF98ADAD952E2A360E65_73(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D68178023585F1F782745740AA583CDC778DB31B3_74(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DE53E13AFB95C5C24DF50875117B7DDCE12937B2E_75(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D6F39BC29A161CAE5394821B1FDE160EB5229AE71_76(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DD99006A6CB3C75EEC7BB6ABEA6AA9C413D59F015_77(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D50AA269217736906D8469B9191F420DC6B13A36A_78(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DC3EE470FCBE9ED2CB4A9FE76CA81B438F69F0C62_79(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DB997A0149EBF3CDD050D72AE1784E375A413B128_80(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D9597ECF10274DDDBFD265D4F66B70BAA9EAA83BD_81(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D9FAE1C8A9B3D68DAE1EEE8A0946441D7078E2021_82(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DAD19F20EECB80A2079F504CB928A26FDE10E8C47_83(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DD129FBC67222EA7D73E90E51E4DCFCA8C7497D67_84(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DB3A60EC240A886DA5AFD600CC73AE12514A881E8_85(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D512F92F4041B190727A330E2A6CC39E5D9EA06E6_86(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DAD200F5E49BD2E5038FA7BD229E2B4429ECA8CDE_87(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D03D9F1A66AED1E059B1609A09E435B708A88C8B8_88(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D26853A2C322BBAD5BBD886C60A32BBBCFE847F00_89(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DD31171F7904EB3247DD4834E43B47B1E2DCB97AC_90(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D5EF7F909EFC731E811E21521A43A80FB5AC0B229_91(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D372040F482ABADADF58EF0C31A6A8BE386AF8A50_92(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D7878E9E7126B2BDF365429C31842AE1903CD2DFF_93(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D882888781BC0DC17021FB4F11BA783038C83B313_94(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DF3BF6F581A24C57F2FFF3D2FD3290FD102BB8566_95(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DC2FEEB3C521ADDD49A534A0876BA97FF5894476E_96(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DC991C784E7697AD0F91A159F03727BF4621A5AB8_97(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DD860D5BD327368D1D4174620FE2E4A91FE9AADEC_98(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DA696E1EE8632C559732B81052E4D2993B8783877_99(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D672385C1D6A6C84A1AC3588540B09C4AE3B87DDC_100(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D71F2B0A8153A60BA8F96A7159B5B92F4CCD7AFA7_101(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D42C3E89412F11AA94E57C09EFB4B2B415C1AAB58_102(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DC2D514B39C8DFA25365195A0759A5AE28D9F2A87_103(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D613CFAEE025A3AF3C6D13DEB22E298C1925C31B5_104(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DB5E8BA68953A5283AD953094F0F391FA4502A3FA_105(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D35E6464339FFAE0D3777B12A371F82D2D1F668CA_106(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D6DEB7F74818574642B0B824B9C08B366C962A360_107(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DBEFC5B901D2D4EE83CB9EB422EFE470C4BF76C6B_108(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D5D41C56232C500092E99AC044D3C5C442B1C834F_109(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DF63639527E877A2CBCB26FFD41D4A59470BFF8C8_110(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DA72FDF28F8C2336EEEF4D5913F57EECDA0A2D3F6_111(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DC4B266E68FA20D0D222D86ADAD31EBB55118CD21_112(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D6AE5F51BDA53455ED8257137D6E6FF2E5A7ECF16_113(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DC95356610D5583976B69017BED7048EB50121B90_114(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DA4313AAA146ACFCA88681B7BFC3D644005F3792B_115(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DCA1986CBA4E4F0DFA3BF6A654EF8A36E6077AD99_116(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D239B59488F1CE7EBE225785FDC22A8E3102A2E82_117(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D310EFB639F3C7677A2A82B54EEED1124ED69E9A3_118(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D428007959831954B0C2DCFAF9DD641D629B00DBF_119(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DFA5B1C8B2F287078ED719C15595DB729BDB85911_120(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D86B0F85AC13B58F88DEFFD8FD6EE095438B98F10_121(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DEAEB85AB6D40E0AB7CE0F65EF7EDF588A4DD81C9_122(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D6AAC0DB543C50F09E879F5B9F757319773564CE1_123(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DFAF70E8DF6971D7BABBCE9FEF83EDA1D5D095D05_124(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D068B2E17352B5B9FF693CAE83421B679E0342A5C_125(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DF2842E2653F6C5E55959B5EC5E07ABAFC0191FB0_126(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D35D6BB00E88996CA4CA6EEB743BE1820C59C5FAD_127(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D310FB325A3EA3EA527B55C2F08544D1CB92C19F4_128(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DC132685022CE310ACFD3A883E0A57033A482A959_129(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D27FED0F92A97C41B08D3115553BBDC064F417B6E_130(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D60A08108A32C9D3F263B2F42095A2694B7C1C1EF_131(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D32ECB35FF8400B4E56FF5E09588FB20DD60350E7_132(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DFB3C663794DD23F500825FF78450D198FE338938_133(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DC5515C87D04DC0D00C7984096F5E35B4944C1CB6_134(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D8330271815E046D369E0B1F7673D308739FDCC07_135(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DD6898715AE96BC2F82A7BBA76E2BFC7100E282C3_136(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DA53306F44DF494A019EA1487807B59CA336BF024_137(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D0982B1B45B764F2694ABC3DE57204AC898651429_138(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D126589410FF9CA1510B9950BF0E79E5BFD60000B_139(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DE647D32D165F3510693DF9787DC98E0A0B63C5C2_140(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D66961AC9ADD7C27DCD1BCA65FCD5C02B74B62F47_141(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D059FFAA29C8FCAA8FDB47FBFDE6FB74F5A43E5C0_142(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D1E3842329C5294DBE1DF588A77C68B35C6AF83BF_143(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D8C9BE3C02B5604C5CBF6A03E8032549588A6ED54_144(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DEBE167F7962841FA83451C9C1663416D69AA5294_145(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D2CABEB86D5B3D362822AF2E5D136A10A17AD85DA_146(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D79A213B796D2AD7A89C2071B0732B78207F5CE01_147(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DBE8E2513259482B6F307AC07F23F5D9FB4841EAA_148(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D083DE622A9A685DC50D8D5653CB388A41343C8EC_149(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D214F93D9222D60794CE1EA0A10389885C5CA9824_150(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D095B351FE2104237B032546280C98C9804D331C5_151(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DCE39574ADC95015A9B5E0475EB65EE8F32353FD4_152(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DEF2680899DD1FCA444AE6B8B3B0CC6C7DD40894B_153(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D96ADC3934F8492C827987DFEE3B4DD4EF1738E78_154(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DCBF7F8D48ACC5EB9048CB8F1FCFAF93B33516965_155(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DBCE617693C33CE2C76FE00F449CA910E4C6E117E_156(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DE0219F11D9EECC43022AA94967780250AC270D4B_157(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D5E7F55149EC07597C76E6E3CD9F62274214061E6_158(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D0D0825E62E82DBEBFAD598623694129548E24C9C_159(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D17E54FCA28103DF892BBB946DDEED4B061D7F5C7_160(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DFE5567E8D769550852182CDF69D74BB16DFF8E29_161(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DF584B6C7CCA3CD4ECC3B9B1E20D2F2EFB73DBBDF_162(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D1FDC8DB567F5AAA7068D0D2A601CD71657CBDF38_163(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DA474A0BEC4E2CE8491839502AE85F6EA8504C6BD_164(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D1B180C6E41F096D53222F5E8EF558B78182CA401_165(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D8ED8F61DAA454B49CD5059AE4486C59174324E9E_166(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DDACFCC5E985D9E113ABB74724C5D3CC4FDC4FB8A_167(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DD068832E6B13A623916709C1E0E25ADCBE7B455F_168(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D79D521E6E3E55103005E9CC3FA43B3174FAF090F_169(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DEB6F545AEF284339D25594F900E7A395212460EB_170(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D850D4DC092689E1F0D8A70B6281848B27DEC0014_171(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D67C0E784F3654B008A81E2988588CF4956CCF3DA_172(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D8457F44B035C9073EE2D1F132D0A8AF5631DCDC8_173(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2DAE6B2897A8B88E297D61124152931A88D5D977F4_174(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D3544182260B8A15D332367E48C7530FC0E901FD3_175(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D6A316789EED01119DE92841832701A40AB0CABD6_176(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D2E868D9F2085DF93F11F58DE61C05E0D8A8F4A71_177(),
	U3CPrivateImplementationDetailsU3E_t9476ACA96D5EE02F5BBB48A0F6FE36EB24F0D0CE_StaticFields::get_offset_of_U24fieldU2D5581A70566F03554D8048EDBFC6E6B399AF9BCB1_178(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5461 = { sizeof (U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D2048_tAEF0826FFE3933BFCB0BCEFA4FF14D175E6C9A99 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5462 = { sizeof (U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D56_tB87BF1665D7F5C1366CBF2E3952F7D2EA1526803 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5463 = { sizeof (U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D1024_t4D19AA19C09B428FA06442BCB087DC6143DF6214 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5464 = { sizeof (U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D32_t6E9BEF962F0E02C51005F006E292E7FF25C2BC99 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5465 = { sizeof (U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D512_t09A18C6E11FCDB8D86E06A5CF62F02C85F9008E6 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5466 = { sizeof (U24ArrayTypeU3D640_t2FF86352B1300B97F0AC45DBCF28BFFF74F29242)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D640_t2FF86352B1300B97F0AC45DBCF28BFFF74F29242 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5467 = { sizeof (U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D256_t12FC2D8AB69EE5101E7A378B6FA3ED1ED6A9923D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5468 = { sizeof (U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D64_t68B6801159918258824F70A65B7BC8C186A89D05 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5469 = { sizeof (U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D16_t4FEAD91581D6D790DB3E0B6C556AF3D1CEF75326 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5470 = { sizeof (U24ArrayTypeU3D72_t2220FD4A9A26B7F357806CAF46D79E53E4F11D64)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D72_t2220FD4A9A26B7F357806CAF46D79E53E4F11D64 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5471 = { sizeof (U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D48_t915AF8EF4B514884B68AD099BA24A5315C9B12A3 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5472 = { sizeof (U24ArrayTypeU3D8_t164480F6E3D5CD24BED4D07818C671A634F568EA)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D8_t164480F6E3D5CD24BED4D07818C671A634F568EA ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5473 = { sizeof (U24ArrayTypeU3D96_t5C15B6692DC57B36771160CFA909A1E1B6A9E4A4)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D96_t5C15B6692DC57B36771160CFA909A1E1B6A9E4A4 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5474 = { sizeof (U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D128_t86B7B65AEE45C88C60349903E5CAC9606602016D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5475 = { sizeof (U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D68_t01417E058C89CBFE48692EC7117BEC72107FAEA5 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5476 = { sizeof (U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D4_t5ED5745FD13269D462C2D8E11D552E593CE2BEC9 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5477 = { sizeof (U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D20_t3C6974B24B71DDE35BE0FDC7CF438293C9CD14B3 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5478 = { sizeof (U24ArrayTypeU3D60_tD3F93F1391B60ED8B4F85262374C3FBC6BB2F4CF)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D60_tD3F93F1391B60ED8B4F85262374C3FBC6BB2F4CF ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5479 = { sizeof (U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_tF140EF8176ADB359E38E7CC2EDD04F7543567723 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5480 = { sizeof (U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_tF4E301A88C1FC5AE3406635020A6E699649A8F33 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5481 = { sizeof (U24ArrayTypeU3D4096_tE76E581E9D7FCAFF3FDB2A1A2F45C247994B04A5)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D4096_tE76E581E9D7FCAFF3FDB2A1A2F45C247994B04A5 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5482 = { sizeof (U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D40_t30FEF54F62CBC9541955162200901918B05295E8 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5483 = { sizeof (U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D28_t610D95459FE7AE34493ABD63FB9BE516837C8D3F ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5484 = { sizeof (U24ArrayTypeU3D36_t9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D36_t9AAE278E88CDD2B4F2D30F0553C812A1781E8F5B ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5485 = { sizeof (U24ArrayTypeU3D44_t156A45D15629E5CD523F90BAE6DB67B5ACBE097F)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D44_t156A45D15629E5CD523F90BAE6DB67B5ACBE097F ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5486 = { sizeof (U24ArrayTypeU3D76_tA66D1F76A98126C3A8C881FB7A1C3D6647924CC3)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D76_tA66D1F76A98126C3A8C881FB7A1C3D6647924CC3 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5487 = { sizeof (U24ArrayTypeU3D6144_t0FA5B30A17373E7B9445630D8FD58FD781E5DA6E)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D6144_t0FA5B30A17373E7B9445630D8FD58FD781E5DA6E ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5488 = { sizeof (U24ArrayTypeU3D384_t2196F8B9AC0E7C9D619BA635541A7E6C3D3A6891)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D384_t2196F8B9AC0E7C9D619BA635541A7E6C3D3A6891 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5489 = { sizeof (U24ArrayTypeU3D124_tC53BBB2379F45A189B7240430D076326E331393F)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D124_tC53BBB2379F45A189B7240430D076326E331393F ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5490 = { sizeof (U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D120_tF3BCD6357BAE42661C2208DCCAC9E84DE70B972B ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5491 = { sizeof (U24ArrayTypeU3D1152_t26C03C6F9BF348D112C4FCF00BB0EC8807B477FF)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D1152_t26C03C6F9BF348D112C4FCF00BB0EC8807B477FF ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5492 = { sizeof (U24ArrayTypeU3D116_t3D3A23342468E840802FEE05074D0A8F384077CF)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D116_t3D3A23342468E840802FEE05074D0A8F384077CF ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5493 = { sizeof (U3CModuleU3E_tBE4A2C03D239132B99D8F2C5938A2EC7220D741C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5494 = { sizeof (DataContractAttribute_tAD58D5877BD04EADB56BB4AEDDE342C73F032FC5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5494[1] = 
{
	DataContractAttribute_tAD58D5877BD04EADB56BB4AEDDE342C73F032FC5::get_offset_of_isReference_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5495 = { sizeof (DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5495[4] = 
{
	DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525::get_offset_of_name_0(),
	DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525::get_offset_of_order_1(),
	DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525::get_offset_of_isRequired_2(),
	DataMemberAttribute_tC865433FEC93FFD46D6F3E4BB28F262C9EE40525::get_offset_of_emitDefaultValue_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5496 = { sizeof (EnumMemberAttribute_t115D80337B2C8222158FC46345EA100EEB63B32D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5496[1] = 
{
	EnumMemberAttribute_t115D80337B2C8222158FC46345EA100EEB63B32D::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5497 = { sizeof (KnownTypeAttribute_tFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5497[1] = 
{
	KnownTypeAttribute_tFCFB5B9A0AE4BBCBD655E2EC756FABAC3ADF487E::get_offset_of_type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5498 = { sizeof (U3CModuleU3E_tB308A2384DEB86F8845A4E61970976B8944B5DC4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5499 = { sizeof (U3CModuleU3E_t93AF16331CBD41D5C3147B00C1826A972C35DD7D), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
