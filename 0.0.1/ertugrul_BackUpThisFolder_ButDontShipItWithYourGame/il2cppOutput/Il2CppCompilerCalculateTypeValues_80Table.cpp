﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BattleBundleLoaderTask
struct BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2;
// BattleEcsInitializer
struct BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67;
// BattleLoaderTask
struct BattleLoaderTask_tEDC6FA66A41F28C5B262EE753494294B090BC0F3;
// BattleSettingsSO
struct BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE;
// ChaptersSO
struct ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1;
// CodeStage.AntiCheat.Examples.CodeHashExample
struct CodeHashExample_tFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1;
// CodeStage.AntiCheat.Examples.DetectorsExamples
struct DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C;
// CodeStage.AntiCheat.Examples.ObscuredPrefsExamples
struct ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063;
// CodeStage.AntiCheat.Examples.ObscuredTypesExamples
struct ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA;
// CodeStage.AntiCheat.Genuine.CodeHash.HashGeneratorResult
struct HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7;
// CodeStage.AntiCheat.ObscuredTypes.ObscuredString
struct ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301;
// Contexts
struct Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D;
// Entitas.ICollector`1<GameEntity>
struct ICollector_1_tDA9721194F1438993CA7EC5D39C69B898FBBB26A;
// Entitas.IGroup`1<GameEntity>
struct IGroup_1_tF4940889845236B5907C50AD6A40AD6CE69EC680;
// FeaturesSwitchConfig
struct FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3;
// GameContext
struct GameContext_t7DA531FCF38581666B3642E3E737E5B057494978;
// GameModeFeature
struct GameModeFeature_t26E5E2E6B53B52AFFBC416103BF675284F1F833C;
// GameSettingsSO
struct GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C;
// InputFeature
struct InputFeature_t5A0C995F504E269254656351086E21C0FAB312AC;
// InventorySystem
struct InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2;
// ItemsSO
struct ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A;
// LevelModel
struct LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C;
// LoginTask
struct LoginTask_t03C0EA4FE385D98BE13064ADE4B32C270EFE52D8;
// LoginTaskResponse
struct LoginTaskResponse_tB278D15FCA56552FD324AF0ECF1DDFE10F0D7DE9;
// RenderFeature
struct RenderFeature_tD8D02B6D1FC897C71185766227AB45DC1E8B7D12;
// SimulationFeature
struct SimulationFeature_tE452891FF1D5A4B359ED58C1EF1F7B2276A87EF2;
// SoundSO
struct SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73;
// SpellModel
struct SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235;
// SpellsSO
struct SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439;
// SpellsWarmupTask
struct SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.String>
struct Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C;
// System.Collections.Generic.Dictionary`2<System.String,AssetData>
struct Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1;
// System.Collections.Generic.Dictionary`2<System.String,MapItemType>
struct Dictionary_2_tD0D2E2BF5E138BBB26E44CB7F9E51861C526EBE3;
// System.Collections.Generic.Dictionary`2<System.String,Tayr.IBasicVO>
struct Dictionary_2_t8196CB40B909BB3D81CA49FBE34F8CE343A54C74;
// System.Collections.Generic.Dictionary`2<System.String,Tayr.TPrefabData>
struct Dictionary_2_t999FF5ACCFAB22CE63918E3F7443780DB84A95EC;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UI.Button>
struct Dictionary_2_t554924F40F85C9BB88122F0D57490C2D3012B71E;
// System.Collections.Generic.Dictionary`2<System.Type,Tayr.Variable>
struct Dictionary_2_t0858D6C06FC1F458F73010213615C9EB8A4F7DC7;
// System.Collections.Generic.Dictionary`2<System.Type,Zenject.DiContainer>
struct Dictionary_2_tEA3EFEB6ED5173AA6CE25B304791D523ED67AA3A;
// System.Collections.Generic.List`1<Entitas.ICleanupSystem>
struct List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC;
// System.Collections.Generic.List`1<Entitas.IExecuteSystem>
struct List_1_tE232269578E5E48549D25DD0C9823B612D968293;
// System.Collections.Generic.List`1<Entitas.IInitializeSystem>
struct List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C;
// System.Collections.Generic.List`1<Entitas.ITearDownSystem>
struct List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913;
// System.Collections.Generic.List`1<GameEntity>
struct List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29;
// System.Collections.Generic.List`1<Tayr.ITUpdatable>
struct List_1_t559E7DE3147BE4C3C954CF553B1BB9B3E6E06F1D;
// System.Func`1<System.Boolean>
struct Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// Tayr.AssetBundleLoaderTask
struct AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD;
// Tayr.GameSparksPlatform
struct GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348;
// Tayr.ILibrary
struct ILibrary_tCBD3DE1F66AD92DAB1112B8018EBA4A4BD23767C;
// Tayr.ITaskContainer
struct ITaskContainer_t2FD3049836F2CE1D1D36D26BE4E990C439E97B3E;
// Tayr.ITaskRunner
struct ITaskRunner_tB1CAE8EDCFC7F57D1F9FCE9C61BE7F3B8BB1389B;
// Tayr.OnTaskCompleted`1<LoginTaskResponse>
struct OnTaskCompleted_1_tF34D5FBC895DC47EDC6B18DA36691D423526A0D1;
// Tayr.OnTaskCompleted`1<System.Int32>
struct OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE;
// Tayr.OnTriggerEvent
struct OnTriggerEvent_t77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019;
// Tayr.ParallelTask
struct ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717;
// Tayr.TSoundSystem
struct TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511;
// Tayr.TSystemsManager
struct TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462;
// Tayr.Trigger
struct Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09;
// Tayr.Variable
struct Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD;
// Tayr.VariableChangedEvent
struct VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4;
// TowersSO
struct TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0;
// TrapsSO
struct TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2;
// UnitsSO
struct UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UserEvents
struct UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7;
// UserVO
struct UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;
// Zenject.DisposableManager
struct DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC;




#ifndef U3CMODULEU3E_T65223B046E79B9B52A6A555C2EFD1DE08ED208AD_H
#define U3CMODULEU3E_T65223B046E79B9B52A6A555C2EFD1DE08ED208AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t65223B046E79B9B52A6A555C2EFD1DE08ED208AD 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T65223B046E79B9B52A6A555C2EFD1DE08ED208AD_H
#ifndef U3CMODULEU3E_TFB778DC5508E752CBCDE1D45B96E5B700D73328E_H
#define U3CMODULEU3E_TFB778DC5508E752CBCDE1D45B96E5B700D73328E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tFB778DC5508E752CBCDE1D45B96E5B700D73328E 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TFB778DC5508E752CBCDE1D45B96E5B700D73328E_H
#ifndef U3CMODULEU3E_T85932CF96972B7D4C02F28C9B1523A5F0B6961A1_H
#define U3CMODULEU3E_T85932CF96972B7D4C02F28C9B1523A5F0B6961A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t85932CF96972B7D4C02F28C9B1523A5F0B6961A1 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T85932CF96972B7D4C02F28C9B1523A5F0B6961A1_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TF68BB53B7720992600AB558F953B0B8E44B8C152_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TF68BB53B7720992600AB558F953B0B8E44B8C152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tF68BB53B7720992600AB558F953B0B8E44B8C152  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tF68BB53B7720992600AB558F953B0B8E44B8C152_StaticFields
{
public:
	// System.Int32 <PrivateImplementationDetails>::C67DE1FA1C75585E4A3C2ED631654C95F818EA95
	int32_t ___C67DE1FA1C75585E4A3C2ED631654C95F818EA95_0;

public:
	inline static int32_t get_offset_of_C67DE1FA1C75585E4A3C2ED631654C95F818EA95_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tF68BB53B7720992600AB558F953B0B8E44B8C152_StaticFields, ___C67DE1FA1C75585E4A3C2ED631654C95F818EA95_0)); }
	inline int32_t get_C67DE1FA1C75585E4A3C2ED631654C95F818EA95_0() const { return ___C67DE1FA1C75585E4A3C2ED631654C95F818EA95_0; }
	inline int32_t* get_address_of_C67DE1FA1C75585E4A3C2ED631654C95F818EA95_0() { return &___C67DE1FA1C75585E4A3C2ED631654C95F818EA95_0; }
	inline void set_C67DE1FA1C75585E4A3C2ED631654C95F818EA95_0(int32_t value)
	{
		___C67DE1FA1C75585E4A3C2ED631654C95F818EA95_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TF68BB53B7720992600AB558F953B0B8E44B8C152_H
#ifndef U3CTASKU3ED__8_T4A983BB41C9B5B44D614E7998619892EB623F24E_H
#define U3CTASKU3ED__8_T4A983BB41C9B5B44D614E7998619892EB623F24E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleBundleLoaderTask_<Task>d__8
struct  U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E  : public RuntimeObject
{
public:
	// System.Int32 BattleBundleLoaderTask_<Task>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BattleBundleLoaderTask_<Task>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BattleBundleLoaderTask BattleBundleLoaderTask_<Task>d__8::<>4__this
	BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2 * ___U3CU3E4__this_2;
	// LevelModel BattleBundleLoaderTask_<Task>d__8::<model>5__2
	LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C * ___U3CmodelU3E5__2_3;
	// Tayr.ParallelTask BattleBundleLoaderTask_<Task>d__8::<parallelTask>5__3
	ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717 * ___U3CparallelTaskU3E5__3_4;
	// Tayr.AssetBundleLoaderTask BattleBundleLoaderTask_<Task>d__8::<chapterTask>5__4
	AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * ___U3CchapterTaskU3E5__4_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E, ___U3CU3E4__this_2)); }
	inline BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CmodelU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E, ___U3CmodelU3E5__2_3)); }
	inline LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C * get_U3CmodelU3E5__2_3() const { return ___U3CmodelU3E5__2_3; }
	inline LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C ** get_address_of_U3CmodelU3E5__2_3() { return &___U3CmodelU3E5__2_3; }
	inline void set_U3CmodelU3E5__2_3(LevelModel_t5549E807391907F7844D01A4FB4004330AF5E05C * value)
	{
		___U3CmodelU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmodelU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CparallelTaskU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E, ___U3CparallelTaskU3E5__3_4)); }
	inline ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717 * get_U3CparallelTaskU3E5__3_4() const { return ___U3CparallelTaskU3E5__3_4; }
	inline ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717 ** get_address_of_U3CparallelTaskU3E5__3_4() { return &___U3CparallelTaskU3E5__3_4; }
	inline void set_U3CparallelTaskU3E5__3_4(ParallelTask_tED959C990B1D6B0E41F2ED292B4AD6AE34939717 * value)
	{
		___U3CparallelTaskU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparallelTaskU3E5__3_4), value);
	}

	inline static int32_t get_offset_of_U3CchapterTaskU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E, ___U3CchapterTaskU3E5__4_5)); }
	inline AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * get_U3CchapterTaskU3E5__4_5() const { return ___U3CchapterTaskU3E5__4_5; }
	inline AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD ** get_address_of_U3CchapterTaskU3E5__4_5() { return &___U3CchapterTaskU3E5__4_5; }
	inline void set_U3CchapterTaskU3E5__4_5(AssetBundleLoaderTask_t285E2ABF6B7FE89A4B4F3DB3C14399F31B6CA2FD * value)
	{
		___U3CchapterTaskU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchapterTaskU3E5__4_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__8_T4A983BB41C9B5B44D614E7998619892EB623F24E_H
#ifndef BATTLEDURATIONCOMPONENT_TEFEEFB80D9135B611600DAA824B424811E2E5B86_H
#define BATTLEDURATIONCOMPONENT_TEFEEFB80D9135B611600DAA824B424811E2E5B86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleDurationComponent
struct  BattleDurationComponent_tEFEEFB80D9135B611600DAA824B424811E2E5B86  : public RuntimeObject
{
public:
	// System.Single BattleDurationComponent::Duration
	float ___Duration_0;

public:
	inline static int32_t get_offset_of_Duration_0() { return static_cast<int32_t>(offsetof(BattleDurationComponent_tEFEEFB80D9135B611600DAA824B424811E2E5B86, ___Duration_0)); }
	inline float get_Duration_0() const { return ___Duration_0; }
	inline float* get_address_of_Duration_0() { return &___Duration_0; }
	inline void set_Duration_0(float value)
	{
		___Duration_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEDURATIONCOMPONENT_TEFEEFB80D9135B611600DAA824B424811E2E5B86_H
#ifndef BATTLEEXTRADURATIONCOMPONENT_T44350E8AE350AE0C46860FD3ACBF906270849581_H
#define BATTLEEXTRADURATIONCOMPONENT_T44350E8AE350AE0C46860FD3ACBF906270849581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleExtraDurationComponent
struct  BattleExtraDurationComponent_t44350E8AE350AE0C46860FD3ACBF906270849581  : public RuntimeObject
{
public:
	// System.Single BattleExtraDurationComponent::Duration
	float ___Duration_0;

public:
	inline static int32_t get_offset_of_Duration_0() { return static_cast<int32_t>(offsetof(BattleExtraDurationComponent_t44350E8AE350AE0C46860FD3ACBF906270849581, ___Duration_0)); }
	inline float get_Duration_0() const { return ___Duration_0; }
	inline float* get_address_of_Duration_0() { return &___Duration_0; }
	inline void set_Duration_0(float value)
	{
		___Duration_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEEXTRADURATIONCOMPONENT_T44350E8AE350AE0C46860FD3ACBF906270849581_H
#ifndef U3CTASKU3ED__2_T8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564_H
#define U3CTASKU3ED__2_T8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleLoaderTask_<Task>d__2
struct  U3CTaskU3Ed__2_t8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564  : public RuntimeObject
{
public:
	// System.Int32 BattleLoaderTask_<Task>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BattleLoaderTask_<Task>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// BattleLoaderTask BattleLoaderTask_<Task>d__2::<>4__this
	BattleLoaderTask_tEDC6FA66A41F28C5B262EE753494294B090BC0F3 * ___U3CU3E4__this_2;
	// BattleBundleLoaderTask BattleLoaderTask_<Task>d__2::<battleBundleLoaderTask>5__2
	BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2 * ___U3CbattleBundleLoaderTaskU3E5__2_3;
	// SpellsWarmupTask BattleLoaderTask_<Task>d__2::<spellsWamupTask>5__3
	SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F * ___U3CspellsWamupTaskU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564, ___U3CU3E4__this_2)); }
	inline BattleLoaderTask_tEDC6FA66A41F28C5B262EE753494294B090BC0F3 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline BattleLoaderTask_tEDC6FA66A41F28C5B262EE753494294B090BC0F3 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(BattleLoaderTask_tEDC6FA66A41F28C5B262EE753494294B090BC0F3 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CbattleBundleLoaderTaskU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564, ___U3CbattleBundleLoaderTaskU3E5__2_3)); }
	inline BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2 * get_U3CbattleBundleLoaderTaskU3E5__2_3() const { return ___U3CbattleBundleLoaderTaskU3E5__2_3; }
	inline BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2 ** get_address_of_U3CbattleBundleLoaderTaskU3E5__2_3() { return &___U3CbattleBundleLoaderTaskU3E5__2_3; }
	inline void set_U3CbattleBundleLoaderTaskU3E5__2_3(BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2 * value)
	{
		___U3CbattleBundleLoaderTaskU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbattleBundleLoaderTaskU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CspellsWamupTaskU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564, ___U3CspellsWamupTaskU3E5__3_4)); }
	inline SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F * get_U3CspellsWamupTaskU3E5__3_4() const { return ___U3CspellsWamupTaskU3E5__3_4; }
	inline SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F ** get_address_of_U3CspellsWamupTaskU3E5__3_4() { return &___U3CspellsWamupTaskU3E5__3_4; }
	inline void set_U3CspellsWamupTaskU3E5__3_4(SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F * value)
	{
		___U3CspellsWamupTaskU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspellsWamupTaskU3E5__3_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__2_T8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564_H
#ifndef BATTLEREVIVECOMPONENT_T274D262B5A3AA78EBC2831B67D816ED2737C8CDA_H
#define BATTLEREVIVECOMPONENT_T274D262B5A3AA78EBC2831B67D816ED2737C8CDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleReviveComponent
struct  BattleReviveComponent_t274D262B5A3AA78EBC2831B67D816ED2737C8CDA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEREVIVECOMPONENT_T274D262B5A3AA78EBC2831B67D816ED2737C8CDA_H
#ifndef BATTLEUIINITSYSTEM_T9CE6165C73567C801A6B4B510CAE915849EC7E50_H
#define BATTLEUIINITSYSTEM_T9CE6165C73567C801A6B4B510CAE915849EC7E50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleUIInitSystem
struct  BattleUIInitSystem_t9CE6165C73567C801A6B4B510CAE915849EC7E50  : public RuntimeObject
{
public:
	// Tayr.ILibrary BattleUIInitSystem::_uiMain
	RuntimeObject* ____uiMain_0;
	// BattleSettingsSO BattleUIInitSystem::_battleSettingsSO
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * ____battleSettingsSO_1;
	// Zenject.DiContainer BattleUIInitSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_2;
	// GameContext BattleUIInitSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__uiMain_0() { return static_cast<int32_t>(offsetof(BattleUIInitSystem_t9CE6165C73567C801A6B4B510CAE915849EC7E50, ____uiMain_0)); }
	inline RuntimeObject* get__uiMain_0() const { return ____uiMain_0; }
	inline RuntimeObject** get_address_of__uiMain_0() { return &____uiMain_0; }
	inline void set__uiMain_0(RuntimeObject* value)
	{
		____uiMain_0 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_0), value);
	}

	inline static int32_t get_offset_of__battleSettingsSO_1() { return static_cast<int32_t>(offsetof(BattleUIInitSystem_t9CE6165C73567C801A6B4B510CAE915849EC7E50, ____battleSettingsSO_1)); }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * get__battleSettingsSO_1() const { return ____battleSettingsSO_1; }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE ** get_address_of__battleSettingsSO_1() { return &____battleSettingsSO_1; }
	inline void set__battleSettingsSO_1(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * value)
	{
		____battleSettingsSO_1 = value;
		Il2CppCodeGenWriteBarrier((&____battleSettingsSO_1), value);
	}

	inline static int32_t get_offset_of__diContainer_2() { return static_cast<int32_t>(offsetof(BattleUIInitSystem_t9CE6165C73567C801A6B4B510CAE915849EC7E50, ____diContainer_2)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_2() const { return ____diContainer_2; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_2() { return &____diContainer_2; }
	inline void set__diContainer_2(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_2 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_2), value);
	}

	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(BattleUIInitSystem_t9CE6165C73567C801A6B4B510CAE915849EC7E50, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEUIINITSYSTEM_T9CE6165C73567C801A6B4B510CAE915849EC7E50_H
#ifndef CARTCOMPONENT_TCAE70D72887274B93314E7A8037EDCB2812AC04B_H
#define CARTCOMPONENT_TCAE70D72887274B93314E7A8037EDCB2812AC04B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CartComponent
struct  CartComponent_tCAE70D72887274B93314E7A8037EDCB2812AC04B  : public RuntimeObject
{
public:
	// System.String CartComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(CartComponent_tCAE70D72887274B93314E7A8037EDCB2812AC04B, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARTCOMPONENT_TCAE70D72887274B93314E7A8037EDCB2812AC04B_H
#ifndef U3CU3EC_T1A1241E63E745B560F6EE1026B54D5F14155C03A_H
#define U3CU3EC_T1A1241E63E745B560F6EE1026B54D5F14155C03A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Examples.ExamplesGUI_<>c
struct  U3CU3Ec_t1A1241E63E745B560F6EE1026B54D5F14155C03A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1A1241E63E745B560F6EE1026B54D5F14155C03A_StaticFields
{
public:
	// CodeStage.AntiCheat.Examples.ExamplesGUI_<>c CodeStage.AntiCheat.Examples.ExamplesGUI_<>c::<>9
	U3CU3Ec_t1A1241E63E745B560F6EE1026B54D5F14155C03A * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.Boolean> CodeStage.AntiCheat.Examples.ExamplesGUI_<>c::<>9__18_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__18_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1A1241E63E745B560F6EE1026B54D5F14155C03A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1A1241E63E745B560F6EE1026B54D5F14155C03A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1A1241E63E745B560F6EE1026B54D5F14155C03A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1A1241E63E745B560F6EE1026B54D5F14155C03A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1A1241E63E745B560F6EE1026B54D5F14155C03A_StaticFields, ___U3CU3E9__18_0_1)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__18_0_1() const { return ___U3CU3E9__18_0_1; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__18_0_1() { return &___U3CU3E9__18_0_1; }
	inline void set_U3CU3E9__18_0_1(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__18_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1A1241E63E745B560F6EE1026B54D5F14155C03A_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_T69526E59360083F1F61FA93BFB53A626667B6EF2_H
#define U3CU3EC__DISPLAYCLASS18_0_T69526E59360083F1F61FA93BFB53A626667B6EF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Examples.ExamplesGUI_<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t69526E59360083F1F61FA93BFB53A626667B6EF2  : public RuntimeObject
{
public:
	// System.String CodeStage.AntiCheat.Examples.ExamplesGUI_<>c__DisplayClass18_0::types
	String_t* ___types_0;

public:
	inline static int32_t get_offset_of_types_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t69526E59360083F1F61FA93BFB53A626667B6EF2, ___types_0)); }
	inline String_t* get_types_0() const { return ___types_0; }
	inline String_t** get_address_of_types_0() { return &___types_0; }
	inline void set_types_0(String_t* value)
	{
		___types_0 = value;
		Il2CppCodeGenWriteBarrier((&___types_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_T69526E59360083F1F61FA93BFB53A626667B6EF2_H
#ifndef DEBUGUTILS_T10B039A2267A5DD52302EB3328C08BA1961DBAA0_H
#define DEBUGUTILS_T10B039A2267A5DD52302EB3328C08BA1961DBAA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugUtils
struct  DebugUtils_t10B039A2267A5DD52302EB3328C08BA1961DBAA0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGUTILS_T10B039A2267A5DD52302EB3328C08BA1961DBAA0_H
#ifndef REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#define REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ReactiveSystem`1<GameEntity>
struct  ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9  : public RuntimeObject
{
public:
	// Entitas.ICollector`1<TEntity> Entitas.ReactiveSystem`1::_collector
	RuntimeObject* ____collector_0;
	// System.Collections.Generic.List`1<TEntity> Entitas.ReactiveSystem`1::_buffer
	List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * ____buffer_1;
	// System.String Entitas.ReactiveSystem`1::_toStringCache
	String_t* ____toStringCache_2;

public:
	inline static int32_t get_offset_of__collector_0() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____collector_0)); }
	inline RuntimeObject* get__collector_0() const { return ____collector_0; }
	inline RuntimeObject** get_address_of__collector_0() { return &____collector_0; }
	inline void set__collector_0(RuntimeObject* value)
	{
		____collector_0 = value;
		Il2CppCodeGenWriteBarrier((&____collector_0), value);
	}

	inline static int32_t get_offset_of__buffer_1() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____buffer_1)); }
	inline List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * get__buffer_1() const { return ____buffer_1; }
	inline List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 ** get_address_of__buffer_1() { return &____buffer_1; }
	inline void set__buffer_1(List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * value)
	{
		____buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_1), value);
	}

	inline static int32_t get_offset_of__toStringCache_2() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____toStringCache_2)); }
	inline String_t* get__toStringCache_2() const { return ____toStringCache_2; }
	inline String_t** get_address_of__toStringCache_2() { return &____toStringCache_2; }
	inline void set__toStringCache_2(String_t* value)
	{
		____toStringCache_2 = value;
		Il2CppCodeGenWriteBarrier((&____toStringCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#ifndef SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#define SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.Systems
struct  Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Entitas.IInitializeSystem> Entitas.Systems::_initializeSystems
	List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * ____initializeSystems_0;
	// System.Collections.Generic.List`1<Entitas.IExecuteSystem> Entitas.Systems::_executeSystems
	List_1_tE232269578E5E48549D25DD0C9823B612D968293 * ____executeSystems_1;
	// System.Collections.Generic.List`1<Entitas.ICleanupSystem> Entitas.Systems::_cleanupSystems
	List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * ____cleanupSystems_2;
	// System.Collections.Generic.List`1<Entitas.ITearDownSystem> Entitas.Systems::_tearDownSystems
	List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * ____tearDownSystems_3;

public:
	inline static int32_t get_offset_of__initializeSystems_0() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____initializeSystems_0)); }
	inline List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * get__initializeSystems_0() const { return ____initializeSystems_0; }
	inline List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C ** get_address_of__initializeSystems_0() { return &____initializeSystems_0; }
	inline void set__initializeSystems_0(List_1_t1E70CB97F04FCE20A9F01BCEBF8EB1C867FC348C * value)
	{
		____initializeSystems_0 = value;
		Il2CppCodeGenWriteBarrier((&____initializeSystems_0), value);
	}

	inline static int32_t get_offset_of__executeSystems_1() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____executeSystems_1)); }
	inline List_1_tE232269578E5E48549D25DD0C9823B612D968293 * get__executeSystems_1() const { return ____executeSystems_1; }
	inline List_1_tE232269578E5E48549D25DD0C9823B612D968293 ** get_address_of__executeSystems_1() { return &____executeSystems_1; }
	inline void set__executeSystems_1(List_1_tE232269578E5E48549D25DD0C9823B612D968293 * value)
	{
		____executeSystems_1 = value;
		Il2CppCodeGenWriteBarrier((&____executeSystems_1), value);
	}

	inline static int32_t get_offset_of__cleanupSystems_2() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____cleanupSystems_2)); }
	inline List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * get__cleanupSystems_2() const { return ____cleanupSystems_2; }
	inline List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC ** get_address_of__cleanupSystems_2() { return &____cleanupSystems_2; }
	inline void set__cleanupSystems_2(List_1_t3E0F132FC1070967FBCD19B4BDF44B4F85CA03AC * value)
	{
		____cleanupSystems_2 = value;
		Il2CppCodeGenWriteBarrier((&____cleanupSystems_2), value);
	}

	inline static int32_t get_offset_of__tearDownSystems_3() { return static_cast<int32_t>(offsetof(Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9, ____tearDownSystems_3)); }
	inline List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * get__tearDownSystems_3() const { return ____tearDownSystems_3; }
	inline List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 ** get_address_of__tearDownSystems_3() { return &____tearDownSystems_3; }
	inline void set__tearDownSystems_3(List_1_t78F50ABA4688AD6A1255F66AB89EE8BAFD069913 * value)
	{
		____tearDownSystems_3 = value;
		Il2CppCodeGenWriteBarrier((&____tearDownSystems_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMS_T4C86DA84B6D6F50FF23B497DD599ED594274DEA9_H
#ifndef GOALCOMPONENT_T9EE6F7FCF9EDEDE9BC7A631CF15CF8ABA51BCBA2_H
#define GOALCOMPONENT_T9EE6F7FCF9EDEDE9BC7A631CF15CF8ABA51BCBA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GoalComponent
struct  GoalComponent_t9EE6F7FCF9EDEDE9BC7A631CF15CF8ABA51BCBA2  : public RuntimeObject
{
public:
	// System.String GoalComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GoalComponent_t9EE6F7FCF9EDEDE9BC7A631CF15CF8ABA51BCBA2, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOALCOMPONENT_T9EE6F7FCF9EDEDE9BC7A631CF15CF8ABA51BCBA2_H
#ifndef HEROCOMPONENT_T0CBCC47B37CE03F28FEF130F385223E3F9E7B8B7_H
#define HEROCOMPONENT_T0CBCC47B37CE03F28FEF130F385223E3F9E7B8B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroComponent
struct  HeroComponent_t0CBCC47B37CE03F28FEF130F385223E3F9E7B8B7  : public RuntimeObject
{
public:
	// System.String HeroComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(HeroComponent_t0CBCC47B37CE03F28FEF130F385223E3F9E7B8B7, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROCOMPONENT_T0CBCC47B37CE03F28FEF130F385223E3F9E7B8B7_H
#ifndef JOYSTICKCOMPONENT_T83263618E7F12C8C23443EB6AAAB252301FEB9BD_H
#define JOYSTICKCOMPONENT_T83263618E7F12C8C23443EB6AAAB252301FEB9BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoystickComponent
struct  JoystickComponent_t83263618E7F12C8C23443EB6AAAB252301FEB9BD  : public RuntimeObject
{
public:
	// System.Single JoystickComponent::Vertical
	float ___Vertical_0;
	// System.Single JoystickComponent::Horizontal
	float ___Horizontal_1;

public:
	inline static int32_t get_offset_of_Vertical_0() { return static_cast<int32_t>(offsetof(JoystickComponent_t83263618E7F12C8C23443EB6AAAB252301FEB9BD, ___Vertical_0)); }
	inline float get_Vertical_0() const { return ___Vertical_0; }
	inline float* get_address_of_Vertical_0() { return &___Vertical_0; }
	inline void set_Vertical_0(float value)
	{
		___Vertical_0 = value;
	}

	inline static int32_t get_offset_of_Horizontal_1() { return static_cast<int32_t>(offsetof(JoystickComponent_t83263618E7F12C8C23443EB6AAAB252301FEB9BD, ___Horizontal_1)); }
	inline float get_Horizontal_1() const { return ___Horizontal_1; }
	inline float* get_address_of_Horizontal_1() { return &___Horizontal_1; }
	inline void set_Horizontal_1(float value)
	{
		___Horizontal_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKCOMPONENT_T83263618E7F12C8C23443EB6AAAB252301FEB9BD_H
#ifndef JOYSTICKINITSYSTEM_T5D264998AF507C48CA3D2EF77E23BE00D6F42335_H
#define JOYSTICKINITSYSTEM_T5D264998AF507C48CA3D2EF77E23BE00D6F42335_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoystickInitSystem
struct  JoystickInitSystem_t5D264998AF507C48CA3D2EF77E23BE00D6F42335  : public RuntimeObject
{
public:
	// Zenject.DiContainer JoystickInitSystem::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;
	// GameContext JoystickInitSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(JoystickInitSystem_t5D264998AF507C48CA3D2EF77E23BE00D6F42335, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__context_1() { return static_cast<int32_t>(offsetof(JoystickInitSystem_t5D264998AF507C48CA3D2EF77E23BE00D6F42335, ____context_1)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_1() const { return ____context_1; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_1() { return &____context_1; }
	inline void set__context_1(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_1 = value;
		Il2CppCodeGenWriteBarrier((&____context_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKINITSYSTEM_T5D264998AF507C48CA3D2EF77E23BE00D6F42335_H
#ifndef U3CU3EC_TBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293_H
#define U3CU3EC_TBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginTask_<>c
struct  U3CU3Ec_tBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293_StaticFields
{
public:
	// LoginTask_<>c LoginTask_<>c::<>9
	U3CU3Ec_tBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293 * ___U3CU3E9_0;
	// System.Func`1<System.Boolean> LoginTask_<>c::<>9__2_0
	Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * ___U3CU3E9__2_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293_StaticFields, ___U3CU3E9__2_0_1)); }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(Func_1_t4ABD6DAD480574F152452DD6B9C9A55F4F6655F1 * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293_H
#ifndef U3CTASKU3ED__2_T274DDE4CC5BA37737A768B205D96D59CB8A1BDAA_H
#define U3CTASKU3ED__2_T274DDE4CC5BA37737A768B205D96D59CB8A1BDAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginTask_<Task>d__2
struct  U3CTaskU3Ed__2_t274DDE4CC5BA37737A768B205D96D59CB8A1BDAA  : public RuntimeObject
{
public:
	// System.Int32 LoginTask_<Task>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object LoginTask_<Task>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// LoginTask LoginTask_<Task>d__2::<>4__this
	LoginTask_t03C0EA4FE385D98BE13064ADE4B32C270EFE52D8 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t274DDE4CC5BA37737A768B205D96D59CB8A1BDAA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t274DDE4CC5BA37737A768B205D96D59CB8A1BDAA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__2_t274DDE4CC5BA37737A768B205D96D59CB8A1BDAA, ___U3CU3E4__this_2)); }
	inline LoginTask_t03C0EA4FE385D98BE13064ADE4B32C270EFE52D8 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LoginTask_t03C0EA4FE385D98BE13064ADE4B32C270EFE52D8 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LoginTask_t03C0EA4FE385D98BE13064ADE4B32C270EFE52D8 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__2_T274DDE4CC5BA37737A768B205D96D59CB8A1BDAA_H
#ifndef LOGINTASKRESPONSE_TB278D15FCA56552FD324AF0ECF1DDFE10F0D7DE9_H
#define LOGINTASKRESPONSE_TB278D15FCA56552FD324AF0ECF1DDFE10F0D7DE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginTaskResponse
struct  LoginTaskResponse_tB278D15FCA56552FD324AF0ECF1DDFE10F0D7DE9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINTASKRESPONSE_TB278D15FCA56552FD324AF0ECF1DDFE10F0D7DE9_H
#ifndef LOGINUTILS_TC8DA4A680696A03AEDD404C5D0863FF0A1719CF7_H
#define LOGINUTILS_TC8DA4A680696A03AEDD404C5D0863FF0A1719CF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginUtils
struct  LoginUtils_tC8DA4A680696A03AEDD404C5D0863FF0A1719CF7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINUTILS_TC8DA4A680696A03AEDD404C5D0863FF0A1719CF7_H
#ifndef MAPITEMCOMPONENT_TB500381AF954BC6E08A9D449F75126054766BE68_H
#define MAPITEMCOMPONENT_TB500381AF954BC6E08A9D449F75126054766BE68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapItemComponent
struct  MapItemComponent_tB500381AF954BC6E08A9D449F75126054766BE68  : public RuntimeObject
{
public:
	// System.String MapItemComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(MapItemComponent_tB500381AF954BC6E08A9D449F75126054766BE68, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPITEMCOMPONENT_TB500381AF954BC6E08A9D449F75126054766BE68_H
#ifndef MAPLOADERSYSTEM_T67B940E2F5BC2F0CD0B2EAFCF9B43F59680BD641_H
#define MAPLOADERSYSTEM_T67B940E2F5BC2F0CD0B2EAFCF9B43F59680BD641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapLoaderSystem
struct  MapLoaderSystem_t67B940E2F5BC2F0CD0B2EAFCF9B43F59680BD641  : public RuntimeObject
{
public:
	// Zenject.DiContainer MapLoaderSystem::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_0;
	// UserVO MapLoaderSystem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_1;
	// ChaptersSO MapLoaderSystem::_chaptersSO
	ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * ____chaptersSO_2;
	// GameContext MapLoaderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(MapLoaderSystem_t67B940E2F5BC2F0CD0B2EAFCF9B43F59680BD641, ____container_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_0() const { return ____container_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__userVO_1() { return static_cast<int32_t>(offsetof(MapLoaderSystem_t67B940E2F5BC2F0CD0B2EAFCF9B43F59680BD641, ____userVO_1)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_1() const { return ____userVO_1; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_1() { return &____userVO_1; }
	inline void set__userVO_1(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_1 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_1), value);
	}

	inline static int32_t get_offset_of__chaptersSO_2() { return static_cast<int32_t>(offsetof(MapLoaderSystem_t67B940E2F5BC2F0CD0B2EAFCF9B43F59680BD641, ____chaptersSO_2)); }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * get__chaptersSO_2() const { return ____chaptersSO_2; }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 ** get_address_of__chaptersSO_2() { return &____chaptersSO_2; }
	inline void set__chaptersSO_2(ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * value)
	{
		____chaptersSO_2 = value;
		Il2CppCodeGenWriteBarrier((&____chaptersSO_2), value);
	}

	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(MapLoaderSystem_t67B940E2F5BC2F0CD0B2EAFCF9B43F59680BD641, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPLOADERSYSTEM_T67B940E2F5BC2F0CD0B2EAFCF9B43F59680BD641_H
#ifndef MAPUTILS_T963968652938F061C2D0E7DB28EE306AF7E4C6C1_H
#define MAPUTILS_T963968652938F061C2D0E7DB28EE306AF7E4C6C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapUtils
struct  MapUtils_t963968652938F061C2D0E7DB28EE306AF7E4C6C1  : public RuntimeObject
{
public:

public:
};

struct MapUtils_t963968652938F061C2D0E7DB28EE306AF7E4C6C1_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,MapItemType> MapUtils::_dictionary
	Dictionary_2_tD0D2E2BF5E138BBB26E44CB7F9E51861C526EBE3 * ____dictionary_0;

public:
	inline static int32_t get_offset_of__dictionary_0() { return static_cast<int32_t>(offsetof(MapUtils_t963968652938F061C2D0E7DB28EE306AF7E4C6C1_StaticFields, ____dictionary_0)); }
	inline Dictionary_2_tD0D2E2BF5E138BBB26E44CB7F9E51861C526EBE3 * get__dictionary_0() const { return ____dictionary_0; }
	inline Dictionary_2_tD0D2E2BF5E138BBB26E44CB7F9E51861C526EBE3 ** get_address_of__dictionary_0() { return &____dictionary_0; }
	inline void set__dictionary_0(Dictionary_2_tD0D2E2BF5E138BBB26E44CB7F9E51861C526EBE3 * value)
	{
		____dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&____dictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPUTILS_T963968652938F061C2D0E7DB28EE306AF7E4C6C1_H
#ifndef OBJECTIVECOMPLETECOMPONENT_T1AA886C7D93C680C37D82B7A0514801EA0F04852_H
#define OBJECTIVECOMPLETECOMPONENT_T1AA886C7D93C680C37D82B7A0514801EA0F04852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectiveCompleteComponent
struct  ObjectiveCompleteComponent_t1AA886C7D93C680C37D82B7A0514801EA0F04852  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVECOMPLETECOMPONENT_T1AA886C7D93C680C37D82B7A0514801EA0F04852_H
#ifndef OBJECTIVEDECREMENTSYSTEM_T002E6B98C540BCE77F2F2B3E01CCE5BD11B3CC99_H
#define OBJECTIVEDECREMENTSYSTEM_T002E6B98C540BCE77F2F2B3E01CCE5BD11B3CC99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectiveDecrementSystem
struct  ObjectiveDecrementSystem_t002E6B98C540BCE77F2F2B3E01CCE5BD11B3CC99  : public RuntimeObject
{
public:
	// GameSettingsSO ObjectiveDecrementSystem::_gameSettingsSO
	GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * ____gameSettingsSO_0;
	// GameContext ObjectiveDecrementSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_1;
	// Entitas.IGroup`1<GameEntity> ObjectiveDecrementSystem::_entities
	RuntimeObject* ____entities_2;

public:
	inline static int32_t get_offset_of__gameSettingsSO_0() { return static_cast<int32_t>(offsetof(ObjectiveDecrementSystem_t002E6B98C540BCE77F2F2B3E01CCE5BD11B3CC99, ____gameSettingsSO_0)); }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * get__gameSettingsSO_0() const { return ____gameSettingsSO_0; }
	inline GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C ** get_address_of__gameSettingsSO_0() { return &____gameSettingsSO_0; }
	inline void set__gameSettingsSO_0(GameSettingsSO_tC1B94CA47B375640E013220718E7800109B9058C * value)
	{
		____gameSettingsSO_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameSettingsSO_0), value);
	}

	inline static int32_t get_offset_of__context_1() { return static_cast<int32_t>(offsetof(ObjectiveDecrementSystem_t002E6B98C540BCE77F2F2B3E01CCE5BD11B3CC99, ____context_1)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_1() const { return ____context_1; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_1() { return &____context_1; }
	inline void set__context_1(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_1 = value;
		Il2CppCodeGenWriteBarrier((&____context_1), value);
	}

	inline static int32_t get_offset_of__entities_2() { return static_cast<int32_t>(offsetof(ObjectiveDecrementSystem_t002E6B98C540BCE77F2F2B3E01CCE5BD11B3CC99, ____entities_2)); }
	inline RuntimeObject* get__entities_2() const { return ____entities_2; }
	inline RuntimeObject** get_address_of__entities_2() { return &____entities_2; }
	inline void set__entities_2(RuntimeObject* value)
	{
		____entities_2 = value;
		Il2CppCodeGenWriteBarrier((&____entities_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVEDECREMENTSYSTEM_T002E6B98C540BCE77F2F2B3E01CCE5BD11B3CC99_H
#ifndef OBJECTIVESTEPCOMPONENT_TA0B86DE1525A7DEE85B2CFC7BC2A5FCAB783676F_H
#define OBJECTIVESTEPCOMPONENT_TA0B86DE1525A7DEE85B2CFC7BC2A5FCAB783676F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectiveStepComponent
struct  ObjectiveStepComponent_tA0B86DE1525A7DEE85B2CFC7BC2A5FCAB783676F  : public RuntimeObject
{
public:
	// System.String ObjectiveStepComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(ObjectiveStepComponent_tA0B86DE1525A7DEE85B2CFC7BC2A5FCAB783676F, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVESTEPCOMPONENT_TA0B86DE1525A7DEE85B2CFC7BC2A5FCAB783676F_H
#ifndef OBJECTIVESCOMPONENT_T62171C52B3CB11A32E0762719903CA1B7DA0E745_H
#define OBJECTIVESCOMPONENT_T62171C52B3CB11A32E0762719903CA1B7DA0E745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectivesComponent
struct  ObjectivesComponent_t62171C52B3CB11A32E0762719903CA1B7DA0E745  : public RuntimeObject
{
public:
	// System.String ObjectivesComponent::TypeId
	String_t* ___TypeId_0;

public:
	inline static int32_t get_offset_of_TypeId_0() { return static_cast<int32_t>(offsetof(ObjectivesComponent_t62171C52B3CB11A32E0762719903CA1B7DA0E745, ___TypeId_0)); }
	inline String_t* get_TypeId_0() const { return ___TypeId_0; }
	inline String_t** get_address_of_TypeId_0() { return &___TypeId_0; }
	inline void set_TypeId_0(String_t* value)
	{
		___TypeId_0 = value;
		Il2CppCodeGenWriteBarrier((&___TypeId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVESCOMPONENT_T62171C52B3CB11A32E0762719903CA1B7DA0E745_H
#ifndef OBJECTIVESINITIALIZERSYSTEM_T138508A1854EDA2457D98CC6AE0E6ABAC3B702B6_H
#define OBJECTIVESINITIALIZERSYSTEM_T138508A1854EDA2457D98CC6AE0E6ABAC3B702B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectivesInitializerSystem
struct  ObjectivesInitializerSystem_t138508A1854EDA2457D98CC6AE0E6ABAC3B702B6  : public RuntimeObject
{
public:
	// Zenject.DiContainer ObjectivesInitializerSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_0;
	// UserVO ObjectivesInitializerSystem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_1;
	// ChaptersSO ObjectivesInitializerSystem::_chaptersSO
	ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * ____chaptersSO_2;
	// GameContext ObjectivesInitializerSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__diContainer_0() { return static_cast<int32_t>(offsetof(ObjectivesInitializerSystem_t138508A1854EDA2457D98CC6AE0E6ABAC3B702B6, ____diContainer_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_0() const { return ____diContainer_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_0() { return &____diContainer_0; }
	inline void set__diContainer_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_0), value);
	}

	inline static int32_t get_offset_of__userVO_1() { return static_cast<int32_t>(offsetof(ObjectivesInitializerSystem_t138508A1854EDA2457D98CC6AE0E6ABAC3B702B6, ____userVO_1)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_1() const { return ____userVO_1; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_1() { return &____userVO_1; }
	inline void set__userVO_1(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_1 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_1), value);
	}

	inline static int32_t get_offset_of__chaptersSO_2() { return static_cast<int32_t>(offsetof(ObjectivesInitializerSystem_t138508A1854EDA2457D98CC6AE0E6ABAC3B702B6, ____chaptersSO_2)); }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * get__chaptersSO_2() const { return ____chaptersSO_2; }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 ** get_address_of__chaptersSO_2() { return &____chaptersSO_2; }
	inline void set__chaptersSO_2(ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * value)
	{
		____chaptersSO_2 = value;
		Il2CppCodeGenWriteBarrier((&____chaptersSO_2), value);
	}

	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(ObjectivesInitializerSystem_t138508A1854EDA2457D98CC6AE0E6ABAC3B702B6, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVESINITIALIZERSYSTEM_T138508A1854EDA2457D98CC6AE0E6ABAC3B702B6_H
#ifndef U3CTASKU3ED__8_TE9E604CAF2979CFC7AA84BCEAD5E3E8AA2985AB8_H
#define U3CTASKU3ED__8_TE9E604CAF2979CFC7AA84BCEAD5E3E8AA2985AB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellsWarmupTask_<Task>d__8
struct  U3CTaskU3Ed__8_tE9E604CAF2979CFC7AA84BCEAD5E3E8AA2985AB8  : public RuntimeObject
{
public:
	// System.Int32 SpellsWarmupTask_<Task>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SpellsWarmupTask_<Task>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SpellsWarmupTask SpellsWarmupTask_<Task>d__8::<>4__this
	SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__8_tE9E604CAF2979CFC7AA84BCEAD5E3E8AA2985AB8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__8_tE9E604CAF2979CFC7AA84BCEAD5E3E8AA2985AB8, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTaskU3Ed__8_tE9E604CAF2979CFC7AA84BCEAD5E3E8AA2985AB8, ___U3CU3E4__this_2)); }
	inline SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTASKU3ED__8_TE9E604CAF2979CFC7AA84BCEAD5E3E8AA2985AB8_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#define BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTSystem
struct  BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8  : public RuntimeObject
{
public:
	// Zenject.DisposableManager Tayr.BasicTSystem::_disposableManager
	DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * ____disposableManager_0;
	// Tayr.TSystemsManager Tayr.BasicTSystem::_systemsManager
	TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * ____systemsManager_1;
	// System.Boolean Tayr.BasicTSystem::<IsInitialized>k__BackingField
	bool ___U3CIsInitializedU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of__disposableManager_0() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ____disposableManager_0)); }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * get__disposableManager_0() const { return ____disposableManager_0; }
	inline DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC ** get_address_of__disposableManager_0() { return &____disposableManager_0; }
	inline void set__disposableManager_0(DisposableManager_tCB394E2EA1FE174EB07A1082C5F1760FFE6F9ADC * value)
	{
		____disposableManager_0 = value;
		Il2CppCodeGenWriteBarrier((&____disposableManager_0), value);
	}

	inline static int32_t get_offset_of__systemsManager_1() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ____systemsManager_1)); }
	inline TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * get__systemsManager_1() const { return ____systemsManager_1; }
	inline TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 ** get_address_of__systemsManager_1() { return &____systemsManager_1; }
	inline void set__systemsManager_1(TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462 * value)
	{
		____systemsManager_1 = value;
		Il2CppCodeGenWriteBarrier((&____systemsManager_1), value);
	}

	inline static int32_t get_offset_of_U3CIsInitializedU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8, ___U3CIsInitializedU3Ek__BackingField_2)); }
	inline bool get_U3CIsInitializedU3Ek__BackingField_2() const { return ___U3CIsInitializedU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsInitializedU3Ek__BackingField_2() { return &___U3CIsInitializedU3Ek__BackingField_2; }
	inline void set_U3CIsInitializedU3Ek__BackingField_2(bool value)
	{
		___U3CIsInitializedU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTSYSTEM_TD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8_H
#ifndef BASICTASK_1_T53F3CF30DDAF2EE5A2B123852828B30534469DE1_H
#define BASICTASK_1_T53F3CF30DDAF2EE5A2B123852828B30534469DE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTask`1<LoginTaskResponse>
struct  BasicTask_1_t53F3CF30DDAF2EE5A2B123852828B30534469DE1  : public RuntimeObject
{
public:
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEvent
	OnTaskCompleted_1_tF34D5FBC895DC47EDC6B18DA36691D423526A0D1 * ____onTaskCompletedEvent_0;
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEventInternal
	OnTaskCompleted_1_tF34D5FBC895DC47EDC6B18DA36691D423526A0D1 * ____onTaskCompletedEventInternal_1;
	// T Tayr.BasicTask`1::Result
	LoginTaskResponse_tB278D15FCA56552FD324AF0ECF1DDFE10F0D7DE9 * ___Result_2;
	// System.Boolean Tayr.BasicTask`1::_isDone
	bool ____isDone_3;
	// Tayr.ITaskRunner Tayr.BasicTask`1::_taskRunner
	RuntimeObject* ____taskRunner_4;
	// Tayr.ITaskContainer Tayr.BasicTask`1::_taskContainer
	RuntimeObject* ____taskContainer_5;
	// System.Int32 Tayr.BasicTask`1::TaskContainerIndex
	int32_t ___TaskContainerIndex_6;

public:
	inline static int32_t get_offset_of__onTaskCompletedEvent_0() { return static_cast<int32_t>(offsetof(BasicTask_1_t53F3CF30DDAF2EE5A2B123852828B30534469DE1, ____onTaskCompletedEvent_0)); }
	inline OnTaskCompleted_1_tF34D5FBC895DC47EDC6B18DA36691D423526A0D1 * get__onTaskCompletedEvent_0() const { return ____onTaskCompletedEvent_0; }
	inline OnTaskCompleted_1_tF34D5FBC895DC47EDC6B18DA36691D423526A0D1 ** get_address_of__onTaskCompletedEvent_0() { return &____onTaskCompletedEvent_0; }
	inline void set__onTaskCompletedEvent_0(OnTaskCompleted_1_tF34D5FBC895DC47EDC6B18DA36691D423526A0D1 * value)
	{
		____onTaskCompletedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEvent_0), value);
	}

	inline static int32_t get_offset_of__onTaskCompletedEventInternal_1() { return static_cast<int32_t>(offsetof(BasicTask_1_t53F3CF30DDAF2EE5A2B123852828B30534469DE1, ____onTaskCompletedEventInternal_1)); }
	inline OnTaskCompleted_1_tF34D5FBC895DC47EDC6B18DA36691D423526A0D1 * get__onTaskCompletedEventInternal_1() const { return ____onTaskCompletedEventInternal_1; }
	inline OnTaskCompleted_1_tF34D5FBC895DC47EDC6B18DA36691D423526A0D1 ** get_address_of__onTaskCompletedEventInternal_1() { return &____onTaskCompletedEventInternal_1; }
	inline void set__onTaskCompletedEventInternal_1(OnTaskCompleted_1_tF34D5FBC895DC47EDC6B18DA36691D423526A0D1 * value)
	{
		____onTaskCompletedEventInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEventInternal_1), value);
	}

	inline static int32_t get_offset_of_Result_2() { return static_cast<int32_t>(offsetof(BasicTask_1_t53F3CF30DDAF2EE5A2B123852828B30534469DE1, ___Result_2)); }
	inline LoginTaskResponse_tB278D15FCA56552FD324AF0ECF1DDFE10F0D7DE9 * get_Result_2() const { return ___Result_2; }
	inline LoginTaskResponse_tB278D15FCA56552FD324AF0ECF1DDFE10F0D7DE9 ** get_address_of_Result_2() { return &___Result_2; }
	inline void set_Result_2(LoginTaskResponse_tB278D15FCA56552FD324AF0ECF1DDFE10F0D7DE9 * value)
	{
		___Result_2 = value;
		Il2CppCodeGenWriteBarrier((&___Result_2), value);
	}

	inline static int32_t get_offset_of__isDone_3() { return static_cast<int32_t>(offsetof(BasicTask_1_t53F3CF30DDAF2EE5A2B123852828B30534469DE1, ____isDone_3)); }
	inline bool get__isDone_3() const { return ____isDone_3; }
	inline bool* get_address_of__isDone_3() { return &____isDone_3; }
	inline void set__isDone_3(bool value)
	{
		____isDone_3 = value;
	}

	inline static int32_t get_offset_of__taskRunner_4() { return static_cast<int32_t>(offsetof(BasicTask_1_t53F3CF30DDAF2EE5A2B123852828B30534469DE1, ____taskRunner_4)); }
	inline RuntimeObject* get__taskRunner_4() const { return ____taskRunner_4; }
	inline RuntimeObject** get_address_of__taskRunner_4() { return &____taskRunner_4; }
	inline void set__taskRunner_4(RuntimeObject* value)
	{
		____taskRunner_4 = value;
		Il2CppCodeGenWriteBarrier((&____taskRunner_4), value);
	}

	inline static int32_t get_offset_of__taskContainer_5() { return static_cast<int32_t>(offsetof(BasicTask_1_t53F3CF30DDAF2EE5A2B123852828B30534469DE1, ____taskContainer_5)); }
	inline RuntimeObject* get__taskContainer_5() const { return ____taskContainer_5; }
	inline RuntimeObject** get_address_of__taskContainer_5() { return &____taskContainer_5; }
	inline void set__taskContainer_5(RuntimeObject* value)
	{
		____taskContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____taskContainer_5), value);
	}

	inline static int32_t get_offset_of_TaskContainerIndex_6() { return static_cast<int32_t>(offsetof(BasicTask_1_t53F3CF30DDAF2EE5A2B123852828B30534469DE1, ___TaskContainerIndex_6)); }
	inline int32_t get_TaskContainerIndex_6() const { return ___TaskContainerIndex_6; }
	inline int32_t* get_address_of_TaskContainerIndex_6() { return &___TaskContainerIndex_6; }
	inline void set_TaskContainerIndex_6(int32_t value)
	{
		___TaskContainerIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTASK_1_T53F3CF30DDAF2EE5A2B123852828B30534469DE1_H
#ifndef BASICTASK_1_TA96B338B10A9FF438D49FEEF0A97378798D99F57_H
#define BASICTASK_1_TA96B338B10A9FF438D49FEEF0A97378798D99F57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.BasicTask`1<System.Int32>
struct  BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57  : public RuntimeObject
{
public:
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEvent
	OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * ____onTaskCompletedEvent_0;
	// Tayr.OnTaskCompleted`1<T> Tayr.BasicTask`1::_onTaskCompletedEventInternal
	OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * ____onTaskCompletedEventInternal_1;
	// T Tayr.BasicTask`1::Result
	int32_t ___Result_2;
	// System.Boolean Tayr.BasicTask`1::_isDone
	bool ____isDone_3;
	// Tayr.ITaskRunner Tayr.BasicTask`1::_taskRunner
	RuntimeObject* ____taskRunner_4;
	// Tayr.ITaskContainer Tayr.BasicTask`1::_taskContainer
	RuntimeObject* ____taskContainer_5;
	// System.Int32 Tayr.BasicTask`1::TaskContainerIndex
	int32_t ___TaskContainerIndex_6;

public:
	inline static int32_t get_offset_of__onTaskCompletedEvent_0() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____onTaskCompletedEvent_0)); }
	inline OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * get__onTaskCompletedEvent_0() const { return ____onTaskCompletedEvent_0; }
	inline OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE ** get_address_of__onTaskCompletedEvent_0() { return &____onTaskCompletedEvent_0; }
	inline void set__onTaskCompletedEvent_0(OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * value)
	{
		____onTaskCompletedEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEvent_0), value);
	}

	inline static int32_t get_offset_of__onTaskCompletedEventInternal_1() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____onTaskCompletedEventInternal_1)); }
	inline OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * get__onTaskCompletedEventInternal_1() const { return ____onTaskCompletedEventInternal_1; }
	inline OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE ** get_address_of__onTaskCompletedEventInternal_1() { return &____onTaskCompletedEventInternal_1; }
	inline void set__onTaskCompletedEventInternal_1(OnTaskCompleted_1_t368CAFE8FF75609E99E539FEAD235A08380DC2DE * value)
	{
		____onTaskCompletedEventInternal_1 = value;
		Il2CppCodeGenWriteBarrier((&____onTaskCompletedEventInternal_1), value);
	}

	inline static int32_t get_offset_of_Result_2() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ___Result_2)); }
	inline int32_t get_Result_2() const { return ___Result_2; }
	inline int32_t* get_address_of_Result_2() { return &___Result_2; }
	inline void set_Result_2(int32_t value)
	{
		___Result_2 = value;
	}

	inline static int32_t get_offset_of__isDone_3() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____isDone_3)); }
	inline bool get__isDone_3() const { return ____isDone_3; }
	inline bool* get_address_of__isDone_3() { return &____isDone_3; }
	inline void set__isDone_3(bool value)
	{
		____isDone_3 = value;
	}

	inline static int32_t get_offset_of__taskRunner_4() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____taskRunner_4)); }
	inline RuntimeObject* get__taskRunner_4() const { return ____taskRunner_4; }
	inline RuntimeObject** get_address_of__taskRunner_4() { return &____taskRunner_4; }
	inline void set__taskRunner_4(RuntimeObject* value)
	{
		____taskRunner_4 = value;
		Il2CppCodeGenWriteBarrier((&____taskRunner_4), value);
	}

	inline static int32_t get_offset_of__taskContainer_5() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ____taskContainer_5)); }
	inline RuntimeObject* get__taskContainer_5() const { return ____taskContainer_5; }
	inline RuntimeObject** get_address_of__taskContainer_5() { return &____taskContainer_5; }
	inline void set__taskContainer_5(RuntimeObject* value)
	{
		____taskContainer_5 = value;
		Il2CppCodeGenWriteBarrier((&____taskContainer_5), value);
	}

	inline static int32_t get_offset_of_TaskContainerIndex_6() { return static_cast<int32_t>(offsetof(BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57, ___TaskContainerIndex_6)); }
	inline int32_t get_TaskContainerIndex_6() const { return ___TaskContainerIndex_6; }
	inline int32_t* get_address_of_TaskContainerIndex_6() { return &___TaskContainerIndex_6; }
	inline void set_TaskContainerIndex_6(int32_t value)
	{
		___TaskContainerIndex_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICTASK_1_TA96B338B10A9FF438D49FEEF0A97378798D99F57_H
#ifndef TDEVICEUTILS_T5A8435FBCA12D5AB75BAFC6B658875F57839E3D1_H
#define TDEVICEUTILS_T5A8435FBCA12D5AB75BAFC6B658875F57839E3D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TDeviceUtils
struct  TDeviceUtils_t5A8435FBCA12D5AB75BAFC6B658875F57839E3D1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TDEVICEUTILS_T5A8435FBCA12D5AB75BAFC6B658875F57839E3D1_H
#ifndef TPREFABDATA_T6BDD5C7ECDED47BA807996C12EB93CAC2C3E826E_H
#define TPREFABDATA_T6BDD5C7ECDED47BA807996C12EB93CAC2C3E826E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TPrefabData
struct  TPrefabData_t6BDD5C7ECDED47BA807996C12EB93CAC2C3E826E  : public RuntimeObject
{
public:
	// System.String Tayr.TPrefabData::AssetBundle
	String_t* ___AssetBundle_0;
	// System.String Tayr.TPrefabData::PrefabName
	String_t* ___PrefabName_1;

public:
	inline static int32_t get_offset_of_AssetBundle_0() { return static_cast<int32_t>(offsetof(TPrefabData_t6BDD5C7ECDED47BA807996C12EB93CAC2C3E826E, ___AssetBundle_0)); }
	inline String_t* get_AssetBundle_0() const { return ___AssetBundle_0; }
	inline String_t** get_address_of_AssetBundle_0() { return &___AssetBundle_0; }
	inline void set_AssetBundle_0(String_t* value)
	{
		___AssetBundle_0 = value;
		Il2CppCodeGenWriteBarrier((&___AssetBundle_0), value);
	}

	inline static int32_t get_offset_of_PrefabName_1() { return static_cast<int32_t>(offsetof(TPrefabData_t6BDD5C7ECDED47BA807996C12EB93CAC2C3E826E, ___PrefabName_1)); }
	inline String_t* get_PrefabName_1() const { return ___PrefabName_1; }
	inline String_t** get_address_of_PrefabName_1() { return &___PrefabName_1; }
	inline void set_PrefabName_1(String_t* value)
	{
		___PrefabName_1 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TPREFABDATA_T6BDD5C7ECDED47BA807996C12EB93CAC2C3E826E_H
#ifndef TPREFABSCONF_T4F6D2D381170516B67D7F3198041F9B3FB0C4DE1_H
#define TPREFABSCONF_T4F6D2D381170516B67D7F3198041F9B3FB0C4DE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TPrefabsConf
struct  TPrefabsConf_t4F6D2D381170516B67D7F3198041F9B3FB0C4DE1  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Tayr.TPrefabData> Tayr.TPrefabsConf::Prefabs
	Dictionary_2_t999FF5ACCFAB22CE63918E3F7443780DB84A95EC * ___Prefabs_0;

public:
	inline static int32_t get_offset_of_Prefabs_0() { return static_cast<int32_t>(offsetof(TPrefabsConf_t4F6D2D381170516B67D7F3198041F9B3FB0C4DE1, ___Prefabs_0)); }
	inline Dictionary_2_t999FF5ACCFAB22CE63918E3F7443780DB84A95EC * get_Prefabs_0() const { return ___Prefabs_0; }
	inline Dictionary_2_t999FF5ACCFAB22CE63918E3F7443780DB84A95EC ** get_address_of_Prefabs_0() { return &___Prefabs_0; }
	inline void set_Prefabs_0(Dictionary_2_t999FF5ACCFAB22CE63918E3F7443780DB84A95EC * value)
	{
		___Prefabs_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefabs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TPREFABSCONF_T4F6D2D381170516B67D7F3198041F9B3FB0C4DE1_H
#ifndef TSYSTEMSMANAGER_T22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462_H
#define TSYSTEMSMANAGER_T22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TSystemsManager
struct  TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Zenject.DiContainer> Tayr.TSystemsManager::_systemsContainers
	Dictionary_2_tEA3EFEB6ED5173AA6CE25B304791D523ED67AA3A * ____systemsContainers_0;

public:
	inline static int32_t get_offset_of__systemsContainers_0() { return static_cast<int32_t>(offsetof(TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462, ____systemsContainers_0)); }
	inline Dictionary_2_tEA3EFEB6ED5173AA6CE25B304791D523ED67AA3A * get__systemsContainers_0() const { return ____systemsContainers_0; }
	inline Dictionary_2_tEA3EFEB6ED5173AA6CE25B304791D523ED67AA3A ** get_address_of__systemsContainers_0() { return &____systemsContainers_0; }
	inline void set__systemsContainers_0(Dictionary_2_tEA3EFEB6ED5173AA6CE25B304791D523ED67AA3A * value)
	{
		____systemsContainers_0 = value;
		Il2CppCodeGenWriteBarrier((&____systemsContainers_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TSYSTEMSMANAGER_T22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462_H
#ifndef TUPDATERSYSTEM_T69A618B692764BD9B69DD0829A3D8DDFEC7AFA2E_H
#define TUPDATERSYSTEM_T69A618B692764BD9B69DD0829A3D8DDFEC7AFA2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TUpdaterSystem
struct  TUpdaterSystem_t69A618B692764BD9B69DD0829A3D8DDFEC7AFA2E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Tayr.ITUpdatable> Tayr.TUpdaterSystem::_updataList
	List_1_t559E7DE3147BE4C3C954CF553B1BB9B3E6E06F1D * ____updataList_0;

public:
	inline static int32_t get_offset_of__updataList_0() { return static_cast<int32_t>(offsetof(TUpdaterSystem_t69A618B692764BD9B69DD0829A3D8DDFEC7AFA2E, ____updataList_0)); }
	inline List_1_t559E7DE3147BE4C3C954CF553B1BB9B3E6E06F1D * get__updataList_0() const { return ____updataList_0; }
	inline List_1_t559E7DE3147BE4C3C954CF553B1BB9B3E6E06F1D ** get_address_of__updataList_0() { return &____updataList_0; }
	inline void set__updataList_0(List_1_t559E7DE3147BE4C3C954CF553B1BB9B3E6E06F1D * value)
	{
		____updataList_0 = value;
		Il2CppCodeGenWriteBarrier((&____updataList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPDATERSYSTEM_T69A618B692764BD9B69DD0829A3D8DDFEC7AFA2E_H
#ifndef UTILS_TB422C8B784B7ABE33AE5F796861A931E89AC0C97_H
#define UTILS_TB422C8B784B7ABE33AE5F796861A931E89AC0C97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.Utils
struct  Utils_tB422C8B784B7ABE33AE5F796861A931E89AC0C97  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_TB422C8B784B7ABE33AE5F796861A931E89AC0C97_H
#ifndef VARIABLESSYSTEM_T32C6897063875FC0FF77B3F2D5E8218BAA904C92_H
#define VARIABLESSYSTEM_T32C6897063875FC0FF77B3F2D5E8218BAA904C92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.VariablesSystem
struct  VariablesSystem_t32C6897063875FC0FF77B3F2D5E8218BAA904C92  : public RuntimeObject
{
public:

public:
};

struct VariablesSystem_t32C6897063875FC0FF77B3F2D5E8218BAA904C92_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Tayr.Variable> Tayr.VariablesSystem::_variables
	Dictionary_2_t0858D6C06FC1F458F73010213615C9EB8A4F7DC7 * ____variables_0;

public:
	inline static int32_t get_offset_of__variables_0() { return static_cast<int32_t>(offsetof(VariablesSystem_t32C6897063875FC0FF77B3F2D5E8218BAA904C92_StaticFields, ____variables_0)); }
	inline Dictionary_2_t0858D6C06FC1F458F73010213615C9EB8A4F7DC7 * get__variables_0() const { return ____variables_0; }
	inline Dictionary_2_t0858D6C06FC1F458F73010213615C9EB8A4F7DC7 ** get_address_of__variables_0() { return &____variables_0; }
	inline void set__variables_0(Dictionary_2_t0858D6C06FC1F458F73010213615C9EB8A4F7DC7 * value)
	{
		____variables_0 = value;
		Il2CppCodeGenWriteBarrier((&____variables_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLESSYSTEM_T32C6897063875FC0FF77B3F2D5E8218BAA904C92_H
#ifndef TIMEPRESSURESYSTEM_T4FA791F11A0516BF61255D3B11478018665F9486_H
#define TIMEPRESSURESYSTEM_T4FA791F11A0516BF61255D3B11478018665F9486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimePressureSystem
struct  TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486  : public RuntimeObject
{
public:
	// Tayr.ILibrary TimePressureSystem::_uiLibrary
	RuntimeObject* ____uiLibrary_0;
	// Zenject.DiContainer TimePressureSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_1;
	// UserVO TimePressureSystem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_2;
	// ChaptersSO TimePressureSystem::_chaptersSO
	ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * ____chaptersSO_3;
	// BattleEcsInitializer TimePressureSystem::_battleEcsInitializer
	BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * ____battleEcsInitializer_4;
	// GameContext TimePressureSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__uiLibrary_0() { return static_cast<int32_t>(offsetof(TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486, ____uiLibrary_0)); }
	inline RuntimeObject* get__uiLibrary_0() const { return ____uiLibrary_0; }
	inline RuntimeObject** get_address_of__uiLibrary_0() { return &____uiLibrary_0; }
	inline void set__uiLibrary_0(RuntimeObject* value)
	{
		____uiLibrary_0 = value;
		Il2CppCodeGenWriteBarrier((&____uiLibrary_0), value);
	}

	inline static int32_t get_offset_of__diContainer_1() { return static_cast<int32_t>(offsetof(TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486, ____diContainer_1)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_1() const { return ____diContainer_1; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_1() { return &____diContainer_1; }
	inline void set__diContainer_1(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_1 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_1), value);
	}

	inline static int32_t get_offset_of__userVO_2() { return static_cast<int32_t>(offsetof(TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486, ____userVO_2)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_2() const { return ____userVO_2; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_2() { return &____userVO_2; }
	inline void set__userVO_2(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_2 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_2), value);
	}

	inline static int32_t get_offset_of__chaptersSO_3() { return static_cast<int32_t>(offsetof(TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486, ____chaptersSO_3)); }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * get__chaptersSO_3() const { return ____chaptersSO_3; }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 ** get_address_of__chaptersSO_3() { return &____chaptersSO_3; }
	inline void set__chaptersSO_3(ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * value)
	{
		____chaptersSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____chaptersSO_3), value);
	}

	inline static int32_t get_offset_of__battleEcsInitializer_4() { return static_cast<int32_t>(offsetof(TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486, ____battleEcsInitializer_4)); }
	inline BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * get__battleEcsInitializer_4() const { return ____battleEcsInitializer_4; }
	inline BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 ** get_address_of__battleEcsInitializer_4() { return &____battleEcsInitializer_4; }
	inline void set__battleEcsInitializer_4(BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * value)
	{
		____battleEcsInitializer_4 = value;
		Il2CppCodeGenWriteBarrier((&____battleEcsInitializer_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEPRESSURESYSTEM_T4FA791F11A0516BF61255D3B11478018665F9486_H
#ifndef TOWERCOMPONENT_T7F9C6F9201DE020590AF5DF3FCF9FEF5D324F121_H
#define TOWERCOMPONENT_T7F9C6F9201DE020590AF5DF3FCF9FEF5D324F121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerComponent
struct  TowerComponent_t7F9C6F9201DE020590AF5DF3FCF9FEF5D324F121  : public RuntimeObject
{
public:
	// System.String TowerComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(TowerComponent_t7F9C6F9201DE020590AF5DF3FCF9FEF5D324F121, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERCOMPONENT_T7F9C6F9201DE020590AF5DF3FCF9FEF5D324F121_H
#ifndef TRAPCOMPONENT_T028E60EBEFA4B61613AD9F3374AABE359CCADC4F_H
#define TRAPCOMPONENT_T028E60EBEFA4B61613AD9F3374AABE359CCADC4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrapComponent
struct  TrapComponent_t028E60EBEFA4B61613AD9F3374AABE359CCADC4F  : public RuntimeObject
{
public:
	// System.String TrapComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(TrapComponent_t028E60EBEFA4B61613AD9F3374AABE359CCADC4F, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAPCOMPONENT_T028E60EBEFA4B61613AD9F3374AABE359CCADC4F_H
#ifndef UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#define UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T6E0F7823762EE94BB8489B5AE41C7802A266D3D5_H
#ifndef WALLCOMPONENT_T9D1052458FC968342C88A87250380745F88CAB88_H
#define WALLCOMPONENT_T9D1052458FC968342C88A87250380745F88CAB88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WallComponent
struct  WallComponent_t9D1052458FC968342C88A87250380745F88CAB88  : public RuntimeObject
{
public:
	// System.String WallComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(WallComponent_t9D1052458FC968342C88A87250380745F88CAB88, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WALLCOMPONENT_T9D1052458FC968342C88A87250380745F88CAB88_H
#ifndef __STATICARRAYINITTYPESIZEU3D10_TA63458BD8CED83456312F196CD9CE7931AA28BF5_H
#define __STATICARRAYINITTYPESIZEU3D10_TA63458BD8CED83456312F196CD9CE7931AA28BF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D10
struct  __StaticArrayInitTypeSizeU3D10_tA63458BD8CED83456312F196CD9CE7931AA28BF5 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D10_tA63458BD8CED83456312F196CD9CE7931AA28BF5__padding[10];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D10_TA63458BD8CED83456312F196CD9CE7931AA28BF5_H
#ifndef __STATICARRAYINITTYPESIZEU3D24_TA66DA9D905CC3FD5269CBA439F53227E088C5962_H
#define __STATICARRAYINITTYPESIZEU3D24_TA66DA9D905CC3FD5269CBA439F53227E088C5962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24
struct  __StaticArrayInitTypeSizeU3D24_tA66DA9D905CC3FD5269CBA439F53227E088C5962 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_tA66DA9D905CC3FD5269CBA439F53227E088C5962__padding[24];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D24_TA66DA9D905CC3FD5269CBA439F53227E088C5962_H
#ifndef __STATICARRAYINITTYPESIZEU3D28_T9E81964A7111D1744E54D30D73684C823A4BCFB9_H
#define __STATICARRAYINITTYPESIZEU3D28_T9E81964A7111D1744E54D30D73684C823A4BCFB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28
struct  __StaticArrayInitTypeSizeU3D28_t9E81964A7111D1744E54D30D73684C823A4BCFB9 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D28_t9E81964A7111D1744E54D30D73684C823A4BCFB9__padding[28];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D28_T9E81964A7111D1744E54D30D73684C823A4BCFB9_H
#ifndef BATTLEBUNDLELOADERTASK_TF6A69A66DF4FBD3B527961F148301FF11E7E9BC2_H
#define BATTLEBUNDLELOADERTASK_TF6A69A66DF4FBD3B527961F148301FF11E7E9BC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleBundleLoaderTask
struct  BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2  : public BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57
{
public:
	// Zenject.DiContainer BattleBundleLoaderTask::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_7;
	// UserVO BattleBundleLoaderTask::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_8;
	// ChaptersSO BattleBundleLoaderTask::_chaptersSO
	ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * ____chaptersSO_9;
	// UnitsSO BattleBundleLoaderTask::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_10;
	// TowersSO BattleBundleLoaderTask::_towersSO
	TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * ____towersSO_11;
	// ItemsSO BattleBundleLoaderTask::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_12;
	// TrapsSO BattleBundleLoaderTask::_trapsSO
	TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2 * ____trapsSO_13;

public:
	inline static int32_t get_offset_of__container_7() { return static_cast<int32_t>(offsetof(BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2, ____container_7)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_7() const { return ____container_7; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_7() { return &____container_7; }
	inline void set__container_7(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_7 = value;
		Il2CppCodeGenWriteBarrier((&____container_7), value);
	}

	inline static int32_t get_offset_of__userVO_8() { return static_cast<int32_t>(offsetof(BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2, ____userVO_8)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_8() const { return ____userVO_8; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_8() { return &____userVO_8; }
	inline void set__userVO_8(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_8 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_8), value);
	}

	inline static int32_t get_offset_of__chaptersSO_9() { return static_cast<int32_t>(offsetof(BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2, ____chaptersSO_9)); }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * get__chaptersSO_9() const { return ____chaptersSO_9; }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 ** get_address_of__chaptersSO_9() { return &____chaptersSO_9; }
	inline void set__chaptersSO_9(ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * value)
	{
		____chaptersSO_9 = value;
		Il2CppCodeGenWriteBarrier((&____chaptersSO_9), value);
	}

	inline static int32_t get_offset_of__unitsSO_10() { return static_cast<int32_t>(offsetof(BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2, ____unitsSO_10)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_10() const { return ____unitsSO_10; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_10() { return &____unitsSO_10; }
	inline void set__unitsSO_10(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_10 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_10), value);
	}

	inline static int32_t get_offset_of__towersSO_11() { return static_cast<int32_t>(offsetof(BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2, ____towersSO_11)); }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * get__towersSO_11() const { return ____towersSO_11; }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 ** get_address_of__towersSO_11() { return &____towersSO_11; }
	inline void set__towersSO_11(TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * value)
	{
		____towersSO_11 = value;
		Il2CppCodeGenWriteBarrier((&____towersSO_11), value);
	}

	inline static int32_t get_offset_of__itemsSO_12() { return static_cast<int32_t>(offsetof(BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2, ____itemsSO_12)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_12() const { return ____itemsSO_12; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_12() { return &____itemsSO_12; }
	inline void set__itemsSO_12(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_12 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_12), value);
	}

	inline static int32_t get_offset_of__trapsSO_13() { return static_cast<int32_t>(offsetof(BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2, ____trapsSO_13)); }
	inline TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2 * get__trapsSO_13() const { return ____trapsSO_13; }
	inline TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2 ** get_address_of__trapsSO_13() { return &____trapsSO_13; }
	inline void set__trapsSO_13(TrapsSO_tFF060852B3652DD05F33E1B19B8EC9166468D9C2 * value)
	{
		____trapsSO_13 = value;
		Il2CppCodeGenWriteBarrier((&____trapsSO_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEBUNDLELOADERTASK_TF6A69A66DF4FBD3B527961F148301FF11E7E9BC2_H
#ifndef BATTLELOADERTASK_TEDC6FA66A41F28C5B262EE753494294B090BC0F3_H
#define BATTLELOADERTASK_TEDC6FA66A41F28C5B262EE753494294B090BC0F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleLoaderTask
struct  BattleLoaderTask_tEDC6FA66A41F28C5B262EE753494294B090BC0F3  : public BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57
{
public:
	// Zenject.DiContainer BattleLoaderTask::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_7;

public:
	inline static int32_t get_offset_of__diContainer_7() { return static_cast<int32_t>(offsetof(BattleLoaderTask_tEDC6FA66A41F28C5B262EE753494294B090BC0F3, ____diContainer_7)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_7() const { return ____diContainer_7; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_7() { return &____diContainer_7; }
	inline void set__diContainer_7(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_7 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLELOADERTASK_TEDC6FA66A41F28C5B262EE753494294B090BC0F3_H
#ifndef BATTLELOADINGSCREENSYSTEM_TA7FAC4ED598184467A80CBDB5125EE4DFA559CF5_H
#define BATTLELOADINGSCREENSYSTEM_TA7FAC4ED598184467A80CBDB5125EE4DFA559CF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleLoadingScreenSystem
struct  BattleLoadingScreenSystem_tA7FAC4ED598184467A80CBDB5125EE4DFA559CF5  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary BattleLoadingScreenSystem::_uiMain
	RuntimeObject* ____uiMain_3;
	// BattleSettingsSO BattleLoadingScreenSystem::_battleSettingsSO
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * ____battleSettingsSO_4;
	// GameContext BattleLoadingScreenSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__uiMain_3() { return static_cast<int32_t>(offsetof(BattleLoadingScreenSystem_tA7FAC4ED598184467A80CBDB5125EE4DFA559CF5, ____uiMain_3)); }
	inline RuntimeObject* get__uiMain_3() const { return ____uiMain_3; }
	inline RuntimeObject** get_address_of__uiMain_3() { return &____uiMain_3; }
	inline void set__uiMain_3(RuntimeObject* value)
	{
		____uiMain_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiMain_3), value);
	}

	inline static int32_t get_offset_of__battleSettingsSO_4() { return static_cast<int32_t>(offsetof(BattleLoadingScreenSystem_tA7FAC4ED598184467A80CBDB5125EE4DFA559CF5, ____battleSettingsSO_4)); }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * get__battleSettingsSO_4() const { return ____battleSettingsSO_4; }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE ** get_address_of__battleSettingsSO_4() { return &____battleSettingsSO_4; }
	inline void set__battleSettingsSO_4(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * value)
	{
		____battleSettingsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____battleSettingsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(BattleLoadingScreenSystem_tA7FAC4ED598184467A80CBDB5125EE4DFA559CF5, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLELOADINGSCREENSYSTEM_TA7FAC4ED598184467A80CBDB5125EE4DFA559CF5_H
#ifndef ACTKBYTE16_TBA7BB660E30157B457EAF59F1313F241EA53C340_H
#define ACTKBYTE16_TBA7BB660E30157B457EAF59F1313F241EA53C340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ACTkByte16
struct  ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340 
{
public:
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b1
	uint8_t ___b1_0;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b2
	uint8_t ___b2_1;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b3
	uint8_t ___b3_2;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b4
	uint8_t ___b4_3;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b5
	uint8_t ___b5_4;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b6
	uint8_t ___b6_5;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b7
	uint8_t ___b7_6;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b8
	uint8_t ___b8_7;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b9
	uint8_t ___b9_8;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b10
	uint8_t ___b10_9;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b11
	uint8_t ___b11_10;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b12
	uint8_t ___b12_11;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b13
	uint8_t ___b13_12;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b14
	uint8_t ___b14_13;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b15
	uint8_t ___b15_14;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b16
	uint8_t ___b16_15;

public:
	inline static int32_t get_offset_of_b1_0() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b1_0)); }
	inline uint8_t get_b1_0() const { return ___b1_0; }
	inline uint8_t* get_address_of_b1_0() { return &___b1_0; }
	inline void set_b1_0(uint8_t value)
	{
		___b1_0 = value;
	}

	inline static int32_t get_offset_of_b2_1() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b2_1)); }
	inline uint8_t get_b2_1() const { return ___b2_1; }
	inline uint8_t* get_address_of_b2_1() { return &___b2_1; }
	inline void set_b2_1(uint8_t value)
	{
		___b2_1 = value;
	}

	inline static int32_t get_offset_of_b3_2() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b3_2)); }
	inline uint8_t get_b3_2() const { return ___b3_2; }
	inline uint8_t* get_address_of_b3_2() { return &___b3_2; }
	inline void set_b3_2(uint8_t value)
	{
		___b3_2 = value;
	}

	inline static int32_t get_offset_of_b4_3() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b4_3)); }
	inline uint8_t get_b4_3() const { return ___b4_3; }
	inline uint8_t* get_address_of_b4_3() { return &___b4_3; }
	inline void set_b4_3(uint8_t value)
	{
		___b4_3 = value;
	}

	inline static int32_t get_offset_of_b5_4() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b5_4)); }
	inline uint8_t get_b5_4() const { return ___b5_4; }
	inline uint8_t* get_address_of_b5_4() { return &___b5_4; }
	inline void set_b5_4(uint8_t value)
	{
		___b5_4 = value;
	}

	inline static int32_t get_offset_of_b6_5() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b6_5)); }
	inline uint8_t get_b6_5() const { return ___b6_5; }
	inline uint8_t* get_address_of_b6_5() { return &___b6_5; }
	inline void set_b6_5(uint8_t value)
	{
		___b6_5 = value;
	}

	inline static int32_t get_offset_of_b7_6() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b7_6)); }
	inline uint8_t get_b7_6() const { return ___b7_6; }
	inline uint8_t* get_address_of_b7_6() { return &___b7_6; }
	inline void set_b7_6(uint8_t value)
	{
		___b7_6 = value;
	}

	inline static int32_t get_offset_of_b8_7() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b8_7)); }
	inline uint8_t get_b8_7() const { return ___b8_7; }
	inline uint8_t* get_address_of_b8_7() { return &___b8_7; }
	inline void set_b8_7(uint8_t value)
	{
		___b8_7 = value;
	}

	inline static int32_t get_offset_of_b9_8() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b9_8)); }
	inline uint8_t get_b9_8() const { return ___b9_8; }
	inline uint8_t* get_address_of_b9_8() { return &___b9_8; }
	inline void set_b9_8(uint8_t value)
	{
		___b9_8 = value;
	}

	inline static int32_t get_offset_of_b10_9() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b10_9)); }
	inline uint8_t get_b10_9() const { return ___b10_9; }
	inline uint8_t* get_address_of_b10_9() { return &___b10_9; }
	inline void set_b10_9(uint8_t value)
	{
		___b10_9 = value;
	}

	inline static int32_t get_offset_of_b11_10() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b11_10)); }
	inline uint8_t get_b11_10() const { return ___b11_10; }
	inline uint8_t* get_address_of_b11_10() { return &___b11_10; }
	inline void set_b11_10(uint8_t value)
	{
		___b11_10 = value;
	}

	inline static int32_t get_offset_of_b12_11() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b12_11)); }
	inline uint8_t get_b12_11() const { return ___b12_11; }
	inline uint8_t* get_address_of_b12_11() { return &___b12_11; }
	inline void set_b12_11(uint8_t value)
	{
		___b12_11 = value;
	}

	inline static int32_t get_offset_of_b13_12() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b13_12)); }
	inline uint8_t get_b13_12() const { return ___b13_12; }
	inline uint8_t* get_address_of_b13_12() { return &___b13_12; }
	inline void set_b13_12(uint8_t value)
	{
		___b13_12 = value;
	}

	inline static int32_t get_offset_of_b14_13() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b14_13)); }
	inline uint8_t get_b14_13() const { return ___b14_13; }
	inline uint8_t* get_address_of_b14_13() { return &___b14_13; }
	inline void set_b14_13(uint8_t value)
	{
		___b14_13 = value;
	}

	inline static int32_t get_offset_of_b15_14() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b15_14)); }
	inline uint8_t get_b15_14() const { return ___b15_14; }
	inline uint8_t* get_address_of_b15_14() { return &___b15_14; }
	inline void set_b15_14(uint8_t value)
	{
		___b15_14 = value;
	}

	inline static int32_t get_offset_of_b16_15() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b16_15)); }
	inline uint8_t get_b16_15() const { return ___b16_15; }
	inline uint8_t* get_address_of_b16_15() { return &___b16_15; }
	inline void set_b16_15(uint8_t value)
	{
		___b16_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKBYTE16_TBA7BB660E30157B457EAF59F1313F241EA53C340_H
#ifndef ACTKBYTE4_T07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE_H
#define ACTKBYTE4_T07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ACTkByte4
struct  ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE 
{
public:
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b1
	uint8_t ___b1_0;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b2
	uint8_t ___b2_1;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b3
	uint8_t ___b3_2;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b4
	uint8_t ___b4_3;

public:
	inline static int32_t get_offset_of_b1_0() { return static_cast<int32_t>(offsetof(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE, ___b1_0)); }
	inline uint8_t get_b1_0() const { return ___b1_0; }
	inline uint8_t* get_address_of_b1_0() { return &___b1_0; }
	inline void set_b1_0(uint8_t value)
	{
		___b1_0 = value;
	}

	inline static int32_t get_offset_of_b2_1() { return static_cast<int32_t>(offsetof(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE, ___b2_1)); }
	inline uint8_t get_b2_1() const { return ___b2_1; }
	inline uint8_t* get_address_of_b2_1() { return &___b2_1; }
	inline void set_b2_1(uint8_t value)
	{
		___b2_1 = value;
	}

	inline static int32_t get_offset_of_b3_2() { return static_cast<int32_t>(offsetof(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE, ___b3_2)); }
	inline uint8_t get_b3_2() const { return ___b3_2; }
	inline uint8_t* get_address_of_b3_2() { return &___b3_2; }
	inline void set_b3_2(uint8_t value)
	{
		___b3_2 = value;
	}

	inline static int32_t get_offset_of_b4_3() { return static_cast<int32_t>(offsetof(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE, ___b4_3)); }
	inline uint8_t get_b4_3() const { return ___b4_3; }
	inline uint8_t* get_address_of_b4_3() { return &___b4_3; }
	inline void set_b4_3(uint8_t value)
	{
		___b4_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKBYTE4_T07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE_H
#ifndef ACTKBYTE8_T833E13ECB9BCA90642862B8F95D336AE37DEA24E_H
#define ACTKBYTE8_T833E13ECB9BCA90642862B8F95D336AE37DEA24E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ACTkByte8
struct  ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E 
{
public:
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b1
	uint8_t ___b1_0;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b2
	uint8_t ___b2_1;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b3
	uint8_t ___b3_2;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b4
	uint8_t ___b4_3;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b5
	uint8_t ___b5_4;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b6
	uint8_t ___b6_5;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b7
	uint8_t ___b7_6;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b8
	uint8_t ___b8_7;

public:
	inline static int32_t get_offset_of_b1_0() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b1_0)); }
	inline uint8_t get_b1_0() const { return ___b1_0; }
	inline uint8_t* get_address_of_b1_0() { return &___b1_0; }
	inline void set_b1_0(uint8_t value)
	{
		___b1_0 = value;
	}

	inline static int32_t get_offset_of_b2_1() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b2_1)); }
	inline uint8_t get_b2_1() const { return ___b2_1; }
	inline uint8_t* get_address_of_b2_1() { return &___b2_1; }
	inline void set_b2_1(uint8_t value)
	{
		___b2_1 = value;
	}

	inline static int32_t get_offset_of_b3_2() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b3_2)); }
	inline uint8_t get_b3_2() const { return ___b3_2; }
	inline uint8_t* get_address_of_b3_2() { return &___b3_2; }
	inline void set_b3_2(uint8_t value)
	{
		___b3_2 = value;
	}

	inline static int32_t get_offset_of_b4_3() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b4_3)); }
	inline uint8_t get_b4_3() const { return ___b4_3; }
	inline uint8_t* get_address_of_b4_3() { return &___b4_3; }
	inline void set_b4_3(uint8_t value)
	{
		___b4_3 = value;
	}

	inline static int32_t get_offset_of_b5_4() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b5_4)); }
	inline uint8_t get_b5_4() const { return ___b5_4; }
	inline uint8_t* get_address_of_b5_4() { return &___b5_4; }
	inline void set_b5_4(uint8_t value)
	{
		___b5_4 = value;
	}

	inline static int32_t get_offset_of_b6_5() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b6_5)); }
	inline uint8_t get_b6_5() const { return ___b6_5; }
	inline uint8_t* get_address_of_b6_5() { return &___b6_5; }
	inline void set_b6_5(uint8_t value)
	{
		___b6_5 = value;
	}

	inline static int32_t get_offset_of_b7_6() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b7_6)); }
	inline uint8_t get_b7_6() const { return ___b7_6; }
	inline uint8_t* get_address_of_b7_6() { return &___b7_6; }
	inline void set_b7_6(uint8_t value)
	{
		___b7_6 = value;
	}

	inline static int32_t get_offset_of_b8_7() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b8_7)); }
	inline uint8_t get_b8_7() const { return ___b8_7; }
	inline uint8_t* get_address_of_b8_7() { return &___b8_7; }
	inline void set_b8_7(uint8_t value)
	{
		___b8_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKBYTE8_T833E13ECB9BCA90642862B8F95D336AE37DEA24E_H
#ifndef OBSCUREDBOOL_T2CC3647BFB7D4EE9035407A3B3579A429F018452_H
#define OBSCUREDBOOL_T2CC3647BFB7D4EE9035407A3B3579A429F018452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredBool
struct  ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452 
{
public:
	// System.Byte CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::currentCryptoKey
	uint8_t ___currentCryptoKey_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::hiddenValue
	int32_t ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::inited
	bool ___inited_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::fakeValue
	bool ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452, ___currentCryptoKey_0)); }
	inline uint8_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline uint8_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(uint8_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452, ___hiddenValue_1)); }
	inline int32_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline int32_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(int32_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452, ___fakeValue_3)); }
	inline bool get_fakeValue_3() const { return ___fakeValue_3; }
	inline bool* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(bool value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredBool
struct ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452_marshaled_pinvoke
{
	uint8_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	int32_t ___inited_2;
	int32_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredBool
struct ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452_marshaled_com
{
	uint8_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	int32_t ___inited_2;
	int32_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDBOOL_T2CC3647BFB7D4EE9035407A3B3579A429F018452_H
#ifndef OBSCUREDINT_TBF7AD58D75DF4170E88029FF34F72A59951A2489_H
#define OBSCUREDINT_TBF7AD58D75DF4170E88029FF34F72A59951A2489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt
struct  ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::currentCryptoKey
	int32_t ___currentCryptoKey_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::hiddenValue
	int32_t ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::inited
	bool ___inited_2;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::fakeValue
	int32_t ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___currentCryptoKey_0)); }
	inline int32_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline int32_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(int32_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___hiddenValue_1)); }
	inline int32_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline int32_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(int32_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___fakeValue_3)); }
	inline int32_t get_fakeValue_3() const { return ___fakeValue_3; }
	inline int32_t* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(int32_t value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredInt
struct ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	int32_t ___inited_2;
	int32_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredInt
struct ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489_marshaled_com
{
	int32_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	int32_t ___inited_2;
	int32_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDINT_TBF7AD58D75DF4170E88029FF34F72A59951A2489_H
#ifndef OBSCUREDLONG_T3A2E6B6B36061DD41638EAFD4FE975FC66CCE609_H
#define OBSCUREDLONG_T3A2E6B6B36061DD41638EAFD4FE975FC66CCE609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredLong
struct  ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609 
{
public:
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredLong::currentCryptoKey
	int64_t ___currentCryptoKey_0;
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredLong::hiddenValue
	int64_t ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredLong::inited
	bool ___inited_2;
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredLong::fakeValue
	int64_t ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredLong::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609, ___currentCryptoKey_0)); }
	inline int64_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline int64_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(int64_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609, ___hiddenValue_1)); }
	inline int64_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline int64_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(int64_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609, ___fakeValue_3)); }
	inline int64_t get_fakeValue_3() const { return ___fakeValue_3; }
	inline int64_t* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(int64_t value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredLong
struct ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609_marshaled_pinvoke
{
	int64_t ___currentCryptoKey_0;
	int64_t ___hiddenValue_1;
	int32_t ___inited_2;
	int64_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredLong
struct ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609_marshaled_com
{
	int64_t ___currentCryptoKey_0;
	int64_t ___hiddenValue_1;
	int32_t ___inited_2;
	int64_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDLONG_T3A2E6B6B36061DD41638EAFD4FE975FC66CCE609_H
#ifndef RAWENCRYPTEDVECTOR2_TFF1E132A3237D22895804BE30F9011B94E9BF5CC_H
#define RAWENCRYPTEDVECTOR2_TFF1E132A3237D22895804BE30F9011B94E9BF5CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2_RawEncryptedVector2
struct  RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2_RawEncryptedVector2::x
	int32_t ___x_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2_RawEncryptedVector2::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWENCRYPTEDVECTOR2_TFF1E132A3237D22895804BE30F9011B94E9BF5CC_H
#ifndef RAWENCRYPTEDVECTOR2INT_T7355329D63C8B123505CCAFD38B6F28BD9093F7D_H
#define RAWENCRYPTEDVECTOR2INT_T7355329D63C8B123505CCAFD38B6F28BD9093F7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int_RawEncryptedVector2Int
struct  RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int_RawEncryptedVector2Int::x
	int32_t ___x_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int_RawEncryptedVector2Int::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWENCRYPTEDVECTOR2INT_T7355329D63C8B123505CCAFD38B6F28BD9093F7D_H
#ifndef RAWENCRYPTEDVECTOR3_T766A78183852F7A89484E372DBFD15EEF76AB627_H
#define RAWENCRYPTEDVECTOR3_T766A78183852F7A89484E372DBFD15EEF76AB627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3
struct  RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3::x
	int32_t ___x_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3::y
	int32_t ___y_1;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3::z
	int32_t ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWENCRYPTEDVECTOR3_T766A78183852F7A89484E372DBFD15EEF76AB627_H
#ifndef RAWENCRYPTEDVECTOR3INT_TFC038D95C234B20C841F481FAB1B435C389129D3_H
#define RAWENCRYPTEDVECTOR3INT_TFC038D95C234B20C841F481FAB1B435C389129D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int_RawEncryptedVector3Int
struct  RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int_RawEncryptedVector3Int::x
	int32_t ___x_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int_RawEncryptedVector3Int::y
	int32_t ___y_1;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int_RawEncryptedVector3Int::z
	int32_t ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWENCRYPTEDVECTOR3INT_TFC038D95C234B20C841F481FAB1B435C389129D3_H
#ifndef FEATURE_T3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F_H
#define FEATURE_T3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Feature
struct  Feature_t3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F  : public Systems_t4C86DA84B6D6F50FF23B497DD599ED594274DEA9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATURE_T3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F_H
#ifndef GAMEOVERSYSTEM_TCCBF26EA8389B7038B4EA7A8FF0E20299F159050_H
#define GAMEOVERSYSTEM_TCCBF26EA8389B7038B4EA7A8FF0E20299F159050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameOverSystem
struct  GameOverSystem_tCCBF26EA8389B7038B4EA7A8FF0E20299F159050  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary GameOverSystem::_uiLibrary
	RuntimeObject* ____uiLibrary_3;
	// UserVO GameOverSystem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_4;
	// BattleEcsInitializer GameOverSystem::_battleEcsInitializer
	BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * ____battleEcsInitializer_5;
	// FeaturesSwitchConfig GameOverSystem::_featuresSwitchConfig
	FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3 * ____featuresSwitchConfig_6;
	// GameContext GameOverSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_7;

public:
	inline static int32_t get_offset_of__uiLibrary_3() { return static_cast<int32_t>(offsetof(GameOverSystem_tCCBF26EA8389B7038B4EA7A8FF0E20299F159050, ____uiLibrary_3)); }
	inline RuntimeObject* get__uiLibrary_3() const { return ____uiLibrary_3; }
	inline RuntimeObject** get_address_of__uiLibrary_3() { return &____uiLibrary_3; }
	inline void set__uiLibrary_3(RuntimeObject* value)
	{
		____uiLibrary_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiLibrary_3), value);
	}

	inline static int32_t get_offset_of__userVO_4() { return static_cast<int32_t>(offsetof(GameOverSystem_tCCBF26EA8389B7038B4EA7A8FF0E20299F159050, ____userVO_4)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_4() const { return ____userVO_4; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_4() { return &____userVO_4; }
	inline void set__userVO_4(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_4 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_4), value);
	}

	inline static int32_t get_offset_of__battleEcsInitializer_5() { return static_cast<int32_t>(offsetof(GameOverSystem_tCCBF26EA8389B7038B4EA7A8FF0E20299F159050, ____battleEcsInitializer_5)); }
	inline BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * get__battleEcsInitializer_5() const { return ____battleEcsInitializer_5; }
	inline BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 ** get_address_of__battleEcsInitializer_5() { return &____battleEcsInitializer_5; }
	inline void set__battleEcsInitializer_5(BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * value)
	{
		____battleEcsInitializer_5 = value;
		Il2CppCodeGenWriteBarrier((&____battleEcsInitializer_5), value);
	}

	inline static int32_t get_offset_of__featuresSwitchConfig_6() { return static_cast<int32_t>(offsetof(GameOverSystem_tCCBF26EA8389B7038B4EA7A8FF0E20299F159050, ____featuresSwitchConfig_6)); }
	inline FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3 * get__featuresSwitchConfig_6() const { return ____featuresSwitchConfig_6; }
	inline FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3 ** get_address_of__featuresSwitchConfig_6() { return &____featuresSwitchConfig_6; }
	inline void set__featuresSwitchConfig_6(FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3 * value)
	{
		____featuresSwitchConfig_6 = value;
		Il2CppCodeGenWriteBarrier((&____featuresSwitchConfig_6), value);
	}

	inline static int32_t get_offset_of__context_7() { return static_cast<int32_t>(offsetof(GameOverSystem_tCCBF26EA8389B7038B4EA7A8FF0E20299F159050, ____context_7)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_7() const { return ____context_7; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_7() { return &____context_7; }
	inline void set__context_7(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_7 = value;
		Il2CppCodeGenWriteBarrier((&____context_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOVERSYSTEM_TCCBF26EA8389B7038B4EA7A8FF0E20299F159050_H
#ifndef LOGINTASK_T03C0EA4FE385D98BE13064ADE4B32C270EFE52D8_H
#define LOGINTASK_T03C0EA4FE385D98BE13064ADE4B32C270EFE52D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginTask
struct  LoginTask_t03C0EA4FE385D98BE13064ADE4B32C270EFE52D8  : public BasicTask_1_t53F3CF30DDAF2EE5A2B123852828B30534469DE1
{
public:
	// Tayr.GameSparksPlatform LoginTask::_gameSparks
	GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * ____gameSparks_7;
	// UserEvents LoginTask::_userEvents
	UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * ____userEvents_8;

public:
	inline static int32_t get_offset_of__gameSparks_7() { return static_cast<int32_t>(offsetof(LoginTask_t03C0EA4FE385D98BE13064ADE4B32C270EFE52D8, ____gameSparks_7)); }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * get__gameSparks_7() const { return ____gameSparks_7; }
	inline GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 ** get_address_of__gameSparks_7() { return &____gameSparks_7; }
	inline void set__gameSparks_7(GameSparksPlatform_t2D9D0BDA751A4D9C3AADF6976B067D809AC59348 * value)
	{
		____gameSparks_7 = value;
		Il2CppCodeGenWriteBarrier((&____gameSparks_7), value);
	}

	inline static int32_t get_offset_of__userEvents_8() { return static_cast<int32_t>(offsetof(LoginTask_t03C0EA4FE385D98BE13064ADE4B32C270EFE52D8, ____userEvents_8)); }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * get__userEvents_8() const { return ____userEvents_8; }
	inline UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 ** get_address_of__userEvents_8() { return &____userEvents_8; }
	inline void set__userEvents_8(UserEvents_t029E2F0A52D6507E0686B07050DBEEEA9A32CDF7 * value)
	{
		____userEvents_8 = value;
		Il2CppCodeGenWriteBarrier((&____userEvents_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINTASK_T03C0EA4FE385D98BE13064ADE4B32C270EFE52D8_H
#ifndef MAPITEMSSYSTEM_TAD3CB1DE75F592F2250FB8ADF9539F6B51ABF3BD_H
#define MAPITEMSSYSTEM_TAD3CB1DE75F592F2250FB8ADF9539F6B51ABF3BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MapItemsSystem
struct  MapItemsSystem_tAD3CB1DE75F592F2250FB8ADF9539F6B51ABF3BD  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Zenject.DiContainer MapItemsSystem::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_3;
	// TowersSO MapItemsSystem::_towersSO
	TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * ____towersSO_4;
	// ItemsSO MapItemsSystem::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_5;
	// GameContext MapItemsSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_6;

public:
	inline static int32_t get_offset_of__container_3() { return static_cast<int32_t>(offsetof(MapItemsSystem_tAD3CB1DE75F592F2250FB8ADF9539F6B51ABF3BD, ____container_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_3() const { return ____container_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_3() { return &____container_3; }
	inline void set__container_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_3 = value;
		Il2CppCodeGenWriteBarrier((&____container_3), value);
	}

	inline static int32_t get_offset_of__towersSO_4() { return static_cast<int32_t>(offsetof(MapItemsSystem_tAD3CB1DE75F592F2250FB8ADF9539F6B51ABF3BD, ____towersSO_4)); }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * get__towersSO_4() const { return ____towersSO_4; }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 ** get_address_of__towersSO_4() { return &____towersSO_4; }
	inline void set__towersSO_4(TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * value)
	{
		____towersSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____towersSO_4), value);
	}

	inline static int32_t get_offset_of__itemsSO_5() { return static_cast<int32_t>(offsetof(MapItemsSystem_tAD3CB1DE75F592F2250FB8ADF9539F6B51ABF3BD, ____itemsSO_5)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_5() const { return ____itemsSO_5; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_5() { return &____itemsSO_5; }
	inline void set__itemsSO_5(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_5 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_5), value);
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(MapItemsSystem_tAD3CB1DE75F592F2250FB8ADF9539F6B51ABF3BD, ____context_6)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_6() const { return ____context_6; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAPITEMSSYSTEM_TAD3CB1DE75F592F2250FB8ADF9539F6B51ABF3BD_H
#ifndef NETWORKCHECKSYSTEM_TF7AB64F4FB1DFD7427C4D10C441BF7DE1CC5D938_H
#define NETWORKCHECKSYSTEM_TF7AB64F4FB1DFD7427C4D10C441BF7DE1CC5D938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NetworkCheckSystem
struct  NetworkCheckSystem_tF7AB64F4FB1DFD7427C4D10C441BF7DE1CC5D938  : public BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8
{
public:

public:
};

struct NetworkCheckSystem_tF7AB64F4FB1DFD7427C4D10C441BF7DE1CC5D938_StaticFields
{
public:
	// System.Single NetworkCheckSystem::_lastTimeCheckedNetwork
	float ____lastTimeCheckedNetwork_4;
	// System.Boolean NetworkCheckSystem::_isOnline
	bool ____isOnline_5;

public:
	inline static int32_t get_offset_of__lastTimeCheckedNetwork_4() { return static_cast<int32_t>(offsetof(NetworkCheckSystem_tF7AB64F4FB1DFD7427C4D10C441BF7DE1CC5D938_StaticFields, ____lastTimeCheckedNetwork_4)); }
	inline float get__lastTimeCheckedNetwork_4() const { return ____lastTimeCheckedNetwork_4; }
	inline float* get_address_of__lastTimeCheckedNetwork_4() { return &____lastTimeCheckedNetwork_4; }
	inline void set__lastTimeCheckedNetwork_4(float value)
	{
		____lastTimeCheckedNetwork_4 = value;
	}

	inline static int32_t get_offset_of__isOnline_5() { return static_cast<int32_t>(offsetof(NetworkCheckSystem_tF7AB64F4FB1DFD7427C4D10C441BF7DE1CC5D938_StaticFields, ____isOnline_5)); }
	inline bool get__isOnline_5() const { return ____isOnline_5; }
	inline bool* get_address_of__isOnline_5() { return &____isOnline_5; }
	inline void set__isOnline_5(bool value)
	{
		____isOnline_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCHECKSYSTEM_TF7AB64F4FB1DFD7427C4D10C441BF7DE1CC5D938_H
#ifndef OBJECTIVECOLLECTABLEDECREMENTSYSTEM_T9605F0194DF972FB383E70A99813F832FA67D784_H
#define OBJECTIVECOLLECTABLEDECREMENTSYSTEM_T9605F0194DF972FB383E70A99813F832FA67D784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectiveCollectableDecrementSystem
struct  ObjectiveCollectableDecrementSystem_t9605F0194DF972FB383E70A99813F832FA67D784  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext ObjectiveCollectableDecrementSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(ObjectiveCollectableDecrementSystem_t9605F0194DF972FB383E70A99813F832FA67D784, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVECOLLECTABLEDECREMENTSYSTEM_T9605F0194DF972FB383E70A99813F832FA67D784_H
#ifndef OBJECTIVECOMPLETESYSTEM_T9DE671D733D2983D240B650DF86180F97E2D2229_H
#define OBJECTIVECOMPLETESYSTEM_T9DE671D733D2983D240B650DF86180F97E2D2229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectiveCompleteSystem
struct  ObjectiveCompleteSystem_t9DE671D733D2983D240B650DF86180F97E2D2229  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary ObjectiveCompleteSystem::_uiLibrary
	RuntimeObject* ____uiLibrary_3;
	// UserVO ObjectiveCompleteSystem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_4;
	// BattleSettingsSO ObjectiveCompleteSystem::_battleSettingsSO
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * ____battleSettingsSO_5;
	// BattleEcsInitializer ObjectiveCompleteSystem::_battleEcsInitializer
	BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * ____battleEcsInitializer_6;
	// GameContext ObjectiveCompleteSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_7;

public:
	inline static int32_t get_offset_of__uiLibrary_3() { return static_cast<int32_t>(offsetof(ObjectiveCompleteSystem_t9DE671D733D2983D240B650DF86180F97E2D2229, ____uiLibrary_3)); }
	inline RuntimeObject* get__uiLibrary_3() const { return ____uiLibrary_3; }
	inline RuntimeObject** get_address_of__uiLibrary_3() { return &____uiLibrary_3; }
	inline void set__uiLibrary_3(RuntimeObject* value)
	{
		____uiLibrary_3 = value;
		Il2CppCodeGenWriteBarrier((&____uiLibrary_3), value);
	}

	inline static int32_t get_offset_of__userVO_4() { return static_cast<int32_t>(offsetof(ObjectiveCompleteSystem_t9DE671D733D2983D240B650DF86180F97E2D2229, ____userVO_4)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_4() const { return ____userVO_4; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_4() { return &____userVO_4; }
	inline void set__userVO_4(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_4 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_4), value);
	}

	inline static int32_t get_offset_of__battleSettingsSO_5() { return static_cast<int32_t>(offsetof(ObjectiveCompleteSystem_t9DE671D733D2983D240B650DF86180F97E2D2229, ____battleSettingsSO_5)); }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * get__battleSettingsSO_5() const { return ____battleSettingsSO_5; }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE ** get_address_of__battleSettingsSO_5() { return &____battleSettingsSO_5; }
	inline void set__battleSettingsSO_5(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * value)
	{
		____battleSettingsSO_5 = value;
		Il2CppCodeGenWriteBarrier((&____battleSettingsSO_5), value);
	}

	inline static int32_t get_offset_of__battleEcsInitializer_6() { return static_cast<int32_t>(offsetof(ObjectiveCompleteSystem_t9DE671D733D2983D240B650DF86180F97E2D2229, ____battleEcsInitializer_6)); }
	inline BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * get__battleEcsInitializer_6() const { return ____battleEcsInitializer_6; }
	inline BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 ** get_address_of__battleEcsInitializer_6() { return &____battleEcsInitializer_6; }
	inline void set__battleEcsInitializer_6(BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67 * value)
	{
		____battleEcsInitializer_6 = value;
		Il2CppCodeGenWriteBarrier((&____battleEcsInitializer_6), value);
	}

	inline static int32_t get_offset_of__context_7() { return static_cast<int32_t>(offsetof(ObjectiveCompleteSystem_t9DE671D733D2983D240B650DF86180F97E2D2229, ____context_7)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_7() const { return ____context_7; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_7() { return &____context_7; }
	inline void set__context_7(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_7 = value;
		Il2CppCodeGenWriteBarrier((&____context_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVECOMPLETESYSTEM_T9DE671D733D2983D240B650DF86180F97E2D2229_H
#ifndef OBJECTIVEDESTROYABLEDECREMENTERSYSTEM_TC6EB762B99BA92F802D11C144F3DEC193C991D81_H
#define OBJECTIVEDESTROYABLEDECREMENTERSYSTEM_TC6EB762B99BA92F802D11C144F3DEC193C991D81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectiveDestroyableDecrementerSystem
struct  ObjectiveDestroyableDecrementerSystem_tC6EB762B99BA92F802D11C144F3DEC193C991D81  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext ObjectiveDestroyableDecrementerSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(ObjectiveDestroyableDecrementerSystem_tC6EB762B99BA92F802D11C144F3DEC193C991D81, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVEDESTROYABLEDECREMENTERSYSTEM_TC6EB762B99BA92F802D11C144F3DEC193C991D81_H
#ifndef OBJECTIVETOWERSDECREMENTSYSTEM_T6E5588ED14611095B7732B0E85865AD886AC8E05_H
#define OBJECTIVETOWERSDECREMENTSYSTEM_T6E5588ED14611095B7732B0E85865AD886AC8E05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectiveTowersDecrementSystem
struct  ObjectiveTowersDecrementSystem_t6E5588ED14611095B7732B0E85865AD886AC8E05  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext ObjectiveTowersDecrementSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(ObjectiveTowersDecrementSystem_t6E5588ED14611095B7732B0E85865AD886AC8E05, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVETOWERSDECREMENTSYSTEM_T6E5588ED14611095B7732B0E85865AD886AC8E05_H
#ifndef SPELLSWARMUPTASK_T6EF33E8213F49AB4F88568DEB6BAE927C0D5240F_H
#define SPELLSWARMUPTASK_T6EF33E8213F49AB4F88568DEB6BAE927C0D5240F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellsWarmupTask
struct  SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F  : public BasicTask_1_tA96B338B10A9FF438D49FEEF0A97378798D99F57
{
public:
	// Zenject.DiContainer SpellsWarmupTask::_container
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____container_7;
	// UserVO SpellsWarmupTask::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_8;
	// ChaptersSO SpellsWarmupTask::_chaptersSO
	ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * ____chaptersSO_9;
	// UnitsSO SpellsWarmupTask::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_10;
	// TowersSO SpellsWarmupTask::_towersSO
	TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * ____towersSO_11;
	// SpellsSO SpellsWarmupTask::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_12;
	// System.Collections.Generic.Dictionary`2<System.String,AssetData> SpellsWarmupTask::_assets
	Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * ____assets_13;

public:
	inline static int32_t get_offset_of__container_7() { return static_cast<int32_t>(offsetof(SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F, ____container_7)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__container_7() const { return ____container_7; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__container_7() { return &____container_7; }
	inline void set__container_7(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____container_7 = value;
		Il2CppCodeGenWriteBarrier((&____container_7), value);
	}

	inline static int32_t get_offset_of__userVO_8() { return static_cast<int32_t>(offsetof(SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F, ____userVO_8)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_8() const { return ____userVO_8; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_8() { return &____userVO_8; }
	inline void set__userVO_8(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_8 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_8), value);
	}

	inline static int32_t get_offset_of__chaptersSO_9() { return static_cast<int32_t>(offsetof(SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F, ____chaptersSO_9)); }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * get__chaptersSO_9() const { return ____chaptersSO_9; }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 ** get_address_of__chaptersSO_9() { return &____chaptersSO_9; }
	inline void set__chaptersSO_9(ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * value)
	{
		____chaptersSO_9 = value;
		Il2CppCodeGenWriteBarrier((&____chaptersSO_9), value);
	}

	inline static int32_t get_offset_of__unitsSO_10() { return static_cast<int32_t>(offsetof(SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F, ____unitsSO_10)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_10() const { return ____unitsSO_10; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_10() { return &____unitsSO_10; }
	inline void set__unitsSO_10(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_10 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_10), value);
	}

	inline static int32_t get_offset_of__towersSO_11() { return static_cast<int32_t>(offsetof(SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F, ____towersSO_11)); }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * get__towersSO_11() const { return ____towersSO_11; }
	inline TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 ** get_address_of__towersSO_11() { return &____towersSO_11; }
	inline void set__towersSO_11(TowersSO_t3B7DE821E5F8017950776EEFC42E680B361BCDF0 * value)
	{
		____towersSO_11 = value;
		Il2CppCodeGenWriteBarrier((&____towersSO_11), value);
	}

	inline static int32_t get_offset_of__spellsSO_12() { return static_cast<int32_t>(offsetof(SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F, ____spellsSO_12)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_12() const { return ____spellsSO_12; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_12() { return &____spellsSO_12; }
	inline void set__spellsSO_12(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_12 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_12), value);
	}

	inline static int32_t get_offset_of__assets_13() { return static_cast<int32_t>(offsetof(SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F, ____assets_13)); }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * get__assets_13() const { return ____assets_13; }
	inline Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 ** get_address_of__assets_13() { return &____assets_13; }
	inline void set__assets_13(Dictionary_2_tE93A2B9ABDA72381551E41330AA05157E9F976C1 * value)
	{
		____assets_13 = value;
		Il2CppCodeGenWriteBarrier((&____assets_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLSWARMUPTASK_T6EF33E8213F49AB4F88568DEB6BAE927C0D5240F_H
#ifndef SPHERECOLLIDERDEBUGSYSTEM_T0C02548EEF82E9B4A6A61701F3268AB1C622B53A_H
#define SPHERECOLLIDERDEBUGSYSTEM_T0C02548EEF82E9B4A6A61701F3268AB1C622B53A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SphereColliderDebugSystem
struct  SphereColliderDebugSystem_t0C02548EEF82E9B4A6A61701F3268AB1C622B53A  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext SphereColliderDebugSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(SphereColliderDebugSystem_t0C02548EEF82E9B4A6A61701F3268AB1C622B53A, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPHERECOLLIDERDEBUGSYSTEM_T0C02548EEF82E9B4A6A61701F3268AB1C622B53A_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#define DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Zero_7)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___One_8)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_One_8() const { return ___One_8; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinusOne_9)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MaxValue_10)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinValue_11)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TSYSTEMATTRIBUTE_TC2CFB3389B6C848683AD9339E48CB651B017218B_H
#define TSYSTEMATTRIBUTE_TC2CFB3389B6C848683AD9339E48CB651B017218B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TSystemAttribute
struct  TSystemAttribute_tC2CFB3389B6C848683AD9339E48CB651B017218B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String Tayr.TSystemAttribute::_container
	String_t* ____container_0;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(TSystemAttribute_tC2CFB3389B6C848683AD9339E48CB651B017218B, ____container_0)); }
	inline String_t* get__container_0() const { return ____container_0; }
	inline String_t** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(String_t* value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TSYSTEMATTRIBUTE_TC2CFB3389B6C848683AD9339E48CB651B017218B_H
#ifndef VOSAVER_T8C114547CE0BD5068CE3D91A3CD858C48DD1A88F_H
#define VOSAVER_T8C114547CE0BD5068CE3D91A3CD858C48DD1A88F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.VOSaver
struct  VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F  : public BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Tayr.IBasicVO> Tayr.VOSaver::_dirtyVO
	Dictionary_2_t8196CB40B909BB3D81CA49FBE34F8CE343A54C74 * ____dirtyVO_3;

public:
	inline static int32_t get_offset_of__dirtyVO_3() { return static_cast<int32_t>(offsetof(VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F, ____dirtyVO_3)); }
	inline Dictionary_2_t8196CB40B909BB3D81CA49FBE34F8CE343A54C74 * get__dirtyVO_3() const { return ____dirtyVO_3; }
	inline Dictionary_2_t8196CB40B909BB3D81CA49FBE34F8CE343A54C74 ** get_address_of__dirtyVO_3() { return &____dirtyVO_3; }
	inline void set__dirtyVO_3(Dictionary_2_t8196CB40B909BB3D81CA49FBE34F8CE343A54C74 * value)
	{
		____dirtyVO_3 = value;
		Il2CppCodeGenWriteBarrier((&____dirtyVO_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOSAVER_T8C114547CE0BD5068CE3D91A3CD858C48DD1A88F_H
#ifndef TRAPRANGEDRAWER_T045EE3126EA63A557C3BC426F63CF00F3A6A6818_H
#define TRAPRANGEDRAWER_T045EE3126EA63A557C3BC426F63CF00F3A6A6818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TrapRangeDrawer
struct  TrapRangeDrawer_t045EE3126EA63A557C3BC426F63CF00F3A6A6818  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext TrapRangeDrawer::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(TrapRangeDrawer_t045EE3126EA63A557C3BC426F63CF00F3A6A6818, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRAPRANGEDRAWER_T045EE3126EA63A557C3BC426F63CF00F3A6A6818_H
#ifndef UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#define UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F_H
#ifndef UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#define UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct  UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T6DD758393B13FC2A58BE44E647D9EBEA4F27D914_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR2INT_T339DA203C037FA6BCFC926C36DC2194D52D5F905_H
#define VECTOR2INT_T339DA203C037FA6BCFC926C36DC2194D52D5F905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2Int
struct  Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 
{
public:
	// System.Int32 UnityEngine.Vector2Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector2Int::m_Y
	int32_t ___m_Y_1;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}
};

struct Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields
{
public:
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Zero
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Zero_2;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_One
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_One_3;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Up
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Up_4;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Down
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Down_5;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Left
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Left_6;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Right
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Right_7;

public:
	inline static int32_t get_offset_of_s_Zero_2() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Zero_2)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Zero_2() const { return ___s_Zero_2; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Zero_2() { return &___s_Zero_2; }
	inline void set_s_Zero_2(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Zero_2 = value;
	}

	inline static int32_t get_offset_of_s_One_3() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_One_3)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_One_3() const { return ___s_One_3; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_One_3() { return &___s_One_3; }
	inline void set_s_One_3(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_One_3 = value;
	}

	inline static int32_t get_offset_of_s_Up_4() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Up_4)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Up_4() const { return ___s_Up_4; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Up_4() { return &___s_Up_4; }
	inline void set_s_Up_4(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Up_4 = value;
	}

	inline static int32_t get_offset_of_s_Down_5() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Down_5)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Down_5() const { return ___s_Down_5; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Down_5() { return &___s_Down_5; }
	inline void set_s_Down_5(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Down_5 = value;
	}

	inline static int32_t get_offset_of_s_Left_6() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Left_6)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Left_6() const { return ___s_Left_6; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Left_6() { return &___s_Left_6; }
	inline void set_s_Left_6(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Left_6 = value;
	}

	inline static int32_t get_offset_of_s_Right_7() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Right_7)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Right_7() const { return ___s_Right_7; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Right_7() { return &___s_Right_7; }
	inline void set_s_Right_7(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Right_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2INT_T339DA203C037FA6BCFC926C36DC2194D52D5F905_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR3INT_TA843C5F8C2EB42492786C5AF82C3E1F4929942B4_H
#define VECTOR3INT_TA843C5F8C2EB42492786C5AF82C3E1F4929942B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3Int
struct  Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 
{
public:
	// System.Int32 UnityEngine.Vector3Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector3Int::m_Y
	int32_t ___m_Y_1;
	// System.Int32 UnityEngine.Vector3Int::m_Z
	int32_t ___m_Z_2;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}

	inline static int32_t get_offset_of_m_Z_2() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4, ___m_Z_2)); }
	inline int32_t get_m_Z_2() const { return ___m_Z_2; }
	inline int32_t* get_address_of_m_Z_2() { return &___m_Z_2; }
	inline void set_m_Z_2(int32_t value)
	{
		___m_Z_2 = value;
	}
};

struct Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields
{
public:
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Zero
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___s_Zero_3;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_One
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___s_One_4;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Up
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___s_Up_5;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Down
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___s_Down_6;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Left
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___s_Left_7;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Right
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___s_Right_8;

public:
	inline static int32_t get_offset_of_s_Zero_3() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields, ___s_Zero_3)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_s_Zero_3() const { return ___s_Zero_3; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_s_Zero_3() { return &___s_Zero_3; }
	inline void set_s_Zero_3(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___s_Zero_3 = value;
	}

	inline static int32_t get_offset_of_s_One_4() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields, ___s_One_4)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_s_One_4() const { return ___s_One_4; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_s_One_4() { return &___s_One_4; }
	inline void set_s_One_4(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___s_One_4 = value;
	}

	inline static int32_t get_offset_of_s_Up_5() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields, ___s_Up_5)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_s_Up_5() const { return ___s_Up_5; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_s_Up_5() { return &___s_Up_5; }
	inline void set_s_Up_5(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___s_Up_5 = value;
	}

	inline static int32_t get_offset_of_s_Down_6() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields, ___s_Down_6)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_s_Down_6() const { return ___s_Down_6; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_s_Down_6() { return &___s_Down_6; }
	inline void set_s_Down_6(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___s_Down_6 = value;
	}

	inline static int32_t get_offset_of_s_Left_7() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields, ___s_Left_7)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_s_Left_7() const { return ___s_Left_7; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_s_Left_7() { return &___s_Left_7; }
	inline void set_s_Left_7(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___s_Left_7 = value;
	}

	inline static int32_t get_offset_of_s_Right_8() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields, ___s_Right_8)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_s_Right_8() const { return ___s_Right_8; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_s_Right_8() { return &___s_Right_8; }
	inline void set_s_Right_8(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___s_Right_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3INT_TA843C5F8C2EB42492786C5AF82C3E1F4929942B4_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T13594387F3B15033150E0CB705727D78DB03377C_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T13594387F3B15033150E0CB705727D78DB03377C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t13594387F3B15033150E0CB705727D78DB03377C  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t13594387F3B15033150E0CB705727D78DB03377C_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D24 <PrivateImplementationDetails>::444C0B6D3BA44B0AEC9133CAA7B1EADF170635FD
	__StaticArrayInitTypeSizeU3D24_tA66DA9D905CC3FD5269CBA439F53227E088C5962  ___444C0B6D3BA44B0AEC9133CAA7B1EADF170635FD_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D28 <PrivateImplementationDetails>::D2FD55BEC43713E0126C440295414130C96CB848
	__StaticArrayInitTypeSizeU3D28_t9E81964A7111D1744E54D30D73684C823A4BCFB9  ___D2FD55BEC43713E0126C440295414130C96CB848_1;

public:
	inline static int32_t get_offset_of_U3444C0B6D3BA44B0AEC9133CAA7B1EADF170635FD_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t13594387F3B15033150E0CB705727D78DB03377C_StaticFields, ___444C0B6D3BA44B0AEC9133CAA7B1EADF170635FD_0)); }
	inline __StaticArrayInitTypeSizeU3D24_tA66DA9D905CC3FD5269CBA439F53227E088C5962  get_U3444C0B6D3BA44B0AEC9133CAA7B1EADF170635FD_0() const { return ___444C0B6D3BA44B0AEC9133CAA7B1EADF170635FD_0; }
	inline __StaticArrayInitTypeSizeU3D24_tA66DA9D905CC3FD5269CBA439F53227E088C5962 * get_address_of_U3444C0B6D3BA44B0AEC9133CAA7B1EADF170635FD_0() { return &___444C0B6D3BA44B0AEC9133CAA7B1EADF170635FD_0; }
	inline void set_U3444C0B6D3BA44B0AEC9133CAA7B1EADF170635FD_0(__StaticArrayInitTypeSizeU3D24_tA66DA9D905CC3FD5269CBA439F53227E088C5962  value)
	{
		___444C0B6D3BA44B0AEC9133CAA7B1EADF170635FD_0 = value;
	}

	inline static int32_t get_offset_of_D2FD55BEC43713E0126C440295414130C96CB848_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t13594387F3B15033150E0CB705727D78DB03377C_StaticFields, ___D2FD55BEC43713E0126C440295414130C96CB848_1)); }
	inline __StaticArrayInitTypeSizeU3D28_t9E81964A7111D1744E54D30D73684C823A4BCFB9  get_D2FD55BEC43713E0126C440295414130C96CB848_1() const { return ___D2FD55BEC43713E0126C440295414130C96CB848_1; }
	inline __StaticArrayInitTypeSizeU3D28_t9E81964A7111D1744E54D30D73684C823A4BCFB9 * get_address_of_D2FD55BEC43713E0126C440295414130C96CB848_1() { return &___D2FD55BEC43713E0126C440295414130C96CB848_1; }
	inline void set_D2FD55BEC43713E0126C440295414130C96CB848_1(__StaticArrayInitTypeSizeU3D28_t9E81964A7111D1744E54D30D73684C823A4BCFB9  value)
	{
		___D2FD55BEC43713E0126C440295414130C96CB848_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T13594387F3B15033150E0CB705727D78DB03377C_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T073529674E8DC05642E31D731B6277B8ECFB65C3_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T073529674E8DC05642E31D731B6277B8ECFB65C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t073529674E8DC05642E31D731B6277B8ECFB65C3  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t073529674E8DC05642E31D731B6277B8ECFB65C3_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D10 <PrivateImplementationDetails>::DFE8865BE0F6D289A72492F3B1D6DC9ED2A6C431
	__StaticArrayInitTypeSizeU3D10_tA63458BD8CED83456312F196CD9CE7931AA28BF5  ___DFE8865BE0F6D289A72492F3B1D6DC9ED2A6C431_0;

public:
	inline static int32_t get_offset_of_DFE8865BE0F6D289A72492F3B1D6DC9ED2A6C431_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t073529674E8DC05642E31D731B6277B8ECFB65C3_StaticFields, ___DFE8865BE0F6D289A72492F3B1D6DC9ED2A6C431_0)); }
	inline __StaticArrayInitTypeSizeU3D10_tA63458BD8CED83456312F196CD9CE7931AA28BF5  get_DFE8865BE0F6D289A72492F3B1D6DC9ED2A6C431_0() const { return ___DFE8865BE0F6D289A72492F3B1D6DC9ED2A6C431_0; }
	inline __StaticArrayInitTypeSizeU3D10_tA63458BD8CED83456312F196CD9CE7931AA28BF5 * get_address_of_DFE8865BE0F6D289A72492F3B1D6DC9ED2A6C431_0() { return &___DFE8865BE0F6D289A72492F3B1D6DC9ED2A6C431_0; }
	inline void set_DFE8865BE0F6D289A72492F3B1D6DC9ED2A6C431_0(__StaticArrayInitTypeSizeU3D10_tA63458BD8CED83456312F196CD9CE7931AA28BF5  value)
	{
		___DFE8865BE0F6D289A72492F3B1D6DC9ED2A6C431_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T073529674E8DC05642E31D731B6277B8ECFB65C3_H
#ifndef EXAMPLEPAGE_T7E7B6984293C4A76FA9F9569D2DB37AFE5792C13_H
#define EXAMPLEPAGE_T7E7B6984293C4A76FA9F9569D2DB37AFE5792C13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Examples.ExamplesGUI_ExamplePage
struct  ExamplePage_t7E7B6984293C4A76FA9F9569D2DB37AFE5792C13 
{
public:
	// System.Int32 CodeStage.AntiCheat.Examples.ExamplesGUI_ExamplePage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExamplePage_t7E7B6984293C4A76FA9F9569D2DB37AFE5792C13, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEPAGE_T7E7B6984293C4A76FA9F9569D2DB37AFE5792C13_H
#ifndef OBSCUREDDECIMAL_T8FDFD59D7553624B2DC58962B8254E274B6E301C_H
#define OBSCUREDDECIMAL_T8FDFD59D7553624B2DC58962B8254E274B6E301C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal
struct  ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C 
{
public:
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::currentCryptoKey
	int64_t ___currentCryptoKey_0;
	// CodeStage.AntiCheat.Common.ACTkByte16 CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::hiddenValue
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::inited
	bool ___inited_2;
	// System.Decimal CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::fakeValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C, ___currentCryptoKey_0)); }
	inline int64_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline int64_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(int64_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C, ___hiddenValue_1)); }
	inline ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340 * get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C, ___fakeValue_3)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_fakeValue_3() const { return ___fakeValue_3; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal
struct ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C_marshaled_pinvoke
{
	int64_t ___currentCryptoKey_0;
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  ___hiddenValue_1;
	int32_t ___inited_2;
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal
struct ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C_marshaled_com
{
	int64_t ___currentCryptoKey_0;
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  ___hiddenValue_1;
	int32_t ___inited_2;
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDDECIMAL_T8FDFD59D7553624B2DC58962B8254E274B6E301C_H
#ifndef OBSCUREDDOUBLE_T482D3EFF59CAD02A1890E031E5215115ED10F71B_H
#define OBSCUREDDOUBLE_T482D3EFF59CAD02A1890E031E5215115ED10F71B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble
struct  ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B 
{
public:
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble::currentCryptoKey
	int64_t ___currentCryptoKey_0;
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble::hiddenValue
	int64_t ___hiddenValue_1;
	// CodeStage.AntiCheat.Common.ACTkByte8 CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble::hiddenValueOldByte8
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  ___hiddenValueOldByte8_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble::inited
	bool ___inited_3;
	// System.Double CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble::fakeValue
	double ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble::fakeValueActive
	bool ___fakeValueActive_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B, ___currentCryptoKey_0)); }
	inline int64_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline int64_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(int64_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B, ___hiddenValue_1)); }
	inline int64_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline int64_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(int64_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValueOldByte8_2() { return static_cast<int32_t>(offsetof(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B, ___hiddenValueOldByte8_2)); }
	inline ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  get_hiddenValueOldByte8_2() const { return ___hiddenValueOldByte8_2; }
	inline ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E * get_address_of_hiddenValueOldByte8_2() { return &___hiddenValueOldByte8_2; }
	inline void set_hiddenValueOldByte8_2(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  value)
	{
		___hiddenValueOldByte8_2 = value;
	}

	inline static int32_t get_offset_of_inited_3() { return static_cast<int32_t>(offsetof(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B, ___inited_3)); }
	inline bool get_inited_3() const { return ___inited_3; }
	inline bool* get_address_of_inited_3() { return &___inited_3; }
	inline void set_inited_3(bool value)
	{
		___inited_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B, ___fakeValue_4)); }
	inline double get_fakeValue_4() const { return ___fakeValue_4; }
	inline double* get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(double value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_5() { return static_cast<int32_t>(offsetof(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B, ___fakeValueActive_5)); }
	inline bool get_fakeValueActive_5() const { return ___fakeValueActive_5; }
	inline bool* get_address_of_fakeValueActive_5() { return &___fakeValueActive_5; }
	inline void set_fakeValueActive_5(bool value)
	{
		___fakeValueActive_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble
struct ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B_marshaled_pinvoke
{
	int64_t ___currentCryptoKey_0;
	int64_t ___hiddenValue_1;
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  ___hiddenValueOldByte8_2;
	int32_t ___inited_3;
	double ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble
struct ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B_marshaled_com
{
	int64_t ___currentCryptoKey_0;
	int64_t ___hiddenValue_1;
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  ___hiddenValueOldByte8_2;
	int32_t ___inited_3;
	double ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
#endif // OBSCUREDDOUBLE_T482D3EFF59CAD02A1890E031E5215115ED10F71B_H
#ifndef OBSCUREDFLOAT_T4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077_H
#define OBSCUREDFLOAT_T4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat
struct  ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::currentCryptoKey
	int32_t ___currentCryptoKey_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::hiddenValue
	int32_t ___hiddenValue_1;
	// CodeStage.AntiCheat.Common.ACTkByte4 CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::hiddenValueOldByte4
	ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  ___hiddenValueOldByte4_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::inited
	bool ___inited_3;
	// System.Single CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::fakeValue
	float ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::fakeValueActive
	bool ___fakeValueActive_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077, ___currentCryptoKey_0)); }
	inline int32_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline int32_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(int32_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077, ___hiddenValue_1)); }
	inline int32_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline int32_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(int32_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValueOldByte4_2() { return static_cast<int32_t>(offsetof(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077, ___hiddenValueOldByte4_2)); }
	inline ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  get_hiddenValueOldByte4_2() const { return ___hiddenValueOldByte4_2; }
	inline ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE * get_address_of_hiddenValueOldByte4_2() { return &___hiddenValueOldByte4_2; }
	inline void set_hiddenValueOldByte4_2(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  value)
	{
		___hiddenValueOldByte4_2 = value;
	}

	inline static int32_t get_offset_of_inited_3() { return static_cast<int32_t>(offsetof(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077, ___inited_3)); }
	inline bool get_inited_3() const { return ___inited_3; }
	inline bool* get_address_of_inited_3() { return &___inited_3; }
	inline void set_inited_3(bool value)
	{
		___inited_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077, ___fakeValue_4)); }
	inline float get_fakeValue_4() const { return ___fakeValue_4; }
	inline float* get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(float value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_5() { return static_cast<int32_t>(offsetof(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077, ___fakeValueActive_5)); }
	inline bool get_fakeValueActive_5() const { return ___fakeValueActive_5; }
	inline bool* get_address_of_fakeValueActive_5() { return &___fakeValueActive_5; }
	inline void set_fakeValueActive_5(bool value)
	{
		___fakeValueActive_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat
struct ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  ___hiddenValueOldByte4_2;
	int32_t ___inited_3;
	float ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat
struct ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077_marshaled_com
{
	int32_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  ___hiddenValueOldByte4_2;
	int32_t ___inited_3;
	float ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
#endif // OBSCUREDFLOAT_T4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077_H
#ifndef OBSCUREDVECTOR2_T11B55619F5729BB5B30E922263095D6D862D5855_H
#define OBSCUREDVECTOR2_T11B55619F5729BB5B30E922263095D6D862D5855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2
struct  ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2::currentCryptoKey
	int32_t ___currentCryptoKey_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2_RawEncryptedVector2 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2::hiddenValue
	RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC  ___hiddenValue_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2::inited
	bool ___inited_3;
	// UnityEngine.Vector2 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2::fakeValue
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2::fakeValueActive
	bool ___fakeValueActive_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855, ___currentCryptoKey_1)); }
	inline int32_t get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline int32_t* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(int32_t value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855, ___hiddenValue_2)); }
	inline RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC  get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC * get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC  value)
	{
		___hiddenValue_2 = value;
	}

	inline static int32_t get_offset_of_inited_3() { return static_cast<int32_t>(offsetof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855, ___inited_3)); }
	inline bool get_inited_3() const { return ___inited_3; }
	inline bool* get_address_of_inited_3() { return &___inited_3; }
	inline void set_inited_3(bool value)
	{
		___inited_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855, ___fakeValue_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_fakeValue_4() const { return ___fakeValue_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_5() { return static_cast<int32_t>(offsetof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855, ___fakeValueActive_5)); }
	inline bool get_fakeValueActive_5() const { return ___fakeValueActive_5; }
	inline bool* get_address_of_fakeValueActive_5() { return &___fakeValueActive_5; }
	inline void set_fakeValueActive_5(bool value)
	{
		___fakeValueActive_5 = value;
	}
};

struct ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855_StaticFields
{
public:
	// UnityEngine.Vector2 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2::Zero
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855_StaticFields, ___Zero_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Zero_0() const { return ___Zero_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2
struct ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2
struct ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855_marshaled_com
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
#endif // OBSCUREDVECTOR2_T11B55619F5729BB5B30E922263095D6D862D5855_H
#ifndef OBSCUREDVECTOR2INT_T195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_H
#define OBSCUREDVECTOR2INT_T195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int
struct  ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int::currentCryptoKey
	int32_t ___currentCryptoKey_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int_RawEncryptedVector2Int CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int::hiddenValue
	RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D  ___hiddenValue_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int::inited
	bool ___inited_3;
	// UnityEngine.Vector2Int CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int::fakeValue
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int::fakeValueActive
	bool ___fakeValueActive_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A, ___currentCryptoKey_1)); }
	inline int32_t get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline int32_t* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(int32_t value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A, ___hiddenValue_2)); }
	inline RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D  get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D * get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D  value)
	{
		___hiddenValue_2 = value;
	}

	inline static int32_t get_offset_of_inited_3() { return static_cast<int32_t>(offsetof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A, ___inited_3)); }
	inline bool get_inited_3() const { return ___inited_3; }
	inline bool* get_address_of_inited_3() { return &___inited_3; }
	inline void set_inited_3(bool value)
	{
		___inited_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A, ___fakeValue_4)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_fakeValue_4() const { return ___fakeValue_4; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_5() { return static_cast<int32_t>(offsetof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A, ___fakeValueActive_5)); }
	inline bool get_fakeValueActive_5() const { return ___fakeValueActive_5; }
	inline bool* get_address_of_fakeValueActive_5() { return &___fakeValueActive_5; }
	inline void set_fakeValueActive_5(bool value)
	{
		___fakeValueActive_5 = value;
	}
};

struct ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_StaticFields
{
public:
	// UnityEngine.Vector2Int CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int::Zero
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_StaticFields, ___Zero_0)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_Zero_0() const { return ___Zero_0; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int
struct ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int
struct ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_marshaled_com
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
#endif // OBSCUREDVECTOR2INT_T195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_H
#ifndef OBSCUREDVECTOR3_T78946FDA503DFD5D6975074F4408EDCF397E1781_H
#define OBSCUREDVECTOR3_T78946FDA503DFD5D6975074F4408EDCF397E1781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3
struct  ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3::currentCryptoKey
	int32_t ___currentCryptoKey_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3::hiddenValue
	RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627  ___hiddenValue_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3::inited
	bool ___inited_3;
	// UnityEngine.Vector3 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3::fakeValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3::fakeValueActive
	bool ___fakeValueActive_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781, ___currentCryptoKey_1)); }
	inline int32_t get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline int32_t* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(int32_t value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781, ___hiddenValue_2)); }
	inline RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627  get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627 * get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627  value)
	{
		___hiddenValue_2 = value;
	}

	inline static int32_t get_offset_of_inited_3() { return static_cast<int32_t>(offsetof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781, ___inited_3)); }
	inline bool get_inited_3() const { return ___inited_3; }
	inline bool* get_address_of_inited_3() { return &___inited_3; }
	inline void set_inited_3(bool value)
	{
		___inited_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781, ___fakeValue_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_fakeValue_4() const { return ___fakeValue_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_5() { return static_cast<int32_t>(offsetof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781, ___fakeValueActive_5)); }
	inline bool get_fakeValueActive_5() const { return ___fakeValueActive_5; }
	inline bool* get_address_of_fakeValueActive_5() { return &___fakeValueActive_5; }
	inline void set_fakeValueActive_5(bool value)
	{
		___fakeValueActive_5 = value;
	}
};

struct ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781_StaticFields
{
public:
	// UnityEngine.Vector3 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3::Zero
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781_StaticFields, ___Zero_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Zero_0() const { return ___Zero_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3
struct ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3
struct ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781_marshaled_com
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
#endif // OBSCUREDVECTOR3_T78946FDA503DFD5D6975074F4408EDCF397E1781_H
#ifndef OBSCUREDVECTOR3INT_T4E47D68D29A1D228D29EC0300213FEB7D891C0BD_H
#define OBSCUREDVECTOR3INT_T4E47D68D29A1D228D29EC0300213FEB7D891C0BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int
struct  ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int::currentCryptoKey
	int32_t ___currentCryptoKey_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int_RawEncryptedVector3Int CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int::hiddenValue
	RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3  ___hiddenValue_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int::inited
	bool ___inited_3;
	// UnityEngine.Vector3Int CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int::fakeValue
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int::fakeValueActive
	bool ___fakeValueActive_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD, ___currentCryptoKey_1)); }
	inline int32_t get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline int32_t* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(int32_t value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD, ___hiddenValue_2)); }
	inline RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3  get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3 * get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3  value)
	{
		___hiddenValue_2 = value;
	}

	inline static int32_t get_offset_of_inited_3() { return static_cast<int32_t>(offsetof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD, ___inited_3)); }
	inline bool get_inited_3() const { return ___inited_3; }
	inline bool* get_address_of_inited_3() { return &___inited_3; }
	inline void set_inited_3(bool value)
	{
		___inited_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD, ___fakeValue_4)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_fakeValue_4() const { return ___fakeValue_4; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_5() { return static_cast<int32_t>(offsetof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD, ___fakeValueActive_5)); }
	inline bool get_fakeValueActive_5() const { return ___fakeValueActive_5; }
	inline bool* get_address_of_fakeValueActive_5() { return &___fakeValueActive_5; }
	inline void set_fakeValueActive_5(bool value)
	{
		___fakeValueActive_5 = value;
	}
};

struct ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD_StaticFields
{
public:
	// UnityEngine.Vector3Int CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int::Zero
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD_StaticFields, ___Zero_0)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_Zero_0() const { return ___Zero_0; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int
struct ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int
struct ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD_marshaled_com
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
#endif // OBSCUREDVECTOR3INT_T4E47D68D29A1D228D29EC0300213FEB7D891C0BD_H
#ifndef DEVICELOCKLEVEL_T343109F2BB7200998C3CB19E58B09AA719CA84C0_H
#define DEVICELOCKLEVEL_T343109F2BB7200998C3CB19E58B09AA719CA84C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Storage.ObscuredPrefs_DeviceLockLevel
struct  DeviceLockLevel_t343109F2BB7200998C3CB19E58B09AA719CA84C0 
{
public:
	// System.Byte CodeStage.AntiCheat.Storage.ObscuredPrefs_DeviceLockLevel::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeviceLockLevel_t343109F2BB7200998C3CB19E58B09AA719CA84C0, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICELOCKLEVEL_T343109F2BB7200998C3CB19E58B09AA719CA84C0_H
#ifndef ERTUGRULFEATURE_TC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484_H
#define ERTUGRULFEATURE_TC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ErtugrulFeature
struct  ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484  : public Feature_t3D2CE4A6E0A21FB131F20D2F64B7A6C289F0CF0F
{
public:
	// Zenject.DiContainer ErtugrulFeature::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_4;
	// Contexts ErtugrulFeature::_contexts
	Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * ____contexts_5;

public:
	inline static int32_t get_offset_of__diContainer_4() { return static_cast<int32_t>(offsetof(ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484, ____diContainer_4)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_4() const { return ____diContainer_4; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_4() { return &____diContainer_4; }
	inline void set__diContainer_4(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_4 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_4), value);
	}

	inline static int32_t get_offset_of__contexts_5() { return static_cast<int32_t>(offsetof(ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484, ____contexts_5)); }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * get__contexts_5() const { return ____contexts_5; }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D ** get_address_of__contexts_5() { return &____contexts_5; }
	inline void set__contexts_5(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * value)
	{
		____contexts_5 = value;
		Il2CppCodeGenWriteBarrier((&____contexts_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERTUGRULFEATURE_TC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484_H
#ifndef JOYSTICKMODE_T4D81E7222D27377A127289ADE6BE889EEBB8E064_H
#define JOYSTICKMODE_T4D81E7222D27377A127289ADE6BE889EEBB8E064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoystickMode
struct  JoystickMode_t4D81E7222D27377A127289ADE6BE889EEBB8E064 
{
public:
	// System.Int32 JoystickMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JoystickMode_t4D81E7222D27377A127289ADE6BE889EEBB8E064, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKMODE_T4D81E7222D27377A127289ADE6BE889EEBB8E064_H
#ifndef OBJECTIVECOUNTERCOMPONENT_TF274AC62BA9337BA17F16505BA1D3468AF527FBF_H
#define OBJECTIVECOUNTERCOMPONENT_TF274AC62BA9337BA17F16505BA1D3468AF527FBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectiveCounterComponent
struct  ObjectiveCounterComponent_tF274AC62BA9337BA17F16505BA1D3468AF527FBF  : public RuntimeObject
{
public:
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt ObjectiveCounterComponent::Count
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  ___Count_0;

public:
	inline static int32_t get_offset_of_Count_0() { return static_cast<int32_t>(offsetof(ObjectiveCounterComponent_tF274AC62BA9337BA17F16505BA1D3468AF527FBF, ___Count_0)); }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  get_Count_0() const { return ___Count_0; }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489 * get_address_of_Count_0() { return &___Count_0; }
	inline void set_Count_0(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  value)
	{
		___Count_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVECOUNTERCOMPONENT_TF274AC62BA9337BA17F16505BA1D3468AF527FBF_H
#ifndef OBJECTIVETYPE_TA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28_H
#define OBJECTIVETYPE_TA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectiveType
struct  ObjectiveType_tA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28 
{
public:
	// System.Int32 ObjectiveType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectiveType_tA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVETYPE_TA6A9DA7F8810313B6E359E2D2B8B619E4E1BEE28_H
#ifndef ONTRIGGEREVENT_T77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019_H
#define ONTRIGGEREVENT_T77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.OnTriggerEvent
struct  OnTriggerEvent_t77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGEREVENT_T77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019_H
#ifndef OPERATOR_TAC7D6EA07F38B0B43477FE21F97E9654F61D3A0F_H
#define OPERATOR_TAC7D6EA07F38B0B43477FE21F97E9654F61D3A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.Operator
struct  Operator_tAC7D6EA07F38B0B43477FE21F97E9654F61D3A0F 
{
public:
	// System.Int32 Tayr.Operator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Operator_tAC7D6EA07F38B0B43477FE21F97E9654F61D3A0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOR_TAC7D6EA07F38B0B43477FE21F97E9654F61D3A0F_H
#ifndef REFREAHTYPE_TD4E50A740AAB4EA48BFF2B426369C53D543441DE_H
#define REFREAHTYPE_TD4E50A740AAB4EA48BFF2B426369C53D543441DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.RefreahType
struct  RefreahType_tD4E50A740AAB4EA48BFF2B426369C53D543441DE 
{
public:
	// System.Int32 Tayr.RefreahType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RefreahType_tD4E50A740AAB4EA48BFF2B426369C53D543441DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFREAHTYPE_TD4E50A740AAB4EA48BFF2B426369C53D543441DE_H
#ifndef TIMEUTILS_TBDFFCBF579233DEEA3761D9C63B8D7E28E18D888_H
#define TIMEUTILS_TBDFFCBF579233DEEA3761D9C63B8D7E28E18D888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TimeUtils
struct  TimeUtils_tBDFFCBF579233DEEA3761D9C63B8D7E28E18D888  : public RuntimeObject
{
public:

public:
};

struct TimeUtils_tBDFFCBF579233DEEA3761D9C63B8D7E28E18D888_StaticFields
{
public:
	// System.DateTime Tayr.TimeUtils::EPOCH
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___EPOCH_0;

public:
	inline static int32_t get_offset_of_EPOCH_0() { return static_cast<int32_t>(offsetof(TimeUtils_tBDFFCBF579233DEEA3761D9C63B8D7E28E18D888_StaticFields, ___EPOCH_0)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_EPOCH_0() const { return ___EPOCH_0; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_EPOCH_0() { return &___EPOCH_0; }
	inline void set_EPOCH_0(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___EPOCH_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEUTILS_TBDFFCBF579233DEEA3761D9C63B8D7E28E18D888_H
#ifndef VARIABLECHANGEDEVENT_T553579619C432FFE7C7636199CE57457FA699BA4_H
#define VARIABLECHANGEDEVENT_T553579619C432FFE7C7636199CE57457FA699BA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.VariableChangedEvent
struct  VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4  : public UnityEvent_1_t6DD758393B13FC2A58BE44E647D9EBEA4F27D914
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLECHANGEDEVENT_T553579619C432FFE7C7636199CE57457FA699BA4_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef SYSTEMLANGUAGE_TDFC6112B5AB6A51D92EFFA0FD9BE2F35E7359ED0_H
#define SYSTEMLANGUAGE_TDFC6112B5AB6A51D92EFFA0FD9BE2F35E7359ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SystemLanguage
struct  SystemLanguage_tDFC6112B5AB6A51D92EFFA0FD9BE2F35E7359ED0 
{
public:
	// System.Int32 UnityEngine.SystemLanguage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SystemLanguage_tDFC6112B5AB6A51D92EFFA0FD9BE2F35E7359ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMLANGUAGE_TDFC6112B5AB6A51D92EFFA0FD9BE2F35E7359ED0_H
#ifndef DEBUGFEATURE_T7C4592F930C1C0E5557E9FFE5E27A3B6A708EF75_H
#define DEBUGFEATURE_T7C4592F930C1C0E5557E9FFE5E27A3B6A708EF75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DebugFeature
struct  DebugFeature_t7C4592F930C1C0E5557E9FFE5E27A3B6A708EF75  : public ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGFEATURE_T7C4592F930C1C0E5557E9FFE5E27A3B6A708EF75_H
#ifndef GAMEMODEFEATURE_T26E5E2E6B53B52AFFBC416103BF675284F1F833C_H
#define GAMEMODEFEATURE_T26E5E2E6B53B52AFFBC416103BF675284F1F833C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameModeFeature
struct  GameModeFeature_t26E5E2E6B53B52AFFBC416103BF675284F1F833C  : public ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484
{
public:
	// FeaturesSwitchConfig GameModeFeature::_featuresSwitchConfig
	FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3 * ____featuresSwitchConfig_6;

public:
	inline static int32_t get_offset_of__featuresSwitchConfig_6() { return static_cast<int32_t>(offsetof(GameModeFeature_t26E5E2E6B53B52AFFBC416103BF675284F1F833C, ____featuresSwitchConfig_6)); }
	inline FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3 * get__featuresSwitchConfig_6() const { return ____featuresSwitchConfig_6; }
	inline FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3 ** get_address_of__featuresSwitchConfig_6() { return &____featuresSwitchConfig_6; }
	inline void set__featuresSwitchConfig_6(FeaturesSwitchConfig_tBF1848651D45C05C78F36D68CB647F5CB2F802D3 * value)
	{
		____featuresSwitchConfig_6 = value;
		Il2CppCodeGenWriteBarrier((&____featuresSwitchConfig_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMODEFEATURE_T26E5E2E6B53B52AFFBC416103BF675284F1F833C_H
#ifndef INPUTFEATURE_T5A0C995F504E269254656351086E21C0FAB312AC_H
#define INPUTFEATURE_T5A0C995F504E269254656351086E21C0FAB312AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputFeature
struct  InputFeature_t5A0C995F504E269254656351086E21C0FAB312AC  : public ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTFEATURE_T5A0C995F504E269254656351086E21C0FAB312AC_H
#ifndef OBJECTIVETYPECOMPONENT_T8DC8BD077A26EC2C7532C777F18D592DA64081C1_H
#define OBJECTIVETYPECOMPONENT_T8DC8BD077A26EC2C7532C777F18D592DA64081C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectiveTypeComponent
struct  ObjectiveTypeComponent_t8DC8BD077A26EC2C7532C777F18D592DA64081C1  : public RuntimeObject
{
public:
	// ObjectiveType ObjectiveTypeComponent::ObjectiveType
	int32_t ___ObjectiveType_0;

public:
	inline static int32_t get_offset_of_ObjectiveType_0() { return static_cast<int32_t>(offsetof(ObjectiveTypeComponent_t8DC8BD077A26EC2C7532C777F18D592DA64081C1, ___ObjectiveType_0)); }
	inline int32_t get_ObjectiveType_0() const { return ___ObjectiveType_0; }
	inline int32_t* get_address_of_ObjectiveType_0() { return &___ObjectiveType_0; }
	inline void set_ObjectiveType_0(int32_t value)
	{
		___ObjectiveType_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTIVETYPECOMPONENT_T8DC8BD077A26EC2C7532C777F18D592DA64081C1_H
#ifndef TRIGGER_TFA4DAF5E2FB996087C1F5DD53203E250D4770C09_H
#define TRIGGER_TFA4DAF5E2FB996087C1F5DD53203E250D4770C09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.Trigger
struct  Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09  : public RuntimeObject
{
public:
	// Tayr.OnTriggerEvent Tayr.Trigger::_onTriggerEvent
	OnTriggerEvent_t77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019 * ____onTriggerEvent_0;
	// Tayr.OnTriggerEvent Tayr.Trigger::_onUpdateEvent
	OnTriggerEvent_t77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019 * ____onUpdateEvent_1;
	// Tayr.Operator Tayr.Trigger::_operator
	int32_t ____operator_2;
	// System.Int32 Tayr.Trigger::_target
	int32_t ____target_3;
	// Tayr.Variable Tayr.Trigger::_variable
	Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD * ____variable_4;
	// System.Boolean Tayr.Trigger::_isTriggered
	bool ____isTriggered_5;
	// System.String Tayr.Trigger::Name
	String_t* ___Name_6;

public:
	inline static int32_t get_offset_of__onTriggerEvent_0() { return static_cast<int32_t>(offsetof(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09, ____onTriggerEvent_0)); }
	inline OnTriggerEvent_t77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019 * get__onTriggerEvent_0() const { return ____onTriggerEvent_0; }
	inline OnTriggerEvent_t77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019 ** get_address_of__onTriggerEvent_0() { return &____onTriggerEvent_0; }
	inline void set__onTriggerEvent_0(OnTriggerEvent_t77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019 * value)
	{
		____onTriggerEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&____onTriggerEvent_0), value);
	}

	inline static int32_t get_offset_of__onUpdateEvent_1() { return static_cast<int32_t>(offsetof(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09, ____onUpdateEvent_1)); }
	inline OnTriggerEvent_t77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019 * get__onUpdateEvent_1() const { return ____onUpdateEvent_1; }
	inline OnTriggerEvent_t77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019 ** get_address_of__onUpdateEvent_1() { return &____onUpdateEvent_1; }
	inline void set__onUpdateEvent_1(OnTriggerEvent_t77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019 * value)
	{
		____onUpdateEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&____onUpdateEvent_1), value);
	}

	inline static int32_t get_offset_of__operator_2() { return static_cast<int32_t>(offsetof(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09, ____operator_2)); }
	inline int32_t get__operator_2() const { return ____operator_2; }
	inline int32_t* get_address_of__operator_2() { return &____operator_2; }
	inline void set__operator_2(int32_t value)
	{
		____operator_2 = value;
	}

	inline static int32_t get_offset_of__target_3() { return static_cast<int32_t>(offsetof(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09, ____target_3)); }
	inline int32_t get__target_3() const { return ____target_3; }
	inline int32_t* get_address_of__target_3() { return &____target_3; }
	inline void set__target_3(int32_t value)
	{
		____target_3 = value;
	}

	inline static int32_t get_offset_of__variable_4() { return static_cast<int32_t>(offsetof(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09, ____variable_4)); }
	inline Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD * get__variable_4() const { return ____variable_4; }
	inline Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD ** get_address_of__variable_4() { return &____variable_4; }
	inline void set__variable_4(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD * value)
	{
		____variable_4 = value;
		Il2CppCodeGenWriteBarrier((&____variable_4), value);
	}

	inline static int32_t get_offset_of__isTriggered_5() { return static_cast<int32_t>(offsetof(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09, ____isTriggered_5)); }
	inline bool get__isTriggered_5() const { return ____isTriggered_5; }
	inline bool* get_address_of__isTriggered_5() { return &____isTriggered_5; }
	inline void set__isTriggered_5(bool value)
	{
		____isTriggered_5 = value;
	}

	inline static int32_t get_offset_of_Name_6() { return static_cast<int32_t>(offsetof(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09, ___Name_6)); }
	inline String_t* get_Name_6() const { return ___Name_6; }
	inline String_t** get_address_of_Name_6() { return &___Name_6; }
	inline void set_Name_6(String_t* value)
	{
		___Name_6 = value;
		Il2CppCodeGenWriteBarrier((&___Name_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGER_TFA4DAF5E2FB996087C1F5DD53203E250D4770C09_H
#ifndef TRIGGERMODEL_T09B711CC906A9A76F767318AF6754B20F253399E_H
#define TRIGGERMODEL_T09B711CC906A9A76F767318AF6754B20F253399E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TriggerModel
struct  TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E  : public RuntimeObject
{
public:
	// Tayr.Operator Tayr.TriggerModel::Operator
	int32_t ___Operator_2;
	// System.Int32 Tayr.TriggerModel::Target
	int32_t ___Target_3;
	// System.String Tayr.TriggerModel::VariableAlias
	String_t* ___VariableAlias_4;
	// Tayr.Variable Tayr.TriggerModel::Variable
	Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD * ___Variable_5;
	// Tayr.Trigger Tayr.TriggerModel::Trigger
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * ___Trigger_6;

public:
	inline static int32_t get_offset_of_Operator_2() { return static_cast<int32_t>(offsetof(TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E, ___Operator_2)); }
	inline int32_t get_Operator_2() const { return ___Operator_2; }
	inline int32_t* get_address_of_Operator_2() { return &___Operator_2; }
	inline void set_Operator_2(int32_t value)
	{
		___Operator_2 = value;
	}

	inline static int32_t get_offset_of_Target_3() { return static_cast<int32_t>(offsetof(TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E, ___Target_3)); }
	inline int32_t get_Target_3() const { return ___Target_3; }
	inline int32_t* get_address_of_Target_3() { return &___Target_3; }
	inline void set_Target_3(int32_t value)
	{
		___Target_3 = value;
	}

	inline static int32_t get_offset_of_VariableAlias_4() { return static_cast<int32_t>(offsetof(TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E, ___VariableAlias_4)); }
	inline String_t* get_VariableAlias_4() const { return ___VariableAlias_4; }
	inline String_t** get_address_of_VariableAlias_4() { return &___VariableAlias_4; }
	inline void set_VariableAlias_4(String_t* value)
	{
		___VariableAlias_4 = value;
		Il2CppCodeGenWriteBarrier((&___VariableAlias_4), value);
	}

	inline static int32_t get_offset_of_Variable_5() { return static_cast<int32_t>(offsetof(TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E, ___Variable_5)); }
	inline Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD * get_Variable_5() const { return ___Variable_5; }
	inline Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD ** get_address_of_Variable_5() { return &___Variable_5; }
	inline void set_Variable_5(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD * value)
	{
		___Variable_5 = value;
		Il2CppCodeGenWriteBarrier((&___Variable_5), value);
	}

	inline static int32_t get_offset_of_Trigger_6() { return static_cast<int32_t>(offsetof(TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E, ___Trigger_6)); }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * get_Trigger_6() const { return ___Trigger_6; }
	inline Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 ** get_address_of_Trigger_6() { return &___Trigger_6; }
	inline void set_Trigger_6(Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09 * value)
	{
		___Trigger_6 = value;
		Il2CppCodeGenWriteBarrier((&___Trigger_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMODEL_T09B711CC906A9A76F767318AF6754B20F253399E_H
#ifndef VARIABLE_T5D003723AEAA82A1E36B619BD98E937C05793BFD_H
#define VARIABLE_T5D003723AEAA82A1E36B619BD98E937C05793BFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.Variable
struct  Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD  : public RuntimeObject
{
public:
	// Tayr.VariableChangedEvent Tayr.Variable::_onVariableChanged
	VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 * ____onVariableChanged_0;
	// System.Int32 Tayr.Variable::_value
	int32_t ____value_1;
	// Tayr.RefreahType Tayr.Variable::_refreshType
	int32_t ____refreshType_2;
	// System.Boolean Tayr.Variable::_isInitialized
	bool ____isInitialized_3;

public:
	inline static int32_t get_offset_of__onVariableChanged_0() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____onVariableChanged_0)); }
	inline VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 * get__onVariableChanged_0() const { return ____onVariableChanged_0; }
	inline VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 ** get_address_of__onVariableChanged_0() { return &____onVariableChanged_0; }
	inline void set__onVariableChanged_0(VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4 * value)
	{
		____onVariableChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&____onVariableChanged_0), value);
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____value_1)); }
	inline int32_t get__value_1() const { return ____value_1; }
	inline int32_t* get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(int32_t value)
	{
		____value_1 = value;
	}

	inline static int32_t get_offset_of__refreshType_2() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____refreshType_2)); }
	inline int32_t get__refreshType_2() const { return ____refreshType_2; }
	inline int32_t* get_address_of__refreshType_2() { return &____refreshType_2; }
	inline void set__refreshType_2(int32_t value)
	{
		____refreshType_2 = value;
	}

	inline static int32_t get_offset_of__isInitialized_3() { return static_cast<int32_t>(offsetof(Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD, ____isInitialized_3)); }
	inline bool get__isInitialized_3() const { return ____isInitialized_3; }
	inline bool* get_address_of__isInitialized_3() { return &____isInitialized_3; }
	inline void set__isInitialized_3(bool value)
	{
		____isInitialized_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLE_T5D003723AEAA82A1E36B619BD98E937C05793BFD_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef CODEHASHEXAMPLE_TFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1_H
#define CODEHASHEXAMPLE_TFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Examples.CodeHashExample
struct  CodeHashExample_tFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String CodeStage.AntiCheat.Examples.CodeHashExample::savedHash
	String_t* ___savedHash_4;
	// CodeStage.AntiCheat.Genuine.CodeHash.HashGeneratorResult CodeStage.AntiCheat.Examples.CodeHashExample::<LastResult>k__BackingField
	HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7 * ___U3CLastResultU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_savedHash_4() { return static_cast<int32_t>(offsetof(CodeHashExample_tFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1, ___savedHash_4)); }
	inline String_t* get_savedHash_4() const { return ___savedHash_4; }
	inline String_t** get_address_of_savedHash_4() { return &___savedHash_4; }
	inline void set_savedHash_4(String_t* value)
	{
		___savedHash_4 = value;
		Il2CppCodeGenWriteBarrier((&___savedHash_4), value);
	}

	inline static int32_t get_offset_of_U3CLastResultU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CodeHashExample_tFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1, ___U3CLastResultU3Ek__BackingField_5)); }
	inline HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7 * get_U3CLastResultU3Ek__BackingField_5() const { return ___U3CLastResultU3Ek__BackingField_5; }
	inline HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7 ** get_address_of_U3CLastResultU3Ek__BackingField_5() { return &___U3CLastResultU3Ek__BackingField_5; }
	inline void set_U3CLastResultU3Ek__BackingField_5(HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7 * value)
	{
		___U3CLastResultU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastResultU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEHASHEXAMPLE_TFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1_H
#ifndef DETECTORSEXAMPLES_T169C89A6581AB652682A19A0B3537B229482845C_H
#define DETECTORSEXAMPLES_T169C89A6581AB652682A19A0B3537B229482845C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Examples.DetectorsExamples
struct  DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean CodeStage.AntiCheat.Examples.DetectorsExamples::injectionDetected
	bool ___injectionDetected_4;
	// System.Boolean CodeStage.AntiCheat.Examples.DetectorsExamples::speedHackDetected
	bool ___speedHackDetected_5;
	// System.Boolean CodeStage.AntiCheat.Examples.DetectorsExamples::wrongTimeDetected
	bool ___wrongTimeDetected_6;
	// System.Boolean CodeStage.AntiCheat.Examples.DetectorsExamples::timeCheatingDetected
	bool ___timeCheatingDetected_7;
	// System.Boolean CodeStage.AntiCheat.Examples.DetectorsExamples::obscuredTypeCheatDetected
	bool ___obscuredTypeCheatDetected_8;
	// System.Boolean CodeStage.AntiCheat.Examples.DetectorsExamples::wallHackCheatDetected
	bool ___wallHackCheatDetected_9;

public:
	inline static int32_t get_offset_of_injectionDetected_4() { return static_cast<int32_t>(offsetof(DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C, ___injectionDetected_4)); }
	inline bool get_injectionDetected_4() const { return ___injectionDetected_4; }
	inline bool* get_address_of_injectionDetected_4() { return &___injectionDetected_4; }
	inline void set_injectionDetected_4(bool value)
	{
		___injectionDetected_4 = value;
	}

	inline static int32_t get_offset_of_speedHackDetected_5() { return static_cast<int32_t>(offsetof(DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C, ___speedHackDetected_5)); }
	inline bool get_speedHackDetected_5() const { return ___speedHackDetected_5; }
	inline bool* get_address_of_speedHackDetected_5() { return &___speedHackDetected_5; }
	inline void set_speedHackDetected_5(bool value)
	{
		___speedHackDetected_5 = value;
	}

	inline static int32_t get_offset_of_wrongTimeDetected_6() { return static_cast<int32_t>(offsetof(DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C, ___wrongTimeDetected_6)); }
	inline bool get_wrongTimeDetected_6() const { return ___wrongTimeDetected_6; }
	inline bool* get_address_of_wrongTimeDetected_6() { return &___wrongTimeDetected_6; }
	inline void set_wrongTimeDetected_6(bool value)
	{
		___wrongTimeDetected_6 = value;
	}

	inline static int32_t get_offset_of_timeCheatingDetected_7() { return static_cast<int32_t>(offsetof(DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C, ___timeCheatingDetected_7)); }
	inline bool get_timeCheatingDetected_7() const { return ___timeCheatingDetected_7; }
	inline bool* get_address_of_timeCheatingDetected_7() { return &___timeCheatingDetected_7; }
	inline void set_timeCheatingDetected_7(bool value)
	{
		___timeCheatingDetected_7 = value;
	}

	inline static int32_t get_offset_of_obscuredTypeCheatDetected_8() { return static_cast<int32_t>(offsetof(DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C, ___obscuredTypeCheatDetected_8)); }
	inline bool get_obscuredTypeCheatDetected_8() const { return ___obscuredTypeCheatDetected_8; }
	inline bool* get_address_of_obscuredTypeCheatDetected_8() { return &___obscuredTypeCheatDetected_8; }
	inline void set_obscuredTypeCheatDetected_8(bool value)
	{
		___obscuredTypeCheatDetected_8 = value;
	}

	inline static int32_t get_offset_of_wallHackCheatDetected_9() { return static_cast<int32_t>(offsetof(DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C, ___wallHackCheatDetected_9)); }
	inline bool get_wallHackCheatDetected_9() const { return ___wallHackCheatDetected_9; }
	inline bool* get_address_of_wallHackCheatDetected_9() { return &___wallHackCheatDetected_9; }
	inline void set_wallHackCheatDetected_9(bool value)
	{
		___wallHackCheatDetected_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTORSEXAMPLES_T169C89A6581AB652682A19A0B3537B229482845C_H
#ifndef EXAMPLESGUI_T724140363AAAC1BE52036C2F20F61A57D7D05D8C_H
#define EXAMPLESGUI_T724140363AAAC1BE52036C2F20F61A57D7D05D8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Examples.ExamplesGUI
struct  ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// CodeStage.AntiCheat.Examples.ObscuredTypesExamples CodeStage.AntiCheat.Examples.ExamplesGUI::obscuredTypesExamples
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA * ___obscuredTypesExamples_7;
	// CodeStage.AntiCheat.Examples.ObscuredPrefsExamples CodeStage.AntiCheat.Examples.ExamplesGUI::obscuredPrefsExamples
	ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063 * ___obscuredPrefsExamples_8;
	// CodeStage.AntiCheat.Examples.DetectorsExamples CodeStage.AntiCheat.Examples.ExamplesGUI::detectorsExamples
	DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C * ___detectorsExamples_9;
	// CodeStage.AntiCheat.Examples.CodeHashExample CodeStage.AntiCheat.Examples.ExamplesGUI::codeHashExample
	CodeHashExample_tFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1 * ___codeHashExample_10;
	// System.String[] CodeStage.AntiCheat.Examples.ExamplesGUI::tabs
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___tabs_11;
	// CodeStage.AntiCheat.Examples.ExamplesGUI_ExamplePage CodeStage.AntiCheat.Examples.ExamplesGUI::currentPage
	int32_t ___currentPage_12;
	// System.String CodeStage.AntiCheat.Examples.ExamplesGUI::allSimpleObscuredTypes
	String_t* ___allSimpleObscuredTypes_13;
	// CodeStage.AntiCheat.Storage.ObscuredPrefs_DeviceLockLevel CodeStage.AntiCheat.Examples.ExamplesGUI::savesLock
	uint8_t ___savesLock_14;
	// UnityEngine.GUIStyle CodeStage.AntiCheat.Examples.ExamplesGUI::centeredStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___centeredStyle_15;

public:
	inline static int32_t get_offset_of_obscuredTypesExamples_7() { return static_cast<int32_t>(offsetof(ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C, ___obscuredTypesExamples_7)); }
	inline ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA * get_obscuredTypesExamples_7() const { return ___obscuredTypesExamples_7; }
	inline ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA ** get_address_of_obscuredTypesExamples_7() { return &___obscuredTypesExamples_7; }
	inline void set_obscuredTypesExamples_7(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA * value)
	{
		___obscuredTypesExamples_7 = value;
		Il2CppCodeGenWriteBarrier((&___obscuredTypesExamples_7), value);
	}

	inline static int32_t get_offset_of_obscuredPrefsExamples_8() { return static_cast<int32_t>(offsetof(ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C, ___obscuredPrefsExamples_8)); }
	inline ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063 * get_obscuredPrefsExamples_8() const { return ___obscuredPrefsExamples_8; }
	inline ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063 ** get_address_of_obscuredPrefsExamples_8() { return &___obscuredPrefsExamples_8; }
	inline void set_obscuredPrefsExamples_8(ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063 * value)
	{
		___obscuredPrefsExamples_8 = value;
		Il2CppCodeGenWriteBarrier((&___obscuredPrefsExamples_8), value);
	}

	inline static int32_t get_offset_of_detectorsExamples_9() { return static_cast<int32_t>(offsetof(ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C, ___detectorsExamples_9)); }
	inline DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C * get_detectorsExamples_9() const { return ___detectorsExamples_9; }
	inline DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C ** get_address_of_detectorsExamples_9() { return &___detectorsExamples_9; }
	inline void set_detectorsExamples_9(DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C * value)
	{
		___detectorsExamples_9 = value;
		Il2CppCodeGenWriteBarrier((&___detectorsExamples_9), value);
	}

	inline static int32_t get_offset_of_codeHashExample_10() { return static_cast<int32_t>(offsetof(ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C, ___codeHashExample_10)); }
	inline CodeHashExample_tFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1 * get_codeHashExample_10() const { return ___codeHashExample_10; }
	inline CodeHashExample_tFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1 ** get_address_of_codeHashExample_10() { return &___codeHashExample_10; }
	inline void set_codeHashExample_10(CodeHashExample_tFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1 * value)
	{
		___codeHashExample_10 = value;
		Il2CppCodeGenWriteBarrier((&___codeHashExample_10), value);
	}

	inline static int32_t get_offset_of_tabs_11() { return static_cast<int32_t>(offsetof(ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C, ___tabs_11)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_tabs_11() const { return ___tabs_11; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_tabs_11() { return &___tabs_11; }
	inline void set_tabs_11(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___tabs_11 = value;
		Il2CppCodeGenWriteBarrier((&___tabs_11), value);
	}

	inline static int32_t get_offset_of_currentPage_12() { return static_cast<int32_t>(offsetof(ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C, ___currentPage_12)); }
	inline int32_t get_currentPage_12() const { return ___currentPage_12; }
	inline int32_t* get_address_of_currentPage_12() { return &___currentPage_12; }
	inline void set_currentPage_12(int32_t value)
	{
		___currentPage_12 = value;
	}

	inline static int32_t get_offset_of_allSimpleObscuredTypes_13() { return static_cast<int32_t>(offsetof(ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C, ___allSimpleObscuredTypes_13)); }
	inline String_t* get_allSimpleObscuredTypes_13() const { return ___allSimpleObscuredTypes_13; }
	inline String_t** get_address_of_allSimpleObscuredTypes_13() { return &___allSimpleObscuredTypes_13; }
	inline void set_allSimpleObscuredTypes_13(String_t* value)
	{
		___allSimpleObscuredTypes_13 = value;
		Il2CppCodeGenWriteBarrier((&___allSimpleObscuredTypes_13), value);
	}

	inline static int32_t get_offset_of_savesLock_14() { return static_cast<int32_t>(offsetof(ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C, ___savesLock_14)); }
	inline uint8_t get_savesLock_14() const { return ___savesLock_14; }
	inline uint8_t* get_address_of_savesLock_14() { return &___savesLock_14; }
	inline void set_savesLock_14(uint8_t value)
	{
		___savesLock_14 = value;
	}

	inline static int32_t get_offset_of_centeredStyle_15() { return static_cast<int32_t>(offsetof(ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C, ___centeredStyle_15)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_centeredStyle_15() const { return ___centeredStyle_15; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_centeredStyle_15() { return &___centeredStyle_15; }
	inline void set_centeredStyle_15(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___centeredStyle_15 = value;
		Il2CppCodeGenWriteBarrier((&___centeredStyle_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLESGUI_T724140363AAAC1BE52036C2F20F61A57D7D05D8C_H
#ifndef GENUINEVALIDATOREXAMPLE_T7161B4F6115F89CD8BD3E68EDDE0E2B23167FBDD_H
#define GENUINEVALIDATOREXAMPLE_T7161B4F6115F89CD8BD3E68EDDE0E2B23167FBDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Examples.Genuine.GenuineValidatorExample
struct  GenuineValidatorExample_t7161B4F6115F89CD8BD3E68EDDE0E2B23167FBDD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String CodeStage.AntiCheat.Examples.Genuine.GenuineValidatorExample::status
	String_t* ___status_9;

public:
	inline static int32_t get_offset_of_status_9() { return static_cast<int32_t>(offsetof(GenuineValidatorExample_t7161B4F6115F89CD8BD3E68EDDE0E2B23167FBDD, ___status_9)); }
	inline String_t* get_status_9() const { return ___status_9; }
	inline String_t** get_address_of_status_9() { return &___status_9; }
	inline void set_status_9(String_t* value)
	{
		___status_9 = value;
		Il2CppCodeGenWriteBarrier((&___status_9), value);
	}
};

struct GenuineValidatorExample_t7161B4F6115F89CD8BD3E68EDDE0E2B23167FBDD_StaticFields
{
public:
	// System.Char[] CodeStage.AntiCheat.Examples.Genuine.GenuineValidatorExample::StringKey
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___StringKey_4;
	// System.Int32 CodeStage.AntiCheat.Examples.Genuine.GenuineValidatorExample::SeparatorLength
	int32_t ___SeparatorLength_8;

public:
	inline static int32_t get_offset_of_StringKey_4() { return static_cast<int32_t>(offsetof(GenuineValidatorExample_t7161B4F6115F89CD8BD3E68EDDE0E2B23167FBDD_StaticFields, ___StringKey_4)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_StringKey_4() const { return ___StringKey_4; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_StringKey_4() { return &___StringKey_4; }
	inline void set_StringKey_4(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___StringKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___StringKey_4), value);
	}

	inline static int32_t get_offset_of_SeparatorLength_8() { return static_cast<int32_t>(offsetof(GenuineValidatorExample_t7161B4F6115F89CD8BD3E68EDDE0E2B23167FBDD_StaticFields, ___SeparatorLength_8)); }
	inline int32_t get_SeparatorLength_8() const { return ___SeparatorLength_8; }
	inline int32_t* get_address_of_SeparatorLength_8() { return &___SeparatorLength_8; }
	inline void set_SeparatorLength_8(int32_t value)
	{
		___SeparatorLength_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENUINEVALIDATOREXAMPLE_T7161B4F6115F89CD8BD3E68EDDE0E2B23167FBDD_H
#ifndef INFINITEROTATOR_TFA6BA16BE7F5E1665489CC31C0C9005040A97DEC_H
#define INFINITEROTATOR_TFA6BA16BE7F5E1665489CC31C0C9005040A97DEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Examples.InfiniteRotator
struct  InfiniteRotator_tFA6BA16BE7F5E1665489CC31C0C9005040A97DEC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single CodeStage.AntiCheat.Examples.InfiniteRotator::speed
	float ___speed_4;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(InfiniteRotator_tFA6BA16BE7F5E1665489CC31C0C9005040A97DEC, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFINITEROTATOR_TFA6BA16BE7F5E1665489CC31C0C9005040A97DEC_H
#ifndef INFINITEROTATORRELIABLE_TCB17DE8224CD5B9E8FAE1FF7B7A79B1617E5602D_H
#define INFINITEROTATORRELIABLE_TCB17DE8224CD5B9E8FAE1FF7B7A79B1617E5602D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Examples.InfiniteRotatorReliable
struct  InfiniteRotatorReliable_tCB17DE8224CD5B9E8FAE1FF7B7A79B1617E5602D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single CodeStage.AntiCheat.Examples.InfiniteRotatorReliable::speed
	float ___speed_4;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(InfiniteRotatorReliable_tCB17DE8224CD5B9E8FAE1FF7B7A79B1617E5602D, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFINITEROTATORRELIABLE_TCB17DE8224CD5B9E8FAE1FF7B7A79B1617E5602D_H
#ifndef OBSCUREDPERFORMANCETESTS_TDE56CF69FAD4F75529F43F17DF79A05157333F78_H
#define OBSCUREDPERFORMANCETESTS_TDE56CF69FAD4F75529F43F17DF79A05157333F78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Examples.ObscuredPerformanceTests
struct  ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::boolTest
	bool ___boolTest_4;
	// System.Int32 CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::boolIterations
	int32_t ___boolIterations_5;
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::byteTest
	bool ___byteTest_6;
	// System.Int32 CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::byteIterations
	int32_t ___byteIterations_7;
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::shortTest
	bool ___shortTest_8;
	// System.Int32 CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::shortIterations
	int32_t ___shortIterations_9;
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::ushortTest
	bool ___ushortTest_10;
	// System.Int32 CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::ushortIterations
	int32_t ___ushortIterations_11;
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::intTest
	bool ___intTest_12;
	// System.Int32 CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::intIterations
	int32_t ___intIterations_13;
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::uintTest
	bool ___uintTest_14;
	// System.Int32 CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::uintIterations
	int32_t ___uintIterations_15;
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::longTest
	bool ___longTest_16;
	// System.Int32 CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::longIterations
	int32_t ___longIterations_17;
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::floatTest
	bool ___floatTest_18;
	// System.Int32 CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::floatIterations
	int32_t ___floatIterations_19;
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::doubleTest
	bool ___doubleTest_20;
	// System.Int32 CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::doubleIterations
	int32_t ___doubleIterations_21;
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::stringTest
	bool ___stringTest_22;
	// System.Int32 CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::stringIterations
	int32_t ___stringIterations_23;
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::vector3Test
	bool ___vector3Test_24;
	// System.Int32 CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::vector3Iterations
	int32_t ___vector3Iterations_25;
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::prefsTest
	bool ___prefsTest_26;
	// System.Int32 CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::prefsIterations
	int32_t ___prefsIterations_27;
	// System.Text.StringBuilder CodeStage.AntiCheat.Examples.ObscuredPerformanceTests::logBuilder
	StringBuilder_t * ___logBuilder_28;

public:
	inline static int32_t get_offset_of_boolTest_4() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___boolTest_4)); }
	inline bool get_boolTest_4() const { return ___boolTest_4; }
	inline bool* get_address_of_boolTest_4() { return &___boolTest_4; }
	inline void set_boolTest_4(bool value)
	{
		___boolTest_4 = value;
	}

	inline static int32_t get_offset_of_boolIterations_5() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___boolIterations_5)); }
	inline int32_t get_boolIterations_5() const { return ___boolIterations_5; }
	inline int32_t* get_address_of_boolIterations_5() { return &___boolIterations_5; }
	inline void set_boolIterations_5(int32_t value)
	{
		___boolIterations_5 = value;
	}

	inline static int32_t get_offset_of_byteTest_6() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___byteTest_6)); }
	inline bool get_byteTest_6() const { return ___byteTest_6; }
	inline bool* get_address_of_byteTest_6() { return &___byteTest_6; }
	inline void set_byteTest_6(bool value)
	{
		___byteTest_6 = value;
	}

	inline static int32_t get_offset_of_byteIterations_7() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___byteIterations_7)); }
	inline int32_t get_byteIterations_7() const { return ___byteIterations_7; }
	inline int32_t* get_address_of_byteIterations_7() { return &___byteIterations_7; }
	inline void set_byteIterations_7(int32_t value)
	{
		___byteIterations_7 = value;
	}

	inline static int32_t get_offset_of_shortTest_8() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___shortTest_8)); }
	inline bool get_shortTest_8() const { return ___shortTest_8; }
	inline bool* get_address_of_shortTest_8() { return &___shortTest_8; }
	inline void set_shortTest_8(bool value)
	{
		___shortTest_8 = value;
	}

	inline static int32_t get_offset_of_shortIterations_9() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___shortIterations_9)); }
	inline int32_t get_shortIterations_9() const { return ___shortIterations_9; }
	inline int32_t* get_address_of_shortIterations_9() { return &___shortIterations_9; }
	inline void set_shortIterations_9(int32_t value)
	{
		___shortIterations_9 = value;
	}

	inline static int32_t get_offset_of_ushortTest_10() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___ushortTest_10)); }
	inline bool get_ushortTest_10() const { return ___ushortTest_10; }
	inline bool* get_address_of_ushortTest_10() { return &___ushortTest_10; }
	inline void set_ushortTest_10(bool value)
	{
		___ushortTest_10 = value;
	}

	inline static int32_t get_offset_of_ushortIterations_11() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___ushortIterations_11)); }
	inline int32_t get_ushortIterations_11() const { return ___ushortIterations_11; }
	inline int32_t* get_address_of_ushortIterations_11() { return &___ushortIterations_11; }
	inline void set_ushortIterations_11(int32_t value)
	{
		___ushortIterations_11 = value;
	}

	inline static int32_t get_offset_of_intTest_12() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___intTest_12)); }
	inline bool get_intTest_12() const { return ___intTest_12; }
	inline bool* get_address_of_intTest_12() { return &___intTest_12; }
	inline void set_intTest_12(bool value)
	{
		___intTest_12 = value;
	}

	inline static int32_t get_offset_of_intIterations_13() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___intIterations_13)); }
	inline int32_t get_intIterations_13() const { return ___intIterations_13; }
	inline int32_t* get_address_of_intIterations_13() { return &___intIterations_13; }
	inline void set_intIterations_13(int32_t value)
	{
		___intIterations_13 = value;
	}

	inline static int32_t get_offset_of_uintTest_14() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___uintTest_14)); }
	inline bool get_uintTest_14() const { return ___uintTest_14; }
	inline bool* get_address_of_uintTest_14() { return &___uintTest_14; }
	inline void set_uintTest_14(bool value)
	{
		___uintTest_14 = value;
	}

	inline static int32_t get_offset_of_uintIterations_15() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___uintIterations_15)); }
	inline int32_t get_uintIterations_15() const { return ___uintIterations_15; }
	inline int32_t* get_address_of_uintIterations_15() { return &___uintIterations_15; }
	inline void set_uintIterations_15(int32_t value)
	{
		___uintIterations_15 = value;
	}

	inline static int32_t get_offset_of_longTest_16() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___longTest_16)); }
	inline bool get_longTest_16() const { return ___longTest_16; }
	inline bool* get_address_of_longTest_16() { return &___longTest_16; }
	inline void set_longTest_16(bool value)
	{
		___longTest_16 = value;
	}

	inline static int32_t get_offset_of_longIterations_17() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___longIterations_17)); }
	inline int32_t get_longIterations_17() const { return ___longIterations_17; }
	inline int32_t* get_address_of_longIterations_17() { return &___longIterations_17; }
	inline void set_longIterations_17(int32_t value)
	{
		___longIterations_17 = value;
	}

	inline static int32_t get_offset_of_floatTest_18() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___floatTest_18)); }
	inline bool get_floatTest_18() const { return ___floatTest_18; }
	inline bool* get_address_of_floatTest_18() { return &___floatTest_18; }
	inline void set_floatTest_18(bool value)
	{
		___floatTest_18 = value;
	}

	inline static int32_t get_offset_of_floatIterations_19() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___floatIterations_19)); }
	inline int32_t get_floatIterations_19() const { return ___floatIterations_19; }
	inline int32_t* get_address_of_floatIterations_19() { return &___floatIterations_19; }
	inline void set_floatIterations_19(int32_t value)
	{
		___floatIterations_19 = value;
	}

	inline static int32_t get_offset_of_doubleTest_20() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___doubleTest_20)); }
	inline bool get_doubleTest_20() const { return ___doubleTest_20; }
	inline bool* get_address_of_doubleTest_20() { return &___doubleTest_20; }
	inline void set_doubleTest_20(bool value)
	{
		___doubleTest_20 = value;
	}

	inline static int32_t get_offset_of_doubleIterations_21() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___doubleIterations_21)); }
	inline int32_t get_doubleIterations_21() const { return ___doubleIterations_21; }
	inline int32_t* get_address_of_doubleIterations_21() { return &___doubleIterations_21; }
	inline void set_doubleIterations_21(int32_t value)
	{
		___doubleIterations_21 = value;
	}

	inline static int32_t get_offset_of_stringTest_22() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___stringTest_22)); }
	inline bool get_stringTest_22() const { return ___stringTest_22; }
	inline bool* get_address_of_stringTest_22() { return &___stringTest_22; }
	inline void set_stringTest_22(bool value)
	{
		___stringTest_22 = value;
	}

	inline static int32_t get_offset_of_stringIterations_23() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___stringIterations_23)); }
	inline int32_t get_stringIterations_23() const { return ___stringIterations_23; }
	inline int32_t* get_address_of_stringIterations_23() { return &___stringIterations_23; }
	inline void set_stringIterations_23(int32_t value)
	{
		___stringIterations_23 = value;
	}

	inline static int32_t get_offset_of_vector3Test_24() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___vector3Test_24)); }
	inline bool get_vector3Test_24() const { return ___vector3Test_24; }
	inline bool* get_address_of_vector3Test_24() { return &___vector3Test_24; }
	inline void set_vector3Test_24(bool value)
	{
		___vector3Test_24 = value;
	}

	inline static int32_t get_offset_of_vector3Iterations_25() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___vector3Iterations_25)); }
	inline int32_t get_vector3Iterations_25() const { return ___vector3Iterations_25; }
	inline int32_t* get_address_of_vector3Iterations_25() { return &___vector3Iterations_25; }
	inline void set_vector3Iterations_25(int32_t value)
	{
		___vector3Iterations_25 = value;
	}

	inline static int32_t get_offset_of_prefsTest_26() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___prefsTest_26)); }
	inline bool get_prefsTest_26() const { return ___prefsTest_26; }
	inline bool* get_address_of_prefsTest_26() { return &___prefsTest_26; }
	inline void set_prefsTest_26(bool value)
	{
		___prefsTest_26 = value;
	}

	inline static int32_t get_offset_of_prefsIterations_27() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___prefsIterations_27)); }
	inline int32_t get_prefsIterations_27() const { return ___prefsIterations_27; }
	inline int32_t* get_address_of_prefsIterations_27() { return &___prefsIterations_27; }
	inline void set_prefsIterations_27(int32_t value)
	{
		___prefsIterations_27 = value;
	}

	inline static int32_t get_offset_of_logBuilder_28() { return static_cast<int32_t>(offsetof(ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78, ___logBuilder_28)); }
	inline StringBuilder_t * get_logBuilder_28() const { return ___logBuilder_28; }
	inline StringBuilder_t ** get_address_of_logBuilder_28() { return &___logBuilder_28; }
	inline void set_logBuilder_28(StringBuilder_t * value)
	{
		___logBuilder_28 = value;
		Il2CppCodeGenWriteBarrier((&___logBuilder_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSCUREDPERFORMANCETESTS_TDE56CF69FAD4F75529F43F17DF79A05157333F78_H
#ifndef OBSCUREDPREFSEXAMPLES_T5044E7EE9EDB3491F0E86094FE8352FA85F54063_H
#define OBSCUREDPREFSEXAMPLES_T5044E7EE9EDB3491F0E86094FE8352FA85F54063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Examples.ObscuredPrefsExamples
struct  ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String CodeStage.AntiCheat.Examples.ObscuredPrefsExamples::regularPrefs
	String_t* ___regularPrefs_17;
	// System.String CodeStage.AntiCheat.Examples.ObscuredPrefsExamples::obscuredPrefs
	String_t* ___obscuredPrefs_18;
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPrefsExamples::savesAlterationDetected
	bool ___savesAlterationDetected_19;
	// System.Boolean CodeStage.AntiCheat.Examples.ObscuredPrefsExamples::foreignSavesDetected
	bool ___foreignSavesDetected_20;

public:
	inline static int32_t get_offset_of_regularPrefs_17() { return static_cast<int32_t>(offsetof(ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063, ___regularPrefs_17)); }
	inline String_t* get_regularPrefs_17() const { return ___regularPrefs_17; }
	inline String_t** get_address_of_regularPrefs_17() { return &___regularPrefs_17; }
	inline void set_regularPrefs_17(String_t* value)
	{
		___regularPrefs_17 = value;
		Il2CppCodeGenWriteBarrier((&___regularPrefs_17), value);
	}

	inline static int32_t get_offset_of_obscuredPrefs_18() { return static_cast<int32_t>(offsetof(ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063, ___obscuredPrefs_18)); }
	inline String_t* get_obscuredPrefs_18() const { return ___obscuredPrefs_18; }
	inline String_t** get_address_of_obscuredPrefs_18() { return &___obscuredPrefs_18; }
	inline void set_obscuredPrefs_18(String_t* value)
	{
		___obscuredPrefs_18 = value;
		Il2CppCodeGenWriteBarrier((&___obscuredPrefs_18), value);
	}

	inline static int32_t get_offset_of_savesAlterationDetected_19() { return static_cast<int32_t>(offsetof(ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063, ___savesAlterationDetected_19)); }
	inline bool get_savesAlterationDetected_19() const { return ___savesAlterationDetected_19; }
	inline bool* get_address_of_savesAlterationDetected_19() { return &___savesAlterationDetected_19; }
	inline void set_savesAlterationDetected_19(bool value)
	{
		___savesAlterationDetected_19 = value;
	}

	inline static int32_t get_offset_of_foreignSavesDetected_20() { return static_cast<int32_t>(offsetof(ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063, ___foreignSavesDetected_20)); }
	inline bool get_foreignSavesDetected_20() const { return ___foreignSavesDetected_20; }
	inline bool* get_address_of_foreignSavesDetected_20() { return &___foreignSavesDetected_20; }
	inline void set_foreignSavesDetected_20(bool value)
	{
		___foreignSavesDetected_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSCUREDPREFSEXAMPLES_T5044E7EE9EDB3491F0E86094FE8352FA85F54063_H
#ifndef OBSCUREDTYPESEXAMPLES_TF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA_H
#define OBSCUREDTYPESEXAMPLES_TF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Examples.ObscuredTypesExamples
struct  ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String CodeStage.AntiCheat.Examples.ObscuredTypesExamples::regularString
	String_t* ___regularString_4;
	// System.Int32 CodeStage.AntiCheat.Examples.ObscuredTypesExamples::regularInt
	int32_t ___regularInt_5;
	// System.Single CodeStage.AntiCheat.Examples.ObscuredTypesExamples::regularFloat
	float ___regularFloat_6;
	// UnityEngine.Vector3 CodeStage.AntiCheat.Examples.ObscuredTypesExamples::regularVector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___regularVector3_7;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredString CodeStage.AntiCheat.Examples.ObscuredTypesExamples::obscuredString
	ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301 * ___obscuredString_8;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt CodeStage.AntiCheat.Examples.ObscuredTypesExamples::obscuredInt
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  ___obscuredInt_9;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat CodeStage.AntiCheat.Examples.ObscuredTypesExamples::obscuredFloat
	ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077  ___obscuredFloat_10;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3 CodeStage.AntiCheat.Examples.ObscuredTypesExamples::obscuredVector3
	ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781  ___obscuredVector3_11;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredBool CodeStage.AntiCheat.Examples.ObscuredTypesExamples::obscuredBool
	ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452  ___obscuredBool_12;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredLong CodeStage.AntiCheat.Examples.ObscuredTypesExamples::obscuredLong
	ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609  ___obscuredLong_13;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble CodeStage.AntiCheat.Examples.ObscuredTypesExamples::obscuredDouble
	ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B  ___obscuredDouble_14;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2 CodeStage.AntiCheat.Examples.ObscuredTypesExamples::obscuredVector2
	ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855  ___obscuredVector2_15;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal CodeStage.AntiCheat.Examples.ObscuredTypesExamples::obscuredDecimal
	ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C  ___obscuredDecimal_16;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int CodeStage.AntiCheat.Examples.ObscuredTypesExamples::obscuredVector2Int
	ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A  ___obscuredVector2Int_17;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int CodeStage.AntiCheat.Examples.ObscuredTypesExamples::obscuredVector3Int
	ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD  ___obscuredVector3Int_18;
	// System.Text.StringBuilder CodeStage.AntiCheat.Examples.ObscuredTypesExamples::logBuilder
	StringBuilder_t * ___logBuilder_19;

public:
	inline static int32_t get_offset_of_regularString_4() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___regularString_4)); }
	inline String_t* get_regularString_4() const { return ___regularString_4; }
	inline String_t** get_address_of_regularString_4() { return &___regularString_4; }
	inline void set_regularString_4(String_t* value)
	{
		___regularString_4 = value;
		Il2CppCodeGenWriteBarrier((&___regularString_4), value);
	}

	inline static int32_t get_offset_of_regularInt_5() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___regularInt_5)); }
	inline int32_t get_regularInt_5() const { return ___regularInt_5; }
	inline int32_t* get_address_of_regularInt_5() { return &___regularInt_5; }
	inline void set_regularInt_5(int32_t value)
	{
		___regularInt_5 = value;
	}

	inline static int32_t get_offset_of_regularFloat_6() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___regularFloat_6)); }
	inline float get_regularFloat_6() const { return ___regularFloat_6; }
	inline float* get_address_of_regularFloat_6() { return &___regularFloat_6; }
	inline void set_regularFloat_6(float value)
	{
		___regularFloat_6 = value;
	}

	inline static int32_t get_offset_of_regularVector3_7() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___regularVector3_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_regularVector3_7() const { return ___regularVector3_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_regularVector3_7() { return &___regularVector3_7; }
	inline void set_regularVector3_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___regularVector3_7 = value;
	}

	inline static int32_t get_offset_of_obscuredString_8() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___obscuredString_8)); }
	inline ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301 * get_obscuredString_8() const { return ___obscuredString_8; }
	inline ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301 ** get_address_of_obscuredString_8() { return &___obscuredString_8; }
	inline void set_obscuredString_8(ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301 * value)
	{
		___obscuredString_8 = value;
		Il2CppCodeGenWriteBarrier((&___obscuredString_8), value);
	}

	inline static int32_t get_offset_of_obscuredInt_9() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___obscuredInt_9)); }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  get_obscuredInt_9() const { return ___obscuredInt_9; }
	inline ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489 * get_address_of_obscuredInt_9() { return &___obscuredInt_9; }
	inline void set_obscuredInt_9(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489  value)
	{
		___obscuredInt_9 = value;
	}

	inline static int32_t get_offset_of_obscuredFloat_10() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___obscuredFloat_10)); }
	inline ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077  get_obscuredFloat_10() const { return ___obscuredFloat_10; }
	inline ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077 * get_address_of_obscuredFloat_10() { return &___obscuredFloat_10; }
	inline void set_obscuredFloat_10(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077  value)
	{
		___obscuredFloat_10 = value;
	}

	inline static int32_t get_offset_of_obscuredVector3_11() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___obscuredVector3_11)); }
	inline ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781  get_obscuredVector3_11() const { return ___obscuredVector3_11; }
	inline ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781 * get_address_of_obscuredVector3_11() { return &___obscuredVector3_11; }
	inline void set_obscuredVector3_11(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781  value)
	{
		___obscuredVector3_11 = value;
	}

	inline static int32_t get_offset_of_obscuredBool_12() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___obscuredBool_12)); }
	inline ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452  get_obscuredBool_12() const { return ___obscuredBool_12; }
	inline ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452 * get_address_of_obscuredBool_12() { return &___obscuredBool_12; }
	inline void set_obscuredBool_12(ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452  value)
	{
		___obscuredBool_12 = value;
	}

	inline static int32_t get_offset_of_obscuredLong_13() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___obscuredLong_13)); }
	inline ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609  get_obscuredLong_13() const { return ___obscuredLong_13; }
	inline ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609 * get_address_of_obscuredLong_13() { return &___obscuredLong_13; }
	inline void set_obscuredLong_13(ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609  value)
	{
		___obscuredLong_13 = value;
	}

	inline static int32_t get_offset_of_obscuredDouble_14() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___obscuredDouble_14)); }
	inline ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B  get_obscuredDouble_14() const { return ___obscuredDouble_14; }
	inline ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B * get_address_of_obscuredDouble_14() { return &___obscuredDouble_14; }
	inline void set_obscuredDouble_14(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B  value)
	{
		___obscuredDouble_14 = value;
	}

	inline static int32_t get_offset_of_obscuredVector2_15() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___obscuredVector2_15)); }
	inline ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855  get_obscuredVector2_15() const { return ___obscuredVector2_15; }
	inline ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855 * get_address_of_obscuredVector2_15() { return &___obscuredVector2_15; }
	inline void set_obscuredVector2_15(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855  value)
	{
		___obscuredVector2_15 = value;
	}

	inline static int32_t get_offset_of_obscuredDecimal_16() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___obscuredDecimal_16)); }
	inline ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C  get_obscuredDecimal_16() const { return ___obscuredDecimal_16; }
	inline ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C * get_address_of_obscuredDecimal_16() { return &___obscuredDecimal_16; }
	inline void set_obscuredDecimal_16(ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C  value)
	{
		___obscuredDecimal_16 = value;
	}

	inline static int32_t get_offset_of_obscuredVector2Int_17() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___obscuredVector2Int_17)); }
	inline ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A  get_obscuredVector2Int_17() const { return ___obscuredVector2Int_17; }
	inline ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A * get_address_of_obscuredVector2Int_17() { return &___obscuredVector2Int_17; }
	inline void set_obscuredVector2Int_17(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A  value)
	{
		___obscuredVector2Int_17 = value;
	}

	inline static int32_t get_offset_of_obscuredVector3Int_18() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___obscuredVector3Int_18)); }
	inline ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD  get_obscuredVector3Int_18() const { return ___obscuredVector3Int_18; }
	inline ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD * get_address_of_obscuredVector3Int_18() { return &___obscuredVector3Int_18; }
	inline void set_obscuredVector3Int_18(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD  value)
	{
		___obscuredVector3Int_18 = value;
	}

	inline static int32_t get_offset_of_logBuilder_19() { return static_cast<int32_t>(offsetof(ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA, ___logBuilder_19)); }
	inline StringBuilder_t * get_logBuilder_19() const { return ___logBuilder_19; }
	inline StringBuilder_t ** get_address_of_logBuilder_19() { return &___logBuilder_19; }
	inline void set_logBuilder_19(StringBuilder_t * value)
	{
		___logBuilder_19 = value;
		Il2CppCodeGenWriteBarrier((&___logBuilder_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSCUREDTYPESEXAMPLES_TF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA_H
#ifndef JOYSTICK_T5A918BD4C98ECB45624B10577F91F1E5FA25F26B_H
#define JOYSTICK_T5A918BD4C98ECB45624B10577F91F1E5FA25F26B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Joystick
struct  Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Joystick::handleLimit
	float ___handleLimit_4;
	// JoystickMode Joystick::joystickMode
	int32_t ___joystickMode_5;
	// UnityEngine.Vector2 Joystick::inputVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___inputVector_6;
	// UnityEngine.RectTransform Joystick::background
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___background_7;
	// UnityEngine.RectTransform Joystick::handle
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___handle_8;

public:
	inline static int32_t get_offset_of_handleLimit_4() { return static_cast<int32_t>(offsetof(Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B, ___handleLimit_4)); }
	inline float get_handleLimit_4() const { return ___handleLimit_4; }
	inline float* get_address_of_handleLimit_4() { return &___handleLimit_4; }
	inline void set_handleLimit_4(float value)
	{
		___handleLimit_4 = value;
	}

	inline static int32_t get_offset_of_joystickMode_5() { return static_cast<int32_t>(offsetof(Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B, ___joystickMode_5)); }
	inline int32_t get_joystickMode_5() const { return ___joystickMode_5; }
	inline int32_t* get_address_of_joystickMode_5() { return &___joystickMode_5; }
	inline void set_joystickMode_5(int32_t value)
	{
		___joystickMode_5 = value;
	}

	inline static int32_t get_offset_of_inputVector_6() { return static_cast<int32_t>(offsetof(Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B, ___inputVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_inputVector_6() const { return ___inputVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_inputVector_6() { return &___inputVector_6; }
	inline void set_inputVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___inputVector_6 = value;
	}

	inline static int32_t get_offset_of_background_7() { return static_cast<int32_t>(offsetof(Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B, ___background_7)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_background_7() const { return ___background_7; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_background_7() { return &___background_7; }
	inline void set_background_7(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___background_7 = value;
		Il2CppCodeGenWriteBarrier((&___background_7), value);
	}

	inline static int32_t get_offset_of_handle_8() { return static_cast<int32_t>(offsetof(Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B, ___handle_8)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_handle_8() const { return ___handle_8; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_handle_8() { return &___handle_8; }
	inline void set_handle_8(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___handle_8 = value;
		Il2CppCodeGenWriteBarrier((&___handle_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T5A918BD4C98ECB45624B10577F91F1E5FA25F26B_H
#ifndef TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#define TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TMonoBehaviour
struct  TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMONOBEHAVIOUR_TC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3_H
#ifndef TPARENT_TFA10DDF9B23B0F392CE2630AD04B8E3DB4ADD674_H
#define TPARENT_TFA10DDF9B23B0F392CE2630AD04B8E3DB4ADD674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TParent
struct  TParent_tFA10DDF9B23B0F392CE2630AD04B8E3DB4ADD674  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Tayr.TParent::ConfKey
	String_t* ___ConfKey_4;
	// System.String Tayr.TParent::PrefabKey
	String_t* ___PrefabKey_5;

public:
	inline static int32_t get_offset_of_ConfKey_4() { return static_cast<int32_t>(offsetof(TParent_tFA10DDF9B23B0F392CE2630AD04B8E3DB4ADD674, ___ConfKey_4)); }
	inline String_t* get_ConfKey_4() const { return ___ConfKey_4; }
	inline String_t** get_address_of_ConfKey_4() { return &___ConfKey_4; }
	inline void set_ConfKey_4(String_t* value)
	{
		___ConfKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___ConfKey_4), value);
	}

	inline static int32_t get_offset_of_PrefabKey_5() { return static_cast<int32_t>(offsetof(TParent_tFA10DDF9B23B0F392CE2630AD04B8E3DB4ADD674, ___PrefabKey_5)); }
	inline String_t* get_PrefabKey_5() const { return ___PrefabKey_5; }
	inline String_t** get_address_of_PrefabKey_5() { return &___PrefabKey_5; }
	inline void set_PrefabKey_5(String_t* value)
	{
		___PrefabKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___PrefabKey_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TPARENT_TFA10DDF9B23B0F392CE2630AD04B8E3DB4ADD674_H
#ifndef TTAG_TC8AB09263C27CCCD698B43F771415D346F9C8A23_H
#define TTAG_TC8AB09263C27CCCD698B43F771415D346F9C8A23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TTag
struct  TTag_tC8AB09263C27CCCD698B43F771415D346F9C8A23  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Tayr.TTag::Tag
	String_t* ___Tag_4;

public:
	inline static int32_t get_offset_of_Tag_4() { return static_cast<int32_t>(offsetof(TTag_tC8AB09263C27CCCD698B43F771415D346F9C8A23, ___Tag_4)); }
	inline String_t* get_Tag_4() const { return ___Tag_4; }
	inline String_t** get_address_of_Tag_4() { return &___Tag_4; }
	inline void set_Tag_4(String_t* value)
	{
		___Tag_4 = value;
		Il2CppCodeGenWriteBarrier((&___Tag_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TTAG_TC8AB09263C27CCCD698B43F771415D346F9C8A23_H
#ifndef BATTLEECSINITIALIZER_TA76C29D8A9881FDF3955010F51F9DA4FA7134C67_H
#define BATTLEECSINITIALIZER_TA76C29D8A9881FDF3955010F51F9DA4FA7134C67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleEcsInitializer
struct  BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// Contexts BattleEcsInitializer::_contexts
	Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * ____contexts_4;
	// InputFeature BattleEcsInitializer::_inputFeature
	InputFeature_t5A0C995F504E269254656351086E21C0FAB312AC * ____inputFeature_5;
	// SimulationFeature BattleEcsInitializer::_simulationFeature
	SimulationFeature_tE452891FF1D5A4B359ED58C1EF1F7B2276A87EF2 * ____simulationFeature_6;
	// GameModeFeature BattleEcsInitializer::_gameModeFeature
	GameModeFeature_t26E5E2E6B53B52AFFBC416103BF675284F1F833C * ____gameModeFeature_7;
	// RenderFeature BattleEcsInitializer::_renderFeature
	RenderFeature_tD8D02B6D1FC897C71185766227AB45DC1E8B7D12 * ____renderFeature_8;

public:
	inline static int32_t get_offset_of__contexts_4() { return static_cast<int32_t>(offsetof(BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67, ____contexts_4)); }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * get__contexts_4() const { return ____contexts_4; }
	inline Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D ** get_address_of__contexts_4() { return &____contexts_4; }
	inline void set__contexts_4(Contexts_t313FEE68C5FB0568E30785C33C0F6209F5F7B87D * value)
	{
		____contexts_4 = value;
		Il2CppCodeGenWriteBarrier((&____contexts_4), value);
	}

	inline static int32_t get_offset_of__inputFeature_5() { return static_cast<int32_t>(offsetof(BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67, ____inputFeature_5)); }
	inline InputFeature_t5A0C995F504E269254656351086E21C0FAB312AC * get__inputFeature_5() const { return ____inputFeature_5; }
	inline InputFeature_t5A0C995F504E269254656351086E21C0FAB312AC ** get_address_of__inputFeature_5() { return &____inputFeature_5; }
	inline void set__inputFeature_5(InputFeature_t5A0C995F504E269254656351086E21C0FAB312AC * value)
	{
		____inputFeature_5 = value;
		Il2CppCodeGenWriteBarrier((&____inputFeature_5), value);
	}

	inline static int32_t get_offset_of__simulationFeature_6() { return static_cast<int32_t>(offsetof(BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67, ____simulationFeature_6)); }
	inline SimulationFeature_tE452891FF1D5A4B359ED58C1EF1F7B2276A87EF2 * get__simulationFeature_6() const { return ____simulationFeature_6; }
	inline SimulationFeature_tE452891FF1D5A4B359ED58C1EF1F7B2276A87EF2 ** get_address_of__simulationFeature_6() { return &____simulationFeature_6; }
	inline void set__simulationFeature_6(SimulationFeature_tE452891FF1D5A4B359ED58C1EF1F7B2276A87EF2 * value)
	{
		____simulationFeature_6 = value;
		Il2CppCodeGenWriteBarrier((&____simulationFeature_6), value);
	}

	inline static int32_t get_offset_of__gameModeFeature_7() { return static_cast<int32_t>(offsetof(BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67, ____gameModeFeature_7)); }
	inline GameModeFeature_t26E5E2E6B53B52AFFBC416103BF675284F1F833C * get__gameModeFeature_7() const { return ____gameModeFeature_7; }
	inline GameModeFeature_t26E5E2E6B53B52AFFBC416103BF675284F1F833C ** get_address_of__gameModeFeature_7() { return &____gameModeFeature_7; }
	inline void set__gameModeFeature_7(GameModeFeature_t26E5E2E6B53B52AFFBC416103BF675284F1F833C * value)
	{
		____gameModeFeature_7 = value;
		Il2CppCodeGenWriteBarrier((&____gameModeFeature_7), value);
	}

	inline static int32_t get_offset_of__renderFeature_8() { return static_cast<int32_t>(offsetof(BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67, ____renderFeature_8)); }
	inline RenderFeature_tD8D02B6D1FC897C71185766227AB45DC1E8B7D12 * get__renderFeature_8() const { return ____renderFeature_8; }
	inline RenderFeature_tD8D02B6D1FC897C71185766227AB45DC1E8B7D12 ** get_address_of__renderFeature_8() { return &____renderFeature_8; }
	inline void set__renderFeature_8(RenderFeature_tD8D02B6D1FC897C71185766227AB45DC1E8B7D12 * value)
	{
		____renderFeature_8 = value;
		Il2CppCodeGenWriteBarrier((&____renderFeature_8), value);
	}
};

struct BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67_StaticFields
{
public:
	// System.Boolean BattleEcsInitializer::IsPaused
	bool ___IsPaused_9;

public:
	inline static int32_t get_offset_of_IsPaused_9() { return static_cast<int32_t>(offsetof(BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67_StaticFields, ___IsPaused_9)); }
	inline bool get_IsPaused_9() const { return ___IsPaused_9; }
	inline bool* get_address_of_IsPaused_9() { return &___IsPaused_9; }
	inline void set_IsPaused_9(bool value)
	{
		___IsPaused_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEECSINITIALIZER_TA76C29D8A9881FDF3955010F51F9DA4FA7134C67_H
#ifndef FLOATINGJOYSTICK_T41C399D884FFC840F0FE3B4A0E6C2EC2B2F8460D_H
#define FLOATINGJOYSTICK_T41C399D884FFC840F0FE3B4A0E6C2EC2B2F8460D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatingJoystick
struct  FloatingJoystick_t41C399D884FFC840F0FE3B4A0E6C2EC2B2F8460D  : public Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B
{
public:
	// UnityEngine.Vector2 FloatingJoystick::joystickCenter
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___joystickCenter_9;

public:
	inline static int32_t get_offset_of_joystickCenter_9() { return static_cast<int32_t>(offsetof(FloatingJoystick_t41C399D884FFC840F0FE3B4A0E6C2EC2B2F8460D, ___joystickCenter_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_joystickCenter_9() const { return ___joystickCenter_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_joystickCenter_9() { return &___joystickCenter_9; }
	inline void set_joystickCenter_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___joystickCenter_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATINGJOYSTICK_T41C399D884FFC840F0FE3B4A0E6C2EC2B2F8460D_H
#ifndef SPELLUIINITIALIZER_T66B06A358BA3B762376CD13F820EFC620ADFDAD8_H
#define SPELLUIINITIALIZER_T66B06A358BA3B762376CD13F820EFC620ADFDAD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpellUIInitializer
struct  SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// UnityEngine.Animator SpellUIInitializer::_animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ____animator_4;
	// UnityEngine.UI.Button SpellUIInitializer::_spell1
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____spell1_5;
	// UnityEngine.UI.Button SpellUIInitializer::_spell2
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____spell2_6;
	// UnityEngine.UI.Button SpellUIInitializer::_spell3
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____spell3_7;
	// UnityEngine.UI.Button SpellUIInitializer::_spell4
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____spell4_8;
	// UnityEngine.UI.Button SpellUIInitializer::_screenBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____screenBtn_9;
	// UnityEngine.UI.Button SpellUIInitializer::_normaleBoostBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____normaleBoostBtn_10;
	// UnityEngine.UI.Button SpellUIInitializer::_megaBoostBtn
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ____megaBoostBtn_11;
	// SpellsSO SpellUIInitializer::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_12;
	// InventorySystem SpellUIInitializer::_inventorySystem
	InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * ____inventorySystem_13;
	// Zenject.DiContainer SpellUIInitializer::_battleContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____battleContainer_14;
	// Tayr.TSoundSystem SpellUIInitializer::_soundSystem
	TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * ____soundSystem_15;
	// SoundSO SpellUIInitializer::_soundSO
	SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * ____soundSO_16;
	// BattleSettingsSO SpellUIInitializer::_battleSettingsSO
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * ____battleSettingsSO_17;
	// UserVO SpellUIInitializer::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_18;
	// GameContext SpellUIInitializer::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_19;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.String> SpellUIInitializer::_spellIndexesDictionary
	Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * ____spellIndexesDictionary_20;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UI.Button> SpellUIInitializer::_spellDictionary
	Dictionary_2_t554924F40F85C9BB88122F0D57490C2D3012B71E * ____spellDictionary_21;
	// SpellModel SpellUIInitializer::_lastSpellModell
	SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235 * ____lastSpellModell_22;
	// System.Int32 SpellUIInitializer::_lastSpellIndex
	int32_t ____lastSpellIndex_23;
	// System.String SpellUIInitializer::_lastTowerTapped
	String_t* ____lastTowerTapped_24;

public:
	inline static int32_t get_offset_of__animator_4() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____animator_4)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get__animator_4() const { return ____animator_4; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of__animator_4() { return &____animator_4; }
	inline void set__animator_4(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		____animator_4 = value;
		Il2CppCodeGenWriteBarrier((&____animator_4), value);
	}

	inline static int32_t get_offset_of__spell1_5() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____spell1_5)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__spell1_5() const { return ____spell1_5; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__spell1_5() { return &____spell1_5; }
	inline void set__spell1_5(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____spell1_5 = value;
		Il2CppCodeGenWriteBarrier((&____spell1_5), value);
	}

	inline static int32_t get_offset_of__spell2_6() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____spell2_6)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__spell2_6() const { return ____spell2_6; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__spell2_6() { return &____spell2_6; }
	inline void set__spell2_6(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____spell2_6 = value;
		Il2CppCodeGenWriteBarrier((&____spell2_6), value);
	}

	inline static int32_t get_offset_of__spell3_7() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____spell3_7)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__spell3_7() const { return ____spell3_7; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__spell3_7() { return &____spell3_7; }
	inline void set__spell3_7(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____spell3_7 = value;
		Il2CppCodeGenWriteBarrier((&____spell3_7), value);
	}

	inline static int32_t get_offset_of__spell4_8() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____spell4_8)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__spell4_8() const { return ____spell4_8; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__spell4_8() { return &____spell4_8; }
	inline void set__spell4_8(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____spell4_8 = value;
		Il2CppCodeGenWriteBarrier((&____spell4_8), value);
	}

	inline static int32_t get_offset_of__screenBtn_9() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____screenBtn_9)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__screenBtn_9() const { return ____screenBtn_9; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__screenBtn_9() { return &____screenBtn_9; }
	inline void set__screenBtn_9(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____screenBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&____screenBtn_9), value);
	}

	inline static int32_t get_offset_of__normaleBoostBtn_10() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____normaleBoostBtn_10)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__normaleBoostBtn_10() const { return ____normaleBoostBtn_10; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__normaleBoostBtn_10() { return &____normaleBoostBtn_10; }
	inline void set__normaleBoostBtn_10(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____normaleBoostBtn_10 = value;
		Il2CppCodeGenWriteBarrier((&____normaleBoostBtn_10), value);
	}

	inline static int32_t get_offset_of__megaBoostBtn_11() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____megaBoostBtn_11)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get__megaBoostBtn_11() const { return ____megaBoostBtn_11; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of__megaBoostBtn_11() { return &____megaBoostBtn_11; }
	inline void set__megaBoostBtn_11(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		____megaBoostBtn_11 = value;
		Il2CppCodeGenWriteBarrier((&____megaBoostBtn_11), value);
	}

	inline static int32_t get_offset_of__spellsSO_12() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____spellsSO_12)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_12() const { return ____spellsSO_12; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_12() { return &____spellsSO_12; }
	inline void set__spellsSO_12(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_12 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_12), value);
	}

	inline static int32_t get_offset_of__inventorySystem_13() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____inventorySystem_13)); }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * get__inventorySystem_13() const { return ____inventorySystem_13; }
	inline InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 ** get_address_of__inventorySystem_13() { return &____inventorySystem_13; }
	inline void set__inventorySystem_13(InventorySystem_t7F77FB9ECBB1DDA540DCA61A2DCEC83620CE61A2 * value)
	{
		____inventorySystem_13 = value;
		Il2CppCodeGenWriteBarrier((&____inventorySystem_13), value);
	}

	inline static int32_t get_offset_of__battleContainer_14() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____battleContainer_14)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__battleContainer_14() const { return ____battleContainer_14; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__battleContainer_14() { return &____battleContainer_14; }
	inline void set__battleContainer_14(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____battleContainer_14 = value;
		Il2CppCodeGenWriteBarrier((&____battleContainer_14), value);
	}

	inline static int32_t get_offset_of__soundSystem_15() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____soundSystem_15)); }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * get__soundSystem_15() const { return ____soundSystem_15; }
	inline TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 ** get_address_of__soundSystem_15() { return &____soundSystem_15; }
	inline void set__soundSystem_15(TSoundSystem_t26E4FEC20D24CB5940F67189D1210E5A6454B511 * value)
	{
		____soundSystem_15 = value;
		Il2CppCodeGenWriteBarrier((&____soundSystem_15), value);
	}

	inline static int32_t get_offset_of__soundSO_16() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____soundSO_16)); }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * get__soundSO_16() const { return ____soundSO_16; }
	inline SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 ** get_address_of__soundSO_16() { return &____soundSO_16; }
	inline void set__soundSO_16(SoundSO_t9B0D26B0A9F986D8019428EBC54B7CC6259E2C73 * value)
	{
		____soundSO_16 = value;
		Il2CppCodeGenWriteBarrier((&____soundSO_16), value);
	}

	inline static int32_t get_offset_of__battleSettingsSO_17() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____battleSettingsSO_17)); }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * get__battleSettingsSO_17() const { return ____battleSettingsSO_17; }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE ** get_address_of__battleSettingsSO_17() { return &____battleSettingsSO_17; }
	inline void set__battleSettingsSO_17(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * value)
	{
		____battleSettingsSO_17 = value;
		Il2CppCodeGenWriteBarrier((&____battleSettingsSO_17), value);
	}

	inline static int32_t get_offset_of__userVO_18() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____userVO_18)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_18() const { return ____userVO_18; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_18() { return &____userVO_18; }
	inline void set__userVO_18(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_18 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_18), value);
	}

	inline static int32_t get_offset_of__context_19() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____context_19)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_19() const { return ____context_19; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_19() { return &____context_19; }
	inline void set__context_19(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_19 = value;
		Il2CppCodeGenWriteBarrier((&____context_19), value);
	}

	inline static int32_t get_offset_of__spellIndexesDictionary_20() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____spellIndexesDictionary_20)); }
	inline Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * get__spellIndexesDictionary_20() const { return ____spellIndexesDictionary_20; }
	inline Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C ** get_address_of__spellIndexesDictionary_20() { return &____spellIndexesDictionary_20; }
	inline void set__spellIndexesDictionary_20(Dictionary_2_t4EFE6A1D6502662B911688316C6920444A18CF0C * value)
	{
		____spellIndexesDictionary_20 = value;
		Il2CppCodeGenWriteBarrier((&____spellIndexesDictionary_20), value);
	}

	inline static int32_t get_offset_of__spellDictionary_21() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____spellDictionary_21)); }
	inline Dictionary_2_t554924F40F85C9BB88122F0D57490C2D3012B71E * get__spellDictionary_21() const { return ____spellDictionary_21; }
	inline Dictionary_2_t554924F40F85C9BB88122F0D57490C2D3012B71E ** get_address_of__spellDictionary_21() { return &____spellDictionary_21; }
	inline void set__spellDictionary_21(Dictionary_2_t554924F40F85C9BB88122F0D57490C2D3012B71E * value)
	{
		____spellDictionary_21 = value;
		Il2CppCodeGenWriteBarrier((&____spellDictionary_21), value);
	}

	inline static int32_t get_offset_of__lastSpellModell_22() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____lastSpellModell_22)); }
	inline SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235 * get__lastSpellModell_22() const { return ____lastSpellModell_22; }
	inline SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235 ** get_address_of__lastSpellModell_22() { return &____lastSpellModell_22; }
	inline void set__lastSpellModell_22(SpellModel_t50B172E1768260E1736AA33F4ED72DB9CDC35235 * value)
	{
		____lastSpellModell_22 = value;
		Il2CppCodeGenWriteBarrier((&____lastSpellModell_22), value);
	}

	inline static int32_t get_offset_of__lastSpellIndex_23() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____lastSpellIndex_23)); }
	inline int32_t get__lastSpellIndex_23() const { return ____lastSpellIndex_23; }
	inline int32_t* get_address_of__lastSpellIndex_23() { return &____lastSpellIndex_23; }
	inline void set__lastSpellIndex_23(int32_t value)
	{
		____lastSpellIndex_23 = value;
	}

	inline static int32_t get_offset_of__lastTowerTapped_24() { return static_cast<int32_t>(offsetof(SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8, ____lastTowerTapped_24)); }
	inline String_t* get__lastTowerTapped_24() const { return ____lastTowerTapped_24; }
	inline String_t** get_address_of__lastTowerTapped_24() { return &____lastTowerTapped_24; }
	inline void set__lastTowerTapped_24(String_t* value)
	{
		____lastTowerTapped_24 = value;
		Il2CppCodeGenWriteBarrier((&____lastTowerTapped_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPELLUIINITIALIZER_T66B06A358BA3B762376CD13F820EFC620ADFDAD8_H
#ifndef TLISTITEM_T3835B3A6EDB64421C763D6938505136E6CD7628C_H
#define TLISTITEM_T3835B3A6EDB64421C763D6938505136E6CD7628C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TListItem
struct  TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLISTITEM_T3835B3A6EDB64421C763D6938505136E6CD7628C_H
#ifndef TLOCALIZERTEXTMESHPROUGUI_TC187D21D45AC70D0B204800BB874B0BEDAAFD557_H
#define TLOCALIZERTEXTMESHPROUGUI_TC187D21D45AC70D0B204800BB874B0BEDAAFD557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TLocalizerTextMeshProUGUI
struct  TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:
	// System.String Tayr.TLocalizerTextMeshProUGUI::LocalizationKey
	String_t* ___LocalizationKey_4;
	// System.Boolean Tayr.TLocalizerTextMeshProUGUI::IsDynamic
	bool ___IsDynamic_5;
	// UnityEngine.SystemLanguage Tayr.TLocalizerTextMeshProUGUI::_language
	int32_t ____language_6;
	// TMPro.TextMeshProUGUI Tayr.TLocalizerTextMeshProUGUI::_component
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ____component_7;

public:
	inline static int32_t get_offset_of_LocalizationKey_4() { return static_cast<int32_t>(offsetof(TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557, ___LocalizationKey_4)); }
	inline String_t* get_LocalizationKey_4() const { return ___LocalizationKey_4; }
	inline String_t** get_address_of_LocalizationKey_4() { return &___LocalizationKey_4; }
	inline void set_LocalizationKey_4(String_t* value)
	{
		___LocalizationKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___LocalizationKey_4), value);
	}

	inline static int32_t get_offset_of_IsDynamic_5() { return static_cast<int32_t>(offsetof(TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557, ___IsDynamic_5)); }
	inline bool get_IsDynamic_5() const { return ___IsDynamic_5; }
	inline bool* get_address_of_IsDynamic_5() { return &___IsDynamic_5; }
	inline void set_IsDynamic_5(bool value)
	{
		___IsDynamic_5 = value;
	}

	inline static int32_t get_offset_of__language_6() { return static_cast<int32_t>(offsetof(TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557, ____language_6)); }
	inline int32_t get__language_6() const { return ____language_6; }
	inline int32_t* get_address_of__language_6() { return &____language_6; }
	inline void set__language_6(int32_t value)
	{
		____language_6 = value;
	}

	inline static int32_t get_offset_of__component_7() { return static_cast<int32_t>(offsetof(TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557, ____component_7)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get__component_7() const { return ____component_7; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of__component_7() { return &____component_7; }
	inline void set__component_7(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		____component_7 = value;
		Il2CppCodeGenWriteBarrier((&____component_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLOCALIZERTEXTMESHPROUGUI_TC187D21D45AC70D0B204800BB874B0BEDAAFD557_H
#ifndef TSYSTEMUPDATERBEHAVIOUR_T05016249E32A3F49E6B0415C46EFBE83FFD4670D_H
#define TSYSTEMUPDATERBEHAVIOUR_T05016249E32A3F49E6B0415C46EFBE83FFD4670D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tayr.TSystemUpdaterBehaviour
struct  TSystemUpdaterBehaviour_t05016249E32A3F49E6B0415C46EFBE83FFD4670D  : public TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TSYSTEMUPDATERBEHAVIOUR_T05016249E32A3F49E6B0415C46EFBE83FFD4670D_H
#ifndef CUSTOMJOYSTICK_T222CBADA241958A1390BC036FA192E3513119867_H
#define CUSTOMJOYSTICK_T222CBADA241958A1390BC036FA192E3513119867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomJoystick
struct  CustomJoystick_t222CBADA241958A1390BC036FA192E3513119867  : public FloatingJoystick_t41C399D884FFC840F0FE3B4A0E6C2EC2B2F8460D
{
public:
	// GameContext CustomJoystick::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_10;
	// UnityEngine.Canvas CustomJoystick::ParentCanvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___ParentCanvas_11;

public:
	inline static int32_t get_offset_of__context_10() { return static_cast<int32_t>(offsetof(CustomJoystick_t222CBADA241958A1390BC036FA192E3513119867, ____context_10)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_10() const { return ____context_10; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_10() { return &____context_10; }
	inline void set__context_10(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_10 = value;
		Il2CppCodeGenWriteBarrier((&____context_10), value);
	}

	inline static int32_t get_offset_of_ParentCanvas_11() { return static_cast<int32_t>(offsetof(CustomJoystick_t222CBADA241958A1390BC036FA192E3513119867, ___ParentCanvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_ParentCanvas_11() const { return ___ParentCanvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_ParentCanvas_11() { return &___ParentCanvas_11; }
	inline void set_ParentCanvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___ParentCanvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___ParentCanvas_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMJOYSTICK_T222CBADA241958A1390BC036FA192E3513119867_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8000 = { sizeof (TListItem_t3835B3A6EDB64421C763D6938505136E6CD7628C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8001 = { sizeof (TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8001[4] = 
{
	TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557::get_offset_of_LocalizationKey_4(),
	TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557::get_offset_of_IsDynamic_5(),
	TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557::get_offset_of__language_6(),
	TLocalizerTextMeshProUGUI_tC187D21D45AC70D0B204800BB874B0BEDAAFD557::get_offset_of__component_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8002 = { sizeof (TMonoBehaviour_tC5EA4E5C9CF2F8B1360110F3DCB3CA86F7D68FF3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8003 = { sizeof (TParent_tFA10DDF9B23B0F392CE2630AD04B8E3DB4ADD674), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8003[2] = 
{
	TParent_tFA10DDF9B23B0F392CE2630AD04B8E3DB4ADD674::get_offset_of_ConfKey_4(),
	TParent_tFA10DDF9B23B0F392CE2630AD04B8E3DB4ADD674::get_offset_of_PrefabKey_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8004 = { sizeof (TPrefabsConf_t4F6D2D381170516B67D7F3198041F9B3FB0C4DE1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8004[1] = 
{
	TPrefabsConf_t4F6D2D381170516B67D7F3198041F9B3FB0C4DE1::get_offset_of_Prefabs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8005 = { sizeof (TPrefabData_t6BDD5C7ECDED47BA807996C12EB93CAC2C3E826E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8005[2] = 
{
	TPrefabData_t6BDD5C7ECDED47BA807996C12EB93CAC2C3E826E::get_offset_of_AssetBundle_0(),
	TPrefabData_t6BDD5C7ECDED47BA807996C12EB93CAC2C3E826E::get_offset_of_PrefabName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8006 = { sizeof (TTag_tC8AB09263C27CCCD698B43F771415D346F9C8A23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8006[1] = 
{
	TTag_tC8AB09263C27CCCD698B43F771415D346F9C8A23::get_offset_of_Tag_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8007 = { sizeof (Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8007[7] = 
{
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09::get_offset_of__onTriggerEvent_0(),
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09::get_offset_of__onUpdateEvent_1(),
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09::get_offset_of__operator_2(),
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09::get_offset_of__target_3(),
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09::get_offset_of__variable_4(),
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09::get_offset_of__isTriggered_5(),
	Trigger_tFA4DAF5E2FB996087C1F5DD53203E250D4770C09::get_offset_of_Name_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8008 = { sizeof (Operator_tAC7D6EA07F38B0B43477FE21F97E9654F61D3A0F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8008[7] = 
{
	Operator_tAC7D6EA07F38B0B43477FE21F97E9654F61D3A0F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8009 = { sizeof (OnTriggerEvent_t77D3DC6AA5ECD88DDAE794D7A976DED80DAD9019), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8010 = { sizeof (TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8010[7] = 
{
	0,
	0,
	TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E::get_offset_of_Operator_2(),
	TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E::get_offset_of_Target_3(),
	TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E::get_offset_of_VariableAlias_4(),
	TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E::get_offset_of_Variable_5(),
	TriggerModel_t09B711CC906A9A76F767318AF6754B20F253399E::get_offset_of_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8011 = { sizeof (TSystemAttribute_tC2CFB3389B6C848683AD9339E48CB651B017218B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8011[1] = 
{
	TSystemAttribute_tC2CFB3389B6C848683AD9339E48CB651B017218B::get_offset_of__container_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8012 = { sizeof (BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8012[3] = 
{
	BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8::get_offset_of__disposableManager_0(),
	BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8::get_offset_of__systemsManager_1(),
	BasicTSystem_tD6508F31E78D89E2227A8E4AD3D70DFDEBCB07F8::get_offset_of_U3CIsInitializedU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8013 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8014 = { sizeof (TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8014[1] = 
{
	TSystemsManager_t22A7C9150EDDB2B8046FF0832CBAEDA8FAEB8462::get_offset_of__systemsContainers_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8015 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8016 = { sizeof (TSystemUpdaterBehaviour_t05016249E32A3F49E6B0415C46EFBE83FFD4670D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8017 = { sizeof (TUpdaterSystem_t69A618B692764BD9B69DD0829A3D8DDFEC7AFA2E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8017[1] = 
{
	TUpdaterSystem_t69A618B692764BD9B69DD0829A3D8DDFEC7AFA2E::get_offset_of__updataList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8018 = { sizeof (TDeviceUtils_t5A8435FBCA12D5AB75BAFC6B658875F57839E3D1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8019 = { sizeof (TimeUtils_tBDFFCBF579233DEEA3761D9C63B8D7E28E18D888), -1, sizeof(TimeUtils_tBDFFCBF579233DEEA3761D9C63B8D7E28E18D888_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8019[1] = 
{
	TimeUtils_tBDFFCBF579233DEEA3761D9C63B8D7E28E18D888_StaticFields::get_offset_of_EPOCH_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8020 = { sizeof (Utils_tB422C8B784B7ABE33AE5F796861A931E89AC0C97), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8021 = { sizeof (Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8021[4] = 
{
	Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD::get_offset_of__onVariableChanged_0(),
	Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD::get_offset_of__value_1(),
	Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD::get_offset_of__refreshType_2(),
	Variable_t5D003723AEAA82A1E36B619BD98E937C05793BFD::get_offset_of__isInitialized_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8022 = { sizeof (VariableChangedEvent_t553579619C432FFE7C7636199CE57457FA699BA4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8023 = { sizeof (RefreahType_tD4E50A740AAB4EA48BFF2B426369C53D543441DE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8023[3] = 
{
	RefreahType_tD4E50A740AAB4EA48BFF2B426369C53D543441DE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8024 = { sizeof (VariablesSystem_t32C6897063875FC0FF77B3F2D5E8218BAA904C92), -1, sizeof(VariablesSystem_t32C6897063875FC0FF77B3F2D5E8218BAA904C92_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8024[1] = 
{
	VariablesSystem_t32C6897063875FC0FF77B3F2D5E8218BAA904C92_StaticFields::get_offset_of__variables_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8025 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8026 = { sizeof (VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8026[1] = 
{
	VOSaver_t8C114547CE0BD5068CE3D91A3CD858C48DD1A88F::get_offset_of__dirtyVO_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8027 = { sizeof (U3CPrivateImplementationDetailsU3E_t13594387F3B15033150E0CB705727D78DB03377C), -1, sizeof(U3CPrivateImplementationDetailsU3E_t13594387F3B15033150E0CB705727D78DB03377C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8027[2] = 
{
	U3CPrivateImplementationDetailsU3E_t13594387F3B15033150E0CB705727D78DB03377C_StaticFields::get_offset_of_U3444C0B6D3BA44B0AEC9133CAA7B1EADF170635FD_0(),
	U3CPrivateImplementationDetailsU3E_t13594387F3B15033150E0CB705727D78DB03377C_StaticFields::get_offset_of_D2FD55BEC43713E0126C440295414130C96CB848_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8028 = { sizeof (__StaticArrayInitTypeSizeU3D24_tA66DA9D905CC3FD5269CBA439F53227E088C5962)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_tA66DA9D905CC3FD5269CBA439F53227E088C5962 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8029 = { sizeof (__StaticArrayInitTypeSizeU3D28_t9E81964A7111D1744E54D30D73684C823A4BCFB9)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D28_t9E81964A7111D1744E54D30D73684C823A4BCFB9 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8030 = { sizeof (U3CModuleU3E_t85932CF96972B7D4C02F28C9B1523A5F0B6961A1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8031 = { sizeof (GenuineValidatorExample_t7161B4F6115F89CD8BD3E68EDDE0E2B23167FBDD), -1, sizeof(GenuineValidatorExample_t7161B4F6115F89CD8BD3E68EDDE0E2B23167FBDD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8031[6] = 
{
	GenuineValidatorExample_t7161B4F6115F89CD8BD3E68EDDE0E2B23167FBDD_StaticFields::get_offset_of_StringKey_4(),
	0,
	0,
	0,
	GenuineValidatorExample_t7161B4F6115F89CD8BD3E68EDDE0E2B23167FBDD_StaticFields::get_offset_of_SeparatorLength_8(),
	GenuineValidatorExample_t7161B4F6115F89CD8BD3E68EDDE0E2B23167FBDD::get_offset_of_status_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8032 = { sizeof (U3CPrivateImplementationDetailsU3E_t073529674E8DC05642E31D731B6277B8ECFB65C3), -1, sizeof(U3CPrivateImplementationDetailsU3E_t073529674E8DC05642E31D731B6277B8ECFB65C3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8032[1] = 
{
	U3CPrivateImplementationDetailsU3E_t073529674E8DC05642E31D731B6277B8ECFB65C3_StaticFields::get_offset_of_DFE8865BE0F6D289A72492F3B1D6DC9ED2A6C431_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8033 = { sizeof (__StaticArrayInitTypeSizeU3D10_tA63458BD8CED83456312F196CD9CE7931AA28BF5)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D10_tA63458BD8CED83456312F196CD9CE7931AA28BF5 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8034 = { sizeof (U3CModuleU3E_tFB778DC5508E752CBCDE1D45B96E5B700D73328E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8035 = { sizeof (ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8035[12] = 
{
	0,
	0,
	0,
	ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C::get_offset_of_obscuredTypesExamples_7(),
	ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C::get_offset_of_obscuredPrefsExamples_8(),
	ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C::get_offset_of_detectorsExamples_9(),
	ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C::get_offset_of_codeHashExample_10(),
	ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C::get_offset_of_tabs_11(),
	ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C::get_offset_of_currentPage_12(),
	ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C::get_offset_of_allSimpleObscuredTypes_13(),
	ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C::get_offset_of_savesLock_14(),
	ExamplesGUI_t724140363AAAC1BE52036C2F20F61A57D7D05D8C::get_offset_of_centeredStyle_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8036 = { sizeof (ExamplePage_t7E7B6984293C4A76FA9F9569D2DB37AFE5792C13)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8036[5] = 
{
	ExamplePage_t7E7B6984293C4A76FA9F9569D2DB37AFE5792C13::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8037 = { sizeof (U3CU3Ec__DisplayClass18_0_t69526E59360083F1F61FA93BFB53A626667B6EF2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8037[1] = 
{
	U3CU3Ec__DisplayClass18_0_t69526E59360083F1F61FA93BFB53A626667B6EF2::get_offset_of_types_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8038 = { sizeof (U3CU3Ec_t1A1241E63E745B560F6EE1026B54D5F14155C03A), -1, sizeof(U3CU3Ec_t1A1241E63E745B560F6EE1026B54D5F14155C03A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8038[2] = 
{
	U3CU3Ec_t1A1241E63E745B560F6EE1026B54D5F14155C03A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1A1241E63E745B560F6EE1026B54D5F14155C03A_StaticFields::get_offset_of_U3CU3E9__18_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8039 = { sizeof (InfiniteRotator_tFA6BA16BE7F5E1665489CC31C0C9005040A97DEC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8039[1] = 
{
	InfiniteRotator_tFA6BA16BE7F5E1665489CC31C0C9005040A97DEC::get_offset_of_speed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8040 = { sizeof (InfiniteRotatorReliable_tCB17DE8224CD5B9E8FAE1FF7B7A79B1617E5602D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8040[1] = 
{
	InfiniteRotatorReliable_tCB17DE8224CD5B9E8FAE1FF7B7A79B1617E5602D::get_offset_of_speed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8041 = { sizeof (ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8041[25] = 
{
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_boolTest_4(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_boolIterations_5(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_byteTest_6(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_byteIterations_7(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_shortTest_8(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_shortIterations_9(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_ushortTest_10(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_ushortIterations_11(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_intTest_12(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_intIterations_13(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_uintTest_14(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_uintIterations_15(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_longTest_16(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_longIterations_17(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_floatTest_18(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_floatIterations_19(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_doubleTest_20(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_doubleIterations_21(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_stringTest_22(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_stringIterations_23(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_vector3Test_24(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_vector3Iterations_25(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_prefsTest_26(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_prefsIterations_27(),
	ObscuredPerformanceTests_tDE56CF69FAD4F75529F43F17DF79A05157333F78::get_offset_of_logBuilder_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8042 = { sizeof (CodeHashExample_tFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8042[2] = 
{
	CodeHashExample_tFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1::get_offset_of_savedHash_4(),
	CodeHashExample_tFD3DBFA2E2931C27094EE9BB3C9D0078DE12A6B1::get_offset_of_U3CLastResultU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8043 = { sizeof (DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8043[6] = 
{
	DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C::get_offset_of_injectionDetected_4(),
	DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C::get_offset_of_speedHackDetected_5(),
	DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C::get_offset_of_wrongTimeDetected_6(),
	DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C::get_offset_of_timeCheatingDetected_7(),
	DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C::get_offset_of_obscuredTypeCheatDetected_8(),
	DetectorsExamples_t169C89A6581AB652682A19A0B3537B229482845C::get_offset_of_wallHackCheatDetected_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8044 = { sizeof (ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8044[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063::get_offset_of_regularPrefs_17(),
	ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063::get_offset_of_obscuredPrefs_18(),
	ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063::get_offset_of_savesAlterationDetected_19(),
	ObscuredPrefsExamples_t5044E7EE9EDB3491F0E86094FE8352FA85F54063::get_offset_of_foreignSavesDetected_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8045 = { sizeof (ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8045[16] = 
{
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_regularString_4(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_regularInt_5(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_regularFloat_6(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_regularVector3_7(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_obscuredString_8(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_obscuredInt_9(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_obscuredFloat_10(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_obscuredVector3_11(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_obscuredBool_12(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_obscuredLong_13(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_obscuredDouble_14(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_obscuredVector2_15(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_obscuredDecimal_16(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_obscuredVector2Int_17(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_obscuredVector3Int_18(),
	ObscuredTypesExamples_tF4D8C6CB2C3D4091713677086E6FA82AEC4B3BDA::get_offset_of_logBuilder_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8046 = { sizeof (U3CPrivateImplementationDetailsU3E_tF68BB53B7720992600AB558F953B0B8E44B8C152), -1, sizeof(U3CPrivateImplementationDetailsU3E_tF68BB53B7720992600AB558F953B0B8E44B8C152_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8046[1] = 
{
	U3CPrivateImplementationDetailsU3E_tF68BB53B7720992600AB558F953B0B8E44B8C152_StaticFields::get_offset_of_C67DE1FA1C75585E4A3C2ED631654C95F818EA95_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8047 = { sizeof (U3CModuleU3E_t65223B046E79B9B52A6A555C2EFD1DE08ED208AD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8048 = { sizeof (LoginTask_t03C0EA4FE385D98BE13064ADE4B32C270EFE52D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8048[2] = 
{
	LoginTask_t03C0EA4FE385D98BE13064ADE4B32C270EFE52D8::get_offset_of__gameSparks_7(),
	LoginTask_t03C0EA4FE385D98BE13064ADE4B32C270EFE52D8::get_offset_of__userEvents_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8049 = { sizeof (U3CU3Ec_tBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293), -1, sizeof(U3CU3Ec_tBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8049[2] = 
{
	U3CU3Ec_tBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tBD7A0CF38E4ECCBA1237E7BD12E69B4651EE1293_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8050 = { sizeof (U3CTaskU3Ed__2_t274DDE4CC5BA37737A768B205D96D59CB8A1BDAA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8050[3] = 
{
	U3CTaskU3Ed__2_t274DDE4CC5BA37737A768B205D96D59CB8A1BDAA::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__2_t274DDE4CC5BA37737A768B205D96D59CB8A1BDAA::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__2_t274DDE4CC5BA37737A768B205D96D59CB8A1BDAA::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8051 = { sizeof (LoginTaskResponse_tB278D15FCA56552FD324AF0ECF1DDFE10F0D7DE9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8052 = { sizeof (LoginUtils_tC8DA4A680696A03AEDD404C5D0863FF0A1719CF7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8053 = { sizeof (NetworkCheckSystem_tF7AB64F4FB1DFD7427C4D10C441BF7DE1CC5D938), -1, sizeof(NetworkCheckSystem_tF7AB64F4FB1DFD7427C4D10C441BF7DE1CC5D938_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8053[3] = 
{
	0,
	NetworkCheckSystem_tF7AB64F4FB1DFD7427C4D10C441BF7DE1CC5D938_StaticFields::get_offset_of__lastTimeCheckedNetwork_4(),
	NetworkCheckSystem_tF7AB64F4FB1DFD7427C4D10C441BF7DE1CC5D938_StaticFields::get_offset_of__isOnline_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8054 = { sizeof (BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8054[7] = 
{
	BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2::get_offset_of__container_7(),
	BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2::get_offset_of__userVO_8(),
	BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2::get_offset_of__chaptersSO_9(),
	BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2::get_offset_of__unitsSO_10(),
	BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2::get_offset_of__towersSO_11(),
	BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2::get_offset_of__itemsSO_12(),
	BattleBundleLoaderTask_tF6A69A66DF4FBD3B527961F148301FF11E7E9BC2::get_offset_of__trapsSO_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8055 = { sizeof (U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8055[6] = 
{
	U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E::get_offset_of_U3CU3E4__this_2(),
	U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E::get_offset_of_U3CmodelU3E5__2_3(),
	U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E::get_offset_of_U3CparallelTaskU3E5__3_4(),
	U3CTaskU3Ed__8_t4A983BB41C9B5B44D614E7998619892EB623F24E::get_offset_of_U3CchapterTaskU3E5__4_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8056 = { sizeof (BattleLoaderTask_tEDC6FA66A41F28C5B262EE753494294B090BC0F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8056[1] = 
{
	BattleLoaderTask_tEDC6FA66A41F28C5B262EE753494294B090BC0F3::get_offset_of__diContainer_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8057 = { sizeof (U3CTaskU3Ed__2_t8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8057[5] = 
{
	U3CTaskU3Ed__2_t8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__2_t8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__2_t8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564::get_offset_of_U3CU3E4__this_2(),
	U3CTaskU3Ed__2_t8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564::get_offset_of_U3CbattleBundleLoaderTaskU3E5__2_3(),
	U3CTaskU3Ed__2_t8CC19F7CCD6D9895D75F2DD2EA3EDF55F1DD7564::get_offset_of_U3CspellsWamupTaskU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8058 = { sizeof (SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8058[7] = 
{
	SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F::get_offset_of__container_7(),
	SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F::get_offset_of__userVO_8(),
	SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F::get_offset_of__chaptersSO_9(),
	SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F::get_offset_of__unitsSO_10(),
	SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F::get_offset_of__towersSO_11(),
	SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F::get_offset_of__spellsSO_12(),
	SpellsWarmupTask_t6EF33E8213F49AB4F88568DEB6BAE927C0D5240F::get_offset_of__assets_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8059 = { sizeof (U3CTaskU3Ed__8_tE9E604CAF2979CFC7AA84BCEAD5E3E8AA2985AB8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8059[3] = 
{
	U3CTaskU3Ed__8_tE9E604CAF2979CFC7AA84BCEAD5E3E8AA2985AB8::get_offset_of_U3CU3E1__state_0(),
	U3CTaskU3Ed__8_tE9E604CAF2979CFC7AA84BCEAD5E3E8AA2985AB8::get_offset_of_U3CU3E2__current_1(),
	U3CTaskU3Ed__8_tE9E604CAF2979CFC7AA84BCEAD5E3E8AA2985AB8::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8060 = { sizeof (BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67), -1, sizeof(BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8060[6] = 
{
	BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67::get_offset_of__contexts_4(),
	BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67::get_offset_of__inputFeature_5(),
	BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67::get_offset_of__simulationFeature_6(),
	BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67::get_offset_of__gameModeFeature_7(),
	BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67::get_offset_of__renderFeature_8(),
	BattleEcsInitializer_tA76C29D8A9881FDF3955010F51F9DA4FA7134C67_StaticFields::get_offset_of_IsPaused_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8061 = { sizeof (SphereColliderDebugSystem_t0C02548EEF82E9B4A6A61701F3268AB1C622B53A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8061[1] = 
{
	SphereColliderDebugSystem_t0C02548EEF82E9B4A6A61701F3268AB1C622B53A::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8062 = { sizeof (DebugFeature_t7C4592F930C1C0E5557E9FFE5E27A3B6A708EF75), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8063 = { sizeof (TrapRangeDrawer_t045EE3126EA63A557C3BC426F63CF00F3A6A6818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8063[1] = 
{
	TrapRangeDrawer_t045EE3126EA63A557C3BC426F63CF00F3A6A6818::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8064 = { sizeof (DebugUtils_t10B039A2267A5DD52302EB3328C08BA1961DBAA0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8065 = { sizeof (ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8065[2] = 
{
	ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484::get_offset_of__diContainer_4(),
	ErtugrulFeature_tC4183E2BE6E3B2EDB7F366467E3D8E03B5A00484::get_offset_of__contexts_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8066 = { sizeof (BattleReviveComponent_t274D262B5A3AA78EBC2831B67D816ED2737C8CDA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8067 = { sizeof (GameModeFeature_t26E5E2E6B53B52AFFBC416103BF675284F1F833C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8067[1] = 
{
	GameModeFeature_t26E5E2E6B53B52AFFBC416103BF675284F1F833C::get_offset_of__featuresSwitchConfig_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8068 = { sizeof (GameOverSystem_tCCBF26EA8389B7038B4EA7A8FF0E20299F159050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8068[5] = 
{
	GameOverSystem_tCCBF26EA8389B7038B4EA7A8FF0E20299F159050::get_offset_of__uiLibrary_3(),
	GameOverSystem_tCCBF26EA8389B7038B4EA7A8FF0E20299F159050::get_offset_of__userVO_4(),
	GameOverSystem_tCCBF26EA8389B7038B4EA7A8FF0E20299F159050::get_offset_of__battleEcsInitializer_5(),
	GameOverSystem_tCCBF26EA8389B7038B4EA7A8FF0E20299F159050::get_offset_of__featuresSwitchConfig_6(),
	GameOverSystem_tCCBF26EA8389B7038B4EA7A8FF0E20299F159050::get_offset_of__context_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8069 = { sizeof (BattleLoadingScreenSystem_tA7FAC4ED598184467A80CBDB5125EE4DFA559CF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8069[3] = 
{
	BattleLoadingScreenSystem_tA7FAC4ED598184467A80CBDB5125EE4DFA559CF5::get_offset_of__uiMain_3(),
	BattleLoadingScreenSystem_tA7FAC4ED598184467A80CBDB5125EE4DFA559CF5::get_offset_of__battleSettingsSO_4(),
	BattleLoadingScreenSystem_tA7FAC4ED598184467A80CBDB5125EE4DFA559CF5::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8070 = { sizeof (ObjectivesComponent_t62171C52B3CB11A32E0762719903CA1B7DA0E745), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8070[1] = 
{
	ObjectivesComponent_t62171C52B3CB11A32E0762719903CA1B7DA0E745::get_offset_of_TypeId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8071 = { sizeof (ObjectiveCounterComponent_tF274AC62BA9337BA17F16505BA1D3468AF527FBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8071[1] = 
{
	ObjectiveCounterComponent_tF274AC62BA9337BA17F16505BA1D3468AF527FBF::get_offset_of_Count_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8072 = { sizeof (ObjectiveTypeComponent_t8DC8BD077A26EC2C7532C777F18D592DA64081C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8072[1] = 
{
	ObjectiveTypeComponent_t8DC8BD077A26EC2C7532C777F18D592DA64081C1::get_offset_of_ObjectiveType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8073 = { sizeof (ObjectiveCompleteComponent_t1AA886C7D93C680C37D82B7A0514801EA0F04852), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8074 = { sizeof (ObjectiveStepComponent_tA0B86DE1525A7DEE85B2CFC7BC2A5FCAB783676F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8074[1] = 
{
	ObjectiveStepComponent_tA0B86DE1525A7DEE85B2CFC7BC2A5FCAB783676F::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8075 = { sizeof (ObjectiveCollectableDecrementSystem_t9605F0194DF972FB383E70A99813F832FA67D784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8075[1] = 
{
	ObjectiveCollectableDecrementSystem_t9605F0194DF972FB383E70A99813F832FA67D784::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8076 = { sizeof (ObjectiveCompleteSystem_t9DE671D733D2983D240B650DF86180F97E2D2229), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8076[5] = 
{
	ObjectiveCompleteSystem_t9DE671D733D2983D240B650DF86180F97E2D2229::get_offset_of__uiLibrary_3(),
	ObjectiveCompleteSystem_t9DE671D733D2983D240B650DF86180F97E2D2229::get_offset_of__userVO_4(),
	ObjectiveCompleteSystem_t9DE671D733D2983D240B650DF86180F97E2D2229::get_offset_of__battleSettingsSO_5(),
	ObjectiveCompleteSystem_t9DE671D733D2983D240B650DF86180F97E2D2229::get_offset_of__battleEcsInitializer_6(),
	ObjectiveCompleteSystem_t9DE671D733D2983D240B650DF86180F97E2D2229::get_offset_of__context_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8077 = { sizeof (ObjectiveDecrementSystem_t002E6B98C540BCE77F2F2B3E01CCE5BD11B3CC99), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8077[3] = 
{
	ObjectiveDecrementSystem_t002E6B98C540BCE77F2F2B3E01CCE5BD11B3CC99::get_offset_of__gameSettingsSO_0(),
	ObjectiveDecrementSystem_t002E6B98C540BCE77F2F2B3E01CCE5BD11B3CC99::get_offset_of__context_1(),
	ObjectiveDecrementSystem_t002E6B98C540BCE77F2F2B3E01CCE5BD11B3CC99::get_offset_of__entities_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8078 = { sizeof (ObjectiveDestroyableDecrementerSystem_tC6EB762B99BA92F802D11C144F3DEC193C991D81), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8078[1] = 
{
	ObjectiveDestroyableDecrementerSystem_tC6EB762B99BA92F802D11C144F3DEC193C991D81::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8079 = { sizeof (ObjectivesInitializerSystem_t138508A1854EDA2457D98CC6AE0E6ABAC3B702B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8079[4] = 
{
	ObjectivesInitializerSystem_t138508A1854EDA2457D98CC6AE0E6ABAC3B702B6::get_offset_of__diContainer_0(),
	ObjectivesInitializerSystem_t138508A1854EDA2457D98CC6AE0E6ABAC3B702B6::get_offset_of__userVO_1(),
	ObjectivesInitializerSystem_t138508A1854EDA2457D98CC6AE0E6ABAC3B702B6::get_offset_of__chaptersSO_2(),
	ObjectivesInitializerSystem_t138508A1854EDA2457D98CC6AE0E6ABAC3B702B6::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8080 = { sizeof (ObjectiveTowersDecrementSystem_t6E5588ED14611095B7732B0E85865AD886AC8E05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8080[1] = 
{
	ObjectiveTowersDecrementSystem_t6E5588ED14611095B7732B0E85865AD886AC8E05::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8081 = { sizeof (BattleDurationComponent_tEFEEFB80D9135B611600DAA824B424811E2E5B86), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8081[1] = 
{
	BattleDurationComponent_tEFEEFB80D9135B611600DAA824B424811E2E5B86::get_offset_of_Duration_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8082 = { sizeof (BattleExtraDurationComponent_t44350E8AE350AE0C46860FD3ACBF906270849581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8082[1] = 
{
	BattleExtraDurationComponent_t44350E8AE350AE0C46860FD3ACBF906270849581::get_offset_of_Duration_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8083 = { sizeof (TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8083[6] = 
{
	TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486::get_offset_of__uiLibrary_0(),
	TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486::get_offset_of__diContainer_1(),
	TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486::get_offset_of__userVO_2(),
	TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486::get_offset_of__chaptersSO_3(),
	TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486::get_offset_of__battleEcsInitializer_4(),
	TimePressureSystem_t4FA791F11A0516BF61255D3B11478018665F9486::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8084 = { sizeof (InputFeature_t5A0C995F504E269254656351086E21C0FAB312AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8085 = { sizeof (CustomJoystick_t222CBADA241958A1390BC036FA192E3513119867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8085[2] = 
{
	CustomJoystick_t222CBADA241958A1390BC036FA192E3513119867::get_offset_of__context_10(),
	CustomJoystick_t222CBADA241958A1390BC036FA192E3513119867::get_offset_of_ParentCanvas_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8086 = { sizeof (JoystickComponent_t83263618E7F12C8C23443EB6AAAB252301FEB9BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8086[2] = 
{
	JoystickComponent_t83263618E7F12C8C23443EB6AAAB252301FEB9BD::get_offset_of_Vertical_0(),
	JoystickComponent_t83263618E7F12C8C23443EB6AAAB252301FEB9BD::get_offset_of_Horizontal_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8087 = { sizeof (JoystickInitSystem_t5D264998AF507C48CA3D2EF77E23BE00D6F42335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8087[2] = 
{
	JoystickInitSystem_t5D264998AF507C48CA3D2EF77E23BE00D6F42335::get_offset_of__container_0(),
	JoystickInitSystem_t5D264998AF507C48CA3D2EF77E23BE00D6F42335::get_offset_of__context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8088 = { sizeof (BattleUIInitSystem_t9CE6165C73567C801A6B4B510CAE915849EC7E50), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8088[4] = 
{
	BattleUIInitSystem_t9CE6165C73567C801A6B4B510CAE915849EC7E50::get_offset_of__uiMain_0(),
	BattleUIInitSystem_t9CE6165C73567C801A6B4B510CAE915849EC7E50::get_offset_of__battleSettingsSO_1(),
	BattleUIInitSystem_t9CE6165C73567C801A6B4B510CAE915849EC7E50::get_offset_of__diContainer_2(),
	BattleUIInitSystem_t9CE6165C73567C801A6B4B510CAE915849EC7E50::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8089 = { sizeof (MapItemComponent_tB500381AF954BC6E08A9D449F75126054766BE68), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8089[1] = 
{
	MapItemComponent_tB500381AF954BC6E08A9D449F75126054766BE68::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8090 = { sizeof (TowerComponent_t7F9C6F9201DE020590AF5DF3FCF9FEF5D324F121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8090[1] = 
{
	TowerComponent_t7F9C6F9201DE020590AF5DF3FCF9FEF5D324F121::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8091 = { sizeof (TrapComponent_t028E60EBEFA4B61613AD9F3374AABE359CCADC4F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8091[1] = 
{
	TrapComponent_t028E60EBEFA4B61613AD9F3374AABE359CCADC4F::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8092 = { sizeof (GoalComponent_t9EE6F7FCF9EDEDE9BC7A631CF15CF8ABA51BCBA2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8092[1] = 
{
	GoalComponent_t9EE6F7FCF9EDEDE9BC7A631CF15CF8ABA51BCBA2::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8093 = { sizeof (CartComponent_tCAE70D72887274B93314E7A8037EDCB2812AC04B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8093[1] = 
{
	CartComponent_tCAE70D72887274B93314E7A8037EDCB2812AC04B::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8094 = { sizeof (WallComponent_t9D1052458FC968342C88A87250380745F88CAB88), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8094[1] = 
{
	WallComponent_t9D1052458FC968342C88A87250380745F88CAB88::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8095 = { sizeof (HeroComponent_t0CBCC47B37CE03F28FEF130F385223E3F9E7B8B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8095[1] = 
{
	HeroComponent_t0CBCC47B37CE03F28FEF130F385223E3F9E7B8B7::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8096 = { sizeof (MapItemsSystem_tAD3CB1DE75F592F2250FB8ADF9539F6B51ABF3BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8096[4] = 
{
	MapItemsSystem_tAD3CB1DE75F592F2250FB8ADF9539F6B51ABF3BD::get_offset_of__container_3(),
	MapItemsSystem_tAD3CB1DE75F592F2250FB8ADF9539F6B51ABF3BD::get_offset_of__towersSO_4(),
	MapItemsSystem_tAD3CB1DE75F592F2250FB8ADF9539F6B51ABF3BD::get_offset_of__itemsSO_5(),
	MapItemsSystem_tAD3CB1DE75F592F2250FB8ADF9539F6B51ABF3BD::get_offset_of__context_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8097 = { sizeof (MapLoaderSystem_t67B940E2F5BC2F0CD0B2EAFCF9B43F59680BD641), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8097[4] = 
{
	MapLoaderSystem_t67B940E2F5BC2F0CD0B2EAFCF9B43F59680BD641::get_offset_of__container_0(),
	MapLoaderSystem_t67B940E2F5BC2F0CD0B2EAFCF9B43F59680BD641::get_offset_of__userVO_1(),
	MapLoaderSystem_t67B940E2F5BC2F0CD0B2EAFCF9B43F59680BD641::get_offset_of__chaptersSO_2(),
	MapLoaderSystem_t67B940E2F5BC2F0CD0B2EAFCF9B43F59680BD641::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8098 = { sizeof (MapUtils_t963968652938F061C2D0E7DB28EE306AF7E4C6C1), -1, sizeof(MapUtils_t963968652938F061C2D0E7DB28EE306AF7E4C6C1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8098[1] = 
{
	MapUtils_t963968652938F061C2D0E7DB28EE306AF7E4C6C1_StaticFields::get_offset_of__dictionary_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8099 = { sizeof (SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8099[21] = 
{
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__animator_4(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__spell1_5(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__spell2_6(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__spell3_7(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__spell4_8(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__screenBtn_9(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__normaleBoostBtn_10(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__megaBoostBtn_11(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__spellsSO_12(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__inventorySystem_13(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__battleContainer_14(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__soundSystem_15(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__soundSO_16(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__battleSettingsSO_17(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__userVO_18(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__context_19(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__spellIndexesDictionary_20(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__spellDictionary_21(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__lastSpellModell_22(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__lastSpellIndex_23(),
	SpellUIInitializer_t66B06A358BA3B762376CD13F820EFC620ADFDAD8::get_offset_of__lastTowerTapped_24(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
