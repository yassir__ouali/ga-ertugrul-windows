﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute[]
struct AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.List`1<System.ComponentModel.MaskedTextProvider/CharDescriptor>
struct List_1_t534EACBABA93D935E71506BB9B9529485F1E0966;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IComparer
struct IComparer_t6A5E1BC727C7FF28888E407A797CE1ED92DA8E95;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.ComponentModel.AttributeCollection
struct AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE;
// System.ComponentModel.Component
struct Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473;
// System.ComponentModel.ComponentCollection
struct ComponentCollection_tCA923631B2E55E6A831F9D5CF5231113764CB895;
// System.ComponentModel.ContainerFilterService
struct ContainerFilterService_t4E2FD208B28F83EA50EBC66500B7DCA29D38FA19;
// System.ComponentModel.Design.DesigntimeLicenseContext
struct DesigntimeLicenseContext_tFBEF4E72A1E0E7F03A01D35ADFEE86A23F848AA1;
// System.ComponentModel.DoWorkEventArgs
struct DoWorkEventArgs_t66930F141A21B5E954CE70A24AD0446E2CD5F606;
// System.ComponentModel.EventDescriptor[]
struct EventDescriptorU5BU5D_t813A4DE1E217CA821E8389F24AB1562B0A5458C4;
// System.ComponentModel.EventHandlerList
struct EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4;
// System.ComponentModel.EventHandlerList/ListEntry
struct ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D;
// System.ComponentModel.HandledEventArgs
struct HandledEventArgs_t26999700606DD7EC365648A50C9E3E15EFCAC2DF;
// System.ComponentModel.IComponent
struct IComponent_t2227ADCB5304EAFD55C447E3EE8453437941B4C6;
// System.ComponentModel.IExtenderProvider
struct IExtenderProvider_tD515C7071D35798A56894C8829AFB46AB868CAB7;
// System.ComponentModel.ISite
struct ISite_t6804B48BC23ABB5F4141903F878589BCEF6097A2;
// System.ComponentModel.ISite[]
struct ISiteU5BU5D_t26A1BA57EE8683FC59C3BDD15CA00512F5A520A8;
// System.ComponentModel.LicenseContext
struct LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A;
// System.ComponentModel.ListChangedEventArgs
struct ListChangedEventArgs_t59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84;
// System.ComponentModel.NestedContainer
struct NestedContainer_t08F11932945A6F531D1338CC5A05BAD8D99F8AE9;
// System.ComponentModel.ProgressChangedEventArgs
struct ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F;
// System.ComponentModel.PropertyChangedEventArgs
struct PropertyChangedEventArgs_t90CF85B82F87D594F73F03364494C77592B11F46;
// System.ComponentModel.PropertyChangingEventArgs
struct PropertyChangingEventArgs_tF5EE6A07599A5560DB307BC199120416F0615DBA;
// System.ComponentModel.PropertyDescriptor
struct PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D;
// System.ComponentModel.PropertyDescriptorCollection
struct PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2;
// System.ComponentModel.PropertyDescriptor[]
struct PropertyDescriptorU5BU5D_tBC9023EDDB37EAAAA6FB719C13CEF781F74B2C1F;
// System.ComponentModel.ReflectPropertyDescriptor
struct ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E;
// System.ComponentModel.TypeConverter
struct TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB;
// System.ComponentModel.TypeConverter/StandardValuesCollection
struct StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef CONTAINER_TC98CD69632DEA16FC061454A770CC7D11FFFEEF1_H
#define CONTAINER_TC98CD69632DEA16FC061454A770CC7D11FFFEEF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Container
struct  Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1  : public RuntimeObject
{
public:
	// System.ComponentModel.ISite[] System.ComponentModel.Container::sites
	ISiteU5BU5D_t26A1BA57EE8683FC59C3BDD15CA00512F5A520A8* ___sites_0;
	// System.Int32 System.ComponentModel.Container::siteCount
	int32_t ___siteCount_1;
	// System.ComponentModel.ComponentCollection System.ComponentModel.Container::components
	ComponentCollection_tCA923631B2E55E6A831F9D5CF5231113764CB895 * ___components_2;
	// System.ComponentModel.ContainerFilterService System.ComponentModel.Container::filter
	ContainerFilterService_t4E2FD208B28F83EA50EBC66500B7DCA29D38FA19 * ___filter_3;
	// System.Boolean System.ComponentModel.Container::checkedFilter
	bool ___checkedFilter_4;
	// System.Object System.ComponentModel.Container::syncObj
	RuntimeObject * ___syncObj_5;

public:
	inline static int32_t get_offset_of_sites_0() { return static_cast<int32_t>(offsetof(Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1, ___sites_0)); }
	inline ISiteU5BU5D_t26A1BA57EE8683FC59C3BDD15CA00512F5A520A8* get_sites_0() const { return ___sites_0; }
	inline ISiteU5BU5D_t26A1BA57EE8683FC59C3BDD15CA00512F5A520A8** get_address_of_sites_0() { return &___sites_0; }
	inline void set_sites_0(ISiteU5BU5D_t26A1BA57EE8683FC59C3BDD15CA00512F5A520A8* value)
	{
		___sites_0 = value;
		Il2CppCodeGenWriteBarrier((&___sites_0), value);
	}

	inline static int32_t get_offset_of_siteCount_1() { return static_cast<int32_t>(offsetof(Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1, ___siteCount_1)); }
	inline int32_t get_siteCount_1() const { return ___siteCount_1; }
	inline int32_t* get_address_of_siteCount_1() { return &___siteCount_1; }
	inline void set_siteCount_1(int32_t value)
	{
		___siteCount_1 = value;
	}

	inline static int32_t get_offset_of_components_2() { return static_cast<int32_t>(offsetof(Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1, ___components_2)); }
	inline ComponentCollection_tCA923631B2E55E6A831F9D5CF5231113764CB895 * get_components_2() const { return ___components_2; }
	inline ComponentCollection_tCA923631B2E55E6A831F9D5CF5231113764CB895 ** get_address_of_components_2() { return &___components_2; }
	inline void set_components_2(ComponentCollection_tCA923631B2E55E6A831F9D5CF5231113764CB895 * value)
	{
		___components_2 = value;
		Il2CppCodeGenWriteBarrier((&___components_2), value);
	}

	inline static int32_t get_offset_of_filter_3() { return static_cast<int32_t>(offsetof(Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1, ___filter_3)); }
	inline ContainerFilterService_t4E2FD208B28F83EA50EBC66500B7DCA29D38FA19 * get_filter_3() const { return ___filter_3; }
	inline ContainerFilterService_t4E2FD208B28F83EA50EBC66500B7DCA29D38FA19 ** get_address_of_filter_3() { return &___filter_3; }
	inline void set_filter_3(ContainerFilterService_t4E2FD208B28F83EA50EBC66500B7DCA29D38FA19 * value)
	{
		___filter_3 = value;
		Il2CppCodeGenWriteBarrier((&___filter_3), value);
	}

	inline static int32_t get_offset_of_checkedFilter_4() { return static_cast<int32_t>(offsetof(Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1, ___checkedFilter_4)); }
	inline bool get_checkedFilter_4() const { return ___checkedFilter_4; }
	inline bool* get_address_of_checkedFilter_4() { return &___checkedFilter_4; }
	inline void set_checkedFilter_4(bool value)
	{
		___checkedFilter_4 = value;
	}

	inline static int32_t get_offset_of_syncObj_5() { return static_cast<int32_t>(offsetof(Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1, ___syncObj_5)); }
	inline RuntimeObject * get_syncObj_5() const { return ___syncObj_5; }
	inline RuntimeObject ** get_address_of_syncObj_5() { return &___syncObj_5; }
	inline void set_syncObj_5(RuntimeObject * value)
	{
		___syncObj_5 = value;
		Il2CppCodeGenWriteBarrier((&___syncObj_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTAINER_TC98CD69632DEA16FC061454A770CC7D11FFFEEF1_H
#ifndef EVENTDESCRIPTORCOLLECTION_TB9FC461177F5D7FE0B79268C77569B36F4E1FF37_H
#define EVENTDESCRIPTORCOLLECTION_TB9FC461177F5D7FE0B79268C77569B36F4E1FF37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.EventDescriptorCollection
struct  EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37  : public RuntimeObject
{
public:
	// System.ComponentModel.EventDescriptor[] System.ComponentModel.EventDescriptorCollection::events
	EventDescriptorU5BU5D_t813A4DE1E217CA821E8389F24AB1562B0A5458C4* ___events_0;
	// System.String[] System.ComponentModel.EventDescriptorCollection::namedSort
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___namedSort_1;
	// System.Collections.IComparer System.ComponentModel.EventDescriptorCollection::comparer
	RuntimeObject* ___comparer_2;
	// System.Boolean System.ComponentModel.EventDescriptorCollection::eventsOwned
	bool ___eventsOwned_3;
	// System.Boolean System.ComponentModel.EventDescriptorCollection::needSort
	bool ___needSort_4;
	// System.Int32 System.ComponentModel.EventDescriptorCollection::eventCount
	int32_t ___eventCount_5;
	// System.Boolean System.ComponentModel.EventDescriptorCollection::readOnly
	bool ___readOnly_6;

public:
	inline static int32_t get_offset_of_events_0() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37, ___events_0)); }
	inline EventDescriptorU5BU5D_t813A4DE1E217CA821E8389F24AB1562B0A5458C4* get_events_0() const { return ___events_0; }
	inline EventDescriptorU5BU5D_t813A4DE1E217CA821E8389F24AB1562B0A5458C4** get_address_of_events_0() { return &___events_0; }
	inline void set_events_0(EventDescriptorU5BU5D_t813A4DE1E217CA821E8389F24AB1562B0A5458C4* value)
	{
		___events_0 = value;
		Il2CppCodeGenWriteBarrier((&___events_0), value);
	}

	inline static int32_t get_offset_of_namedSort_1() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37, ___namedSort_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_namedSort_1() const { return ___namedSort_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_namedSort_1() { return &___namedSort_1; }
	inline void set_namedSort_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___namedSort_1 = value;
		Il2CppCodeGenWriteBarrier((&___namedSort_1), value);
	}

	inline static int32_t get_offset_of_comparer_2() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37, ___comparer_2)); }
	inline RuntimeObject* get_comparer_2() const { return ___comparer_2; }
	inline RuntimeObject** get_address_of_comparer_2() { return &___comparer_2; }
	inline void set_comparer_2(RuntimeObject* value)
	{
		___comparer_2 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_2), value);
	}

	inline static int32_t get_offset_of_eventsOwned_3() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37, ___eventsOwned_3)); }
	inline bool get_eventsOwned_3() const { return ___eventsOwned_3; }
	inline bool* get_address_of_eventsOwned_3() { return &___eventsOwned_3; }
	inline void set_eventsOwned_3(bool value)
	{
		___eventsOwned_3 = value;
	}

	inline static int32_t get_offset_of_needSort_4() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37, ___needSort_4)); }
	inline bool get_needSort_4() const { return ___needSort_4; }
	inline bool* get_address_of_needSort_4() { return &___needSort_4; }
	inline void set_needSort_4(bool value)
	{
		___needSort_4 = value;
	}

	inline static int32_t get_offset_of_eventCount_5() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37, ___eventCount_5)); }
	inline int32_t get_eventCount_5() const { return ___eventCount_5; }
	inline int32_t* get_address_of_eventCount_5() { return &___eventCount_5; }
	inline void set_eventCount_5(int32_t value)
	{
		___eventCount_5 = value;
	}

	inline static int32_t get_offset_of_readOnly_6() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37, ___readOnly_6)); }
	inline bool get_readOnly_6() const { return ___readOnly_6; }
	inline bool* get_address_of_readOnly_6() { return &___readOnly_6; }
	inline void set_readOnly_6(bool value)
	{
		___readOnly_6 = value;
	}
};

struct EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37_StaticFields
{
public:
	// System.ComponentModel.EventDescriptorCollection System.ComponentModel.EventDescriptorCollection::Empty
	EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37 * ___Empty_7;

public:
	inline static int32_t get_offset_of_Empty_7() { return static_cast<int32_t>(offsetof(EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37_StaticFields, ___Empty_7)); }
	inline EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37 * get_Empty_7() const { return ___Empty_7; }
	inline EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37 ** get_address_of_Empty_7() { return &___Empty_7; }
	inline void set_Empty_7(EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37 * value)
	{
		___Empty_7 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDESCRIPTORCOLLECTION_TB9FC461177F5D7FE0B79268C77569B36F4E1FF37_H
#ifndef EVENTHANDLERLIST_TFE9EF79E85419EBB2C206CF475E29A9960699BE4_H
#define EVENTHANDLERLIST_TFE9EF79E85419EBB2C206CF475E29A9960699BE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.EventHandlerList
struct  EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4  : public RuntimeObject
{
public:
	// System.ComponentModel.EventHandlerList_ListEntry System.ComponentModel.EventHandlerList::head
	ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D * ___head_0;
	// System.ComponentModel.Component System.ComponentModel.EventHandlerList::parent
	Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473 * ___parent_1;

public:
	inline static int32_t get_offset_of_head_0() { return static_cast<int32_t>(offsetof(EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4, ___head_0)); }
	inline ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D * get_head_0() const { return ___head_0; }
	inline ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D ** get_address_of_head_0() { return &___head_0; }
	inline void set_head_0(ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D * value)
	{
		___head_0 = value;
		Il2CppCodeGenWriteBarrier((&___head_0), value);
	}

	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4, ___parent_1)); }
	inline Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473 * get_parent_1() const { return ___parent_1; }
	inline Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473 ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(Component_t7AEFE153F6778CF52E1981BC3E811A9604B29473 * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier((&___parent_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLERLIST_TFE9EF79E85419EBB2C206CF475E29A9960699BE4_H
#ifndef LISTENTRY_T32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D_H
#define LISTENTRY_T32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.EventHandlerList_ListEntry
struct  ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D  : public RuntimeObject
{
public:
	// System.ComponentModel.EventHandlerList_ListEntry System.ComponentModel.EventHandlerList_ListEntry::next
	ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D * ___next_0;
	// System.Object System.ComponentModel.EventHandlerList_ListEntry::key
	RuntimeObject * ___key_1;
	// System.Delegate System.ComponentModel.EventHandlerList_ListEntry::handler
	Delegate_t * ___handler_2;

public:
	inline static int32_t get_offset_of_next_0() { return static_cast<int32_t>(offsetof(ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D, ___next_0)); }
	inline ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D * get_next_0() const { return ___next_0; }
	inline ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D ** get_address_of_next_0() { return &___next_0; }
	inline void set_next_0(ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D * value)
	{
		___next_0 = value;
		Il2CppCodeGenWriteBarrier((&___next_0), value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D, ___key_1)); }
	inline RuntimeObject * get_key_1() const { return ___key_1; }
	inline RuntimeObject ** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(RuntimeObject * value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier((&___key_1), value);
	}

	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D, ___handler_2)); }
	inline Delegate_t * get_handler_2() const { return ___handler_2; }
	inline Delegate_t ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(Delegate_t * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier((&___handler_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTENTRY_T32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D_H
#ifndef INSTANCECREATIONEDITOR_T87E9919F45C092DA810FAC5AEFD200F26A9D7EEA_H
#define INSTANCECREATIONEDITOR_T87E9919F45C092DA810FAC5AEFD200F26A9D7EEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.InstanceCreationEditor
struct  InstanceCreationEditor_t87E9919F45C092DA810FAC5AEFD200F26A9D7EEA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCECREATIONEDITOR_T87E9919F45C092DA810FAC5AEFD200F26A9D7EEA_H
#ifndef INTSECURITY_T3FC031652848FDE0BB103E7A46BA8DF90840B117_H
#define INTSECURITY_T3FC031652848FDE0BB103E7A46BA8DF90840B117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.IntSecurity
struct  IntSecurity_t3FC031652848FDE0BB103E7A46BA8DF90840B117  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTSECURITY_T3FC031652848FDE0BB103E7A46BA8DF90840B117_H
#ifndef LICENSE_T9011453B97942387364692C7DFAD2DEDB2160F8C_H
#define LICENSE_T9011453B97942387364692C7DFAD2DEDB2160F8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.License
struct  License_t9011453B97942387364692C7DFAD2DEDB2160F8C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LICENSE_T9011453B97942387364692C7DFAD2DEDB2160F8C_H
#ifndef LICENSECONTEXT_TE7068766A5D105EA910974E3F08D0AF7534A806A_H
#define LICENSECONTEXT_TE7068766A5D105EA910974E3F08D0AF7534A806A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.LicenseContext
struct  LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LICENSECONTEXT_TE7068766A5D105EA910974E3F08D0AF7534A806A_H
#ifndef LICENSEMANAGER_T8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_H
#define LICENSEMANAGER_T8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.LicenseManager
struct  LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1  : public RuntimeObject
{
public:

public:
};

struct LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields
{
public:
	// System.Object System.ComponentModel.LicenseManager::selfLock
	RuntimeObject * ___selfLock_0;
	// System.ComponentModel.LicenseContext modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.LicenseManager::context
	LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A * ___context_1;
	// System.Object System.ComponentModel.LicenseManager::contextLockHolder
	RuntimeObject * ___contextLockHolder_2;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.LicenseManager::providers
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___providers_3;
	// System.Collections.Hashtable modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.LicenseManager::providerInstances
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___providerInstances_4;
	// System.Object System.ComponentModel.LicenseManager::internalSyncObject
	RuntimeObject * ___internalSyncObject_5;

public:
	inline static int32_t get_offset_of_selfLock_0() { return static_cast<int32_t>(offsetof(LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields, ___selfLock_0)); }
	inline RuntimeObject * get_selfLock_0() const { return ___selfLock_0; }
	inline RuntimeObject ** get_address_of_selfLock_0() { return &___selfLock_0; }
	inline void set_selfLock_0(RuntimeObject * value)
	{
		___selfLock_0 = value;
		Il2CppCodeGenWriteBarrier((&___selfLock_0), value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields, ___context_1)); }
	inline LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A * get_context_1() const { return ___context_1; }
	inline LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((&___context_1), value);
	}

	inline static int32_t get_offset_of_contextLockHolder_2() { return static_cast<int32_t>(offsetof(LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields, ___contextLockHolder_2)); }
	inline RuntimeObject * get_contextLockHolder_2() const { return ___contextLockHolder_2; }
	inline RuntimeObject ** get_address_of_contextLockHolder_2() { return &___contextLockHolder_2; }
	inline void set_contextLockHolder_2(RuntimeObject * value)
	{
		___contextLockHolder_2 = value;
		Il2CppCodeGenWriteBarrier((&___contextLockHolder_2), value);
	}

	inline static int32_t get_offset_of_providers_3() { return static_cast<int32_t>(offsetof(LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields, ___providers_3)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_providers_3() const { return ___providers_3; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_providers_3() { return &___providers_3; }
	inline void set_providers_3(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___providers_3 = value;
		Il2CppCodeGenWriteBarrier((&___providers_3), value);
	}

	inline static int32_t get_offset_of_providerInstances_4() { return static_cast<int32_t>(offsetof(LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields, ___providerInstances_4)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_providerInstances_4() const { return ___providerInstances_4; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_providerInstances_4() { return &___providerInstances_4; }
	inline void set_providerInstances_4(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___providerInstances_4 = value;
		Il2CppCodeGenWriteBarrier((&___providerInstances_4), value);
	}

	inline static int32_t get_offset_of_internalSyncObject_5() { return static_cast<int32_t>(offsetof(LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields, ___internalSyncObject_5)); }
	inline RuntimeObject * get_internalSyncObject_5() const { return ___internalSyncObject_5; }
	inline RuntimeObject ** get_address_of_internalSyncObject_5() { return &___internalSyncObject_5; }
	inline void set_internalSyncObject_5(RuntimeObject * value)
	{
		___internalSyncObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___internalSyncObject_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LICENSEMANAGER_T8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_H
#ifndef LICENSEINTEROPHELPER_T528682251600D62797521A8A3CCAD83BD1A47C0E_H
#define LICENSEINTEROPHELPER_T528682251600D62797521A8A3CCAD83BD1A47C0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.LicenseManager_LicenseInteropHelper
struct  LicenseInteropHelper_t528682251600D62797521A8A3CCAD83BD1A47C0E  : public RuntimeObject
{
public:
	// System.ComponentModel.Design.DesigntimeLicenseContext System.ComponentModel.LicenseManager_LicenseInteropHelper::helperContext
	DesigntimeLicenseContext_tFBEF4E72A1E0E7F03A01D35ADFEE86A23F848AA1 * ___helperContext_4;
	// System.ComponentModel.LicenseContext System.ComponentModel.LicenseManager_LicenseInteropHelper::savedLicenseContext
	LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A * ___savedLicenseContext_5;
	// System.Type System.ComponentModel.LicenseManager_LicenseInteropHelper::savedType
	Type_t * ___savedType_6;

public:
	inline static int32_t get_offset_of_helperContext_4() { return static_cast<int32_t>(offsetof(LicenseInteropHelper_t528682251600D62797521A8A3CCAD83BD1A47C0E, ___helperContext_4)); }
	inline DesigntimeLicenseContext_tFBEF4E72A1E0E7F03A01D35ADFEE86A23F848AA1 * get_helperContext_4() const { return ___helperContext_4; }
	inline DesigntimeLicenseContext_tFBEF4E72A1E0E7F03A01D35ADFEE86A23F848AA1 ** get_address_of_helperContext_4() { return &___helperContext_4; }
	inline void set_helperContext_4(DesigntimeLicenseContext_tFBEF4E72A1E0E7F03A01D35ADFEE86A23F848AA1 * value)
	{
		___helperContext_4 = value;
		Il2CppCodeGenWriteBarrier((&___helperContext_4), value);
	}

	inline static int32_t get_offset_of_savedLicenseContext_5() { return static_cast<int32_t>(offsetof(LicenseInteropHelper_t528682251600D62797521A8A3CCAD83BD1A47C0E, ___savedLicenseContext_5)); }
	inline LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A * get_savedLicenseContext_5() const { return ___savedLicenseContext_5; }
	inline LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A ** get_address_of_savedLicenseContext_5() { return &___savedLicenseContext_5; }
	inline void set_savedLicenseContext_5(LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A * value)
	{
		___savedLicenseContext_5 = value;
		Il2CppCodeGenWriteBarrier((&___savedLicenseContext_5), value);
	}

	inline static int32_t get_offset_of_savedType_6() { return static_cast<int32_t>(offsetof(LicenseInteropHelper_t528682251600D62797521A8A3CCAD83BD1A47C0E, ___savedType_6)); }
	inline Type_t * get_savedType_6() const { return ___savedType_6; }
	inline Type_t ** get_address_of_savedType_6() { return &___savedType_6; }
	inline void set_savedType_6(Type_t * value)
	{
		___savedType_6 = value;
		Il2CppCodeGenWriteBarrier((&___savedType_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LICENSEINTEROPHELPER_T528682251600D62797521A8A3CCAD83BD1A47C0E_H
#ifndef LICENSEPROVIDER_T586048163F4CCA6D8C6592E696379137A18B0596_H
#define LICENSEPROVIDER_T586048163F4CCA6D8C6592E696379137A18B0596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.LicenseProvider
struct  LicenseProvider_t586048163F4CCA6D8C6592E696379137A18B0596  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LICENSEPROVIDER_T586048163F4CCA6D8C6592E696379137A18B0596_H
#ifndef LISTSORTDESCRIPTIONCOLLECTION_T861BA7E63FFEBDC5C46C048FEA7B60EC580964CB_H
#define LISTSORTDESCRIPTIONCOLLECTION_T861BA7E63FFEBDC5C46C048FEA7B60EC580964CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ListSortDescriptionCollection
struct  ListSortDescriptionCollection_t861BA7E63FFEBDC5C46C048FEA7B60EC580964CB  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.ComponentModel.ListSortDescriptionCollection::sorts
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___sorts_0;

public:
	inline static int32_t get_offset_of_sorts_0() { return static_cast<int32_t>(offsetof(ListSortDescriptionCollection_t861BA7E63FFEBDC5C46C048FEA7B60EC580964CB, ___sorts_0)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_sorts_0() const { return ___sorts_0; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_sorts_0() { return &___sorts_0; }
	inline void set_sorts_0(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___sorts_0 = value;
		Il2CppCodeGenWriteBarrier((&___sorts_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTSORTDESCRIPTIONCOLLECTION_T861BA7E63FFEBDC5C46C048FEA7B60EC580964CB_H
#ifndef MARSHALBYVALUECOMPONENT_TADC0E481D4D19F965AB659F9038A1D7D47FA636B_H
#define MARSHALBYVALUECOMPONENT_TADC0E481D4D19F965AB659F9038A1D7D47FA636B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MarshalByValueComponent
struct  MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B  : public RuntimeObject
{
public:
	// System.ComponentModel.ISite System.ComponentModel.MarshalByValueComponent::site
	RuntimeObject* ___site_1;
	// System.ComponentModel.EventHandlerList System.ComponentModel.MarshalByValueComponent::events
	EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * ___events_2;

public:
	inline static int32_t get_offset_of_site_1() { return static_cast<int32_t>(offsetof(MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B, ___site_1)); }
	inline RuntimeObject* get_site_1() const { return ___site_1; }
	inline RuntimeObject** get_address_of_site_1() { return &___site_1; }
	inline void set_site_1(RuntimeObject* value)
	{
		___site_1 = value;
		Il2CppCodeGenWriteBarrier((&___site_1), value);
	}

	inline static int32_t get_offset_of_events_2() { return static_cast<int32_t>(offsetof(MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B, ___events_2)); }
	inline EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * get_events_2() const { return ___events_2; }
	inline EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 ** get_address_of_events_2() { return &___events_2; }
	inline void set_events_2(EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4 * value)
	{
		___events_2 = value;
		Il2CppCodeGenWriteBarrier((&___events_2), value);
	}
};

struct MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B_StaticFields
{
public:
	// System.Object System.ComponentModel.MarshalByValueComponent::EventDisposed
	RuntimeObject * ___EventDisposed_0;

public:
	inline static int32_t get_offset_of_EventDisposed_0() { return static_cast<int32_t>(offsetof(MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B_StaticFields, ___EventDisposed_0)); }
	inline RuntimeObject * get_EventDisposed_0() const { return ___EventDisposed_0; }
	inline RuntimeObject ** get_address_of_EventDisposed_0() { return &___EventDisposed_0; }
	inline void set_EventDisposed_0(RuntimeObject * value)
	{
		___EventDisposed_0 = value;
		Il2CppCodeGenWriteBarrier((&___EventDisposed_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYVALUECOMPONENT_TADC0E481D4D19F965AB659F9038A1D7D47FA636B_H
#ifndef MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#define MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MemberDescriptor
struct  MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8  : public RuntimeObject
{
public:
	// System.String System.ComponentModel.MemberDescriptor::name
	String_t* ___name_0;
	// System.String System.ComponentModel.MemberDescriptor::displayName
	String_t* ___displayName_1;
	// System.Int32 System.ComponentModel.MemberDescriptor::nameHash
	int32_t ___nameHash_2;
	// System.ComponentModel.AttributeCollection System.ComponentModel.MemberDescriptor::attributeCollection
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * ___attributeCollection_3;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::attributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___attributes_4;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::originalAttributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___originalAttributes_5;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFiltered
	bool ___attributesFiltered_6;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFilled
	bool ___attributesFilled_7;
	// System.Int32 System.ComponentModel.MemberDescriptor::metadataVersion
	int32_t ___metadataVersion_8;
	// System.String System.ComponentModel.MemberDescriptor::category
	String_t* ___category_9;
	// System.String System.ComponentModel.MemberDescriptor::description
	String_t* ___description_10;
	// System.Object System.ComponentModel.MemberDescriptor::lockCookie
	RuntimeObject * ___lockCookie_11;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_displayName_1() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___displayName_1)); }
	inline String_t* get_displayName_1() const { return ___displayName_1; }
	inline String_t** get_address_of_displayName_1() { return &___displayName_1; }
	inline void set_displayName_1(String_t* value)
	{
		___displayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_1), value);
	}

	inline static int32_t get_offset_of_nameHash_2() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___nameHash_2)); }
	inline int32_t get_nameHash_2() const { return ___nameHash_2; }
	inline int32_t* get_address_of_nameHash_2() { return &___nameHash_2; }
	inline void set_nameHash_2(int32_t value)
	{
		___nameHash_2 = value;
	}

	inline static int32_t get_offset_of_attributeCollection_3() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributeCollection_3)); }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * get_attributeCollection_3() const { return ___attributeCollection_3; }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE ** get_address_of_attributeCollection_3() { return &___attributeCollection_3; }
	inline void set_attributeCollection_3(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * value)
	{
		___attributeCollection_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributeCollection_3), value);
	}

	inline static int32_t get_offset_of_attributes_4() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributes_4)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_attributes_4() const { return ___attributes_4; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_attributes_4() { return &___attributes_4; }
	inline void set_attributes_4(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___attributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_4), value);
	}

	inline static int32_t get_offset_of_originalAttributes_5() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___originalAttributes_5)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_originalAttributes_5() const { return ___originalAttributes_5; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_originalAttributes_5() { return &___originalAttributes_5; }
	inline void set_originalAttributes_5(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___originalAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___originalAttributes_5), value);
	}

	inline static int32_t get_offset_of_attributesFiltered_6() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFiltered_6)); }
	inline bool get_attributesFiltered_6() const { return ___attributesFiltered_6; }
	inline bool* get_address_of_attributesFiltered_6() { return &___attributesFiltered_6; }
	inline void set_attributesFiltered_6(bool value)
	{
		___attributesFiltered_6 = value;
	}

	inline static int32_t get_offset_of_attributesFilled_7() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFilled_7)); }
	inline bool get_attributesFilled_7() const { return ___attributesFilled_7; }
	inline bool* get_address_of_attributesFilled_7() { return &___attributesFilled_7; }
	inline void set_attributesFilled_7(bool value)
	{
		___attributesFilled_7 = value;
	}

	inline static int32_t get_offset_of_metadataVersion_8() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___metadataVersion_8)); }
	inline int32_t get_metadataVersion_8() const { return ___metadataVersion_8; }
	inline int32_t* get_address_of_metadataVersion_8() { return &___metadataVersion_8; }
	inline void set_metadataVersion_8(int32_t value)
	{
		___metadataVersion_8 = value;
	}

	inline static int32_t get_offset_of_category_9() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___category_9)); }
	inline String_t* get_category_9() const { return ___category_9; }
	inline String_t** get_address_of_category_9() { return &___category_9; }
	inline void set_category_9(String_t* value)
	{
		___category_9 = value;
		Il2CppCodeGenWriteBarrier((&___category_9), value);
	}

	inline static int32_t get_offset_of_description_10() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___description_10)); }
	inline String_t* get_description_10() const { return ___description_10; }
	inline String_t** get_address_of_description_10() { return &___description_10; }
	inline void set_description_10(String_t* value)
	{
		___description_10 = value;
		Il2CppCodeGenWriteBarrier((&___description_10), value);
	}

	inline static int32_t get_offset_of_lockCookie_11() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___lockCookie_11)); }
	inline RuntimeObject * get_lockCookie_11() const { return ___lockCookie_11; }
	inline RuntimeObject ** get_address_of_lockCookie_11() { return &___lockCookie_11; }
	inline void set_lockCookie_11(RuntimeObject * value)
	{
		___lockCookie_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockCookie_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifndef SITE_T416D1B752B924BEC6C03A474D7DC852F255B3E5A_H
#define SITE_T416D1B752B924BEC6C03A474D7DC852F255B3E5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.NestedContainer_Site
struct  Site_t416D1B752B924BEC6C03A474D7DC852F255B3E5A  : public RuntimeObject
{
public:
	// System.ComponentModel.IComponent System.ComponentModel.NestedContainer_Site::component
	RuntimeObject* ___component_0;
	// System.ComponentModel.NestedContainer System.ComponentModel.NestedContainer_Site::container
	NestedContainer_t08F11932945A6F531D1338CC5A05BAD8D99F8AE9 * ___container_1;
	// System.String System.ComponentModel.NestedContainer_Site::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_component_0() { return static_cast<int32_t>(offsetof(Site_t416D1B752B924BEC6C03A474D7DC852F255B3E5A, ___component_0)); }
	inline RuntimeObject* get_component_0() const { return ___component_0; }
	inline RuntimeObject** get_address_of_component_0() { return &___component_0; }
	inline void set_component_0(RuntimeObject* value)
	{
		___component_0 = value;
		Il2CppCodeGenWriteBarrier((&___component_0), value);
	}

	inline static int32_t get_offset_of_container_1() { return static_cast<int32_t>(offsetof(Site_t416D1B752B924BEC6C03A474D7DC852F255B3E5A, ___container_1)); }
	inline NestedContainer_t08F11932945A6F531D1338CC5A05BAD8D99F8AE9 * get_container_1() const { return ___container_1; }
	inline NestedContainer_t08F11932945A6F531D1338CC5A05BAD8D99F8AE9 ** get_address_of_container_1() { return &___container_1; }
	inline void set_container_1(NestedContainer_t08F11932945A6F531D1338CC5A05BAD8D99F8AE9 * value)
	{
		___container_1 = value;
		Il2CppCodeGenWriteBarrier((&___container_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Site_t416D1B752B924BEC6C03A474D7DC852F255B3E5A, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SITE_T416D1B752B924BEC6C03A474D7DC852F255B3E5A_H
#ifndef PROPERTYDESCRIPTORCOLLECTION_T19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2_H
#define PROPERTYDESCRIPTORCOLLECTION_T19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyDescriptorCollection
struct  PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2  : public RuntimeObject
{
public:
	// System.Collections.IDictionary System.ComponentModel.PropertyDescriptorCollection::cachedFoundProperties
	RuntimeObject* ___cachedFoundProperties_1;
	// System.Boolean System.ComponentModel.PropertyDescriptorCollection::cachedIgnoreCase
	bool ___cachedIgnoreCase_2;
	// System.ComponentModel.PropertyDescriptor[] System.ComponentModel.PropertyDescriptorCollection::properties
	PropertyDescriptorU5BU5D_tBC9023EDDB37EAAAA6FB719C13CEF781F74B2C1F* ___properties_3;
	// System.Int32 System.ComponentModel.PropertyDescriptorCollection::propCount
	int32_t ___propCount_4;
	// System.String[] System.ComponentModel.PropertyDescriptorCollection::namedSort
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___namedSort_5;
	// System.Collections.IComparer System.ComponentModel.PropertyDescriptorCollection::comparer
	RuntimeObject* ___comparer_6;
	// System.Boolean System.ComponentModel.PropertyDescriptorCollection::propsOwned
	bool ___propsOwned_7;
	// System.Boolean System.ComponentModel.PropertyDescriptorCollection::needSort
	bool ___needSort_8;
	// System.Boolean System.ComponentModel.PropertyDescriptorCollection::readOnly
	bool ___readOnly_9;

public:
	inline static int32_t get_offset_of_cachedFoundProperties_1() { return static_cast<int32_t>(offsetof(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2, ___cachedFoundProperties_1)); }
	inline RuntimeObject* get_cachedFoundProperties_1() const { return ___cachedFoundProperties_1; }
	inline RuntimeObject** get_address_of_cachedFoundProperties_1() { return &___cachedFoundProperties_1; }
	inline void set_cachedFoundProperties_1(RuntimeObject* value)
	{
		___cachedFoundProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___cachedFoundProperties_1), value);
	}

	inline static int32_t get_offset_of_cachedIgnoreCase_2() { return static_cast<int32_t>(offsetof(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2, ___cachedIgnoreCase_2)); }
	inline bool get_cachedIgnoreCase_2() const { return ___cachedIgnoreCase_2; }
	inline bool* get_address_of_cachedIgnoreCase_2() { return &___cachedIgnoreCase_2; }
	inline void set_cachedIgnoreCase_2(bool value)
	{
		___cachedIgnoreCase_2 = value;
	}

	inline static int32_t get_offset_of_properties_3() { return static_cast<int32_t>(offsetof(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2, ___properties_3)); }
	inline PropertyDescriptorU5BU5D_tBC9023EDDB37EAAAA6FB719C13CEF781F74B2C1F* get_properties_3() const { return ___properties_3; }
	inline PropertyDescriptorU5BU5D_tBC9023EDDB37EAAAA6FB719C13CEF781F74B2C1F** get_address_of_properties_3() { return &___properties_3; }
	inline void set_properties_3(PropertyDescriptorU5BU5D_tBC9023EDDB37EAAAA6FB719C13CEF781F74B2C1F* value)
	{
		___properties_3 = value;
		Il2CppCodeGenWriteBarrier((&___properties_3), value);
	}

	inline static int32_t get_offset_of_propCount_4() { return static_cast<int32_t>(offsetof(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2, ___propCount_4)); }
	inline int32_t get_propCount_4() const { return ___propCount_4; }
	inline int32_t* get_address_of_propCount_4() { return &___propCount_4; }
	inline void set_propCount_4(int32_t value)
	{
		___propCount_4 = value;
	}

	inline static int32_t get_offset_of_namedSort_5() { return static_cast<int32_t>(offsetof(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2, ___namedSort_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_namedSort_5() const { return ___namedSort_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_namedSort_5() { return &___namedSort_5; }
	inline void set_namedSort_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___namedSort_5 = value;
		Il2CppCodeGenWriteBarrier((&___namedSort_5), value);
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_propsOwned_7() { return static_cast<int32_t>(offsetof(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2, ___propsOwned_7)); }
	inline bool get_propsOwned_7() const { return ___propsOwned_7; }
	inline bool* get_address_of_propsOwned_7() { return &___propsOwned_7; }
	inline void set_propsOwned_7(bool value)
	{
		___propsOwned_7 = value;
	}

	inline static int32_t get_offset_of_needSort_8() { return static_cast<int32_t>(offsetof(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2, ___needSort_8)); }
	inline bool get_needSort_8() const { return ___needSort_8; }
	inline bool* get_address_of_needSort_8() { return &___needSort_8; }
	inline void set_needSort_8(bool value)
	{
		___needSort_8 = value;
	}

	inline static int32_t get_offset_of_readOnly_9() { return static_cast<int32_t>(offsetof(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2, ___readOnly_9)); }
	inline bool get_readOnly_9() const { return ___readOnly_9; }
	inline bool* get_address_of_readOnly_9() { return &___readOnly_9; }
	inline void set_readOnly_9(bool value)
	{
		___readOnly_9 = value;
	}
};

struct PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2_StaticFields
{
public:
	// System.ComponentModel.PropertyDescriptorCollection System.ComponentModel.PropertyDescriptorCollection::Empty
	PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2_StaticFields, ___Empty_0)); }
	inline PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2 * get_Empty_0() const { return ___Empty_0; }
	inline PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYDESCRIPTORCOLLECTION_T19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2_H
#ifndef PROPERTYDESCRIPTORENUMERATOR_T8DE08E0C89BAD17FE8FD71FD3159C80833CE2D9D_H
#define PROPERTYDESCRIPTORENUMERATOR_T8DE08E0C89BAD17FE8FD71FD3159C80833CE2D9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyDescriptorCollection_PropertyDescriptorEnumerator
struct  PropertyDescriptorEnumerator_t8DE08E0C89BAD17FE8FD71FD3159C80833CE2D9D  : public RuntimeObject
{
public:
	// System.ComponentModel.PropertyDescriptorCollection System.ComponentModel.PropertyDescriptorCollection_PropertyDescriptorEnumerator::owner
	PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2 * ___owner_0;
	// System.Int32 System.ComponentModel.PropertyDescriptorCollection_PropertyDescriptorEnumerator::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_owner_0() { return static_cast<int32_t>(offsetof(PropertyDescriptorEnumerator_t8DE08E0C89BAD17FE8FD71FD3159C80833CE2D9D, ___owner_0)); }
	inline PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2 * get_owner_0() const { return ___owner_0; }
	inline PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2 ** get_address_of_owner_0() { return &___owner_0; }
	inline void set_owner_0(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2 * value)
	{
		___owner_0 = value;
		Il2CppCodeGenWriteBarrier((&___owner_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(PropertyDescriptorEnumerator_t8DE08E0C89BAD17FE8FD71FD3159C80833CE2D9D, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYDESCRIPTORENUMERATOR_T8DE08E0C89BAD17FE8FD71FD3159C80833CE2D9D_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef BITVECTOR32_TDE1C7920F117A283D9595A597572E8910691A0F1_H
#define BITVECTOR32_TDE1C7920F117A283D9595A597572E8910691A0F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.BitVector32
struct  BitVector32_tDE1C7920F117A283D9595A597572E8910691A0F1 
{
public:
	// System.UInt32 System.Collections.Specialized.BitVector32::data
	uint32_t ___data_0;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(BitVector32_tDE1C7920F117A283D9595A597572E8910691A0F1, ___data_0)); }
	inline uint32_t get_data_0() const { return ___data_0; }
	inline uint32_t* get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(uint32_t value)
	{
		___data_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITVECTOR32_TDE1C7920F117A283D9595A597572E8910691A0F1_H
#ifndef CANCELEVENTARGS_T2843141F2893A01C11535CD7E38072CA26D7794D_H
#define CANCELEVENTARGS_T2843141F2893A01C11535CD7E38072CA26D7794D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.CancelEventArgs
struct  CancelEventArgs_t2843141F2893A01C11535CD7E38072CA26D7794D  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Boolean System.ComponentModel.CancelEventArgs::cancel
	bool ___cancel_1;

public:
	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(CancelEventArgs_t2843141F2893A01C11535CD7E38072CA26D7794D, ___cancel_1)); }
	inline bool get_cancel_1() const { return ___cancel_1; }
	inline bool* get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(bool value)
	{
		___cancel_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANCELEVENTARGS_T2843141F2893A01C11535CD7E38072CA26D7794D_H
#ifndef DESIGNERCATEGORYATTRIBUTE_TE79E5894EFE62D19C0CD24911AB16B9E996BA7FA_H
#define DESIGNERCATEGORYATTRIBUTE_TE79E5894EFE62D19C0CD24911AB16B9E996BA7FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DesignerCategoryAttribute
struct  DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.DesignerCategoryAttribute::category
	String_t* ___category_0;
	// System.String System.ComponentModel.DesignerCategoryAttribute::typeId
	String_t* ___typeId_1;

public:
	inline static int32_t get_offset_of_category_0() { return static_cast<int32_t>(offsetof(DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA, ___category_0)); }
	inline String_t* get_category_0() const { return ___category_0; }
	inline String_t** get_address_of_category_0() { return &___category_0; }
	inline void set_category_0(String_t* value)
	{
		___category_0 = value;
		Il2CppCodeGenWriteBarrier((&___category_0), value);
	}

	inline static int32_t get_offset_of_typeId_1() { return static_cast<int32_t>(offsetof(DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA, ___typeId_1)); }
	inline String_t* get_typeId_1() const { return ___typeId_1; }
	inline String_t** get_address_of_typeId_1() { return &___typeId_1; }
	inline void set_typeId_1(String_t* value)
	{
		___typeId_1 = value;
		Il2CppCodeGenWriteBarrier((&___typeId_1), value);
	}
};

struct DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA_StaticFields
{
public:
	// System.ComponentModel.DesignerCategoryAttribute System.ComponentModel.DesignerCategoryAttribute::Component
	DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA * ___Component_2;
	// System.ComponentModel.DesignerCategoryAttribute System.ComponentModel.DesignerCategoryAttribute::Default
	DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA * ___Default_3;
	// System.ComponentModel.DesignerCategoryAttribute System.ComponentModel.DesignerCategoryAttribute::Form
	DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA * ___Form_4;
	// System.ComponentModel.DesignerCategoryAttribute System.ComponentModel.DesignerCategoryAttribute::Generic
	DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA * ___Generic_5;

public:
	inline static int32_t get_offset_of_Component_2() { return static_cast<int32_t>(offsetof(DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA_StaticFields, ___Component_2)); }
	inline DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA * get_Component_2() const { return ___Component_2; }
	inline DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA ** get_address_of_Component_2() { return &___Component_2; }
	inline void set_Component_2(DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA * value)
	{
		___Component_2 = value;
		Il2CppCodeGenWriteBarrier((&___Component_2), value);
	}

	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA_StaticFields, ___Default_3)); }
	inline DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA * get_Default_3() const { return ___Default_3; }
	inline DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((&___Default_3), value);
	}

	inline static int32_t get_offset_of_Form_4() { return static_cast<int32_t>(offsetof(DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA_StaticFields, ___Form_4)); }
	inline DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA * get_Form_4() const { return ___Form_4; }
	inline DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA ** get_address_of_Form_4() { return &___Form_4; }
	inline void set_Form_4(DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA * value)
	{
		___Form_4 = value;
		Il2CppCodeGenWriteBarrier((&___Form_4), value);
	}

	inline static int32_t get_offset_of_Generic_5() { return static_cast<int32_t>(offsetof(DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA_StaticFields, ___Generic_5)); }
	inline DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA * get_Generic_5() const { return ___Generic_5; }
	inline DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA ** get_address_of_Generic_5() { return &___Generic_5; }
	inline void set_Generic_5(DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA * value)
	{
		___Generic_5 = value;
		Il2CppCodeGenWriteBarrier((&___Generic_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNERCATEGORYATTRIBUTE_TE79E5894EFE62D19C0CD24911AB16B9E996BA7FA_H
#ifndef DISPLAYNAMEATTRIBUTE_TE54161FBBAB6EF141BE2925404185B82B67C1BCE_H
#define DISPLAYNAMEATTRIBUTE_TE54161FBBAB6EF141BE2925404185B82B67C1BCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DisplayNameAttribute
struct  DisplayNameAttribute_tE54161FBBAB6EF141BE2925404185B82B67C1BCE  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.DisplayNameAttribute::_displayName
	String_t* ____displayName_1;

public:
	inline static int32_t get_offset_of__displayName_1() { return static_cast<int32_t>(offsetof(DisplayNameAttribute_tE54161FBBAB6EF141BE2925404185B82B67C1BCE, ____displayName_1)); }
	inline String_t* get__displayName_1() const { return ____displayName_1; }
	inline String_t** get_address_of__displayName_1() { return &____displayName_1; }
	inline void set__displayName_1(String_t* value)
	{
		____displayName_1 = value;
		Il2CppCodeGenWriteBarrier((&____displayName_1), value);
	}
};

struct DisplayNameAttribute_tE54161FBBAB6EF141BE2925404185B82B67C1BCE_StaticFields
{
public:
	// System.ComponentModel.DisplayNameAttribute System.ComponentModel.DisplayNameAttribute::Default
	DisplayNameAttribute_tE54161FBBAB6EF141BE2925404185B82B67C1BCE * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(DisplayNameAttribute_tE54161FBBAB6EF141BE2925404185B82B67C1BCE_StaticFields, ___Default_0)); }
	inline DisplayNameAttribute_tE54161FBBAB6EF141BE2925404185B82B67C1BCE * get_Default_0() const { return ___Default_0; }
	inline DisplayNameAttribute_tE54161FBBAB6EF141BE2925404185B82B67C1BCE ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(DisplayNameAttribute_tE54161FBBAB6EF141BE2925404185B82B67C1BCE * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYNAMEATTRIBUTE_TE54161FBBAB6EF141BE2925404185B82B67C1BCE_H
#ifndef EDITORATTRIBUTE_TD8A0A6A2F1AC5829C1C1CB563A83B1CD6D6273D5_H
#define EDITORATTRIBUTE_TD8A0A6A2F1AC5829C1C1CB563A83B1CD6D6273D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.EditorAttribute
struct  EditorAttribute_tD8A0A6A2F1AC5829C1C1CB563A83B1CD6D6273D5  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.EditorAttribute::baseTypeName
	String_t* ___baseTypeName_0;
	// System.String System.ComponentModel.EditorAttribute::typeName
	String_t* ___typeName_1;
	// System.String System.ComponentModel.EditorAttribute::typeId
	String_t* ___typeId_2;

public:
	inline static int32_t get_offset_of_baseTypeName_0() { return static_cast<int32_t>(offsetof(EditorAttribute_tD8A0A6A2F1AC5829C1C1CB563A83B1CD6D6273D5, ___baseTypeName_0)); }
	inline String_t* get_baseTypeName_0() const { return ___baseTypeName_0; }
	inline String_t** get_address_of_baseTypeName_0() { return &___baseTypeName_0; }
	inline void set_baseTypeName_0(String_t* value)
	{
		___baseTypeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_0), value);
	}

	inline static int32_t get_offset_of_typeName_1() { return static_cast<int32_t>(offsetof(EditorAttribute_tD8A0A6A2F1AC5829C1C1CB563A83B1CD6D6273D5, ___typeName_1)); }
	inline String_t* get_typeName_1() const { return ___typeName_1; }
	inline String_t** get_address_of_typeName_1() { return &___typeName_1; }
	inline void set_typeName_1(String_t* value)
	{
		___typeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_1), value);
	}

	inline static int32_t get_offset_of_typeId_2() { return static_cast<int32_t>(offsetof(EditorAttribute_tD8A0A6A2F1AC5829C1C1CB563A83B1CD6D6273D5, ___typeId_2)); }
	inline String_t* get_typeId_2() const { return ___typeId_2; }
	inline String_t** get_address_of_typeId_2() { return &___typeId_2; }
	inline void set_typeId_2(String_t* value)
	{
		___typeId_2 = value;
		Il2CppCodeGenWriteBarrier((&___typeId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORATTRIBUTE_TD8A0A6A2F1AC5829C1C1CB563A83B1CD6D6273D5_H
#ifndef EVENTDESCRIPTOR_TAB488D66C0409A1889EE56355848CDA43ED95222_H
#define EVENTDESCRIPTOR_TAB488D66C0409A1889EE56355848CDA43ED95222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.EventDescriptor
struct  EventDescriptor_tAB488D66C0409A1889EE56355848CDA43ED95222  : public MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDESCRIPTOR_TAB488D66C0409A1889EE56355848CDA43ED95222_H
#ifndef EXTENDERPROVIDEDPROPERTYATTRIBUTE_T6C2D566DED3E19650BDE6A721FB2ADF877670D68_H
#define EXTENDERPROVIDEDPROPERTYATTRIBUTE_T6C2D566DED3E19650BDE6A721FB2ADF877670D68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ExtenderProvidedPropertyAttribute
struct  ExtenderProvidedPropertyAttribute_t6C2D566DED3E19650BDE6A721FB2ADF877670D68  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.ComponentModel.PropertyDescriptor System.ComponentModel.ExtenderProvidedPropertyAttribute::extenderProperty
	PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D * ___extenderProperty_0;
	// System.ComponentModel.IExtenderProvider System.ComponentModel.ExtenderProvidedPropertyAttribute::provider
	RuntimeObject* ___provider_1;
	// System.Type System.ComponentModel.ExtenderProvidedPropertyAttribute::receiverType
	Type_t * ___receiverType_2;

public:
	inline static int32_t get_offset_of_extenderProperty_0() { return static_cast<int32_t>(offsetof(ExtenderProvidedPropertyAttribute_t6C2D566DED3E19650BDE6A721FB2ADF877670D68, ___extenderProperty_0)); }
	inline PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D * get_extenderProperty_0() const { return ___extenderProperty_0; }
	inline PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D ** get_address_of_extenderProperty_0() { return &___extenderProperty_0; }
	inline void set_extenderProperty_0(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D * value)
	{
		___extenderProperty_0 = value;
		Il2CppCodeGenWriteBarrier((&___extenderProperty_0), value);
	}

	inline static int32_t get_offset_of_provider_1() { return static_cast<int32_t>(offsetof(ExtenderProvidedPropertyAttribute_t6C2D566DED3E19650BDE6A721FB2ADF877670D68, ___provider_1)); }
	inline RuntimeObject* get_provider_1() const { return ___provider_1; }
	inline RuntimeObject** get_address_of_provider_1() { return &___provider_1; }
	inline void set_provider_1(RuntimeObject* value)
	{
		___provider_1 = value;
		Il2CppCodeGenWriteBarrier((&___provider_1), value);
	}

	inline static int32_t get_offset_of_receiverType_2() { return static_cast<int32_t>(offsetof(ExtenderProvidedPropertyAttribute_t6C2D566DED3E19650BDE6A721FB2ADF877670D68, ___receiverType_2)); }
	inline Type_t * get_receiverType_2() const { return ___receiverType_2; }
	inline Type_t ** get_address_of_receiverType_2() { return &___receiverType_2; }
	inline void set_receiverType_2(Type_t * value)
	{
		___receiverType_2 = value;
		Il2CppCodeGenWriteBarrier((&___receiverType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDERPROVIDEDPROPERTYATTRIBUTE_T6C2D566DED3E19650BDE6A721FB2ADF877670D68_H
#ifndef HANDLEDEVENTARGS_T26999700606DD7EC365648A50C9E3E15EFCAC2DF_H
#define HANDLEDEVENTARGS_T26999700606DD7EC365648A50C9E3E15EFCAC2DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.HandledEventArgs
struct  HandledEventArgs_t26999700606DD7EC365648A50C9E3E15EFCAC2DF  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Boolean System.ComponentModel.HandledEventArgs::handled
	bool ___handled_1;

public:
	inline static int32_t get_offset_of_handled_1() { return static_cast<int32_t>(offsetof(HandledEventArgs_t26999700606DD7EC365648A50C9E3E15EFCAC2DF, ___handled_1)); }
	inline bool get_handled_1() const { return ___handled_1; }
	inline bool* get_address_of_handled_1() { return &___handled_1; }
	inline void set_handled_1(bool value)
	{
		___handled_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLEDEVENTARGS_T26999700606DD7EC365648A50C9E3E15EFCAC2DF_H
#ifndef IMMUTABLEOBJECTATTRIBUTE_TEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492_H
#define IMMUTABLEOBJECTATTRIBUTE_TEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ImmutableObjectAttribute
struct  ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.ImmutableObjectAttribute::immutable
	bool ___immutable_3;

public:
	inline static int32_t get_offset_of_immutable_3() { return static_cast<int32_t>(offsetof(ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492, ___immutable_3)); }
	inline bool get_immutable_3() const { return ___immutable_3; }
	inline bool* get_address_of_immutable_3() { return &___immutable_3; }
	inline void set_immutable_3(bool value)
	{
		___immutable_3 = value;
	}
};

struct ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492_StaticFields
{
public:
	// System.ComponentModel.ImmutableObjectAttribute System.ComponentModel.ImmutableObjectAttribute::Yes
	ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492 * ___Yes_0;
	// System.ComponentModel.ImmutableObjectAttribute System.ComponentModel.ImmutableObjectAttribute::No
	ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492 * ___No_1;
	// System.ComponentModel.ImmutableObjectAttribute System.ComponentModel.ImmutableObjectAttribute::Default
	ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492 * ___Default_2;

public:
	inline static int32_t get_offset_of_Yes_0() { return static_cast<int32_t>(offsetof(ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492_StaticFields, ___Yes_0)); }
	inline ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492 * get_Yes_0() const { return ___Yes_0; }
	inline ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492 ** get_address_of_Yes_0() { return &___Yes_0; }
	inline void set_Yes_0(ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492 * value)
	{
		___Yes_0 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_0), value);
	}

	inline static int32_t get_offset_of_No_1() { return static_cast<int32_t>(offsetof(ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492_StaticFields, ___No_1)); }
	inline ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492 * get_No_1() const { return ___No_1; }
	inline ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492 ** get_address_of_No_1() { return &___No_1; }
	inline void set_No_1(ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492 * value)
	{
		___No_1 = value;
		Il2CppCodeGenWriteBarrier((&___No_1), value);
	}

	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492_StaticFields, ___Default_2)); }
	inline ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492 * get_Default_2() const { return ___Default_2; }
	inline ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMMUTABLEOBJECTATTRIBUTE_TEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492_H
#ifndef INITIALIZATIONEVENTATTRIBUTE_T66A3424618601597D5221545AA27CCAE24DC19B1_H
#define INITIALIZATIONEVENTATTRIBUTE_T66A3424618601597D5221545AA27CCAE24DC19B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.InitializationEventAttribute
struct  InitializationEventAttribute_t66A3424618601597D5221545AA27CCAE24DC19B1  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.InitializationEventAttribute::eventName
	String_t* ___eventName_0;

public:
	inline static int32_t get_offset_of_eventName_0() { return static_cast<int32_t>(offsetof(InitializationEventAttribute_t66A3424618601597D5221545AA27CCAE24DC19B1, ___eventName_0)); }
	inline String_t* get_eventName_0() const { return ___eventName_0; }
	inline String_t** get_address_of_eventName_0() { return &___eventName_0; }
	inline void set_eventName_0(String_t* value)
	{
		___eventName_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZATIONEVENTATTRIBUTE_T66A3424618601597D5221545AA27CCAE24DC19B1_H
#ifndef INSTALLERTYPEATTRIBUTE_T69E2B80148C986610B0F88883070C0BAD2A2B755_H
#define INSTALLERTYPEATTRIBUTE_T69E2B80148C986610B0F88883070C0BAD2A2B755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.InstallerTypeAttribute
struct  InstallerTypeAttribute_t69E2B80148C986610B0F88883070C0BAD2A2B755  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.InstallerTypeAttribute::_typeName
	String_t* ____typeName_0;

public:
	inline static int32_t get_offset_of__typeName_0() { return static_cast<int32_t>(offsetof(InstallerTypeAttribute_t69E2B80148C986610B0F88883070C0BAD2A2B755, ____typeName_0)); }
	inline String_t* get__typeName_0() const { return ____typeName_0; }
	inline String_t** get_address_of__typeName_0() { return &____typeName_0; }
	inline void set__typeName_0(String_t* value)
	{
		____typeName_0 = value;
		Il2CppCodeGenWriteBarrier((&____typeName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLERTYPEATTRIBUTE_T69E2B80148C986610B0F88883070C0BAD2A2B755_H
#ifndef LICFILELICENSEPROVIDER_T458B091F8501C2890FD617BB025AB49DC494076F_H
#define LICFILELICENSEPROVIDER_T458B091F8501C2890FD617BB025AB49DC494076F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.LicFileLicenseProvider
struct  LicFileLicenseProvider_t458B091F8501C2890FD617BB025AB49DC494076F  : public LicenseProvider_t586048163F4CCA6D8C6592E696379137A18B0596
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LICFILELICENSEPROVIDER_T458B091F8501C2890FD617BB025AB49DC494076F_H
#ifndef LICFILELICENSE_T5AF4C870B7DF7EE91D6C0FA5DA1354763EF9E5FF_H
#define LICFILELICENSE_T5AF4C870B7DF7EE91D6C0FA5DA1354763EF9E5FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.LicFileLicenseProvider_LicFileLicense
struct  LicFileLicense_t5AF4C870B7DF7EE91D6C0FA5DA1354763EF9E5FF  : public License_t9011453B97942387364692C7DFAD2DEDB2160F8C
{
public:
	// System.String System.ComponentModel.LicFileLicenseProvider_LicFileLicense::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(LicFileLicense_t5AF4C870B7DF7EE91D6C0FA5DA1354763EF9E5FF, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LICFILELICENSE_T5AF4C870B7DF7EE91D6C0FA5DA1354763EF9E5FF_H
#ifndef LICENSEPROVIDERATTRIBUTE_T27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D_H
#define LICENSEPROVIDERATTRIBUTE_T27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.LicenseProviderAttribute
struct  LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Type System.ComponentModel.LicenseProviderAttribute::licenseProviderType
	Type_t * ___licenseProviderType_1;
	// System.String System.ComponentModel.LicenseProviderAttribute::licenseProviderName
	String_t* ___licenseProviderName_2;

public:
	inline static int32_t get_offset_of_licenseProviderType_1() { return static_cast<int32_t>(offsetof(LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D, ___licenseProviderType_1)); }
	inline Type_t * get_licenseProviderType_1() const { return ___licenseProviderType_1; }
	inline Type_t ** get_address_of_licenseProviderType_1() { return &___licenseProviderType_1; }
	inline void set_licenseProviderType_1(Type_t * value)
	{
		___licenseProviderType_1 = value;
		Il2CppCodeGenWriteBarrier((&___licenseProviderType_1), value);
	}

	inline static int32_t get_offset_of_licenseProviderName_2() { return static_cast<int32_t>(offsetof(LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D, ___licenseProviderName_2)); }
	inline String_t* get_licenseProviderName_2() const { return ___licenseProviderName_2; }
	inline String_t** get_address_of_licenseProviderName_2() { return &___licenseProviderName_2; }
	inline void set_licenseProviderName_2(String_t* value)
	{
		___licenseProviderName_2 = value;
		Il2CppCodeGenWriteBarrier((&___licenseProviderName_2), value);
	}
};

struct LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D_StaticFields
{
public:
	// System.ComponentModel.LicenseProviderAttribute System.ComponentModel.LicenseProviderAttribute::Default
	LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D * ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D_StaticFields, ___Default_0)); }
	inline LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D * get_Default_0() const { return ___Default_0; }
	inline LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D ** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D * value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LICENSEPROVIDERATTRIBUTE_T27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D_H
#ifndef LISTBINDABLEATTRIBUTE_T455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2_H
#define LISTBINDABLEATTRIBUTE_T455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ListBindableAttribute
struct  ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.ListBindableAttribute::listBindable
	bool ___listBindable_3;
	// System.Boolean System.ComponentModel.ListBindableAttribute::isDefault
	bool ___isDefault_4;

public:
	inline static int32_t get_offset_of_listBindable_3() { return static_cast<int32_t>(offsetof(ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2, ___listBindable_3)); }
	inline bool get_listBindable_3() const { return ___listBindable_3; }
	inline bool* get_address_of_listBindable_3() { return &___listBindable_3; }
	inline void set_listBindable_3(bool value)
	{
		___listBindable_3 = value;
	}

	inline static int32_t get_offset_of_isDefault_4() { return static_cast<int32_t>(offsetof(ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2, ___isDefault_4)); }
	inline bool get_isDefault_4() const { return ___isDefault_4; }
	inline bool* get_address_of_isDefault_4() { return &___isDefault_4; }
	inline void set_isDefault_4(bool value)
	{
		___isDefault_4 = value;
	}
};

struct ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2_StaticFields
{
public:
	// System.ComponentModel.ListBindableAttribute System.ComponentModel.ListBindableAttribute::Yes
	ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2 * ___Yes_0;
	// System.ComponentModel.ListBindableAttribute System.ComponentModel.ListBindableAttribute::No
	ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2 * ___No_1;
	// System.ComponentModel.ListBindableAttribute System.ComponentModel.ListBindableAttribute::Default
	ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2 * ___Default_2;

public:
	inline static int32_t get_offset_of_Yes_0() { return static_cast<int32_t>(offsetof(ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2_StaticFields, ___Yes_0)); }
	inline ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2 * get_Yes_0() const { return ___Yes_0; }
	inline ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2 ** get_address_of_Yes_0() { return &___Yes_0; }
	inline void set_Yes_0(ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2 * value)
	{
		___Yes_0 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_0), value);
	}

	inline static int32_t get_offset_of_No_1() { return static_cast<int32_t>(offsetof(ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2_StaticFields, ___No_1)); }
	inline ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2 * get_No_1() const { return ___No_1; }
	inline ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2 ** get_address_of_No_1() { return &___No_1; }
	inline void set_No_1(ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2 * value)
	{
		___No_1 = value;
		Il2CppCodeGenWriteBarrier((&___No_1), value);
	}

	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2_StaticFields, ___Default_2)); }
	inline ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2 * get_Default_2() const { return ___Default_2; }
	inline ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTBINDABLEATTRIBUTE_T455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2_H
#ifndef LOCALIZABLEATTRIBUTE_T4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F_H
#define LOCALIZABLEATTRIBUTE_T4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.LocalizableAttribute
struct  LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.LocalizableAttribute::isLocalizable
	bool ___isLocalizable_0;

public:
	inline static int32_t get_offset_of_isLocalizable_0() { return static_cast<int32_t>(offsetof(LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F, ___isLocalizable_0)); }
	inline bool get_isLocalizable_0() const { return ___isLocalizable_0; }
	inline bool* get_address_of_isLocalizable_0() { return &___isLocalizable_0; }
	inline void set_isLocalizable_0(bool value)
	{
		___isLocalizable_0 = value;
	}
};

struct LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F_StaticFields
{
public:
	// System.ComponentModel.LocalizableAttribute System.ComponentModel.LocalizableAttribute::Yes
	LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F * ___Yes_1;
	// System.ComponentModel.LocalizableAttribute System.ComponentModel.LocalizableAttribute::No
	LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F * ___No_2;
	// System.ComponentModel.LocalizableAttribute System.ComponentModel.LocalizableAttribute::Default
	LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F * ___Default_3;

public:
	inline static int32_t get_offset_of_Yes_1() { return static_cast<int32_t>(offsetof(LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F_StaticFields, ___Yes_1)); }
	inline LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F * get_Yes_1() const { return ___Yes_1; }
	inline LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F ** get_address_of_Yes_1() { return &___Yes_1; }
	inline void set_Yes_1(LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F * value)
	{
		___Yes_1 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_1), value);
	}

	inline static int32_t get_offset_of_No_2() { return static_cast<int32_t>(offsetof(LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F_StaticFields, ___No_2)); }
	inline LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F * get_No_2() const { return ___No_2; }
	inline LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F ** get_address_of_No_2() { return &___No_2; }
	inline void set_No_2(LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F * value)
	{
		___No_2 = value;
		Il2CppCodeGenWriteBarrier((&___No_2), value);
	}

	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F_StaticFields, ___Default_3)); }
	inline LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F * get_Default_3() const { return ___Default_3; }
	inline LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((&___Default_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZABLEATTRIBUTE_T4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F_H
#ifndef LOOKUPBINDINGPROPERTIESATTRIBUTE_TFB30823108AD583FC2406504B9601C38AE83FD1B_H
#define LOOKUPBINDINGPROPERTIESATTRIBUTE_TFB30823108AD583FC2406504B9601C38AE83FD1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.LookupBindingPropertiesAttribute
struct  LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.LookupBindingPropertiesAttribute::dataSource
	String_t* ___dataSource_0;
	// System.String System.ComponentModel.LookupBindingPropertiesAttribute::displayMember
	String_t* ___displayMember_1;
	// System.String System.ComponentModel.LookupBindingPropertiesAttribute::valueMember
	String_t* ___valueMember_2;
	// System.String System.ComponentModel.LookupBindingPropertiesAttribute::lookupMember
	String_t* ___lookupMember_3;

public:
	inline static int32_t get_offset_of_dataSource_0() { return static_cast<int32_t>(offsetof(LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B, ___dataSource_0)); }
	inline String_t* get_dataSource_0() const { return ___dataSource_0; }
	inline String_t** get_address_of_dataSource_0() { return &___dataSource_0; }
	inline void set_dataSource_0(String_t* value)
	{
		___dataSource_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataSource_0), value);
	}

	inline static int32_t get_offset_of_displayMember_1() { return static_cast<int32_t>(offsetof(LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B, ___displayMember_1)); }
	inline String_t* get_displayMember_1() const { return ___displayMember_1; }
	inline String_t** get_address_of_displayMember_1() { return &___displayMember_1; }
	inline void set_displayMember_1(String_t* value)
	{
		___displayMember_1 = value;
		Il2CppCodeGenWriteBarrier((&___displayMember_1), value);
	}

	inline static int32_t get_offset_of_valueMember_2() { return static_cast<int32_t>(offsetof(LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B, ___valueMember_2)); }
	inline String_t* get_valueMember_2() const { return ___valueMember_2; }
	inline String_t** get_address_of_valueMember_2() { return &___valueMember_2; }
	inline void set_valueMember_2(String_t* value)
	{
		___valueMember_2 = value;
		Il2CppCodeGenWriteBarrier((&___valueMember_2), value);
	}

	inline static int32_t get_offset_of_lookupMember_3() { return static_cast<int32_t>(offsetof(LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B, ___lookupMember_3)); }
	inline String_t* get_lookupMember_3() const { return ___lookupMember_3; }
	inline String_t** get_address_of_lookupMember_3() { return &___lookupMember_3; }
	inline void set_lookupMember_3(String_t* value)
	{
		___lookupMember_3 = value;
		Il2CppCodeGenWriteBarrier((&___lookupMember_3), value);
	}
};

struct LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B_StaticFields
{
public:
	// System.ComponentModel.LookupBindingPropertiesAttribute System.ComponentModel.LookupBindingPropertiesAttribute::Default
	LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B * ___Default_4;

public:
	inline static int32_t get_offset_of_Default_4() { return static_cast<int32_t>(offsetof(LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B_StaticFields, ___Default_4)); }
	inline LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B * get_Default_4() const { return ___Default_4; }
	inline LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B ** get_address_of_Default_4() { return &___Default_4; }
	inline void set_Default_4(LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B * value)
	{
		___Default_4 = value;
		Il2CppCodeGenWriteBarrier((&___Default_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOKUPBINDINGPROPERTIESATTRIBUTE_TFB30823108AD583FC2406504B9601C38AE83FD1B_H
#ifndef MERGABLEPROPERTYATTRIBUTE_TB1134B55C33FBE33AD138D5E5CBDF4BAC6014909_H
#define MERGABLEPROPERTYATTRIBUTE_TB1134B55C33FBE33AD138D5E5CBDF4BAC6014909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MergablePropertyAttribute
struct  MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.MergablePropertyAttribute::allowMerge
	bool ___allowMerge_3;

public:
	inline static int32_t get_offset_of_allowMerge_3() { return static_cast<int32_t>(offsetof(MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909, ___allowMerge_3)); }
	inline bool get_allowMerge_3() const { return ___allowMerge_3; }
	inline bool* get_address_of_allowMerge_3() { return &___allowMerge_3; }
	inline void set_allowMerge_3(bool value)
	{
		___allowMerge_3 = value;
	}
};

struct MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909_StaticFields
{
public:
	// System.ComponentModel.MergablePropertyAttribute System.ComponentModel.MergablePropertyAttribute::Yes
	MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909 * ___Yes_0;
	// System.ComponentModel.MergablePropertyAttribute System.ComponentModel.MergablePropertyAttribute::No
	MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909 * ___No_1;
	// System.ComponentModel.MergablePropertyAttribute System.ComponentModel.MergablePropertyAttribute::Default
	MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909 * ___Default_2;

public:
	inline static int32_t get_offset_of_Yes_0() { return static_cast<int32_t>(offsetof(MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909_StaticFields, ___Yes_0)); }
	inline MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909 * get_Yes_0() const { return ___Yes_0; }
	inline MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909 ** get_address_of_Yes_0() { return &___Yes_0; }
	inline void set_Yes_0(MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909 * value)
	{
		___Yes_0 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_0), value);
	}

	inline static int32_t get_offset_of_No_1() { return static_cast<int32_t>(offsetof(MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909_StaticFields, ___No_1)); }
	inline MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909 * get_No_1() const { return ___No_1; }
	inline MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909 ** get_address_of_No_1() { return &___No_1; }
	inline void set_No_1(MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909 * value)
	{
		___No_1 = value;
		Il2CppCodeGenWriteBarrier((&___No_1), value);
	}

	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909_StaticFields, ___Default_2)); }
	inline MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909 * get_Default_2() const { return ___Default_2; }
	inline MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MERGABLEPROPERTYATTRIBUTE_TB1134B55C33FBE33AD138D5E5CBDF4BAC6014909_H
#ifndef NESTEDCONTAINER_T08F11932945A6F531D1338CC5A05BAD8D99F8AE9_H
#define NESTEDCONTAINER_T08F11932945A6F531D1338CC5A05BAD8D99F8AE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.NestedContainer
struct  NestedContainer_t08F11932945A6F531D1338CC5A05BAD8D99F8AE9  : public Container_tC98CD69632DEA16FC061454A770CC7D11FFFEEF1
{
public:
	// System.ComponentModel.IComponent System.ComponentModel.NestedContainer::_owner
	RuntimeObject* ____owner_6;

public:
	inline static int32_t get_offset_of__owner_6() { return static_cast<int32_t>(offsetof(NestedContainer_t08F11932945A6F531D1338CC5A05BAD8D99F8AE9, ____owner_6)); }
	inline RuntimeObject* get__owner_6() const { return ____owner_6; }
	inline RuntimeObject** get_address_of__owner_6() { return &____owner_6; }
	inline void set__owner_6(RuntimeObject* value)
	{
		____owner_6 = value;
		Il2CppCodeGenWriteBarrier((&____owner_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NESTEDCONTAINER_T08F11932945A6F531D1338CC5A05BAD8D99F8AE9_H
#ifndef PASSWORDPROPERTYTEXTATTRIBUTE_TF8F54903B400BCD43A3561510E32FA16FB1B33C1_H
#define PASSWORDPROPERTYTEXTATTRIBUTE_TF8F54903B400BCD43A3561510E32FA16FB1B33C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PasswordPropertyTextAttribute
struct  PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.Boolean System.ComponentModel.PasswordPropertyTextAttribute::_password
	bool ____password_3;

public:
	inline static int32_t get_offset_of__password_3() { return static_cast<int32_t>(offsetof(PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1, ____password_3)); }
	inline bool get__password_3() const { return ____password_3; }
	inline bool* get_address_of__password_3() { return &____password_3; }
	inline void set__password_3(bool value)
	{
		____password_3 = value;
	}
};

struct PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1_StaticFields
{
public:
	// System.ComponentModel.PasswordPropertyTextAttribute System.ComponentModel.PasswordPropertyTextAttribute::Yes
	PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1 * ___Yes_0;
	// System.ComponentModel.PasswordPropertyTextAttribute System.ComponentModel.PasswordPropertyTextAttribute::No
	PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1 * ___No_1;
	// System.ComponentModel.PasswordPropertyTextAttribute System.ComponentModel.PasswordPropertyTextAttribute::Default
	PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1 * ___Default_2;

public:
	inline static int32_t get_offset_of_Yes_0() { return static_cast<int32_t>(offsetof(PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1_StaticFields, ___Yes_0)); }
	inline PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1 * get_Yes_0() const { return ___Yes_0; }
	inline PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1 ** get_address_of_Yes_0() { return &___Yes_0; }
	inline void set_Yes_0(PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1 * value)
	{
		___Yes_0 = value;
		Il2CppCodeGenWriteBarrier((&___Yes_0), value);
	}

	inline static int32_t get_offset_of_No_1() { return static_cast<int32_t>(offsetof(PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1_StaticFields, ___No_1)); }
	inline PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1 * get_No_1() const { return ___No_1; }
	inline PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1 ** get_address_of_No_1() { return &___No_1; }
	inline void set_No_1(PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1 * value)
	{
		___No_1 = value;
		Il2CppCodeGenWriteBarrier((&___No_1), value);
	}

	inline static int32_t get_offset_of_Default_2() { return static_cast<int32_t>(offsetof(PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1_StaticFields, ___Default_2)); }
	inline PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1 * get_Default_2() const { return ___Default_2; }
	inline PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1 ** get_address_of_Default_2() { return &___Default_2; }
	inline void set_Default_2(PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1 * value)
	{
		___Default_2 = value;
		Il2CppCodeGenWriteBarrier((&___Default_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PASSWORDPROPERTYTEXTATTRIBUTE_TF8F54903B400BCD43A3561510E32FA16FB1B33C1_H
#ifndef PROGRESSCHANGEDEVENTARGS_TC79597AB8E4151EDFF627F615FE36B8D91811F3F_H
#define PROGRESSCHANGEDEVENTARGS_TC79597AB8E4151EDFF627F615FE36B8D91811F3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ProgressChangedEventArgs
struct  ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Int32 System.ComponentModel.ProgressChangedEventArgs::progressPercentage
	int32_t ___progressPercentage_1;
	// System.Object System.ComponentModel.ProgressChangedEventArgs::userState
	RuntimeObject * ___userState_2;

public:
	inline static int32_t get_offset_of_progressPercentage_1() { return static_cast<int32_t>(offsetof(ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F, ___progressPercentage_1)); }
	inline int32_t get_progressPercentage_1() const { return ___progressPercentage_1; }
	inline int32_t* get_address_of_progressPercentage_1() { return &___progressPercentage_1; }
	inline void set_progressPercentage_1(int32_t value)
	{
		___progressPercentage_1 = value;
	}

	inline static int32_t get_offset_of_userState_2() { return static_cast<int32_t>(offsetof(ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F, ___userState_2)); }
	inline RuntimeObject * get_userState_2() const { return ___userState_2; }
	inline RuntimeObject ** get_address_of_userState_2() { return &___userState_2; }
	inline void set_userState_2(RuntimeObject * value)
	{
		___userState_2 = value;
		Il2CppCodeGenWriteBarrier((&___userState_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSCHANGEDEVENTARGS_TC79597AB8E4151EDFF627F615FE36B8D91811F3F_H
#ifndef PROPERTYCHANGEDEVENTARGS_T90CF85B82F87D594F73F03364494C77592B11F46_H
#define PROPERTYCHANGEDEVENTARGS_T90CF85B82F87D594F73F03364494C77592B11F46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyChangedEventArgs
struct  PropertyChangedEventArgs_t90CF85B82F87D594F73F03364494C77592B11F46  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String System.ComponentModel.PropertyChangedEventArgs::propertyName
	String_t* ___propertyName_1;

public:
	inline static int32_t get_offset_of_propertyName_1() { return static_cast<int32_t>(offsetof(PropertyChangedEventArgs_t90CF85B82F87D594F73F03364494C77592B11F46, ___propertyName_1)); }
	inline String_t* get_propertyName_1() const { return ___propertyName_1; }
	inline String_t** get_address_of_propertyName_1() { return &___propertyName_1; }
	inline void set_propertyName_1(String_t* value)
	{
		___propertyName_1 = value;
		Il2CppCodeGenWriteBarrier((&___propertyName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYCHANGEDEVENTARGS_T90CF85B82F87D594F73F03364494C77592B11F46_H
#ifndef PROPERTYCHANGINGEVENTARGS_TF5EE6A07599A5560DB307BC199120416F0615DBA_H
#define PROPERTYCHANGINGEVENTARGS_TF5EE6A07599A5560DB307BC199120416F0615DBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyChangingEventArgs
struct  PropertyChangingEventArgs_tF5EE6A07599A5560DB307BC199120416F0615DBA  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.String System.ComponentModel.PropertyChangingEventArgs::propertyName
	String_t* ___propertyName_1;

public:
	inline static int32_t get_offset_of_propertyName_1() { return static_cast<int32_t>(offsetof(PropertyChangingEventArgs_tF5EE6A07599A5560DB307BC199120416F0615DBA, ___propertyName_1)); }
	inline String_t* get_propertyName_1() const { return ___propertyName_1; }
	inline String_t** get_address_of_propertyName_1() { return &___propertyName_1; }
	inline void set_propertyName_1(String_t* value)
	{
		___propertyName_1 = value;
		Il2CppCodeGenWriteBarrier((&___propertyName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYCHANGINGEVENTARGS_TF5EE6A07599A5560DB307BC199120416F0615DBA_H
#ifndef PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#define PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyDescriptor
struct  PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D  : public MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8
{
public:
	// System.ComponentModel.TypeConverter System.ComponentModel.PropertyDescriptor::converter
	TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * ___converter_12;
	// System.Collections.Hashtable System.ComponentModel.PropertyDescriptor::valueChangedHandlers
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___valueChangedHandlers_13;
	// System.Object[] System.ComponentModel.PropertyDescriptor::editors
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___editors_14;
	// System.Type[] System.ComponentModel.PropertyDescriptor::editorTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___editorTypes_15;
	// System.Int32 System.ComponentModel.PropertyDescriptor::editorCount
	int32_t ___editorCount_16;

public:
	inline static int32_t get_offset_of_converter_12() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___converter_12)); }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * get_converter_12() const { return ___converter_12; }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB ** get_address_of_converter_12() { return &___converter_12; }
	inline void set_converter_12(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * value)
	{
		___converter_12 = value;
		Il2CppCodeGenWriteBarrier((&___converter_12), value);
	}

	inline static int32_t get_offset_of_valueChangedHandlers_13() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___valueChangedHandlers_13)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_valueChangedHandlers_13() const { return ___valueChangedHandlers_13; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_valueChangedHandlers_13() { return &___valueChangedHandlers_13; }
	inline void set_valueChangedHandlers_13(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___valueChangedHandlers_13 = value;
		Il2CppCodeGenWriteBarrier((&___valueChangedHandlers_13), value);
	}

	inline static int32_t get_offset_of_editors_14() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editors_14)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_editors_14() const { return ___editors_14; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_editors_14() { return &___editors_14; }
	inline void set_editors_14(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___editors_14 = value;
		Il2CppCodeGenWriteBarrier((&___editors_14), value);
	}

	inline static int32_t get_offset_of_editorTypes_15() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorTypes_15)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_editorTypes_15() const { return ___editorTypes_15; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_editorTypes_15() { return &___editorTypes_15; }
	inline void set_editorTypes_15(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___editorTypes_15 = value;
		Il2CppCodeGenWriteBarrier((&___editorTypes_15), value);
	}

	inline static int32_t get_offset_of_editorCount_16() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorCount_16)); }
	inline int32_t get_editorCount_16() const { return ___editorCount_16; }
	inline int32_t* get_address_of_editorCount_16() { return &___editorCount_16; }
	inline void set_editorCount_16(int32_t value)
	{
		___editorCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifndef PROVIDEPROPERTYATTRIBUTE_TF58AE8F88029C5D1249A8A149173482A9BB08BC3_H
#define PROVIDEPROPERTYATTRIBUTE_TF58AE8F88029C5D1249A8A149173482A9BB08BC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ProvidePropertyAttribute
struct  ProvidePropertyAttribute_tF58AE8F88029C5D1249A8A149173482A9BB08BC3  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String System.ComponentModel.ProvidePropertyAttribute::propertyName
	String_t* ___propertyName_0;
	// System.String System.ComponentModel.ProvidePropertyAttribute::receiverTypeName
	String_t* ___receiverTypeName_1;

public:
	inline static int32_t get_offset_of_propertyName_0() { return static_cast<int32_t>(offsetof(ProvidePropertyAttribute_tF58AE8F88029C5D1249A8A149173482A9BB08BC3, ___propertyName_0)); }
	inline String_t* get_propertyName_0() const { return ___propertyName_0; }
	inline String_t** get_address_of_propertyName_0() { return &___propertyName_0; }
	inline void set_propertyName_0(String_t* value)
	{
		___propertyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyName_0), value);
	}

	inline static int32_t get_offset_of_receiverTypeName_1() { return static_cast<int32_t>(offsetof(ProvidePropertyAttribute_tF58AE8F88029C5D1249A8A149173482A9BB08BC3, ___receiverTypeName_1)); }
	inline String_t* get_receiverTypeName_1() const { return ___receiverTypeName_1; }
	inline String_t** get_address_of_receiverTypeName_1() { return &___receiverTypeName_1; }
	inline void set_receiverTypeName_1(String_t* value)
	{
		___receiverTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___receiverTypeName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDEPROPERTYATTRIBUTE_TF58AE8F88029C5D1249A8A149173482A9BB08BC3_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#define ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ArgumentException
struct  ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_paramName_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGUMENTEXCEPTION_TEDCD16F20A09ECE461C3DA766C16EDA8864057D1_H
#ifndef DESIGNERSERIALIZATIONVISIBILITY_TCD99EB7FAAE0F69CABCFCE53E16C39DDCB2FFC5A_H
#define DESIGNERSERIALIZATIONVISIBILITY_TCD99EB7FAAE0F69CABCFCE53E16C39DDCB2FFC5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DesignerSerializationVisibility
struct  DesignerSerializationVisibility_tCD99EB7FAAE0F69CABCFCE53E16C39DDCB2FFC5A 
{
public:
	// System.Int32 System.ComponentModel.DesignerSerializationVisibility::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DesignerSerializationVisibility_tCD99EB7FAAE0F69CABCFCE53E16C39DDCB2FFC5A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNERSERIALIZATIONVISIBILITY_TCD99EB7FAAE0F69CABCFCE53E16C39DDCB2FFC5A_H
#ifndef DOWORKEVENTARGS_T66930F141A21B5E954CE70A24AD0446E2CD5F606_H
#define DOWORKEVENTARGS_T66930F141A21B5E954CE70A24AD0446E2CD5F606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DoWorkEventArgs
struct  DoWorkEventArgs_t66930F141A21B5E954CE70A24AD0446E2CD5F606  : public CancelEventArgs_t2843141F2893A01C11535CD7E38072CA26D7794D
{
public:
	// System.Object System.ComponentModel.DoWorkEventArgs::result
	RuntimeObject * ___result_2;
	// System.Object System.ComponentModel.DoWorkEventArgs::argument
	RuntimeObject * ___argument_3;

public:
	inline static int32_t get_offset_of_result_2() { return static_cast<int32_t>(offsetof(DoWorkEventArgs_t66930F141A21B5E954CE70A24AD0446E2CD5F606, ___result_2)); }
	inline RuntimeObject * get_result_2() const { return ___result_2; }
	inline RuntimeObject ** get_address_of_result_2() { return &___result_2; }
	inline void set_result_2(RuntimeObject * value)
	{
		___result_2 = value;
		Il2CppCodeGenWriteBarrier((&___result_2), value);
	}

	inline static int32_t get_offset_of_argument_3() { return static_cast<int32_t>(offsetof(DoWorkEventArgs_t66930F141A21B5E954CE70A24AD0446E2CD5F606, ___argument_3)); }
	inline RuntimeObject * get_argument_3() const { return ___argument_3; }
	inline RuntimeObject ** get_address_of_argument_3() { return &___argument_3; }
	inline void set_argument_3(RuntimeObject * value)
	{
		___argument_3 = value;
		Il2CppCodeGenWriteBarrier((&___argument_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWORKEVENTARGS_T66930F141A21B5E954CE70A24AD0446E2CD5F606_H
#ifndef EDITORBROWSABLESTATE_T8EAF9BADAAE7DA735C235280DF4B8974EAA39C1B_H
#define EDITORBROWSABLESTATE_T8EAF9BADAAE7DA735C235280DF4B8974EAA39C1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.EditorBrowsableState
struct  EditorBrowsableState_t8EAF9BADAAE7DA735C235280DF4B8974EAA39C1B 
{
public:
	// System.Int32 System.ComponentModel.EditorBrowsableState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EditorBrowsableState_t8EAF9BADAAE7DA735C235280DF4B8974EAA39C1B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORBROWSABLESTATE_T8EAF9BADAAE7DA735C235280DF4B8974EAA39C1B_H
#ifndef EXTENDEDPROPERTYDESCRIPTOR_TABDBA511CEE5442C0B06059799418904896E0CF6_H
#define EXTENDEDPROPERTYDESCRIPTOR_TABDBA511CEE5442C0B06059799418904896E0CF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ExtendedPropertyDescriptor
struct  ExtendedPropertyDescriptor_tABDBA511CEE5442C0B06059799418904896E0CF6  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:
	// System.ComponentModel.ReflectPropertyDescriptor System.ComponentModel.ExtendedPropertyDescriptor::extenderInfo
	ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E * ___extenderInfo_17;
	// System.ComponentModel.IExtenderProvider System.ComponentModel.ExtendedPropertyDescriptor::provider
	RuntimeObject* ___provider_18;

public:
	inline static int32_t get_offset_of_extenderInfo_17() { return static_cast<int32_t>(offsetof(ExtendedPropertyDescriptor_tABDBA511CEE5442C0B06059799418904896E0CF6, ___extenderInfo_17)); }
	inline ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E * get_extenderInfo_17() const { return ___extenderInfo_17; }
	inline ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E ** get_address_of_extenderInfo_17() { return &___extenderInfo_17; }
	inline void set_extenderInfo_17(ReflectPropertyDescriptor_t4DC7A4205276D400462B36386EEDEEBA6289C11E * value)
	{
		___extenderInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___extenderInfo_17), value);
	}

	inline static int32_t get_offset_of_provider_18() { return static_cast<int32_t>(offsetof(ExtendedPropertyDescriptor_tABDBA511CEE5442C0B06059799418904896E0CF6, ___provider_18)); }
	inline RuntimeObject* get_provider_18() const { return ___provider_18; }
	inline RuntimeObject** get_address_of_provider_18() { return &___provider_18; }
	inline void set_provider_18(RuntimeObject* value)
	{
		___provider_18 = value;
		Il2CppCodeGenWriteBarrier((&___provider_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDEDPROPERTYDESCRIPTOR_TABDBA511CEE5442C0B06059799418904896E0CF6_H
#ifndef LICENSEEXCEPTION_T68C548BE94D4AD7F82C5AD5E710AC18F50F30F9D_H
#define LICENSEEXCEPTION_T68C548BE94D4AD7F82C5AD5E710AC18F50F30F9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.LicenseException
struct  LicenseException_t68C548BE94D4AD7F82C5AD5E710AC18F50F30F9D  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.Type System.ComponentModel.LicenseException::type
	Type_t * ___type_17;
	// System.Object System.ComponentModel.LicenseException::instance
	RuntimeObject * ___instance_18;

public:
	inline static int32_t get_offset_of_type_17() { return static_cast<int32_t>(offsetof(LicenseException_t68C548BE94D4AD7F82C5AD5E710AC18F50F30F9D, ___type_17)); }
	inline Type_t * get_type_17() const { return ___type_17; }
	inline Type_t ** get_address_of_type_17() { return &___type_17; }
	inline void set_type_17(Type_t * value)
	{
		___type_17 = value;
		Il2CppCodeGenWriteBarrier((&___type_17), value);
	}

	inline static int32_t get_offset_of_instance_18() { return static_cast<int32_t>(offsetof(LicenseException_t68C548BE94D4AD7F82C5AD5E710AC18F50F30F9D, ___instance_18)); }
	inline RuntimeObject * get_instance_18() const { return ___instance_18; }
	inline RuntimeObject ** get_address_of_instance_18() { return &___instance_18; }
	inline void set_instance_18(RuntimeObject * value)
	{
		___instance_18 = value;
		Il2CppCodeGenWriteBarrier((&___instance_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LICENSEEXCEPTION_T68C548BE94D4AD7F82C5AD5E710AC18F50F30F9D_H
#ifndef LICENSEUSAGEMODE_T2DEE0920CFCD0D47584E94D5D6D9AFEC4F82883F_H
#define LICENSEUSAGEMODE_T2DEE0920CFCD0D47584E94D5D6D9AFEC4F82883F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.LicenseUsageMode
struct  LicenseUsageMode_t2DEE0920CFCD0D47584E94D5D6D9AFEC4F82883F 
{
public:
	// System.Int32 System.ComponentModel.LicenseUsageMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LicenseUsageMode_t2DEE0920CFCD0D47584E94D5D6D9AFEC4F82883F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LICENSEUSAGEMODE_T2DEE0920CFCD0D47584E94D5D6D9AFEC4F82883F_H
#ifndef LISTCHANGEDTYPE_TE885755B4022635C38211A1864E2A851CEE324F8_H
#define LISTCHANGEDTYPE_TE885755B4022635C38211A1864E2A851CEE324F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ListChangedType
struct  ListChangedType_tE885755B4022635C38211A1864E2A851CEE324F8 
{
public:
	// System.Int32 System.ComponentModel.ListChangedType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ListChangedType_tE885755B4022635C38211A1864E2A851CEE324F8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTCHANGEDTYPE_TE885755B4022635C38211A1864E2A851CEE324F8_H
#ifndef LISTSORTDIRECTION_T345E309405F5CEFC88A1F4F34ACE985B9690FF20_H
#define LISTSORTDIRECTION_T345E309405F5CEFC88A1F4F34ACE985B9690FF20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ListSortDirection
struct  ListSortDirection_t345E309405F5CEFC88A1F4F34ACE985B9690FF20 
{
public:
	// System.Int32 System.ComponentModel.ListSortDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ListSortDirection_t345E309405F5CEFC88A1F4F34ACE985B9690FF20, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTSORTDIRECTION_T345E309405F5CEFC88A1F4F34ACE985B9690FF20_H
#ifndef MASKEDTEXTPROVIDER_T63841E659F7586B73FFADC5F0315F78E4D32B197_H
#define MASKEDTEXTPROVIDER_T63841E659F7586B73FFADC5F0315F78E4D32B197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MaskedTextProvider
struct  MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197  : public RuntimeObject
{
public:
	// System.Collections.Specialized.BitVector32 System.ComponentModel.MaskedTextProvider::flagState
	BitVector32_tDE1C7920F117A283D9595A597572E8910691A0F1  ___flagState_18;
	// System.Globalization.CultureInfo System.ComponentModel.MaskedTextProvider::culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___culture_19;
	// System.Text.StringBuilder System.ComponentModel.MaskedTextProvider::testString
	StringBuilder_t * ___testString_20;
	// System.Int32 System.ComponentModel.MaskedTextProvider::assignedCharCount
	int32_t ___assignedCharCount_21;
	// System.Int32 System.ComponentModel.MaskedTextProvider::requiredCharCount
	int32_t ___requiredCharCount_22;
	// System.Int32 System.ComponentModel.MaskedTextProvider::requiredEditChars
	int32_t ___requiredEditChars_23;
	// System.Int32 System.ComponentModel.MaskedTextProvider::optionalEditChars
	int32_t ___optionalEditChars_24;
	// System.String System.ComponentModel.MaskedTextProvider::mask
	String_t* ___mask_25;
	// System.Char System.ComponentModel.MaskedTextProvider::passwordChar
	Il2CppChar ___passwordChar_26;
	// System.Char System.ComponentModel.MaskedTextProvider::promptChar
	Il2CppChar ___promptChar_27;
	// System.Collections.Generic.List`1<System.ComponentModel.MaskedTextProvider_CharDescriptor> System.ComponentModel.MaskedTextProvider::stringDescriptor
	List_1_t534EACBABA93D935E71506BB9B9529485F1E0966 * ___stringDescriptor_28;

public:
	inline static int32_t get_offset_of_flagState_18() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197, ___flagState_18)); }
	inline BitVector32_tDE1C7920F117A283D9595A597572E8910691A0F1  get_flagState_18() const { return ___flagState_18; }
	inline BitVector32_tDE1C7920F117A283D9595A597572E8910691A0F1 * get_address_of_flagState_18() { return &___flagState_18; }
	inline void set_flagState_18(BitVector32_tDE1C7920F117A283D9595A597572E8910691A0F1  value)
	{
		___flagState_18 = value;
	}

	inline static int32_t get_offset_of_culture_19() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197, ___culture_19)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_culture_19() const { return ___culture_19; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_culture_19() { return &___culture_19; }
	inline void set_culture_19(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___culture_19 = value;
		Il2CppCodeGenWriteBarrier((&___culture_19), value);
	}

	inline static int32_t get_offset_of_testString_20() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197, ___testString_20)); }
	inline StringBuilder_t * get_testString_20() const { return ___testString_20; }
	inline StringBuilder_t ** get_address_of_testString_20() { return &___testString_20; }
	inline void set_testString_20(StringBuilder_t * value)
	{
		___testString_20 = value;
		Il2CppCodeGenWriteBarrier((&___testString_20), value);
	}

	inline static int32_t get_offset_of_assignedCharCount_21() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197, ___assignedCharCount_21)); }
	inline int32_t get_assignedCharCount_21() const { return ___assignedCharCount_21; }
	inline int32_t* get_address_of_assignedCharCount_21() { return &___assignedCharCount_21; }
	inline void set_assignedCharCount_21(int32_t value)
	{
		___assignedCharCount_21 = value;
	}

	inline static int32_t get_offset_of_requiredCharCount_22() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197, ___requiredCharCount_22)); }
	inline int32_t get_requiredCharCount_22() const { return ___requiredCharCount_22; }
	inline int32_t* get_address_of_requiredCharCount_22() { return &___requiredCharCount_22; }
	inline void set_requiredCharCount_22(int32_t value)
	{
		___requiredCharCount_22 = value;
	}

	inline static int32_t get_offset_of_requiredEditChars_23() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197, ___requiredEditChars_23)); }
	inline int32_t get_requiredEditChars_23() const { return ___requiredEditChars_23; }
	inline int32_t* get_address_of_requiredEditChars_23() { return &___requiredEditChars_23; }
	inline void set_requiredEditChars_23(int32_t value)
	{
		___requiredEditChars_23 = value;
	}

	inline static int32_t get_offset_of_optionalEditChars_24() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197, ___optionalEditChars_24)); }
	inline int32_t get_optionalEditChars_24() const { return ___optionalEditChars_24; }
	inline int32_t* get_address_of_optionalEditChars_24() { return &___optionalEditChars_24; }
	inline void set_optionalEditChars_24(int32_t value)
	{
		___optionalEditChars_24 = value;
	}

	inline static int32_t get_offset_of_mask_25() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197, ___mask_25)); }
	inline String_t* get_mask_25() const { return ___mask_25; }
	inline String_t** get_address_of_mask_25() { return &___mask_25; }
	inline void set_mask_25(String_t* value)
	{
		___mask_25 = value;
		Il2CppCodeGenWriteBarrier((&___mask_25), value);
	}

	inline static int32_t get_offset_of_passwordChar_26() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197, ___passwordChar_26)); }
	inline Il2CppChar get_passwordChar_26() const { return ___passwordChar_26; }
	inline Il2CppChar* get_address_of_passwordChar_26() { return &___passwordChar_26; }
	inline void set_passwordChar_26(Il2CppChar value)
	{
		___passwordChar_26 = value;
	}

	inline static int32_t get_offset_of_promptChar_27() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197, ___promptChar_27)); }
	inline Il2CppChar get_promptChar_27() const { return ___promptChar_27; }
	inline Il2CppChar* get_address_of_promptChar_27() { return &___promptChar_27; }
	inline void set_promptChar_27(Il2CppChar value)
	{
		___promptChar_27 = value;
	}

	inline static int32_t get_offset_of_stringDescriptor_28() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197, ___stringDescriptor_28)); }
	inline List_1_t534EACBABA93D935E71506BB9B9529485F1E0966 * get_stringDescriptor_28() const { return ___stringDescriptor_28; }
	inline List_1_t534EACBABA93D935E71506BB9B9529485F1E0966 ** get_address_of_stringDescriptor_28() { return &___stringDescriptor_28; }
	inline void set_stringDescriptor_28(List_1_t534EACBABA93D935E71506BB9B9529485F1E0966 * value)
	{
		___stringDescriptor_28 = value;
		Il2CppCodeGenWriteBarrier((&___stringDescriptor_28), value);
	}
};

struct MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields
{
public:
	// System.Int32 System.ComponentModel.MaskedTextProvider::ASCII_ONLY
	int32_t ___ASCII_ONLY_10;
	// System.Int32 System.ComponentModel.MaskedTextProvider::ALLOW_PROMPT_AS_INPUT
	int32_t ___ALLOW_PROMPT_AS_INPUT_11;
	// System.Int32 System.ComponentModel.MaskedTextProvider::INCLUDE_PROMPT
	int32_t ___INCLUDE_PROMPT_12;
	// System.Int32 System.ComponentModel.MaskedTextProvider::INCLUDE_LITERALS
	int32_t ___INCLUDE_LITERALS_13;
	// System.Int32 System.ComponentModel.MaskedTextProvider::RESET_ON_PROMPT
	int32_t ___RESET_ON_PROMPT_14;
	// System.Int32 System.ComponentModel.MaskedTextProvider::RESET_ON_LITERALS
	int32_t ___RESET_ON_LITERALS_15;
	// System.Int32 System.ComponentModel.MaskedTextProvider::SKIP_SPACE
	int32_t ___SKIP_SPACE_16;
	// System.Type System.ComponentModel.MaskedTextProvider::maskTextProviderType
	Type_t * ___maskTextProviderType_17;

public:
	inline static int32_t get_offset_of_ASCII_ONLY_10() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields, ___ASCII_ONLY_10)); }
	inline int32_t get_ASCII_ONLY_10() const { return ___ASCII_ONLY_10; }
	inline int32_t* get_address_of_ASCII_ONLY_10() { return &___ASCII_ONLY_10; }
	inline void set_ASCII_ONLY_10(int32_t value)
	{
		___ASCII_ONLY_10 = value;
	}

	inline static int32_t get_offset_of_ALLOW_PROMPT_AS_INPUT_11() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields, ___ALLOW_PROMPT_AS_INPUT_11)); }
	inline int32_t get_ALLOW_PROMPT_AS_INPUT_11() const { return ___ALLOW_PROMPT_AS_INPUT_11; }
	inline int32_t* get_address_of_ALLOW_PROMPT_AS_INPUT_11() { return &___ALLOW_PROMPT_AS_INPUT_11; }
	inline void set_ALLOW_PROMPT_AS_INPUT_11(int32_t value)
	{
		___ALLOW_PROMPT_AS_INPUT_11 = value;
	}

	inline static int32_t get_offset_of_INCLUDE_PROMPT_12() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields, ___INCLUDE_PROMPT_12)); }
	inline int32_t get_INCLUDE_PROMPT_12() const { return ___INCLUDE_PROMPT_12; }
	inline int32_t* get_address_of_INCLUDE_PROMPT_12() { return &___INCLUDE_PROMPT_12; }
	inline void set_INCLUDE_PROMPT_12(int32_t value)
	{
		___INCLUDE_PROMPT_12 = value;
	}

	inline static int32_t get_offset_of_INCLUDE_LITERALS_13() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields, ___INCLUDE_LITERALS_13)); }
	inline int32_t get_INCLUDE_LITERALS_13() const { return ___INCLUDE_LITERALS_13; }
	inline int32_t* get_address_of_INCLUDE_LITERALS_13() { return &___INCLUDE_LITERALS_13; }
	inline void set_INCLUDE_LITERALS_13(int32_t value)
	{
		___INCLUDE_LITERALS_13 = value;
	}

	inline static int32_t get_offset_of_RESET_ON_PROMPT_14() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields, ___RESET_ON_PROMPT_14)); }
	inline int32_t get_RESET_ON_PROMPT_14() const { return ___RESET_ON_PROMPT_14; }
	inline int32_t* get_address_of_RESET_ON_PROMPT_14() { return &___RESET_ON_PROMPT_14; }
	inline void set_RESET_ON_PROMPT_14(int32_t value)
	{
		___RESET_ON_PROMPT_14 = value;
	}

	inline static int32_t get_offset_of_RESET_ON_LITERALS_15() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields, ___RESET_ON_LITERALS_15)); }
	inline int32_t get_RESET_ON_LITERALS_15() const { return ___RESET_ON_LITERALS_15; }
	inline int32_t* get_address_of_RESET_ON_LITERALS_15() { return &___RESET_ON_LITERALS_15; }
	inline void set_RESET_ON_LITERALS_15(int32_t value)
	{
		___RESET_ON_LITERALS_15 = value;
	}

	inline static int32_t get_offset_of_SKIP_SPACE_16() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields, ___SKIP_SPACE_16)); }
	inline int32_t get_SKIP_SPACE_16() const { return ___SKIP_SPACE_16; }
	inline int32_t* get_address_of_SKIP_SPACE_16() { return &___SKIP_SPACE_16; }
	inline void set_SKIP_SPACE_16(int32_t value)
	{
		___SKIP_SPACE_16 = value;
	}

	inline static int32_t get_offset_of_maskTextProviderType_17() { return static_cast<int32_t>(offsetof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields, ___maskTextProviderType_17)); }
	inline Type_t * get_maskTextProviderType_17() const { return ___maskTextProviderType_17; }
	inline Type_t ** get_address_of_maskTextProviderType_17() { return &___maskTextProviderType_17; }
	inline void set_maskTextProviderType_17(Type_t * value)
	{
		___maskTextProviderType_17 = value;
		Il2CppCodeGenWriteBarrier((&___maskTextProviderType_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKEDTEXTPROVIDER_T63841E659F7586B73FFADC5F0315F78E4D32B197_H
#ifndef CASECONVERSION_T193D8AE57418D335B7AD2FEE554BB2230D2F6491_H
#define CASECONVERSION_T193D8AE57418D335B7AD2FEE554BB2230D2F6491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MaskedTextProvider_CaseConversion
struct  CaseConversion_t193D8AE57418D335B7AD2FEE554BB2230D2F6491 
{
public:
	// System.Int32 System.ComponentModel.MaskedTextProvider_CaseConversion::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CaseConversion_t193D8AE57418D335B7AD2FEE554BB2230D2F6491, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CASECONVERSION_T193D8AE57418D335B7AD2FEE554BB2230D2F6491_H
#ifndef CHARTYPE_TC58073D5F1CD7F2BF4709E8CEBF07D1D6680159A_H
#define CHARTYPE_TC58073D5F1CD7F2BF4709E8CEBF07D1D6680159A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MaskedTextProvider_CharType
struct  CharType_tC58073D5F1CD7F2BF4709E8CEBF07D1D6680159A 
{
public:
	// System.Int32 System.ComponentModel.MaskedTextProvider_CharType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharType_tC58073D5F1CD7F2BF4709E8CEBF07D1D6680159A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARTYPE_TC58073D5F1CD7F2BF4709E8CEBF07D1D6680159A_H
#ifndef MASKEDTEXTRESULTHINT_T1F48AE21276DEB55A953A4E9C671EABD191761A5_H
#define MASKEDTEXTRESULTHINT_T1F48AE21276DEB55A953A4E9C671EABD191761A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MaskedTextResultHint
struct  MaskedTextResultHint_t1F48AE21276DEB55A953A4E9C671EABD191761A5 
{
public:
	// System.Int32 System.ComponentModel.MaskedTextResultHint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaskedTextResultHint_t1F48AE21276DEB55A953A4E9C671EABD191761A5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKEDTEXTRESULTHINT_T1F48AE21276DEB55A953A4E9C671EABD191761A5_H
#ifndef TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#define TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverter
struct  TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB  : public RuntimeObject
{
public:

public:
};

struct TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB_StaticFields
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.ComponentModel.TypeConverter::useCompatibleTypeConversion
	bool ___useCompatibleTypeConversion_1;

public:
	inline static int32_t get_offset_of_useCompatibleTypeConversion_1() { return static_cast<int32_t>(offsetof(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB_StaticFields, ___useCompatibleTypeConversion_1)); }
	inline bool get_useCompatibleTypeConversion_1() const { return ___useCompatibleTypeConversion_1; }
	inline bool* get_address_of_useCompatibleTypeConversion_1() { return &___useCompatibleTypeConversion_1; }
	inline void set_useCompatibleTypeConversion_1(bool value)
	{
		___useCompatibleTypeConversion_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECONVERTER_T8306AE03734853B551DDF089C1F17836A7764DBB_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef BASENUMBERCONVERTER_T6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63_H
#define BASENUMBERCONVERTER_T6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.BaseNumberConverter
struct  BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASENUMBERCONVERTER_T6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63_H
#ifndef DESIGNERSERIALIZATIONVISIBILITYATTRIBUTE_TC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA_H
#define DESIGNERSERIALIZATIONVISIBILITYATTRIBUTE_TC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DesignerSerializationVisibilityAttribute
struct  DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.ComponentModel.DesignerSerializationVisibility System.ComponentModel.DesignerSerializationVisibilityAttribute::visibility
	int32_t ___visibility_4;

public:
	inline static int32_t get_offset_of_visibility_4() { return static_cast<int32_t>(offsetof(DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA, ___visibility_4)); }
	inline int32_t get_visibility_4() const { return ___visibility_4; }
	inline int32_t* get_address_of_visibility_4() { return &___visibility_4; }
	inline void set_visibility_4(int32_t value)
	{
		___visibility_4 = value;
	}
};

struct DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA_StaticFields
{
public:
	// System.ComponentModel.DesignerSerializationVisibilityAttribute System.ComponentModel.DesignerSerializationVisibilityAttribute::Content
	DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA * ___Content_0;
	// System.ComponentModel.DesignerSerializationVisibilityAttribute System.ComponentModel.DesignerSerializationVisibilityAttribute::Hidden
	DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA * ___Hidden_1;
	// System.ComponentModel.DesignerSerializationVisibilityAttribute System.ComponentModel.DesignerSerializationVisibilityAttribute::Visible
	DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA * ___Visible_2;
	// System.ComponentModel.DesignerSerializationVisibilityAttribute System.ComponentModel.DesignerSerializationVisibilityAttribute::Default
	DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA * ___Default_3;

public:
	inline static int32_t get_offset_of_Content_0() { return static_cast<int32_t>(offsetof(DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA_StaticFields, ___Content_0)); }
	inline DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA * get_Content_0() const { return ___Content_0; }
	inline DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA ** get_address_of_Content_0() { return &___Content_0; }
	inline void set_Content_0(DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA * value)
	{
		___Content_0 = value;
		Il2CppCodeGenWriteBarrier((&___Content_0), value);
	}

	inline static int32_t get_offset_of_Hidden_1() { return static_cast<int32_t>(offsetof(DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA_StaticFields, ___Hidden_1)); }
	inline DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA * get_Hidden_1() const { return ___Hidden_1; }
	inline DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA ** get_address_of_Hidden_1() { return &___Hidden_1; }
	inline void set_Hidden_1(DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA * value)
	{
		___Hidden_1 = value;
		Il2CppCodeGenWriteBarrier((&___Hidden_1), value);
	}

	inline static int32_t get_offset_of_Visible_2() { return static_cast<int32_t>(offsetof(DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA_StaticFields, ___Visible_2)); }
	inline DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA * get_Visible_2() const { return ___Visible_2; }
	inline DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA ** get_address_of_Visible_2() { return &___Visible_2; }
	inline void set_Visible_2(DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA * value)
	{
		___Visible_2 = value;
		Il2CppCodeGenWriteBarrier((&___Visible_2), value);
	}

	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA_StaticFields, ___Default_3)); }
	inline DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA * get_Default_3() const { return ___Default_3; }
	inline DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((&___Default_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESIGNERSERIALIZATIONVISIBILITYATTRIBUTE_TC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA_H
#ifndef EDITORBROWSABLEATTRIBUTE_TF3507DF0AB82A8D54C70D6F7FB4D363DF729D516_H
#define EDITORBROWSABLEATTRIBUTE_TF3507DF0AB82A8D54C70D6F7FB4D363DF729D516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.EditorBrowsableAttribute
struct  EditorBrowsableAttribute_tF3507DF0AB82A8D54C70D6F7FB4D363DF729D516  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.ComponentModel.EditorBrowsableState System.ComponentModel.EditorBrowsableAttribute::browsableState
	int32_t ___browsableState_0;

public:
	inline static int32_t get_offset_of_browsableState_0() { return static_cast<int32_t>(offsetof(EditorBrowsableAttribute_tF3507DF0AB82A8D54C70D6F7FB4D363DF729D516, ___browsableState_0)); }
	inline int32_t get_browsableState_0() const { return ___browsableState_0; }
	inline int32_t* get_address_of_browsableState_0() { return &___browsableState_0; }
	inline void set_browsableState_0(int32_t value)
	{
		___browsableState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORBROWSABLEATTRIBUTE_TF3507DF0AB82A8D54C70D6F7FB4D363DF729D516_H
#ifndef ENUMCONVERTER_T5DA4CB27C27A8C37C31B2A4DE0C4C37820638E12_H
#define ENUMCONVERTER_T5DA4CB27C27A8C37C31B2A4DE0C4C37820638E12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.EnumConverter
struct  EnumConverter_t5DA4CB27C27A8C37C31B2A4DE0C4C37820638E12  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:
	// System.ComponentModel.TypeConverter_StandardValuesCollection System.ComponentModel.EnumConverter::values
	StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * ___values_2;
	// System.Type System.ComponentModel.EnumConverter::type
	Type_t * ___type_3;

public:
	inline static int32_t get_offset_of_values_2() { return static_cast<int32_t>(offsetof(EnumConverter_t5DA4CB27C27A8C37C31B2A4DE0C4C37820638E12, ___values_2)); }
	inline StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * get_values_2() const { return ___values_2; }
	inline StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 ** get_address_of_values_2() { return &___values_2; }
	inline void set_values_2(StandardValuesCollection_t929677712574EF02F5C4CF4C38E070841C20BDA3 * value)
	{
		___values_2 = value;
		Il2CppCodeGenWriteBarrier((&___values_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(EnumConverter_t5DA4CB27C27A8C37C31B2A4DE0C4C37820638E12, ___type_3)); }
	inline Type_t * get_type_3() const { return ___type_3; }
	inline Type_t ** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(Type_t * value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMCONVERTER_T5DA4CB27C27A8C37C31B2A4DE0C4C37820638E12_H
#ifndef EXPANDABLEOBJECTCONVERTER_TC19580E01F630034FD5140CFA7453E1125E13F99_H
#define EXPANDABLEOBJECTCONVERTER_TC19580E01F630034FD5140CFA7453E1125E13F99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ExpandableObjectConverter
struct  ExpandableObjectConverter_tC19580E01F630034FD5140CFA7453E1125E13F99  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPANDABLEOBJECTCONVERTER_TC19580E01F630034FD5140CFA7453E1125E13F99_H
#ifndef GUIDCONVERTER_T9207FA129C0A5A7AD84D5FEC9C9F2CA5F3FB9D7D_H
#define GUIDCONVERTER_T9207FA129C0A5A7AD84D5FEC9C9F2CA5F3FB9D7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.GuidConverter
struct  GuidConverter_t9207FA129C0A5A7AD84D5FEC9C9F2CA5F3FB9D7D  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUIDCONVERTER_T9207FA129C0A5A7AD84D5FEC9C9F2CA5F3FB9D7D_H
#ifndef INVALIDASYNCHRONOUSSTATEEXCEPTION_T7DB1CEE45E73613ADBB359792244856600B5E5D7_H
#define INVALIDASYNCHRONOUSSTATEEXCEPTION_T7DB1CEE45E73613ADBB359792244856600B5E5D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.InvalidAsynchronousStateException
struct  InvalidAsynchronousStateException_t7DB1CEE45E73613ADBB359792244856600B5E5D7  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDASYNCHRONOUSSTATEEXCEPTION_T7DB1CEE45E73613ADBB359792244856600B5E5D7_H
#ifndef INVALIDENUMARGUMENTEXCEPTION_TCC80F4F8EF655A404E9D11895A6477AA3D210BB1_H
#define INVALIDENUMARGUMENTEXCEPTION_TCC80F4F8EF655A404E9D11895A6477AA3D210BB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.InvalidEnumArgumentException
struct  InvalidEnumArgumentException_tCC80F4F8EF655A404E9D11895A6477AA3D210BB1  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDENUMARGUMENTEXCEPTION_TCC80F4F8EF655A404E9D11895A6477AA3D210BB1_H
#ifndef CLRLICENSECONTEXT_T16B4876D030AD05B4AEA1B65FDC49BC8E4BF64DC_H
#define CLRLICENSECONTEXT_T16B4876D030AD05B4AEA1B65FDC49BC8E4BF64DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.LicenseManager_LicenseInteropHelper_CLRLicenseContext
struct  CLRLicenseContext_t16B4876D030AD05B4AEA1B65FDC49BC8E4BF64DC  : public LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A
{
public:
	// System.ComponentModel.LicenseUsageMode System.ComponentModel.LicenseManager_LicenseInteropHelper_CLRLicenseContext::usageMode
	int32_t ___usageMode_0;
	// System.Type System.ComponentModel.LicenseManager_LicenseInteropHelper_CLRLicenseContext::type
	Type_t * ___type_1;
	// System.String System.ComponentModel.LicenseManager_LicenseInteropHelper_CLRLicenseContext::key
	String_t* ___key_2;

public:
	inline static int32_t get_offset_of_usageMode_0() { return static_cast<int32_t>(offsetof(CLRLicenseContext_t16B4876D030AD05B4AEA1B65FDC49BC8E4BF64DC, ___usageMode_0)); }
	inline int32_t get_usageMode_0() const { return ___usageMode_0; }
	inline int32_t* get_address_of_usageMode_0() { return &___usageMode_0; }
	inline void set_usageMode_0(int32_t value)
	{
		___usageMode_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(CLRLicenseContext_t16B4876D030AD05B4AEA1B65FDC49BC8E4BF64DC, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(CLRLicenseContext_t16B4876D030AD05B4AEA1B65FDC49BC8E4BF64DC, ___key_2)); }
	inline String_t* get_key_2() const { return ___key_2; }
	inline String_t** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(String_t* value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLRLICENSECONTEXT_T16B4876D030AD05B4AEA1B65FDC49BC8E4BF64DC_H
#ifndef LISTCHANGEDEVENTARGS_T59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84_H
#define LISTCHANGEDEVENTARGS_T59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ListChangedEventArgs
struct  ListChangedEventArgs_t59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.ComponentModel.ListChangedType System.ComponentModel.ListChangedEventArgs::listChangedType
	int32_t ___listChangedType_1;
	// System.Int32 System.ComponentModel.ListChangedEventArgs::newIndex
	int32_t ___newIndex_2;
	// System.Int32 System.ComponentModel.ListChangedEventArgs::oldIndex
	int32_t ___oldIndex_3;
	// System.ComponentModel.PropertyDescriptor System.ComponentModel.ListChangedEventArgs::propDesc
	PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D * ___propDesc_4;

public:
	inline static int32_t get_offset_of_listChangedType_1() { return static_cast<int32_t>(offsetof(ListChangedEventArgs_t59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84, ___listChangedType_1)); }
	inline int32_t get_listChangedType_1() const { return ___listChangedType_1; }
	inline int32_t* get_address_of_listChangedType_1() { return &___listChangedType_1; }
	inline void set_listChangedType_1(int32_t value)
	{
		___listChangedType_1 = value;
	}

	inline static int32_t get_offset_of_newIndex_2() { return static_cast<int32_t>(offsetof(ListChangedEventArgs_t59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84, ___newIndex_2)); }
	inline int32_t get_newIndex_2() const { return ___newIndex_2; }
	inline int32_t* get_address_of_newIndex_2() { return &___newIndex_2; }
	inline void set_newIndex_2(int32_t value)
	{
		___newIndex_2 = value;
	}

	inline static int32_t get_offset_of_oldIndex_3() { return static_cast<int32_t>(offsetof(ListChangedEventArgs_t59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84, ___oldIndex_3)); }
	inline int32_t get_oldIndex_3() const { return ___oldIndex_3; }
	inline int32_t* get_address_of_oldIndex_3() { return &___oldIndex_3; }
	inline void set_oldIndex_3(int32_t value)
	{
		___oldIndex_3 = value;
	}

	inline static int32_t get_offset_of_propDesc_4() { return static_cast<int32_t>(offsetof(ListChangedEventArgs_t59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84, ___propDesc_4)); }
	inline PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D * get_propDesc_4() const { return ___propDesc_4; }
	inline PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D ** get_address_of_propDesc_4() { return &___propDesc_4; }
	inline void set_propDesc_4(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D * value)
	{
		___propDesc_4 = value;
		Il2CppCodeGenWriteBarrier((&___propDesc_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTCHANGEDEVENTARGS_T59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84_H
#ifndef LISTSORTDESCRIPTION_T70B03E24814E0475DA906045690FBE5010F28C75_H
#define LISTSORTDESCRIPTION_T70B03E24814E0475DA906045690FBE5010F28C75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ListSortDescription
struct  ListSortDescription_t70B03E24814E0475DA906045690FBE5010F28C75  : public RuntimeObject
{
public:
	// System.ComponentModel.PropertyDescriptor System.ComponentModel.ListSortDescription::property
	PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D * ___property_0;
	// System.ComponentModel.ListSortDirection System.ComponentModel.ListSortDescription::sortDirection
	int32_t ___sortDirection_1;

public:
	inline static int32_t get_offset_of_property_0() { return static_cast<int32_t>(offsetof(ListSortDescription_t70B03E24814E0475DA906045690FBE5010F28C75, ___property_0)); }
	inline PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D * get_property_0() const { return ___property_0; }
	inline PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D ** get_address_of_property_0() { return &___property_0; }
	inline void set_property_0(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D * value)
	{
		___property_0 = value;
		Il2CppCodeGenWriteBarrier((&___property_0), value);
	}

	inline static int32_t get_offset_of_sortDirection_1() { return static_cast<int32_t>(offsetof(ListSortDescription_t70B03E24814E0475DA906045690FBE5010F28C75, ___sortDirection_1)); }
	inline int32_t get_sortDirection_1() const { return ___sortDirection_1; }
	inline int32_t* get_address_of_sortDirection_1() { return &___sortDirection_1; }
	inline void set_sortDirection_1(int32_t value)
	{
		___sortDirection_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTSORTDESCRIPTION_T70B03E24814E0475DA906045690FBE5010F28C75_H
#ifndef CHARDESCRIPTOR_T8FA4F99310872A548406BE90FD1A7E1484C6ED5D_H
#define CHARDESCRIPTOR_T8FA4F99310872A548406BE90FD1A7E1484C6ED5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MaskedTextProvider_CharDescriptor
struct  CharDescriptor_t8FA4F99310872A548406BE90FD1A7E1484C6ED5D  : public RuntimeObject
{
public:
	// System.Int32 System.ComponentModel.MaskedTextProvider_CharDescriptor::MaskPosition
	int32_t ___MaskPosition_0;
	// System.ComponentModel.MaskedTextProvider_CaseConversion System.ComponentModel.MaskedTextProvider_CharDescriptor::CaseConversion
	int32_t ___CaseConversion_1;
	// System.ComponentModel.MaskedTextProvider_CharType System.ComponentModel.MaskedTextProvider_CharDescriptor::CharType
	int32_t ___CharType_2;
	// System.Boolean System.ComponentModel.MaskedTextProvider_CharDescriptor::IsAssigned
	bool ___IsAssigned_3;

public:
	inline static int32_t get_offset_of_MaskPosition_0() { return static_cast<int32_t>(offsetof(CharDescriptor_t8FA4F99310872A548406BE90FD1A7E1484C6ED5D, ___MaskPosition_0)); }
	inline int32_t get_MaskPosition_0() const { return ___MaskPosition_0; }
	inline int32_t* get_address_of_MaskPosition_0() { return &___MaskPosition_0; }
	inline void set_MaskPosition_0(int32_t value)
	{
		___MaskPosition_0 = value;
	}

	inline static int32_t get_offset_of_CaseConversion_1() { return static_cast<int32_t>(offsetof(CharDescriptor_t8FA4F99310872A548406BE90FD1A7E1484C6ED5D, ___CaseConversion_1)); }
	inline int32_t get_CaseConversion_1() const { return ___CaseConversion_1; }
	inline int32_t* get_address_of_CaseConversion_1() { return &___CaseConversion_1; }
	inline void set_CaseConversion_1(int32_t value)
	{
		___CaseConversion_1 = value;
	}

	inline static int32_t get_offset_of_CharType_2() { return static_cast<int32_t>(offsetof(CharDescriptor_t8FA4F99310872A548406BE90FD1A7E1484C6ED5D, ___CharType_2)); }
	inline int32_t get_CharType_2() const { return ___CharType_2; }
	inline int32_t* get_address_of_CharType_2() { return &___CharType_2; }
	inline void set_CharType_2(int32_t value)
	{
		___CharType_2 = value;
	}

	inline static int32_t get_offset_of_IsAssigned_3() { return static_cast<int32_t>(offsetof(CharDescriptor_t8FA4F99310872A548406BE90FD1A7E1484C6ED5D, ___IsAssigned_3)); }
	inline bool get_IsAssigned_3() const { return ___IsAssigned_3; }
	inline bool* get_address_of_IsAssigned_3() { return &___IsAssigned_3; }
	inline void set_IsAssigned_3(bool value)
	{
		___IsAssigned_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARDESCRIPTOR_T8FA4F99310872A548406BE90FD1A7E1484C6ED5D_H
#ifndef MULTILINESTRINGCONVERTER_T339792E6A737C1E96D03DA7172324B8EAEBA85A1_H
#define MULTILINESTRINGCONVERTER_T339792E6A737C1E96D03DA7172324B8EAEBA85A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MultilineStringConverter
struct  MultilineStringConverter_t339792E6A737C1E96D03DA7172324B8EAEBA85A1  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTILINESTRINGCONVERTER_T339792E6A737C1E96D03DA7172324B8EAEBA85A1_H
#ifndef NULLABLECONVERTER_T46A6256BAFD4B0C501724527189C888B36E48E71_H
#define NULLABLECONVERTER_T46A6256BAFD4B0C501724527189C888B36E48E71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.NullableConverter
struct  NullableConverter_t46A6256BAFD4B0C501724527189C888B36E48E71  : public TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB
{
public:
	// System.Type System.ComponentModel.NullableConverter::nullableType
	Type_t * ___nullableType_2;
	// System.Type System.ComponentModel.NullableConverter::simpleType
	Type_t * ___simpleType_3;
	// System.ComponentModel.TypeConverter System.ComponentModel.NullableConverter::simpleTypeConverter
	TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * ___simpleTypeConverter_4;

public:
	inline static int32_t get_offset_of_nullableType_2() { return static_cast<int32_t>(offsetof(NullableConverter_t46A6256BAFD4B0C501724527189C888B36E48E71, ___nullableType_2)); }
	inline Type_t * get_nullableType_2() const { return ___nullableType_2; }
	inline Type_t ** get_address_of_nullableType_2() { return &___nullableType_2; }
	inline void set_nullableType_2(Type_t * value)
	{
		___nullableType_2 = value;
		Il2CppCodeGenWriteBarrier((&___nullableType_2), value);
	}

	inline static int32_t get_offset_of_simpleType_3() { return static_cast<int32_t>(offsetof(NullableConverter_t46A6256BAFD4B0C501724527189C888B36E48E71, ___simpleType_3)); }
	inline Type_t * get_simpleType_3() const { return ___simpleType_3; }
	inline Type_t ** get_address_of_simpleType_3() { return &___simpleType_3; }
	inline void set_simpleType_3(Type_t * value)
	{
		___simpleType_3 = value;
		Il2CppCodeGenWriteBarrier((&___simpleType_3), value);
	}

	inline static int32_t get_offset_of_simpleTypeConverter_4() { return static_cast<int32_t>(offsetof(NullableConverter_t46A6256BAFD4B0C501724527189C888B36E48E71, ___simpleTypeConverter_4)); }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * get_simpleTypeConverter_4() const { return ___simpleTypeConverter_4; }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB ** get_address_of_simpleTypeConverter_4() { return &___simpleTypeConverter_4; }
	inline void set_simpleTypeConverter_4(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * value)
	{
		___simpleTypeConverter_4 = value;
		Il2CppCodeGenWriteBarrier((&___simpleTypeConverter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLECONVERTER_T46A6256BAFD4B0C501724527189C888B36E48E71_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef DOWORKEVENTHANDLER_T106AA6C0F543A5A4998F9E9E73F4461D404F20DD_H
#define DOWORKEVENTHANDLER_T106AA6C0F543A5A4998F9E9E73F4461D404F20DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DoWorkEventHandler
struct  DoWorkEventHandler_t106AA6C0F543A5A4998F9E9E73F4461D404F20DD  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWORKEVENTHANDLER_T106AA6C0F543A5A4998F9E9E73F4461D404F20DD_H
#ifndef DOUBLECONVERTER_T65A5D8B0C2736FC5469CE5DFA468D371DD5F9F75_H
#define DOUBLECONVERTER_T65A5D8B0C2736FC5469CE5DFA468D371DD5F9F75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.DoubleConverter
struct  DoubleConverter_t65A5D8B0C2736FC5469CE5DFA468D371DD5F9F75  : public BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLECONVERTER_T65A5D8B0C2736FC5469CE5DFA468D371DD5F9F75_H
#ifndef HANDLEDEVENTHANDLER_T17748FC020C52377E8BE63A85595C31181910699_H
#define HANDLEDEVENTHANDLER_T17748FC020C52377E8BE63A85595C31181910699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.HandledEventHandler
struct  HandledEventHandler_t17748FC020C52377E8BE63A85595C31181910699  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDLEDEVENTHANDLER_T17748FC020C52377E8BE63A85595C31181910699_H
#ifndef INT16CONVERTER_TA2223DDF2BE99AD79AD836FCEC90F2C12705433B_H
#define INT16CONVERTER_TA2223DDF2BE99AD79AD836FCEC90F2C12705433B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Int16Converter
struct  Int16Converter_tA2223DDF2BE99AD79AD836FCEC90F2C12705433B  : public BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT16CONVERTER_TA2223DDF2BE99AD79AD836FCEC90F2C12705433B_H
#ifndef INT32CONVERTER_T73A6E403EBE01B56528CB227509954397A46BA22_H
#define INT32CONVERTER_T73A6E403EBE01B56528CB227509954397A46BA22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Int32Converter
struct  Int32Converter_t73A6E403EBE01B56528CB227509954397A46BA22  : public BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32CONVERTER_T73A6E403EBE01B56528CB227509954397A46BA22_H
#ifndef INT64CONVERTER_TED09C98C409F894484943F8D4F9C6468A97F1447_H
#define INT64CONVERTER_TED09C98C409F894484943F8D4F9C6468A97F1447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Int64Converter
struct  Int64Converter_tED09C98C409F894484943F8D4F9C6468A97F1447  : public BaseNumberConverter_t6AF36A2503E7BABF7FB9A8EC05DF8B828491AC63
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64CONVERTER_TED09C98C409F894484943F8D4F9C6468A97F1447_H
#ifndef LISTCHANGEDEVENTHANDLER_T8081F1428D22013519901C16884C5ACE86A72A88_H
#define LISTCHANGEDEVENTHANDLER_T8081F1428D22013519901C16884C5ACE86A72A88_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ListChangedEventHandler
struct  ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTCHANGEDEVENTHANDLER_T8081F1428D22013519901C16884C5ACE86A72A88_H
#ifndef PROGRESSCHANGEDEVENTHANDLER_T7C77D0D690442151F8203A0BF38966A9B2CF33C0_H
#define PROGRESSCHANGEDEVENTHANDLER_T7C77D0D690442151F8203A0BF38966A9B2CF33C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.ProgressChangedEventHandler
struct  ProgressChangedEventHandler_t7C77D0D690442151F8203A0BF38966A9B2CF33C0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSCHANGEDEVENTHANDLER_T7C77D0D690442151F8203A0BF38966A9B2CF33C0_H
#ifndef PROPERTYCHANGEDEVENTHANDLER_T617E98E1876A8EB394D2B329340CE02D21FFFC82_H
#define PROPERTYCHANGEDEVENTHANDLER_T617E98E1876A8EB394D2B329340CE02D21FFFC82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyChangedEventHandler
struct  PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYCHANGEDEVENTHANDLER_T617E98E1876A8EB394D2B329340CE02D21FFFC82_H
#ifndef PROPERTYCHANGINGEVENTHANDLER_TE2424019EC48E76381C3EB037326A821D4F833C1_H
#define PROPERTYCHANGINGEVENTHANDLER_TE2424019EC48E76381C3EB037326A821D4F833C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyChangingEventHandler
struct  PropertyChangingEventHandler_tE2424019EC48E76381C3EB037326A821D4F833C1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYCHANGINGEVENTHANDLER_TE2424019EC48E76381C3EB037326A821D4F833C1_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2600 = { sizeof (DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA), -1, sizeof(DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2600[6] = 
{
	DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA::get_offset_of_category_0(),
	DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA::get_offset_of_typeId_1(),
	DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA_StaticFields::get_offset_of_Component_2(),
	DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA_StaticFields::get_offset_of_Default_3(),
	DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA_StaticFields::get_offset_of_Form_4(),
	DesignerCategoryAttribute_tE79E5894EFE62D19C0CD24911AB16B9E996BA7FA_StaticFields::get_offset_of_Generic_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2601 = { sizeof (DesignerSerializationVisibility_tCD99EB7FAAE0F69CABCFCE53E16C39DDCB2FFC5A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2601[4] = 
{
	DesignerSerializationVisibility_tCD99EB7FAAE0F69CABCFCE53E16C39DDCB2FFC5A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2602 = { sizeof (DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA), -1, sizeof(DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2602[5] = 
{
	DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA_StaticFields::get_offset_of_Content_0(),
	DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA_StaticFields::get_offset_of_Hidden_1(),
	DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA_StaticFields::get_offset_of_Visible_2(),
	DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA_StaticFields::get_offset_of_Default_3(),
	DesignerSerializationVisibilityAttribute_tC50BBA28AF86896B3F34ECB9D56CB0A15BB4CBFA::get_offset_of_visibility_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2603 = { sizeof (DisplayNameAttribute_tE54161FBBAB6EF141BE2925404185B82B67C1BCE), -1, sizeof(DisplayNameAttribute_tE54161FBBAB6EF141BE2925404185B82B67C1BCE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2603[2] = 
{
	DisplayNameAttribute_tE54161FBBAB6EF141BE2925404185B82B67C1BCE_StaticFields::get_offset_of_Default_0(),
	DisplayNameAttribute_tE54161FBBAB6EF141BE2925404185B82B67C1BCE::get_offset_of__displayName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2604 = { sizeof (DoWorkEventArgs_t66930F141A21B5E954CE70A24AD0446E2CD5F606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2604[2] = 
{
	DoWorkEventArgs_t66930F141A21B5E954CE70A24AD0446E2CD5F606::get_offset_of_result_2(),
	DoWorkEventArgs_t66930F141A21B5E954CE70A24AD0446E2CD5F606::get_offset_of_argument_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2605 = { sizeof (DoWorkEventHandler_t106AA6C0F543A5A4998F9E9E73F4461D404F20DD), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2606 = { sizeof (DoubleConverter_t65A5D8B0C2736FC5469CE5DFA468D371DD5F9F75), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2607 = { sizeof (EditorAttribute_tD8A0A6A2F1AC5829C1C1CB563A83B1CD6D6273D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2607[3] = 
{
	EditorAttribute_tD8A0A6A2F1AC5829C1C1CB563A83B1CD6D6273D5::get_offset_of_baseTypeName_0(),
	EditorAttribute_tD8A0A6A2F1AC5829C1C1CB563A83B1CD6D6273D5::get_offset_of_typeName_1(),
	EditorAttribute_tD8A0A6A2F1AC5829C1C1CB563A83B1CD6D6273D5::get_offset_of_typeId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2608 = { sizeof (EditorBrowsableAttribute_tF3507DF0AB82A8D54C70D6F7FB4D363DF729D516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2608[1] = 
{
	EditorBrowsableAttribute_tF3507DF0AB82A8D54C70D6F7FB4D363DF729D516::get_offset_of_browsableState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2609 = { sizeof (EditorBrowsableState_t8EAF9BADAAE7DA735C235280DF4B8974EAA39C1B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2609[4] = 
{
	EditorBrowsableState_t8EAF9BADAAE7DA735C235280DF4B8974EAA39C1B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2610 = { sizeof (EnumConverter_t5DA4CB27C27A8C37C31B2A4DE0C4C37820638E12), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2610[2] = 
{
	EnumConverter_t5DA4CB27C27A8C37C31B2A4DE0C4C37820638E12::get_offset_of_values_2(),
	EnumConverter_t5DA4CB27C27A8C37C31B2A4DE0C4C37820638E12::get_offset_of_type_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2611 = { sizeof (EventDescriptor_tAB488D66C0409A1889EE56355848CDA43ED95222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2612 = { sizeof (EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37), -1, sizeof(EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2612[8] = 
{
	EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37::get_offset_of_events_0(),
	EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37::get_offset_of_namedSort_1(),
	EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37::get_offset_of_comparer_2(),
	EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37::get_offset_of_eventsOwned_3(),
	EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37::get_offset_of_needSort_4(),
	EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37::get_offset_of_eventCount_5(),
	EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37::get_offset_of_readOnly_6(),
	EventDescriptorCollection_tB9FC461177F5D7FE0B79268C77569B36F4E1FF37_StaticFields::get_offset_of_Empty_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2613 = { sizeof (EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2613[2] = 
{
	EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4::get_offset_of_head_0(),
	EventHandlerList_tFE9EF79E85419EBB2C206CF475E29A9960699BE4::get_offset_of_parent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2614 = { sizeof (ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2614[3] = 
{
	ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D::get_offset_of_next_0(),
	ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D::get_offset_of_key_1(),
	ListEntry_t32989B38CAC0D49C6A5AC5BA1622A62088BA6E6D::get_offset_of_handler_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2615 = { sizeof (ExpandableObjectConverter_tC19580E01F630034FD5140CFA7453E1125E13F99), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2616 = { sizeof (ExtendedPropertyDescriptor_tABDBA511CEE5442C0B06059799418904896E0CF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2616[2] = 
{
	ExtendedPropertyDescriptor_tABDBA511CEE5442C0B06059799418904896E0CF6::get_offset_of_extenderInfo_17(),
	ExtendedPropertyDescriptor_tABDBA511CEE5442C0B06059799418904896E0CF6::get_offset_of_provider_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2617 = { sizeof (ExtenderProvidedPropertyAttribute_t6C2D566DED3E19650BDE6A721FB2ADF877670D68), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2617[3] = 
{
	ExtenderProvidedPropertyAttribute_t6C2D566DED3E19650BDE6A721FB2ADF877670D68::get_offset_of_extenderProperty_0(),
	ExtenderProvidedPropertyAttribute_t6C2D566DED3E19650BDE6A721FB2ADF877670D68::get_offset_of_provider_1(),
	ExtenderProvidedPropertyAttribute_t6C2D566DED3E19650BDE6A721FB2ADF877670D68::get_offset_of_receiverType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2618 = { sizeof (GuidConverter_t9207FA129C0A5A7AD84D5FEC9C9F2CA5F3FB9D7D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2619 = { sizeof (HandledEventArgs_t26999700606DD7EC365648A50C9E3E15EFCAC2DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2619[1] = 
{
	HandledEventArgs_t26999700606DD7EC365648A50C9E3E15EFCAC2DF::get_offset_of_handled_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2620 = { sizeof (HandledEventHandler_t17748FC020C52377E8BE63A85595C31181910699), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2621 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2622 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2623 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2624 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2625 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2626 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2627 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2628 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2629 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2630 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2631 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2632 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2633 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2634 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2635 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2636 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2637 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2638 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2639 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2640 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2641 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2642 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2643 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2644 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2645 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2646 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2647 = { sizeof (ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492), -1, sizeof(ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2647[4] = 
{
	ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492_StaticFields::get_offset_of_Yes_0(),
	ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492_StaticFields::get_offset_of_No_1(),
	ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492_StaticFields::get_offset_of_Default_2(),
	ImmutableObjectAttribute_tEBEA4EFBB8E9D169AB4FFA9E7B4240969CD60492::get_offset_of_immutable_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2648 = { sizeof (InitializationEventAttribute_t66A3424618601597D5221545AA27CCAE24DC19B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2648[1] = 
{
	InitializationEventAttribute_t66A3424618601597D5221545AA27CCAE24DC19B1::get_offset_of_eventName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2649 = { sizeof (InstallerTypeAttribute_t69E2B80148C986610B0F88883070C0BAD2A2B755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2649[1] = 
{
	InstallerTypeAttribute_t69E2B80148C986610B0F88883070C0BAD2A2B755::get_offset_of__typeName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2650 = { sizeof (InstanceCreationEditor_t87E9919F45C092DA810FAC5AEFD200F26A9D7EEA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2651 = { sizeof (Int16Converter_tA2223DDF2BE99AD79AD836FCEC90F2C12705433B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2652 = { sizeof (Int32Converter_t73A6E403EBE01B56528CB227509954397A46BA22), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2653 = { sizeof (Int64Converter_tED09C98C409F894484943F8D4F9C6468A97F1447), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2654 = { sizeof (IntSecurity_t3FC031652848FDE0BB103E7A46BA8DF90840B117), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2655 = { sizeof (InvalidAsynchronousStateException_t7DB1CEE45E73613ADBB359792244856600B5E5D7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2656 = { sizeof (InvalidEnumArgumentException_tCC80F4F8EF655A404E9D11895A6477AA3D210BB1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2657 = { sizeof (LicFileLicenseProvider_t458B091F8501C2890FD617BB025AB49DC494076F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2658 = { sizeof (LicFileLicense_t5AF4C870B7DF7EE91D6C0FA5DA1354763EF9E5FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2658[1] = 
{
	LicFileLicense_t5AF4C870B7DF7EE91D6C0FA5DA1354763EF9E5FF::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2659 = { sizeof (License_t9011453B97942387364692C7DFAD2DEDB2160F8C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2660 = { sizeof (LicenseContext_tE7068766A5D105EA910974E3F08D0AF7534A806A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2661 = { sizeof (LicenseException_t68C548BE94D4AD7F82C5AD5E710AC18F50F30F9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2661[2] = 
{
	LicenseException_t68C548BE94D4AD7F82C5AD5E710AC18F50F30F9D::get_offset_of_type_17(),
	LicenseException_t68C548BE94D4AD7F82C5AD5E710AC18F50F30F9D::get_offset_of_instance_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2662 = { sizeof (LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1), -1, sizeof(LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2662[6] = 
{
	LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields::get_offset_of_selfLock_0(),
	LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields::get_offset_of_context_1(),
	LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields::get_offset_of_contextLockHolder_2(),
	LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields::get_offset_of_providers_3(),
	LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields::get_offset_of_providerInstances_4(),
	LicenseManager_t8D3254EFF06EA8AB79A3AE3E4193AC9239D5B3B1_StaticFields::get_offset_of_internalSyncObject_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2663 = { sizeof (LicenseInteropHelper_t528682251600D62797521A8A3CCAD83BD1A47C0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2663[7] = 
{
	0,
	0,
	0,
	0,
	LicenseInteropHelper_t528682251600D62797521A8A3CCAD83BD1A47C0E::get_offset_of_helperContext_4(),
	LicenseInteropHelper_t528682251600D62797521A8A3CCAD83BD1A47C0E::get_offset_of_savedLicenseContext_5(),
	LicenseInteropHelper_t528682251600D62797521A8A3CCAD83BD1A47C0E::get_offset_of_savedType_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2664 = { sizeof (CLRLicenseContext_t16B4876D030AD05B4AEA1B65FDC49BC8E4BF64DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2664[3] = 
{
	CLRLicenseContext_t16B4876D030AD05B4AEA1B65FDC49BC8E4BF64DC::get_offset_of_usageMode_0(),
	CLRLicenseContext_t16B4876D030AD05B4AEA1B65FDC49BC8E4BF64DC::get_offset_of_type_1(),
	CLRLicenseContext_t16B4876D030AD05B4AEA1B65FDC49BC8E4BF64DC::get_offset_of_key_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2665 = { sizeof (LicenseProvider_t586048163F4CCA6D8C6592E696379137A18B0596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2666 = { sizeof (LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D), -1, sizeof(LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2666[3] = 
{
	LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D_StaticFields::get_offset_of_Default_0(),
	LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D::get_offset_of_licenseProviderType_1(),
	LicenseProviderAttribute_t27AE126583B6023AFF6F0F80AB0C4CFCA2FAF17D::get_offset_of_licenseProviderName_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2667 = { sizeof (LicenseUsageMode_t2DEE0920CFCD0D47584E94D5D6D9AFEC4F82883F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2667[3] = 
{
	LicenseUsageMode_t2DEE0920CFCD0D47584E94D5D6D9AFEC4F82883F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2668 = { sizeof (ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2), -1, sizeof(ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2668[5] = 
{
	ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2_StaticFields::get_offset_of_Yes_0(),
	ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2_StaticFields::get_offset_of_No_1(),
	ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2_StaticFields::get_offset_of_Default_2(),
	ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2::get_offset_of_listBindable_3(),
	ListBindableAttribute_t455DF9EC6868D0DD6230ACF5C9094EBD078CE6F2::get_offset_of_isDefault_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2669 = { sizeof (ListChangedEventArgs_t59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2669[4] = 
{
	ListChangedEventArgs_t59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84::get_offset_of_listChangedType_1(),
	ListChangedEventArgs_t59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84::get_offset_of_newIndex_2(),
	ListChangedEventArgs_t59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84::get_offset_of_oldIndex_3(),
	ListChangedEventArgs_t59D7FF8643A44CDDC35C47DEF87AAE0FCA2B8F84::get_offset_of_propDesc_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2670 = { sizeof (ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2671 = { sizeof (ListChangedType_tE885755B4022635C38211A1864E2A851CEE324F8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2671[9] = 
{
	ListChangedType_tE885755B4022635C38211A1864E2A851CEE324F8::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2672 = { sizeof (ListSortDescription_t70B03E24814E0475DA906045690FBE5010F28C75), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2672[2] = 
{
	ListSortDescription_t70B03E24814E0475DA906045690FBE5010F28C75::get_offset_of_property_0(),
	ListSortDescription_t70B03E24814E0475DA906045690FBE5010F28C75::get_offset_of_sortDirection_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2673 = { sizeof (ListSortDescriptionCollection_t861BA7E63FFEBDC5C46C048FEA7B60EC580964CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2673[1] = 
{
	ListSortDescriptionCollection_t861BA7E63FFEBDC5C46C048FEA7B60EC580964CB::get_offset_of_sorts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2674 = { sizeof (ListSortDirection_t345E309405F5CEFC88A1F4F34ACE985B9690FF20)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2674[3] = 
{
	ListSortDirection_t345E309405F5CEFC88A1F4F34ACE985B9690FF20::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2675 = { sizeof (LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F), -1, sizeof(LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2675[4] = 
{
	LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F::get_offset_of_isLocalizable_0(),
	LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F_StaticFields::get_offset_of_Yes_1(),
	LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F_StaticFields::get_offset_of_No_2(),
	LocalizableAttribute_t4436E5AD54CDF3F6ECB93A1EC4D23BA727C3B61F_StaticFields::get_offset_of_Default_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2676 = { sizeof (LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B), -1, sizeof(LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2676[5] = 
{
	LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B::get_offset_of_dataSource_0(),
	LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B::get_offset_of_displayMember_1(),
	LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B::get_offset_of_valueMember_2(),
	LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B::get_offset_of_lookupMember_3(),
	LookupBindingPropertiesAttribute_tFB30823108AD583FC2406504B9601C38AE83FD1B_StaticFields::get_offset_of_Default_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2677 = { sizeof (MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B), -1, sizeof(MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2677[3] = 
{
	MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B_StaticFields::get_offset_of_EventDisposed_0(),
	MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B::get_offset_of_site_1(),
	MarshalByValueComponent_tADC0E481D4D19F965AB659F9038A1D7D47FA636B::get_offset_of_events_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2678 = { sizeof (MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197), -1, sizeof(MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2678[29] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields::get_offset_of_ASCII_ONLY_10(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields::get_offset_of_ALLOW_PROMPT_AS_INPUT_11(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields::get_offset_of_INCLUDE_PROMPT_12(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields::get_offset_of_INCLUDE_LITERALS_13(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields::get_offset_of_RESET_ON_PROMPT_14(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields::get_offset_of_RESET_ON_LITERALS_15(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields::get_offset_of_SKIP_SPACE_16(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197_StaticFields::get_offset_of_maskTextProviderType_17(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197::get_offset_of_flagState_18(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197::get_offset_of_culture_19(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197::get_offset_of_testString_20(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197::get_offset_of_assignedCharCount_21(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197::get_offset_of_requiredCharCount_22(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197::get_offset_of_requiredEditChars_23(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197::get_offset_of_optionalEditChars_24(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197::get_offset_of_mask_25(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197::get_offset_of_passwordChar_26(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197::get_offset_of_promptChar_27(),
	MaskedTextProvider_t63841E659F7586B73FFADC5F0315F78E4D32B197::get_offset_of_stringDescriptor_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2679 = { sizeof (CaseConversion_t193D8AE57418D335B7AD2FEE554BB2230D2F6491)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2679[4] = 
{
	CaseConversion_t193D8AE57418D335B7AD2FEE554BB2230D2F6491::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2680 = { sizeof (CharType_tC58073D5F1CD7F2BF4709E8CEBF07D1D6680159A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2680[6] = 
{
	CharType_tC58073D5F1CD7F2BF4709E8CEBF07D1D6680159A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2681 = { sizeof (CharDescriptor_t8FA4F99310872A548406BE90FD1A7E1484C6ED5D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2681[4] = 
{
	CharDescriptor_t8FA4F99310872A548406BE90FD1A7E1484C6ED5D::get_offset_of_MaskPosition_0(),
	CharDescriptor_t8FA4F99310872A548406BE90FD1A7E1484C6ED5D::get_offset_of_CaseConversion_1(),
	CharDescriptor_t8FA4F99310872A548406BE90FD1A7E1484C6ED5D::get_offset_of_CharType_2(),
	CharDescriptor_t8FA4F99310872A548406BE90FD1A7E1484C6ED5D::get_offset_of_IsAssigned_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2682 = { sizeof (MaskedTextResultHint_t1F48AE21276DEB55A953A4E9C671EABD191761A5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2682[16] = 
{
	MaskedTextResultHint_t1F48AE21276DEB55A953A4E9C671EABD191761A5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2683 = { sizeof (MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2683[12] = 
{
	MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8::get_offset_of_name_0(),
	MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8::get_offset_of_displayName_1(),
	MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8::get_offset_of_nameHash_2(),
	MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8::get_offset_of_attributeCollection_3(),
	MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8::get_offset_of_attributes_4(),
	MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8::get_offset_of_originalAttributes_5(),
	MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8::get_offset_of_attributesFiltered_6(),
	MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8::get_offset_of_attributesFilled_7(),
	MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8::get_offset_of_metadataVersion_8(),
	MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8::get_offset_of_category_9(),
	MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8::get_offset_of_description_10(),
	MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8::get_offset_of_lockCookie_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2684 = { sizeof (MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909), -1, sizeof(MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2684[4] = 
{
	MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909_StaticFields::get_offset_of_Yes_0(),
	MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909_StaticFields::get_offset_of_No_1(),
	MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909_StaticFields::get_offset_of_Default_2(),
	MergablePropertyAttribute_tB1134B55C33FBE33AD138D5E5CBDF4BAC6014909::get_offset_of_allowMerge_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2685 = { sizeof (MultilineStringConverter_t339792E6A737C1E96D03DA7172324B8EAEBA85A1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2686 = { sizeof (NestedContainer_t08F11932945A6F531D1338CC5A05BAD8D99F8AE9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2686[1] = 
{
	NestedContainer_t08F11932945A6F531D1338CC5A05BAD8D99F8AE9::get_offset_of__owner_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2687 = { sizeof (Site_t416D1B752B924BEC6C03A474D7DC852F255B3E5A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2687[3] = 
{
	Site_t416D1B752B924BEC6C03A474D7DC852F255B3E5A::get_offset_of_component_0(),
	Site_t416D1B752B924BEC6C03A474D7DC852F255B3E5A::get_offset_of_container_1(),
	Site_t416D1B752B924BEC6C03A474D7DC852F255B3E5A::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2688 = { sizeof (NullableConverter_t46A6256BAFD4B0C501724527189C888B36E48E71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2688[3] = 
{
	NullableConverter_t46A6256BAFD4B0C501724527189C888B36E48E71::get_offset_of_nullableType_2(),
	NullableConverter_t46A6256BAFD4B0C501724527189C888B36E48E71::get_offset_of_simpleType_3(),
	NullableConverter_t46A6256BAFD4B0C501724527189C888B36E48E71::get_offset_of_simpleTypeConverter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2689 = { sizeof (PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1), -1, sizeof(PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2689[4] = 
{
	PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1_StaticFields::get_offset_of_Yes_0(),
	PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1_StaticFields::get_offset_of_No_1(),
	PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1_StaticFields::get_offset_of_Default_2(),
	PasswordPropertyTextAttribute_tF8F54903B400BCD43A3561510E32FA16FB1B33C1::get_offset_of__password_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2690 = { sizeof (ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2690[2] = 
{
	ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F::get_offset_of_progressPercentage_1(),
	ProgressChangedEventArgs_tC79597AB8E4151EDFF627F615FE36B8D91811F3F::get_offset_of_userState_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2691 = { sizeof (ProgressChangedEventHandler_t7C77D0D690442151F8203A0BF38966A9B2CF33C0), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2692 = { sizeof (PropertyChangedEventArgs_t90CF85B82F87D594F73F03364494C77592B11F46), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2692[1] = 
{
	PropertyChangedEventArgs_t90CF85B82F87D594F73F03364494C77592B11F46::get_offset_of_propertyName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2693 = { sizeof (PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2694 = { sizeof (PropertyChangingEventArgs_tF5EE6A07599A5560DB307BC199120416F0615DBA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2694[1] = 
{
	PropertyChangingEventArgs_tF5EE6A07599A5560DB307BC199120416F0615DBA::get_offset_of_propertyName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2695 = { sizeof (PropertyChangingEventHandler_tE2424019EC48E76381C3EB037326A821D4F833C1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2696 = { sizeof (PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2696[5] = 
{
	PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D::get_offset_of_converter_12(),
	PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D::get_offset_of_valueChangedHandlers_13(),
	PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D::get_offset_of_editors_14(),
	PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D::get_offset_of_editorTypes_15(),
	PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D::get_offset_of_editorCount_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2697 = { sizeof (PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2), -1, sizeof(PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2697[10] = 
{
	PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2_StaticFields::get_offset_of_Empty_0(),
	PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2::get_offset_of_cachedFoundProperties_1(),
	PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2::get_offset_of_cachedIgnoreCase_2(),
	PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2::get_offset_of_properties_3(),
	PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2::get_offset_of_propCount_4(),
	PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2::get_offset_of_namedSort_5(),
	PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2::get_offset_of_comparer_6(),
	PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2::get_offset_of_propsOwned_7(),
	PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2::get_offset_of_needSort_8(),
	PropertyDescriptorCollection_t19FEFDD6CEF7609BB10282A4B52C3C09A04B41A2::get_offset_of_readOnly_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2698 = { sizeof (PropertyDescriptorEnumerator_t8DE08E0C89BAD17FE8FD71FD3159C80833CE2D9D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2698[2] = 
{
	PropertyDescriptorEnumerator_t8DE08E0C89BAD17FE8FD71FD3159C80833CE2D9D::get_offset_of_owner_0(),
	PropertyDescriptorEnumerator_t8DE08E0C89BAD17FE8FD71FD3159C80833CE2D9D::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2699 = { sizeof (ProvidePropertyAttribute_tF58AE8F88029C5D1249A8A149173482A9BB08BC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2699[2] = 
{
	ProvidePropertyAttribute_tF58AE8F88029C5D1249A8A149173482A9BB08BC3::get_offset_of_propertyName_0(),
	ProvidePropertyAttribute_tF58AE8F88029C5D1249A8A149173482A9BB08BC3::get_offset_of_receiverTypeName_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
