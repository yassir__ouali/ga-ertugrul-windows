﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// CodeStage.AntiCheat.Time.SpeedHackProofTime
struct SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
struct Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Char>
struct Dictionary_2_t1BF6D09B2C0226835F78B1BFABE6A8FA1030F08B;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E;
// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Style>
struct Dictionary_2_t1B12246F7055F99EF606808AA8031C87EC25F3C4;
// System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32>
struct Dictionary_2_t85CBDDBFA5D263E087932D42B863C888CEC9D354;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker/SpriteData>
struct List_1_tE7393D2C01F9068EFE97A136116883DEBE6A3D3E;
// System.Collections.Generic.List`1<TMPro.TMP_Sprite>
struct List_1_t8BBE089D87BC1A1157DE98EEB1348E833555E233;
// System.Collections.Generic.List`1<TMPro.TMP_SpriteAsset>
struct List_1_tE6AB11E0703EB02C9F65EEEB0B61E330B08E8C0B;
// System.Collections.Generic.List`1<TMPro.TMP_SpriteCharacter>
struct List_1_t0A3E8FF46D5FE9057636C7E2DBB12C5EDFC0A6A8;
// System.Collections.Generic.List`1<TMPro.TMP_SpriteGlyph>
struct List_1_tE0F8547D4420BB67D96E2E772D7EA26E193C345E;
// System.Collections.Generic.List`1<TMPro.TMP_Style>
struct List_1_t3C8C2798F78B514EA59D1ECB6A8BD82AD7F8A0F8;
// System.Collections.Generic.List`1<TMPro.TMP_Text>
struct List_1_t1073F21F60C9CB285CF2F0951405E58C95C20F90;
// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>
struct List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC;
// System.Func`2<TMPro.TMP_SpriteCharacter,System.UInt32>
struct Func_2_t1C8AB99415040E1EE4FD57EDA6E51A30F42355D3;
// System.Func`2<TMPro.TMP_SpriteGlyph,System.UInt32>
struct Func_2_t03DA4A88AE48124A7D4FE25F709EA4F7752FBFCE;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Random
struct Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.FastAction
struct FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB;
// TMPro.FastAction`1<System.Boolean>
struct FastAction_1_tCCD240F66F98E2B0797C8E1D4A3A4292A1EA025D;
// TMPro.FastAction`1<TMPro.TMP_ColorGradient>
struct FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9;
// TMPro.FastAction`1<UnityEngine.Object>
struct FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09;
// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset>
struct FastAction_2_t4B7909D9E4639D203432784287F6C947223EBE2F;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro>
struct FastAction_2_t5E47E91BEC2F90CCC3CF6559C3A7AACCF179088D;
// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI>
struct FastAction_2_t62452E49E55558D8C779CF5550B547347EDE0BFD;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Material>
struct FastAction_2_tCD2BB87052DC554C74E5BDB9067F719ADEB4924F;
// TMPro.FastAction`2<System.Boolean,UnityEngine.Object>
struct FastAction_2_tEE90F5B2E87756276EC3D85B1645A416171B8B4B;
// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs>
struct FastAction_2_t470741C1212B744F12DEA1C792E25238328F2763;
// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material>
struct FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4;
// TMPro.FontWeight[]
struct FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446;
// TMPro.MaterialReference[]
struct MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B;
// TMPro.RichTextTagAttribute[]
struct RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652;
// TMPro.TMP_Character
struct TMP_Character_t1875AACA978396521498D6A699052C187903553D;
// TMPro.TMP_CharacterInfo[]
struct TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604;
// TMPro.TMP_ColorGradient
struct TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7;
// TMPro.TMP_ColorGradient[]
struct TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C;
// TMPro.TMP_LineInfo[]
struct TMP_LineInfoU5BU5D_t3D5D11E746B537C3951927E490B7A1BAB9C23A5C;
// TMPro.TMP_LinkInfo[]
struct TMP_LinkInfoU5BU5D_t5965804162EB43CD70F792B74DA179B32224BB0D;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9;
// TMPro.TMP_PageInfo[]
struct TMP_PageInfoU5BU5D_tFB7F7AD2CD9ADBE07099C1A06170B51AA8D9D847;
// TMPro.TMP_SpriteAnimator
struct TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487;
// TMPro.TMP_Text
struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7;
// TMPro.TMP_Text/UnicodeChar[]
struct UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505;
// TMPro.TMP_TextElement
struct TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181;
// TMPro.TMP_WordInfo[]
struct TMP_WordInfoU5BU5D_t2C9C805935A8C8FFD43BF92C96AC70737AA52F09;
// TMPro.TextAlignmentOptions[]
struct TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B;
// TMPro.TextMeshPro
struct TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Material[]
struct MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MeshFilter
struct MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.TextCore.Glyph
struct Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;

struct Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 ;
struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D ;
struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 ;
struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E ;



#ifndef U3CMODULEU3E_T57A3C2925E0AB089D4622AEA90D6704E6A4ED13D_H
#define U3CMODULEU3E_T57A3C2925E0AB089D4622AEA90D6704E6A4ED13D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t57A3C2925E0AB089D4622AEA90D6704E6A4ED13D 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T57A3C2925E0AB089D4622AEA90D6704E6A4ED13D_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef OBSCUREDSTRING_T40CD1B096CD406BAD6EE65842C59A7814BE0B301_H
#define OBSCUREDSTRING_T40CD1B096CD406BAD6EE65842C59A7814BE0B301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredString
struct  ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301  : public RuntimeObject
{
public:
	// System.String CodeStage.AntiCheat.ObscuredTypes.ObscuredString::currentCryptoKey
	String_t* ___currentCryptoKey_0;
	// System.Byte[] CodeStage.AntiCheat.ObscuredTypes.ObscuredString::hiddenValue
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___hiddenValue_1;
	// System.Char[] CodeStage.AntiCheat.ObscuredTypes.ObscuredString::cryptoKey
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___cryptoKey_2;
	// System.Char[] CodeStage.AntiCheat.ObscuredTypes.ObscuredString::hiddenChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___hiddenChars_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredString::inited
	bool ___inited_4;
	// System.String CodeStage.AntiCheat.ObscuredTypes.ObscuredString::fakeValue
	String_t* ___fakeValue_5;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredString::fakeValueActive
	bool ___fakeValueActive_6;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301, ___currentCryptoKey_0)); }
	inline String_t* get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline String_t** get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(String_t* value)
	{
		___currentCryptoKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentCryptoKey_0), value);
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301, ___hiddenValue_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___hiddenValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___hiddenValue_1), value);
	}

	inline static int32_t get_offset_of_cryptoKey_2() { return static_cast<int32_t>(offsetof(ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301, ___cryptoKey_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_cryptoKey_2() const { return ___cryptoKey_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_cryptoKey_2() { return &___cryptoKey_2; }
	inline void set_cryptoKey_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___cryptoKey_2 = value;
		Il2CppCodeGenWriteBarrier((&___cryptoKey_2), value);
	}

	inline static int32_t get_offset_of_hiddenChars_3() { return static_cast<int32_t>(offsetof(ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301, ___hiddenChars_3)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_hiddenChars_3() const { return ___hiddenChars_3; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_hiddenChars_3() { return &___hiddenChars_3; }
	inline void set_hiddenChars_3(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___hiddenChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___hiddenChars_3), value);
	}

	inline static int32_t get_offset_of_inited_4() { return static_cast<int32_t>(offsetof(ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301, ___inited_4)); }
	inline bool get_inited_4() const { return ___inited_4; }
	inline bool* get_address_of_inited_4() { return &___inited_4; }
	inline void set_inited_4(bool value)
	{
		___inited_4 = value;
	}

	inline static int32_t get_offset_of_fakeValue_5() { return static_cast<int32_t>(offsetof(ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301, ___fakeValue_5)); }
	inline String_t* get_fakeValue_5() const { return ___fakeValue_5; }
	inline String_t** get_address_of_fakeValue_5() { return &___fakeValue_5; }
	inline void set_fakeValue_5(String_t* value)
	{
		___fakeValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___fakeValue_5), value);
	}

	inline static int32_t get_offset_of_fakeValueActive_6() { return static_cast<int32_t>(offsetof(ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301, ___fakeValueActive_6)); }
	inline bool get_fakeValueActive_6() const { return ___fakeValueActive_6; }
	inline bool* get_address_of_fakeValueActive_6() { return &___fakeValueActive_6; }
	inline void set_fakeValueActive_6(bool value)
	{
		___fakeValueActive_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSCUREDSTRING_T40CD1B096CD406BAD6EE65842C59A7814BE0B301_H
#ifndef BASE64UTILS_T79BFAC462546502E6252BEEEEF2C394BF13FC1E1_H
#define BASE64UTILS_T79BFAC462546502E6252BEEEEF2C394BF13FC1E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Utils.Base64Utils
struct  Base64Utils_t79BFAC462546502E6252BEEEEF2C394BF13FC1E1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE64UTILS_T79BFAC462546502E6252BEEEEF2C394BF13FC1E1_H
#ifndef RANDOMUTILS_T5051C16386B1EAA11EBD541E7D2591056760608B_H
#define RANDOMUTILS_T5051C16386B1EAA11EBD541E7D2591056760608B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Utils.RandomUtils
struct  RandomUtils_t5051C16386B1EAA11EBD541E7D2591056760608B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMUTILS_T5051C16386B1EAA11EBD541E7D2591056760608B_H
#ifndef STRINGUTILS_T15FD11EEF0ACC9FA70BC4A7B4746B6ECED981AE4_H
#define STRINGUTILS_T15FD11EEF0ACC9FA70BC4A7B4746B6ECED981AE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Utils.StringUtils
struct  StringUtils_t15FD11EEF0ACC9FA70BC4A7B4746B6ECED981AE4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T15FD11EEF0ACC9FA70BC4A7B4746B6ECED981AE4_H
#ifndef THREADSAFERANDOM_T73C8BC7D28AB4C56E5EA2130E90E6D88C4ED5D41_H
#define THREADSAFERANDOM_T73C8BC7D28AB4C56E5EA2130E90E6D88C4ED5D41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Utils.ThreadSafeRandom
struct  ThreadSafeRandom_t73C8BC7D28AB4C56E5EA2130E90E6D88C4ED5D41  : public RuntimeObject
{
public:

public:
};

struct ThreadSafeRandom_t73C8BC7D28AB4C56E5EA2130E90E6D88C4ED5D41_StaticFields
{
public:
	// System.Random CodeStage.AntiCheat.Utils.ThreadSafeRandom::Global
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___Global_0;

public:
	inline static int32_t get_offset_of_Global_0() { return static_cast<int32_t>(offsetof(ThreadSafeRandom_t73C8BC7D28AB4C56E5EA2130E90E6D88C4ED5D41_StaticFields, ___Global_0)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_Global_0() const { return ___Global_0; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_Global_0() { return &___Global_0; }
	inline void set_Global_0(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___Global_0 = value;
		Il2CppCodeGenWriteBarrier((&___Global_0), value);
	}
};

struct ThreadSafeRandom_t73C8BC7D28AB4C56E5EA2130E90E6D88C4ED5D41_ThreadStaticFields
{
public:
	// System.Random CodeStage.AntiCheat.Utils.ThreadSafeRandom::local
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___local_1;

public:
	inline static int32_t get_offset_of_local_1() { return static_cast<int32_t>(offsetof(ThreadSafeRandom_t73C8BC7D28AB4C56E5EA2130E90E6D88C4ED5D41_ThreadStaticFields, ___local_1)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_local_1() const { return ___local_1; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_local_1() { return &___local_1; }
	inline void set_local_1(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___local_1 = value;
		Il2CppCodeGenWriteBarrier((&___local_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSAFERANDOM_T73C8BC7D28AB4C56E5EA2130E90E6D88C4ED5D41_H
#ifndef TIMEUTILS_T4A0F1B90D14DDC898D601A4F397D051BF305C3CB_H
#define TIMEUTILS_T4A0F1B90D14DDC898D601A4F397D051BF305C3CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Utils.TimeUtils
struct  TimeUtils_t4A0F1B90D14DDC898D601A4F397D051BF305C3CB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEUTILS_T4A0F1B90D14DDC898D601A4F397D051BF305C3CB_H
#ifndef XXHASH_T31A700F9BD226933F618C4C4665F640CE97C2A51_H
#define XXHASH_T31A700F9BD226933F618C4C4665F640CE97C2A51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Utils.xxHash
struct  xxHash_t31A700F9BD226933F618C4C4665F640CE97C2A51  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XXHASH_T31A700F9BD226933F618C4C4665F640CE97C2A51_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef SHADERUTILITIES_T94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_H
#define SHADERUTILITIES_T94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ShaderUtilities
struct  ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8  : public RuntimeObject
{
public:

public:
};

struct ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields
{
public:
	// System.Int32 TMPro.ShaderUtilities::ID_MainTex
	int32_t ___ID_MainTex_0;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceTex
	int32_t ___ID_FaceTex_1;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceColor
	int32_t ___ID_FaceColor_2;
	// System.Int32 TMPro.ShaderUtilities::ID_FaceDilate
	int32_t ___ID_FaceDilate_3;
	// System.Int32 TMPro.ShaderUtilities::ID_Shininess
	int32_t ___ID_Shininess_4;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayColor
	int32_t ___ID_UnderlayColor_5;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetX
	int32_t ___ID_UnderlayOffsetX_6;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayOffsetY
	int32_t ___ID_UnderlayOffsetY_7;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlayDilate
	int32_t ___ID_UnderlayDilate_8;
	// System.Int32 TMPro.ShaderUtilities::ID_UnderlaySoftness
	int32_t ___ID_UnderlaySoftness_9;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightNormal
	int32_t ___ID_WeightNormal_10;
	// System.Int32 TMPro.ShaderUtilities::ID_WeightBold
	int32_t ___ID_WeightBold_11;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineTex
	int32_t ___ID_OutlineTex_12;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineWidth
	int32_t ___ID_OutlineWidth_13;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineSoftness
	int32_t ___ID_OutlineSoftness_14;
	// System.Int32 TMPro.ShaderUtilities::ID_OutlineColor
	int32_t ___ID_OutlineColor_15;
	// System.Int32 TMPro.ShaderUtilities::ID_Padding
	int32_t ___ID_Padding_16;
	// System.Int32 TMPro.ShaderUtilities::ID_GradientScale
	int32_t ___ID_GradientScale_17;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleX
	int32_t ___ID_ScaleX_18;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleY
	int32_t ___ID_ScaleY_19;
	// System.Int32 TMPro.ShaderUtilities::ID_PerspectiveFilter
	int32_t ___ID_PerspectiveFilter_20;
	// System.Int32 TMPro.ShaderUtilities::ID_Sharpness
	int32_t ___ID_Sharpness_21;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureWidth
	int32_t ___ID_TextureWidth_22;
	// System.Int32 TMPro.ShaderUtilities::ID_TextureHeight
	int32_t ___ID_TextureHeight_23;
	// System.Int32 TMPro.ShaderUtilities::ID_BevelAmount
	int32_t ___ID_BevelAmount_24;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowColor
	int32_t ___ID_GlowColor_25;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOffset
	int32_t ___ID_GlowOffset_26;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowPower
	int32_t ___ID_GlowPower_27;
	// System.Int32 TMPro.ShaderUtilities::ID_GlowOuter
	int32_t ___ID_GlowOuter_28;
	// System.Int32 TMPro.ShaderUtilities::ID_LightAngle
	int32_t ___ID_LightAngle_29;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMap
	int32_t ___ID_EnvMap_30;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrix
	int32_t ___ID_EnvMatrix_31;
	// System.Int32 TMPro.ShaderUtilities::ID_EnvMatrixRotation
	int32_t ___ID_EnvMatrixRotation_32;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskCoord
	int32_t ___ID_MaskCoord_33;
	// System.Int32 TMPro.ShaderUtilities::ID_ClipRect
	int32_t ___ID_ClipRect_34;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessX
	int32_t ___ID_MaskSoftnessX_35;
	// System.Int32 TMPro.ShaderUtilities::ID_MaskSoftnessY
	int32_t ___ID_MaskSoftnessY_36;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetX
	int32_t ___ID_VertexOffsetX_37;
	// System.Int32 TMPro.ShaderUtilities::ID_VertexOffsetY
	int32_t ___ID_VertexOffsetY_38;
	// System.Int32 TMPro.ShaderUtilities::ID_UseClipRect
	int32_t ___ID_UseClipRect_39;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilID
	int32_t ___ID_StencilID_40;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilOp
	int32_t ___ID_StencilOp_41;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilComp
	int32_t ___ID_StencilComp_42;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilReadMask
	int32_t ___ID_StencilReadMask_43;
	// System.Int32 TMPro.ShaderUtilities::ID_StencilWriteMask
	int32_t ___ID_StencilWriteMask_44;
	// System.Int32 TMPro.ShaderUtilities::ID_ShaderFlags
	int32_t ___ID_ShaderFlags_45;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_A
	int32_t ___ID_ScaleRatio_A_46;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_B
	int32_t ___ID_ScaleRatio_B_47;
	// System.Int32 TMPro.ShaderUtilities::ID_ScaleRatio_C
	int32_t ___ID_ScaleRatio_C_48;
	// System.String TMPro.ShaderUtilities::Keyword_Bevel
	String_t* ___Keyword_Bevel_49;
	// System.String TMPro.ShaderUtilities::Keyword_Glow
	String_t* ___Keyword_Glow_50;
	// System.String TMPro.ShaderUtilities::Keyword_Underlay
	String_t* ___Keyword_Underlay_51;
	// System.String TMPro.ShaderUtilities::Keyword_Ratios
	String_t* ___Keyword_Ratios_52;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_SOFT
	String_t* ___Keyword_MASK_SOFT_53;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_HARD
	String_t* ___Keyword_MASK_HARD_54;
	// System.String TMPro.ShaderUtilities::Keyword_MASK_TEX
	String_t* ___Keyword_MASK_TEX_55;
	// System.String TMPro.ShaderUtilities::Keyword_Outline
	String_t* ___Keyword_Outline_56;
	// System.String TMPro.ShaderUtilities::ShaderTag_ZTestMode
	String_t* ___ShaderTag_ZTestMode_57;
	// System.String TMPro.ShaderUtilities::ShaderTag_CullMode
	String_t* ___ShaderTag_CullMode_58;
	// System.Single TMPro.ShaderUtilities::m_clamp
	float ___m_clamp_59;
	// System.Boolean TMPro.ShaderUtilities::isInitialized
	bool ___isInitialized_60;
	// UnityEngine.Shader TMPro.ShaderUtilities::k_ShaderRef_MobileSDF
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___k_ShaderRef_MobileSDF_61;
	// UnityEngine.Shader TMPro.ShaderUtilities::k_ShaderRef_MobileBitmap
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___k_ShaderRef_MobileBitmap_62;

public:
	inline static int32_t get_offset_of_ID_MainTex_0() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_MainTex_0)); }
	inline int32_t get_ID_MainTex_0() const { return ___ID_MainTex_0; }
	inline int32_t* get_address_of_ID_MainTex_0() { return &___ID_MainTex_0; }
	inline void set_ID_MainTex_0(int32_t value)
	{
		___ID_MainTex_0 = value;
	}

	inline static int32_t get_offset_of_ID_FaceTex_1() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_FaceTex_1)); }
	inline int32_t get_ID_FaceTex_1() const { return ___ID_FaceTex_1; }
	inline int32_t* get_address_of_ID_FaceTex_1() { return &___ID_FaceTex_1; }
	inline void set_ID_FaceTex_1(int32_t value)
	{
		___ID_FaceTex_1 = value;
	}

	inline static int32_t get_offset_of_ID_FaceColor_2() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_FaceColor_2)); }
	inline int32_t get_ID_FaceColor_2() const { return ___ID_FaceColor_2; }
	inline int32_t* get_address_of_ID_FaceColor_2() { return &___ID_FaceColor_2; }
	inline void set_ID_FaceColor_2(int32_t value)
	{
		___ID_FaceColor_2 = value;
	}

	inline static int32_t get_offset_of_ID_FaceDilate_3() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_FaceDilate_3)); }
	inline int32_t get_ID_FaceDilate_3() const { return ___ID_FaceDilate_3; }
	inline int32_t* get_address_of_ID_FaceDilate_3() { return &___ID_FaceDilate_3; }
	inline void set_ID_FaceDilate_3(int32_t value)
	{
		___ID_FaceDilate_3 = value;
	}

	inline static int32_t get_offset_of_ID_Shininess_4() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_Shininess_4)); }
	inline int32_t get_ID_Shininess_4() const { return ___ID_Shininess_4; }
	inline int32_t* get_address_of_ID_Shininess_4() { return &___ID_Shininess_4; }
	inline void set_ID_Shininess_4(int32_t value)
	{
		___ID_Shininess_4 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayColor_5() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlayColor_5)); }
	inline int32_t get_ID_UnderlayColor_5() const { return ___ID_UnderlayColor_5; }
	inline int32_t* get_address_of_ID_UnderlayColor_5() { return &___ID_UnderlayColor_5; }
	inline void set_ID_UnderlayColor_5(int32_t value)
	{
		___ID_UnderlayColor_5 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetX_6() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlayOffsetX_6)); }
	inline int32_t get_ID_UnderlayOffsetX_6() const { return ___ID_UnderlayOffsetX_6; }
	inline int32_t* get_address_of_ID_UnderlayOffsetX_6() { return &___ID_UnderlayOffsetX_6; }
	inline void set_ID_UnderlayOffsetX_6(int32_t value)
	{
		___ID_UnderlayOffsetX_6 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayOffsetY_7() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlayOffsetY_7)); }
	inline int32_t get_ID_UnderlayOffsetY_7() const { return ___ID_UnderlayOffsetY_7; }
	inline int32_t* get_address_of_ID_UnderlayOffsetY_7() { return &___ID_UnderlayOffsetY_7; }
	inline void set_ID_UnderlayOffsetY_7(int32_t value)
	{
		___ID_UnderlayOffsetY_7 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlayDilate_8() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlayDilate_8)); }
	inline int32_t get_ID_UnderlayDilate_8() const { return ___ID_UnderlayDilate_8; }
	inline int32_t* get_address_of_ID_UnderlayDilate_8() { return &___ID_UnderlayDilate_8; }
	inline void set_ID_UnderlayDilate_8(int32_t value)
	{
		___ID_UnderlayDilate_8 = value;
	}

	inline static int32_t get_offset_of_ID_UnderlaySoftness_9() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UnderlaySoftness_9)); }
	inline int32_t get_ID_UnderlaySoftness_9() const { return ___ID_UnderlaySoftness_9; }
	inline int32_t* get_address_of_ID_UnderlaySoftness_9() { return &___ID_UnderlaySoftness_9; }
	inline void set_ID_UnderlaySoftness_9(int32_t value)
	{
		___ID_UnderlaySoftness_9 = value;
	}

	inline static int32_t get_offset_of_ID_WeightNormal_10() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_WeightNormal_10)); }
	inline int32_t get_ID_WeightNormal_10() const { return ___ID_WeightNormal_10; }
	inline int32_t* get_address_of_ID_WeightNormal_10() { return &___ID_WeightNormal_10; }
	inline void set_ID_WeightNormal_10(int32_t value)
	{
		___ID_WeightNormal_10 = value;
	}

	inline static int32_t get_offset_of_ID_WeightBold_11() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_WeightBold_11)); }
	inline int32_t get_ID_WeightBold_11() const { return ___ID_WeightBold_11; }
	inline int32_t* get_address_of_ID_WeightBold_11() { return &___ID_WeightBold_11; }
	inline void set_ID_WeightBold_11(int32_t value)
	{
		___ID_WeightBold_11 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineTex_12() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_OutlineTex_12)); }
	inline int32_t get_ID_OutlineTex_12() const { return ___ID_OutlineTex_12; }
	inline int32_t* get_address_of_ID_OutlineTex_12() { return &___ID_OutlineTex_12; }
	inline void set_ID_OutlineTex_12(int32_t value)
	{
		___ID_OutlineTex_12 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineWidth_13() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_OutlineWidth_13)); }
	inline int32_t get_ID_OutlineWidth_13() const { return ___ID_OutlineWidth_13; }
	inline int32_t* get_address_of_ID_OutlineWidth_13() { return &___ID_OutlineWidth_13; }
	inline void set_ID_OutlineWidth_13(int32_t value)
	{
		___ID_OutlineWidth_13 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineSoftness_14() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_OutlineSoftness_14)); }
	inline int32_t get_ID_OutlineSoftness_14() const { return ___ID_OutlineSoftness_14; }
	inline int32_t* get_address_of_ID_OutlineSoftness_14() { return &___ID_OutlineSoftness_14; }
	inline void set_ID_OutlineSoftness_14(int32_t value)
	{
		___ID_OutlineSoftness_14 = value;
	}

	inline static int32_t get_offset_of_ID_OutlineColor_15() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_OutlineColor_15)); }
	inline int32_t get_ID_OutlineColor_15() const { return ___ID_OutlineColor_15; }
	inline int32_t* get_address_of_ID_OutlineColor_15() { return &___ID_OutlineColor_15; }
	inline void set_ID_OutlineColor_15(int32_t value)
	{
		___ID_OutlineColor_15 = value;
	}

	inline static int32_t get_offset_of_ID_Padding_16() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_Padding_16)); }
	inline int32_t get_ID_Padding_16() const { return ___ID_Padding_16; }
	inline int32_t* get_address_of_ID_Padding_16() { return &___ID_Padding_16; }
	inline void set_ID_Padding_16(int32_t value)
	{
		___ID_Padding_16 = value;
	}

	inline static int32_t get_offset_of_ID_GradientScale_17() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GradientScale_17)); }
	inline int32_t get_ID_GradientScale_17() const { return ___ID_GradientScale_17; }
	inline int32_t* get_address_of_ID_GradientScale_17() { return &___ID_GradientScale_17; }
	inline void set_ID_GradientScale_17(int32_t value)
	{
		___ID_GradientScale_17 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleX_18() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleX_18)); }
	inline int32_t get_ID_ScaleX_18() const { return ___ID_ScaleX_18; }
	inline int32_t* get_address_of_ID_ScaleX_18() { return &___ID_ScaleX_18; }
	inline void set_ID_ScaleX_18(int32_t value)
	{
		___ID_ScaleX_18 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleY_19() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleY_19)); }
	inline int32_t get_ID_ScaleY_19() const { return ___ID_ScaleY_19; }
	inline int32_t* get_address_of_ID_ScaleY_19() { return &___ID_ScaleY_19; }
	inline void set_ID_ScaleY_19(int32_t value)
	{
		___ID_ScaleY_19 = value;
	}

	inline static int32_t get_offset_of_ID_PerspectiveFilter_20() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_PerspectiveFilter_20)); }
	inline int32_t get_ID_PerspectiveFilter_20() const { return ___ID_PerspectiveFilter_20; }
	inline int32_t* get_address_of_ID_PerspectiveFilter_20() { return &___ID_PerspectiveFilter_20; }
	inline void set_ID_PerspectiveFilter_20(int32_t value)
	{
		___ID_PerspectiveFilter_20 = value;
	}

	inline static int32_t get_offset_of_ID_Sharpness_21() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_Sharpness_21)); }
	inline int32_t get_ID_Sharpness_21() const { return ___ID_Sharpness_21; }
	inline int32_t* get_address_of_ID_Sharpness_21() { return &___ID_Sharpness_21; }
	inline void set_ID_Sharpness_21(int32_t value)
	{
		___ID_Sharpness_21 = value;
	}

	inline static int32_t get_offset_of_ID_TextureWidth_22() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_TextureWidth_22)); }
	inline int32_t get_ID_TextureWidth_22() const { return ___ID_TextureWidth_22; }
	inline int32_t* get_address_of_ID_TextureWidth_22() { return &___ID_TextureWidth_22; }
	inline void set_ID_TextureWidth_22(int32_t value)
	{
		___ID_TextureWidth_22 = value;
	}

	inline static int32_t get_offset_of_ID_TextureHeight_23() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_TextureHeight_23)); }
	inline int32_t get_ID_TextureHeight_23() const { return ___ID_TextureHeight_23; }
	inline int32_t* get_address_of_ID_TextureHeight_23() { return &___ID_TextureHeight_23; }
	inline void set_ID_TextureHeight_23(int32_t value)
	{
		___ID_TextureHeight_23 = value;
	}

	inline static int32_t get_offset_of_ID_BevelAmount_24() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_BevelAmount_24)); }
	inline int32_t get_ID_BevelAmount_24() const { return ___ID_BevelAmount_24; }
	inline int32_t* get_address_of_ID_BevelAmount_24() { return &___ID_BevelAmount_24; }
	inline void set_ID_BevelAmount_24(int32_t value)
	{
		___ID_BevelAmount_24 = value;
	}

	inline static int32_t get_offset_of_ID_GlowColor_25() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GlowColor_25)); }
	inline int32_t get_ID_GlowColor_25() const { return ___ID_GlowColor_25; }
	inline int32_t* get_address_of_ID_GlowColor_25() { return &___ID_GlowColor_25; }
	inline void set_ID_GlowColor_25(int32_t value)
	{
		___ID_GlowColor_25 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOffset_26() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GlowOffset_26)); }
	inline int32_t get_ID_GlowOffset_26() const { return ___ID_GlowOffset_26; }
	inline int32_t* get_address_of_ID_GlowOffset_26() { return &___ID_GlowOffset_26; }
	inline void set_ID_GlowOffset_26(int32_t value)
	{
		___ID_GlowOffset_26 = value;
	}

	inline static int32_t get_offset_of_ID_GlowPower_27() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GlowPower_27)); }
	inline int32_t get_ID_GlowPower_27() const { return ___ID_GlowPower_27; }
	inline int32_t* get_address_of_ID_GlowPower_27() { return &___ID_GlowPower_27; }
	inline void set_ID_GlowPower_27(int32_t value)
	{
		___ID_GlowPower_27 = value;
	}

	inline static int32_t get_offset_of_ID_GlowOuter_28() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_GlowOuter_28)); }
	inline int32_t get_ID_GlowOuter_28() const { return ___ID_GlowOuter_28; }
	inline int32_t* get_address_of_ID_GlowOuter_28() { return &___ID_GlowOuter_28; }
	inline void set_ID_GlowOuter_28(int32_t value)
	{
		___ID_GlowOuter_28 = value;
	}

	inline static int32_t get_offset_of_ID_LightAngle_29() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_LightAngle_29)); }
	inline int32_t get_ID_LightAngle_29() const { return ___ID_LightAngle_29; }
	inline int32_t* get_address_of_ID_LightAngle_29() { return &___ID_LightAngle_29; }
	inline void set_ID_LightAngle_29(int32_t value)
	{
		___ID_LightAngle_29 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMap_30() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_EnvMap_30)); }
	inline int32_t get_ID_EnvMap_30() const { return ___ID_EnvMap_30; }
	inline int32_t* get_address_of_ID_EnvMap_30() { return &___ID_EnvMap_30; }
	inline void set_ID_EnvMap_30(int32_t value)
	{
		___ID_EnvMap_30 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrix_31() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_EnvMatrix_31)); }
	inline int32_t get_ID_EnvMatrix_31() const { return ___ID_EnvMatrix_31; }
	inline int32_t* get_address_of_ID_EnvMatrix_31() { return &___ID_EnvMatrix_31; }
	inline void set_ID_EnvMatrix_31(int32_t value)
	{
		___ID_EnvMatrix_31 = value;
	}

	inline static int32_t get_offset_of_ID_EnvMatrixRotation_32() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_EnvMatrixRotation_32)); }
	inline int32_t get_ID_EnvMatrixRotation_32() const { return ___ID_EnvMatrixRotation_32; }
	inline int32_t* get_address_of_ID_EnvMatrixRotation_32() { return &___ID_EnvMatrixRotation_32; }
	inline void set_ID_EnvMatrixRotation_32(int32_t value)
	{
		___ID_EnvMatrixRotation_32 = value;
	}

	inline static int32_t get_offset_of_ID_MaskCoord_33() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_MaskCoord_33)); }
	inline int32_t get_ID_MaskCoord_33() const { return ___ID_MaskCoord_33; }
	inline int32_t* get_address_of_ID_MaskCoord_33() { return &___ID_MaskCoord_33; }
	inline void set_ID_MaskCoord_33(int32_t value)
	{
		___ID_MaskCoord_33 = value;
	}

	inline static int32_t get_offset_of_ID_ClipRect_34() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ClipRect_34)); }
	inline int32_t get_ID_ClipRect_34() const { return ___ID_ClipRect_34; }
	inline int32_t* get_address_of_ID_ClipRect_34() { return &___ID_ClipRect_34; }
	inline void set_ID_ClipRect_34(int32_t value)
	{
		___ID_ClipRect_34 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessX_35() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_MaskSoftnessX_35)); }
	inline int32_t get_ID_MaskSoftnessX_35() const { return ___ID_MaskSoftnessX_35; }
	inline int32_t* get_address_of_ID_MaskSoftnessX_35() { return &___ID_MaskSoftnessX_35; }
	inline void set_ID_MaskSoftnessX_35(int32_t value)
	{
		___ID_MaskSoftnessX_35 = value;
	}

	inline static int32_t get_offset_of_ID_MaskSoftnessY_36() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_MaskSoftnessY_36)); }
	inline int32_t get_ID_MaskSoftnessY_36() const { return ___ID_MaskSoftnessY_36; }
	inline int32_t* get_address_of_ID_MaskSoftnessY_36() { return &___ID_MaskSoftnessY_36; }
	inline void set_ID_MaskSoftnessY_36(int32_t value)
	{
		___ID_MaskSoftnessY_36 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetX_37() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_VertexOffsetX_37)); }
	inline int32_t get_ID_VertexOffsetX_37() const { return ___ID_VertexOffsetX_37; }
	inline int32_t* get_address_of_ID_VertexOffsetX_37() { return &___ID_VertexOffsetX_37; }
	inline void set_ID_VertexOffsetX_37(int32_t value)
	{
		___ID_VertexOffsetX_37 = value;
	}

	inline static int32_t get_offset_of_ID_VertexOffsetY_38() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_VertexOffsetY_38)); }
	inline int32_t get_ID_VertexOffsetY_38() const { return ___ID_VertexOffsetY_38; }
	inline int32_t* get_address_of_ID_VertexOffsetY_38() { return &___ID_VertexOffsetY_38; }
	inline void set_ID_VertexOffsetY_38(int32_t value)
	{
		___ID_VertexOffsetY_38 = value;
	}

	inline static int32_t get_offset_of_ID_UseClipRect_39() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_UseClipRect_39)); }
	inline int32_t get_ID_UseClipRect_39() const { return ___ID_UseClipRect_39; }
	inline int32_t* get_address_of_ID_UseClipRect_39() { return &___ID_UseClipRect_39; }
	inline void set_ID_UseClipRect_39(int32_t value)
	{
		___ID_UseClipRect_39 = value;
	}

	inline static int32_t get_offset_of_ID_StencilID_40() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilID_40)); }
	inline int32_t get_ID_StencilID_40() const { return ___ID_StencilID_40; }
	inline int32_t* get_address_of_ID_StencilID_40() { return &___ID_StencilID_40; }
	inline void set_ID_StencilID_40(int32_t value)
	{
		___ID_StencilID_40 = value;
	}

	inline static int32_t get_offset_of_ID_StencilOp_41() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilOp_41)); }
	inline int32_t get_ID_StencilOp_41() const { return ___ID_StencilOp_41; }
	inline int32_t* get_address_of_ID_StencilOp_41() { return &___ID_StencilOp_41; }
	inline void set_ID_StencilOp_41(int32_t value)
	{
		___ID_StencilOp_41 = value;
	}

	inline static int32_t get_offset_of_ID_StencilComp_42() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilComp_42)); }
	inline int32_t get_ID_StencilComp_42() const { return ___ID_StencilComp_42; }
	inline int32_t* get_address_of_ID_StencilComp_42() { return &___ID_StencilComp_42; }
	inline void set_ID_StencilComp_42(int32_t value)
	{
		___ID_StencilComp_42 = value;
	}

	inline static int32_t get_offset_of_ID_StencilReadMask_43() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilReadMask_43)); }
	inline int32_t get_ID_StencilReadMask_43() const { return ___ID_StencilReadMask_43; }
	inline int32_t* get_address_of_ID_StencilReadMask_43() { return &___ID_StencilReadMask_43; }
	inline void set_ID_StencilReadMask_43(int32_t value)
	{
		___ID_StencilReadMask_43 = value;
	}

	inline static int32_t get_offset_of_ID_StencilWriteMask_44() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_StencilWriteMask_44)); }
	inline int32_t get_ID_StencilWriteMask_44() const { return ___ID_StencilWriteMask_44; }
	inline int32_t* get_address_of_ID_StencilWriteMask_44() { return &___ID_StencilWriteMask_44; }
	inline void set_ID_StencilWriteMask_44(int32_t value)
	{
		___ID_StencilWriteMask_44 = value;
	}

	inline static int32_t get_offset_of_ID_ShaderFlags_45() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ShaderFlags_45)); }
	inline int32_t get_ID_ShaderFlags_45() const { return ___ID_ShaderFlags_45; }
	inline int32_t* get_address_of_ID_ShaderFlags_45() { return &___ID_ShaderFlags_45; }
	inline void set_ID_ShaderFlags_45(int32_t value)
	{
		___ID_ShaderFlags_45 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_A_46() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleRatio_A_46)); }
	inline int32_t get_ID_ScaleRatio_A_46() const { return ___ID_ScaleRatio_A_46; }
	inline int32_t* get_address_of_ID_ScaleRatio_A_46() { return &___ID_ScaleRatio_A_46; }
	inline void set_ID_ScaleRatio_A_46(int32_t value)
	{
		___ID_ScaleRatio_A_46 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_B_47() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleRatio_B_47)); }
	inline int32_t get_ID_ScaleRatio_B_47() const { return ___ID_ScaleRatio_B_47; }
	inline int32_t* get_address_of_ID_ScaleRatio_B_47() { return &___ID_ScaleRatio_B_47; }
	inline void set_ID_ScaleRatio_B_47(int32_t value)
	{
		___ID_ScaleRatio_B_47 = value;
	}

	inline static int32_t get_offset_of_ID_ScaleRatio_C_48() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ID_ScaleRatio_C_48)); }
	inline int32_t get_ID_ScaleRatio_C_48() const { return ___ID_ScaleRatio_C_48; }
	inline int32_t* get_address_of_ID_ScaleRatio_C_48() { return &___ID_ScaleRatio_C_48; }
	inline void set_ID_ScaleRatio_C_48(int32_t value)
	{
		___ID_ScaleRatio_C_48 = value;
	}

	inline static int32_t get_offset_of_Keyword_Bevel_49() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Bevel_49)); }
	inline String_t* get_Keyword_Bevel_49() const { return ___Keyword_Bevel_49; }
	inline String_t** get_address_of_Keyword_Bevel_49() { return &___Keyword_Bevel_49; }
	inline void set_Keyword_Bevel_49(String_t* value)
	{
		___Keyword_Bevel_49 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Bevel_49), value);
	}

	inline static int32_t get_offset_of_Keyword_Glow_50() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Glow_50)); }
	inline String_t* get_Keyword_Glow_50() const { return ___Keyword_Glow_50; }
	inline String_t** get_address_of_Keyword_Glow_50() { return &___Keyword_Glow_50; }
	inline void set_Keyword_Glow_50(String_t* value)
	{
		___Keyword_Glow_50 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Glow_50), value);
	}

	inline static int32_t get_offset_of_Keyword_Underlay_51() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Underlay_51)); }
	inline String_t* get_Keyword_Underlay_51() const { return ___Keyword_Underlay_51; }
	inline String_t** get_address_of_Keyword_Underlay_51() { return &___Keyword_Underlay_51; }
	inline void set_Keyword_Underlay_51(String_t* value)
	{
		___Keyword_Underlay_51 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Underlay_51), value);
	}

	inline static int32_t get_offset_of_Keyword_Ratios_52() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Ratios_52)); }
	inline String_t* get_Keyword_Ratios_52() const { return ___Keyword_Ratios_52; }
	inline String_t** get_address_of_Keyword_Ratios_52() { return &___Keyword_Ratios_52; }
	inline void set_Keyword_Ratios_52(String_t* value)
	{
		___Keyword_Ratios_52 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Ratios_52), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_SOFT_53() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_MASK_SOFT_53)); }
	inline String_t* get_Keyword_MASK_SOFT_53() const { return ___Keyword_MASK_SOFT_53; }
	inline String_t** get_address_of_Keyword_MASK_SOFT_53() { return &___Keyword_MASK_SOFT_53; }
	inline void set_Keyword_MASK_SOFT_53(String_t* value)
	{
		___Keyword_MASK_SOFT_53 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_SOFT_53), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_HARD_54() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_MASK_HARD_54)); }
	inline String_t* get_Keyword_MASK_HARD_54() const { return ___Keyword_MASK_HARD_54; }
	inline String_t** get_address_of_Keyword_MASK_HARD_54() { return &___Keyword_MASK_HARD_54; }
	inline void set_Keyword_MASK_HARD_54(String_t* value)
	{
		___Keyword_MASK_HARD_54 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_HARD_54), value);
	}

	inline static int32_t get_offset_of_Keyword_MASK_TEX_55() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_MASK_TEX_55)); }
	inline String_t* get_Keyword_MASK_TEX_55() const { return ___Keyword_MASK_TEX_55; }
	inline String_t** get_address_of_Keyword_MASK_TEX_55() { return &___Keyword_MASK_TEX_55; }
	inline void set_Keyword_MASK_TEX_55(String_t* value)
	{
		___Keyword_MASK_TEX_55 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_MASK_TEX_55), value);
	}

	inline static int32_t get_offset_of_Keyword_Outline_56() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___Keyword_Outline_56)); }
	inline String_t* get_Keyword_Outline_56() const { return ___Keyword_Outline_56; }
	inline String_t** get_address_of_Keyword_Outline_56() { return &___Keyword_Outline_56; }
	inline void set_Keyword_Outline_56(String_t* value)
	{
		___Keyword_Outline_56 = value;
		Il2CppCodeGenWriteBarrier((&___Keyword_Outline_56), value);
	}

	inline static int32_t get_offset_of_ShaderTag_ZTestMode_57() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ShaderTag_ZTestMode_57)); }
	inline String_t* get_ShaderTag_ZTestMode_57() const { return ___ShaderTag_ZTestMode_57; }
	inline String_t** get_address_of_ShaderTag_ZTestMode_57() { return &___ShaderTag_ZTestMode_57; }
	inline void set_ShaderTag_ZTestMode_57(String_t* value)
	{
		___ShaderTag_ZTestMode_57 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_ZTestMode_57), value);
	}

	inline static int32_t get_offset_of_ShaderTag_CullMode_58() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___ShaderTag_CullMode_58)); }
	inline String_t* get_ShaderTag_CullMode_58() const { return ___ShaderTag_CullMode_58; }
	inline String_t** get_address_of_ShaderTag_CullMode_58() { return &___ShaderTag_CullMode_58; }
	inline void set_ShaderTag_CullMode_58(String_t* value)
	{
		___ShaderTag_CullMode_58 = value;
		Il2CppCodeGenWriteBarrier((&___ShaderTag_CullMode_58), value);
	}

	inline static int32_t get_offset_of_m_clamp_59() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___m_clamp_59)); }
	inline float get_m_clamp_59() const { return ___m_clamp_59; }
	inline float* get_address_of_m_clamp_59() { return &___m_clamp_59; }
	inline void set_m_clamp_59(float value)
	{
		___m_clamp_59 = value;
	}

	inline static int32_t get_offset_of_isInitialized_60() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___isInitialized_60)); }
	inline bool get_isInitialized_60() const { return ___isInitialized_60; }
	inline bool* get_address_of_isInitialized_60() { return &___isInitialized_60; }
	inline void set_isInitialized_60(bool value)
	{
		___isInitialized_60 = value;
	}

	inline static int32_t get_offset_of_k_ShaderRef_MobileSDF_61() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___k_ShaderRef_MobileSDF_61)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_k_ShaderRef_MobileSDF_61() const { return ___k_ShaderRef_MobileSDF_61; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_k_ShaderRef_MobileSDF_61() { return &___k_ShaderRef_MobileSDF_61; }
	inline void set_k_ShaderRef_MobileSDF_61(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___k_ShaderRef_MobileSDF_61 = value;
		Il2CppCodeGenWriteBarrier((&___k_ShaderRef_MobileSDF_61), value);
	}

	inline static int32_t get_offset_of_k_ShaderRef_MobileBitmap_62() { return static_cast<int32_t>(offsetof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields, ___k_ShaderRef_MobileBitmap_62)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_k_ShaderRef_MobileBitmap_62() const { return ___k_ShaderRef_MobileBitmap_62; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_k_ShaderRef_MobileBitmap_62() { return &___k_ShaderRef_MobileBitmap_62; }
	inline void set_k_ShaderRef_MobileBitmap_62(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___k_ShaderRef_MobileBitmap_62 = value;
		Il2CppCodeGenWriteBarrier((&___k_ShaderRef_MobileBitmap_62), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERUTILITIES_T94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_H
#ifndef TEXTUREPACKER_T2549189919276EB833F83EA937AB992420E1B199_H
#define TEXTUREPACKER_T2549189919276EB833F83EA937AB992420E1B199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker
struct  TexturePacker_t2549189919276EB833F83EA937AB992420E1B199  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREPACKER_T2549189919276EB833F83EA937AB992420E1B199_H
#ifndef SPRITEDATAOBJECT_T3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E_H
#define SPRITEDATAOBJECT_T3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteDataObject
struct  SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.SpriteAssetUtilities.TexturePacker_SpriteData> TMPro.SpriteAssetUtilities.TexturePacker_SpriteDataObject::frames
	List_1_tE7393D2C01F9068EFE97A136116883DEBE6A3D3E * ___frames_0;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E, ___frames_0)); }
	inline List_1_tE7393D2C01F9068EFE97A136116883DEBE6A3D3E * get_frames_0() const { return ___frames_0; }
	inline List_1_tE7393D2C01F9068EFE97A136116883DEBE6A3D3E ** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(List_1_tE7393D2C01F9068EFE97A136116883DEBE6A3D3E * value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEDATAOBJECT_T3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E_H
#ifndef LINEBREAKINGTABLE_TB80E0533075B07F5080E99393CEF91FDC8C2AB25_H
#define LINEBREAKINGTABLE_TB80E0533075B07F5080E99393CEF91FDC8C2AB25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Settings_LineBreakingTable
struct  LineBreakingTable_tB80E0533075B07F5080E99393CEF91FDC8C2AB25  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Char> TMPro.TMP_Settings_LineBreakingTable::leadingCharacters
	Dictionary_2_t1BF6D09B2C0226835F78B1BFABE6A8FA1030F08B * ___leadingCharacters_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Char> TMPro.TMP_Settings_LineBreakingTable::followingCharacters
	Dictionary_2_t1BF6D09B2C0226835F78B1BFABE6A8FA1030F08B * ___followingCharacters_1;

public:
	inline static int32_t get_offset_of_leadingCharacters_0() { return static_cast<int32_t>(offsetof(LineBreakingTable_tB80E0533075B07F5080E99393CEF91FDC8C2AB25, ___leadingCharacters_0)); }
	inline Dictionary_2_t1BF6D09B2C0226835F78B1BFABE6A8FA1030F08B * get_leadingCharacters_0() const { return ___leadingCharacters_0; }
	inline Dictionary_2_t1BF6D09B2C0226835F78B1BFABE6A8FA1030F08B ** get_address_of_leadingCharacters_0() { return &___leadingCharacters_0; }
	inline void set_leadingCharacters_0(Dictionary_2_t1BF6D09B2C0226835F78B1BFABE6A8FA1030F08B * value)
	{
		___leadingCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___leadingCharacters_0), value);
	}

	inline static int32_t get_offset_of_followingCharacters_1() { return static_cast<int32_t>(offsetof(LineBreakingTable_tB80E0533075B07F5080E99393CEF91FDC8C2AB25, ___followingCharacters_1)); }
	inline Dictionary_2_t1BF6D09B2C0226835F78B1BFABE6A8FA1030F08B * get_followingCharacters_1() const { return ___followingCharacters_1; }
	inline Dictionary_2_t1BF6D09B2C0226835F78B1BFABE6A8FA1030F08B ** get_address_of_followingCharacters_1() { return &___followingCharacters_1; }
	inline void set_followingCharacters_1(Dictionary_2_t1BF6D09B2C0226835F78B1BFABE6A8FA1030F08B * value)
	{
		___followingCharacters_1 = value;
		Il2CppCodeGenWriteBarrier((&___followingCharacters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEBREAKINGTABLE_TB80E0533075B07F5080E99393CEF91FDC8C2AB25_H
#ifndef U3CU3EC_T685586B0F954FF1162ABB09FB424BB1AED63C346_H
#define U3CU3EC_T685586B0F954FF1162ABB09FB424BB1AED63C346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAsset_<>c
struct  U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346_StaticFields
{
public:
	// TMPro.TMP_SpriteAsset_<>c TMPro.TMP_SpriteAsset_<>c::<>9
	U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346 * ___U3CU3E9_0;
	// System.Func`2<TMPro.TMP_SpriteGlyph,System.UInt32> TMPro.TMP_SpriteAsset_<>c::<>9__32_0
	Func_2_t03DA4A88AE48124A7D4FE25F709EA4F7752FBFCE * ___U3CU3E9__32_0_1;
	// System.Func`2<TMPro.TMP_SpriteCharacter,System.UInt32> TMPro.TMP_SpriteAsset_<>c::<>9__33_0
	Func_2_t1C8AB99415040E1EE4FD57EDA6E51A30F42355D3 * ___U3CU3E9__33_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__32_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346_StaticFields, ___U3CU3E9__32_0_1)); }
	inline Func_2_t03DA4A88AE48124A7D4FE25F709EA4F7752FBFCE * get_U3CU3E9__32_0_1() const { return ___U3CU3E9__32_0_1; }
	inline Func_2_t03DA4A88AE48124A7D4FE25F709EA4F7752FBFCE ** get_address_of_U3CU3E9__32_0_1() { return &___U3CU3E9__32_0_1; }
	inline void set_U3CU3E9__32_0_1(Func_2_t03DA4A88AE48124A7D4FE25F709EA4F7752FBFCE * value)
	{
		___U3CU3E9__32_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__32_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__33_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346_StaticFields, ___U3CU3E9__33_0_2)); }
	inline Func_2_t1C8AB99415040E1EE4FD57EDA6E51A30F42355D3 * get_U3CU3E9__33_0_2() const { return ___U3CU3E9__33_0_2; }
	inline Func_2_t1C8AB99415040E1EE4FD57EDA6E51A30F42355D3 ** get_address_of_U3CU3E9__33_0_2() { return &___U3CU3E9__33_0_2; }
	inline void set_U3CU3E9__33_0_2(Func_2_t1C8AB99415040E1EE4FD57EDA6E51A30F42355D3 * value)
	{
		___U3CU3E9__33_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__33_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T685586B0F954FF1162ABB09FB424BB1AED63C346_H
#ifndef TMP_STYLE_T9FD01084B9E3F1D4B92E87114C454C98BA20FBAD_H
#define TMP_STYLE_T9FD01084B9E3F1D4B92E87114C454C98BA20FBAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Style
struct  TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD  : public RuntimeObject
{
public:
	// System.String TMPro.TMP_Style::m_Name
	String_t* ___m_Name_0;
	// System.Int32 TMPro.TMP_Style::m_HashCode
	int32_t ___m_HashCode_1;
	// System.String TMPro.TMP_Style::m_OpeningDefinition
	String_t* ___m_OpeningDefinition_2;
	// System.String TMPro.TMP_Style::m_ClosingDefinition
	String_t* ___m_ClosingDefinition_3;
	// System.Int32[] TMPro.TMP_Style::m_OpeningTagArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_OpeningTagArray_4;
	// System.Int32[] TMPro.TMP_Style::m_ClosingTagArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_ClosingTagArray_5;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_0), value);
	}

	inline static int32_t get_offset_of_m_HashCode_1() { return static_cast<int32_t>(offsetof(TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD, ___m_HashCode_1)); }
	inline int32_t get_m_HashCode_1() const { return ___m_HashCode_1; }
	inline int32_t* get_address_of_m_HashCode_1() { return &___m_HashCode_1; }
	inline void set_m_HashCode_1(int32_t value)
	{
		___m_HashCode_1 = value;
	}

	inline static int32_t get_offset_of_m_OpeningDefinition_2() { return static_cast<int32_t>(offsetof(TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD, ___m_OpeningDefinition_2)); }
	inline String_t* get_m_OpeningDefinition_2() const { return ___m_OpeningDefinition_2; }
	inline String_t** get_address_of_m_OpeningDefinition_2() { return &___m_OpeningDefinition_2; }
	inline void set_m_OpeningDefinition_2(String_t* value)
	{
		___m_OpeningDefinition_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_OpeningDefinition_2), value);
	}

	inline static int32_t get_offset_of_m_ClosingDefinition_3() { return static_cast<int32_t>(offsetof(TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD, ___m_ClosingDefinition_3)); }
	inline String_t* get_m_ClosingDefinition_3() const { return ___m_ClosingDefinition_3; }
	inline String_t** get_address_of_m_ClosingDefinition_3() { return &___m_ClosingDefinition_3; }
	inline void set_m_ClosingDefinition_3(String_t* value)
	{
		___m_ClosingDefinition_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClosingDefinition_3), value);
	}

	inline static int32_t get_offset_of_m_OpeningTagArray_4() { return static_cast<int32_t>(offsetof(TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD, ___m_OpeningTagArray_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_OpeningTagArray_4() const { return ___m_OpeningTagArray_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_OpeningTagArray_4() { return &___m_OpeningTagArray_4; }
	inline void set_m_OpeningTagArray_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_OpeningTagArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_OpeningTagArray_4), value);
	}

	inline static int32_t get_offset_of_m_ClosingTagArray_5() { return static_cast<int32_t>(offsetof(TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD, ___m_ClosingTagArray_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_ClosingTagArray_5() const { return ___m_ClosingTagArray_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_ClosingTagArray_5() { return &___m_ClosingTagArray_5; }
	inline void set_m_ClosingTagArray_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_ClosingTagArray_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClosingTagArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_STYLE_T9FD01084B9E3F1D4B92E87114C454C98BA20FBAD_H
#ifndef TMP_TEXTELEMENT_LEGACY_T020BAF673D3D29BC2682AEA5717411BFB13C6D05_H
#define TMP_TEXTELEMENT_LEGACY_T020BAF673D3D29BC2682AEA5717411BFB13C6D05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement_Legacy
struct  TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_TextElement_Legacy::id
	int32_t ___id_0;
	// System.Single TMPro.TMP_TextElement_Legacy::x
	float ___x_1;
	// System.Single TMPro.TMP_TextElement_Legacy::y
	float ___y_2;
	// System.Single TMPro.TMP_TextElement_Legacy::width
	float ___width_3;
	// System.Single TMPro.TMP_TextElement_Legacy::height
	float ___height_4;
	// System.Single TMPro.TMP_TextElement_Legacy::xOffset
	float ___xOffset_5;
	// System.Single TMPro.TMP_TextElement_Legacy::yOffset
	float ___yOffset_6;
	// System.Single TMPro.TMP_TextElement_Legacy::xAdvance
	float ___xAdvance_7;
	// System.Single TMPro.TMP_TextElement_Legacy::scale
	float ___scale_8;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___width_3)); }
	inline float get_width_3() const { return ___width_3; }
	inline float* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(float value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___height_4)); }
	inline float get_height_4() const { return ___height_4; }
	inline float* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(float value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_xOffset_5() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___xOffset_5)); }
	inline float get_xOffset_5() const { return ___xOffset_5; }
	inline float* get_address_of_xOffset_5() { return &___xOffset_5; }
	inline void set_xOffset_5(float value)
	{
		___xOffset_5 = value;
	}

	inline static int32_t get_offset_of_yOffset_6() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___yOffset_6)); }
	inline float get_yOffset_6() const { return ___yOffset_6; }
	inline float* get_address_of_yOffset_6() { return &___yOffset_6; }
	inline void set_yOffset_6(float value)
	{
		___yOffset_6 = value;
	}

	inline static int32_t get_offset_of_xAdvance_7() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___xAdvance_7)); }
	inline float get_xAdvance_7() const { return ___xAdvance_7; }
	inline float* get_address_of_xAdvance_7() { return &___xAdvance_7; }
	inline void set_xAdvance_7(float value)
	{
		___xAdvance_7 = value;
	}

	inline static int32_t get_offset_of_scale_8() { return static_cast<int32_t>(offsetof(TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05, ___scale_8)); }
	inline float get_scale_8() const { return ___scale_8; }
	inline float* get_address_of_scale_8() { return &___scale_8; }
	inline void set_scale_8(float value)
	{
		___scale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_LEGACY_T020BAF673D3D29BC2682AEA5717411BFB13C6D05_H
#ifndef TMP_TEXTPARSINGUTILITIES_TA5D4616296766ECCFF80C5F3A800D7B92155AD35_H
#define TMP_TEXTPARSINGUTILITIES_TA5D4616296766ECCFF80C5F3A800D7B92155AD35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextParsingUtilities
struct  TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35  : public RuntimeObject
{
public:

public:
};

struct TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35_StaticFields
{
public:
	// TMPro.TMP_TextParsingUtilities TMPro.TMP_TextParsingUtilities::s_Instance
	TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35 * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35_StaticFields, ___s_Instance_0)); }
	inline TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35 * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35 ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35 * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTPARSINGUTILITIES_TA5D4616296766ECCFF80C5F3A800D7B92155AD35_H
#ifndef TMP_TEXTUTILITIES_T0C64120E363A3DA0CB859D321248294080076A45_H
#define TMP_TEXTUTILITIES_T0C64120E363A3DA0CB859D321248294080076A45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities
struct  TMP_TextUtilities_t0C64120E363A3DA0CB859D321248294080076A45  : public RuntimeObject
{
public:

public:
};

struct TMP_TextUtilities_t0C64120E363A3DA0CB859D321248294080076A45_StaticFields
{
public:
	// UnityEngine.Vector3[] TMPro.TMP_TextUtilities::m_rectWorldCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_rectWorldCorners_0;

public:
	inline static int32_t get_offset_of_m_rectWorldCorners_0() { return static_cast<int32_t>(offsetof(TMP_TextUtilities_t0C64120E363A3DA0CB859D321248294080076A45_StaticFields, ___m_rectWorldCorners_0)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_rectWorldCorners_0() const { return ___m_rectWorldCorners_0; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_rectWorldCorners_0() { return &___m_rectWorldCorners_0; }
	inline void set_m_rectWorldCorners_0(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_rectWorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectWorldCorners_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTUTILITIES_T0C64120E363A3DA0CB859D321248294080076A45_H
#ifndef TMP_UPDATEMANAGER_T0982D5C74E5439571872EB41119F1FFFE74DEF4B_H
#define TMP_UPDATEMANAGER_T0982D5C74E5439571872EB41119F1FFFE74DEF4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateManager
struct  TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_LayoutRebuildQueue
	List_1_t1073F21F60C9CB285CF2F0951405E58C95C20F90 * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_LayoutQueueLookup
	Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_GraphicRebuildQueue
	List_1_t1073F21F60C9CB285CF2F0951405E58C95C20F90 * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_GraphicQueueLookup
	Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * ___m_GraphicQueueLookup_4;
	// System.Collections.Generic.List`1<TMPro.TMP_Text> TMPro.TMP_UpdateManager::m_InternalUpdateQueue
	List_1_t1073F21F60C9CB285CF2F0951405E58C95C20F90 * ___m_InternalUpdateQueue_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateManager::m_InternalUpdateLookup
	Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * ___m_InternalUpdateLookup_6;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B, ___m_LayoutRebuildQueue_1)); }
	inline List_1_t1073F21F60C9CB285CF2F0951405E58C95C20F90 * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_t1073F21F60C9CB285CF2F0951405E58C95C20F90 ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_t1073F21F60C9CB285CF2F0951405E58C95C20F90 * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B, ___m_GraphicRebuildQueue_3)); }
	inline List_1_t1073F21F60C9CB285CF2F0951405E58C95C20F90 * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_t1073F21F60C9CB285CF2F0951405E58C95C20F90 ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_t1073F21F60C9CB285CF2F0951405E58C95C20F90 * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}

	inline static int32_t get_offset_of_m_InternalUpdateQueue_5() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B, ___m_InternalUpdateQueue_5)); }
	inline List_1_t1073F21F60C9CB285CF2F0951405E58C95C20F90 * get_m_InternalUpdateQueue_5() const { return ___m_InternalUpdateQueue_5; }
	inline List_1_t1073F21F60C9CB285CF2F0951405E58C95C20F90 ** get_address_of_m_InternalUpdateQueue_5() { return &___m_InternalUpdateQueue_5; }
	inline void set_m_InternalUpdateQueue_5(List_1_t1073F21F60C9CB285CF2F0951405E58C95C20F90 * value)
	{
		___m_InternalUpdateQueue_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalUpdateQueue_5), value);
	}

	inline static int32_t get_offset_of_m_InternalUpdateLookup_6() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B, ___m_InternalUpdateLookup_6)); }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * get_m_InternalUpdateLookup_6() const { return ___m_InternalUpdateLookup_6; }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E ** get_address_of_m_InternalUpdateLookup_6() { return &___m_InternalUpdateLookup_6; }
	inline void set_m_InternalUpdateLookup_6(Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * value)
	{
		___m_InternalUpdateLookup_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalUpdateLookup_6), value);
	}
};

struct TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B_StaticFields
{
public:
	// TMPro.TMP_UpdateManager TMPro.TMP_UpdateManager::s_Instance
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEMANAGER_T0982D5C74E5439571872EB41119F1FFFE74DEF4B_H
#ifndef TMP_UPDATEREGISTRY_TB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_H
#define TMP_UPDATEREGISTRY_TB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_UpdateRegistry
struct  TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_LayoutRebuildQueue
	List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC * ___m_LayoutRebuildQueue_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_LayoutQueueLookup
	Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * ___m_LayoutQueueLookup_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement> TMPro.TMP_UpdateRegistry::m_GraphicRebuildQueue
	List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC * ___m_GraphicRebuildQueue_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_UpdateRegistry::m_GraphicQueueLookup
	Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * ___m_GraphicQueueLookup_4;

public:
	inline static int32_t get_offset_of_m_LayoutRebuildQueue_1() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC, ___m_LayoutRebuildQueue_1)); }
	inline List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC * get_m_LayoutRebuildQueue_1() const { return ___m_LayoutRebuildQueue_1; }
	inline List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC ** get_address_of_m_LayoutRebuildQueue_1() { return &___m_LayoutRebuildQueue_1; }
	inline void set_m_LayoutRebuildQueue_1(List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC * value)
	{
		___m_LayoutRebuildQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutRebuildQueue_1), value);
	}

	inline static int32_t get_offset_of_m_LayoutQueueLookup_2() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC, ___m_LayoutQueueLookup_2)); }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * get_m_LayoutQueueLookup_2() const { return ___m_LayoutQueueLookup_2; }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E ** get_address_of_m_LayoutQueueLookup_2() { return &___m_LayoutQueueLookup_2; }
	inline void set_m_LayoutQueueLookup_2(Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * value)
	{
		___m_LayoutQueueLookup_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutQueueLookup_2), value);
	}

	inline static int32_t get_offset_of_m_GraphicRebuildQueue_3() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC, ___m_GraphicRebuildQueue_3)); }
	inline List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC * get_m_GraphicRebuildQueue_3() const { return ___m_GraphicRebuildQueue_3; }
	inline List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC ** get_address_of_m_GraphicRebuildQueue_3() { return &___m_GraphicRebuildQueue_3; }
	inline void set_m_GraphicRebuildQueue_3(List_1_tDD74EF2F2FE045C18B4D47301A0F9B3340A7A6BC * value)
	{
		___m_GraphicRebuildQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicRebuildQueue_3), value);
	}

	inline static int32_t get_offset_of_m_GraphicQueueLookup_4() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC, ___m_GraphicQueueLookup_4)); }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * get_m_GraphicQueueLookup_4() const { return ___m_GraphicQueueLookup_4; }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E ** get_address_of_m_GraphicQueueLookup_4() { return &___m_GraphicQueueLookup_4; }
	inline void set_m_GraphicQueueLookup_4(Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * value)
	{
		___m_GraphicQueueLookup_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_GraphicQueueLookup_4), value);
	}
};

struct TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_StaticFields
{
public:
	// TMPro.TMP_UpdateRegistry TMPro.TMP_UpdateRegistry::s_Instance
	TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC * ___s_Instance_0;

public:
	inline static int32_t get_offset_of_s_Instance_0() { return static_cast<int32_t>(offsetof(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_StaticFields, ___s_Instance_0)); }
	inline TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC * get_s_Instance_0() const { return ___s_Instance_0; }
	inline TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC ** get_address_of_s_Instance_0() { return &___s_Instance_0; }
	inline void set_s_Instance_0(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC * value)
	{
		___s_Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_UPDATEREGISTRY_TB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_H
#ifndef TMPRO_EVENTMANAGER_T0831C7F02A59F06755AFFF94AAF826EEE77E516D_H
#define TMPRO_EVENTMANAGER_T0831C7F02A59F06755AFFF94AAF826EEE77E516D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_EventManager
struct  TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D  : public RuntimeObject
{
public:

public:
};

struct TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields
{
public:
	// TMPro.FastAction`2<System.Object,TMPro.Compute_DT_EventArgs> TMPro.TMPro_EventManager::COMPUTE_DT_EVENT
	FastAction_2_t470741C1212B744F12DEA1C792E25238328F2763 * ___COMPUTE_DT_EVENT_0;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Material> TMPro.TMPro_EventManager::MATERIAL_PROPERTY_EVENT
	FastAction_2_tCD2BB87052DC554C74E5BDB9067F719ADEB4924F * ___MATERIAL_PROPERTY_EVENT_1;
	// TMPro.FastAction`2<System.Boolean,TMPro.TMP_FontAsset> TMPro.TMPro_EventManager::FONT_PROPERTY_EVENT
	FastAction_2_t4B7909D9E4639D203432784287F6C947223EBE2F * ___FONT_PROPERTY_EVENT_2;
	// TMPro.FastAction`2<System.Boolean,UnityEngine.Object> TMPro.TMPro_EventManager::SPRITE_ASSET_PROPERTY_EVENT
	FastAction_2_tEE90F5B2E87756276EC3D85B1645A416171B8B4B * ___SPRITE_ASSET_PROPERTY_EVENT_3;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshPro> TMPro.TMPro_EventManager::TEXTMESHPRO_PROPERTY_EVENT
	FastAction_2_t5E47E91BEC2F90CCC3CF6559C3A7AACCF179088D * ___TEXTMESHPRO_PROPERTY_EVENT_4;
	// TMPro.FastAction`3<UnityEngine.GameObject,UnityEngine.Material,UnityEngine.Material> TMPro.TMPro_EventManager::DRAG_AND_DROP_MATERIAL_EVENT
	FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4 * ___DRAG_AND_DROP_MATERIAL_EVENT_5;
	// TMPro.FastAction`1<System.Boolean> TMPro.TMPro_EventManager::TEXT_STYLE_PROPERTY_EVENT
	FastAction_1_tCCD240F66F98E2B0797C8E1D4A3A4292A1EA025D * ___TEXT_STYLE_PROPERTY_EVENT_6;
	// TMPro.FastAction`1<TMPro.TMP_ColorGradient> TMPro.TMPro_EventManager::COLOR_GRADIENT_PROPERTY_EVENT
	FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9 * ___COLOR_GRADIENT_PROPERTY_EVENT_7;
	// TMPro.FastAction TMPro.TMPro_EventManager::TMP_SETTINGS_PROPERTY_EVENT
	FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * ___TMP_SETTINGS_PROPERTY_EVENT_8;
	// TMPro.FastAction TMPro.TMPro_EventManager::RESOURCE_LOAD_EVENT
	FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * ___RESOURCE_LOAD_EVENT_9;
	// TMPro.FastAction`2<System.Boolean,TMPro.TextMeshProUGUI> TMPro.TMPro_EventManager::TEXTMESHPRO_UGUI_PROPERTY_EVENT
	FastAction_2_t62452E49E55558D8C779CF5550B547347EDE0BFD * ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10;
	// TMPro.FastAction TMPro.TMPro_EventManager::OnPreRenderObject_Event
	FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * ___OnPreRenderObject_Event_11;
	// TMPro.FastAction`1<UnityEngine.Object> TMPro.TMPro_EventManager::TEXT_CHANGED_EVENT
	FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09 * ___TEXT_CHANGED_EVENT_12;

public:
	inline static int32_t get_offset_of_COMPUTE_DT_EVENT_0() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___COMPUTE_DT_EVENT_0)); }
	inline FastAction_2_t470741C1212B744F12DEA1C792E25238328F2763 * get_COMPUTE_DT_EVENT_0() const { return ___COMPUTE_DT_EVENT_0; }
	inline FastAction_2_t470741C1212B744F12DEA1C792E25238328F2763 ** get_address_of_COMPUTE_DT_EVENT_0() { return &___COMPUTE_DT_EVENT_0; }
	inline void set_COMPUTE_DT_EVENT_0(FastAction_2_t470741C1212B744F12DEA1C792E25238328F2763 * value)
	{
		___COMPUTE_DT_EVENT_0 = value;
		Il2CppCodeGenWriteBarrier((&___COMPUTE_DT_EVENT_0), value);
	}

	inline static int32_t get_offset_of_MATERIAL_PROPERTY_EVENT_1() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___MATERIAL_PROPERTY_EVENT_1)); }
	inline FastAction_2_tCD2BB87052DC554C74E5BDB9067F719ADEB4924F * get_MATERIAL_PROPERTY_EVENT_1() const { return ___MATERIAL_PROPERTY_EVENT_1; }
	inline FastAction_2_tCD2BB87052DC554C74E5BDB9067F719ADEB4924F ** get_address_of_MATERIAL_PROPERTY_EVENT_1() { return &___MATERIAL_PROPERTY_EVENT_1; }
	inline void set_MATERIAL_PROPERTY_EVENT_1(FastAction_2_tCD2BB87052DC554C74E5BDB9067F719ADEB4924F * value)
	{
		___MATERIAL_PROPERTY_EVENT_1 = value;
		Il2CppCodeGenWriteBarrier((&___MATERIAL_PROPERTY_EVENT_1), value);
	}

	inline static int32_t get_offset_of_FONT_PROPERTY_EVENT_2() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___FONT_PROPERTY_EVENT_2)); }
	inline FastAction_2_t4B7909D9E4639D203432784287F6C947223EBE2F * get_FONT_PROPERTY_EVENT_2() const { return ___FONT_PROPERTY_EVENT_2; }
	inline FastAction_2_t4B7909D9E4639D203432784287F6C947223EBE2F ** get_address_of_FONT_PROPERTY_EVENT_2() { return &___FONT_PROPERTY_EVENT_2; }
	inline void set_FONT_PROPERTY_EVENT_2(FastAction_2_t4B7909D9E4639D203432784287F6C947223EBE2F * value)
	{
		___FONT_PROPERTY_EVENT_2 = value;
		Il2CppCodeGenWriteBarrier((&___FONT_PROPERTY_EVENT_2), value);
	}

	inline static int32_t get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___SPRITE_ASSET_PROPERTY_EVENT_3)); }
	inline FastAction_2_tEE90F5B2E87756276EC3D85B1645A416171B8B4B * get_SPRITE_ASSET_PROPERTY_EVENT_3() const { return ___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline FastAction_2_tEE90F5B2E87756276EC3D85B1645A416171B8B4B ** get_address_of_SPRITE_ASSET_PROPERTY_EVENT_3() { return &___SPRITE_ASSET_PROPERTY_EVENT_3; }
	inline void set_SPRITE_ASSET_PROPERTY_EVENT_3(FastAction_2_tEE90F5B2E87756276EC3D85B1645A416171B8B4B * value)
	{
		___SPRITE_ASSET_PROPERTY_EVENT_3 = value;
		Il2CppCodeGenWriteBarrier((&___SPRITE_ASSET_PROPERTY_EVENT_3), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TEXTMESHPRO_PROPERTY_EVENT_4)); }
	inline FastAction_2_t5E47E91BEC2F90CCC3CF6559C3A7AACCF179088D * get_TEXTMESHPRO_PROPERTY_EVENT_4() const { return ___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline FastAction_2_t5E47E91BEC2F90CCC3CF6559C3A7AACCF179088D ** get_address_of_TEXTMESHPRO_PROPERTY_EVENT_4() { return &___TEXTMESHPRO_PROPERTY_EVENT_4; }
	inline void set_TEXTMESHPRO_PROPERTY_EVENT_4(FastAction_2_t5E47E91BEC2F90CCC3CF6559C3A7AACCF179088D * value)
	{
		___TEXTMESHPRO_PROPERTY_EVENT_4 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_PROPERTY_EVENT_4), value);
	}

	inline static int32_t get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___DRAG_AND_DROP_MATERIAL_EVENT_5)); }
	inline FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4 * get_DRAG_AND_DROP_MATERIAL_EVENT_5() const { return ___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4 ** get_address_of_DRAG_AND_DROP_MATERIAL_EVENT_5() { return &___DRAG_AND_DROP_MATERIAL_EVENT_5; }
	inline void set_DRAG_AND_DROP_MATERIAL_EVENT_5(FastAction_3_t2EB8ADF78F86E27B29B67AA5A8A75B91C797D3D4 * value)
	{
		___DRAG_AND_DROP_MATERIAL_EVENT_5 = value;
		Il2CppCodeGenWriteBarrier((&___DRAG_AND_DROP_MATERIAL_EVENT_5), value);
	}

	inline static int32_t get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TEXT_STYLE_PROPERTY_EVENT_6)); }
	inline FastAction_1_tCCD240F66F98E2B0797C8E1D4A3A4292A1EA025D * get_TEXT_STYLE_PROPERTY_EVENT_6() const { return ___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline FastAction_1_tCCD240F66F98E2B0797C8E1D4A3A4292A1EA025D ** get_address_of_TEXT_STYLE_PROPERTY_EVENT_6() { return &___TEXT_STYLE_PROPERTY_EVENT_6; }
	inline void set_TEXT_STYLE_PROPERTY_EVENT_6(FastAction_1_tCCD240F66F98E2B0797C8E1D4A3A4292A1EA025D * value)
	{
		___TEXT_STYLE_PROPERTY_EVENT_6 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_STYLE_PROPERTY_EVENT_6), value);
	}

	inline static int32_t get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___COLOR_GRADIENT_PROPERTY_EVENT_7)); }
	inline FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9 * get_COLOR_GRADIENT_PROPERTY_EVENT_7() const { return ___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9 ** get_address_of_COLOR_GRADIENT_PROPERTY_EVENT_7() { return &___COLOR_GRADIENT_PROPERTY_EVENT_7; }
	inline void set_COLOR_GRADIENT_PROPERTY_EVENT_7(FastAction_1_t1380158AFE3DBBC197AFE5E2C45A2B50E03B25F9 * value)
	{
		___COLOR_GRADIENT_PROPERTY_EVENT_7 = value;
		Il2CppCodeGenWriteBarrier((&___COLOR_GRADIENT_PROPERTY_EVENT_7), value);
	}

	inline static int32_t get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TMP_SETTINGS_PROPERTY_EVENT_8)); }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * get_TMP_SETTINGS_PROPERTY_EVENT_8() const { return ___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB ** get_address_of_TMP_SETTINGS_PROPERTY_EVENT_8() { return &___TMP_SETTINGS_PROPERTY_EVENT_8; }
	inline void set_TMP_SETTINGS_PROPERTY_EVENT_8(FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * value)
	{
		___TMP_SETTINGS_PROPERTY_EVENT_8 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_SETTINGS_PROPERTY_EVENT_8), value);
	}

	inline static int32_t get_offset_of_RESOURCE_LOAD_EVENT_9() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___RESOURCE_LOAD_EVENT_9)); }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * get_RESOURCE_LOAD_EVENT_9() const { return ___RESOURCE_LOAD_EVENT_9; }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB ** get_address_of_RESOURCE_LOAD_EVENT_9() { return &___RESOURCE_LOAD_EVENT_9; }
	inline void set_RESOURCE_LOAD_EVENT_9(FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * value)
	{
		___RESOURCE_LOAD_EVENT_9 = value;
		Il2CppCodeGenWriteBarrier((&___RESOURCE_LOAD_EVENT_9), value);
	}

	inline static int32_t get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10)); }
	inline FastAction_2_t62452E49E55558D8C779CF5550B547347EDE0BFD * get_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() const { return ___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10; }
	inline FastAction_2_t62452E49E55558D8C779CF5550B547347EDE0BFD ** get_address_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10() { return &___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10; }
	inline void set_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10(FastAction_2_t62452E49E55558D8C779CF5550B547347EDE0BFD * value)
	{
		___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10 = value;
		Il2CppCodeGenWriteBarrier((&___TEXTMESHPRO_UGUI_PROPERTY_EVENT_10), value);
	}

	inline static int32_t get_offset_of_OnPreRenderObject_Event_11() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___OnPreRenderObject_Event_11)); }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * get_OnPreRenderObject_Event_11() const { return ___OnPreRenderObject_Event_11; }
	inline FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB ** get_address_of_OnPreRenderObject_Event_11() { return &___OnPreRenderObject_Event_11; }
	inline void set_OnPreRenderObject_Event_11(FastAction_t270E4D4A1102DD4C6E4D0634A4CBBD052A5D91FB * value)
	{
		___OnPreRenderObject_Event_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnPreRenderObject_Event_11), value);
	}

	inline static int32_t get_offset_of_TEXT_CHANGED_EVENT_12() { return static_cast<int32_t>(offsetof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields, ___TEXT_CHANGED_EVENT_12)); }
	inline FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09 * get_TEXT_CHANGED_EVENT_12() const { return ___TEXT_CHANGED_EVENT_12; }
	inline FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09 ** get_address_of_TEXT_CHANGED_EVENT_12() { return &___TEXT_CHANGED_EVENT_12; }
	inline void set_TEXT_CHANGED_EVENT_12(FastAction_1_t4B3FBD9A28CE7B1C5AB15A8A9087AE92B7A28E09 * value)
	{
		___TEXT_CHANGED_EVENT_12 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_CHANGED_EVENT_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EVENTMANAGER_T0831C7F02A59F06755AFFF94AAF826EEE77E516D_H
#ifndef TMPRO_EXTENSIONMETHODS_T2A24D41EAD2F3568C5617941DE7558790B9B1835_H
#define TMPRO_EXTENSIONMETHODS_T2A24D41EAD2F3568C5617941DE7558790B9B1835_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMPro_ExtensionMethods
struct  TMPro_ExtensionMethods_t2A24D41EAD2F3568C5617941DE7558790B9B1835  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMPRO_EXTENSIONMETHODS_T2A24D41EAD2F3568C5617941DE7558790B9B1835_H
#ifndef __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#define __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12
struct  __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D12_T02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22_H
#ifndef __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#define __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40
struct  __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D__padding[40];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D40_TB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D_H
#ifndef ACTKBYTE16_TBA7BB660E30157B457EAF59F1313F241EA53C340_H
#define ACTKBYTE16_TBA7BB660E30157B457EAF59F1313F241EA53C340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ACTkByte16
struct  ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340 
{
public:
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b1
	uint8_t ___b1_0;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b2
	uint8_t ___b2_1;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b3
	uint8_t ___b3_2;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b4
	uint8_t ___b4_3;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b5
	uint8_t ___b5_4;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b6
	uint8_t ___b6_5;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b7
	uint8_t ___b7_6;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b8
	uint8_t ___b8_7;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b9
	uint8_t ___b9_8;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b10
	uint8_t ___b10_9;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b11
	uint8_t ___b11_10;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b12
	uint8_t ___b12_11;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b13
	uint8_t ___b13_12;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b14
	uint8_t ___b14_13;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b15
	uint8_t ___b15_14;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b16
	uint8_t ___b16_15;

public:
	inline static int32_t get_offset_of_b1_0() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b1_0)); }
	inline uint8_t get_b1_0() const { return ___b1_0; }
	inline uint8_t* get_address_of_b1_0() { return &___b1_0; }
	inline void set_b1_0(uint8_t value)
	{
		___b1_0 = value;
	}

	inline static int32_t get_offset_of_b2_1() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b2_1)); }
	inline uint8_t get_b2_1() const { return ___b2_1; }
	inline uint8_t* get_address_of_b2_1() { return &___b2_1; }
	inline void set_b2_1(uint8_t value)
	{
		___b2_1 = value;
	}

	inline static int32_t get_offset_of_b3_2() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b3_2)); }
	inline uint8_t get_b3_2() const { return ___b3_2; }
	inline uint8_t* get_address_of_b3_2() { return &___b3_2; }
	inline void set_b3_2(uint8_t value)
	{
		___b3_2 = value;
	}

	inline static int32_t get_offset_of_b4_3() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b4_3)); }
	inline uint8_t get_b4_3() const { return ___b4_3; }
	inline uint8_t* get_address_of_b4_3() { return &___b4_3; }
	inline void set_b4_3(uint8_t value)
	{
		___b4_3 = value;
	}

	inline static int32_t get_offset_of_b5_4() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b5_4)); }
	inline uint8_t get_b5_4() const { return ___b5_4; }
	inline uint8_t* get_address_of_b5_4() { return &___b5_4; }
	inline void set_b5_4(uint8_t value)
	{
		___b5_4 = value;
	}

	inline static int32_t get_offset_of_b6_5() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b6_5)); }
	inline uint8_t get_b6_5() const { return ___b6_5; }
	inline uint8_t* get_address_of_b6_5() { return &___b6_5; }
	inline void set_b6_5(uint8_t value)
	{
		___b6_5 = value;
	}

	inline static int32_t get_offset_of_b7_6() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b7_6)); }
	inline uint8_t get_b7_6() const { return ___b7_6; }
	inline uint8_t* get_address_of_b7_6() { return &___b7_6; }
	inline void set_b7_6(uint8_t value)
	{
		___b7_6 = value;
	}

	inline static int32_t get_offset_of_b8_7() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b8_7)); }
	inline uint8_t get_b8_7() const { return ___b8_7; }
	inline uint8_t* get_address_of_b8_7() { return &___b8_7; }
	inline void set_b8_7(uint8_t value)
	{
		___b8_7 = value;
	}

	inline static int32_t get_offset_of_b9_8() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b9_8)); }
	inline uint8_t get_b9_8() const { return ___b9_8; }
	inline uint8_t* get_address_of_b9_8() { return &___b9_8; }
	inline void set_b9_8(uint8_t value)
	{
		___b9_8 = value;
	}

	inline static int32_t get_offset_of_b10_9() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b10_9)); }
	inline uint8_t get_b10_9() const { return ___b10_9; }
	inline uint8_t* get_address_of_b10_9() { return &___b10_9; }
	inline void set_b10_9(uint8_t value)
	{
		___b10_9 = value;
	}

	inline static int32_t get_offset_of_b11_10() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b11_10)); }
	inline uint8_t get_b11_10() const { return ___b11_10; }
	inline uint8_t* get_address_of_b11_10() { return &___b11_10; }
	inline void set_b11_10(uint8_t value)
	{
		___b11_10 = value;
	}

	inline static int32_t get_offset_of_b12_11() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b12_11)); }
	inline uint8_t get_b12_11() const { return ___b12_11; }
	inline uint8_t* get_address_of_b12_11() { return &___b12_11; }
	inline void set_b12_11(uint8_t value)
	{
		___b12_11 = value;
	}

	inline static int32_t get_offset_of_b13_12() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b13_12)); }
	inline uint8_t get_b13_12() const { return ___b13_12; }
	inline uint8_t* get_address_of_b13_12() { return &___b13_12; }
	inline void set_b13_12(uint8_t value)
	{
		___b13_12 = value;
	}

	inline static int32_t get_offset_of_b14_13() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b14_13)); }
	inline uint8_t get_b14_13() const { return ___b14_13; }
	inline uint8_t* get_address_of_b14_13() { return &___b14_13; }
	inline void set_b14_13(uint8_t value)
	{
		___b14_13 = value;
	}

	inline static int32_t get_offset_of_b15_14() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b15_14)); }
	inline uint8_t get_b15_14() const { return ___b15_14; }
	inline uint8_t* get_address_of_b15_14() { return &___b15_14; }
	inline void set_b15_14(uint8_t value)
	{
		___b15_14 = value;
	}

	inline static int32_t get_offset_of_b16_15() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b16_15)); }
	inline uint8_t get_b16_15() const { return ___b16_15; }
	inline uint8_t* get_address_of_b16_15() { return &___b16_15; }
	inline void set_b16_15(uint8_t value)
	{
		___b16_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKBYTE16_TBA7BB660E30157B457EAF59F1313F241EA53C340_H
#ifndef ACTKBYTE4_T07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE_H
#define ACTKBYTE4_T07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ACTkByte4
struct  ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE 
{
public:
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b1
	uint8_t ___b1_0;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b2
	uint8_t ___b2_1;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b3
	uint8_t ___b3_2;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b4
	uint8_t ___b4_3;

public:
	inline static int32_t get_offset_of_b1_0() { return static_cast<int32_t>(offsetof(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE, ___b1_0)); }
	inline uint8_t get_b1_0() const { return ___b1_0; }
	inline uint8_t* get_address_of_b1_0() { return &___b1_0; }
	inline void set_b1_0(uint8_t value)
	{
		___b1_0 = value;
	}

	inline static int32_t get_offset_of_b2_1() { return static_cast<int32_t>(offsetof(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE, ___b2_1)); }
	inline uint8_t get_b2_1() const { return ___b2_1; }
	inline uint8_t* get_address_of_b2_1() { return &___b2_1; }
	inline void set_b2_1(uint8_t value)
	{
		___b2_1 = value;
	}

	inline static int32_t get_offset_of_b3_2() { return static_cast<int32_t>(offsetof(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE, ___b3_2)); }
	inline uint8_t get_b3_2() const { return ___b3_2; }
	inline uint8_t* get_address_of_b3_2() { return &___b3_2; }
	inline void set_b3_2(uint8_t value)
	{
		___b3_2 = value;
	}

	inline static int32_t get_offset_of_b4_3() { return static_cast<int32_t>(offsetof(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE, ___b4_3)); }
	inline uint8_t get_b4_3() const { return ___b4_3; }
	inline uint8_t* get_address_of_b4_3() { return &___b4_3; }
	inline void set_b4_3(uint8_t value)
	{
		___b4_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKBYTE4_T07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE_H
#ifndef ACTKBYTE8_T833E13ECB9BCA90642862B8F95D336AE37DEA24E_H
#define ACTKBYTE8_T833E13ECB9BCA90642862B8F95D336AE37DEA24E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ACTkByte8
struct  ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E 
{
public:
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b1
	uint8_t ___b1_0;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b2
	uint8_t ___b2_1;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b3
	uint8_t ___b3_2;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b4
	uint8_t ___b4_3;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b5
	uint8_t ___b5_4;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b6
	uint8_t ___b6_5;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b7
	uint8_t ___b7_6;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b8
	uint8_t ___b8_7;

public:
	inline static int32_t get_offset_of_b1_0() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b1_0)); }
	inline uint8_t get_b1_0() const { return ___b1_0; }
	inline uint8_t* get_address_of_b1_0() { return &___b1_0; }
	inline void set_b1_0(uint8_t value)
	{
		___b1_0 = value;
	}

	inline static int32_t get_offset_of_b2_1() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b2_1)); }
	inline uint8_t get_b2_1() const { return ___b2_1; }
	inline uint8_t* get_address_of_b2_1() { return &___b2_1; }
	inline void set_b2_1(uint8_t value)
	{
		___b2_1 = value;
	}

	inline static int32_t get_offset_of_b3_2() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b3_2)); }
	inline uint8_t get_b3_2() const { return ___b3_2; }
	inline uint8_t* get_address_of_b3_2() { return &___b3_2; }
	inline void set_b3_2(uint8_t value)
	{
		___b3_2 = value;
	}

	inline static int32_t get_offset_of_b4_3() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b4_3)); }
	inline uint8_t get_b4_3() const { return ___b4_3; }
	inline uint8_t* get_address_of_b4_3() { return &___b4_3; }
	inline void set_b4_3(uint8_t value)
	{
		___b4_3 = value;
	}

	inline static int32_t get_offset_of_b5_4() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b5_4)); }
	inline uint8_t get_b5_4() const { return ___b5_4; }
	inline uint8_t* get_address_of_b5_4() { return &___b5_4; }
	inline void set_b5_4(uint8_t value)
	{
		___b5_4 = value;
	}

	inline static int32_t get_offset_of_b6_5() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b6_5)); }
	inline uint8_t get_b6_5() const { return ___b6_5; }
	inline uint8_t* get_address_of_b6_5() { return &___b6_5; }
	inline void set_b6_5(uint8_t value)
	{
		___b6_5 = value;
	}

	inline static int32_t get_offset_of_b7_6() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b7_6)); }
	inline uint8_t get_b7_6() const { return ___b7_6; }
	inline uint8_t* get_address_of_b7_6() { return &___b7_6; }
	inline void set_b7_6(uint8_t value)
	{
		___b7_6 = value;
	}

	inline static int32_t get_offset_of_b8_7() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b8_7)); }
	inline uint8_t get_b8_7() const { return ___b8_7; }
	inline uint8_t* get_address_of_b8_7() { return &___b8_7; }
	inline void set_b8_7(uint8_t value)
	{
		___b8_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKBYTE8_T833E13ECB9BCA90642862B8F95D336AE37DEA24E_H
#ifndef OBSCUREDBOOL_T2CC3647BFB7D4EE9035407A3B3579A429F018452_H
#define OBSCUREDBOOL_T2CC3647BFB7D4EE9035407A3B3579A429F018452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredBool
struct  ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452 
{
public:
	// System.Byte CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::currentCryptoKey
	uint8_t ___currentCryptoKey_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::hiddenValue
	int32_t ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::inited
	bool ___inited_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::fakeValue
	bool ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredBool::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452, ___currentCryptoKey_0)); }
	inline uint8_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline uint8_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(uint8_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452, ___hiddenValue_1)); }
	inline int32_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline int32_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(int32_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452, ___fakeValue_3)); }
	inline bool get_fakeValue_3() const { return ___fakeValue_3; }
	inline bool* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(bool value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredBool
struct ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452_marshaled_pinvoke
{
	uint8_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	int32_t ___inited_2;
	int32_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredBool
struct ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452_marshaled_com
{
	uint8_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	int32_t ___inited_2;
	int32_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDBOOL_T2CC3647BFB7D4EE9035407A3B3579A429F018452_H
#ifndef OBSCUREDBYTE_TAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB_H
#define OBSCUREDBYTE_TAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredByte
struct  ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB 
{
public:
	// System.Byte CodeStage.AntiCheat.ObscuredTypes.ObscuredByte::currentCryptoKey
	uint8_t ___currentCryptoKey_0;
	// System.Byte CodeStage.AntiCheat.ObscuredTypes.ObscuredByte::hiddenValue
	uint8_t ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredByte::inited
	bool ___inited_2;
	// System.Byte CodeStage.AntiCheat.ObscuredTypes.ObscuredByte::fakeValue
	uint8_t ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredByte::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB, ___currentCryptoKey_0)); }
	inline uint8_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline uint8_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(uint8_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB, ___hiddenValue_1)); }
	inline uint8_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline uint8_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(uint8_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB, ___fakeValue_3)); }
	inline uint8_t get_fakeValue_3() const { return ___fakeValue_3; }
	inline uint8_t* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(uint8_t value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredByte
struct ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB_marshaled_pinvoke
{
	uint8_t ___currentCryptoKey_0;
	uint8_t ___hiddenValue_1;
	int32_t ___inited_2;
	uint8_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredByte
struct ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB_marshaled_com
{
	uint8_t ___currentCryptoKey_0;
	uint8_t ___hiddenValue_1;
	int32_t ___inited_2;
	uint8_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDBYTE_TAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB_H
#ifndef OBSCUREDCHAR_T2A92A080202CC0C24D08B780B2F39CED1A3BA266_H
#define OBSCUREDCHAR_T2A92A080202CC0C24D08B780B2F39CED1A3BA266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredChar
struct  ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266 
{
public:
	// System.Char CodeStage.AntiCheat.ObscuredTypes.ObscuredChar::currentCryptoKey
	Il2CppChar ___currentCryptoKey_0;
	// System.Char CodeStage.AntiCheat.ObscuredTypes.ObscuredChar::hiddenValue
	Il2CppChar ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredChar::inited
	bool ___inited_2;
	// System.Char CodeStage.AntiCheat.ObscuredTypes.ObscuredChar::fakeValue
	Il2CppChar ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredChar::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266, ___currentCryptoKey_0)); }
	inline Il2CppChar get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline Il2CppChar* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(Il2CppChar value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266, ___hiddenValue_1)); }
	inline Il2CppChar get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline Il2CppChar* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(Il2CppChar value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266, ___fakeValue_3)); }
	inline Il2CppChar get_fakeValue_3() const { return ___fakeValue_3; }
	inline Il2CppChar* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(Il2CppChar value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredChar
struct ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266_marshaled_pinvoke
{
	uint8_t ___currentCryptoKey_0;
	uint8_t ___hiddenValue_1;
	int32_t ___inited_2;
	uint8_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredChar
struct ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266_marshaled_com
{
	uint8_t ___currentCryptoKey_0;
	uint8_t ___hiddenValue_1;
	int32_t ___inited_2;
	uint8_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDCHAR_T2A92A080202CC0C24D08B780B2F39CED1A3BA266_H
#ifndef OBSCUREDINT_TBF7AD58D75DF4170E88029FF34F72A59951A2489_H
#define OBSCUREDINT_TBF7AD58D75DF4170E88029FF34F72A59951A2489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredInt
struct  ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::currentCryptoKey
	int32_t ___currentCryptoKey_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::hiddenValue
	int32_t ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::inited
	bool ___inited_2;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::fakeValue
	int32_t ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredInt::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___currentCryptoKey_0)); }
	inline int32_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline int32_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(int32_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___hiddenValue_1)); }
	inline int32_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline int32_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(int32_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___fakeValue_3)); }
	inline int32_t get_fakeValue_3() const { return ___fakeValue_3; }
	inline int32_t* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(int32_t value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredInt
struct ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	int32_t ___inited_2;
	int32_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredInt
struct ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489_marshaled_com
{
	int32_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	int32_t ___inited_2;
	int32_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDINT_TBF7AD58D75DF4170E88029FF34F72A59951A2489_H
#ifndef OBSCUREDLONG_T3A2E6B6B36061DD41638EAFD4FE975FC66CCE609_H
#define OBSCUREDLONG_T3A2E6B6B36061DD41638EAFD4FE975FC66CCE609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredLong
struct  ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609 
{
public:
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredLong::currentCryptoKey
	int64_t ___currentCryptoKey_0;
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredLong::hiddenValue
	int64_t ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredLong::inited
	bool ___inited_2;
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredLong::fakeValue
	int64_t ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredLong::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609, ___currentCryptoKey_0)); }
	inline int64_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline int64_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(int64_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609, ___hiddenValue_1)); }
	inline int64_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline int64_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(int64_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609, ___fakeValue_3)); }
	inline int64_t get_fakeValue_3() const { return ___fakeValue_3; }
	inline int64_t* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(int64_t value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredLong
struct ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609_marshaled_pinvoke
{
	int64_t ___currentCryptoKey_0;
	int64_t ___hiddenValue_1;
	int32_t ___inited_2;
	int64_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredLong
struct ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609_marshaled_com
{
	int64_t ___currentCryptoKey_0;
	int64_t ___hiddenValue_1;
	int32_t ___inited_2;
	int64_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDLONG_T3A2E6B6B36061DD41638EAFD4FE975FC66CCE609_H
#ifndef RAWENCRYPTEDQUATERNION_T51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7_H
#define RAWENCRYPTEDQUATERNION_T51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion_RawEncryptedQuaternion
struct  RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion_RawEncryptedQuaternion::x
	int32_t ___x_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion_RawEncryptedQuaternion::y
	int32_t ___y_1;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion_RawEncryptedQuaternion::z
	int32_t ___z_2;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion_RawEncryptedQuaternion::w
	int32_t ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7, ___w_3)); }
	inline int32_t get_w_3() const { return ___w_3; }
	inline int32_t* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(int32_t value)
	{
		___w_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWENCRYPTEDQUATERNION_T51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7_H
#ifndef OBSCUREDSBYTE_TBFFB3C503276E226BA65C81689A69D3AEBC803A5_H
#define OBSCUREDSBYTE_TBFFB3C503276E226BA65C81689A69D3AEBC803A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte
struct  ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5 
{
public:
	// System.SByte CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte::currentCryptoKey
	int8_t ___currentCryptoKey_0;
	// System.SByte CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte::hiddenValue
	int8_t ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte::inited
	bool ___inited_2;
	// System.SByte CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte::fakeValue
	int8_t ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5, ___currentCryptoKey_0)); }
	inline int8_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline int8_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(int8_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5, ___hiddenValue_1)); }
	inline int8_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline int8_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(int8_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5, ___fakeValue_3)); }
	inline int8_t get_fakeValue_3() const { return ___fakeValue_3; }
	inline int8_t* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(int8_t value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte
struct ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5_marshaled_pinvoke
{
	int8_t ___currentCryptoKey_0;
	int8_t ___hiddenValue_1;
	int32_t ___inited_2;
	int8_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredSByte
struct ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5_marshaled_com
{
	int8_t ___currentCryptoKey_0;
	int8_t ___hiddenValue_1;
	int32_t ___inited_2;
	int8_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDSBYTE_TBFFB3C503276E226BA65C81689A69D3AEBC803A5_H
#ifndef OBSCUREDSHORT_T3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F_H
#define OBSCUREDSHORT_T3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredShort
struct  ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F 
{
public:
	// System.Int16 CodeStage.AntiCheat.ObscuredTypes.ObscuredShort::currentCryptoKey
	int16_t ___currentCryptoKey_0;
	// System.Int16 CodeStage.AntiCheat.ObscuredTypes.ObscuredShort::hiddenValue
	int16_t ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredShort::inited
	bool ___inited_2;
	// System.Int16 CodeStage.AntiCheat.ObscuredTypes.ObscuredShort::fakeValue
	int16_t ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredShort::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F, ___currentCryptoKey_0)); }
	inline int16_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline int16_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(int16_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F, ___hiddenValue_1)); }
	inline int16_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline int16_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(int16_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F, ___fakeValue_3)); }
	inline int16_t get_fakeValue_3() const { return ___fakeValue_3; }
	inline int16_t* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(int16_t value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredShort
struct ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F_marshaled_pinvoke
{
	int16_t ___currentCryptoKey_0;
	int16_t ___hiddenValue_1;
	int32_t ___inited_2;
	int16_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredShort
struct ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F_marshaled_com
{
	int16_t ___currentCryptoKey_0;
	int16_t ___hiddenValue_1;
	int32_t ___inited_2;
	int16_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDSHORT_T3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F_H
#ifndef OBSCUREDUINT_T473890BB340A9E2E57D22A4B0599FA3104D3B473_H
#define OBSCUREDUINT_T473890BB340A9E2E57D22A4B0599FA3104D3B473_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredUInt
struct  ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473 
{
public:
	// System.UInt32 CodeStage.AntiCheat.ObscuredTypes.ObscuredUInt::currentCryptoKey
	uint32_t ___currentCryptoKey_0;
	// System.UInt32 CodeStage.AntiCheat.ObscuredTypes.ObscuredUInt::hiddenValue
	uint32_t ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredUInt::inited
	bool ___inited_2;
	// System.UInt32 CodeStage.AntiCheat.ObscuredTypes.ObscuredUInt::fakeValue
	uint32_t ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredUInt::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473, ___currentCryptoKey_0)); }
	inline uint32_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline uint32_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(uint32_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473, ___hiddenValue_1)); }
	inline uint32_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline uint32_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(uint32_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473, ___fakeValue_3)); }
	inline uint32_t get_fakeValue_3() const { return ___fakeValue_3; }
	inline uint32_t* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(uint32_t value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredUInt
struct ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473_marshaled_pinvoke
{
	uint32_t ___currentCryptoKey_0;
	uint32_t ___hiddenValue_1;
	int32_t ___inited_2;
	uint32_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredUInt
struct ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473_marshaled_com
{
	uint32_t ___currentCryptoKey_0;
	uint32_t ___hiddenValue_1;
	int32_t ___inited_2;
	uint32_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDUINT_T473890BB340A9E2E57D22A4B0599FA3104D3B473_H
#ifndef OBSCUREDULONG_T690D7C9B998CA88E496894395B7CBAB00B652ECB_H
#define OBSCUREDULONG_T690D7C9B998CA88E496894395B7CBAB00B652ECB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredULong
struct  ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB 
{
public:
	// System.UInt64 CodeStage.AntiCheat.ObscuredTypes.ObscuredULong::currentCryptoKey
	uint64_t ___currentCryptoKey_0;
	// System.UInt64 CodeStage.AntiCheat.ObscuredTypes.ObscuredULong::hiddenValue
	uint64_t ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredULong::inited
	bool ___inited_2;
	// System.UInt64 CodeStage.AntiCheat.ObscuredTypes.ObscuredULong::fakeValue
	uint64_t ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredULong::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB, ___currentCryptoKey_0)); }
	inline uint64_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline uint64_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(uint64_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB, ___hiddenValue_1)); }
	inline uint64_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline uint64_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(uint64_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB, ___fakeValue_3)); }
	inline uint64_t get_fakeValue_3() const { return ___fakeValue_3; }
	inline uint64_t* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(uint64_t value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredULong
struct ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB_marshaled_pinvoke
{
	uint64_t ___currentCryptoKey_0;
	uint64_t ___hiddenValue_1;
	int32_t ___inited_2;
	uint64_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredULong
struct ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB_marshaled_com
{
	uint64_t ___currentCryptoKey_0;
	uint64_t ___hiddenValue_1;
	int32_t ___inited_2;
	uint64_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDULONG_T690D7C9B998CA88E496894395B7CBAB00B652ECB_H
#ifndef OBSCUREDUSHORT_T0DC9F5CCECED350EFE386F1AF70ACE885C1333DF_H
#define OBSCUREDUSHORT_T0DC9F5CCECED350EFE386F1AF70ACE885C1333DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredUShort
struct  ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF 
{
public:
	// System.UInt16 CodeStage.AntiCheat.ObscuredTypes.ObscuredUShort::currentCryptoKey
	uint16_t ___currentCryptoKey_0;
	// System.UInt16 CodeStage.AntiCheat.ObscuredTypes.ObscuredUShort::hiddenValue
	uint16_t ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredUShort::inited
	bool ___inited_2;
	// System.UInt16 CodeStage.AntiCheat.ObscuredTypes.ObscuredUShort::fakeValue
	uint16_t ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredUShort::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF, ___currentCryptoKey_0)); }
	inline uint16_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline uint16_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(uint16_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF, ___hiddenValue_1)); }
	inline uint16_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline uint16_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(uint16_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF, ___fakeValue_3)); }
	inline uint16_t get_fakeValue_3() const { return ___fakeValue_3; }
	inline uint16_t* get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(uint16_t value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredUShort
struct ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF_marshaled_pinvoke
{
	uint16_t ___currentCryptoKey_0;
	uint16_t ___hiddenValue_1;
	int32_t ___inited_2;
	uint16_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredUShort
struct ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF_marshaled_com
{
	uint16_t ___currentCryptoKey_0;
	uint16_t ___hiddenValue_1;
	int32_t ___inited_2;
	uint16_t ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDUSHORT_T0DC9F5CCECED350EFE386F1AF70ACE885C1333DF_H
#ifndef RAWENCRYPTEDVECTOR2_TFF1E132A3237D22895804BE30F9011B94E9BF5CC_H
#define RAWENCRYPTEDVECTOR2_TFF1E132A3237D22895804BE30F9011B94E9BF5CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2_RawEncryptedVector2
struct  RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2_RawEncryptedVector2::x
	int32_t ___x_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2_RawEncryptedVector2::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWENCRYPTEDVECTOR2_TFF1E132A3237D22895804BE30F9011B94E9BF5CC_H
#ifndef RAWENCRYPTEDVECTOR2INT_T7355329D63C8B123505CCAFD38B6F28BD9093F7D_H
#define RAWENCRYPTEDVECTOR2INT_T7355329D63C8B123505CCAFD38B6F28BD9093F7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int_RawEncryptedVector2Int
struct  RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int_RawEncryptedVector2Int::x
	int32_t ___x_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int_RawEncryptedVector2Int::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWENCRYPTEDVECTOR2INT_T7355329D63C8B123505CCAFD38B6F28BD9093F7D_H
#ifndef RAWENCRYPTEDVECTOR3_T766A78183852F7A89484E372DBFD15EEF76AB627_H
#define RAWENCRYPTEDVECTOR3_T766A78183852F7A89484E372DBFD15EEF76AB627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3
struct  RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3::x
	int32_t ___x_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3::y
	int32_t ___y_1;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3::z
	int32_t ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWENCRYPTEDVECTOR3_T766A78183852F7A89484E372DBFD15EEF76AB627_H
#ifndef DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#define DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Zero_7)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___One_8)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_One_8() const { return ___One_8; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinusOne_9)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MaxValue_10)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinValue_11)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#define MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaterialReference
struct  MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F 
{
public:
	// System.Int32 TMPro.MaterialReference::index
	int32_t ___index_0;
	// TMPro.TMP_FontAsset TMPro.MaterialReference::fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	// TMPro.TMP_SpriteAsset TMPro.MaterialReference::spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	// UnityEngine.Material TMPro.MaterialReference::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	// System.Boolean TMPro.MaterialReference::isDefaultMaterial
	bool ___isDefaultMaterial_4;
	// System.Boolean TMPro.MaterialReference::isFallbackMaterial
	bool ___isFallbackMaterial_5;
	// UnityEngine.Material TMPro.MaterialReference::fallbackMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	// System.Single TMPro.MaterialReference::padding
	float ___padding_7;
	// System.Int32 TMPro.MaterialReference::referenceCount
	int32_t ___referenceCount_8;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_fontAsset_1() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fontAsset_1)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_fontAsset_1() const { return ___fontAsset_1; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_fontAsset_1() { return &___fontAsset_1; }
	inline void set_fontAsset_1(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___fontAsset_1 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_1), value);
	}

	inline static int32_t get_offset_of_spriteAsset_2() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___spriteAsset_2)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_spriteAsset_2() const { return ___spriteAsset_2; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_spriteAsset_2() { return &___spriteAsset_2; }
	inline void set_spriteAsset_2(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_2), value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___material_3)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_3() const { return ___material_3; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_isDefaultMaterial_4() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isDefaultMaterial_4)); }
	inline bool get_isDefaultMaterial_4() const { return ___isDefaultMaterial_4; }
	inline bool* get_address_of_isDefaultMaterial_4() { return &___isDefaultMaterial_4; }
	inline void set_isDefaultMaterial_4(bool value)
	{
		___isDefaultMaterial_4 = value;
	}

	inline static int32_t get_offset_of_isFallbackMaterial_5() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___isFallbackMaterial_5)); }
	inline bool get_isFallbackMaterial_5() const { return ___isFallbackMaterial_5; }
	inline bool* get_address_of_isFallbackMaterial_5() { return &___isFallbackMaterial_5; }
	inline void set_isFallbackMaterial_5(bool value)
	{
		___isFallbackMaterial_5 = value;
	}

	inline static int32_t get_offset_of_fallbackMaterial_6() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___fallbackMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_fallbackMaterial_6() const { return ___fallbackMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_fallbackMaterial_6() { return &___fallbackMaterial_6; }
	inline void set_fallbackMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___fallbackMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackMaterial_6), value);
	}

	inline static int32_t get_offset_of_padding_7() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___padding_7)); }
	inline float get_padding_7() const { return ___padding_7; }
	inline float* get_address_of_padding_7() { return &___padding_7; }
	inline void set_padding_7(float value)
	{
		___padding_7 = value;
	}

	inline static int32_t get_offset_of_referenceCount_8() { return static_cast<int32_t>(offsetof(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F, ___referenceCount_8)); }
	inline int32_t get_referenceCount_8() const { return ___referenceCount_8; }
	inline int32_t* get_address_of_referenceCount_8() { return &___referenceCount_8; }
	inline void set_referenceCount_8(int32_t value)
	{
		___referenceCount_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_pinvoke
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
// Native definition for COM marshalling of TMPro.MaterialReference
struct MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_marshaled_com
{
	int32_t ___index_0;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_1;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_2;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_3;
	int32_t ___isDefaultMaterial_4;
	int32_t ___isFallbackMaterial_5;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___fallbackMaterial_6;
	float ___padding_7;
	int32_t ___referenceCount_8;
};
#endif // MATERIALREFERENCE_TFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F_H
#ifndef SPRITEFRAME_TDB681A7461FA0C10DA42E9A984BDDD0199AB2C04_H
#define SPRITEFRAME_TDB681A7461FA0C10DA42E9A984BDDD0199AB2C04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame
struct  SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::x
	float ___x_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::y
	float ___y_1;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::w
	float ___w_2;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame::h
	float ___h_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___w_2)); }
	inline float get_w_2() const { return ___w_2; }
	inline float* get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(float value)
	{
		___w_2 = value;
	}

	inline static int32_t get_offset_of_h_3() { return static_cast<int32_t>(offsetof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04, ___h_3)); }
	inline float get_h_3() const { return ___h_3; }
	inline float* get_address_of_h_3() { return &___h_3; }
	inline void set_h_3(float value)
	{
		___h_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEFRAME_TDB681A7461FA0C10DA42E9A984BDDD0199AB2C04_H
#ifndef SPRITESIZE_T143F23923B1D48E84CB38DCDD532F408936AB67E_H
#define SPRITESIZE_T143F23923B1D48E84CB38DCDD532F408936AB67E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize
struct  SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E 
{
public:
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize::w
	float ___w_0;
	// System.Single TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize::h
	float ___h_1;

public:
	inline static int32_t get_offset_of_w_0() { return static_cast<int32_t>(offsetof(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E, ___w_0)); }
	inline float get_w_0() const { return ___w_0; }
	inline float* get_address_of_w_0() { return &___w_0; }
	inline void set_w_0(float value)
	{
		___w_0 = value;
	}

	inline static int32_t get_offset_of_h_1() { return static_cast<int32_t>(offsetof(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E, ___h_1)); }
	inline float get_h_1() const { return ___h_1; }
	inline float* get_address_of_h_1() { return &___h_1; }
	inline void set_h_1(float value)
	{
		___h_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITESIZE_T143F23923B1D48E84CB38DCDD532F408936AB67E_H
#ifndef TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#define TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontStyleStack
struct  TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 
{
public:
	// System.Byte TMPro.TMP_FontStyleStack::bold
	uint8_t ___bold_0;
	// System.Byte TMPro.TMP_FontStyleStack::italic
	uint8_t ___italic_1;
	// System.Byte TMPro.TMP_FontStyleStack::underline
	uint8_t ___underline_2;
	// System.Byte TMPro.TMP_FontStyleStack::strikethrough
	uint8_t ___strikethrough_3;
	// System.Byte TMPro.TMP_FontStyleStack::highlight
	uint8_t ___highlight_4;
	// System.Byte TMPro.TMP_FontStyleStack::superscript
	uint8_t ___superscript_5;
	// System.Byte TMPro.TMP_FontStyleStack::subscript
	uint8_t ___subscript_6;
	// System.Byte TMPro.TMP_FontStyleStack::uppercase
	uint8_t ___uppercase_7;
	// System.Byte TMPro.TMP_FontStyleStack::lowercase
	uint8_t ___lowercase_8;
	// System.Byte TMPro.TMP_FontStyleStack::smallcaps
	uint8_t ___smallcaps_9;

public:
	inline static int32_t get_offset_of_bold_0() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___bold_0)); }
	inline uint8_t get_bold_0() const { return ___bold_0; }
	inline uint8_t* get_address_of_bold_0() { return &___bold_0; }
	inline void set_bold_0(uint8_t value)
	{
		___bold_0 = value;
	}

	inline static int32_t get_offset_of_italic_1() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___italic_1)); }
	inline uint8_t get_italic_1() const { return ___italic_1; }
	inline uint8_t* get_address_of_italic_1() { return &___italic_1; }
	inline void set_italic_1(uint8_t value)
	{
		___italic_1 = value;
	}

	inline static int32_t get_offset_of_underline_2() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___underline_2)); }
	inline uint8_t get_underline_2() const { return ___underline_2; }
	inline uint8_t* get_address_of_underline_2() { return &___underline_2; }
	inline void set_underline_2(uint8_t value)
	{
		___underline_2 = value;
	}

	inline static int32_t get_offset_of_strikethrough_3() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___strikethrough_3)); }
	inline uint8_t get_strikethrough_3() const { return ___strikethrough_3; }
	inline uint8_t* get_address_of_strikethrough_3() { return &___strikethrough_3; }
	inline void set_strikethrough_3(uint8_t value)
	{
		___strikethrough_3 = value;
	}

	inline static int32_t get_offset_of_highlight_4() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___highlight_4)); }
	inline uint8_t get_highlight_4() const { return ___highlight_4; }
	inline uint8_t* get_address_of_highlight_4() { return &___highlight_4; }
	inline void set_highlight_4(uint8_t value)
	{
		___highlight_4 = value;
	}

	inline static int32_t get_offset_of_superscript_5() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___superscript_5)); }
	inline uint8_t get_superscript_5() const { return ___superscript_5; }
	inline uint8_t* get_address_of_superscript_5() { return &___superscript_5; }
	inline void set_superscript_5(uint8_t value)
	{
		___superscript_5 = value;
	}

	inline static int32_t get_offset_of_subscript_6() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___subscript_6)); }
	inline uint8_t get_subscript_6() const { return ___subscript_6; }
	inline uint8_t* get_address_of_subscript_6() { return &___subscript_6; }
	inline void set_subscript_6(uint8_t value)
	{
		___subscript_6 = value;
	}

	inline static int32_t get_offset_of_uppercase_7() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___uppercase_7)); }
	inline uint8_t get_uppercase_7() const { return ___uppercase_7; }
	inline uint8_t* get_address_of_uppercase_7() { return &___uppercase_7; }
	inline void set_uppercase_7(uint8_t value)
	{
		___uppercase_7 = value;
	}

	inline static int32_t get_offset_of_lowercase_8() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___lowercase_8)); }
	inline uint8_t get_lowercase_8() const { return ___lowercase_8; }
	inline uint8_t* get_address_of_lowercase_8() { return &___lowercase_8; }
	inline void set_lowercase_8(uint8_t value)
	{
		___lowercase_8 = value;
	}

	inline static int32_t get_offset_of_smallcaps_9() { return static_cast<int32_t>(offsetof(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84, ___smallcaps_9)); }
	inline uint8_t get_smallcaps_9() const { return ___smallcaps_9; }
	inline uint8_t* get_address_of_smallcaps_9() { return &___smallcaps_9; }
	inline void set_smallcaps_9(uint8_t value)
	{
		___smallcaps_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_FONTSTYLESTACK_TC7146DA5AD4540B2C8733862D785AD50AD229E84_H
#ifndef TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#define TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LinkInfo
struct  TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468 
{
public:
	// TMPro.TMP_Text TMPro.TMP_LinkInfo::textComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	// System.Int32 TMPro.TMP_LinkInfo::hashCode
	int32_t ___hashCode_1;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdFirstCharacterIndex
	int32_t ___linkIdFirstCharacterIndex_2;
	// System.Int32 TMPro.TMP_LinkInfo::linkIdLength
	int32_t ___linkIdLength_3;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextfirstCharacterIndex
	int32_t ___linkTextfirstCharacterIndex_4;
	// System.Int32 TMPro.TMP_LinkInfo::linkTextLength
	int32_t ___linkTextLength_5;
	// System.Char[] TMPro.TMP_LinkInfo::linkID
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___linkID_6;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___textComponent_0)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_hashCode_1() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___hashCode_1)); }
	inline int32_t get_hashCode_1() const { return ___hashCode_1; }
	inline int32_t* get_address_of_hashCode_1() { return &___hashCode_1; }
	inline void set_hashCode_1(int32_t value)
	{
		___hashCode_1 = value;
	}

	inline static int32_t get_offset_of_linkIdFirstCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkIdFirstCharacterIndex_2)); }
	inline int32_t get_linkIdFirstCharacterIndex_2() const { return ___linkIdFirstCharacterIndex_2; }
	inline int32_t* get_address_of_linkIdFirstCharacterIndex_2() { return &___linkIdFirstCharacterIndex_2; }
	inline void set_linkIdFirstCharacterIndex_2(int32_t value)
	{
		___linkIdFirstCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_linkIdLength_3() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkIdLength_3)); }
	inline int32_t get_linkIdLength_3() const { return ___linkIdLength_3; }
	inline int32_t* get_address_of_linkIdLength_3() { return &___linkIdLength_3; }
	inline void set_linkIdLength_3(int32_t value)
	{
		___linkIdLength_3 = value;
	}

	inline static int32_t get_offset_of_linkTextfirstCharacterIndex_4() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkTextfirstCharacterIndex_4)); }
	inline int32_t get_linkTextfirstCharacterIndex_4() const { return ___linkTextfirstCharacterIndex_4; }
	inline int32_t* get_address_of_linkTextfirstCharacterIndex_4() { return &___linkTextfirstCharacterIndex_4; }
	inline void set_linkTextfirstCharacterIndex_4(int32_t value)
	{
		___linkTextfirstCharacterIndex_4 = value;
	}

	inline static int32_t get_offset_of_linkTextLength_5() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkTextLength_5)); }
	inline int32_t get_linkTextLength_5() const { return ___linkTextLength_5; }
	inline int32_t* get_address_of_linkTextLength_5() { return &___linkTextLength_5; }
	inline void set_linkTextLength_5(int32_t value)
	{
		___linkTextLength_5 = value;
	}

	inline static int32_t get_offset_of_linkID_6() { return static_cast<int32_t>(offsetof(TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468, ___linkID_6)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_linkID_6() const { return ___linkID_6; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_linkID_6() { return &___linkID_6; }
	inline void set_linkID_6(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___linkID_6 = value;
		Il2CppCodeGenWriteBarrier((&___linkID_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468_marshaled_pinvoke
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
// Native definition for COM marshalling of TMPro.TMP_LinkInfo
struct TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468_marshaled_com
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___hashCode_1;
	int32_t ___linkIdFirstCharacterIndex_2;
	int32_t ___linkIdLength_3;
	int32_t ___linkTextfirstCharacterIndex_4;
	int32_t ___linkTextLength_5;
	uint8_t* ___linkID_6;
};
#endif // TMP_LINKINFO_T7F4B699290A975144DF7094667825BCD52594468_H
#ifndef TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#define TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_PageInfo
struct  TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24 
{
public:
	// System.Int32 TMPro.TMP_PageInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_0;
	// System.Int32 TMPro.TMP_PageInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_1;
	// System.Single TMPro.TMP_PageInfo::ascender
	float ___ascender_2;
	// System.Single TMPro.TMP_PageInfo::baseLine
	float ___baseLine_3;
	// System.Single TMPro.TMP_PageInfo::descender
	float ___descender_4;

public:
	inline static int32_t get_offset_of_firstCharacterIndex_0() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___firstCharacterIndex_0)); }
	inline int32_t get_firstCharacterIndex_0() const { return ___firstCharacterIndex_0; }
	inline int32_t* get_address_of_firstCharacterIndex_0() { return &___firstCharacterIndex_0; }
	inline void set_firstCharacterIndex_0(int32_t value)
	{
		___firstCharacterIndex_0 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___lastCharacterIndex_1)); }
	inline int32_t get_lastCharacterIndex_1() const { return ___lastCharacterIndex_1; }
	inline int32_t* get_address_of_lastCharacterIndex_1() { return &___lastCharacterIndex_1; }
	inline void set_lastCharacterIndex_1(int32_t value)
	{
		___lastCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_ascender_2() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___ascender_2)); }
	inline float get_ascender_2() const { return ___ascender_2; }
	inline float* get_address_of_ascender_2() { return &___ascender_2; }
	inline void set_ascender_2(float value)
	{
		___ascender_2 = value;
	}

	inline static int32_t get_offset_of_baseLine_3() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___baseLine_3)); }
	inline float get_baseLine_3() const { return ___baseLine_3; }
	inline float* get_address_of_baseLine_3() { return &___baseLine_3; }
	inline void set_baseLine_3(float value)
	{
		___baseLine_3 = value;
	}

	inline static int32_t get_offset_of_descender_4() { return static_cast<int32_t>(offsetof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24, ___descender_4)); }
	inline float get_descender_4() const { return ___descender_4; }
	inline float* get_address_of_descender_4() { return &___descender_4; }
	inline void set_descender_4(float value)
	{
		___descender_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_PAGEINFO_T5D305B11116379997CA9649E8D87B3D7162ABB24_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T629E00E06021AA51A5AD8607BAFC61DB099F2D7F_H
#define TMP_RICHTEXTTAGSTACK_1_T629E00E06021AA51A5AD8607BAFC61DB099F2D7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<System.Int32>
struct  TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F, ___m_ItemStack_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T629E00E06021AA51A5AD8607BAFC61DB099F2D7F_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T221674BAE112F99AB4BDB4D127F20A021FF50CA3_H
#define TMP_RICHTEXTTAGSTACK_1_T221674BAE112F99AB4BDB4D127F20A021FF50CA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<System.Single>
struct  TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	float ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3, ___m_ItemStack_0)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3, ___m_DefaultItem_3)); }
	inline float get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline float* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(float value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T221674BAE112F99AB4BDB4D127F20A021FF50CA3_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#define TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient>
struct  TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_ItemStack_0)); }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(TMP_ColorGradientU5BU5D_t0948D618AC4240E6F0CFE0125BB6A4E931DE847C* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D, ___m_DefaultItem_3)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_DefaultItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultItem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D_H
#ifndef TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#define TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteInfo
struct  TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB 
{
public:
	// System.Int32 TMPro.TMP_SpriteInfo::spriteIndex
	int32_t ___spriteIndex_0;
	// System.Int32 TMPro.TMP_SpriteInfo::characterIndex
	int32_t ___characterIndex_1;
	// System.Int32 TMPro.TMP_SpriteInfo::vertexIndex
	int32_t ___vertexIndex_2;

public:
	inline static int32_t get_offset_of_spriteIndex_0() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___spriteIndex_0)); }
	inline int32_t get_spriteIndex_0() const { return ___spriteIndex_0; }
	inline int32_t* get_address_of_spriteIndex_0() { return &___spriteIndex_0; }
	inline void set_spriteIndex_0(int32_t value)
	{
		___spriteIndex_0 = value;
	}

	inline static int32_t get_offset_of_characterIndex_1() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___characterIndex_1)); }
	inline int32_t get_characterIndex_1() const { return ___characterIndex_1; }
	inline int32_t* get_address_of_characterIndex_1() { return &___characterIndex_1; }
	inline void set_characterIndex_1(int32_t value)
	{
		___characterIndex_1 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_2() { return static_cast<int32_t>(offsetof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB, ___vertexIndex_2)); }
	inline int32_t get_vertexIndex_2() const { return ___vertexIndex_2; }
	inline int32_t* get_address_of_vertexIndex_2() { return &___vertexIndex_2; }
	inline void set_vertexIndex_2(int32_t value)
	{
		___vertexIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEINFO_T55432612FE0D00F32826D0F817E8462F66CBABBB_H
#ifndef UNICODECHAR_T29383F22AA9A3AA4A2061312113FDF2887834F2A_H
#define UNICODECHAR_T29383F22AA9A3AA4A2061312113FDF2887834F2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text_UnicodeChar
struct  UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A 
{
public:
	// System.Int32 TMPro.TMP_Text_UnicodeChar::unicode
	int32_t ___unicode_0;
	// System.Int32 TMPro.TMP_Text_UnicodeChar::stringIndex
	int32_t ___stringIndex_1;
	// System.Int32 TMPro.TMP_Text_UnicodeChar::length
	int32_t ___length_2;

public:
	inline static int32_t get_offset_of_unicode_0() { return static_cast<int32_t>(offsetof(UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A, ___unicode_0)); }
	inline int32_t get_unicode_0() const { return ___unicode_0; }
	inline int32_t* get_address_of_unicode_0() { return &___unicode_0; }
	inline void set_unicode_0(int32_t value)
	{
		___unicode_0 = value;
	}

	inline static int32_t get_offset_of_stringIndex_1() { return static_cast<int32_t>(offsetof(UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A, ___stringIndex_1)); }
	inline int32_t get_stringIndex_1() const { return ___stringIndex_1; }
	inline int32_t* get_address_of_stringIndex_1() { return &___stringIndex_1; }
	inline void set_stringIndex_1(int32_t value)
	{
		___stringIndex_1 = value;
	}

	inline static int32_t get_offset_of_length_2() { return static_cast<int32_t>(offsetof(UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A, ___length_2)); }
	inline int32_t get_length_2() const { return ___length_2; }
	inline int32_t* get_address_of_length_2() { return &___length_2; }
	inline void set_length_2(int32_t value)
	{
		___length_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNICODECHAR_T29383F22AA9A3AA4A2061312113FDF2887834F2A_H
#ifndef TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#define TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_WordInfo
struct  TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90 
{
public:
	// TMPro.TMP_Text TMPro.TMP_WordInfo::textComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	// System.Int32 TMPro.TMP_WordInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_1;
	// System.Int32 TMPro.TMP_WordInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_2;
	// System.Int32 TMPro.TMP_WordInfo::characterCount
	int32_t ___characterCount_3;

public:
	inline static int32_t get_offset_of_textComponent_0() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___textComponent_0)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_textComponent_0() const { return ___textComponent_0; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_textComponent_0() { return &___textComponent_0; }
	inline void set_textComponent_0(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___textComponent_0 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_0), value);
	}

	inline static int32_t get_offset_of_firstCharacterIndex_1() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___firstCharacterIndex_1)); }
	inline int32_t get_firstCharacterIndex_1() const { return ___firstCharacterIndex_1; }
	inline int32_t* get_address_of_firstCharacterIndex_1() { return &___firstCharacterIndex_1; }
	inline void set_firstCharacterIndex_1(int32_t value)
	{
		___firstCharacterIndex_1 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_2() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___lastCharacterIndex_2)); }
	inline int32_t get_lastCharacterIndex_2() const { return ___lastCharacterIndex_2; }
	inline int32_t* get_address_of_lastCharacterIndex_2() { return &___lastCharacterIndex_2; }
	inline void set_lastCharacterIndex_2(int32_t value)
	{
		___lastCharacterIndex_2 = value;
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90_marshaled_pinvoke
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
// Native definition for COM marshalling of TMPro.TMP_WordInfo
struct TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90_marshaled_com
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_0;
	int32_t ___firstCharacterIndex_1;
	int32_t ___lastCharacterIndex_2;
	int32_t ___characterCount_3;
};
#endif // TMP_WORDINFO_T856E4994B49881E370B28E1D0C35EEDA56120D90_H
#ifndef TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#define TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagAttribute
struct  TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5 
{
public:
	// System.Int32 TMPro.TagAttribute::startIndex
	int32_t ___startIndex_0;
	// System.Int32 TMPro.TagAttribute::length
	int32_t ___length_1;
	// System.Int32 TMPro.TagAttribute::hashCode
	int32_t ___hashCode_2;

public:
	inline static int32_t get_offset_of_startIndex_0() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___startIndex_0)); }
	inline int32_t get_startIndex_0() const { return ___startIndex_0; }
	inline int32_t* get_address_of_startIndex_0() { return &___startIndex_0; }
	inline void set_startIndex_0(int32_t value)
	{
		___startIndex_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_hashCode_2() { return static_cast<int32_t>(offsetof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5, ___hashCode_2)); }
	inline int32_t get_hashCode_2() const { return ___hashCode_2; }
	inline int32_t* get_address_of_hashCode_2() { return &___hashCode_2; }
	inline void set_hashCode_2(int32_t value)
	{
		___hashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGATTRIBUTE_T76F6882B9F1A6E98098CFBB2D4B933BE36B543E5_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#define COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23_H
#ifndef MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#define MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T6BF60F70C9169DF14C9D2577672A44224B236ECA_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef GLYPHMETRICS_T1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB_H
#define GLYPHMETRICS_T1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.GlyphMetrics
struct  GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB 
{
public:
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_Width
	float ___m_Width_0;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_Height
	float ___m_Height_1;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalBearingX
	float ___m_HorizontalBearingX_2;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalBearingY
	float ___m_HorizontalBearingY_3;
	// System.Single UnityEngine.TextCore.GlyphMetrics::m_HorizontalAdvance
	float ___m_HorizontalAdvance_4;

public:
	inline static int32_t get_offset_of_m_Width_0() { return static_cast<int32_t>(offsetof(GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB, ___m_Width_0)); }
	inline float get_m_Width_0() const { return ___m_Width_0; }
	inline float* get_address_of_m_Width_0() { return &___m_Width_0; }
	inline void set_m_Width_0(float value)
	{
		___m_Width_0 = value;
	}

	inline static int32_t get_offset_of_m_Height_1() { return static_cast<int32_t>(offsetof(GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB, ___m_Height_1)); }
	inline float get_m_Height_1() const { return ___m_Height_1; }
	inline float* get_address_of_m_Height_1() { return &___m_Height_1; }
	inline void set_m_Height_1(float value)
	{
		___m_Height_1 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalBearingX_2() { return static_cast<int32_t>(offsetof(GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB, ___m_HorizontalBearingX_2)); }
	inline float get_m_HorizontalBearingX_2() const { return ___m_HorizontalBearingX_2; }
	inline float* get_address_of_m_HorizontalBearingX_2() { return &___m_HorizontalBearingX_2; }
	inline void set_m_HorizontalBearingX_2(float value)
	{
		___m_HorizontalBearingX_2 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalBearingY_3() { return static_cast<int32_t>(offsetof(GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB, ___m_HorizontalBearingY_3)); }
	inline float get_m_HorizontalBearingY_3() const { return ___m_HorizontalBearingY_3; }
	inline float* get_address_of_m_HorizontalBearingY_3() { return &___m_HorizontalBearingY_3; }
	inline void set_m_HorizontalBearingY_3(float value)
	{
		___m_HorizontalBearingY_3 = value;
	}

	inline static int32_t get_offset_of_m_HorizontalAdvance_4() { return static_cast<int32_t>(offsetof(GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB, ___m_HorizontalAdvance_4)); }
	inline float get_m_HorizontalAdvance_4() const { return ___m_HorizontalAdvance_4; }
	inline float* get_address_of_m_HorizontalAdvance_4() { return &___m_HorizontalAdvance_4; }
	inline void set_m_HorizontalAdvance_4(float value)
	{
		___m_HorizontalAdvance_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHMETRICS_T1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB_H
#ifndef GLYPHRECT_T398045C795E0E1264236DFAA5712796CC23C3E7C_H
#define GLYPHRECT_T398045C795E0E1264236DFAA5712796CC23C3E7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.GlyphRect
struct  GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C 
{
public:
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Y
	int32_t ___m_Y_1;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Width
	int32_t ___m_Width_2;
	// System.Int32 UnityEngine.TextCore.GlyphRect::m_Height
	int32_t ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C, ___m_Width_2)); }
	inline int32_t get_m_Width_2() const { return ___m_Width_2; }
	inline int32_t* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(int32_t value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C, ___m_Height_3)); }
	inline int32_t get_m_Height_3() const { return ___m_Height_3; }
	inline int32_t* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(int32_t value)
	{
		___m_Height_3 = value;
	}
};

struct GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C_StaticFields
{
public:
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.GlyphRect::s_ZeroGlyphRect
	GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  ___s_ZeroGlyphRect_4;

public:
	inline static int32_t get_offset_of_s_ZeroGlyphRect_4() { return static_cast<int32_t>(offsetof(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C_StaticFields, ___s_ZeroGlyphRect_4)); }
	inline GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  get_s_ZeroGlyphRect_4() const { return ___s_ZeroGlyphRect_4; }
	inline GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C * get_address_of_s_ZeroGlyphRect_4() { return &___s_ZeroGlyphRect_4; }
	inline void set_s_ZeroGlyphRect_4(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  value)
	{
		___s_ZeroGlyphRect_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GLYPHRECT_T398045C795E0E1264236DFAA5712796CC23C3E7C_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR2INT_T339DA203C037FA6BCFC926C36DC2194D52D5F905_H
#define VECTOR2INT_T339DA203C037FA6BCFC926C36DC2194D52D5F905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2Int
struct  Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 
{
public:
	// System.Int32 UnityEngine.Vector2Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector2Int::m_Y
	int32_t ___m_Y_1;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}
};

struct Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields
{
public:
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Zero
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Zero_2;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_One
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_One_3;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Up
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Up_4;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Down
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Down_5;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Left
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Left_6;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Right
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___s_Right_7;

public:
	inline static int32_t get_offset_of_s_Zero_2() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Zero_2)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Zero_2() const { return ___s_Zero_2; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Zero_2() { return &___s_Zero_2; }
	inline void set_s_Zero_2(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Zero_2 = value;
	}

	inline static int32_t get_offset_of_s_One_3() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_One_3)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_One_3() const { return ___s_One_3; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_One_3() { return &___s_One_3; }
	inline void set_s_One_3(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_One_3 = value;
	}

	inline static int32_t get_offset_of_s_Up_4() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Up_4)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Up_4() const { return ___s_Up_4; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Up_4() { return &___s_Up_4; }
	inline void set_s_Up_4(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Up_4 = value;
	}

	inline static int32_t get_offset_of_s_Down_5() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Down_5)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Down_5() const { return ___s_Down_5; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Down_5() { return &___s_Down_5; }
	inline void set_s_Down_5(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Down_5 = value;
	}

	inline static int32_t get_offset_of_s_Left_6() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Left_6)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Left_6() const { return ___s_Left_6; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Left_6() { return &___s_Left_6; }
	inline void set_s_Left_6(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Left_6 = value;
	}

	inline static int32_t get_offset_of_s_Right_7() { return static_cast<int32_t>(offsetof(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905_StaticFields, ___s_Right_7)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_s_Right_7() const { return ___s_Right_7; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_s_Right_7() { return &___s_Right_7; }
	inline void set_s_Right_7(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___s_Right_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2INT_T339DA203C037FA6BCFC926C36DC2194D52D5F905_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D12 <PrivateImplementationDetails>::7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D40 <PrivateImplementationDetails>::9E6378168821DBABB7EE3D0154346480FAC8AEF1
	__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1;

public:
	inline static int32_t get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields, ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  get_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline __StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 * get_address_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22  value)
	{
		___7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}

	inline static int32_t get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields, ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1)); }
	inline __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  get_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() const { return ___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline __StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D * get_address_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1() { return &___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1; }
	inline void set_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D  value)
	{
		___9E6378168821DBABB7EE3D0154346480FAC8AEF1_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T2BDD5686E87BA22996C7749A9C7A7F86EB589380_H
#ifndef OBSCUREDDECIMAL_T8FDFD59D7553624B2DC58962B8254E274B6E301C_H
#define OBSCUREDDECIMAL_T8FDFD59D7553624B2DC58962B8254E274B6E301C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal
struct  ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C 
{
public:
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::currentCryptoKey
	int64_t ___currentCryptoKey_0;
	// CodeStage.AntiCheat.Common.ACTkByte16 CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::hiddenValue
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  ___hiddenValue_1;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::inited
	bool ___inited_2;
	// System.Decimal CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::fakeValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___fakeValue_3;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal::fakeValueActive
	bool ___fakeValueActive_4;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C, ___currentCryptoKey_0)); }
	inline int64_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline int64_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(int64_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C, ___hiddenValue_1)); }
	inline ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340 * get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_inited_2() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C, ___inited_2)); }
	inline bool get_inited_2() const { return ___inited_2; }
	inline bool* get_address_of_inited_2() { return &___inited_2; }
	inline void set_inited_2(bool value)
	{
		___inited_2 = value;
	}

	inline static int32_t get_offset_of_fakeValue_3() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C, ___fakeValue_3)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_fakeValue_3() const { return ___fakeValue_3; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_fakeValue_3() { return &___fakeValue_3; }
	inline void set_fakeValue_3(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___fakeValue_3 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_4() { return static_cast<int32_t>(offsetof(ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C, ___fakeValueActive_4)); }
	inline bool get_fakeValueActive_4() const { return ___fakeValueActive_4; }
	inline bool* get_address_of_fakeValueActive_4() { return &___fakeValueActive_4; }
	inline void set_fakeValueActive_4(bool value)
	{
		___fakeValueActive_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal
struct ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C_marshaled_pinvoke
{
	int64_t ___currentCryptoKey_0;
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  ___hiddenValue_1;
	int32_t ___inited_2;
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal
struct ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C_marshaled_com
{
	int64_t ___currentCryptoKey_0;
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  ___hiddenValue_1;
	int32_t ___inited_2;
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___fakeValue_3;
	int32_t ___fakeValueActive_4;
};
#endif // OBSCUREDDECIMAL_T8FDFD59D7553624B2DC58962B8254E274B6E301C_H
#ifndef DECIMALLONGBYTESUNION_T950EAF510D7465C08CCE3AA133D59507B66A06B9_H
#define DECIMALLONGBYTESUNION_T950EAF510D7465C08CCE3AA133D59507B66A06B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal_DecimalLongBytesUnion
struct  DecimalLongBytesUnion_t950EAF510D7465C08CCE3AA133D59507B66A06B9 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Decimal CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal_DecimalLongBytesUnion::d
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___d_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___d_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal_DecimalLongBytesUnion::l1
			int64_t ___l1_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int64_t ___l1_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___l2_2_OffsetPadding[8];
			// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal_DecimalLongBytesUnion::l2
			int64_t ___l2_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___l2_2_OffsetPadding_forAlignmentOnly[8];
			int64_t ___l2_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// CodeStage.AntiCheat.Common.ACTkByte16 CodeStage.AntiCheat.ObscuredTypes.ObscuredDecimal_DecimalLongBytesUnion::b16
			ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  ___b16_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  ___b16_3_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(DecimalLongBytesUnion_t950EAF510D7465C08CCE3AA133D59507B66A06B9, ___d_0)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_d_0() const { return ___d_0; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___d_0 = value;
	}

	inline static int32_t get_offset_of_l1_1() { return static_cast<int32_t>(offsetof(DecimalLongBytesUnion_t950EAF510D7465C08CCE3AA133D59507B66A06B9, ___l1_1)); }
	inline int64_t get_l1_1() const { return ___l1_1; }
	inline int64_t* get_address_of_l1_1() { return &___l1_1; }
	inline void set_l1_1(int64_t value)
	{
		___l1_1 = value;
	}

	inline static int32_t get_offset_of_l2_2() { return static_cast<int32_t>(offsetof(DecimalLongBytesUnion_t950EAF510D7465C08CCE3AA133D59507B66A06B9, ___l2_2)); }
	inline int64_t get_l2_2() const { return ___l2_2; }
	inline int64_t* get_address_of_l2_2() { return &___l2_2; }
	inline void set_l2_2(int64_t value)
	{
		___l2_2 = value;
	}

	inline static int32_t get_offset_of_b16_3() { return static_cast<int32_t>(offsetof(DecimalLongBytesUnion_t950EAF510D7465C08CCE3AA133D59507B66A06B9, ___b16_3)); }
	inline ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  get_b16_3() const { return ___b16_3; }
	inline ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340 * get_address_of_b16_3() { return &___b16_3; }
	inline void set_b16_3(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340  value)
	{
		___b16_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMALLONGBYTESUNION_T950EAF510D7465C08CCE3AA133D59507B66A06B9_H
#ifndef OBSCUREDDOUBLE_T482D3EFF59CAD02A1890E031E5215115ED10F71B_H
#define OBSCUREDDOUBLE_T482D3EFF59CAD02A1890E031E5215115ED10F71B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble
struct  ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B 
{
public:
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble::currentCryptoKey
	int64_t ___currentCryptoKey_0;
	// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble::hiddenValue
	int64_t ___hiddenValue_1;
	// CodeStage.AntiCheat.Common.ACTkByte8 CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble::hiddenValueOldByte8
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  ___hiddenValueOldByte8_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble::inited
	bool ___inited_3;
	// System.Double CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble::fakeValue
	double ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble::fakeValueActive
	bool ___fakeValueActive_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B, ___currentCryptoKey_0)); }
	inline int64_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline int64_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(int64_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B, ___hiddenValue_1)); }
	inline int64_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline int64_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(int64_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValueOldByte8_2() { return static_cast<int32_t>(offsetof(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B, ___hiddenValueOldByte8_2)); }
	inline ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  get_hiddenValueOldByte8_2() const { return ___hiddenValueOldByte8_2; }
	inline ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E * get_address_of_hiddenValueOldByte8_2() { return &___hiddenValueOldByte8_2; }
	inline void set_hiddenValueOldByte8_2(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  value)
	{
		___hiddenValueOldByte8_2 = value;
	}

	inline static int32_t get_offset_of_inited_3() { return static_cast<int32_t>(offsetof(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B, ___inited_3)); }
	inline bool get_inited_3() const { return ___inited_3; }
	inline bool* get_address_of_inited_3() { return &___inited_3; }
	inline void set_inited_3(bool value)
	{
		___inited_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B, ___fakeValue_4)); }
	inline double get_fakeValue_4() const { return ___fakeValue_4; }
	inline double* get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(double value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_5() { return static_cast<int32_t>(offsetof(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B, ___fakeValueActive_5)); }
	inline bool get_fakeValueActive_5() const { return ___fakeValueActive_5; }
	inline bool* get_address_of_fakeValueActive_5() { return &___fakeValueActive_5; }
	inline void set_fakeValueActive_5(bool value)
	{
		___fakeValueActive_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble
struct ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B_marshaled_pinvoke
{
	int64_t ___currentCryptoKey_0;
	int64_t ___hiddenValue_1;
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  ___hiddenValueOldByte8_2;
	int32_t ___inited_3;
	double ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble
struct ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B_marshaled_com
{
	int64_t ___currentCryptoKey_0;
	int64_t ___hiddenValue_1;
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  ___hiddenValueOldByte8_2;
	int32_t ___inited_3;
	double ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
#endif // OBSCUREDDOUBLE_T482D3EFF59CAD02A1890E031E5215115ED10F71B_H
#ifndef DOUBLELONGBYTESUNION_T052DF0AE955CA3ABEBD3C19ED4CD222D89F74458_H
#define DOUBLELONGBYTESUNION_T052DF0AE955CA3ABEBD3C19ED4CD222D89F74458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble_DoubleLongBytesUnion
struct  DoubleLongBytesUnion_t052DF0AE955CA3ABEBD3C19ED4CD222D89F74458 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Double CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble_DoubleLongBytesUnion::d
			double ___d_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___d_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int64 CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble_DoubleLongBytesUnion::l
			int64_t ___l_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int64_t ___l_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// CodeStage.AntiCheat.Common.ACTkByte8 CodeStage.AntiCheat.ObscuredTypes.ObscuredDouble_DoubleLongBytesUnion::b8
			ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  ___b8_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  ___b8_2_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_d_0() { return static_cast<int32_t>(offsetof(DoubleLongBytesUnion_t052DF0AE955CA3ABEBD3C19ED4CD222D89F74458, ___d_0)); }
	inline double get_d_0() const { return ___d_0; }
	inline double* get_address_of_d_0() { return &___d_0; }
	inline void set_d_0(double value)
	{
		___d_0 = value;
	}

	inline static int32_t get_offset_of_l_1() { return static_cast<int32_t>(offsetof(DoubleLongBytesUnion_t052DF0AE955CA3ABEBD3C19ED4CD222D89F74458, ___l_1)); }
	inline int64_t get_l_1() const { return ___l_1; }
	inline int64_t* get_address_of_l_1() { return &___l_1; }
	inline void set_l_1(int64_t value)
	{
		___l_1 = value;
	}

	inline static int32_t get_offset_of_b8_2() { return static_cast<int32_t>(offsetof(DoubleLongBytesUnion_t052DF0AE955CA3ABEBD3C19ED4CD222D89F74458, ___b8_2)); }
	inline ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  get_b8_2() const { return ___b8_2; }
	inline ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E * get_address_of_b8_2() { return &___b8_2; }
	inline void set_b8_2(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E  value)
	{
		___b8_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLELONGBYTESUNION_T052DF0AE955CA3ABEBD3C19ED4CD222D89F74458_H
#ifndef OBSCUREDFLOAT_T4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077_H
#define OBSCUREDFLOAT_T4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat
struct  ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::currentCryptoKey
	int32_t ___currentCryptoKey_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::hiddenValue
	int32_t ___hiddenValue_1;
	// CodeStage.AntiCheat.Common.ACTkByte4 CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::hiddenValueOldByte4
	ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  ___hiddenValueOldByte4_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::inited
	bool ___inited_3;
	// System.Single CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::fakeValue
	float ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat::fakeValueActive
	bool ___fakeValueActive_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_0() { return static_cast<int32_t>(offsetof(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077, ___currentCryptoKey_0)); }
	inline int32_t get_currentCryptoKey_0() const { return ___currentCryptoKey_0; }
	inline int32_t* get_address_of_currentCryptoKey_0() { return &___currentCryptoKey_0; }
	inline void set_currentCryptoKey_0(int32_t value)
	{
		___currentCryptoKey_0 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_1() { return static_cast<int32_t>(offsetof(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077, ___hiddenValue_1)); }
	inline int32_t get_hiddenValue_1() const { return ___hiddenValue_1; }
	inline int32_t* get_address_of_hiddenValue_1() { return &___hiddenValue_1; }
	inline void set_hiddenValue_1(int32_t value)
	{
		___hiddenValue_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValueOldByte4_2() { return static_cast<int32_t>(offsetof(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077, ___hiddenValueOldByte4_2)); }
	inline ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  get_hiddenValueOldByte4_2() const { return ___hiddenValueOldByte4_2; }
	inline ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE * get_address_of_hiddenValueOldByte4_2() { return &___hiddenValueOldByte4_2; }
	inline void set_hiddenValueOldByte4_2(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  value)
	{
		___hiddenValueOldByte4_2 = value;
	}

	inline static int32_t get_offset_of_inited_3() { return static_cast<int32_t>(offsetof(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077, ___inited_3)); }
	inline bool get_inited_3() const { return ___inited_3; }
	inline bool* get_address_of_inited_3() { return &___inited_3; }
	inline void set_inited_3(bool value)
	{
		___inited_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077, ___fakeValue_4)); }
	inline float get_fakeValue_4() const { return ___fakeValue_4; }
	inline float* get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(float value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_5() { return static_cast<int32_t>(offsetof(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077, ___fakeValueActive_5)); }
	inline bool get_fakeValueActive_5() const { return ___fakeValueActive_5; }
	inline bool* get_address_of_fakeValueActive_5() { return &___fakeValueActive_5; }
	inline void set_fakeValueActive_5(bool value)
	{
		___fakeValueActive_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat
struct ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  ___hiddenValueOldByte4_2;
	int32_t ___inited_3;
	float ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat
struct ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077_marshaled_com
{
	int32_t ___currentCryptoKey_0;
	int32_t ___hiddenValue_1;
	ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  ___hiddenValueOldByte4_2;
	int32_t ___inited_3;
	float ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
#endif // OBSCUREDFLOAT_T4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077_H
#ifndef FLOATINTBYTESUNION_TD999463DD3670D86407DB05972540430AB56D42B_H
#define FLOATINTBYTESUNION_TD999463DD3670D86407DB05972540430AB56D42B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat_FloatIntBytesUnion
struct  FloatIntBytesUnion_tD999463DD3670D86407DB05972540430AB56D42B 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat_FloatIntBytesUnion::f
			float ___f_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___f_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat_FloatIntBytesUnion::i
			int32_t ___i_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___i_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// CodeStage.AntiCheat.Common.ACTkByte4 CodeStage.AntiCheat.ObscuredTypes.ObscuredFloat_FloatIntBytesUnion::b4
			ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  ___b4_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  ___b4_2_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_f_0() { return static_cast<int32_t>(offsetof(FloatIntBytesUnion_tD999463DD3670D86407DB05972540430AB56D42B, ___f_0)); }
	inline float get_f_0() const { return ___f_0; }
	inline float* get_address_of_f_0() { return &___f_0; }
	inline void set_f_0(float value)
	{
		___f_0 = value;
	}

	inline static int32_t get_offset_of_i_1() { return static_cast<int32_t>(offsetof(FloatIntBytesUnion_tD999463DD3670D86407DB05972540430AB56D42B, ___i_1)); }
	inline int32_t get_i_1() const { return ___i_1; }
	inline int32_t* get_address_of_i_1() { return &___i_1; }
	inline void set_i_1(int32_t value)
	{
		___i_1 = value;
	}

	inline static int32_t get_offset_of_b4_2() { return static_cast<int32_t>(offsetof(FloatIntBytesUnion_tD999463DD3670D86407DB05972540430AB56D42B, ___b4_2)); }
	inline ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  get_b4_2() const { return ___b4_2; }
	inline ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE * get_address_of_b4_2() { return &___b4_2; }
	inline void set_b4_2(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE  value)
	{
		___b4_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATINTBYTESUNION_TD999463DD3670D86407DB05972540430AB56D42B_H
#ifndef OBSCUREDQUATERNION_TAE6966A12AB044F8E660DEEB7CED4EC2550A257A_H
#define OBSCUREDQUATERNION_TAE6966A12AB044F8E660DEEB7CED4EC2550A257A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion
struct  ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion::currentCryptoKey
	int32_t ___currentCryptoKey_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion_RawEncryptedQuaternion CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion::hiddenValue
	RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7  ___hiddenValue_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion::inited
	bool ___inited_3;
	// UnityEngine.Quaternion CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion::fakeValue
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion::fakeValueActive
	bool ___fakeValueActive_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A, ___currentCryptoKey_1)); }
	inline int32_t get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline int32_t* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(int32_t value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A, ___hiddenValue_2)); }
	inline RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7  get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7 * get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7  value)
	{
		___hiddenValue_2 = value;
	}

	inline static int32_t get_offset_of_inited_3() { return static_cast<int32_t>(offsetof(ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A, ___inited_3)); }
	inline bool get_inited_3() const { return ___inited_3; }
	inline bool* get_address_of_inited_3() { return &___inited_3; }
	inline void set_inited_3(bool value)
	{
		___inited_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A, ___fakeValue_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_fakeValue_4() const { return ___fakeValue_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_5() { return static_cast<int32_t>(offsetof(ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A, ___fakeValueActive_5)); }
	inline bool get_fakeValueActive_5() const { return ___fakeValueActive_5; }
	inline bool* get_address_of_fakeValueActive_5() { return &___fakeValueActive_5; }
	inline void set_fakeValueActive_5(bool value)
	{
		___fakeValueActive_5 = value;
	}
};

struct ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A_StaticFields
{
public:
	// UnityEngine.Quaternion CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion::Identity
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___Identity_0;

public:
	inline static int32_t get_offset_of_Identity_0() { return static_cast<int32_t>(offsetof(ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A_StaticFields, ___Identity_0)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_Identity_0() const { return ___Identity_0; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_Identity_0() { return &___Identity_0; }
	inline void set_Identity_0(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___Identity_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion
struct ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7  ___hiddenValue_2;
	int32_t ___inited_3;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredQuaternion
struct ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A_marshaled_com
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7  ___hiddenValue_2;
	int32_t ___inited_3;
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
#endif // OBSCUREDQUATERNION_TAE6966A12AB044F8E660DEEB7CED4EC2550A257A_H
#ifndef OBSCUREDVECTOR2_T11B55619F5729BB5B30E922263095D6D862D5855_H
#define OBSCUREDVECTOR2_T11B55619F5729BB5B30E922263095D6D862D5855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2
struct  ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2::currentCryptoKey
	int32_t ___currentCryptoKey_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2_RawEncryptedVector2 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2::hiddenValue
	RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC  ___hiddenValue_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2::inited
	bool ___inited_3;
	// UnityEngine.Vector2 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2::fakeValue
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2::fakeValueActive
	bool ___fakeValueActive_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855, ___currentCryptoKey_1)); }
	inline int32_t get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline int32_t* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(int32_t value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855, ___hiddenValue_2)); }
	inline RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC  get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC * get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC  value)
	{
		___hiddenValue_2 = value;
	}

	inline static int32_t get_offset_of_inited_3() { return static_cast<int32_t>(offsetof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855, ___inited_3)); }
	inline bool get_inited_3() const { return ___inited_3; }
	inline bool* get_address_of_inited_3() { return &___inited_3; }
	inline void set_inited_3(bool value)
	{
		___inited_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855, ___fakeValue_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_fakeValue_4() const { return ___fakeValue_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_5() { return static_cast<int32_t>(offsetof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855, ___fakeValueActive_5)); }
	inline bool get_fakeValueActive_5() const { return ___fakeValueActive_5; }
	inline bool* get_address_of_fakeValueActive_5() { return &___fakeValueActive_5; }
	inline void set_fakeValueActive_5(bool value)
	{
		___fakeValueActive_5 = value;
	}
};

struct ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855_StaticFields
{
public:
	// UnityEngine.Vector2 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2::Zero
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855_StaticFields, ___Zero_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_Zero_0() const { return ___Zero_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2
struct ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2
struct ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855_marshaled_com
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
#endif // OBSCUREDVECTOR2_T11B55619F5729BB5B30E922263095D6D862D5855_H
#ifndef OBSCUREDVECTOR2INT_T195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_H
#define OBSCUREDVECTOR2INT_T195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int
struct  ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int::currentCryptoKey
	int32_t ___currentCryptoKey_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int_RawEncryptedVector2Int CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int::hiddenValue
	RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D  ___hiddenValue_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int::inited
	bool ___inited_3;
	// UnityEngine.Vector2Int CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int::fakeValue
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int::fakeValueActive
	bool ___fakeValueActive_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A, ___currentCryptoKey_1)); }
	inline int32_t get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline int32_t* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(int32_t value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A, ___hiddenValue_2)); }
	inline RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D  get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D * get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D  value)
	{
		___hiddenValue_2 = value;
	}

	inline static int32_t get_offset_of_inited_3() { return static_cast<int32_t>(offsetof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A, ___inited_3)); }
	inline bool get_inited_3() const { return ___inited_3; }
	inline bool* get_address_of_inited_3() { return &___inited_3; }
	inline void set_inited_3(bool value)
	{
		___inited_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A, ___fakeValue_4)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_fakeValue_4() const { return ___fakeValue_4; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_5() { return static_cast<int32_t>(offsetof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A, ___fakeValueActive_5)); }
	inline bool get_fakeValueActive_5() const { return ___fakeValueActive_5; }
	inline bool* get_address_of_fakeValueActive_5() { return &___fakeValueActive_5; }
	inline void set_fakeValueActive_5(bool value)
	{
		___fakeValueActive_5 = value;
	}
};

struct ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_StaticFields
{
public:
	// UnityEngine.Vector2Int CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int::Zero
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_StaticFields, ___Zero_0)); }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  get_Zero_0() const { return ___Zero_0; }
	inline Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int
struct ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector2Int
struct ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_marshaled_com
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector2Int_t339DA203C037FA6BCFC926C36DC2194D52D5F905  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
#endif // OBSCUREDVECTOR2INT_T195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_H
#ifndef OBSCUREDVECTOR3_T78946FDA503DFD5D6975074F4408EDCF397E1781_H
#define OBSCUREDVECTOR3_T78946FDA503DFD5D6975074F4408EDCF397E1781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3
struct  ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3::currentCryptoKey
	int32_t ___currentCryptoKey_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3::hiddenValue
	RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627  ___hiddenValue_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3::inited
	bool ___inited_3;
	// UnityEngine.Vector3 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3::fakeValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3::fakeValueActive
	bool ___fakeValueActive_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781, ___currentCryptoKey_1)); }
	inline int32_t get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline int32_t* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(int32_t value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781, ___hiddenValue_2)); }
	inline RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627  get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627 * get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627  value)
	{
		___hiddenValue_2 = value;
	}

	inline static int32_t get_offset_of_inited_3() { return static_cast<int32_t>(offsetof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781, ___inited_3)); }
	inline bool get_inited_3() const { return ___inited_3; }
	inline bool* get_address_of_inited_3() { return &___inited_3; }
	inline void set_inited_3(bool value)
	{
		___inited_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781, ___fakeValue_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_fakeValue_4() const { return ___fakeValue_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_5() { return static_cast<int32_t>(offsetof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781, ___fakeValueActive_5)); }
	inline bool get_fakeValueActive_5() const { return ___fakeValueActive_5; }
	inline bool* get_address_of_fakeValueActive_5() { return &___fakeValueActive_5; }
	inline void set_fakeValueActive_5(bool value)
	{
		___fakeValueActive_5 = value;
	}
};

struct ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781_StaticFields
{
public:
	// UnityEngine.Vector3 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3::Zero
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781_StaticFields, ___Zero_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Zero_0() const { return ___Zero_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3
struct ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3
struct ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781_marshaled_com
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
#endif // OBSCUREDVECTOR3_T78946FDA503DFD5D6975074F4408EDCF397E1781_H
#ifndef DATATYPE_T239D221E2F1F3CB5396A74F07DA04CF862D61F23_H
#define DATATYPE_T239D221E2F1F3CB5396A74F07DA04CF862D61F23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Storage.ObscuredPrefs_DataType
struct  DataType_t239D221E2F1F3CB5396A74F07DA04CF862D61F23 
{
public:
	// System.Byte CodeStage.AntiCheat.Storage.ObscuredPrefs_DataType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataType_t239D221E2F1F3CB5396A74F07DA04CF862D61F23, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_T239D221E2F1F3CB5396A74F07DA04CF862D61F23_H
#ifndef DEVICELOCKLEVEL_T343109F2BB7200998C3CB19E58B09AA719CA84C0_H
#define DEVICELOCKLEVEL_T343109F2BB7200998C3CB19E58B09AA719CA84C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Storage.ObscuredPrefs_DeviceLockLevel
struct  DeviceLockLevel_t343109F2BB7200998C3CB19E58B09AA719CA84C0 
{
public:
	// System.Byte CodeStage.AntiCheat.Storage.ObscuredPrefs_DeviceLockLevel::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DeviceLockLevel_t343109F2BB7200998C3CB19E58B09AA719CA84C0, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICELOCKLEVEL_T343109F2BB7200998C3CB19E58B09AA719CA84C0_H
#ifndef CARETPOSITION_T6781B2FD769974C40ECDB58E3BCBA4ADC1B34483_H
#define CARETPOSITION_T6781B2FD769974C40ECDB58E3BCBA4ADC1B34483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretPosition
struct  CaretPosition_t6781B2FD769974C40ECDB58E3BCBA4ADC1B34483 
{
public:
	// System.Int32 TMPro.CaretPosition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CaretPosition_t6781B2FD769974C40ECDB58E3BCBA4ADC1B34483, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETPOSITION_T6781B2FD769974C40ECDB58E3BCBA4ADC1B34483_H
#ifndef COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#define COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.ColorMode
struct  ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3 
{
public:
	// System.Int32 TMPro.ColorMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorMode_tA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_TA3D65CECD3289ADB3A3C5A936DC23B41C364C4C3_H
#ifndef COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T7D7D951983E29A63EA0B4AE2EBC021B801291E50_H
#define COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T7D7D951983E29A63EA0B4AE2EBC021B801291E50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DistanceTransform_EventTypes
struct  Compute_DistanceTransform_EventTypes_t7D7D951983E29A63EA0B4AE2EBC021B801291E50 
{
public:
	// System.Int32 TMPro.Compute_DistanceTransform_EventTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Compute_DistanceTransform_EventTypes_t7D7D951983E29A63EA0B4AE2EBC021B801291E50, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DISTANCETRANSFORM_EVENTTYPES_T7D7D951983E29A63EA0B4AE2EBC021B801291E50_H
#ifndef EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#define EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Extents
struct  Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 
{
public:
	// UnityEngine.Vector2 TMPro.Extents::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 TMPro.Extents::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTS_TB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3_H
#ifndef FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#define FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontStyles
struct  FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893 
{
public:
	// System.Int32 TMPro.FontStyles::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLES_T31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893_H
#ifndef FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#define FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.FontWeight
struct  FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C 
{
public:
	// System.Int32 TMPro.FontWeight::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTWEIGHT_TE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C_H
#ifndef MASKINGOFFSETMODE_T2EC14EE19A8002F3390C5706568D36A3C7E0C7DA_H
#define MASKINGOFFSETMODE_T2EC14EE19A8002F3390C5706568D36A3C7E0C7DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingOffsetMode
struct  MaskingOffsetMode_t2EC14EE19A8002F3390C5706568D36A3C7E0C7DA 
{
public:
	// System.Int32 TMPro.MaskingOffsetMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaskingOffsetMode_t2EC14EE19A8002F3390C5706568D36A3C7E0C7DA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGOFFSETMODE_T2EC14EE19A8002F3390C5706568D36A3C7E0C7DA_H
#ifndef MASKINGTYPES_T37B6F292739A890CF34EA024D24A5BFA88579086_H
#define MASKINGTYPES_T37B6F292739A890CF34EA024D24A5BFA88579086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.MaskingTypes
struct  MaskingTypes_t37B6F292739A890CF34EA024D24A5BFA88579086 
{
public:
	// System.Int32 TMPro.MaskingTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MaskingTypes_t37B6F292739A890CF34EA024D24A5BFA88579086, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKINGTYPES_T37B6F292739A890CF34EA024D24A5BFA88579086_H
#ifndef MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#define MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Mesh_Extents
struct  Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8 
{
public:
	// UnityEngine.Vector2 TMPro.Mesh_Extents::min
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___min_0;
	// UnityEngine.Vector2 TMPro.Mesh_Extents::max
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8, ___min_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_min_0() const { return ___min_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8, ___max_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_max_1() const { return ___max_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___max_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_EXTENTS_T4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8_H
#ifndef SPRITEASSETIMPORTFORMATS_T9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E_H
#define SPRITEASSETIMPORTFORMATS_T9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.SpriteAssetImportFormats
struct  SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E 
{
public:
	// System.Int32 TMPro.SpriteAssetUtilities.SpriteAssetImportFormats::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEASSETIMPORTFORMATS_T9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E_H
#ifndef SPRITEDATA_T8ADAD39500AF244CC913ED9B1F964DF04B4E5728_H
#define SPRITEDATA_T8ADAD39500AF244CC913ED9B1F964DF04B4E5728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.SpriteAssetUtilities.TexturePacker_SpriteData
struct  SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728 
{
public:
	// System.String TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::filename
	String_t* ___filename_0;
	// TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::frame
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___frame_1;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::rotated
	bool ___rotated_2;
	// System.Boolean TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::trimmed
	bool ___trimmed_3;
	// TMPro.SpriteAssetUtilities.TexturePacker_SpriteFrame TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::spriteSourceSize
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___spriteSourceSize_4;
	// TMPro.SpriteAssetUtilities.TexturePacker_SpriteSize TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::sourceSize
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  ___sourceSize_5;
	// UnityEngine.Vector2 TMPro.SpriteAssetUtilities.TexturePacker_SpriteData::pivot
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_6;

public:
	inline static int32_t get_offset_of_filename_0() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___filename_0)); }
	inline String_t* get_filename_0() const { return ___filename_0; }
	inline String_t** get_address_of_filename_0() { return &___filename_0; }
	inline void set_filename_0(String_t* value)
	{
		___filename_0 = value;
		Il2CppCodeGenWriteBarrier((&___filename_0), value);
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___frame_1)); }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  get_frame_1() const { return ___frame_1; }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 * get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  value)
	{
		___frame_1 = value;
	}

	inline static int32_t get_offset_of_rotated_2() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___rotated_2)); }
	inline bool get_rotated_2() const { return ___rotated_2; }
	inline bool* get_address_of_rotated_2() { return &___rotated_2; }
	inline void set_rotated_2(bool value)
	{
		___rotated_2 = value;
	}

	inline static int32_t get_offset_of_trimmed_3() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___trimmed_3)); }
	inline bool get_trimmed_3() const { return ___trimmed_3; }
	inline bool* get_address_of_trimmed_3() { return &___trimmed_3; }
	inline void set_trimmed_3(bool value)
	{
		___trimmed_3 = value;
	}

	inline static int32_t get_offset_of_spriteSourceSize_4() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___spriteSourceSize_4)); }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  get_spriteSourceSize_4() const { return ___spriteSourceSize_4; }
	inline SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 * get_address_of_spriteSourceSize_4() { return &___spriteSourceSize_4; }
	inline void set_spriteSourceSize_4(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  value)
	{
		___spriteSourceSize_4 = value;
	}

	inline static int32_t get_offset_of_sourceSize_5() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___sourceSize_5)); }
	inline SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  get_sourceSize_5() const { return ___sourceSize_5; }
	inline SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E * get_address_of_sourceSize_5() { return &___sourceSize_5; }
	inline void set_sourceSize_5(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  value)
	{
		___sourceSize_5 = value;
	}

	inline static int32_t get_offset_of_pivot_6() { return static_cast<int32_t>(offsetof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728, ___pivot_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_pivot_6() const { return ___pivot_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_pivot_6() { return &___pivot_6; }
	inline void set_pivot_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___pivot_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728_marshaled_pinvoke
{
	char* ___filename_0;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___spriteSourceSize_4;
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  ___sourceSize_5;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_6;
};
// Native definition for COM marshalling of TMPro.SpriteAssetUtilities.TexturePacker/SpriteData
struct SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728_marshaled_com
{
	Il2CppChar* ___filename_0;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___frame_1;
	int32_t ___rotated_2;
	int32_t ___trimmed_3;
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04  ___spriteSourceSize_4;
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E  ___sourceSize_5;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_6;
};
#endif // SPRITEDATA_T8ADAD39500AF244CC913ED9B1F964DF04B4E5728_H
#ifndef TMP_MATH_T074FB253662A213E8905642B8B29473EC794FE44_H
#define TMP_MATH_T074FB253662A213E8905642B8B29473EC794FE44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Math
struct  TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44  : public RuntimeObject
{
public:

public:
};

struct TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_Math::MAX_16BIT
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___MAX_16BIT_6;
	// UnityEngine.Vector2 TMPro.TMP_Math::MIN_16BIT
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___MIN_16BIT_7;

public:
	inline static int32_t get_offset_of_MAX_16BIT_6() { return static_cast<int32_t>(offsetof(TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields, ___MAX_16BIT_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_MAX_16BIT_6() const { return ___MAX_16BIT_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_MAX_16BIT_6() { return &___MAX_16BIT_6; }
	inline void set_MAX_16BIT_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___MAX_16BIT_6 = value;
	}

	inline static int32_t get_offset_of_MIN_16BIT_7() { return static_cast<int32_t>(offsetof(TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields, ___MIN_16BIT_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_MIN_16BIT_7() const { return ___MIN_16BIT_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_MIN_16BIT_7() { return &___MIN_16BIT_7; }
	inline void set_MIN_16BIT_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___MIN_16BIT_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_MATH_T074FB253662A213E8905642B8B29473EC794FE44_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#define TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference>
struct  TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_ItemStack_0)); }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9, ___m_DefaultItem_3)); }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F * get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(MaterialReference_tFDD866CC1D210125CDEC9DCB60B9AACB2FE3AF7F  value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T9742B1BC2B58D513502935F599F4AF09FFC379A9_H
#ifndef TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#define TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32>
struct  TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_ItemStack_0)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0, ___m_DefaultItem_3)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_TDAE1C182F153530A3E6A3ADC1803919A380BCDF0_H
#ifndef TMP_SPRITE_T8D26AE7781056AB44B074C80FFC74AFB076E1353_H
#define TMP_SPRITE_T8D26AE7781056AB44B074C80FFC74AFB076E1353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Sprite
struct  TMP_Sprite_t8D26AE7781056AB44B074C80FFC74AFB076E1353  : public TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05
{
public:
	// System.String TMPro.TMP_Sprite::name
	String_t* ___name_9;
	// System.Int32 TMPro.TMP_Sprite::hashCode
	int32_t ___hashCode_10;
	// System.Int32 TMPro.TMP_Sprite::unicode
	int32_t ___unicode_11;
	// UnityEngine.Vector2 TMPro.TMP_Sprite::pivot
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot_12;
	// UnityEngine.Sprite TMPro.TMP_Sprite::sprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___sprite_13;

public:
	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(TMP_Sprite_t8D26AE7781056AB44B074C80FFC74AFB076E1353, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_hashCode_10() { return static_cast<int32_t>(offsetof(TMP_Sprite_t8D26AE7781056AB44B074C80FFC74AFB076E1353, ___hashCode_10)); }
	inline int32_t get_hashCode_10() const { return ___hashCode_10; }
	inline int32_t* get_address_of_hashCode_10() { return &___hashCode_10; }
	inline void set_hashCode_10(int32_t value)
	{
		___hashCode_10 = value;
	}

	inline static int32_t get_offset_of_unicode_11() { return static_cast<int32_t>(offsetof(TMP_Sprite_t8D26AE7781056AB44B074C80FFC74AFB076E1353, ___unicode_11)); }
	inline int32_t get_unicode_11() const { return ___unicode_11; }
	inline int32_t* get_address_of_unicode_11() { return &___unicode_11; }
	inline void set_unicode_11(int32_t value)
	{
		___unicode_11 = value;
	}

	inline static int32_t get_offset_of_pivot_12() { return static_cast<int32_t>(offsetof(TMP_Sprite_t8D26AE7781056AB44B074C80FFC74AFB076E1353, ___pivot_12)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_pivot_12() const { return ___pivot_12; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_pivot_12() { return &___pivot_12; }
	inline void set_pivot_12(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___pivot_12 = value;
	}

	inline static int32_t get_offset_of_sprite_13() { return static_cast<int32_t>(offsetof(TMP_Sprite_t8D26AE7781056AB44B074C80FFC74AFB076E1353, ___sprite_13)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_sprite_13() const { return ___sprite_13; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_sprite_13() { return &___sprite_13; }
	inline void set_sprite_13(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___sprite_13 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITE_T8D26AE7781056AB44B074C80FFC74AFB076E1353_H
#ifndef TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#define TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text_TextInputSources
struct  TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4 
{
public:
	// System.Int32 TMPro.TMP_Text_TextInputSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTINPUTSOURCES_T08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4_H
#ifndef TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#define TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElementType
struct  TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509 
{
public:
	// System.Int32 TMPro.TMP_TextElementType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENTTYPE_TBF2553FA730CC21CF99473E591C33DC52360D509_H
#ifndef TMP_TEXTINFO_TC40DAAB47C5BD5AD21B3F456D860474D96D9C181_H
#define TMP_TEXTINFO_TC40DAAB47C5BD5AD21B3F456D860474D96D9C181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextInfo
struct  TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181  : public RuntimeObject
{
public:
	// TMPro.TMP_Text TMPro.TMP_TextInfo::textComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___textComponent_2;
	// System.Int32 TMPro.TMP_TextInfo::characterCount
	int32_t ___characterCount_3;
	// System.Int32 TMPro.TMP_TextInfo::spriteCount
	int32_t ___spriteCount_4;
	// System.Int32 TMPro.TMP_TextInfo::spaceCount
	int32_t ___spaceCount_5;
	// System.Int32 TMPro.TMP_TextInfo::wordCount
	int32_t ___wordCount_6;
	// System.Int32 TMPro.TMP_TextInfo::linkCount
	int32_t ___linkCount_7;
	// System.Int32 TMPro.TMP_TextInfo::lineCount
	int32_t ___lineCount_8;
	// System.Int32 TMPro.TMP_TextInfo::pageCount
	int32_t ___pageCount_9;
	// System.Int32 TMPro.TMP_TextInfo::materialCount
	int32_t ___materialCount_10;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_TextInfo::characterInfo
	TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* ___characterInfo_11;
	// TMPro.TMP_WordInfo[] TMPro.TMP_TextInfo::wordInfo
	TMP_WordInfoU5BU5D_t2C9C805935A8C8FFD43BF92C96AC70737AA52F09* ___wordInfo_12;
	// TMPro.TMP_LinkInfo[] TMPro.TMP_TextInfo::linkInfo
	TMP_LinkInfoU5BU5D_t5965804162EB43CD70F792B74DA179B32224BB0D* ___linkInfo_13;
	// TMPro.TMP_LineInfo[] TMPro.TMP_TextInfo::lineInfo
	TMP_LineInfoU5BU5D_t3D5D11E746B537C3951927E490B7A1BAB9C23A5C* ___lineInfo_14;
	// TMPro.TMP_PageInfo[] TMPro.TMP_TextInfo::pageInfo
	TMP_PageInfoU5BU5D_tFB7F7AD2CD9ADBE07099C1A06170B51AA8D9D847* ___pageInfo_15;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::meshInfo
	TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* ___meshInfo_16;
	// TMPro.TMP_MeshInfo[] TMPro.TMP_TextInfo::m_CachedMeshInfo
	TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* ___m_CachedMeshInfo_17;

public:
	inline static int32_t get_offset_of_textComponent_2() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___textComponent_2)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_textComponent_2() const { return ___textComponent_2; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_textComponent_2() { return &___textComponent_2; }
	inline void set_textComponent_2(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___textComponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___textComponent_2), value);
	}

	inline static int32_t get_offset_of_characterCount_3() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___characterCount_3)); }
	inline int32_t get_characterCount_3() const { return ___characterCount_3; }
	inline int32_t* get_address_of_characterCount_3() { return &___characterCount_3; }
	inline void set_characterCount_3(int32_t value)
	{
		___characterCount_3 = value;
	}

	inline static int32_t get_offset_of_spriteCount_4() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___spriteCount_4)); }
	inline int32_t get_spriteCount_4() const { return ___spriteCount_4; }
	inline int32_t* get_address_of_spriteCount_4() { return &___spriteCount_4; }
	inline void set_spriteCount_4(int32_t value)
	{
		___spriteCount_4 = value;
	}

	inline static int32_t get_offset_of_spaceCount_5() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___spaceCount_5)); }
	inline int32_t get_spaceCount_5() const { return ___spaceCount_5; }
	inline int32_t* get_address_of_spaceCount_5() { return &___spaceCount_5; }
	inline void set_spaceCount_5(int32_t value)
	{
		___spaceCount_5 = value;
	}

	inline static int32_t get_offset_of_wordCount_6() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___wordCount_6)); }
	inline int32_t get_wordCount_6() const { return ___wordCount_6; }
	inline int32_t* get_address_of_wordCount_6() { return &___wordCount_6; }
	inline void set_wordCount_6(int32_t value)
	{
		___wordCount_6 = value;
	}

	inline static int32_t get_offset_of_linkCount_7() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___linkCount_7)); }
	inline int32_t get_linkCount_7() const { return ___linkCount_7; }
	inline int32_t* get_address_of_linkCount_7() { return &___linkCount_7; }
	inline void set_linkCount_7(int32_t value)
	{
		___linkCount_7 = value;
	}

	inline static int32_t get_offset_of_lineCount_8() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___lineCount_8)); }
	inline int32_t get_lineCount_8() const { return ___lineCount_8; }
	inline int32_t* get_address_of_lineCount_8() { return &___lineCount_8; }
	inline void set_lineCount_8(int32_t value)
	{
		___lineCount_8 = value;
	}

	inline static int32_t get_offset_of_pageCount_9() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___pageCount_9)); }
	inline int32_t get_pageCount_9() const { return ___pageCount_9; }
	inline int32_t* get_address_of_pageCount_9() { return &___pageCount_9; }
	inline void set_pageCount_9(int32_t value)
	{
		___pageCount_9 = value;
	}

	inline static int32_t get_offset_of_materialCount_10() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___materialCount_10)); }
	inline int32_t get_materialCount_10() const { return ___materialCount_10; }
	inline int32_t* get_address_of_materialCount_10() { return &___materialCount_10; }
	inline void set_materialCount_10(int32_t value)
	{
		___materialCount_10 = value;
	}

	inline static int32_t get_offset_of_characterInfo_11() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___characterInfo_11)); }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* get_characterInfo_11() const { return ___characterInfo_11; }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604** get_address_of_characterInfo_11() { return &___characterInfo_11; }
	inline void set_characterInfo_11(TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* value)
	{
		___characterInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___characterInfo_11), value);
	}

	inline static int32_t get_offset_of_wordInfo_12() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___wordInfo_12)); }
	inline TMP_WordInfoU5BU5D_t2C9C805935A8C8FFD43BF92C96AC70737AA52F09* get_wordInfo_12() const { return ___wordInfo_12; }
	inline TMP_WordInfoU5BU5D_t2C9C805935A8C8FFD43BF92C96AC70737AA52F09** get_address_of_wordInfo_12() { return &___wordInfo_12; }
	inline void set_wordInfo_12(TMP_WordInfoU5BU5D_t2C9C805935A8C8FFD43BF92C96AC70737AA52F09* value)
	{
		___wordInfo_12 = value;
		Il2CppCodeGenWriteBarrier((&___wordInfo_12), value);
	}

	inline static int32_t get_offset_of_linkInfo_13() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___linkInfo_13)); }
	inline TMP_LinkInfoU5BU5D_t5965804162EB43CD70F792B74DA179B32224BB0D* get_linkInfo_13() const { return ___linkInfo_13; }
	inline TMP_LinkInfoU5BU5D_t5965804162EB43CD70F792B74DA179B32224BB0D** get_address_of_linkInfo_13() { return &___linkInfo_13; }
	inline void set_linkInfo_13(TMP_LinkInfoU5BU5D_t5965804162EB43CD70F792B74DA179B32224BB0D* value)
	{
		___linkInfo_13 = value;
		Il2CppCodeGenWriteBarrier((&___linkInfo_13), value);
	}

	inline static int32_t get_offset_of_lineInfo_14() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___lineInfo_14)); }
	inline TMP_LineInfoU5BU5D_t3D5D11E746B537C3951927E490B7A1BAB9C23A5C* get_lineInfo_14() const { return ___lineInfo_14; }
	inline TMP_LineInfoU5BU5D_t3D5D11E746B537C3951927E490B7A1BAB9C23A5C** get_address_of_lineInfo_14() { return &___lineInfo_14; }
	inline void set_lineInfo_14(TMP_LineInfoU5BU5D_t3D5D11E746B537C3951927E490B7A1BAB9C23A5C* value)
	{
		___lineInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_14), value);
	}

	inline static int32_t get_offset_of_pageInfo_15() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___pageInfo_15)); }
	inline TMP_PageInfoU5BU5D_tFB7F7AD2CD9ADBE07099C1A06170B51AA8D9D847* get_pageInfo_15() const { return ___pageInfo_15; }
	inline TMP_PageInfoU5BU5D_tFB7F7AD2CD9ADBE07099C1A06170B51AA8D9D847** get_address_of_pageInfo_15() { return &___pageInfo_15; }
	inline void set_pageInfo_15(TMP_PageInfoU5BU5D_tFB7F7AD2CD9ADBE07099C1A06170B51AA8D9D847* value)
	{
		___pageInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___pageInfo_15), value);
	}

	inline static int32_t get_offset_of_meshInfo_16() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___meshInfo_16)); }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* get_meshInfo_16() const { return ___meshInfo_16; }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9** get_address_of_meshInfo_16() { return &___meshInfo_16; }
	inline void set_meshInfo_16(TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* value)
	{
		___meshInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___meshInfo_16), value);
	}

	inline static int32_t get_offset_of_m_CachedMeshInfo_17() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181, ___m_CachedMeshInfo_17)); }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* get_m_CachedMeshInfo_17() const { return ___m_CachedMeshInfo_17; }
	inline TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9** get_address_of_m_CachedMeshInfo_17() { return &___m_CachedMeshInfo_17; }
	inline void set_m_CachedMeshInfo_17(TMP_MeshInfoU5BU5D_t7F7564862ADABD75DAD9B09FF274591F807FFDE9* value)
	{
		___m_CachedMeshInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CachedMeshInfo_17), value);
	}
};

struct TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181_StaticFields
{
public:
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorPositive
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_InfinityVectorPositive_0;
	// UnityEngine.Vector2 TMPro.TMP_TextInfo::k_InfinityVectorNegative
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_InfinityVectorNegative_1;

public:
	inline static int32_t get_offset_of_k_InfinityVectorPositive_0() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181_StaticFields, ___k_InfinityVectorPositive_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_InfinityVectorPositive_0() const { return ___k_InfinityVectorPositive_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_InfinityVectorPositive_0() { return &___k_InfinityVectorPositive_0; }
	inline void set_k_InfinityVectorPositive_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_InfinityVectorPositive_0 = value;
	}

	inline static int32_t get_offset_of_k_InfinityVectorNegative_1() { return static_cast<int32_t>(offsetof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181_StaticFields, ___k_InfinityVectorNegative_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_InfinityVectorNegative_1() const { return ___k_InfinityVectorNegative_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_InfinityVectorNegative_1() { return &___k_InfinityVectorNegative_1; }
	inline void set_k_InfinityVectorNegative_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_InfinityVectorNegative_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTINFO_TC40DAAB47C5BD5AD21B3F456D860474D96D9C181_H
#ifndef LINESEGMENT_T27893280FED499A0FC1183D56B9F8BC56D42D84D_H
#define LINESEGMENT_T27893280FED499A0FC1183D56B9F8BC56D42D84D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextUtilities_LineSegment
struct  LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities_LineSegment::Point1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Point1_0;
	// UnityEngine.Vector3 TMPro.TMP_TextUtilities_LineSegment::Point2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Point2_1;

public:
	inline static int32_t get_offset_of_Point1_0() { return static_cast<int32_t>(offsetof(LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D, ___Point1_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Point1_0() const { return ___Point1_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Point1_0() { return &___Point1_0; }
	inline void set_Point1_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Point1_0 = value;
	}

	inline static int32_t get_offset_of_Point2_1() { return static_cast<int32_t>(offsetof(LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D, ___Point2_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Point2_1() const { return ___Point2_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Point2_1() { return &___Point2_1; }
	inline void set_Point2_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Point2_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESEGMENT_T27893280FED499A0FC1183D56B9F8BC56D42D84D_H
#ifndef TMP_VERTEX_T4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0_H
#define TMP_VERTEX_T4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Vertex
struct  TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 
{
public:
	// UnityEngine.Vector3 TMPro.TMP_Vertex::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv_1;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv2_2;
	// UnityEngine.Vector2 TMPro.TMP_Vertex::uv4
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___uv4_3;
	// UnityEngine.Color32 TMPro.TMP_Vertex::color
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_4;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_uv_1() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___uv_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv_1() const { return ___uv_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv_1() { return &___uv_1; }
	inline void set_uv_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv_1 = value;
	}

	inline static int32_t get_offset_of_uv2_2() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___uv2_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv2_2() const { return ___uv2_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv2_2() { return &___uv2_2; }
	inline void set_uv2_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv2_2 = value;
	}

	inline static int32_t get_offset_of_uv4_3() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___uv4_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_uv4_3() const { return ___uv4_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_uv4_3() { return &___uv4_3; }
	inline void set_uv4_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___uv4_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0, ___color_4)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_color_4() const { return ___color_4; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEX_T4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0_H
#ifndef TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#define TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_VertexDataUpdateFlags
struct  TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142 
{
public:
	// System.Int32 TMPro.TMP_VertexDataUpdateFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_VERTEXDATAUPDATEFLAGS_TBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142_H
#ifndef TAGUNITTYPE_T5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF_H
#define TAGUNITTYPE_T5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagUnitType
struct  TagUnitType_t5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF 
{
public:
	// System.Int32 TMPro.TagUnitType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TagUnitType_t5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGUNITTYPE_T5F2B8EA2F25FEA0BAEC4A0151C29BD7D262553CF_H
#ifndef TAGVALUETYPE_TB0AE4FE83F0293DDD337886C8D5268947F25F5CC_H
#define TAGVALUETYPE_TB0AE4FE83F0293DDD337886C8D5268947F25F5CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TagValueType
struct  TagValueType_tB0AE4FE83F0293DDD337886C8D5268947F25F5CC 
{
public:
	// System.Int32 TMPro.TagValueType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TagValueType_tB0AE4FE83F0293DDD337886C8D5268947F25F5CC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGVALUETYPE_TB0AE4FE83F0293DDD337886C8D5268947F25F5CC_H
#ifndef TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#define TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextAlignmentOptions
struct  TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337 
{
public:
	// System.Int32 TMPro.TextAlignmentOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTALIGNMENTOPTIONS_T4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337_H
#ifndef TEXTELEMENTTYPE_T3C95010E28DAFD09E9C361EEB679937475CEE857_H
#define TEXTELEMENTTYPE_T3C95010E28DAFD09E9C361EEB679937475CEE857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextElementType
struct  TextElementType_t3C95010E28DAFD09E9C361EEB679937475CEE857 
{
public:
	// System.Byte TMPro.TextElementType::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextElementType_t3C95010E28DAFD09E9C361EEB679937475CEE857, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTELEMENTTYPE_T3C95010E28DAFD09E9C361EEB679937475CEE857_H
#ifndef TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#define TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextOverflowModes
struct  TextOverflowModes_tC4F820014333ECAF4D52B02F75171FD9E52B9D76 
{
public:
	// System.Int32 TMPro.TextOverflowModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextOverflowModes_tC4F820014333ECAF4D52B02F75171FD9E52B9D76, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTOVERFLOWMODES_TC4F820014333ECAF4D52B02F75171FD9E52B9D76_H
#ifndef TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#define TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextRenderFlags
struct  TextRenderFlags_t29165355D5674BAEF40359B740631503FA9C0B56 
{
public:
	// System.Int32 TMPro.TextRenderFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextRenderFlags_t29165355D5674BAEF40359B740631503FA9C0B56, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTRENDERFLAGS_T29165355D5674BAEF40359B740631503FA9C0B56_H
#ifndef TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#define TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextureMappingOptions
struct  TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10 
{
public:
	// System.Int32 TMPro.TextureMappingOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREMAPPINGOPTIONS_TAC77A218D6DF5F386DA38AEAF3D9C943F084BD10_H
#ifndef VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#define VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexGradient
struct  VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A 
{
public:
	// UnityEngine.Color TMPro.VertexGradient::topLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topLeft_0;
	// UnityEngine.Color TMPro.VertexGradient::topRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___topRight_1;
	// UnityEngine.Color TMPro.VertexGradient::bottomLeft
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomLeft_2;
	// UnityEngine.Color TMPro.VertexGradient::bottomRight
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___bottomRight_3;

public:
	inline static int32_t get_offset_of_topLeft_0() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topLeft_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topLeft_0() const { return ___topLeft_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topLeft_0() { return &___topLeft_0; }
	inline void set_topLeft_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topLeft_0 = value;
	}

	inline static int32_t get_offset_of_topRight_1() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___topRight_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_topRight_1() const { return ___topRight_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_topRight_1() { return &___topRight_1; }
	inline void set_topRight_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___topRight_1 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_2() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomLeft_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomLeft_2() const { return ___bottomLeft_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomLeft_2() { return &___bottomLeft_2; }
	inline void set_bottomLeft_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomLeft_2 = value;
	}

	inline static int32_t get_offset_of_bottomRight_3() { return static_cast<int32_t>(offsetof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A, ___bottomRight_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_bottomRight_3() const { return ___bottomRight_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_bottomRight_3() { return &___bottomRight_3; }
	inline void set_bottomRight_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___bottomRight_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXGRADIENT_TDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A_H
#ifndef VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#define VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.VertexSortingOrder
struct  VertexSortingOrder_t2571FF911BB69CC1CC229DF12DE68568E3F850E5 
{
public:
	// System.Int32 TMPro.VertexSortingOrder::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VertexSortingOrder_t2571FF911BB69CC1CC229DF12DE68568E3F850E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXSORTINGORDER_T2571FF911BB69CC1CC229DF12DE68568E3F850E5_H
#ifndef _HORIZONTALALIGNMENTOPTIONS_TAB8CD906738706E3798D087C5C993333187F8882_H
#define _HORIZONTALALIGNMENTOPTIONS_TAB8CD906738706E3798D087C5C993333187F8882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro._HorizontalAlignmentOptions
struct  _HorizontalAlignmentOptions_tAB8CD906738706E3798D087C5C993333187F8882 
{
public:
	// System.Int32 TMPro._HorizontalAlignmentOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(_HorizontalAlignmentOptions_tAB8CD906738706E3798D087C5C993333187F8882, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _HORIZONTALALIGNMENTOPTIONS_TAB8CD906738706E3798D087C5C993333187F8882_H
#ifndef _VERTICALALIGNMENTOPTIONS_T1E4A066C44CB004C8AA7F96631DE179C296CC20C_H
#define _VERTICALALIGNMENTOPTIONS_T1E4A066C44CB004C8AA7F96631DE179C296CC20C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro._VerticalAlignmentOptions
struct  _VerticalAlignmentOptions_t1E4A066C44CB004C8AA7F96631DE179C296CC20C 
{
public:
	// System.Int32 TMPro._VerticalAlignmentOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(_VerticalAlignmentOptions_t1E4A066C44CB004C8AA7F96631DE179C296CC20C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _VERTICALALIGNMENTOPTIONS_T1E4A066C44CB004C8AA7F96631DE179C296CC20C_H
#ifndef BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#define BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890, ___m_Extents_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_TA2716F5212749C61B0E7B7B77E0CD3D79B742890_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef GLYPH_T64B599C17F15D84707C46FE272E98B347E1283B4_H
#define GLYPH_T64B599C17F15D84707C46FE272E98B347E1283B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextCore.Glyph
struct  Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4  : public RuntimeObject
{
public:
	// System.UInt32 UnityEngine.TextCore.Glyph::m_Index
	uint32_t ___m_Index_0;
	// UnityEngine.TextCore.GlyphMetrics UnityEngine.TextCore.Glyph::m_Metrics
	GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB  ___m_Metrics_1;
	// UnityEngine.TextCore.GlyphRect UnityEngine.TextCore.Glyph::m_GlyphRect
	GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  ___m_GlyphRect_2;
	// System.Single UnityEngine.TextCore.Glyph::m_Scale
	float ___m_Scale_3;
	// System.Int32 UnityEngine.TextCore.Glyph::m_AtlasIndex
	int32_t ___m_AtlasIndex_4;

public:
	inline static int32_t get_offset_of_m_Index_0() { return static_cast<int32_t>(offsetof(Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4, ___m_Index_0)); }
	inline uint32_t get_m_Index_0() const { return ___m_Index_0; }
	inline uint32_t* get_address_of_m_Index_0() { return &___m_Index_0; }
	inline void set_m_Index_0(uint32_t value)
	{
		___m_Index_0 = value;
	}

	inline static int32_t get_offset_of_m_Metrics_1() { return static_cast<int32_t>(offsetof(Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4, ___m_Metrics_1)); }
	inline GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB  get_m_Metrics_1() const { return ___m_Metrics_1; }
	inline GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB * get_address_of_m_Metrics_1() { return &___m_Metrics_1; }
	inline void set_m_Metrics_1(GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB  value)
	{
		___m_Metrics_1 = value;
	}

	inline static int32_t get_offset_of_m_GlyphRect_2() { return static_cast<int32_t>(offsetof(Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4, ___m_GlyphRect_2)); }
	inline GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  get_m_GlyphRect_2() const { return ___m_GlyphRect_2; }
	inline GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C * get_address_of_m_GlyphRect_2() { return &___m_GlyphRect_2; }
	inline void set_m_GlyphRect_2(GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  value)
	{
		___m_GlyphRect_2 = value;
	}

	inline static int32_t get_offset_of_m_Scale_3() { return static_cast<int32_t>(offsetof(Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4, ___m_Scale_3)); }
	inline float get_m_Scale_3() const { return ___m_Scale_3; }
	inline float* get_address_of_m_Scale_3() { return &___m_Scale_3; }
	inline void set_m_Scale_3(float value)
	{
		___m_Scale_3 = value;
	}

	inline static int32_t get_offset_of_m_AtlasIndex_4() { return static_cast<int32_t>(offsetof(Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4, ___m_AtlasIndex_4)); }
	inline int32_t get_m_AtlasIndex_4() const { return ___m_AtlasIndex_4; }
	inline int32_t* get_address_of_m_AtlasIndex_4() { return &___m_AtlasIndex_4; }
	inline void set_m_AtlasIndex_4(int32_t value)
	{
		___m_AtlasIndex_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TextCore.Glyph
struct Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4_marshaled_pinvoke
{
	uint32_t ___m_Index_0;
	GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB  ___m_Metrics_1;
	GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  ___m_GlyphRect_2;
	float ___m_Scale_3;
	int32_t ___m_AtlasIndex_4;
};
// Native definition for COM marshalling of UnityEngine.TextCore.Glyph
struct Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4_marshaled_com
{
	uint32_t ___m_Index_0;
	GlyphMetrics_t1CEF63AFDC4C55F3A8AF76BF32542B638C5608CB  ___m_Metrics_1;
	GlyphRect_t398045C795E0E1264236DFAA5712796CC23C3E7C  ___m_GlyphRect_2;
	float ___m_Scale_3;
	int32_t ___m_AtlasIndex_4;
};
#endif // GLYPH_T64B599C17F15D84707C46FE272E98B347E1283B4_H
#ifndef OBSCUREDPREFS_TECD4A8C13FF0745D8C958CDA7541FD55AB775C75_H
#define OBSCUREDPREFS_TECD4A8C13FF0745D8C958CDA7541FD55AB775C75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Storage.ObscuredPrefs
struct  ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75  : public RuntimeObject
{
public:

public:
};

struct ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields
{
public:
	// System.Boolean CodeStage.AntiCheat.Storage.ObscuredPrefs::alterationReported
	bool ___alterationReported_4;
	// System.Boolean CodeStage.AntiCheat.Storage.ObscuredPrefs::foreignSavesReported
	bool ___foreignSavesReported_5;
	// System.String CodeStage.AntiCheat.Storage.ObscuredPrefs::deviceId
	String_t* ___deviceId_6;
	// System.String CodeStage.AntiCheat.Storage.ObscuredPrefs::cryptoKeyObsolete
	String_t* ___cryptoKeyObsolete_7;
	// System.String CodeStage.AntiCheat.Storage.ObscuredPrefs::cryptoKeyObsoleteForMigration
	String_t* ___cryptoKeyObsoleteForMigration_8;
	// System.UInt32 CodeStage.AntiCheat.Storage.ObscuredPrefs::deviceIdHash
	uint32_t ___deviceIdHash_9;
	// System.Action CodeStage.AntiCheat.Storage.ObscuredPrefs::OnAlterationDetected
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnAlterationDetected_10;
	// System.Action CodeStage.AntiCheat.Storage.ObscuredPrefs::OnPossibleForeignSavesDetected
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnPossibleForeignSavesDetected_11;
	// System.Boolean CodeStage.AntiCheat.Storage.ObscuredPrefs::preservePlayerPrefs
	bool ___preservePlayerPrefs_12;
	// CodeStage.AntiCheat.Storage.ObscuredPrefs_DeviceLockLevel CodeStage.AntiCheat.Storage.ObscuredPrefs::lockToDevice
	uint8_t ___lockToDevice_13;
	// System.Boolean CodeStage.AntiCheat.Storage.ObscuredPrefs::readForeignSaves
	bool ___readForeignSaves_14;
	// System.Boolean CodeStage.AntiCheat.Storage.ObscuredPrefs::emergencyMode
	bool ___emergencyMode_15;
	// System.Type CodeStage.AntiCheat.Storage.ObscuredPrefs::IntType
	Type_t * ___IntType_16;
	// System.Type CodeStage.AntiCheat.Storage.ObscuredPrefs::UIntType
	Type_t * ___UIntType_17;
	// System.Type CodeStage.AntiCheat.Storage.ObscuredPrefs::StringType
	Type_t * ___StringType_18;
	// System.Type CodeStage.AntiCheat.Storage.ObscuredPrefs::FloatType
	Type_t * ___FloatType_19;
	// System.Type CodeStage.AntiCheat.Storage.ObscuredPrefs::DoubleType
	Type_t * ___DoubleType_20;
	// System.Type CodeStage.AntiCheat.Storage.ObscuredPrefs::DecimalType
	Type_t * ___DecimalType_21;
	// System.Type CodeStage.AntiCheat.Storage.ObscuredPrefs::LongType
	Type_t * ___LongType_22;
	// System.Type CodeStage.AntiCheat.Storage.ObscuredPrefs::ULongType
	Type_t * ___ULongType_23;
	// System.Type CodeStage.AntiCheat.Storage.ObscuredPrefs::BoolType
	Type_t * ___BoolType_24;
	// System.Char[] CodeStage.AntiCheat.Storage.ObscuredPrefs::generatedCryptoKey
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___generatedCryptoKey_25;
	// System.Boolean CodeStage.AntiCheat.Storage.ObscuredPrefs::migratingFromACTkV1
	bool ___migratingFromACTkV1_26;

public:
	inline static int32_t get_offset_of_alterationReported_4() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___alterationReported_4)); }
	inline bool get_alterationReported_4() const { return ___alterationReported_4; }
	inline bool* get_address_of_alterationReported_4() { return &___alterationReported_4; }
	inline void set_alterationReported_4(bool value)
	{
		___alterationReported_4 = value;
	}

	inline static int32_t get_offset_of_foreignSavesReported_5() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___foreignSavesReported_5)); }
	inline bool get_foreignSavesReported_5() const { return ___foreignSavesReported_5; }
	inline bool* get_address_of_foreignSavesReported_5() { return &___foreignSavesReported_5; }
	inline void set_foreignSavesReported_5(bool value)
	{
		___foreignSavesReported_5 = value;
	}

	inline static int32_t get_offset_of_deviceId_6() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___deviceId_6)); }
	inline String_t* get_deviceId_6() const { return ___deviceId_6; }
	inline String_t** get_address_of_deviceId_6() { return &___deviceId_6; }
	inline void set_deviceId_6(String_t* value)
	{
		___deviceId_6 = value;
		Il2CppCodeGenWriteBarrier((&___deviceId_6), value);
	}

	inline static int32_t get_offset_of_cryptoKeyObsolete_7() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___cryptoKeyObsolete_7)); }
	inline String_t* get_cryptoKeyObsolete_7() const { return ___cryptoKeyObsolete_7; }
	inline String_t** get_address_of_cryptoKeyObsolete_7() { return &___cryptoKeyObsolete_7; }
	inline void set_cryptoKeyObsolete_7(String_t* value)
	{
		___cryptoKeyObsolete_7 = value;
		Il2CppCodeGenWriteBarrier((&___cryptoKeyObsolete_7), value);
	}

	inline static int32_t get_offset_of_cryptoKeyObsoleteForMigration_8() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___cryptoKeyObsoleteForMigration_8)); }
	inline String_t* get_cryptoKeyObsoleteForMigration_8() const { return ___cryptoKeyObsoleteForMigration_8; }
	inline String_t** get_address_of_cryptoKeyObsoleteForMigration_8() { return &___cryptoKeyObsoleteForMigration_8; }
	inline void set_cryptoKeyObsoleteForMigration_8(String_t* value)
	{
		___cryptoKeyObsoleteForMigration_8 = value;
		Il2CppCodeGenWriteBarrier((&___cryptoKeyObsoleteForMigration_8), value);
	}

	inline static int32_t get_offset_of_deviceIdHash_9() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___deviceIdHash_9)); }
	inline uint32_t get_deviceIdHash_9() const { return ___deviceIdHash_9; }
	inline uint32_t* get_address_of_deviceIdHash_9() { return &___deviceIdHash_9; }
	inline void set_deviceIdHash_9(uint32_t value)
	{
		___deviceIdHash_9 = value;
	}

	inline static int32_t get_offset_of_OnAlterationDetected_10() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___OnAlterationDetected_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnAlterationDetected_10() const { return ___OnAlterationDetected_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnAlterationDetected_10() { return &___OnAlterationDetected_10; }
	inline void set_OnAlterationDetected_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnAlterationDetected_10 = value;
		Il2CppCodeGenWriteBarrier((&___OnAlterationDetected_10), value);
	}

	inline static int32_t get_offset_of_OnPossibleForeignSavesDetected_11() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___OnPossibleForeignSavesDetected_11)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnPossibleForeignSavesDetected_11() const { return ___OnPossibleForeignSavesDetected_11; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnPossibleForeignSavesDetected_11() { return &___OnPossibleForeignSavesDetected_11; }
	inline void set_OnPossibleForeignSavesDetected_11(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnPossibleForeignSavesDetected_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnPossibleForeignSavesDetected_11), value);
	}

	inline static int32_t get_offset_of_preservePlayerPrefs_12() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___preservePlayerPrefs_12)); }
	inline bool get_preservePlayerPrefs_12() const { return ___preservePlayerPrefs_12; }
	inline bool* get_address_of_preservePlayerPrefs_12() { return &___preservePlayerPrefs_12; }
	inline void set_preservePlayerPrefs_12(bool value)
	{
		___preservePlayerPrefs_12 = value;
	}

	inline static int32_t get_offset_of_lockToDevice_13() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___lockToDevice_13)); }
	inline uint8_t get_lockToDevice_13() const { return ___lockToDevice_13; }
	inline uint8_t* get_address_of_lockToDevice_13() { return &___lockToDevice_13; }
	inline void set_lockToDevice_13(uint8_t value)
	{
		___lockToDevice_13 = value;
	}

	inline static int32_t get_offset_of_readForeignSaves_14() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___readForeignSaves_14)); }
	inline bool get_readForeignSaves_14() const { return ___readForeignSaves_14; }
	inline bool* get_address_of_readForeignSaves_14() { return &___readForeignSaves_14; }
	inline void set_readForeignSaves_14(bool value)
	{
		___readForeignSaves_14 = value;
	}

	inline static int32_t get_offset_of_emergencyMode_15() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___emergencyMode_15)); }
	inline bool get_emergencyMode_15() const { return ___emergencyMode_15; }
	inline bool* get_address_of_emergencyMode_15() { return &___emergencyMode_15; }
	inline void set_emergencyMode_15(bool value)
	{
		___emergencyMode_15 = value;
	}

	inline static int32_t get_offset_of_IntType_16() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___IntType_16)); }
	inline Type_t * get_IntType_16() const { return ___IntType_16; }
	inline Type_t ** get_address_of_IntType_16() { return &___IntType_16; }
	inline void set_IntType_16(Type_t * value)
	{
		___IntType_16 = value;
		Il2CppCodeGenWriteBarrier((&___IntType_16), value);
	}

	inline static int32_t get_offset_of_UIntType_17() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___UIntType_17)); }
	inline Type_t * get_UIntType_17() const { return ___UIntType_17; }
	inline Type_t ** get_address_of_UIntType_17() { return &___UIntType_17; }
	inline void set_UIntType_17(Type_t * value)
	{
		___UIntType_17 = value;
		Il2CppCodeGenWriteBarrier((&___UIntType_17), value);
	}

	inline static int32_t get_offset_of_StringType_18() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___StringType_18)); }
	inline Type_t * get_StringType_18() const { return ___StringType_18; }
	inline Type_t ** get_address_of_StringType_18() { return &___StringType_18; }
	inline void set_StringType_18(Type_t * value)
	{
		___StringType_18 = value;
		Il2CppCodeGenWriteBarrier((&___StringType_18), value);
	}

	inline static int32_t get_offset_of_FloatType_19() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___FloatType_19)); }
	inline Type_t * get_FloatType_19() const { return ___FloatType_19; }
	inline Type_t ** get_address_of_FloatType_19() { return &___FloatType_19; }
	inline void set_FloatType_19(Type_t * value)
	{
		___FloatType_19 = value;
		Il2CppCodeGenWriteBarrier((&___FloatType_19), value);
	}

	inline static int32_t get_offset_of_DoubleType_20() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___DoubleType_20)); }
	inline Type_t * get_DoubleType_20() const { return ___DoubleType_20; }
	inline Type_t ** get_address_of_DoubleType_20() { return &___DoubleType_20; }
	inline void set_DoubleType_20(Type_t * value)
	{
		___DoubleType_20 = value;
		Il2CppCodeGenWriteBarrier((&___DoubleType_20), value);
	}

	inline static int32_t get_offset_of_DecimalType_21() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___DecimalType_21)); }
	inline Type_t * get_DecimalType_21() const { return ___DecimalType_21; }
	inline Type_t ** get_address_of_DecimalType_21() { return &___DecimalType_21; }
	inline void set_DecimalType_21(Type_t * value)
	{
		___DecimalType_21 = value;
		Il2CppCodeGenWriteBarrier((&___DecimalType_21), value);
	}

	inline static int32_t get_offset_of_LongType_22() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___LongType_22)); }
	inline Type_t * get_LongType_22() const { return ___LongType_22; }
	inline Type_t ** get_address_of_LongType_22() { return &___LongType_22; }
	inline void set_LongType_22(Type_t * value)
	{
		___LongType_22 = value;
		Il2CppCodeGenWriteBarrier((&___LongType_22), value);
	}

	inline static int32_t get_offset_of_ULongType_23() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___ULongType_23)); }
	inline Type_t * get_ULongType_23() const { return ___ULongType_23; }
	inline Type_t ** get_address_of_ULongType_23() { return &___ULongType_23; }
	inline void set_ULongType_23(Type_t * value)
	{
		___ULongType_23 = value;
		Il2CppCodeGenWriteBarrier((&___ULongType_23), value);
	}

	inline static int32_t get_offset_of_BoolType_24() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___BoolType_24)); }
	inline Type_t * get_BoolType_24() const { return ___BoolType_24; }
	inline Type_t ** get_address_of_BoolType_24() { return &___BoolType_24; }
	inline void set_BoolType_24(Type_t * value)
	{
		___BoolType_24 = value;
		Il2CppCodeGenWriteBarrier((&___BoolType_24), value);
	}

	inline static int32_t get_offset_of_generatedCryptoKey_25() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___generatedCryptoKey_25)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_generatedCryptoKey_25() const { return ___generatedCryptoKey_25; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_generatedCryptoKey_25() { return &___generatedCryptoKey_25; }
	inline void set_generatedCryptoKey_25(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___generatedCryptoKey_25 = value;
		Il2CppCodeGenWriteBarrier((&___generatedCryptoKey_25), value);
	}

	inline static int32_t get_offset_of_migratingFromACTkV1_26() { return static_cast<int32_t>(offsetof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields, ___migratingFromACTkV1_26)); }
	inline bool get_migratingFromACTkV1_26() const { return ___migratingFromACTkV1_26; }
	inline bool* get_address_of_migratingFromACTkV1_26() { return &___migratingFromACTkV1_26; }
	inline void set_migratingFromACTkV1_26(bool value)
	{
		___migratingFromACTkV1_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSCUREDPREFS_TECD4A8C13FF0745D8C958CDA7541FD55AB775C75_H
#ifndef CARETINFO_TA8526784E8AE82A9BA08B9D5C28483AE2416F708_H
#define CARETINFO_TA8526784E8AE82A9BA08B9D5C28483AE2416F708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.CaretInfo
struct  CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708 
{
public:
	// System.Int32 TMPro.CaretInfo::index
	int32_t ___index_0;
	// TMPro.CaretPosition TMPro.CaretInfo::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARETINFO_TA8526784E8AE82A9BA08B9D5C28483AE2416F708_H
#ifndef COMPUTE_DT_EVENTARGS_T17A56E99E621F8C211A13BE81BEAE18806DA7B11_H
#define COMPUTE_DT_EVENTARGS_T17A56E99E621F8C211A13BE81BEAE18806DA7B11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Compute_DT_EventArgs
struct  Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11  : public RuntimeObject
{
public:
	// TMPro.Compute_DistanceTransform_EventTypes TMPro.Compute_DT_EventArgs::EventType
	int32_t ___EventType_0;
	// System.Single TMPro.Compute_DT_EventArgs::ProgressPercentage
	float ___ProgressPercentage_1;
	// UnityEngine.Color[] TMPro.Compute_DT_EventArgs::Colors
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___Colors_2;

public:
	inline static int32_t get_offset_of_EventType_0() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11, ___EventType_0)); }
	inline int32_t get_EventType_0() const { return ___EventType_0; }
	inline int32_t* get_address_of_EventType_0() { return &___EventType_0; }
	inline void set_EventType_0(int32_t value)
	{
		___EventType_0 = value;
	}

	inline static int32_t get_offset_of_ProgressPercentage_1() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11, ___ProgressPercentage_1)); }
	inline float get_ProgressPercentage_1() const { return ___ProgressPercentage_1; }
	inline float* get_address_of_ProgressPercentage_1() { return &___ProgressPercentage_1; }
	inline void set_ProgressPercentage_1(float value)
	{
		___ProgressPercentage_1 = value;
	}

	inline static int32_t get_offset_of_Colors_2() { return static_cast<int32_t>(offsetof(Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11, ___Colors_2)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_Colors_2() const { return ___Colors_2; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_Colors_2() { return &___Colors_2; }
	inline void set_Colors_2(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___Colors_2 = value;
		Il2CppCodeGenWriteBarrier((&___Colors_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPUTE_DT_EVENTARGS_T17A56E99E621F8C211A13BE81BEAE18806DA7B11_H
#ifndef RICHTEXTTAGATTRIBUTE_T381E96CA7820A787C5D88B6DA0181DFA85ADBA98_H
#define RICHTEXTTAGATTRIBUTE_T381E96CA7820A787C5D88B6DA0181DFA85ADBA98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.RichTextTagAttribute
struct  RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98 
{
public:
	// System.Int32 TMPro.RichTextTagAttribute::nameHashCode
	int32_t ___nameHashCode_0;
	// System.Int32 TMPro.RichTextTagAttribute::valueHashCode
	int32_t ___valueHashCode_1;
	// TMPro.TagValueType TMPro.RichTextTagAttribute::valueType
	int32_t ___valueType_2;
	// System.Int32 TMPro.RichTextTagAttribute::valueStartIndex
	int32_t ___valueStartIndex_3;
	// System.Int32 TMPro.RichTextTagAttribute::valueLength
	int32_t ___valueLength_4;
	// TMPro.TagUnitType TMPro.RichTextTagAttribute::unitType
	int32_t ___unitType_5;

public:
	inline static int32_t get_offset_of_nameHashCode_0() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___nameHashCode_0)); }
	inline int32_t get_nameHashCode_0() const { return ___nameHashCode_0; }
	inline int32_t* get_address_of_nameHashCode_0() { return &___nameHashCode_0; }
	inline void set_nameHashCode_0(int32_t value)
	{
		___nameHashCode_0 = value;
	}

	inline static int32_t get_offset_of_valueHashCode_1() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___valueHashCode_1)); }
	inline int32_t get_valueHashCode_1() const { return ___valueHashCode_1; }
	inline int32_t* get_address_of_valueHashCode_1() { return &___valueHashCode_1; }
	inline void set_valueHashCode_1(int32_t value)
	{
		___valueHashCode_1 = value;
	}

	inline static int32_t get_offset_of_valueType_2() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___valueType_2)); }
	inline int32_t get_valueType_2() const { return ___valueType_2; }
	inline int32_t* get_address_of_valueType_2() { return &___valueType_2; }
	inline void set_valueType_2(int32_t value)
	{
		___valueType_2 = value;
	}

	inline static int32_t get_offset_of_valueStartIndex_3() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___valueStartIndex_3)); }
	inline int32_t get_valueStartIndex_3() const { return ___valueStartIndex_3; }
	inline int32_t* get_address_of_valueStartIndex_3() { return &___valueStartIndex_3; }
	inline void set_valueStartIndex_3(int32_t value)
	{
		___valueStartIndex_3 = value;
	}

	inline static int32_t get_offset_of_valueLength_4() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___valueLength_4)); }
	inline int32_t get_valueLength_4() const { return ___valueLength_4; }
	inline int32_t* get_address_of_valueLength_4() { return &___valueLength_4; }
	inline void set_valueLength_4(int32_t value)
	{
		___valueLength_4 = value;
	}

	inline static int32_t get_offset_of_unitType_5() { return static_cast<int32_t>(offsetof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98, ___unitType_5)); }
	inline int32_t get_unitType_5() const { return ___unitType_5; }
	inline int32_t* get_address_of_unitType_5() { return &___unitType_5; }
	inline void set_unitType_5(int32_t value)
	{
		___unitType_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RICHTEXTTAGATTRIBUTE_T381E96CA7820A787C5D88B6DA0181DFA85ADBA98_H
#ifndef TMP_CHARACTERINFO_T15C146F0B08EE44A63EC777AC32151D061AFFAF1_H
#define TMP_CHARACTERINFO_T15C146F0B08EE44A63EC777AC32151D061AFFAF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_CharacterInfo
struct  TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1 
{
public:
	// System.Char TMPro.TMP_CharacterInfo::character
	Il2CppChar ___character_0;
	// System.Int32 TMPro.TMP_CharacterInfo::index
	int32_t ___index_1;
	// System.Int32 TMPro.TMP_CharacterInfo::stringLength
	int32_t ___stringLength_2;
	// TMPro.TMP_TextElementType TMPro.TMP_CharacterInfo::elementType
	int32_t ___elementType_3;
	// TMPro.TMP_TextElement TMPro.TMP_CharacterInfo::textElement
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___textElement_4;
	// TMPro.TMP_FontAsset TMPro.TMP_CharacterInfo::fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_5;
	// TMPro.TMP_SpriteAsset TMPro.TMP_CharacterInfo::spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_6;
	// System.Int32 TMPro.TMP_CharacterInfo::spriteIndex
	int32_t ___spriteIndex_7;
	// UnityEngine.Material TMPro.TMP_CharacterInfo::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_8;
	// System.Int32 TMPro.TMP_CharacterInfo::materialReferenceIndex
	int32_t ___materialReferenceIndex_9;
	// System.Boolean TMPro.TMP_CharacterInfo::isUsingAlternateTypeface
	bool ___isUsingAlternateTypeface_10;
	// System.Single TMPro.TMP_CharacterInfo::pointSize
	float ___pointSize_11;
	// System.Int32 TMPro.TMP_CharacterInfo::lineNumber
	int32_t ___lineNumber_12;
	// System.Int32 TMPro.TMP_CharacterInfo::pageNumber
	int32_t ___pageNumber_13;
	// System.Int32 TMPro.TMP_CharacterInfo::vertexIndex
	int32_t ___vertexIndex_14;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BL
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BL_15;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TL
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TL_16;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_TR
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TR_17;
	// TMPro.TMP_Vertex TMPro.TMP_CharacterInfo::vertex_BR
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BR_18;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topLeft
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topLeft_19;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomLeft
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomLeft_20;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::topRight
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topRight_21;
	// UnityEngine.Vector3 TMPro.TMP_CharacterInfo::bottomRight
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRight_22;
	// System.Single TMPro.TMP_CharacterInfo::origin
	float ___origin_23;
	// System.Single TMPro.TMP_CharacterInfo::ascender
	float ___ascender_24;
	// System.Single TMPro.TMP_CharacterInfo::baseLine
	float ___baseLine_25;
	// System.Single TMPro.TMP_CharacterInfo::descender
	float ___descender_26;
	// System.Single TMPro.TMP_CharacterInfo::xAdvance
	float ___xAdvance_27;
	// System.Single TMPro.TMP_CharacterInfo::aspectRatio
	float ___aspectRatio_28;
	// System.Single TMPro.TMP_CharacterInfo::scale
	float ___scale_29;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::color
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_30;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_31;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_32;
	// UnityEngine.Color32 TMPro.TMP_CharacterInfo::highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_33;
	// TMPro.FontStyles TMPro.TMP_CharacterInfo::style
	int32_t ___style_34;
	// System.Boolean TMPro.TMP_CharacterInfo::isVisible
	bool ___isVisible_35;

public:
	inline static int32_t get_offset_of_character_0() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___character_0)); }
	inline Il2CppChar get_character_0() const { return ___character_0; }
	inline Il2CppChar* get_address_of_character_0() { return &___character_0; }
	inline void set_character_0(Il2CppChar value)
	{
		___character_0 = value;
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_stringLength_2() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___stringLength_2)); }
	inline int32_t get_stringLength_2() const { return ___stringLength_2; }
	inline int32_t* get_address_of_stringLength_2() { return &___stringLength_2; }
	inline void set_stringLength_2(int32_t value)
	{
		___stringLength_2 = value;
	}

	inline static int32_t get_offset_of_elementType_3() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___elementType_3)); }
	inline int32_t get_elementType_3() const { return ___elementType_3; }
	inline int32_t* get_address_of_elementType_3() { return &___elementType_3; }
	inline void set_elementType_3(int32_t value)
	{
		___elementType_3 = value;
	}

	inline static int32_t get_offset_of_textElement_4() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___textElement_4)); }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * get_textElement_4() const { return ___textElement_4; }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 ** get_address_of_textElement_4() { return &___textElement_4; }
	inline void set_textElement_4(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * value)
	{
		___textElement_4 = value;
		Il2CppCodeGenWriteBarrier((&___textElement_4), value);
	}

	inline static int32_t get_offset_of_fontAsset_5() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___fontAsset_5)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_fontAsset_5() const { return ___fontAsset_5; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_fontAsset_5() { return &___fontAsset_5; }
	inline void set_fontAsset_5(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___fontAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___fontAsset_5), value);
	}

	inline static int32_t get_offset_of_spriteAsset_6() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___spriteAsset_6)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_spriteAsset_6() const { return ___spriteAsset_6; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_spriteAsset_6() { return &___spriteAsset_6; }
	inline void set_spriteAsset_6(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___spriteAsset_6 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_6), value);
	}

	inline static int32_t get_offset_of_spriteIndex_7() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___spriteIndex_7)); }
	inline int32_t get_spriteIndex_7() const { return ___spriteIndex_7; }
	inline int32_t* get_address_of_spriteIndex_7() { return &___spriteIndex_7; }
	inline void set_spriteIndex_7(int32_t value)
	{
		___spriteIndex_7 = value;
	}

	inline static int32_t get_offset_of_material_8() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___material_8)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_8() const { return ___material_8; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_8() { return &___material_8; }
	inline void set_material_8(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_8 = value;
		Il2CppCodeGenWriteBarrier((&___material_8), value);
	}

	inline static int32_t get_offset_of_materialReferenceIndex_9() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___materialReferenceIndex_9)); }
	inline int32_t get_materialReferenceIndex_9() const { return ___materialReferenceIndex_9; }
	inline int32_t* get_address_of_materialReferenceIndex_9() { return &___materialReferenceIndex_9; }
	inline void set_materialReferenceIndex_9(int32_t value)
	{
		___materialReferenceIndex_9 = value;
	}

	inline static int32_t get_offset_of_isUsingAlternateTypeface_10() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___isUsingAlternateTypeface_10)); }
	inline bool get_isUsingAlternateTypeface_10() const { return ___isUsingAlternateTypeface_10; }
	inline bool* get_address_of_isUsingAlternateTypeface_10() { return &___isUsingAlternateTypeface_10; }
	inline void set_isUsingAlternateTypeface_10(bool value)
	{
		___isUsingAlternateTypeface_10 = value;
	}

	inline static int32_t get_offset_of_pointSize_11() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___pointSize_11)); }
	inline float get_pointSize_11() const { return ___pointSize_11; }
	inline float* get_address_of_pointSize_11() { return &___pointSize_11; }
	inline void set_pointSize_11(float value)
	{
		___pointSize_11 = value;
	}

	inline static int32_t get_offset_of_lineNumber_12() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___lineNumber_12)); }
	inline int32_t get_lineNumber_12() const { return ___lineNumber_12; }
	inline int32_t* get_address_of_lineNumber_12() { return &___lineNumber_12; }
	inline void set_lineNumber_12(int32_t value)
	{
		___lineNumber_12 = value;
	}

	inline static int32_t get_offset_of_pageNumber_13() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___pageNumber_13)); }
	inline int32_t get_pageNumber_13() const { return ___pageNumber_13; }
	inline int32_t* get_address_of_pageNumber_13() { return &___pageNumber_13; }
	inline void set_pageNumber_13(int32_t value)
	{
		___pageNumber_13 = value;
	}

	inline static int32_t get_offset_of_vertexIndex_14() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertexIndex_14)); }
	inline int32_t get_vertexIndex_14() const { return ___vertexIndex_14; }
	inline int32_t* get_address_of_vertexIndex_14() { return &___vertexIndex_14; }
	inline void set_vertexIndex_14(int32_t value)
	{
		___vertexIndex_14 = value;
	}

	inline static int32_t get_offset_of_vertex_BL_15() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_BL_15)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_BL_15() const { return ___vertex_BL_15; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_BL_15() { return &___vertex_BL_15; }
	inline void set_vertex_BL_15(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_BL_15 = value;
	}

	inline static int32_t get_offset_of_vertex_TL_16() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_TL_16)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_TL_16() const { return ___vertex_TL_16; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_TL_16() { return &___vertex_TL_16; }
	inline void set_vertex_TL_16(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_TL_16 = value;
	}

	inline static int32_t get_offset_of_vertex_TR_17() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_TR_17)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_TR_17() const { return ___vertex_TR_17; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_TR_17() { return &___vertex_TR_17; }
	inline void set_vertex_TR_17(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_TR_17 = value;
	}

	inline static int32_t get_offset_of_vertex_BR_18() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___vertex_BR_18)); }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  get_vertex_BR_18() const { return ___vertex_BR_18; }
	inline TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0 * get_address_of_vertex_BR_18() { return &___vertex_BR_18; }
	inline void set_vertex_BR_18(TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  value)
	{
		___vertex_BR_18 = value;
	}

	inline static int32_t get_offset_of_topLeft_19() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___topLeft_19)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_topLeft_19() const { return ___topLeft_19; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_topLeft_19() { return &___topLeft_19; }
	inline void set_topLeft_19(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___topLeft_19 = value;
	}

	inline static int32_t get_offset_of_bottomLeft_20() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___bottomLeft_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomLeft_20() const { return ___bottomLeft_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomLeft_20() { return &___bottomLeft_20; }
	inline void set_bottomLeft_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomLeft_20 = value;
	}

	inline static int32_t get_offset_of_topRight_21() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___topRight_21)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_topRight_21() const { return ___topRight_21; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_topRight_21() { return &___topRight_21; }
	inline void set_topRight_21(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___topRight_21 = value;
	}

	inline static int32_t get_offset_of_bottomRight_22() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___bottomRight_22)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_bottomRight_22() const { return ___bottomRight_22; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_bottomRight_22() { return &___bottomRight_22; }
	inline void set_bottomRight_22(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___bottomRight_22 = value;
	}

	inline static int32_t get_offset_of_origin_23() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___origin_23)); }
	inline float get_origin_23() const { return ___origin_23; }
	inline float* get_address_of_origin_23() { return &___origin_23; }
	inline void set_origin_23(float value)
	{
		___origin_23 = value;
	}

	inline static int32_t get_offset_of_ascender_24() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___ascender_24)); }
	inline float get_ascender_24() const { return ___ascender_24; }
	inline float* get_address_of_ascender_24() { return &___ascender_24; }
	inline void set_ascender_24(float value)
	{
		___ascender_24 = value;
	}

	inline static int32_t get_offset_of_baseLine_25() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___baseLine_25)); }
	inline float get_baseLine_25() const { return ___baseLine_25; }
	inline float* get_address_of_baseLine_25() { return &___baseLine_25; }
	inline void set_baseLine_25(float value)
	{
		___baseLine_25 = value;
	}

	inline static int32_t get_offset_of_descender_26() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___descender_26)); }
	inline float get_descender_26() const { return ___descender_26; }
	inline float* get_address_of_descender_26() { return &___descender_26; }
	inline void set_descender_26(float value)
	{
		___descender_26 = value;
	}

	inline static int32_t get_offset_of_xAdvance_27() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___xAdvance_27)); }
	inline float get_xAdvance_27() const { return ___xAdvance_27; }
	inline float* get_address_of_xAdvance_27() { return &___xAdvance_27; }
	inline void set_xAdvance_27(float value)
	{
		___xAdvance_27 = value;
	}

	inline static int32_t get_offset_of_aspectRatio_28() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___aspectRatio_28)); }
	inline float get_aspectRatio_28() const { return ___aspectRatio_28; }
	inline float* get_address_of_aspectRatio_28() { return &___aspectRatio_28; }
	inline void set_aspectRatio_28(float value)
	{
		___aspectRatio_28 = value;
	}

	inline static int32_t get_offset_of_scale_29() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___scale_29)); }
	inline float get_scale_29() const { return ___scale_29; }
	inline float* get_address_of_scale_29() { return &___scale_29; }
	inline void set_scale_29(float value)
	{
		___scale_29 = value;
	}

	inline static int32_t get_offset_of_color_30() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___color_30)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_color_30() const { return ___color_30; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_color_30() { return &___color_30; }
	inline void set_color_30(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___color_30 = value;
	}

	inline static int32_t get_offset_of_underlineColor_31() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___underlineColor_31)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_underlineColor_31() const { return ___underlineColor_31; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_underlineColor_31() { return &___underlineColor_31; }
	inline void set_underlineColor_31(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___underlineColor_31 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_32() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___strikethroughColor_32)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_strikethroughColor_32() const { return ___strikethroughColor_32; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_strikethroughColor_32() { return &___strikethroughColor_32; }
	inline void set_strikethroughColor_32(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___strikethroughColor_32 = value;
	}

	inline static int32_t get_offset_of_highlightColor_33() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___highlightColor_33)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_highlightColor_33() const { return ___highlightColor_33; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_highlightColor_33() { return &___highlightColor_33; }
	inline void set_highlightColor_33(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___highlightColor_33 = value;
	}

	inline static int32_t get_offset_of_style_34() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___style_34)); }
	inline int32_t get_style_34() const { return ___style_34; }
	inline int32_t* get_address_of_style_34() { return &___style_34; }
	inline void set_style_34(int32_t value)
	{
		___style_34 = value;
	}

	inline static int32_t get_offset_of_isVisible_35() { return static_cast<int32_t>(offsetof(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1, ___isVisible_35)); }
	inline bool get_isVisible_35() const { return ___isVisible_35; }
	inline bool* get_address_of_isVisible_35() { return &___isVisible_35; }
	inline void set_isVisible_35(bool value)
	{
		___isVisible_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1_marshaled_pinvoke
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___stringLength_2;
	int32_t ___elementType_3;
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___textElement_4;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_5;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_6;
	int32_t ___spriteIndex_7;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_8;
	int32_t ___materialReferenceIndex_9;
	int32_t ___isUsingAlternateTypeface_10;
	float ___pointSize_11;
	int32_t ___lineNumber_12;
	int32_t ___pageNumber_13;
	int32_t ___vertexIndex_14;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BL_15;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TL_16;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TR_17;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BR_18;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topLeft_19;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomLeft_20;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topRight_21;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRight_22;
	float ___origin_23;
	float ___ascender_24;
	float ___baseLine_25;
	float ___descender_26;
	float ___xAdvance_27;
	float ___aspectRatio_28;
	float ___scale_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_32;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_33;
	int32_t ___style_34;
	int32_t ___isVisible_35;
};
// Native definition for COM marshalling of TMPro.TMP_CharacterInfo
struct TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1_marshaled_com
{
	uint8_t ___character_0;
	int32_t ___index_1;
	int32_t ___stringLength_2;
	int32_t ___elementType_3;
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___textElement_4;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___fontAsset_5;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_6;
	int32_t ___spriteIndex_7;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_8;
	int32_t ___materialReferenceIndex_9;
	int32_t ___isUsingAlternateTypeface_10;
	float ___pointSize_11;
	int32_t ___lineNumber_12;
	int32_t ___pageNumber_13;
	int32_t ___vertexIndex_14;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BL_15;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TL_16;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_TR_17;
	TMP_Vertex_t4F9D3FA0EB3F5F4E94EC06582B857C3C23AC2EA0  ___vertex_BR_18;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topLeft_19;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomLeft_20;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___topRight_21;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___bottomRight_22;
	float ___origin_23;
	float ___ascender_24;
	float ___baseLine_25;
	float ___descender_26;
	float ___xAdvance_27;
	float ___aspectRatio_28;
	float ___scale_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___color_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_32;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_33;
	int32_t ___style_34;
	int32_t ___isVisible_35;
};
#endif // TMP_CHARACTERINFO_T15C146F0B08EE44A63EC777AC32151D061AFFAF1_H
#ifndef TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#define TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_LineInfo
struct  TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 
{
public:
	// System.Int32 TMPro.TMP_LineInfo::controlCharacterCount
	int32_t ___controlCharacterCount_0;
	// System.Int32 TMPro.TMP_LineInfo::characterCount
	int32_t ___characterCount_1;
	// System.Int32 TMPro.TMP_LineInfo::visibleCharacterCount
	int32_t ___visibleCharacterCount_2;
	// System.Int32 TMPro.TMP_LineInfo::spaceCount
	int32_t ___spaceCount_3;
	// System.Int32 TMPro.TMP_LineInfo::wordCount
	int32_t ___wordCount_4;
	// System.Int32 TMPro.TMP_LineInfo::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.TMP_LineInfo::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.TMP_LineInfo::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.TMP_LineInfo::lastVisibleCharacterIndex
	int32_t ___lastVisibleCharacterIndex_8;
	// System.Single TMPro.TMP_LineInfo::length
	float ___length_9;
	// System.Single TMPro.TMP_LineInfo::lineHeight
	float ___lineHeight_10;
	// System.Single TMPro.TMP_LineInfo::ascender
	float ___ascender_11;
	// System.Single TMPro.TMP_LineInfo::baseline
	float ___baseline_12;
	// System.Single TMPro.TMP_LineInfo::descender
	float ___descender_13;
	// System.Single TMPro.TMP_LineInfo::maxAdvance
	float ___maxAdvance_14;
	// System.Single TMPro.TMP_LineInfo::width
	float ___width_15;
	// System.Single TMPro.TMP_LineInfo::marginLeft
	float ___marginLeft_16;
	// System.Single TMPro.TMP_LineInfo::marginRight
	float ___marginRight_17;
	// TMPro.TextAlignmentOptions TMPro.TMP_LineInfo::alignment
	int32_t ___alignment_18;
	// TMPro.Extents TMPro.TMP_LineInfo::lineExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___lineExtents_19;

public:
	inline static int32_t get_offset_of_controlCharacterCount_0() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___controlCharacterCount_0)); }
	inline int32_t get_controlCharacterCount_0() const { return ___controlCharacterCount_0; }
	inline int32_t* get_address_of_controlCharacterCount_0() { return &___controlCharacterCount_0; }
	inline void set_controlCharacterCount_0(int32_t value)
	{
		___controlCharacterCount_0 = value;
	}

	inline static int32_t get_offset_of_characterCount_1() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___characterCount_1)); }
	inline int32_t get_characterCount_1() const { return ___characterCount_1; }
	inline int32_t* get_address_of_characterCount_1() { return &___characterCount_1; }
	inline void set_characterCount_1(int32_t value)
	{
		___characterCount_1 = value;
	}

	inline static int32_t get_offset_of_visibleCharacterCount_2() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___visibleCharacterCount_2)); }
	inline int32_t get_visibleCharacterCount_2() const { return ___visibleCharacterCount_2; }
	inline int32_t* get_address_of_visibleCharacterCount_2() { return &___visibleCharacterCount_2; }
	inline void set_visibleCharacterCount_2(int32_t value)
	{
		___visibleCharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_spaceCount_3() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___spaceCount_3)); }
	inline int32_t get_spaceCount_3() const { return ___spaceCount_3; }
	inline int32_t* get_address_of_spaceCount_3() { return &___spaceCount_3; }
	inline void set_spaceCount_3(int32_t value)
	{
		___spaceCount_3 = value;
	}

	inline static int32_t get_offset_of_wordCount_4() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___wordCount_4)); }
	inline int32_t get_wordCount_4() const { return ___wordCount_4; }
	inline int32_t* get_address_of_wordCount_4() { return &___wordCount_4; }
	inline void set_wordCount_4(int32_t value)
	{
		___wordCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharacterIndex_8() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lastVisibleCharacterIndex_8)); }
	inline int32_t get_lastVisibleCharacterIndex_8() const { return ___lastVisibleCharacterIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharacterIndex_8() { return &___lastVisibleCharacterIndex_8; }
	inline void set_lastVisibleCharacterIndex_8(int32_t value)
	{
		___lastVisibleCharacterIndex_8 = value;
	}

	inline static int32_t get_offset_of_length_9() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___length_9)); }
	inline float get_length_9() const { return ___length_9; }
	inline float* get_address_of_length_9() { return &___length_9; }
	inline void set_length_9(float value)
	{
		___length_9 = value;
	}

	inline static int32_t get_offset_of_lineHeight_10() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineHeight_10)); }
	inline float get_lineHeight_10() const { return ___lineHeight_10; }
	inline float* get_address_of_lineHeight_10() { return &___lineHeight_10; }
	inline void set_lineHeight_10(float value)
	{
		___lineHeight_10 = value;
	}

	inline static int32_t get_offset_of_ascender_11() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___ascender_11)); }
	inline float get_ascender_11() const { return ___ascender_11; }
	inline float* get_address_of_ascender_11() { return &___ascender_11; }
	inline void set_ascender_11(float value)
	{
		___ascender_11 = value;
	}

	inline static int32_t get_offset_of_baseline_12() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___baseline_12)); }
	inline float get_baseline_12() const { return ___baseline_12; }
	inline float* get_address_of_baseline_12() { return &___baseline_12; }
	inline void set_baseline_12(float value)
	{
		___baseline_12 = value;
	}

	inline static int32_t get_offset_of_descender_13() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___descender_13)); }
	inline float get_descender_13() const { return ___descender_13; }
	inline float* get_address_of_descender_13() { return &___descender_13; }
	inline void set_descender_13(float value)
	{
		___descender_13 = value;
	}

	inline static int32_t get_offset_of_maxAdvance_14() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___maxAdvance_14)); }
	inline float get_maxAdvance_14() const { return ___maxAdvance_14; }
	inline float* get_address_of_maxAdvance_14() { return &___maxAdvance_14; }
	inline void set_maxAdvance_14(float value)
	{
		___maxAdvance_14 = value;
	}

	inline static int32_t get_offset_of_width_15() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___width_15)); }
	inline float get_width_15() const { return ___width_15; }
	inline float* get_address_of_width_15() { return &___width_15; }
	inline void set_width_15(float value)
	{
		___width_15 = value;
	}

	inline static int32_t get_offset_of_marginLeft_16() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginLeft_16)); }
	inline float get_marginLeft_16() const { return ___marginLeft_16; }
	inline float* get_address_of_marginLeft_16() { return &___marginLeft_16; }
	inline void set_marginLeft_16(float value)
	{
		___marginLeft_16 = value;
	}

	inline static int32_t get_offset_of_marginRight_17() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___marginRight_17)); }
	inline float get_marginRight_17() const { return ___marginRight_17; }
	inline float* get_address_of_marginRight_17() { return &___marginRight_17; }
	inline void set_marginRight_17(float value)
	{
		___marginRight_17 = value;
	}

	inline static int32_t get_offset_of_alignment_18() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___alignment_18)); }
	inline int32_t get_alignment_18() const { return ___alignment_18; }
	inline int32_t* get_address_of_alignment_18() { return &___alignment_18; }
	inline void set_alignment_18(int32_t value)
	{
		___alignment_18 = value;
	}

	inline static int32_t get_offset_of_lineExtents_19() { return static_cast<int32_t>(offsetof(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442, ___lineExtents_19)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_lineExtents_19() const { return ___lineExtents_19; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_lineExtents_19() { return &___lineExtents_19; }
	inline void set_lineExtents_19(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___lineExtents_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_LINEINFO_TE89A82D872E55C3DDF29C4C8D862358633D0B442_H
#ifndef TMP_MESHINFO_T0140B4A33090360DC5CFB47CD8419369BBE3AD2E_H
#define TMP_MESHINFO_T0140B4A33090360DC5CFB47CD8419369BBE3AD2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MeshInfo
struct  TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E 
{
public:
	// UnityEngine.Mesh TMPro.TMP_MeshInfo::mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh_4;
	// System.Int32 TMPro.TMP_MeshInfo::vertexCount
	int32_t ___vertexCount_5;
	// UnityEngine.Vector3[] TMPro.TMP_MeshInfo::vertices
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___vertices_6;
	// UnityEngine.Vector3[] TMPro.TMP_MeshInfo::normals
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___normals_7;
	// UnityEngine.Vector4[] TMPro.TMP_MeshInfo::tangents
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ___tangents_8;
	// UnityEngine.Vector2[] TMPro.TMP_MeshInfo::uvs0
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___uvs0_9;
	// UnityEngine.Vector2[] TMPro.TMP_MeshInfo::uvs2
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___uvs2_10;
	// UnityEngine.Color32[] TMPro.TMP_MeshInfo::colors32
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___colors32_11;
	// System.Int32[] TMPro.TMP_MeshInfo::triangles
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___triangles_12;

public:
	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___mesh_4)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_mesh_4() const { return ___mesh_4; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_4), value);
	}

	inline static int32_t get_offset_of_vertexCount_5() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___vertexCount_5)); }
	inline int32_t get_vertexCount_5() const { return ___vertexCount_5; }
	inline int32_t* get_address_of_vertexCount_5() { return &___vertexCount_5; }
	inline void set_vertexCount_5(int32_t value)
	{
		___vertexCount_5 = value;
	}

	inline static int32_t get_offset_of_vertices_6() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___vertices_6)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_vertices_6() const { return ___vertices_6; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_vertices_6() { return &___vertices_6; }
	inline void set_vertices_6(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___vertices_6 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_6), value);
	}

	inline static int32_t get_offset_of_normals_7() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___normals_7)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_normals_7() const { return ___normals_7; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_normals_7() { return &___normals_7; }
	inline void set_normals_7(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___normals_7 = value;
		Il2CppCodeGenWriteBarrier((&___normals_7), value);
	}

	inline static int32_t get_offset_of_tangents_8() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___tangents_8)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get_tangents_8() const { return ___tangents_8; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of_tangents_8() { return &___tangents_8; }
	inline void set_tangents_8(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		___tangents_8 = value;
		Il2CppCodeGenWriteBarrier((&___tangents_8), value);
	}

	inline static int32_t get_offset_of_uvs0_9() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___uvs0_9)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_uvs0_9() const { return ___uvs0_9; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_uvs0_9() { return &___uvs0_9; }
	inline void set_uvs0_9(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___uvs0_9 = value;
		Il2CppCodeGenWriteBarrier((&___uvs0_9), value);
	}

	inline static int32_t get_offset_of_uvs2_10() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___uvs2_10)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_uvs2_10() const { return ___uvs2_10; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_uvs2_10() { return &___uvs2_10; }
	inline void set_uvs2_10(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___uvs2_10 = value;
		Il2CppCodeGenWriteBarrier((&___uvs2_10), value);
	}

	inline static int32_t get_offset_of_colors32_11() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___colors32_11)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_colors32_11() const { return ___colors32_11; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_colors32_11() { return &___colors32_11; }
	inline void set_colors32_11(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___colors32_11 = value;
		Il2CppCodeGenWriteBarrier((&___colors32_11), value);
	}

	inline static int32_t get_offset_of_triangles_12() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E, ___triangles_12)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_triangles_12() const { return ___triangles_12; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_triangles_12() { return &___triangles_12; }
	inline void set_triangles_12(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___triangles_12 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_12), value);
	}
};

struct TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_MeshInfo::s_DefaultColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___s_DefaultColor_0;
	// UnityEngine.Vector3 TMPro.TMP_MeshInfo::s_DefaultNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___s_DefaultNormal_1;
	// UnityEngine.Vector4 TMPro.TMP_MeshInfo::s_DefaultTangent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___s_DefaultTangent_2;
	// UnityEngine.Bounds TMPro.TMP_MeshInfo::s_DefaultBounds
	Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  ___s_DefaultBounds_3;

public:
	inline static int32_t get_offset_of_s_DefaultColor_0() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields, ___s_DefaultColor_0)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_s_DefaultColor_0() const { return ___s_DefaultColor_0; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_s_DefaultColor_0() { return &___s_DefaultColor_0; }
	inline void set_s_DefaultColor_0(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___s_DefaultColor_0 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_1() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields, ___s_DefaultNormal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_s_DefaultNormal_1() const { return ___s_DefaultNormal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_s_DefaultNormal_1() { return &___s_DefaultNormal_1; }
	inline void set_s_DefaultNormal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___s_DefaultNormal_1 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_2() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields, ___s_DefaultTangent_2)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_s_DefaultTangent_2() const { return ___s_DefaultTangent_2; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_s_DefaultTangent_2() { return &___s_DefaultTangent_2; }
	inline void set_s_DefaultTangent_2(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___s_DefaultTangent_2 = value;
	}

	inline static int32_t get_offset_of_s_DefaultBounds_3() { return static_cast<int32_t>(offsetof(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_StaticFields, ___s_DefaultBounds_3)); }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  get_s_DefaultBounds_3() const { return ___s_DefaultBounds_3; }
	inline Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890 * get_address_of_s_DefaultBounds_3() { return &___s_DefaultBounds_3; }
	inline void set_s_DefaultBounds_3(Bounds_tA2716F5212749C61B0E7B7B77E0CD3D79B742890  value)
	{
		___s_DefaultBounds_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.TMP_MeshInfo
struct TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_marshaled_pinvoke
{
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh_4;
	int32_t ___vertexCount_5;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___vertices_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___normals_7;
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * ___tangents_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * ___uvs0_9;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * ___uvs2_10;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * ___colors32_11;
	int32_t* ___triangles_12;
};
// Native definition for COM marshalling of TMPro.TMP_MeshInfo
struct TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E_marshaled_com
{
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___mesh_4;
	int32_t ___vertexCount_5;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___vertices_6;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * ___normals_7;
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * ___tangents_8;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * ___uvs0_9;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * ___uvs2_10;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * ___colors32_11;
	int32_t* ___triangles_12;
};
#endif // TMP_MESHINFO_T0140B4A33090360DC5CFB47CD8419369BBE3AD2E_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#define TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight>
struct  TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_ItemStack_0)); }
	inline FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(FontWeightU5BU5D_t7A186E8DAEB072A355A6CCC80B3FFD219E538446* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T9B6C6D23490A525AEA83F4301C7523574055098B_H
#ifndef TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#define TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions>
struct  TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 
{
public:
	// T[] TMPro.TMP_RichTextTagStack`1::m_ItemStack
	TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* ___m_ItemStack_0;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Index
	int32_t ___m_Index_1;
	// System.Int32 TMPro.TMP_RichTextTagStack`1::m_Capacity
	int32_t ___m_Capacity_2;
	// T TMPro.TMP_RichTextTagStack`1::m_DefaultItem
	int32_t ___m_DefaultItem_3;

public:
	inline static int32_t get_offset_of_m_ItemStack_0() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_ItemStack_0)); }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* get_m_ItemStack_0() const { return ___m_ItemStack_0; }
	inline TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B** get_address_of_m_ItemStack_0() { return &___m_ItemStack_0; }
	inline void set_m_ItemStack_0(TextAlignmentOptionsU5BU5D_t4AE8FA5E3D695ED64EBBCFBAF8C780A0EB0BD33B* value)
	{
		___m_ItemStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemStack_0), value);
	}

	inline static int32_t get_offset_of_m_Index_1() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_Index_1)); }
	inline int32_t get_m_Index_1() const { return ___m_Index_1; }
	inline int32_t* get_address_of_m_Index_1() { return &___m_Index_1; }
	inline void set_m_Index_1(int32_t value)
	{
		___m_Index_1 = value;
	}

	inline static int32_t get_offset_of_m_Capacity_2() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_Capacity_2)); }
	inline int32_t get_m_Capacity_2() const { return ___m_Capacity_2; }
	inline int32_t* get_address_of_m_Capacity_2() { return &___m_Capacity_2; }
	inline void set_m_Capacity_2(int32_t value)
	{
		___m_Capacity_2 = value;
	}

	inline static int32_t get_offset_of_m_DefaultItem_3() { return static_cast<int32_t>(offsetof(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115, ___m_DefaultItem_3)); }
	inline int32_t get_m_DefaultItem_3() const { return ___m_DefaultItem_3; }
	inline int32_t* get_address_of_m_DefaultItem_3() { return &___m_DefaultItem_3; }
	inline void set_m_DefaultItem_3(int32_t value)
	{
		___m_DefaultItem_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_RICHTEXTTAGSTACK_1_T435AA844A7DBDA7E54BCDA3C53622D4B28952115_H
#ifndef TMP_SPRITEGLYPH_T423E5984649351521A513FDF257D33C67116BF9B_H
#define TMP_SPRITEGLYPH_T423E5984649351521A513FDF257D33C67116BF9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteGlyph
struct  TMP_SpriteGlyph_t423E5984649351521A513FDF257D33C67116BF9B  : public Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4
{
public:
	// UnityEngine.Sprite TMPro.TMP_SpriteGlyph::sprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___sprite_5;

public:
	inline static int32_t get_offset_of_sprite_5() { return static_cast<int32_t>(offsetof(TMP_SpriteGlyph_t423E5984649351521A513FDF257D33C67116BF9B, ___sprite_5)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_sprite_5() const { return ___sprite_5; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_sprite_5() { return &___sprite_5; }
	inline void set_sprite_5(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___sprite_5 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEGLYPH_T423E5984649351521A513FDF257D33C67116BF9B_H
#ifndef TMP_TEXTELEMENT_TB9A6A361BB93487BD07DDDA37A368819DA46C344_H
#define TMP_TEXTELEMENT_TB9A6A361BB93487BD07DDDA37A368819DA46C344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_TextElement
struct  TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344  : public RuntimeObject
{
public:
	// TMPro.TextElementType TMPro.TMP_TextElement::m_ElementType
	uint8_t ___m_ElementType_0;
	// System.UInt32 TMPro.TMP_TextElement::m_Unicode
	uint32_t ___m_Unicode_1;
	// UnityEngine.TextCore.Glyph TMPro.TMP_TextElement::m_Glyph
	Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4 * ___m_Glyph_2;
	// System.UInt32 TMPro.TMP_TextElement::m_GlyphIndex
	uint32_t ___m_GlyphIndex_3;
	// System.Single TMPro.TMP_TextElement::m_Scale
	float ___m_Scale_4;

public:
	inline static int32_t get_offset_of_m_ElementType_0() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_ElementType_0)); }
	inline uint8_t get_m_ElementType_0() const { return ___m_ElementType_0; }
	inline uint8_t* get_address_of_m_ElementType_0() { return &___m_ElementType_0; }
	inline void set_m_ElementType_0(uint8_t value)
	{
		___m_ElementType_0 = value;
	}

	inline static int32_t get_offset_of_m_Unicode_1() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_Unicode_1)); }
	inline uint32_t get_m_Unicode_1() const { return ___m_Unicode_1; }
	inline uint32_t* get_address_of_m_Unicode_1() { return &___m_Unicode_1; }
	inline void set_m_Unicode_1(uint32_t value)
	{
		___m_Unicode_1 = value;
	}

	inline static int32_t get_offset_of_m_Glyph_2() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_Glyph_2)); }
	inline Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4 * get_m_Glyph_2() const { return ___m_Glyph_2; }
	inline Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4 ** get_address_of_m_Glyph_2() { return &___m_Glyph_2; }
	inline void set_m_Glyph_2(Glyph_t64B599C17F15D84707C46FE272E98B347E1283B4 * value)
	{
		___m_Glyph_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Glyph_2), value);
	}

	inline static int32_t get_offset_of_m_GlyphIndex_3() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_GlyphIndex_3)); }
	inline uint32_t get_m_GlyphIndex_3() const { return ___m_GlyphIndex_3; }
	inline uint32_t* get_address_of_m_GlyphIndex_3() { return &___m_GlyphIndex_3; }
	inline void set_m_GlyphIndex_3(uint32_t value)
	{
		___m_GlyphIndex_3 = value;
	}

	inline static int32_t get_offset_of_m_Scale_4() { return static_cast<int32_t>(offsetof(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344, ___m_Scale_4)); }
	inline float get_m_Scale_4() const { return ___m_Scale_4; }
	inline float* get_address_of_m_Scale_4() { return &___m_Scale_4; }
	inline void set_m_Scale_4(float value)
	{
		___m_Scale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXTELEMENT_TB9A6A361BB93487BD07DDDA37A368819DA46C344_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef TMP_ASSET_TE47F21E07C734D11D5DCEA5C0A0264465963CB2D_H
#define TMP_ASSET_TE47F21E07C734D11D5DCEA5C0A0264465963CB2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Asset
struct  TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Int32 TMPro.TMP_Asset::hashCode
	int32_t ___hashCode_4;
	// UnityEngine.Material TMPro.TMP_Asset::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_5;
	// System.Int32 TMPro.TMP_Asset::materialHashCode
	int32_t ___materialHashCode_6;

public:
	inline static int32_t get_offset_of_hashCode_4() { return static_cast<int32_t>(offsetof(TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D, ___hashCode_4)); }
	inline int32_t get_hashCode_4() const { return ___hashCode_4; }
	inline int32_t* get_address_of_hashCode_4() { return &___hashCode_4; }
	inline void set_hashCode_4(int32_t value)
	{
		___hashCode_4 = value;
	}

	inline static int32_t get_offset_of_material_5() { return static_cast<int32_t>(offsetof(TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D, ___material_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_5() const { return ___material_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_5() { return &___material_5; }
	inline void set_material_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_5 = value;
		Il2CppCodeGenWriteBarrier((&___material_5), value);
	}

	inline static int32_t get_offset_of_materialHashCode_6() { return static_cast<int32_t>(offsetof(TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D, ___materialHashCode_6)); }
	inline int32_t get_materialHashCode_6() const { return ___materialHashCode_6; }
	inline int32_t* get_address_of_materialHashCode_6() { return &___materialHashCode_6; }
	inline void set_materialHashCode_6(int32_t value)
	{
		___materialHashCode_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_ASSET_TE47F21E07C734D11D5DCEA5C0A0264465963CB2D_H
#ifndef U3CDOSPRITEANIMATIONINTERNALU3ED__7_T31ADE0DE24AE64EC437AED7B01D5749E9248C2AC_H
#define U3CDOSPRITEANIMATIONINTERNALU3ED__7_T31ADE0DE24AE64EC437AED7B01D5749E9248C2AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7
struct  U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC  : public RuntimeObject
{
public:
	// System.Int32 TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::<>4__this
	TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * ___U3CU3E4__this_2;
	// System.Int32 TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::start
	int32_t ___start_3;
	// System.Int32 TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::end
	int32_t ___end_4;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___spriteAsset_5;
	// System.Int32 TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::currentCharacter
	int32_t ___currentCharacter_6;
	// System.Int32 TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::framerate
	int32_t ___framerate_7;
	// System.Int32 TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::<currentFrame>5__2
	int32_t ___U3CcurrentFrameU3E5__2_8;
	// TMPro.TMP_CharacterInfo TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::<charInfo>5__3
	TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1  ___U3CcharInfoU3E5__3_9;
	// System.Int32 TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::<materialIndex>5__4
	int32_t ___U3CmaterialIndexU3E5__4_10;
	// System.Int32 TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::<vertexIndex>5__5
	int32_t ___U3CvertexIndexU3E5__5_11;
	// TMPro.TMP_MeshInfo TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::<meshInfo>5__6
	TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E  ___U3CmeshInfoU3E5__6_12;
	// System.Single TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::<elapsedTime>5__7
	float ___U3CelapsedTimeU3E5__7_13;
	// System.Single TMPro.TMP_SpriteAnimator_<DoSpriteAnimationInternal>d__7::<targetTime>5__8
	float ___U3CtargetTimeU3E5__8_14;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___U3CU3E4__this_2)); }
	inline TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_start_3() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___start_3)); }
	inline int32_t get_start_3() const { return ___start_3; }
	inline int32_t* get_address_of_start_3() { return &___start_3; }
	inline void set_start_3(int32_t value)
	{
		___start_3 = value;
	}

	inline static int32_t get_offset_of_end_4() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___end_4)); }
	inline int32_t get_end_4() const { return ___end_4; }
	inline int32_t* get_address_of_end_4() { return &___end_4; }
	inline void set_end_4(int32_t value)
	{
		___end_4 = value;
	}

	inline static int32_t get_offset_of_spriteAsset_5() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___spriteAsset_5)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_spriteAsset_5() const { return ___spriteAsset_5; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_spriteAsset_5() { return &___spriteAsset_5; }
	inline void set_spriteAsset_5(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___spriteAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___spriteAsset_5), value);
	}

	inline static int32_t get_offset_of_currentCharacter_6() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___currentCharacter_6)); }
	inline int32_t get_currentCharacter_6() const { return ___currentCharacter_6; }
	inline int32_t* get_address_of_currentCharacter_6() { return &___currentCharacter_6; }
	inline void set_currentCharacter_6(int32_t value)
	{
		___currentCharacter_6 = value;
	}

	inline static int32_t get_offset_of_framerate_7() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___framerate_7)); }
	inline int32_t get_framerate_7() const { return ___framerate_7; }
	inline int32_t* get_address_of_framerate_7() { return &___framerate_7; }
	inline void set_framerate_7(int32_t value)
	{
		___framerate_7 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentFrameU3E5__2_8() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___U3CcurrentFrameU3E5__2_8)); }
	inline int32_t get_U3CcurrentFrameU3E5__2_8() const { return ___U3CcurrentFrameU3E5__2_8; }
	inline int32_t* get_address_of_U3CcurrentFrameU3E5__2_8() { return &___U3CcurrentFrameU3E5__2_8; }
	inline void set_U3CcurrentFrameU3E5__2_8(int32_t value)
	{
		___U3CcurrentFrameU3E5__2_8 = value;
	}

	inline static int32_t get_offset_of_U3CcharInfoU3E5__3_9() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___U3CcharInfoU3E5__3_9)); }
	inline TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1  get_U3CcharInfoU3E5__3_9() const { return ___U3CcharInfoU3E5__3_9; }
	inline TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1 * get_address_of_U3CcharInfoU3E5__3_9() { return &___U3CcharInfoU3E5__3_9; }
	inline void set_U3CcharInfoU3E5__3_9(TMP_CharacterInfo_t15C146F0B08EE44A63EC777AC32151D061AFFAF1  value)
	{
		___U3CcharInfoU3E5__3_9 = value;
	}

	inline static int32_t get_offset_of_U3CmaterialIndexU3E5__4_10() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___U3CmaterialIndexU3E5__4_10)); }
	inline int32_t get_U3CmaterialIndexU3E5__4_10() const { return ___U3CmaterialIndexU3E5__4_10; }
	inline int32_t* get_address_of_U3CmaterialIndexU3E5__4_10() { return &___U3CmaterialIndexU3E5__4_10; }
	inline void set_U3CmaterialIndexU3E5__4_10(int32_t value)
	{
		___U3CmaterialIndexU3E5__4_10 = value;
	}

	inline static int32_t get_offset_of_U3CvertexIndexU3E5__5_11() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___U3CvertexIndexU3E5__5_11)); }
	inline int32_t get_U3CvertexIndexU3E5__5_11() const { return ___U3CvertexIndexU3E5__5_11; }
	inline int32_t* get_address_of_U3CvertexIndexU3E5__5_11() { return &___U3CvertexIndexU3E5__5_11; }
	inline void set_U3CvertexIndexU3E5__5_11(int32_t value)
	{
		___U3CvertexIndexU3E5__5_11 = value;
	}

	inline static int32_t get_offset_of_U3CmeshInfoU3E5__6_12() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___U3CmeshInfoU3E5__6_12)); }
	inline TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E  get_U3CmeshInfoU3E5__6_12() const { return ___U3CmeshInfoU3E5__6_12; }
	inline TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E * get_address_of_U3CmeshInfoU3E5__6_12() { return &___U3CmeshInfoU3E5__6_12; }
	inline void set_U3CmeshInfoU3E5__6_12(TMP_MeshInfo_t0140B4A33090360DC5CFB47CD8419369BBE3AD2E  value)
	{
		___U3CmeshInfoU3E5__6_12 = value;
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__7_13() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___U3CelapsedTimeU3E5__7_13)); }
	inline float get_U3CelapsedTimeU3E5__7_13() const { return ___U3CelapsedTimeU3E5__7_13; }
	inline float* get_address_of_U3CelapsedTimeU3E5__7_13() { return &___U3CelapsedTimeU3E5__7_13; }
	inline void set_U3CelapsedTimeU3E5__7_13(float value)
	{
		___U3CelapsedTimeU3E5__7_13 = value;
	}

	inline static int32_t get_offset_of_U3CtargetTimeU3E5__8_14() { return static_cast<int32_t>(offsetof(U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC, ___U3CtargetTimeU3E5__8_14)); }
	inline float get_U3CtargetTimeU3E5__8_14() const { return ___U3CtargetTimeU3E5__8_14; }
	inline float* get_address_of_U3CtargetTimeU3E5__8_14() { return &___U3CtargetTimeU3E5__8_14; }
	inline void set_U3CtargetTimeU3E5__8_14(float value)
	{
		___U3CtargetTimeU3E5__8_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOSPRITEANIMATIONINTERNALU3ED__7_T31ADE0DE24AE64EC437AED7B01D5749E9248C2AC_H
#ifndef TMP_SPRITECHARACTER_TDFDF0D32E583270A561804076B01146C324F4D33_H
#define TMP_SPRITECHARACTER_TDFDF0D32E583270A561804076B01146C324F4D33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteCharacter
struct  TMP_SpriteCharacter_tDFDF0D32E583270A561804076B01146C324F4D33  : public TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344
{
public:
	// System.String TMPro.TMP_SpriteCharacter::m_Name
	String_t* ___m_Name_5;
	// System.Int32 TMPro.TMP_SpriteCharacter::m_HashCode
	int32_t ___m_HashCode_6;

public:
	inline static int32_t get_offset_of_m_Name_5() { return static_cast<int32_t>(offsetof(TMP_SpriteCharacter_tDFDF0D32E583270A561804076B01146C324F4D33, ___m_Name_5)); }
	inline String_t* get_m_Name_5() const { return ___m_Name_5; }
	inline String_t** get_address_of_m_Name_5() { return &___m_Name_5; }
	inline void set_m_Name_5(String_t* value)
	{
		___m_Name_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Name_5), value);
	}

	inline static int32_t get_offset_of_m_HashCode_6() { return static_cast<int32_t>(offsetof(TMP_SpriteCharacter_tDFDF0D32E583270A561804076B01146C324F4D33, ___m_HashCode_6)); }
	inline int32_t get_m_HashCode_6() const { return ___m_HashCode_6; }
	inline int32_t* get_address_of_m_HashCode_6() { return &___m_HashCode_6; }
	inline void set_m_HashCode_6(int32_t value)
	{
		___m_HashCode_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITECHARACTER_TDFDF0D32E583270A561804076B01146C324F4D33_H
#ifndef TMP_STYLESHEET_TC6C45E5B0EC8EF4BA7BB147712516656B0D26C04_H
#define TMP_STYLESHEET_TC6C45E5B0EC8EF4BA7BB147712516656B0D26C04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_StyleSheet
struct  TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Collections.Generic.List`1<TMPro.TMP_Style> TMPro.TMP_StyleSheet::m_StyleList
	List_1_t3C8C2798F78B514EA59D1ECB6A8BD82AD7F8A0F8 * ___m_StyleList_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,TMPro.TMP_Style> TMPro.TMP_StyleSheet::m_StyleDictionary
	Dictionary_2_t1B12246F7055F99EF606808AA8031C87EC25F3C4 * ___m_StyleDictionary_6;

public:
	inline static int32_t get_offset_of_m_StyleList_5() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04, ___m_StyleList_5)); }
	inline List_1_t3C8C2798F78B514EA59D1ECB6A8BD82AD7F8A0F8 * get_m_StyleList_5() const { return ___m_StyleList_5; }
	inline List_1_t3C8C2798F78B514EA59D1ECB6A8BD82AD7F8A0F8 ** get_address_of_m_StyleList_5() { return &___m_StyleList_5; }
	inline void set_m_StyleList_5(List_1_t3C8C2798F78B514EA59D1ECB6A8BD82AD7F8A0F8 * value)
	{
		___m_StyleList_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StyleList_5), value);
	}

	inline static int32_t get_offset_of_m_StyleDictionary_6() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04, ___m_StyleDictionary_6)); }
	inline Dictionary_2_t1B12246F7055F99EF606808AA8031C87EC25F3C4 * get_m_StyleDictionary_6() const { return ___m_StyleDictionary_6; }
	inline Dictionary_2_t1B12246F7055F99EF606808AA8031C87EC25F3C4 ** get_address_of_m_StyleDictionary_6() { return &___m_StyleDictionary_6; }
	inline void set_m_StyleDictionary_6(Dictionary_2_t1B12246F7055F99EF606808AA8031C87EC25F3C4 * value)
	{
		___m_StyleDictionary_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_StyleDictionary_6), value);
	}
};

struct TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04_StaticFields
{
public:
	// TMPro.TMP_StyleSheet TMPro.TMP_StyleSheet::s_Instance
	TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04 * ___s_Instance_4;

public:
	inline static int32_t get_offset_of_s_Instance_4() { return static_cast<int32_t>(offsetof(TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04_StaticFields, ___s_Instance_4)); }
	inline TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04 * get_s_Instance_4() const { return ___s_Instance_4; }
	inline TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04 ** get_address_of_s_Instance_4() { return &___s_Instance_4; }
	inline void set_s_Instance_4(TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04 * value)
	{
		___s_Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_STYLESHEET_TC6C45E5B0EC8EF4BA7BB147712516656B0D26C04_H
#ifndef WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#define WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	// TMPro.TMP_FontStyleStack TMPro.WordWrapState::basicStyleStack
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient> TMPro.WordWrapState::colorGradientStack
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___sizeStack_39;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___indentStack_40;
	// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight> TMPro.WordWrapState::fontWeightStack
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___styleStack_42;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___baselineStack_43;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___actionStack_44;
	// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_47;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_51;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_53;
	// System.Boolean TMPro.WordWrapState::isNonBreakingSpace
	bool ___isNonBreakingSpace_54;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___textInfo_27)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_27), value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineInfo_28)); }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___vertexColor_29)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColor_30)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColor_31)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColor_32)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___basicStyleStack_33)); }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorStack_34)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___underlineColorStack_35)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___strikethroughColorStack_36)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___highlightColorStack_37)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_colorGradientStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___colorGradientStack_38)); }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  get_colorGradientStack_38() const { return ___colorGradientStack_38; }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D * get_address_of_colorGradientStack_38() { return &___colorGradientStack_38; }
	inline void set_colorGradientStack_38(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  value)
	{
		___colorGradientStack_38 = value;
	}

	inline static int32_t get_offset_of_sizeStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___sizeStack_39)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_sizeStack_39() const { return ___sizeStack_39; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_sizeStack_39() { return &___sizeStack_39; }
	inline void set_sizeStack_39(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___sizeStack_39 = value;
	}

	inline static int32_t get_offset_of_indentStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___indentStack_40)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_indentStack_40() const { return ___indentStack_40; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_indentStack_40() { return &___indentStack_40; }
	inline void set_indentStack_40(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___indentStack_40 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___fontWeightStack_41)); }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  get_fontWeightStack_41() const { return ___fontWeightStack_41; }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B * get_address_of_fontWeightStack_41() { return &___fontWeightStack_41; }
	inline void set_fontWeightStack_41(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  value)
	{
		___fontWeightStack_41 = value;
	}

	inline static int32_t get_offset_of_styleStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___styleStack_42)); }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  get_styleStack_42() const { return ___styleStack_42; }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F * get_address_of_styleStack_42() { return &___styleStack_42; }
	inline void set_styleStack_42(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  value)
	{
		___styleStack_42 = value;
	}

	inline static int32_t get_offset_of_baselineStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___baselineStack_43)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_baselineStack_43() const { return ___baselineStack_43; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_baselineStack_43() { return &___baselineStack_43; }
	inline void set_baselineStack_43(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___baselineStack_43 = value;
	}

	inline static int32_t get_offset_of_actionStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___actionStack_44)); }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  get_actionStack_44() const { return ___actionStack_44; }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F * get_address_of_actionStack_44() { return &___actionStack_44; }
	inline void set_actionStack_44(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  value)
	{
		___actionStack_44 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___materialReferenceStack_45)); }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  get_materialReferenceStack_45() const { return ___materialReferenceStack_45; }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 * get_address_of_materialReferenceStack_45() { return &___materialReferenceStack_45; }
	inline void set_materialReferenceStack_45(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  value)
	{
		___materialReferenceStack_45 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_46() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___lineJustificationStack_46)); }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  get_lineJustificationStack_46() const { return ___lineJustificationStack_46; }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 * get_address_of_lineJustificationStack_46() { return &___lineJustificationStack_46; }
	inline void set_lineJustificationStack_46(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  value)
	{
		___lineJustificationStack_46 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_47() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___spriteAnimationID_47)); }
	inline int32_t get_spriteAnimationID_47() const { return ___spriteAnimationID_47; }
	inline int32_t* get_address_of_spriteAnimationID_47() { return &___spriteAnimationID_47; }
	inline void set_spriteAnimationID_47(int32_t value)
	{
		___spriteAnimationID_47 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentFontAsset_48)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_currentFontAsset_48() const { return ___currentFontAsset_48; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_currentFontAsset_48() { return &___currentFontAsset_48; }
	inline void set_currentFontAsset_48(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___currentFontAsset_48 = value;
		Il2CppCodeGenWriteBarrier((&___currentFontAsset_48), value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_49() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentSpriteAsset_49)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_currentSpriteAsset_49() const { return ___currentSpriteAsset_49; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_currentSpriteAsset_49() { return &___currentSpriteAsset_49; }
	inline void set_currentSpriteAsset_49(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___currentSpriteAsset_49 = value;
		Il2CppCodeGenWriteBarrier((&___currentSpriteAsset_49), value);
	}

	inline static int32_t get_offset_of_currentMaterial_50() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterial_50)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_currentMaterial_50() const { return ___currentMaterial_50; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_currentMaterial_50() { return &___currentMaterial_50; }
	inline void set_currentMaterial_50(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___currentMaterial_50 = value;
		Il2CppCodeGenWriteBarrier((&___currentMaterial_50), value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_51() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___currentMaterialIndex_51)); }
	inline int32_t get_currentMaterialIndex_51() const { return ___currentMaterialIndex_51; }
	inline int32_t* get_address_of_currentMaterialIndex_51() { return &___currentMaterialIndex_51; }
	inline void set_currentMaterialIndex_51(int32_t value)
	{
		___currentMaterialIndex_51 = value;
	}

	inline static int32_t get_offset_of_meshExtents_52() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___meshExtents_52)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_meshExtents_52() const { return ___meshExtents_52; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_meshExtents_52() { return &___meshExtents_52; }
	inline void set_meshExtents_52(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___meshExtents_52 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_53() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___tagNoParsing_53)); }
	inline bool get_tagNoParsing_53() const { return ___tagNoParsing_53; }
	inline bool* get_address_of_tagNoParsing_53() { return &___tagNoParsing_53; }
	inline void set_tagNoParsing_53(bool value)
	{
		___tagNoParsing_53 = value;
	}

	inline static int32_t get_offset_of_isNonBreakingSpace_54() { return static_cast<int32_t>(offsetof(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557, ___isNonBreakingSpace_54)); }
	inline bool get_isNonBreakingSpace_54() const { return ___isNonBreakingSpace_54; }
	inline bool* get_address_of_isNonBreakingSpace_54() { return &___isNonBreakingSpace_54; }
	inline void set_isNonBreakingSpace_54(bool value)
	{
		___isNonBreakingSpace_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___sizeStack_39;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___indentStack_40;
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___styleStack_42;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___baselineStack_43;
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___actionStack_44;
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___textInfo_27;
	TMP_LineInfo_tE89A82D872E55C3DDF29C4C8D862358633D0B442  ___lineInfo_28;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___vertexColor_29;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___underlineColor_30;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___strikethroughColor_31;
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___highlightColor_32;
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___basicStyleStack_33;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___colorStack_34;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___underlineColorStack_35;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___strikethroughColorStack_36;
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___highlightColorStack_37;
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___colorGradientStack_38;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___sizeStack_39;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___indentStack_40;
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___fontWeightStack_41;
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___styleStack_42;
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___baselineStack_43;
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___actionStack_44;
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___materialReferenceStack_45;
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___lineJustificationStack_46;
	int32_t ___spriteAnimationID_47;
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___currentFontAsset_48;
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___currentSpriteAsset_49;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___currentMaterial_50;
	int32_t ___currentMaterialIndex_51;
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___meshExtents_52;
	int32_t ___tagNoParsing_53;
	int32_t ___isNonBreakingSpace_54;
};
#endif // WORDWRAPSTATE_T415B8622774DD094A9CD7447D298B33B7365A557_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef TMP_SPRITEASSET_TF896FFED2AA9395D6BC40FFEAC6DE7555A27A487_H
#define TMP_SPRITEASSET_TF896FFED2AA9395D6BC40FFEAC6DE7555A27A487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAsset
struct  TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487  : public TMP_Asset_tE47F21E07C734D11D5DCEA5C0A0264465963CB2D
{
public:
	// System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32> TMPro.TMP_SpriteAsset::m_UnicodeLookup
	Dictionary_2_t85CBDDBFA5D263E087932D42B863C888CEC9D354 * ___m_UnicodeLookup_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_SpriteAsset::m_NameLookup
	Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * ___m_NameLookup_8;
	// System.Collections.Generic.Dictionary`2<System.UInt32,System.Int32> TMPro.TMP_SpriteAsset::m_GlyphIndexLookup
	Dictionary_2_t85CBDDBFA5D263E087932D42B863C888CEC9D354 * ___m_GlyphIndexLookup_9;
	// System.String TMPro.TMP_SpriteAsset::m_Version
	String_t* ___m_Version_10;
	// UnityEngine.Texture TMPro.TMP_SpriteAsset::spriteSheet
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___spriteSheet_11;
	// System.Collections.Generic.List`1<TMPro.TMP_SpriteCharacter> TMPro.TMP_SpriteAsset::m_SpriteCharacterTable
	List_1_t0A3E8FF46D5FE9057636C7E2DBB12C5EDFC0A6A8 * ___m_SpriteCharacterTable_12;
	// System.Collections.Generic.List`1<TMPro.TMP_SpriteGlyph> TMPro.TMP_SpriteAsset::m_SpriteGlyphTable
	List_1_tE0F8547D4420BB67D96E2E772D7EA26E193C345E * ___m_SpriteGlyphTable_13;
	// System.Collections.Generic.List`1<TMPro.TMP_Sprite> TMPro.TMP_SpriteAsset::spriteInfoList
	List_1_t8BBE089D87BC1A1157DE98EEB1348E833555E233 * ___spriteInfoList_14;
	// System.Collections.Generic.List`1<TMPro.TMP_SpriteAsset> TMPro.TMP_SpriteAsset::fallbackSpriteAssets
	List_1_tE6AB11E0703EB02C9F65EEEB0B61E330B08E8C0B * ___fallbackSpriteAssets_15;
	// System.Boolean TMPro.TMP_SpriteAsset::m_IsSpriteAssetLookupTablesDirty
	bool ___m_IsSpriteAssetLookupTablesDirty_16;

public:
	inline static int32_t get_offset_of_m_UnicodeLookup_7() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487, ___m_UnicodeLookup_7)); }
	inline Dictionary_2_t85CBDDBFA5D263E087932D42B863C888CEC9D354 * get_m_UnicodeLookup_7() const { return ___m_UnicodeLookup_7; }
	inline Dictionary_2_t85CBDDBFA5D263E087932D42B863C888CEC9D354 ** get_address_of_m_UnicodeLookup_7() { return &___m_UnicodeLookup_7; }
	inline void set_m_UnicodeLookup_7(Dictionary_2_t85CBDDBFA5D263E087932D42B863C888CEC9D354 * value)
	{
		___m_UnicodeLookup_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_UnicodeLookup_7), value);
	}

	inline static int32_t get_offset_of_m_NameLookup_8() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487, ___m_NameLookup_8)); }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * get_m_NameLookup_8() const { return ___m_NameLookup_8; }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E ** get_address_of_m_NameLookup_8() { return &___m_NameLookup_8; }
	inline void set_m_NameLookup_8(Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * value)
	{
		___m_NameLookup_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_NameLookup_8), value);
	}

	inline static int32_t get_offset_of_m_GlyphIndexLookup_9() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487, ___m_GlyphIndexLookup_9)); }
	inline Dictionary_2_t85CBDDBFA5D263E087932D42B863C888CEC9D354 * get_m_GlyphIndexLookup_9() const { return ___m_GlyphIndexLookup_9; }
	inline Dictionary_2_t85CBDDBFA5D263E087932D42B863C888CEC9D354 ** get_address_of_m_GlyphIndexLookup_9() { return &___m_GlyphIndexLookup_9; }
	inline void set_m_GlyphIndexLookup_9(Dictionary_2_t85CBDDBFA5D263E087932D42B863C888CEC9D354 * value)
	{
		___m_GlyphIndexLookup_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_GlyphIndexLookup_9), value);
	}

	inline static int32_t get_offset_of_m_Version_10() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487, ___m_Version_10)); }
	inline String_t* get_m_Version_10() const { return ___m_Version_10; }
	inline String_t** get_address_of_m_Version_10() { return &___m_Version_10; }
	inline void set_m_Version_10(String_t* value)
	{
		___m_Version_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Version_10), value);
	}

	inline static int32_t get_offset_of_spriteSheet_11() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487, ___spriteSheet_11)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_spriteSheet_11() const { return ___spriteSheet_11; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_spriteSheet_11() { return &___spriteSheet_11; }
	inline void set_spriteSheet_11(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___spriteSheet_11 = value;
		Il2CppCodeGenWriteBarrier((&___spriteSheet_11), value);
	}

	inline static int32_t get_offset_of_m_SpriteCharacterTable_12() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487, ___m_SpriteCharacterTable_12)); }
	inline List_1_t0A3E8FF46D5FE9057636C7E2DBB12C5EDFC0A6A8 * get_m_SpriteCharacterTable_12() const { return ___m_SpriteCharacterTable_12; }
	inline List_1_t0A3E8FF46D5FE9057636C7E2DBB12C5EDFC0A6A8 ** get_address_of_m_SpriteCharacterTable_12() { return &___m_SpriteCharacterTable_12; }
	inline void set_m_SpriteCharacterTable_12(List_1_t0A3E8FF46D5FE9057636C7E2DBB12C5EDFC0A6A8 * value)
	{
		___m_SpriteCharacterTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpriteCharacterTable_12), value);
	}

	inline static int32_t get_offset_of_m_SpriteGlyphTable_13() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487, ___m_SpriteGlyphTable_13)); }
	inline List_1_tE0F8547D4420BB67D96E2E772D7EA26E193C345E * get_m_SpriteGlyphTable_13() const { return ___m_SpriteGlyphTable_13; }
	inline List_1_tE0F8547D4420BB67D96E2E772D7EA26E193C345E ** get_address_of_m_SpriteGlyphTable_13() { return &___m_SpriteGlyphTable_13; }
	inline void set_m_SpriteGlyphTable_13(List_1_tE0F8547D4420BB67D96E2E772D7EA26E193C345E * value)
	{
		___m_SpriteGlyphTable_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpriteGlyphTable_13), value);
	}

	inline static int32_t get_offset_of_spriteInfoList_14() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487, ___spriteInfoList_14)); }
	inline List_1_t8BBE089D87BC1A1157DE98EEB1348E833555E233 * get_spriteInfoList_14() const { return ___spriteInfoList_14; }
	inline List_1_t8BBE089D87BC1A1157DE98EEB1348E833555E233 ** get_address_of_spriteInfoList_14() { return &___spriteInfoList_14; }
	inline void set_spriteInfoList_14(List_1_t8BBE089D87BC1A1157DE98EEB1348E833555E233 * value)
	{
		___spriteInfoList_14 = value;
		Il2CppCodeGenWriteBarrier((&___spriteInfoList_14), value);
	}

	inline static int32_t get_offset_of_fallbackSpriteAssets_15() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487, ___fallbackSpriteAssets_15)); }
	inline List_1_tE6AB11E0703EB02C9F65EEEB0B61E330B08E8C0B * get_fallbackSpriteAssets_15() const { return ___fallbackSpriteAssets_15; }
	inline List_1_tE6AB11E0703EB02C9F65EEEB0B61E330B08E8C0B ** get_address_of_fallbackSpriteAssets_15() { return &___fallbackSpriteAssets_15; }
	inline void set_fallbackSpriteAssets_15(List_1_tE6AB11E0703EB02C9F65EEEB0B61E330B08E8C0B * value)
	{
		___fallbackSpriteAssets_15 = value;
		Il2CppCodeGenWriteBarrier((&___fallbackSpriteAssets_15), value);
	}

	inline static int32_t get_offset_of_m_IsSpriteAssetLookupTablesDirty_16() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487, ___m_IsSpriteAssetLookupTablesDirty_16)); }
	inline bool get_m_IsSpriteAssetLookupTablesDirty_16() const { return ___m_IsSpriteAssetLookupTablesDirty_16; }
	inline bool* get_address_of_m_IsSpriteAssetLookupTablesDirty_16() { return &___m_IsSpriteAssetLookupTablesDirty_16; }
	inline void set_m_IsSpriteAssetLookupTablesDirty_16(bool value)
	{
		___m_IsSpriteAssetLookupTablesDirty_16 = value;
	}
};

struct TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Int32> TMPro.TMP_SpriteAsset::k_searchedSpriteAssets
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___k_searchedSpriteAssets_17;

public:
	inline static int32_t get_offset_of_k_searchedSpriteAssets_17() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487_StaticFields, ___k_searchedSpriteAssets_17)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get_k_searchedSpriteAssets_17() const { return ___k_searchedSpriteAssets_17; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of_k_searchedSpriteAssets_17() { return &___k_searchedSpriteAssets_17; }
	inline void set_k_searchedSpriteAssets_17(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		___k_searchedSpriteAssets_17 = value;
		Il2CppCodeGenWriteBarrier((&___k_searchedSpriteAssets_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEASSET_TF896FFED2AA9395D6BC40FFEAC6DE7555A27A487_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef KEEPALIVEBEHAVIOUR_1_TAD540201F06EB8719D9AFD2D4D92088A6DCE2114_H
#define KEEPALIVEBEHAVIOUR_1_TAD540201F06EB8719D9AFD2D4D92088A6DCE2114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.KeepAliveBehaviour`1<CodeStage.AntiCheat.Time.SpeedHackProofTime>
struct  KeepAliveBehaviour_1_tAD540201F06EB8719D9AFD2D4D92088A6DCE2114  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::keepAlive
	bool ___keepAlive_4;
	// System.Int32 CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::instancesInScene
	int32_t ___instancesInScene_5;

public:
	inline static int32_t get_offset_of_keepAlive_4() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_tAD540201F06EB8719D9AFD2D4D92088A6DCE2114, ___keepAlive_4)); }
	inline bool get_keepAlive_4() const { return ___keepAlive_4; }
	inline bool* get_address_of_keepAlive_4() { return &___keepAlive_4; }
	inline void set_keepAlive_4(bool value)
	{
		___keepAlive_4 = value;
	}

	inline static int32_t get_offset_of_instancesInScene_5() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_tAD540201F06EB8719D9AFD2D4D92088A6DCE2114, ___instancesInScene_5)); }
	inline int32_t get_instancesInScene_5() const { return ___instancesInScene_5; }
	inline int32_t* get_address_of_instancesInScene_5() { return &___instancesInScene_5; }
	inline void set_instancesInScene_5(int32_t value)
	{
		___instancesInScene_5 = value;
	}
};

struct KeepAliveBehaviour_1_tAD540201F06EB8719D9AFD2D4D92088A6DCE2114_StaticFields
{
public:
	// T CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::<Instance>k__BackingField
	SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_tAD540201F06EB8719D9AFD2D4D92088A6DCE2114_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEEPALIVEBEHAVIOUR_1_TAD540201F06EB8719D9AFD2D4D92088A6DCE2114_H
#ifndef TMP_SPRITEANIMATOR_TEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17_H
#define TMP_SPRITEANIMATOR_TEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAnimator
struct  TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> TMPro.TMP_SpriteAnimator::m_animations
	Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 * ___m_animations_4;
	// TMPro.TMP_Text TMPro.TMP_SpriteAnimator::m_TextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_TextComponent_5;

public:
	inline static int32_t get_offset_of_m_animations_4() { return static_cast<int32_t>(offsetof(TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17, ___m_animations_4)); }
	inline Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 * get_m_animations_4() const { return ___m_animations_4; }
	inline Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 ** get_address_of_m_animations_4() { return &___m_animations_4; }
	inline void set_m_animations_4(Dictionary_2_t51623C556AA5634DE50ABE8918A82995978C8D71 * value)
	{
		___m_animations_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_animations_4), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_5() { return static_cast<int32_t>(offsetof(TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17, ___m_TextComponent_5)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_TextComponent_5() const { return ___m_TextComponent_5; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_TextComponent_5() { return &___m_TextComponent_5; }
	inline void set_m_TextComponent_5(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_TextComponent_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SPRITEANIMATOR_TEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17_H
#ifndef TMP_SUBMESH_TB9C2AFAA42A17F92D31845EEFCD99B144867A96D_H
#define TMP_SUBMESH_TB9C2AFAA42A17F92D31845EEFCD99B144867A96D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SubMesh
struct  TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_SubMesh::m_fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___m_fontAsset_4;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SubMesh::m_spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_spriteAsset_5;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_material_6;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_sharedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_sharedMaterial_7;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_fallbackMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_fallbackMaterial_8;
	// UnityEngine.Material TMPro.TMP_SubMesh::m_fallbackSourceMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_fallbackSourceMaterial_9;
	// System.Boolean TMPro.TMP_SubMesh::m_isDefaultMaterial
	bool ___m_isDefaultMaterial_10;
	// System.Single TMPro.TMP_SubMesh::m_padding
	float ___m_padding_11;
	// UnityEngine.Renderer TMPro.TMP_SubMesh::m_renderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___m_renderer_12;
	// UnityEngine.MeshFilter TMPro.TMP_SubMesh::m_meshFilter
	MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * ___m_meshFilter_13;
	// UnityEngine.Mesh TMPro.TMP_SubMesh::m_mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_mesh_14;
	// TMPro.TextMeshPro TMPro.TMP_SubMesh::m_TextComponent
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * ___m_TextComponent_15;
	// System.Boolean TMPro.TMP_SubMesh::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_16;

public:
	inline static int32_t get_offset_of_m_fontAsset_4() { return static_cast<int32_t>(offsetof(TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D, ___m_fontAsset_4)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_m_fontAsset_4() const { return ___m_fontAsset_4; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_m_fontAsset_4() { return &___m_fontAsset_4; }
	inline void set_m_fontAsset_4(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___m_fontAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_4), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_5() { return static_cast<int32_t>(offsetof(TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D, ___m_spriteAsset_5)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_spriteAsset_5() const { return ___m_spriteAsset_5; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_spriteAsset_5() { return &___m_spriteAsset_5; }
	inline void set_m_spriteAsset_5(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_spriteAsset_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_5), value);
	}

	inline static int32_t get_offset_of_m_material_6() { return static_cast<int32_t>(offsetof(TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D, ___m_material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_material_6() const { return ___m_material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_material_6() { return &___m_material_6; }
	inline void set_m_material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_6), value);
	}

	inline static int32_t get_offset_of_m_sharedMaterial_7() { return static_cast<int32_t>(offsetof(TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D, ___m_sharedMaterial_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_sharedMaterial_7() const { return ___m_sharedMaterial_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_sharedMaterial_7() { return &___m_sharedMaterial_7; }
	inline void set_m_sharedMaterial_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_sharedMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_7), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterial_8() { return static_cast<int32_t>(offsetof(TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D, ___m_fallbackMaterial_8)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_fallbackMaterial_8() const { return ___m_fallbackMaterial_8; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_fallbackMaterial_8() { return &___m_fallbackMaterial_8; }
	inline void set_m_fallbackMaterial_8(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_fallbackMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterial_8), value);
	}

	inline static int32_t get_offset_of_m_fallbackSourceMaterial_9() { return static_cast<int32_t>(offsetof(TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D, ___m_fallbackSourceMaterial_9)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_fallbackSourceMaterial_9() const { return ___m_fallbackSourceMaterial_9; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_fallbackSourceMaterial_9() { return &___m_fallbackSourceMaterial_9; }
	inline void set_m_fallbackSourceMaterial_9(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_fallbackSourceMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackSourceMaterial_9), value);
	}

	inline static int32_t get_offset_of_m_isDefaultMaterial_10() { return static_cast<int32_t>(offsetof(TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D, ___m_isDefaultMaterial_10)); }
	inline bool get_m_isDefaultMaterial_10() const { return ___m_isDefaultMaterial_10; }
	inline bool* get_address_of_m_isDefaultMaterial_10() { return &___m_isDefaultMaterial_10; }
	inline void set_m_isDefaultMaterial_10(bool value)
	{
		___m_isDefaultMaterial_10 = value;
	}

	inline static int32_t get_offset_of_m_padding_11() { return static_cast<int32_t>(offsetof(TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D, ___m_padding_11)); }
	inline float get_m_padding_11() const { return ___m_padding_11; }
	inline float* get_address_of_m_padding_11() { return &___m_padding_11; }
	inline void set_m_padding_11(float value)
	{
		___m_padding_11 = value;
	}

	inline static int32_t get_offset_of_m_renderer_12() { return static_cast<int32_t>(offsetof(TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D, ___m_renderer_12)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_m_renderer_12() const { return ___m_renderer_12; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_m_renderer_12() { return &___m_renderer_12; }
	inline void set_m_renderer_12(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___m_renderer_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_renderer_12), value);
	}

	inline static int32_t get_offset_of_m_meshFilter_13() { return static_cast<int32_t>(offsetof(TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D, ___m_meshFilter_13)); }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * get_m_meshFilter_13() const { return ___m_meshFilter_13; }
	inline MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 ** get_address_of_m_meshFilter_13() { return &___m_meshFilter_13; }
	inline void set_m_meshFilter_13(MeshFilter_t8D4BA8E8723DE5CFF53B0DA5EE2F6B3A5B0E0FE0 * value)
	{
		___m_meshFilter_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_meshFilter_13), value);
	}

	inline static int32_t get_offset_of_m_mesh_14() { return static_cast<int32_t>(offsetof(TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D, ___m_mesh_14)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_mesh_14() const { return ___m_mesh_14; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_mesh_14() { return &___m_mesh_14; }
	inline void set_m_mesh_14(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_mesh_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_14), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_15() { return static_cast<int32_t>(offsetof(TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D, ___m_TextComponent_15)); }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * get_m_TextComponent_15() const { return ___m_TextComponent_15; }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 ** get_address_of_m_TextComponent_15() { return &___m_TextComponent_15; }
	inline void set_m_TextComponent_15(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * value)
	{
		___m_TextComponent_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_15), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_16() { return static_cast<int32_t>(offsetof(TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D, ___m_isRegisteredForEvents_16)); }
	inline bool get_m_isRegisteredForEvents_16() const { return ___m_isRegisteredForEvents_16; }
	inline bool* get_address_of_m_isRegisteredForEvents_16() { return &___m_isRegisteredForEvents_16; }
	inline void set_m_isRegisteredForEvents_16(bool value)
	{
		___m_isRegisteredForEvents_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SUBMESH_TB9C2AFAA42A17F92D31845EEFCD99B144867A96D_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef SPEEDHACKPROOFTIME_TE628F6372CF4274190B0BF833F0E10AF9D47E23C_H
#define SPEEDHACKPROOFTIME_TE628F6372CF4274190B0BF833F0E10AF9D47E23C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Time.SpeedHackProofTime
struct  SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C  : public KeepAliveBehaviour_1_tAD540201F06EB8719D9AFD2D4D92088A6DCE2114
{
public:
	// System.Int64 CodeStage.AntiCheat.Time.SpeedHackProofTime::currentReliableTicks
	int64_t ___currentReliableTicks_16;
	// System.Int64 CodeStage.AntiCheat.Time.SpeedHackProofTime::lastFrameReliableTicks
	int64_t ___lastFrameReliableTicks_17;
	// System.Int64 CodeStage.AntiCheat.Time.SpeedHackProofTime::reliableTicksDelta
	int64_t ___reliableTicksDelta_18;

public:
	inline static int32_t get_offset_of_currentReliableTicks_16() { return static_cast<int32_t>(offsetof(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C, ___currentReliableTicks_16)); }
	inline int64_t get_currentReliableTicks_16() const { return ___currentReliableTicks_16; }
	inline int64_t* get_address_of_currentReliableTicks_16() { return &___currentReliableTicks_16; }
	inline void set_currentReliableTicks_16(int64_t value)
	{
		___currentReliableTicks_16 = value;
	}

	inline static int32_t get_offset_of_lastFrameReliableTicks_17() { return static_cast<int32_t>(offsetof(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C, ___lastFrameReliableTicks_17)); }
	inline int64_t get_lastFrameReliableTicks_17() const { return ___lastFrameReliableTicks_17; }
	inline int64_t* get_address_of_lastFrameReliableTicks_17() { return &___lastFrameReliableTicks_17; }
	inline void set_lastFrameReliableTicks_17(int64_t value)
	{
		___lastFrameReliableTicks_17 = value;
	}

	inline static int32_t get_offset_of_reliableTicksDelta_18() { return static_cast<int32_t>(offsetof(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C, ___reliableTicksDelta_18)); }
	inline int64_t get_reliableTicksDelta_18() const { return ___reliableTicksDelta_18; }
	inline int64_t* get_address_of_reliableTicksDelta_18() { return &___reliableTicksDelta_18; }
	inline void set_reliableTicksDelta_18(int64_t value)
	{
		___reliableTicksDelta_18 = value;
	}
};

struct SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields
{
public:
	// System.Boolean CodeStage.AntiCheat.Time.SpeedHackProofTime::inited
	bool ___inited_7;
	// System.Boolean CodeStage.AntiCheat.Time.SpeedHackProofTime::speedHackDetected
	bool ___speedHackDetected_8;
	// System.Single CodeStage.AntiCheat.Time.SpeedHackProofTime::reliableTime
	float ___reliableTime_9;
	// System.Single CodeStage.AntiCheat.Time.SpeedHackProofTime::reliableDeltaTime
	float ___reliableDeltaTime_10;
	// System.Single CodeStage.AntiCheat.Time.SpeedHackProofTime::reliableUnscaledTime
	float ___reliableUnscaledTime_11;
	// System.Single CodeStage.AntiCheat.Time.SpeedHackProofTime::reliableUnscaledDeltaTime
	float ___reliableUnscaledDeltaTime_12;
	// System.Single CodeStage.AntiCheat.Time.SpeedHackProofTime::reliableRealtimeSinceStartup
	float ___reliableRealtimeSinceStartup_13;
	// System.Single CodeStage.AntiCheat.Time.SpeedHackProofTime::reliableTimeSinceLevelLoad
	float ___reliableTimeSinceLevelLoad_14;
	// System.Boolean CodeStage.AntiCheat.Time.SpeedHackProofTime::warningShot
	bool ___warningShot_15;

public:
	inline static int32_t get_offset_of_inited_7() { return static_cast<int32_t>(offsetof(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields, ___inited_7)); }
	inline bool get_inited_7() const { return ___inited_7; }
	inline bool* get_address_of_inited_7() { return &___inited_7; }
	inline void set_inited_7(bool value)
	{
		___inited_7 = value;
	}

	inline static int32_t get_offset_of_speedHackDetected_8() { return static_cast<int32_t>(offsetof(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields, ___speedHackDetected_8)); }
	inline bool get_speedHackDetected_8() const { return ___speedHackDetected_8; }
	inline bool* get_address_of_speedHackDetected_8() { return &___speedHackDetected_8; }
	inline void set_speedHackDetected_8(bool value)
	{
		___speedHackDetected_8 = value;
	}

	inline static int32_t get_offset_of_reliableTime_9() { return static_cast<int32_t>(offsetof(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields, ___reliableTime_9)); }
	inline float get_reliableTime_9() const { return ___reliableTime_9; }
	inline float* get_address_of_reliableTime_9() { return &___reliableTime_9; }
	inline void set_reliableTime_9(float value)
	{
		___reliableTime_9 = value;
	}

	inline static int32_t get_offset_of_reliableDeltaTime_10() { return static_cast<int32_t>(offsetof(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields, ___reliableDeltaTime_10)); }
	inline float get_reliableDeltaTime_10() const { return ___reliableDeltaTime_10; }
	inline float* get_address_of_reliableDeltaTime_10() { return &___reliableDeltaTime_10; }
	inline void set_reliableDeltaTime_10(float value)
	{
		___reliableDeltaTime_10 = value;
	}

	inline static int32_t get_offset_of_reliableUnscaledTime_11() { return static_cast<int32_t>(offsetof(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields, ___reliableUnscaledTime_11)); }
	inline float get_reliableUnscaledTime_11() const { return ___reliableUnscaledTime_11; }
	inline float* get_address_of_reliableUnscaledTime_11() { return &___reliableUnscaledTime_11; }
	inline void set_reliableUnscaledTime_11(float value)
	{
		___reliableUnscaledTime_11 = value;
	}

	inline static int32_t get_offset_of_reliableUnscaledDeltaTime_12() { return static_cast<int32_t>(offsetof(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields, ___reliableUnscaledDeltaTime_12)); }
	inline float get_reliableUnscaledDeltaTime_12() const { return ___reliableUnscaledDeltaTime_12; }
	inline float* get_address_of_reliableUnscaledDeltaTime_12() { return &___reliableUnscaledDeltaTime_12; }
	inline void set_reliableUnscaledDeltaTime_12(float value)
	{
		___reliableUnscaledDeltaTime_12 = value;
	}

	inline static int32_t get_offset_of_reliableRealtimeSinceStartup_13() { return static_cast<int32_t>(offsetof(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields, ___reliableRealtimeSinceStartup_13)); }
	inline float get_reliableRealtimeSinceStartup_13() const { return ___reliableRealtimeSinceStartup_13; }
	inline float* get_address_of_reliableRealtimeSinceStartup_13() { return &___reliableRealtimeSinceStartup_13; }
	inline void set_reliableRealtimeSinceStartup_13(float value)
	{
		___reliableRealtimeSinceStartup_13 = value;
	}

	inline static int32_t get_offset_of_reliableTimeSinceLevelLoad_14() { return static_cast<int32_t>(offsetof(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields, ___reliableTimeSinceLevelLoad_14)); }
	inline float get_reliableTimeSinceLevelLoad_14() const { return ___reliableTimeSinceLevelLoad_14; }
	inline float* get_address_of_reliableTimeSinceLevelLoad_14() { return &___reliableTimeSinceLevelLoad_14; }
	inline void set_reliableTimeSinceLevelLoad_14(float value)
	{
		___reliableTimeSinceLevelLoad_14 = value;
	}

	inline static int32_t get_offset_of_warningShot_15() { return static_cast<int32_t>(offsetof(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields, ___warningShot_15)); }
	inline bool get_warningShot_15() const { return ___warningShot_15; }
	inline bool* get_address_of_warningShot_15() { return &___warningShot_15; }
	inline void set_warningShot_15(bool value)
	{
		___warningShot_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEEDHACKPROOFTIME_TE628F6372CF4274190B0BF833F0E10AF9D47E23C_H
#ifndef GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#define GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_9)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_11)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_TBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_H
#ifndef MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#define MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_22)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_23)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_29)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_TDA46A5925C6A2101217C9F52C855B5C1A36A7A0F_H
#ifndef TMP_SUBMESHUI_TA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1_H
#define TMP_SUBMESHUI_TA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SubMeshUI
struct  TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// TMPro.TMP_FontAsset TMPro.TMP_SubMeshUI::m_fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___m_fontAsset_30;
	// TMPro.TMP_SpriteAsset TMPro.TMP_SubMeshUI::m_spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_spriteAsset_31;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_material_32;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_sharedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_sharedMaterial_33;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_fallbackMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_fallbackMaterial_34;
	// UnityEngine.Material TMPro.TMP_SubMeshUI::m_fallbackSourceMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_fallbackSourceMaterial_35;
	// System.Boolean TMPro.TMP_SubMeshUI::m_isDefaultMaterial
	bool ___m_isDefaultMaterial_36;
	// System.Single TMPro.TMP_SubMeshUI::m_padding
	float ___m_padding_37;
	// UnityEngine.CanvasRenderer TMPro.TMP_SubMeshUI::m_canvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_canvasRenderer_38;
	// UnityEngine.Mesh TMPro.TMP_SubMeshUI::m_mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_mesh_39;
	// TMPro.TextMeshProUGUI TMPro.TMP_SubMeshUI::m_TextComponent
	TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * ___m_TextComponent_40;
	// System.Boolean TMPro.TMP_SubMeshUI::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_41;
	// System.Boolean TMPro.TMP_SubMeshUI::m_materialDirty
	bool ___m_materialDirty_42;
	// System.Int32 TMPro.TMP_SubMeshUI::m_materialReferenceIndex
	int32_t ___m_materialReferenceIndex_43;

public:
	inline static int32_t get_offset_of_m_fontAsset_30() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_fontAsset_30)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_m_fontAsset_30() const { return ___m_fontAsset_30; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_m_fontAsset_30() { return &___m_fontAsset_30; }
	inline void set_m_fontAsset_30(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___m_fontAsset_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_30), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_31() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_spriteAsset_31)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_spriteAsset_31() const { return ___m_spriteAsset_31; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_spriteAsset_31() { return &___m_spriteAsset_31; }
	inline void set_m_spriteAsset_31(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_spriteAsset_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_31), value);
	}

	inline static int32_t get_offset_of_m_material_32() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_material_32)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_material_32() const { return ___m_material_32; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_material_32() { return &___m_material_32; }
	inline void set_m_material_32(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_material_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_32), value);
	}

	inline static int32_t get_offset_of_m_sharedMaterial_33() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_sharedMaterial_33)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_sharedMaterial_33() const { return ___m_sharedMaterial_33; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_sharedMaterial_33() { return &___m_sharedMaterial_33; }
	inline void set_m_sharedMaterial_33(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_sharedMaterial_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_33), value);
	}

	inline static int32_t get_offset_of_m_fallbackMaterial_34() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_fallbackMaterial_34)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_fallbackMaterial_34() const { return ___m_fallbackMaterial_34; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_fallbackMaterial_34() { return &___m_fallbackMaterial_34; }
	inline void set_m_fallbackMaterial_34(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_fallbackMaterial_34 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackMaterial_34), value);
	}

	inline static int32_t get_offset_of_m_fallbackSourceMaterial_35() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_fallbackSourceMaterial_35)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_fallbackSourceMaterial_35() const { return ___m_fallbackSourceMaterial_35; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_fallbackSourceMaterial_35() { return &___m_fallbackSourceMaterial_35; }
	inline void set_m_fallbackSourceMaterial_35(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_fallbackSourceMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_fallbackSourceMaterial_35), value);
	}

	inline static int32_t get_offset_of_m_isDefaultMaterial_36() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_isDefaultMaterial_36)); }
	inline bool get_m_isDefaultMaterial_36() const { return ___m_isDefaultMaterial_36; }
	inline bool* get_address_of_m_isDefaultMaterial_36() { return &___m_isDefaultMaterial_36; }
	inline void set_m_isDefaultMaterial_36(bool value)
	{
		___m_isDefaultMaterial_36 = value;
	}

	inline static int32_t get_offset_of_m_padding_37() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_padding_37)); }
	inline float get_m_padding_37() const { return ___m_padding_37; }
	inline float* get_address_of_m_padding_37() { return &___m_padding_37; }
	inline void set_m_padding_37(float value)
	{
		___m_padding_37 = value;
	}

	inline static int32_t get_offset_of_m_canvasRenderer_38() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_canvasRenderer_38)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_canvasRenderer_38() const { return ___m_canvasRenderer_38; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_canvasRenderer_38() { return &___m_canvasRenderer_38; }
	inline void set_m_canvasRenderer_38(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_canvasRenderer_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_canvasRenderer_38), value);
	}

	inline static int32_t get_offset_of_m_mesh_39() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_mesh_39)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_mesh_39() const { return ___m_mesh_39; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_mesh_39() { return &___m_mesh_39; }
	inline void set_m_mesh_39(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_mesh_39 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_39), value);
	}

	inline static int32_t get_offset_of_m_TextComponent_40() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_TextComponent_40)); }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * get_m_TextComponent_40() const { return ___m_TextComponent_40; }
	inline TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 ** get_address_of_m_TextComponent_40() { return &___m_TextComponent_40; }
	inline void set_m_TextComponent_40(TextMeshProUGUI_tBA60B913AB6151F8563F7078AD67EB6458129438 * value)
	{
		___m_TextComponent_40 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextComponent_40), value);
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_41() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_isRegisteredForEvents_41)); }
	inline bool get_m_isRegisteredForEvents_41() const { return ___m_isRegisteredForEvents_41; }
	inline bool* get_address_of_m_isRegisteredForEvents_41() { return &___m_isRegisteredForEvents_41; }
	inline void set_m_isRegisteredForEvents_41(bool value)
	{
		___m_isRegisteredForEvents_41 = value;
	}

	inline static int32_t get_offset_of_m_materialDirty_42() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_materialDirty_42)); }
	inline bool get_m_materialDirty_42() const { return ___m_materialDirty_42; }
	inline bool* get_address_of_m_materialDirty_42() { return &___m_materialDirty_42; }
	inline void set_m_materialDirty_42(bool value)
	{
		___m_materialDirty_42 = value;
	}

	inline static int32_t get_offset_of_m_materialReferenceIndex_43() { return static_cast<int32_t>(offsetof(TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1, ___m_materialReferenceIndex_43)); }
	inline int32_t get_m_materialReferenceIndex_43() const { return ___m_materialReferenceIndex_43; }
	inline int32_t* get_address_of_m_materialReferenceIndex_43() { return &___m_materialReferenceIndex_43; }
	inline void set_m_materialReferenceIndex_43(int32_t value)
	{
		___m_materialReferenceIndex_43 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_SUBMESHUI_TA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1_H
#ifndef TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#define TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_Text
struct  TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// System.String TMPro.TMP_Text::m_text
	String_t* ___m_text_30;
	// System.Boolean TMPro.TMP_Text::m_isRightToLeft
	bool ___m_isRightToLeft_31;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_fontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___m_fontAsset_32;
	// TMPro.TMP_FontAsset TMPro.TMP_Text::m_currentFontAsset
	TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * ___m_currentFontAsset_33;
	// System.Boolean TMPro.TMP_Text::m_isSDFShader
	bool ___m_isSDFShader_34;
	// UnityEngine.Material TMPro.TMP_Text::m_sharedMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_sharedMaterial_35;
	// UnityEngine.Material TMPro.TMP_Text::m_currentMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_currentMaterial_36;
	// TMPro.MaterialReference[] TMPro.TMP_Text::m_materialReferences
	MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* ___m_materialReferences_37;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_Text::m_materialReferenceIndexLookup
	Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * ___m_materialReferenceIndexLookup_38;
	// TMPro.TMP_RichTextTagStack`1<TMPro.MaterialReference> TMPro.TMP_Text::m_materialReferenceStack
	TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  ___m_materialReferenceStack_39;
	// System.Int32 TMPro.TMP_Text::m_currentMaterialIndex
	int32_t ___m_currentMaterialIndex_40;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontSharedMaterials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___m_fontSharedMaterials_41;
	// UnityEngine.Material TMPro.TMP_Text::m_fontMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_fontMaterial_42;
	// UnityEngine.Material[] TMPro.TMP_Text::m_fontMaterials
	MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* ___m_fontMaterials_43;
	// System.Boolean TMPro.TMP_Text::m_isMaterialDirty
	bool ___m_isMaterialDirty_44;
	// UnityEngine.Color32 TMPro.TMP_Text::m_fontColor32
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_fontColor32_45;
	// UnityEngine.Color TMPro.TMP_Text::m_fontColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_fontColor_46;
	// UnityEngine.Color32 TMPro.TMP_Text::m_underlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_underlineColor_48;
	// UnityEngine.Color32 TMPro.TMP_Text::m_strikethroughColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_strikethroughColor_49;
	// UnityEngine.Color32 TMPro.TMP_Text::m_highlightColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_highlightColor_50;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_highlightPadding
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_highlightPadding_51;
	// System.Boolean TMPro.TMP_Text::m_enableVertexGradient
	bool ___m_enableVertexGradient_52;
	// TMPro.ColorMode TMPro.TMP_Text::m_colorMode
	int32_t ___m_colorMode_53;
	// TMPro.VertexGradient TMPro.TMP_Text::m_fontColorGradient
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  ___m_fontColorGradient_54;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_fontColorGradientPreset
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_fontColorGradientPreset_55;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_spriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_spriteAsset_56;
	// System.Boolean TMPro.TMP_Text::m_tintAllSprites
	bool ___m_tintAllSprites_57;
	// System.Boolean TMPro.TMP_Text::m_tintSprite
	bool ___m_tintSprite_58;
	// UnityEngine.Color32 TMPro.TMP_Text::m_spriteColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_spriteColor_59;
	// System.Boolean TMPro.TMP_Text::m_overrideHtmlColors
	bool ___m_overrideHtmlColors_60;
	// UnityEngine.Color32 TMPro.TMP_Text::m_faceColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_faceColor_61;
	// UnityEngine.Color32 TMPro.TMP_Text::m_outlineColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_outlineColor_62;
	// System.Single TMPro.TMP_Text::m_outlineWidth
	float ___m_outlineWidth_63;
	// System.Single TMPro.TMP_Text::m_fontSize
	float ___m_fontSize_64;
	// System.Single TMPro.TMP_Text::m_currentFontSize
	float ___m_currentFontSize_65;
	// System.Single TMPro.TMP_Text::m_fontSizeBase
	float ___m_fontSizeBase_66;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.TMP_Text::m_sizeStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___m_sizeStack_67;
	// TMPro.FontWeight TMPro.TMP_Text::m_fontWeight
	int32_t ___m_fontWeight_68;
	// TMPro.FontWeight TMPro.TMP_Text::m_FontWeightInternal
	int32_t ___m_FontWeightInternal_69;
	// TMPro.TMP_RichTextTagStack`1<TMPro.FontWeight> TMPro.TMP_Text::m_FontWeightStack
	TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  ___m_FontWeightStack_70;
	// System.Boolean TMPro.TMP_Text::m_enableAutoSizing
	bool ___m_enableAutoSizing_71;
	// System.Single TMPro.TMP_Text::m_maxFontSize
	float ___m_maxFontSize_72;
	// System.Single TMPro.TMP_Text::m_minFontSize
	float ___m_minFontSize_73;
	// System.Single TMPro.TMP_Text::m_fontSizeMin
	float ___m_fontSizeMin_74;
	// System.Single TMPro.TMP_Text::m_fontSizeMax
	float ___m_fontSizeMax_75;
	// TMPro.FontStyles TMPro.TMP_Text::m_fontStyle
	int32_t ___m_fontStyle_76;
	// TMPro.FontStyles TMPro.TMP_Text::m_FontStyleInternal
	int32_t ___m_FontStyleInternal_77;
	// TMPro.TMP_FontStyleStack TMPro.TMP_Text::m_fontStyleStack
	TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  ___m_fontStyleStack_78;
	// System.Boolean TMPro.TMP_Text::m_isUsingBold
	bool ___m_isUsingBold_79;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_textAlignment
	int32_t ___m_textAlignment_80;
	// TMPro.TextAlignmentOptions TMPro.TMP_Text::m_lineJustification
	int32_t ___m_lineJustification_81;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TextAlignmentOptions> TMPro.TMP_Text::m_lineJustificationStack
	TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  ___m_lineJustificationStack_82;
	// UnityEngine.Vector3[] TMPro.TMP_Text::m_textContainerLocalCorners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_textContainerLocalCorners_83;
	// System.Single TMPro.TMP_Text::m_characterSpacing
	float ___m_characterSpacing_84;
	// System.Single TMPro.TMP_Text::m_cSpacing
	float ___m_cSpacing_85;
	// System.Single TMPro.TMP_Text::m_monoSpacing
	float ___m_monoSpacing_86;
	// System.Single TMPro.TMP_Text::m_wordSpacing
	float ___m_wordSpacing_87;
	// System.Single TMPro.TMP_Text::m_lineSpacing
	float ___m_lineSpacing_88;
	// System.Single TMPro.TMP_Text::m_lineSpacingDelta
	float ___m_lineSpacingDelta_89;
	// System.Single TMPro.TMP_Text::m_lineHeight
	float ___m_lineHeight_90;
	// System.Single TMPro.TMP_Text::m_lineSpacingMax
	float ___m_lineSpacingMax_91;
	// System.Single TMPro.TMP_Text::m_paragraphSpacing
	float ___m_paragraphSpacing_92;
	// System.Single TMPro.TMP_Text::m_charWidthMaxAdj
	float ___m_charWidthMaxAdj_93;
	// System.Single TMPro.TMP_Text::m_charWidthAdjDelta
	float ___m_charWidthAdjDelta_94;
	// System.Boolean TMPro.TMP_Text::m_enableWordWrapping
	bool ___m_enableWordWrapping_95;
	// System.Boolean TMPro.TMP_Text::m_isCharacterWrappingEnabled
	bool ___m_isCharacterWrappingEnabled_96;
	// System.Boolean TMPro.TMP_Text::m_isNonBreakingSpace
	bool ___m_isNonBreakingSpace_97;
	// System.Boolean TMPro.TMP_Text::m_isIgnoringAlignment
	bool ___m_isIgnoringAlignment_98;
	// System.Single TMPro.TMP_Text::m_wordWrappingRatios
	float ___m_wordWrappingRatios_99;
	// TMPro.TextOverflowModes TMPro.TMP_Text::m_overflowMode
	int32_t ___m_overflowMode_100;
	// System.Int32 TMPro.TMP_Text::m_firstOverflowCharacterIndex
	int32_t ___m_firstOverflowCharacterIndex_101;
	// TMPro.TMP_Text TMPro.TMP_Text::m_linkedTextComponent
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___m_linkedTextComponent_102;
	// System.Boolean TMPro.TMP_Text::m_isLinkedTextComponent
	bool ___m_isLinkedTextComponent_103;
	// System.Boolean TMPro.TMP_Text::m_isTextTruncated
	bool ___m_isTextTruncated_104;
	// System.Boolean TMPro.TMP_Text::m_enableKerning
	bool ___m_enableKerning_105;
	// System.Boolean TMPro.TMP_Text::m_enableExtraPadding
	bool ___m_enableExtraPadding_106;
	// System.Boolean TMPro.TMP_Text::checkPaddingRequired
	bool ___checkPaddingRequired_107;
	// System.Boolean TMPro.TMP_Text::m_isRichText
	bool ___m_isRichText_108;
	// System.Boolean TMPro.TMP_Text::m_parseCtrlCharacters
	bool ___m_parseCtrlCharacters_109;
	// System.Boolean TMPro.TMP_Text::m_isOverlay
	bool ___m_isOverlay_110;
	// System.Boolean TMPro.TMP_Text::m_isOrthographic
	bool ___m_isOrthographic_111;
	// System.Boolean TMPro.TMP_Text::m_isCullingEnabled
	bool ___m_isCullingEnabled_112;
	// System.Boolean TMPro.TMP_Text::m_ignoreRectMaskCulling
	bool ___m_ignoreRectMaskCulling_113;
	// System.Boolean TMPro.TMP_Text::m_ignoreCulling
	bool ___m_ignoreCulling_114;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_horizontalMapping
	int32_t ___m_horizontalMapping_115;
	// TMPro.TextureMappingOptions TMPro.TMP_Text::m_verticalMapping
	int32_t ___m_verticalMapping_116;
	// System.Single TMPro.TMP_Text::m_uvLineOffset
	float ___m_uvLineOffset_117;
	// TMPro.TextRenderFlags TMPro.TMP_Text::m_renderMode
	int32_t ___m_renderMode_118;
	// TMPro.VertexSortingOrder TMPro.TMP_Text::m_geometrySortingOrder
	int32_t ___m_geometrySortingOrder_119;
	// System.Boolean TMPro.TMP_Text::m_VertexBufferAutoSizeReduction
	bool ___m_VertexBufferAutoSizeReduction_120;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacter
	int32_t ___m_firstVisibleCharacter_121;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleCharacters
	int32_t ___m_maxVisibleCharacters_122;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleWords
	int32_t ___m_maxVisibleWords_123;
	// System.Int32 TMPro.TMP_Text::m_maxVisibleLines
	int32_t ___m_maxVisibleLines_124;
	// System.Boolean TMPro.TMP_Text::m_useMaxVisibleDescender
	bool ___m_useMaxVisibleDescender_125;
	// System.Int32 TMPro.TMP_Text::m_pageToDisplay
	int32_t ___m_pageToDisplay_126;
	// System.Boolean TMPro.TMP_Text::m_isNewPage
	bool ___m_isNewPage_127;
	// UnityEngine.Vector4 TMPro.TMP_Text::m_margin
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___m_margin_128;
	// System.Single TMPro.TMP_Text::m_marginLeft
	float ___m_marginLeft_129;
	// System.Single TMPro.TMP_Text::m_marginRight
	float ___m_marginRight_130;
	// System.Single TMPro.TMP_Text::m_marginWidth
	float ___m_marginWidth_131;
	// System.Single TMPro.TMP_Text::m_marginHeight
	float ___m_marginHeight_132;
	// System.Single TMPro.TMP_Text::m_width
	float ___m_width_133;
	// TMPro.TMP_TextInfo TMPro.TMP_Text::m_textInfo
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * ___m_textInfo_134;
	// System.Boolean TMPro.TMP_Text::m_havePropertiesChanged
	bool ___m_havePropertiesChanged_135;
	// System.Boolean TMPro.TMP_Text::m_isUsingLegacyAnimationComponent
	bool ___m_isUsingLegacyAnimationComponent_136;
	// UnityEngine.Transform TMPro.TMP_Text::m_transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_transform_137;
	// UnityEngine.RectTransform TMPro.TMP_Text::m_rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_rectTransform_138;
	// System.Boolean TMPro.TMP_Text::<autoSizeTextContainer>k__BackingField
	bool ___U3CautoSizeTextContainerU3Ek__BackingField_139;
	// System.Boolean TMPro.TMP_Text::m_autoSizeTextContainer
	bool ___m_autoSizeTextContainer_140;
	// UnityEngine.Mesh TMPro.TMP_Text::m_mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_mesh_141;
	// System.Boolean TMPro.TMP_Text::m_isVolumetricText
	bool ___m_isVolumetricText_142;
	// TMPro.TMP_SpriteAnimator TMPro.TMP_Text::m_spriteAnimator
	TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * ___m_spriteAnimator_143;
	// System.Single TMPro.TMP_Text::m_flexibleHeight
	float ___m_flexibleHeight_144;
	// System.Single TMPro.TMP_Text::m_flexibleWidth
	float ___m_flexibleWidth_145;
	// System.Single TMPro.TMP_Text::m_minWidth
	float ___m_minWidth_146;
	// System.Single TMPro.TMP_Text::m_minHeight
	float ___m_minHeight_147;
	// System.Single TMPro.TMP_Text::m_maxWidth
	float ___m_maxWidth_148;
	// System.Single TMPro.TMP_Text::m_maxHeight
	float ___m_maxHeight_149;
	// UnityEngine.UI.LayoutElement TMPro.TMP_Text::m_LayoutElement
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * ___m_LayoutElement_150;
	// System.Single TMPro.TMP_Text::m_preferredWidth
	float ___m_preferredWidth_151;
	// System.Single TMPro.TMP_Text::m_renderedWidth
	float ___m_renderedWidth_152;
	// System.Boolean TMPro.TMP_Text::m_isPreferredWidthDirty
	bool ___m_isPreferredWidthDirty_153;
	// System.Single TMPro.TMP_Text::m_preferredHeight
	float ___m_preferredHeight_154;
	// System.Single TMPro.TMP_Text::m_renderedHeight
	float ___m_renderedHeight_155;
	// System.Boolean TMPro.TMP_Text::m_isPreferredHeightDirty
	bool ___m_isPreferredHeightDirty_156;
	// System.Boolean TMPro.TMP_Text::m_isCalculatingPreferredValues
	bool ___m_isCalculatingPreferredValues_157;
	// System.Int32 TMPro.TMP_Text::m_recursiveCount
	int32_t ___m_recursiveCount_158;
	// System.Int32 TMPro.TMP_Text::m_layoutPriority
	int32_t ___m_layoutPriority_159;
	// System.Boolean TMPro.TMP_Text::m_isCalculateSizeRequired
	bool ___m_isCalculateSizeRequired_160;
	// System.Boolean TMPro.TMP_Text::m_isLayoutDirty
	bool ___m_isLayoutDirty_161;
	// System.Boolean TMPro.TMP_Text::m_verticesAlreadyDirty
	bool ___m_verticesAlreadyDirty_162;
	// System.Boolean TMPro.TMP_Text::m_layoutAlreadyDirty
	bool ___m_layoutAlreadyDirty_163;
	// System.Boolean TMPro.TMP_Text::m_isAwake
	bool ___m_isAwake_164;
	// System.Boolean TMPro.TMP_Text::m_isWaitingOnResourceLoad
	bool ___m_isWaitingOnResourceLoad_165;
	// System.Boolean TMPro.TMP_Text::m_isInputParsingRequired
	bool ___m_isInputParsingRequired_166;
	// TMPro.TMP_Text_TextInputSources TMPro.TMP_Text::m_inputSource
	int32_t ___m_inputSource_167;
	// System.String TMPro.TMP_Text::old_text
	String_t* ___old_text_168;
	// System.Single TMPro.TMP_Text::m_fontScale
	float ___m_fontScale_169;
	// System.Single TMPro.TMP_Text::m_fontScaleMultiplier
	float ___m_fontScaleMultiplier_170;
	// System.Char[] TMPro.TMP_Text::m_htmlTag
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_htmlTag_171;
	// TMPro.RichTextTagAttribute[] TMPro.TMP_Text::m_xmlAttribute
	RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652* ___m_xmlAttribute_172;
	// System.Single[] TMPro.TMP_Text::m_attributeParameterValues
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___m_attributeParameterValues_173;
	// System.Single TMPro.TMP_Text::tag_LineIndent
	float ___tag_LineIndent_174;
	// System.Single TMPro.TMP_Text::tag_Indent
	float ___tag_Indent_175;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.TMP_Text::m_indentStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___m_indentStack_176;
	// System.Boolean TMPro.TMP_Text::tag_NoParsing
	bool ___tag_NoParsing_177;
	// System.Boolean TMPro.TMP_Text::m_isParsingText
	bool ___m_isParsingText_178;
	// UnityEngine.Matrix4x4 TMPro.TMP_Text::m_FXMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___m_FXMatrix_179;
	// System.Boolean TMPro.TMP_Text::m_isFXMatrixSet
	bool ___m_isFXMatrixSet_180;
	// TMPro.TMP_Text_UnicodeChar[] TMPro.TMP_Text::m_TextParsingBuffer
	UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505* ___m_TextParsingBuffer_181;
	// TMPro.TMP_CharacterInfo[] TMPro.TMP_Text::m_internalCharacterInfo
	TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* ___m_internalCharacterInfo_182;
	// System.Char[] TMPro.TMP_Text::m_input_CharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___m_input_CharArray_183;
	// System.Int32 TMPro.TMP_Text::m_charArray_Length
	int32_t ___m_charArray_Length_184;
	// System.Int32 TMPro.TMP_Text::m_totalCharacterCount
	int32_t ___m_totalCharacterCount_185;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedWordWrapState
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  ___m_SavedWordWrapState_186;
	// TMPro.WordWrapState TMPro.TMP_Text::m_SavedLineState
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  ___m_SavedLineState_187;
	// System.Int32 TMPro.TMP_Text::m_characterCount
	int32_t ___m_characterCount_188;
	// System.Int32 TMPro.TMP_Text::m_firstCharacterOfLine
	int32_t ___m_firstCharacterOfLine_189;
	// System.Int32 TMPro.TMP_Text::m_firstVisibleCharacterOfLine
	int32_t ___m_firstVisibleCharacterOfLine_190;
	// System.Int32 TMPro.TMP_Text::m_lastCharacterOfLine
	int32_t ___m_lastCharacterOfLine_191;
	// System.Int32 TMPro.TMP_Text::m_lastVisibleCharacterOfLine
	int32_t ___m_lastVisibleCharacterOfLine_192;
	// System.Int32 TMPro.TMP_Text::m_lineNumber
	int32_t ___m_lineNumber_193;
	// System.Int32 TMPro.TMP_Text::m_lineVisibleCharacterCount
	int32_t ___m_lineVisibleCharacterCount_194;
	// System.Int32 TMPro.TMP_Text::m_pageNumber
	int32_t ___m_pageNumber_195;
	// System.Single TMPro.TMP_Text::m_maxAscender
	float ___m_maxAscender_196;
	// System.Single TMPro.TMP_Text::m_maxCapHeight
	float ___m_maxCapHeight_197;
	// System.Single TMPro.TMP_Text::m_maxDescender
	float ___m_maxDescender_198;
	// System.Single TMPro.TMP_Text::m_maxLineAscender
	float ___m_maxLineAscender_199;
	// System.Single TMPro.TMP_Text::m_maxLineDescender
	float ___m_maxLineDescender_200;
	// System.Single TMPro.TMP_Text::m_startOfLineAscender
	float ___m_startOfLineAscender_201;
	// System.Single TMPro.TMP_Text::m_lineOffset
	float ___m_lineOffset_202;
	// TMPro.Extents TMPro.TMP_Text::m_meshExtents
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  ___m_meshExtents_203;
	// UnityEngine.Color32 TMPro.TMP_Text::m_htmlColor
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___m_htmlColor_204;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_colorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_colorStack_205;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_underlineColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_underlineColorStack_206;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_strikethroughColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_strikethroughColorStack_207;
	// TMPro.TMP_RichTextTagStack`1<UnityEngine.Color32> TMPro.TMP_Text::m_highlightColorStack
	TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  ___m_highlightColorStack_208;
	// TMPro.TMP_ColorGradient TMPro.TMP_Text::m_colorGradientPreset
	TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * ___m_colorGradientPreset_209;
	// TMPro.TMP_RichTextTagStack`1<TMPro.TMP_ColorGradient> TMPro.TMP_Text::m_colorGradientStack
	TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  ___m_colorGradientStack_210;
	// System.Single TMPro.TMP_Text::m_tabSpacing
	float ___m_tabSpacing_211;
	// System.Single TMPro.TMP_Text::m_spacing
	float ___m_spacing_212;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.TMP_Text::m_styleStack
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___m_styleStack_213;
	// TMPro.TMP_RichTextTagStack`1<System.Int32> TMPro.TMP_Text::m_actionStack
	TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  ___m_actionStack_214;
	// System.Single TMPro.TMP_Text::m_padding
	float ___m_padding_215;
	// System.Single TMPro.TMP_Text::m_baselineOffset
	float ___m_baselineOffset_216;
	// TMPro.TMP_RichTextTagStack`1<System.Single> TMPro.TMP_Text::m_baselineOffsetStack
	TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  ___m_baselineOffsetStack_217;
	// System.Single TMPro.TMP_Text::m_xAdvance
	float ___m_xAdvance_218;
	// TMPro.TMP_TextElementType TMPro.TMP_Text::m_textElementType
	int32_t ___m_textElementType_219;
	// TMPro.TMP_TextElement TMPro.TMP_Text::m_cached_TextElement
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * ___m_cached_TextElement_220;
	// TMPro.TMP_Character TMPro.TMP_Text::m_cached_Underline_Character
	TMP_Character_t1875AACA978396521498D6A699052C187903553D * ___m_cached_Underline_Character_221;
	// TMPro.TMP_Character TMPro.TMP_Text::m_cached_Ellipsis_Character
	TMP_Character_t1875AACA978396521498D6A699052C187903553D * ___m_cached_Ellipsis_Character_222;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_defaultSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_defaultSpriteAsset_223;
	// TMPro.TMP_SpriteAsset TMPro.TMP_Text::m_currentSpriteAsset
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * ___m_currentSpriteAsset_224;
	// System.Int32 TMPro.TMP_Text::m_spriteCount
	int32_t ___m_spriteCount_225;
	// System.Int32 TMPro.TMP_Text::m_spriteIndex
	int32_t ___m_spriteIndex_226;
	// System.Int32 TMPro.TMP_Text::m_spriteAnimationID
	int32_t ___m_spriteAnimationID_227;
	// System.Boolean TMPro.TMP_Text::m_ignoreActiveState
	bool ___m_ignoreActiveState_228;
	// System.Single[] TMPro.TMP_Text::k_Power
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___k_Power_229;

public:
	inline static int32_t get_offset_of_m_text_30() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_text_30)); }
	inline String_t* get_m_text_30() const { return ___m_text_30; }
	inline String_t** get_address_of_m_text_30() { return &___m_text_30; }
	inline void set_m_text_30(String_t* value)
	{
		___m_text_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_text_30), value);
	}

	inline static int32_t get_offset_of_m_isRightToLeft_31() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isRightToLeft_31)); }
	inline bool get_m_isRightToLeft_31() const { return ___m_isRightToLeft_31; }
	inline bool* get_address_of_m_isRightToLeft_31() { return &___m_isRightToLeft_31; }
	inline void set_m_isRightToLeft_31(bool value)
	{
		___m_isRightToLeft_31 = value;
	}

	inline static int32_t get_offset_of_m_fontAsset_32() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontAsset_32)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_m_fontAsset_32() const { return ___m_fontAsset_32; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_m_fontAsset_32() { return &___m_fontAsset_32; }
	inline void set_m_fontAsset_32(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___m_fontAsset_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontAsset_32), value);
	}

	inline static int32_t get_offset_of_m_currentFontAsset_33() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentFontAsset_33)); }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * get_m_currentFontAsset_33() const { return ___m_currentFontAsset_33; }
	inline TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C ** get_address_of_m_currentFontAsset_33() { return &___m_currentFontAsset_33; }
	inline void set_m_currentFontAsset_33(TMP_FontAsset_t44D2006105B39FB33AE5A0ADF07A7EF36C72385C * value)
	{
		___m_currentFontAsset_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentFontAsset_33), value);
	}

	inline static int32_t get_offset_of_m_isSDFShader_34() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isSDFShader_34)); }
	inline bool get_m_isSDFShader_34() const { return ___m_isSDFShader_34; }
	inline bool* get_address_of_m_isSDFShader_34() { return &___m_isSDFShader_34; }
	inline void set_m_isSDFShader_34(bool value)
	{
		___m_isSDFShader_34 = value;
	}

	inline static int32_t get_offset_of_m_sharedMaterial_35() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_sharedMaterial_35)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_sharedMaterial_35() const { return ___m_sharedMaterial_35; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_sharedMaterial_35() { return &___m_sharedMaterial_35; }
	inline void set_m_sharedMaterial_35(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_sharedMaterial_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_sharedMaterial_35), value);
	}

	inline static int32_t get_offset_of_m_currentMaterial_36() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentMaterial_36)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_currentMaterial_36() const { return ___m_currentMaterial_36; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_currentMaterial_36() { return &___m_currentMaterial_36; }
	inline void set_m_currentMaterial_36(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_currentMaterial_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentMaterial_36), value);
	}

	inline static int32_t get_offset_of_m_materialReferences_37() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferences_37)); }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* get_m_materialReferences_37() const { return ___m_materialReferences_37; }
	inline MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B** get_address_of_m_materialReferences_37() { return &___m_materialReferences_37; }
	inline void set_m_materialReferences_37(MaterialReferenceU5BU5D_t01EC9C1C00A504C2EF9FBAF95DE26BB88E9B743B* value)
	{
		___m_materialReferences_37 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferences_37), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceIndexLookup_38() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferenceIndexLookup_38)); }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * get_m_materialReferenceIndexLookup_38() const { return ___m_materialReferenceIndexLookup_38; }
	inline Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E ** get_address_of_m_materialReferenceIndexLookup_38() { return &___m_materialReferenceIndexLookup_38; }
	inline void set_m_materialReferenceIndexLookup_38(Dictionary_2_tB3CE2F81C1BDF4445C80AD88C8FD1AAC91B75D4E * value)
	{
		___m_materialReferenceIndexLookup_38 = value;
		Il2CppCodeGenWriteBarrier((&___m_materialReferenceIndexLookup_38), value);
	}

	inline static int32_t get_offset_of_m_materialReferenceStack_39() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_materialReferenceStack_39)); }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  get_m_materialReferenceStack_39() const { return ___m_materialReferenceStack_39; }
	inline TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9 * get_address_of_m_materialReferenceStack_39() { return &___m_materialReferenceStack_39; }
	inline void set_m_materialReferenceStack_39(TMP_RichTextTagStack_1_t9742B1BC2B58D513502935F599F4AF09FFC379A9  value)
	{
		___m_materialReferenceStack_39 = value;
	}

	inline static int32_t get_offset_of_m_currentMaterialIndex_40() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentMaterialIndex_40)); }
	inline int32_t get_m_currentMaterialIndex_40() const { return ___m_currentMaterialIndex_40; }
	inline int32_t* get_address_of_m_currentMaterialIndex_40() { return &___m_currentMaterialIndex_40; }
	inline void set_m_currentMaterialIndex_40(int32_t value)
	{
		___m_currentMaterialIndex_40 = value;
	}

	inline static int32_t get_offset_of_m_fontSharedMaterials_41() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSharedMaterials_41)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_m_fontSharedMaterials_41() const { return ___m_fontSharedMaterials_41; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_m_fontSharedMaterials_41() { return &___m_fontSharedMaterials_41; }
	inline void set_m_fontSharedMaterials_41(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___m_fontSharedMaterials_41 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontSharedMaterials_41), value);
	}

	inline static int32_t get_offset_of_m_fontMaterial_42() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontMaterial_42)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_fontMaterial_42() const { return ___m_fontMaterial_42; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_fontMaterial_42() { return &___m_fontMaterial_42; }
	inline void set_m_fontMaterial_42(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_fontMaterial_42 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterial_42), value);
	}

	inline static int32_t get_offset_of_m_fontMaterials_43() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontMaterials_43)); }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* get_m_fontMaterials_43() const { return ___m_fontMaterials_43; }
	inline MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398** get_address_of_m_fontMaterials_43() { return &___m_fontMaterials_43; }
	inline void set_m_fontMaterials_43(MaterialU5BU5D_tD2350F98F2A1BB6C7A5FBFE1474DFC47331AB398* value)
	{
		___m_fontMaterials_43 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontMaterials_43), value);
	}

	inline static int32_t get_offset_of_m_isMaterialDirty_44() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isMaterialDirty_44)); }
	inline bool get_m_isMaterialDirty_44() const { return ___m_isMaterialDirty_44; }
	inline bool* get_address_of_m_isMaterialDirty_44() { return &___m_isMaterialDirty_44; }
	inline void set_m_isMaterialDirty_44(bool value)
	{
		___m_isMaterialDirty_44 = value;
	}

	inline static int32_t get_offset_of_m_fontColor32_45() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColor32_45)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_fontColor32_45() const { return ___m_fontColor32_45; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_fontColor32_45() { return &___m_fontColor32_45; }
	inline void set_m_fontColor32_45(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_fontColor32_45 = value;
	}

	inline static int32_t get_offset_of_m_fontColor_46() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColor_46)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_fontColor_46() const { return ___m_fontColor_46; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_fontColor_46() { return &___m_fontColor_46; }
	inline void set_m_fontColor_46(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_fontColor_46 = value;
	}

	inline static int32_t get_offset_of_m_underlineColor_48() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_underlineColor_48)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_underlineColor_48() const { return ___m_underlineColor_48; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_underlineColor_48() { return &___m_underlineColor_48; }
	inline void set_m_underlineColor_48(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_underlineColor_48 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColor_49() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_strikethroughColor_49)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_strikethroughColor_49() const { return ___m_strikethroughColor_49; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_strikethroughColor_49() { return &___m_strikethroughColor_49; }
	inline void set_m_strikethroughColor_49(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_strikethroughColor_49 = value;
	}

	inline static int32_t get_offset_of_m_highlightColor_50() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightColor_50)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_highlightColor_50() const { return ___m_highlightColor_50; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_highlightColor_50() { return &___m_highlightColor_50; }
	inline void set_m_highlightColor_50(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_highlightColor_50 = value;
	}

	inline static int32_t get_offset_of_m_highlightPadding_51() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightPadding_51)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_highlightPadding_51() const { return ___m_highlightPadding_51; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_highlightPadding_51() { return &___m_highlightPadding_51; }
	inline void set_m_highlightPadding_51(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_highlightPadding_51 = value;
	}

	inline static int32_t get_offset_of_m_enableVertexGradient_52() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableVertexGradient_52)); }
	inline bool get_m_enableVertexGradient_52() const { return ___m_enableVertexGradient_52; }
	inline bool* get_address_of_m_enableVertexGradient_52() { return &___m_enableVertexGradient_52; }
	inline void set_m_enableVertexGradient_52(bool value)
	{
		___m_enableVertexGradient_52 = value;
	}

	inline static int32_t get_offset_of_m_colorMode_53() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorMode_53)); }
	inline int32_t get_m_colorMode_53() const { return ___m_colorMode_53; }
	inline int32_t* get_address_of_m_colorMode_53() { return &___m_colorMode_53; }
	inline void set_m_colorMode_53(int32_t value)
	{
		___m_colorMode_53 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradient_54() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColorGradient_54)); }
	inline VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  get_m_fontColorGradient_54() const { return ___m_fontColorGradient_54; }
	inline VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A * get_address_of_m_fontColorGradient_54() { return &___m_fontColorGradient_54; }
	inline void set_m_fontColorGradient_54(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A  value)
	{
		___m_fontColorGradient_54 = value;
	}

	inline static int32_t get_offset_of_m_fontColorGradientPreset_55() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontColorGradientPreset_55)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_fontColorGradientPreset_55() const { return ___m_fontColorGradientPreset_55; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_fontColorGradientPreset_55() { return &___m_fontColorGradientPreset_55; }
	inline void set_m_fontColorGradientPreset_55(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_fontColorGradientPreset_55 = value;
		Il2CppCodeGenWriteBarrier((&___m_fontColorGradientPreset_55), value);
	}

	inline static int32_t get_offset_of_m_spriteAsset_56() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAsset_56)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_spriteAsset_56() const { return ___m_spriteAsset_56; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_spriteAsset_56() { return &___m_spriteAsset_56; }
	inline void set_m_spriteAsset_56(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_spriteAsset_56 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAsset_56), value);
	}

	inline static int32_t get_offset_of_m_tintAllSprites_57() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tintAllSprites_57)); }
	inline bool get_m_tintAllSprites_57() const { return ___m_tintAllSprites_57; }
	inline bool* get_address_of_m_tintAllSprites_57() { return &___m_tintAllSprites_57; }
	inline void set_m_tintAllSprites_57(bool value)
	{
		___m_tintAllSprites_57 = value;
	}

	inline static int32_t get_offset_of_m_tintSprite_58() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tintSprite_58)); }
	inline bool get_m_tintSprite_58() const { return ___m_tintSprite_58; }
	inline bool* get_address_of_m_tintSprite_58() { return &___m_tintSprite_58; }
	inline void set_m_tintSprite_58(bool value)
	{
		___m_tintSprite_58 = value;
	}

	inline static int32_t get_offset_of_m_spriteColor_59() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteColor_59)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_spriteColor_59() const { return ___m_spriteColor_59; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_spriteColor_59() { return &___m_spriteColor_59; }
	inline void set_m_spriteColor_59(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_spriteColor_59 = value;
	}

	inline static int32_t get_offset_of_m_overrideHtmlColors_60() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_overrideHtmlColors_60)); }
	inline bool get_m_overrideHtmlColors_60() const { return ___m_overrideHtmlColors_60; }
	inline bool* get_address_of_m_overrideHtmlColors_60() { return &___m_overrideHtmlColors_60; }
	inline void set_m_overrideHtmlColors_60(bool value)
	{
		___m_overrideHtmlColors_60 = value;
	}

	inline static int32_t get_offset_of_m_faceColor_61() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_faceColor_61)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_faceColor_61() const { return ___m_faceColor_61; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_faceColor_61() { return &___m_faceColor_61; }
	inline void set_m_faceColor_61(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_faceColor_61 = value;
	}

	inline static int32_t get_offset_of_m_outlineColor_62() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_outlineColor_62)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_outlineColor_62() const { return ___m_outlineColor_62; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_outlineColor_62() { return &___m_outlineColor_62; }
	inline void set_m_outlineColor_62(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_outlineColor_62 = value;
	}

	inline static int32_t get_offset_of_m_outlineWidth_63() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_outlineWidth_63)); }
	inline float get_m_outlineWidth_63() const { return ___m_outlineWidth_63; }
	inline float* get_address_of_m_outlineWidth_63() { return &___m_outlineWidth_63; }
	inline void set_m_outlineWidth_63(float value)
	{
		___m_outlineWidth_63 = value;
	}

	inline static int32_t get_offset_of_m_fontSize_64() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSize_64)); }
	inline float get_m_fontSize_64() const { return ___m_fontSize_64; }
	inline float* get_address_of_m_fontSize_64() { return &___m_fontSize_64; }
	inline void set_m_fontSize_64(float value)
	{
		___m_fontSize_64 = value;
	}

	inline static int32_t get_offset_of_m_currentFontSize_65() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentFontSize_65)); }
	inline float get_m_currentFontSize_65() const { return ___m_currentFontSize_65; }
	inline float* get_address_of_m_currentFontSize_65() { return &___m_currentFontSize_65; }
	inline void set_m_currentFontSize_65(float value)
	{
		___m_currentFontSize_65 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeBase_66() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeBase_66)); }
	inline float get_m_fontSizeBase_66() const { return ___m_fontSizeBase_66; }
	inline float* get_address_of_m_fontSizeBase_66() { return &___m_fontSizeBase_66; }
	inline void set_m_fontSizeBase_66(float value)
	{
		___m_fontSizeBase_66 = value;
	}

	inline static int32_t get_offset_of_m_sizeStack_67() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_sizeStack_67)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_m_sizeStack_67() const { return ___m_sizeStack_67; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_m_sizeStack_67() { return &___m_sizeStack_67; }
	inline void set_m_sizeStack_67(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___m_sizeStack_67 = value;
	}

	inline static int32_t get_offset_of_m_fontWeight_68() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontWeight_68)); }
	inline int32_t get_m_fontWeight_68() const { return ___m_fontWeight_68; }
	inline int32_t* get_address_of_m_fontWeight_68() { return &___m_fontWeight_68; }
	inline void set_m_fontWeight_68(int32_t value)
	{
		___m_fontWeight_68 = value;
	}

	inline static int32_t get_offset_of_m_FontWeightInternal_69() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FontWeightInternal_69)); }
	inline int32_t get_m_FontWeightInternal_69() const { return ___m_FontWeightInternal_69; }
	inline int32_t* get_address_of_m_FontWeightInternal_69() { return &___m_FontWeightInternal_69; }
	inline void set_m_FontWeightInternal_69(int32_t value)
	{
		___m_FontWeightInternal_69 = value;
	}

	inline static int32_t get_offset_of_m_FontWeightStack_70() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FontWeightStack_70)); }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  get_m_FontWeightStack_70() const { return ___m_FontWeightStack_70; }
	inline TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B * get_address_of_m_FontWeightStack_70() { return &___m_FontWeightStack_70; }
	inline void set_m_FontWeightStack_70(TMP_RichTextTagStack_1_t9B6C6D23490A525AEA83F4301C7523574055098B  value)
	{
		___m_FontWeightStack_70 = value;
	}

	inline static int32_t get_offset_of_m_enableAutoSizing_71() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableAutoSizing_71)); }
	inline bool get_m_enableAutoSizing_71() const { return ___m_enableAutoSizing_71; }
	inline bool* get_address_of_m_enableAutoSizing_71() { return &___m_enableAutoSizing_71; }
	inline void set_m_enableAutoSizing_71(bool value)
	{
		___m_enableAutoSizing_71 = value;
	}

	inline static int32_t get_offset_of_m_maxFontSize_72() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxFontSize_72)); }
	inline float get_m_maxFontSize_72() const { return ___m_maxFontSize_72; }
	inline float* get_address_of_m_maxFontSize_72() { return &___m_maxFontSize_72; }
	inline void set_m_maxFontSize_72(float value)
	{
		___m_maxFontSize_72 = value;
	}

	inline static int32_t get_offset_of_m_minFontSize_73() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minFontSize_73)); }
	inline float get_m_minFontSize_73() const { return ___m_minFontSize_73; }
	inline float* get_address_of_m_minFontSize_73() { return &___m_minFontSize_73; }
	inline void set_m_minFontSize_73(float value)
	{
		___m_minFontSize_73 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMin_74() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeMin_74)); }
	inline float get_m_fontSizeMin_74() const { return ___m_fontSizeMin_74; }
	inline float* get_address_of_m_fontSizeMin_74() { return &___m_fontSizeMin_74; }
	inline void set_m_fontSizeMin_74(float value)
	{
		___m_fontSizeMin_74 = value;
	}

	inline static int32_t get_offset_of_m_fontSizeMax_75() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontSizeMax_75)); }
	inline float get_m_fontSizeMax_75() const { return ___m_fontSizeMax_75; }
	inline float* get_address_of_m_fontSizeMax_75() { return &___m_fontSizeMax_75; }
	inline void set_m_fontSizeMax_75(float value)
	{
		___m_fontSizeMax_75 = value;
	}

	inline static int32_t get_offset_of_m_fontStyle_76() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontStyle_76)); }
	inline int32_t get_m_fontStyle_76() const { return ___m_fontStyle_76; }
	inline int32_t* get_address_of_m_fontStyle_76() { return &___m_fontStyle_76; }
	inline void set_m_fontStyle_76(int32_t value)
	{
		___m_fontStyle_76 = value;
	}

	inline static int32_t get_offset_of_m_FontStyleInternal_77() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FontStyleInternal_77)); }
	inline int32_t get_m_FontStyleInternal_77() const { return ___m_FontStyleInternal_77; }
	inline int32_t* get_address_of_m_FontStyleInternal_77() { return &___m_FontStyleInternal_77; }
	inline void set_m_FontStyleInternal_77(int32_t value)
	{
		___m_FontStyleInternal_77 = value;
	}

	inline static int32_t get_offset_of_m_fontStyleStack_78() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontStyleStack_78)); }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  get_m_fontStyleStack_78() const { return ___m_fontStyleStack_78; }
	inline TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84 * get_address_of_m_fontStyleStack_78() { return &___m_fontStyleStack_78; }
	inline void set_m_fontStyleStack_78(TMP_FontStyleStack_tC7146DA5AD4540B2C8733862D785AD50AD229E84  value)
	{
		___m_fontStyleStack_78 = value;
	}

	inline static int32_t get_offset_of_m_isUsingBold_79() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isUsingBold_79)); }
	inline bool get_m_isUsingBold_79() const { return ___m_isUsingBold_79; }
	inline bool* get_address_of_m_isUsingBold_79() { return &___m_isUsingBold_79; }
	inline void set_m_isUsingBold_79(bool value)
	{
		___m_isUsingBold_79 = value;
	}

	inline static int32_t get_offset_of_m_textAlignment_80() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textAlignment_80)); }
	inline int32_t get_m_textAlignment_80() const { return ___m_textAlignment_80; }
	inline int32_t* get_address_of_m_textAlignment_80() { return &___m_textAlignment_80; }
	inline void set_m_textAlignment_80(int32_t value)
	{
		___m_textAlignment_80 = value;
	}

	inline static int32_t get_offset_of_m_lineJustification_81() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineJustification_81)); }
	inline int32_t get_m_lineJustification_81() const { return ___m_lineJustification_81; }
	inline int32_t* get_address_of_m_lineJustification_81() { return &___m_lineJustification_81; }
	inline void set_m_lineJustification_81(int32_t value)
	{
		___m_lineJustification_81 = value;
	}

	inline static int32_t get_offset_of_m_lineJustificationStack_82() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineJustificationStack_82)); }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  get_m_lineJustificationStack_82() const { return ___m_lineJustificationStack_82; }
	inline TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115 * get_address_of_m_lineJustificationStack_82() { return &___m_lineJustificationStack_82; }
	inline void set_m_lineJustificationStack_82(TMP_RichTextTagStack_1_t435AA844A7DBDA7E54BCDA3C53622D4B28952115  value)
	{
		___m_lineJustificationStack_82 = value;
	}

	inline static int32_t get_offset_of_m_textContainerLocalCorners_83() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textContainerLocalCorners_83)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_textContainerLocalCorners_83() const { return ___m_textContainerLocalCorners_83; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_textContainerLocalCorners_83() { return &___m_textContainerLocalCorners_83; }
	inline void set_m_textContainerLocalCorners_83(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_textContainerLocalCorners_83 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainerLocalCorners_83), value);
	}

	inline static int32_t get_offset_of_m_characterSpacing_84() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_characterSpacing_84)); }
	inline float get_m_characterSpacing_84() const { return ___m_characterSpacing_84; }
	inline float* get_address_of_m_characterSpacing_84() { return &___m_characterSpacing_84; }
	inline void set_m_characterSpacing_84(float value)
	{
		___m_characterSpacing_84 = value;
	}

	inline static int32_t get_offset_of_m_cSpacing_85() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cSpacing_85)); }
	inline float get_m_cSpacing_85() const { return ___m_cSpacing_85; }
	inline float* get_address_of_m_cSpacing_85() { return &___m_cSpacing_85; }
	inline void set_m_cSpacing_85(float value)
	{
		___m_cSpacing_85 = value;
	}

	inline static int32_t get_offset_of_m_monoSpacing_86() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_monoSpacing_86)); }
	inline float get_m_monoSpacing_86() const { return ___m_monoSpacing_86; }
	inline float* get_address_of_m_monoSpacing_86() { return &___m_monoSpacing_86; }
	inline void set_m_monoSpacing_86(float value)
	{
		___m_monoSpacing_86 = value;
	}

	inline static int32_t get_offset_of_m_wordSpacing_87() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_wordSpacing_87)); }
	inline float get_m_wordSpacing_87() const { return ___m_wordSpacing_87; }
	inline float* get_address_of_m_wordSpacing_87() { return &___m_wordSpacing_87; }
	inline void set_m_wordSpacing_87(float value)
	{
		___m_wordSpacing_87 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacing_88() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacing_88)); }
	inline float get_m_lineSpacing_88() const { return ___m_lineSpacing_88; }
	inline float* get_address_of_m_lineSpacing_88() { return &___m_lineSpacing_88; }
	inline void set_m_lineSpacing_88(float value)
	{
		___m_lineSpacing_88 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingDelta_89() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacingDelta_89)); }
	inline float get_m_lineSpacingDelta_89() const { return ___m_lineSpacingDelta_89; }
	inline float* get_address_of_m_lineSpacingDelta_89() { return &___m_lineSpacingDelta_89; }
	inline void set_m_lineSpacingDelta_89(float value)
	{
		___m_lineSpacingDelta_89 = value;
	}

	inline static int32_t get_offset_of_m_lineHeight_90() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineHeight_90)); }
	inline float get_m_lineHeight_90() const { return ___m_lineHeight_90; }
	inline float* get_address_of_m_lineHeight_90() { return &___m_lineHeight_90; }
	inline void set_m_lineHeight_90(float value)
	{
		___m_lineHeight_90 = value;
	}

	inline static int32_t get_offset_of_m_lineSpacingMax_91() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineSpacingMax_91)); }
	inline float get_m_lineSpacingMax_91() const { return ___m_lineSpacingMax_91; }
	inline float* get_address_of_m_lineSpacingMax_91() { return &___m_lineSpacingMax_91; }
	inline void set_m_lineSpacingMax_91(float value)
	{
		___m_lineSpacingMax_91 = value;
	}

	inline static int32_t get_offset_of_m_paragraphSpacing_92() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_paragraphSpacing_92)); }
	inline float get_m_paragraphSpacing_92() const { return ___m_paragraphSpacing_92; }
	inline float* get_address_of_m_paragraphSpacing_92() { return &___m_paragraphSpacing_92; }
	inline void set_m_paragraphSpacing_92(float value)
	{
		___m_paragraphSpacing_92 = value;
	}

	inline static int32_t get_offset_of_m_charWidthMaxAdj_93() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charWidthMaxAdj_93)); }
	inline float get_m_charWidthMaxAdj_93() const { return ___m_charWidthMaxAdj_93; }
	inline float* get_address_of_m_charWidthMaxAdj_93() { return &___m_charWidthMaxAdj_93; }
	inline void set_m_charWidthMaxAdj_93(float value)
	{
		___m_charWidthMaxAdj_93 = value;
	}

	inline static int32_t get_offset_of_m_charWidthAdjDelta_94() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charWidthAdjDelta_94)); }
	inline float get_m_charWidthAdjDelta_94() const { return ___m_charWidthAdjDelta_94; }
	inline float* get_address_of_m_charWidthAdjDelta_94() { return &___m_charWidthAdjDelta_94; }
	inline void set_m_charWidthAdjDelta_94(float value)
	{
		___m_charWidthAdjDelta_94 = value;
	}

	inline static int32_t get_offset_of_m_enableWordWrapping_95() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableWordWrapping_95)); }
	inline bool get_m_enableWordWrapping_95() const { return ___m_enableWordWrapping_95; }
	inline bool* get_address_of_m_enableWordWrapping_95() { return &___m_enableWordWrapping_95; }
	inline void set_m_enableWordWrapping_95(bool value)
	{
		___m_enableWordWrapping_95 = value;
	}

	inline static int32_t get_offset_of_m_isCharacterWrappingEnabled_96() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCharacterWrappingEnabled_96)); }
	inline bool get_m_isCharacterWrappingEnabled_96() const { return ___m_isCharacterWrappingEnabled_96; }
	inline bool* get_address_of_m_isCharacterWrappingEnabled_96() { return &___m_isCharacterWrappingEnabled_96; }
	inline void set_m_isCharacterWrappingEnabled_96(bool value)
	{
		___m_isCharacterWrappingEnabled_96 = value;
	}

	inline static int32_t get_offset_of_m_isNonBreakingSpace_97() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isNonBreakingSpace_97)); }
	inline bool get_m_isNonBreakingSpace_97() const { return ___m_isNonBreakingSpace_97; }
	inline bool* get_address_of_m_isNonBreakingSpace_97() { return &___m_isNonBreakingSpace_97; }
	inline void set_m_isNonBreakingSpace_97(bool value)
	{
		___m_isNonBreakingSpace_97 = value;
	}

	inline static int32_t get_offset_of_m_isIgnoringAlignment_98() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isIgnoringAlignment_98)); }
	inline bool get_m_isIgnoringAlignment_98() const { return ___m_isIgnoringAlignment_98; }
	inline bool* get_address_of_m_isIgnoringAlignment_98() { return &___m_isIgnoringAlignment_98; }
	inline void set_m_isIgnoringAlignment_98(bool value)
	{
		___m_isIgnoringAlignment_98 = value;
	}

	inline static int32_t get_offset_of_m_wordWrappingRatios_99() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_wordWrappingRatios_99)); }
	inline float get_m_wordWrappingRatios_99() const { return ___m_wordWrappingRatios_99; }
	inline float* get_address_of_m_wordWrappingRatios_99() { return &___m_wordWrappingRatios_99; }
	inline void set_m_wordWrappingRatios_99(float value)
	{
		___m_wordWrappingRatios_99 = value;
	}

	inline static int32_t get_offset_of_m_overflowMode_100() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_overflowMode_100)); }
	inline int32_t get_m_overflowMode_100() const { return ___m_overflowMode_100; }
	inline int32_t* get_address_of_m_overflowMode_100() { return &___m_overflowMode_100; }
	inline void set_m_overflowMode_100(int32_t value)
	{
		___m_overflowMode_100 = value;
	}

	inline static int32_t get_offset_of_m_firstOverflowCharacterIndex_101() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstOverflowCharacterIndex_101)); }
	inline int32_t get_m_firstOverflowCharacterIndex_101() const { return ___m_firstOverflowCharacterIndex_101; }
	inline int32_t* get_address_of_m_firstOverflowCharacterIndex_101() { return &___m_firstOverflowCharacterIndex_101; }
	inline void set_m_firstOverflowCharacterIndex_101(int32_t value)
	{
		___m_firstOverflowCharacterIndex_101 = value;
	}

	inline static int32_t get_offset_of_m_linkedTextComponent_102() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_linkedTextComponent_102)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_m_linkedTextComponent_102() const { return ___m_linkedTextComponent_102; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_m_linkedTextComponent_102() { return &___m_linkedTextComponent_102; }
	inline void set_m_linkedTextComponent_102(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___m_linkedTextComponent_102 = value;
		Il2CppCodeGenWriteBarrier((&___m_linkedTextComponent_102), value);
	}

	inline static int32_t get_offset_of_m_isLinkedTextComponent_103() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isLinkedTextComponent_103)); }
	inline bool get_m_isLinkedTextComponent_103() const { return ___m_isLinkedTextComponent_103; }
	inline bool* get_address_of_m_isLinkedTextComponent_103() { return &___m_isLinkedTextComponent_103; }
	inline void set_m_isLinkedTextComponent_103(bool value)
	{
		___m_isLinkedTextComponent_103 = value;
	}

	inline static int32_t get_offset_of_m_isTextTruncated_104() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isTextTruncated_104)); }
	inline bool get_m_isTextTruncated_104() const { return ___m_isTextTruncated_104; }
	inline bool* get_address_of_m_isTextTruncated_104() { return &___m_isTextTruncated_104; }
	inline void set_m_isTextTruncated_104(bool value)
	{
		___m_isTextTruncated_104 = value;
	}

	inline static int32_t get_offset_of_m_enableKerning_105() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableKerning_105)); }
	inline bool get_m_enableKerning_105() const { return ___m_enableKerning_105; }
	inline bool* get_address_of_m_enableKerning_105() { return &___m_enableKerning_105; }
	inline void set_m_enableKerning_105(bool value)
	{
		___m_enableKerning_105 = value;
	}

	inline static int32_t get_offset_of_m_enableExtraPadding_106() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_enableExtraPadding_106)); }
	inline bool get_m_enableExtraPadding_106() const { return ___m_enableExtraPadding_106; }
	inline bool* get_address_of_m_enableExtraPadding_106() { return &___m_enableExtraPadding_106; }
	inline void set_m_enableExtraPadding_106(bool value)
	{
		___m_enableExtraPadding_106 = value;
	}

	inline static int32_t get_offset_of_checkPaddingRequired_107() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___checkPaddingRequired_107)); }
	inline bool get_checkPaddingRequired_107() const { return ___checkPaddingRequired_107; }
	inline bool* get_address_of_checkPaddingRequired_107() { return &___checkPaddingRequired_107; }
	inline void set_checkPaddingRequired_107(bool value)
	{
		___checkPaddingRequired_107 = value;
	}

	inline static int32_t get_offset_of_m_isRichText_108() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isRichText_108)); }
	inline bool get_m_isRichText_108() const { return ___m_isRichText_108; }
	inline bool* get_address_of_m_isRichText_108() { return &___m_isRichText_108; }
	inline void set_m_isRichText_108(bool value)
	{
		___m_isRichText_108 = value;
	}

	inline static int32_t get_offset_of_m_parseCtrlCharacters_109() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_parseCtrlCharacters_109)); }
	inline bool get_m_parseCtrlCharacters_109() const { return ___m_parseCtrlCharacters_109; }
	inline bool* get_address_of_m_parseCtrlCharacters_109() { return &___m_parseCtrlCharacters_109; }
	inline void set_m_parseCtrlCharacters_109(bool value)
	{
		___m_parseCtrlCharacters_109 = value;
	}

	inline static int32_t get_offset_of_m_isOverlay_110() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isOverlay_110)); }
	inline bool get_m_isOverlay_110() const { return ___m_isOverlay_110; }
	inline bool* get_address_of_m_isOverlay_110() { return &___m_isOverlay_110; }
	inline void set_m_isOverlay_110(bool value)
	{
		___m_isOverlay_110 = value;
	}

	inline static int32_t get_offset_of_m_isOrthographic_111() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isOrthographic_111)); }
	inline bool get_m_isOrthographic_111() const { return ___m_isOrthographic_111; }
	inline bool* get_address_of_m_isOrthographic_111() { return &___m_isOrthographic_111; }
	inline void set_m_isOrthographic_111(bool value)
	{
		___m_isOrthographic_111 = value;
	}

	inline static int32_t get_offset_of_m_isCullingEnabled_112() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCullingEnabled_112)); }
	inline bool get_m_isCullingEnabled_112() const { return ___m_isCullingEnabled_112; }
	inline bool* get_address_of_m_isCullingEnabled_112() { return &___m_isCullingEnabled_112; }
	inline void set_m_isCullingEnabled_112(bool value)
	{
		___m_isCullingEnabled_112 = value;
	}

	inline static int32_t get_offset_of_m_ignoreRectMaskCulling_113() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreRectMaskCulling_113)); }
	inline bool get_m_ignoreRectMaskCulling_113() const { return ___m_ignoreRectMaskCulling_113; }
	inline bool* get_address_of_m_ignoreRectMaskCulling_113() { return &___m_ignoreRectMaskCulling_113; }
	inline void set_m_ignoreRectMaskCulling_113(bool value)
	{
		___m_ignoreRectMaskCulling_113 = value;
	}

	inline static int32_t get_offset_of_m_ignoreCulling_114() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreCulling_114)); }
	inline bool get_m_ignoreCulling_114() const { return ___m_ignoreCulling_114; }
	inline bool* get_address_of_m_ignoreCulling_114() { return &___m_ignoreCulling_114; }
	inline void set_m_ignoreCulling_114(bool value)
	{
		___m_ignoreCulling_114 = value;
	}

	inline static int32_t get_offset_of_m_horizontalMapping_115() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_horizontalMapping_115)); }
	inline int32_t get_m_horizontalMapping_115() const { return ___m_horizontalMapping_115; }
	inline int32_t* get_address_of_m_horizontalMapping_115() { return &___m_horizontalMapping_115; }
	inline void set_m_horizontalMapping_115(int32_t value)
	{
		___m_horizontalMapping_115 = value;
	}

	inline static int32_t get_offset_of_m_verticalMapping_116() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_verticalMapping_116)); }
	inline int32_t get_m_verticalMapping_116() const { return ___m_verticalMapping_116; }
	inline int32_t* get_address_of_m_verticalMapping_116() { return &___m_verticalMapping_116; }
	inline void set_m_verticalMapping_116(int32_t value)
	{
		___m_verticalMapping_116 = value;
	}

	inline static int32_t get_offset_of_m_uvLineOffset_117() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_uvLineOffset_117)); }
	inline float get_m_uvLineOffset_117() const { return ___m_uvLineOffset_117; }
	inline float* get_address_of_m_uvLineOffset_117() { return &___m_uvLineOffset_117; }
	inline void set_m_uvLineOffset_117(float value)
	{
		___m_uvLineOffset_117 = value;
	}

	inline static int32_t get_offset_of_m_renderMode_118() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderMode_118)); }
	inline int32_t get_m_renderMode_118() const { return ___m_renderMode_118; }
	inline int32_t* get_address_of_m_renderMode_118() { return &___m_renderMode_118; }
	inline void set_m_renderMode_118(int32_t value)
	{
		___m_renderMode_118 = value;
	}

	inline static int32_t get_offset_of_m_geometrySortingOrder_119() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_geometrySortingOrder_119)); }
	inline int32_t get_m_geometrySortingOrder_119() const { return ___m_geometrySortingOrder_119; }
	inline int32_t* get_address_of_m_geometrySortingOrder_119() { return &___m_geometrySortingOrder_119; }
	inline void set_m_geometrySortingOrder_119(int32_t value)
	{
		___m_geometrySortingOrder_119 = value;
	}

	inline static int32_t get_offset_of_m_VertexBufferAutoSizeReduction_120() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_VertexBufferAutoSizeReduction_120)); }
	inline bool get_m_VertexBufferAutoSizeReduction_120() const { return ___m_VertexBufferAutoSizeReduction_120; }
	inline bool* get_address_of_m_VertexBufferAutoSizeReduction_120() { return &___m_VertexBufferAutoSizeReduction_120; }
	inline void set_m_VertexBufferAutoSizeReduction_120(bool value)
	{
		___m_VertexBufferAutoSizeReduction_120 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacter_121() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstVisibleCharacter_121)); }
	inline int32_t get_m_firstVisibleCharacter_121() const { return ___m_firstVisibleCharacter_121; }
	inline int32_t* get_address_of_m_firstVisibleCharacter_121() { return &___m_firstVisibleCharacter_121; }
	inline void set_m_firstVisibleCharacter_121(int32_t value)
	{
		___m_firstVisibleCharacter_121 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleCharacters_122() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleCharacters_122)); }
	inline int32_t get_m_maxVisibleCharacters_122() const { return ___m_maxVisibleCharacters_122; }
	inline int32_t* get_address_of_m_maxVisibleCharacters_122() { return &___m_maxVisibleCharacters_122; }
	inline void set_m_maxVisibleCharacters_122(int32_t value)
	{
		___m_maxVisibleCharacters_122 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleWords_123() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleWords_123)); }
	inline int32_t get_m_maxVisibleWords_123() const { return ___m_maxVisibleWords_123; }
	inline int32_t* get_address_of_m_maxVisibleWords_123() { return &___m_maxVisibleWords_123; }
	inline void set_m_maxVisibleWords_123(int32_t value)
	{
		___m_maxVisibleWords_123 = value;
	}

	inline static int32_t get_offset_of_m_maxVisibleLines_124() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxVisibleLines_124)); }
	inline int32_t get_m_maxVisibleLines_124() const { return ___m_maxVisibleLines_124; }
	inline int32_t* get_address_of_m_maxVisibleLines_124() { return &___m_maxVisibleLines_124; }
	inline void set_m_maxVisibleLines_124(int32_t value)
	{
		___m_maxVisibleLines_124 = value;
	}

	inline static int32_t get_offset_of_m_useMaxVisibleDescender_125() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_useMaxVisibleDescender_125)); }
	inline bool get_m_useMaxVisibleDescender_125() const { return ___m_useMaxVisibleDescender_125; }
	inline bool* get_address_of_m_useMaxVisibleDescender_125() { return &___m_useMaxVisibleDescender_125; }
	inline void set_m_useMaxVisibleDescender_125(bool value)
	{
		___m_useMaxVisibleDescender_125 = value;
	}

	inline static int32_t get_offset_of_m_pageToDisplay_126() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_pageToDisplay_126)); }
	inline int32_t get_m_pageToDisplay_126() const { return ___m_pageToDisplay_126; }
	inline int32_t* get_address_of_m_pageToDisplay_126() { return &___m_pageToDisplay_126; }
	inline void set_m_pageToDisplay_126(int32_t value)
	{
		___m_pageToDisplay_126 = value;
	}

	inline static int32_t get_offset_of_m_isNewPage_127() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isNewPage_127)); }
	inline bool get_m_isNewPage_127() const { return ___m_isNewPage_127; }
	inline bool* get_address_of_m_isNewPage_127() { return &___m_isNewPage_127; }
	inline void set_m_isNewPage_127(bool value)
	{
		___m_isNewPage_127 = value;
	}

	inline static int32_t get_offset_of_m_margin_128() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_margin_128)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_m_margin_128() const { return ___m_margin_128; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_m_margin_128() { return &___m_margin_128; }
	inline void set_m_margin_128(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___m_margin_128 = value;
	}

	inline static int32_t get_offset_of_m_marginLeft_129() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginLeft_129)); }
	inline float get_m_marginLeft_129() const { return ___m_marginLeft_129; }
	inline float* get_address_of_m_marginLeft_129() { return &___m_marginLeft_129; }
	inline void set_m_marginLeft_129(float value)
	{
		___m_marginLeft_129 = value;
	}

	inline static int32_t get_offset_of_m_marginRight_130() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginRight_130)); }
	inline float get_m_marginRight_130() const { return ___m_marginRight_130; }
	inline float* get_address_of_m_marginRight_130() { return &___m_marginRight_130; }
	inline void set_m_marginRight_130(float value)
	{
		___m_marginRight_130 = value;
	}

	inline static int32_t get_offset_of_m_marginWidth_131() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginWidth_131)); }
	inline float get_m_marginWidth_131() const { return ___m_marginWidth_131; }
	inline float* get_address_of_m_marginWidth_131() { return &___m_marginWidth_131; }
	inline void set_m_marginWidth_131(float value)
	{
		___m_marginWidth_131 = value;
	}

	inline static int32_t get_offset_of_m_marginHeight_132() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_marginHeight_132)); }
	inline float get_m_marginHeight_132() const { return ___m_marginHeight_132; }
	inline float* get_address_of_m_marginHeight_132() { return &___m_marginHeight_132; }
	inline void set_m_marginHeight_132(float value)
	{
		___m_marginHeight_132 = value;
	}

	inline static int32_t get_offset_of_m_width_133() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_width_133)); }
	inline float get_m_width_133() const { return ___m_width_133; }
	inline float* get_address_of_m_width_133() { return &___m_width_133; }
	inline void set_m_width_133(float value)
	{
		___m_width_133 = value;
	}

	inline static int32_t get_offset_of_m_textInfo_134() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textInfo_134)); }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * get_m_textInfo_134() const { return ___m_textInfo_134; }
	inline TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 ** get_address_of_m_textInfo_134() { return &___m_textInfo_134; }
	inline void set_m_textInfo_134(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181 * value)
	{
		___m_textInfo_134 = value;
		Il2CppCodeGenWriteBarrier((&___m_textInfo_134), value);
	}

	inline static int32_t get_offset_of_m_havePropertiesChanged_135() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_havePropertiesChanged_135)); }
	inline bool get_m_havePropertiesChanged_135() const { return ___m_havePropertiesChanged_135; }
	inline bool* get_address_of_m_havePropertiesChanged_135() { return &___m_havePropertiesChanged_135; }
	inline void set_m_havePropertiesChanged_135(bool value)
	{
		___m_havePropertiesChanged_135 = value;
	}

	inline static int32_t get_offset_of_m_isUsingLegacyAnimationComponent_136() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isUsingLegacyAnimationComponent_136)); }
	inline bool get_m_isUsingLegacyAnimationComponent_136() const { return ___m_isUsingLegacyAnimationComponent_136; }
	inline bool* get_address_of_m_isUsingLegacyAnimationComponent_136() { return &___m_isUsingLegacyAnimationComponent_136; }
	inline void set_m_isUsingLegacyAnimationComponent_136(bool value)
	{
		___m_isUsingLegacyAnimationComponent_136 = value;
	}

	inline static int32_t get_offset_of_m_transform_137() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_transform_137)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_transform_137() const { return ___m_transform_137; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_transform_137() { return &___m_transform_137; }
	inline void set_m_transform_137(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_transform_137 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_137), value);
	}

	inline static int32_t get_offset_of_m_rectTransform_138() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_rectTransform_138)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_rectTransform_138() const { return ___m_rectTransform_138; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_rectTransform_138() { return &___m_rectTransform_138; }
	inline void set_m_rectTransform_138(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_rectTransform_138 = value;
		Il2CppCodeGenWriteBarrier((&___m_rectTransform_138), value);
	}

	inline static int32_t get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_139() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___U3CautoSizeTextContainerU3Ek__BackingField_139)); }
	inline bool get_U3CautoSizeTextContainerU3Ek__BackingField_139() const { return ___U3CautoSizeTextContainerU3Ek__BackingField_139; }
	inline bool* get_address_of_U3CautoSizeTextContainerU3Ek__BackingField_139() { return &___U3CautoSizeTextContainerU3Ek__BackingField_139; }
	inline void set_U3CautoSizeTextContainerU3Ek__BackingField_139(bool value)
	{
		___U3CautoSizeTextContainerU3Ek__BackingField_139 = value;
	}

	inline static int32_t get_offset_of_m_autoSizeTextContainer_140() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_autoSizeTextContainer_140)); }
	inline bool get_m_autoSizeTextContainer_140() const { return ___m_autoSizeTextContainer_140; }
	inline bool* get_address_of_m_autoSizeTextContainer_140() { return &___m_autoSizeTextContainer_140; }
	inline void set_m_autoSizeTextContainer_140(bool value)
	{
		___m_autoSizeTextContainer_140 = value;
	}

	inline static int32_t get_offset_of_m_mesh_141() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_mesh_141)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_mesh_141() const { return ___m_mesh_141; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_mesh_141() { return &___m_mesh_141; }
	inline void set_m_mesh_141(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_mesh_141 = value;
		Il2CppCodeGenWriteBarrier((&___m_mesh_141), value);
	}

	inline static int32_t get_offset_of_m_isVolumetricText_142() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isVolumetricText_142)); }
	inline bool get_m_isVolumetricText_142() const { return ___m_isVolumetricText_142; }
	inline bool* get_address_of_m_isVolumetricText_142() { return &___m_isVolumetricText_142; }
	inline void set_m_isVolumetricText_142(bool value)
	{
		___m_isVolumetricText_142 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimator_143() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAnimator_143)); }
	inline TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * get_m_spriteAnimator_143() const { return ___m_spriteAnimator_143; }
	inline TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 ** get_address_of_m_spriteAnimator_143() { return &___m_spriteAnimator_143; }
	inline void set_m_spriteAnimator_143(TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17 * value)
	{
		___m_spriteAnimator_143 = value;
		Il2CppCodeGenWriteBarrier((&___m_spriteAnimator_143), value);
	}

	inline static int32_t get_offset_of_m_flexibleHeight_144() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_flexibleHeight_144)); }
	inline float get_m_flexibleHeight_144() const { return ___m_flexibleHeight_144; }
	inline float* get_address_of_m_flexibleHeight_144() { return &___m_flexibleHeight_144; }
	inline void set_m_flexibleHeight_144(float value)
	{
		___m_flexibleHeight_144 = value;
	}

	inline static int32_t get_offset_of_m_flexibleWidth_145() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_flexibleWidth_145)); }
	inline float get_m_flexibleWidth_145() const { return ___m_flexibleWidth_145; }
	inline float* get_address_of_m_flexibleWidth_145() { return &___m_flexibleWidth_145; }
	inline void set_m_flexibleWidth_145(float value)
	{
		___m_flexibleWidth_145 = value;
	}

	inline static int32_t get_offset_of_m_minWidth_146() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minWidth_146)); }
	inline float get_m_minWidth_146() const { return ___m_minWidth_146; }
	inline float* get_address_of_m_minWidth_146() { return &___m_minWidth_146; }
	inline void set_m_minWidth_146(float value)
	{
		___m_minWidth_146 = value;
	}

	inline static int32_t get_offset_of_m_minHeight_147() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_minHeight_147)); }
	inline float get_m_minHeight_147() const { return ___m_minHeight_147; }
	inline float* get_address_of_m_minHeight_147() { return &___m_minHeight_147; }
	inline void set_m_minHeight_147(float value)
	{
		___m_minHeight_147 = value;
	}

	inline static int32_t get_offset_of_m_maxWidth_148() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxWidth_148)); }
	inline float get_m_maxWidth_148() const { return ___m_maxWidth_148; }
	inline float* get_address_of_m_maxWidth_148() { return &___m_maxWidth_148; }
	inline void set_m_maxWidth_148(float value)
	{
		___m_maxWidth_148 = value;
	}

	inline static int32_t get_offset_of_m_maxHeight_149() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxHeight_149)); }
	inline float get_m_maxHeight_149() const { return ___m_maxHeight_149; }
	inline float* get_address_of_m_maxHeight_149() { return &___m_maxHeight_149; }
	inline void set_m_maxHeight_149(float value)
	{
		___m_maxHeight_149 = value;
	}

	inline static int32_t get_offset_of_m_LayoutElement_150() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_LayoutElement_150)); }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * get_m_LayoutElement_150() const { return ___m_LayoutElement_150; }
	inline LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B ** get_address_of_m_LayoutElement_150() { return &___m_LayoutElement_150; }
	inline void set_m_LayoutElement_150(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B * value)
	{
		___m_LayoutElement_150 = value;
		Il2CppCodeGenWriteBarrier((&___m_LayoutElement_150), value);
	}

	inline static int32_t get_offset_of_m_preferredWidth_151() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_preferredWidth_151)); }
	inline float get_m_preferredWidth_151() const { return ___m_preferredWidth_151; }
	inline float* get_address_of_m_preferredWidth_151() { return &___m_preferredWidth_151; }
	inline void set_m_preferredWidth_151(float value)
	{
		___m_preferredWidth_151 = value;
	}

	inline static int32_t get_offset_of_m_renderedWidth_152() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderedWidth_152)); }
	inline float get_m_renderedWidth_152() const { return ___m_renderedWidth_152; }
	inline float* get_address_of_m_renderedWidth_152() { return &___m_renderedWidth_152; }
	inline void set_m_renderedWidth_152(float value)
	{
		___m_renderedWidth_152 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredWidthDirty_153() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isPreferredWidthDirty_153)); }
	inline bool get_m_isPreferredWidthDirty_153() const { return ___m_isPreferredWidthDirty_153; }
	inline bool* get_address_of_m_isPreferredWidthDirty_153() { return &___m_isPreferredWidthDirty_153; }
	inline void set_m_isPreferredWidthDirty_153(bool value)
	{
		___m_isPreferredWidthDirty_153 = value;
	}

	inline static int32_t get_offset_of_m_preferredHeight_154() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_preferredHeight_154)); }
	inline float get_m_preferredHeight_154() const { return ___m_preferredHeight_154; }
	inline float* get_address_of_m_preferredHeight_154() { return &___m_preferredHeight_154; }
	inline void set_m_preferredHeight_154(float value)
	{
		___m_preferredHeight_154 = value;
	}

	inline static int32_t get_offset_of_m_renderedHeight_155() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_renderedHeight_155)); }
	inline float get_m_renderedHeight_155() const { return ___m_renderedHeight_155; }
	inline float* get_address_of_m_renderedHeight_155() { return &___m_renderedHeight_155; }
	inline void set_m_renderedHeight_155(float value)
	{
		___m_renderedHeight_155 = value;
	}

	inline static int32_t get_offset_of_m_isPreferredHeightDirty_156() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isPreferredHeightDirty_156)); }
	inline bool get_m_isPreferredHeightDirty_156() const { return ___m_isPreferredHeightDirty_156; }
	inline bool* get_address_of_m_isPreferredHeightDirty_156() { return &___m_isPreferredHeightDirty_156; }
	inline void set_m_isPreferredHeightDirty_156(bool value)
	{
		___m_isPreferredHeightDirty_156 = value;
	}

	inline static int32_t get_offset_of_m_isCalculatingPreferredValues_157() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCalculatingPreferredValues_157)); }
	inline bool get_m_isCalculatingPreferredValues_157() const { return ___m_isCalculatingPreferredValues_157; }
	inline bool* get_address_of_m_isCalculatingPreferredValues_157() { return &___m_isCalculatingPreferredValues_157; }
	inline void set_m_isCalculatingPreferredValues_157(bool value)
	{
		___m_isCalculatingPreferredValues_157 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCount_158() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_recursiveCount_158)); }
	inline int32_t get_m_recursiveCount_158() const { return ___m_recursiveCount_158; }
	inline int32_t* get_address_of_m_recursiveCount_158() { return &___m_recursiveCount_158; }
	inline void set_m_recursiveCount_158(int32_t value)
	{
		___m_recursiveCount_158 = value;
	}

	inline static int32_t get_offset_of_m_layoutPriority_159() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_layoutPriority_159)); }
	inline int32_t get_m_layoutPriority_159() const { return ___m_layoutPriority_159; }
	inline int32_t* get_address_of_m_layoutPriority_159() { return &___m_layoutPriority_159; }
	inline void set_m_layoutPriority_159(int32_t value)
	{
		___m_layoutPriority_159 = value;
	}

	inline static int32_t get_offset_of_m_isCalculateSizeRequired_160() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isCalculateSizeRequired_160)); }
	inline bool get_m_isCalculateSizeRequired_160() const { return ___m_isCalculateSizeRequired_160; }
	inline bool* get_address_of_m_isCalculateSizeRequired_160() { return &___m_isCalculateSizeRequired_160; }
	inline void set_m_isCalculateSizeRequired_160(bool value)
	{
		___m_isCalculateSizeRequired_160 = value;
	}

	inline static int32_t get_offset_of_m_isLayoutDirty_161() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isLayoutDirty_161)); }
	inline bool get_m_isLayoutDirty_161() const { return ___m_isLayoutDirty_161; }
	inline bool* get_address_of_m_isLayoutDirty_161() { return &___m_isLayoutDirty_161; }
	inline void set_m_isLayoutDirty_161(bool value)
	{
		___m_isLayoutDirty_161 = value;
	}

	inline static int32_t get_offset_of_m_verticesAlreadyDirty_162() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_verticesAlreadyDirty_162)); }
	inline bool get_m_verticesAlreadyDirty_162() const { return ___m_verticesAlreadyDirty_162; }
	inline bool* get_address_of_m_verticesAlreadyDirty_162() { return &___m_verticesAlreadyDirty_162; }
	inline void set_m_verticesAlreadyDirty_162(bool value)
	{
		___m_verticesAlreadyDirty_162 = value;
	}

	inline static int32_t get_offset_of_m_layoutAlreadyDirty_163() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_layoutAlreadyDirty_163)); }
	inline bool get_m_layoutAlreadyDirty_163() const { return ___m_layoutAlreadyDirty_163; }
	inline bool* get_address_of_m_layoutAlreadyDirty_163() { return &___m_layoutAlreadyDirty_163; }
	inline void set_m_layoutAlreadyDirty_163(bool value)
	{
		___m_layoutAlreadyDirty_163 = value;
	}

	inline static int32_t get_offset_of_m_isAwake_164() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isAwake_164)); }
	inline bool get_m_isAwake_164() const { return ___m_isAwake_164; }
	inline bool* get_address_of_m_isAwake_164() { return &___m_isAwake_164; }
	inline void set_m_isAwake_164(bool value)
	{
		___m_isAwake_164 = value;
	}

	inline static int32_t get_offset_of_m_isWaitingOnResourceLoad_165() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isWaitingOnResourceLoad_165)); }
	inline bool get_m_isWaitingOnResourceLoad_165() const { return ___m_isWaitingOnResourceLoad_165; }
	inline bool* get_address_of_m_isWaitingOnResourceLoad_165() { return &___m_isWaitingOnResourceLoad_165; }
	inline void set_m_isWaitingOnResourceLoad_165(bool value)
	{
		___m_isWaitingOnResourceLoad_165 = value;
	}

	inline static int32_t get_offset_of_m_isInputParsingRequired_166() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isInputParsingRequired_166)); }
	inline bool get_m_isInputParsingRequired_166() const { return ___m_isInputParsingRequired_166; }
	inline bool* get_address_of_m_isInputParsingRequired_166() { return &___m_isInputParsingRequired_166; }
	inline void set_m_isInputParsingRequired_166(bool value)
	{
		___m_isInputParsingRequired_166 = value;
	}

	inline static int32_t get_offset_of_m_inputSource_167() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_inputSource_167)); }
	inline int32_t get_m_inputSource_167() const { return ___m_inputSource_167; }
	inline int32_t* get_address_of_m_inputSource_167() { return &___m_inputSource_167; }
	inline void set_m_inputSource_167(int32_t value)
	{
		___m_inputSource_167 = value;
	}

	inline static int32_t get_offset_of_old_text_168() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___old_text_168)); }
	inline String_t* get_old_text_168() const { return ___old_text_168; }
	inline String_t** get_address_of_old_text_168() { return &___old_text_168; }
	inline void set_old_text_168(String_t* value)
	{
		___old_text_168 = value;
		Il2CppCodeGenWriteBarrier((&___old_text_168), value);
	}

	inline static int32_t get_offset_of_m_fontScale_169() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontScale_169)); }
	inline float get_m_fontScale_169() const { return ___m_fontScale_169; }
	inline float* get_address_of_m_fontScale_169() { return &___m_fontScale_169; }
	inline void set_m_fontScale_169(float value)
	{
		___m_fontScale_169 = value;
	}

	inline static int32_t get_offset_of_m_fontScaleMultiplier_170() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_fontScaleMultiplier_170)); }
	inline float get_m_fontScaleMultiplier_170() const { return ___m_fontScaleMultiplier_170; }
	inline float* get_address_of_m_fontScaleMultiplier_170() { return &___m_fontScaleMultiplier_170; }
	inline void set_m_fontScaleMultiplier_170(float value)
	{
		___m_fontScaleMultiplier_170 = value;
	}

	inline static int32_t get_offset_of_m_htmlTag_171() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_htmlTag_171)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_htmlTag_171() const { return ___m_htmlTag_171; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_htmlTag_171() { return &___m_htmlTag_171; }
	inline void set_m_htmlTag_171(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_htmlTag_171 = value;
		Il2CppCodeGenWriteBarrier((&___m_htmlTag_171), value);
	}

	inline static int32_t get_offset_of_m_xmlAttribute_172() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_xmlAttribute_172)); }
	inline RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652* get_m_xmlAttribute_172() const { return ___m_xmlAttribute_172; }
	inline RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652** get_address_of_m_xmlAttribute_172() { return &___m_xmlAttribute_172; }
	inline void set_m_xmlAttribute_172(RichTextTagAttributeU5BU5D_tDDFB2F68801310D7EEE16822832E48E70B11C652* value)
	{
		___m_xmlAttribute_172 = value;
		Il2CppCodeGenWriteBarrier((&___m_xmlAttribute_172), value);
	}

	inline static int32_t get_offset_of_m_attributeParameterValues_173() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_attributeParameterValues_173)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_m_attributeParameterValues_173() const { return ___m_attributeParameterValues_173; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_m_attributeParameterValues_173() { return &___m_attributeParameterValues_173; }
	inline void set_m_attributeParameterValues_173(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___m_attributeParameterValues_173 = value;
		Il2CppCodeGenWriteBarrier((&___m_attributeParameterValues_173), value);
	}

	inline static int32_t get_offset_of_tag_LineIndent_174() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_LineIndent_174)); }
	inline float get_tag_LineIndent_174() const { return ___tag_LineIndent_174; }
	inline float* get_address_of_tag_LineIndent_174() { return &___tag_LineIndent_174; }
	inline void set_tag_LineIndent_174(float value)
	{
		___tag_LineIndent_174 = value;
	}

	inline static int32_t get_offset_of_tag_Indent_175() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_Indent_175)); }
	inline float get_tag_Indent_175() const { return ___tag_Indent_175; }
	inline float* get_address_of_tag_Indent_175() { return &___tag_Indent_175; }
	inline void set_tag_Indent_175(float value)
	{
		___tag_Indent_175 = value;
	}

	inline static int32_t get_offset_of_m_indentStack_176() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_indentStack_176)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_m_indentStack_176() const { return ___m_indentStack_176; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_m_indentStack_176() { return &___m_indentStack_176; }
	inline void set_m_indentStack_176(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___m_indentStack_176 = value;
	}

	inline static int32_t get_offset_of_tag_NoParsing_177() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___tag_NoParsing_177)); }
	inline bool get_tag_NoParsing_177() const { return ___tag_NoParsing_177; }
	inline bool* get_address_of_tag_NoParsing_177() { return &___tag_NoParsing_177; }
	inline void set_tag_NoParsing_177(bool value)
	{
		___tag_NoParsing_177 = value;
	}

	inline static int32_t get_offset_of_m_isParsingText_178() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isParsingText_178)); }
	inline bool get_m_isParsingText_178() const { return ___m_isParsingText_178; }
	inline bool* get_address_of_m_isParsingText_178() { return &___m_isParsingText_178; }
	inline void set_m_isParsingText_178(bool value)
	{
		___m_isParsingText_178 = value;
	}

	inline static int32_t get_offset_of_m_FXMatrix_179() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_FXMatrix_179)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_m_FXMatrix_179() const { return ___m_FXMatrix_179; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_m_FXMatrix_179() { return &___m_FXMatrix_179; }
	inline void set_m_FXMatrix_179(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___m_FXMatrix_179 = value;
	}

	inline static int32_t get_offset_of_m_isFXMatrixSet_180() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_isFXMatrixSet_180)); }
	inline bool get_m_isFXMatrixSet_180() const { return ___m_isFXMatrixSet_180; }
	inline bool* get_address_of_m_isFXMatrixSet_180() { return &___m_isFXMatrixSet_180; }
	inline void set_m_isFXMatrixSet_180(bool value)
	{
		___m_isFXMatrixSet_180 = value;
	}

	inline static int32_t get_offset_of_m_TextParsingBuffer_181() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_TextParsingBuffer_181)); }
	inline UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505* get_m_TextParsingBuffer_181() const { return ___m_TextParsingBuffer_181; }
	inline UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505** get_address_of_m_TextParsingBuffer_181() { return &___m_TextParsingBuffer_181; }
	inline void set_m_TextParsingBuffer_181(UnicodeCharU5BU5D_t14B138F2B44C8EA3A5A5DB234E3739F385E55505* value)
	{
		___m_TextParsingBuffer_181 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextParsingBuffer_181), value);
	}

	inline static int32_t get_offset_of_m_internalCharacterInfo_182() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_internalCharacterInfo_182)); }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* get_m_internalCharacterInfo_182() const { return ___m_internalCharacterInfo_182; }
	inline TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604** get_address_of_m_internalCharacterInfo_182() { return &___m_internalCharacterInfo_182; }
	inline void set_m_internalCharacterInfo_182(TMP_CharacterInfoU5BU5D_t415BD08A7E8A8C311B1F7BD9C3AC60BF99339604* value)
	{
		___m_internalCharacterInfo_182 = value;
		Il2CppCodeGenWriteBarrier((&___m_internalCharacterInfo_182), value);
	}

	inline static int32_t get_offset_of_m_input_CharArray_183() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_input_CharArray_183)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_m_input_CharArray_183() const { return ___m_input_CharArray_183; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_m_input_CharArray_183() { return &___m_input_CharArray_183; }
	inline void set_m_input_CharArray_183(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___m_input_CharArray_183 = value;
		Il2CppCodeGenWriteBarrier((&___m_input_CharArray_183), value);
	}

	inline static int32_t get_offset_of_m_charArray_Length_184() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_charArray_Length_184)); }
	inline int32_t get_m_charArray_Length_184() const { return ___m_charArray_Length_184; }
	inline int32_t* get_address_of_m_charArray_Length_184() { return &___m_charArray_Length_184; }
	inline void set_m_charArray_Length_184(int32_t value)
	{
		___m_charArray_Length_184 = value;
	}

	inline static int32_t get_offset_of_m_totalCharacterCount_185() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_totalCharacterCount_185)); }
	inline int32_t get_m_totalCharacterCount_185() const { return ___m_totalCharacterCount_185; }
	inline int32_t* get_address_of_m_totalCharacterCount_185() { return &___m_totalCharacterCount_185; }
	inline void set_m_totalCharacterCount_185(int32_t value)
	{
		___m_totalCharacterCount_185 = value;
	}

	inline static int32_t get_offset_of_m_SavedWordWrapState_186() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_SavedWordWrapState_186)); }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  get_m_SavedWordWrapState_186() const { return ___m_SavedWordWrapState_186; }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 * get_address_of_m_SavedWordWrapState_186() { return &___m_SavedWordWrapState_186; }
	inline void set_m_SavedWordWrapState_186(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  value)
	{
		___m_SavedWordWrapState_186 = value;
	}

	inline static int32_t get_offset_of_m_SavedLineState_187() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_SavedLineState_187)); }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  get_m_SavedLineState_187() const { return ___m_SavedLineState_187; }
	inline WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557 * get_address_of_m_SavedLineState_187() { return &___m_SavedLineState_187; }
	inline void set_m_SavedLineState_187(WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557  value)
	{
		___m_SavedLineState_187 = value;
	}

	inline static int32_t get_offset_of_m_characterCount_188() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_characterCount_188)); }
	inline int32_t get_m_characterCount_188() const { return ___m_characterCount_188; }
	inline int32_t* get_address_of_m_characterCount_188() { return &___m_characterCount_188; }
	inline void set_m_characterCount_188(int32_t value)
	{
		___m_characterCount_188 = value;
	}

	inline static int32_t get_offset_of_m_firstCharacterOfLine_189() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstCharacterOfLine_189)); }
	inline int32_t get_m_firstCharacterOfLine_189() const { return ___m_firstCharacterOfLine_189; }
	inline int32_t* get_address_of_m_firstCharacterOfLine_189() { return &___m_firstCharacterOfLine_189; }
	inline void set_m_firstCharacterOfLine_189(int32_t value)
	{
		___m_firstCharacterOfLine_189 = value;
	}

	inline static int32_t get_offset_of_m_firstVisibleCharacterOfLine_190() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_firstVisibleCharacterOfLine_190)); }
	inline int32_t get_m_firstVisibleCharacterOfLine_190() const { return ___m_firstVisibleCharacterOfLine_190; }
	inline int32_t* get_address_of_m_firstVisibleCharacterOfLine_190() { return &___m_firstVisibleCharacterOfLine_190; }
	inline void set_m_firstVisibleCharacterOfLine_190(int32_t value)
	{
		___m_firstVisibleCharacterOfLine_190 = value;
	}

	inline static int32_t get_offset_of_m_lastCharacterOfLine_191() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lastCharacterOfLine_191)); }
	inline int32_t get_m_lastCharacterOfLine_191() const { return ___m_lastCharacterOfLine_191; }
	inline int32_t* get_address_of_m_lastCharacterOfLine_191() { return &___m_lastCharacterOfLine_191; }
	inline void set_m_lastCharacterOfLine_191(int32_t value)
	{
		___m_lastCharacterOfLine_191 = value;
	}

	inline static int32_t get_offset_of_m_lastVisibleCharacterOfLine_192() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lastVisibleCharacterOfLine_192)); }
	inline int32_t get_m_lastVisibleCharacterOfLine_192() const { return ___m_lastVisibleCharacterOfLine_192; }
	inline int32_t* get_address_of_m_lastVisibleCharacterOfLine_192() { return &___m_lastVisibleCharacterOfLine_192; }
	inline void set_m_lastVisibleCharacterOfLine_192(int32_t value)
	{
		___m_lastVisibleCharacterOfLine_192 = value;
	}

	inline static int32_t get_offset_of_m_lineNumber_193() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineNumber_193)); }
	inline int32_t get_m_lineNumber_193() const { return ___m_lineNumber_193; }
	inline int32_t* get_address_of_m_lineNumber_193() { return &___m_lineNumber_193; }
	inline void set_m_lineNumber_193(int32_t value)
	{
		___m_lineNumber_193 = value;
	}

	inline static int32_t get_offset_of_m_lineVisibleCharacterCount_194() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineVisibleCharacterCount_194)); }
	inline int32_t get_m_lineVisibleCharacterCount_194() const { return ___m_lineVisibleCharacterCount_194; }
	inline int32_t* get_address_of_m_lineVisibleCharacterCount_194() { return &___m_lineVisibleCharacterCount_194; }
	inline void set_m_lineVisibleCharacterCount_194(int32_t value)
	{
		___m_lineVisibleCharacterCount_194 = value;
	}

	inline static int32_t get_offset_of_m_pageNumber_195() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_pageNumber_195)); }
	inline int32_t get_m_pageNumber_195() const { return ___m_pageNumber_195; }
	inline int32_t* get_address_of_m_pageNumber_195() { return &___m_pageNumber_195; }
	inline void set_m_pageNumber_195(int32_t value)
	{
		___m_pageNumber_195 = value;
	}

	inline static int32_t get_offset_of_m_maxAscender_196() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxAscender_196)); }
	inline float get_m_maxAscender_196() const { return ___m_maxAscender_196; }
	inline float* get_address_of_m_maxAscender_196() { return &___m_maxAscender_196; }
	inline void set_m_maxAscender_196(float value)
	{
		___m_maxAscender_196 = value;
	}

	inline static int32_t get_offset_of_m_maxCapHeight_197() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxCapHeight_197)); }
	inline float get_m_maxCapHeight_197() const { return ___m_maxCapHeight_197; }
	inline float* get_address_of_m_maxCapHeight_197() { return &___m_maxCapHeight_197; }
	inline void set_m_maxCapHeight_197(float value)
	{
		___m_maxCapHeight_197 = value;
	}

	inline static int32_t get_offset_of_m_maxDescender_198() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxDescender_198)); }
	inline float get_m_maxDescender_198() const { return ___m_maxDescender_198; }
	inline float* get_address_of_m_maxDescender_198() { return &___m_maxDescender_198; }
	inline void set_m_maxDescender_198(float value)
	{
		___m_maxDescender_198 = value;
	}

	inline static int32_t get_offset_of_m_maxLineAscender_199() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxLineAscender_199)); }
	inline float get_m_maxLineAscender_199() const { return ___m_maxLineAscender_199; }
	inline float* get_address_of_m_maxLineAscender_199() { return &___m_maxLineAscender_199; }
	inline void set_m_maxLineAscender_199(float value)
	{
		___m_maxLineAscender_199 = value;
	}

	inline static int32_t get_offset_of_m_maxLineDescender_200() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_maxLineDescender_200)); }
	inline float get_m_maxLineDescender_200() const { return ___m_maxLineDescender_200; }
	inline float* get_address_of_m_maxLineDescender_200() { return &___m_maxLineDescender_200; }
	inline void set_m_maxLineDescender_200(float value)
	{
		___m_maxLineDescender_200 = value;
	}

	inline static int32_t get_offset_of_m_startOfLineAscender_201() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_startOfLineAscender_201)); }
	inline float get_m_startOfLineAscender_201() const { return ___m_startOfLineAscender_201; }
	inline float* get_address_of_m_startOfLineAscender_201() { return &___m_startOfLineAscender_201; }
	inline void set_m_startOfLineAscender_201(float value)
	{
		___m_startOfLineAscender_201 = value;
	}

	inline static int32_t get_offset_of_m_lineOffset_202() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_lineOffset_202)); }
	inline float get_m_lineOffset_202() const { return ___m_lineOffset_202; }
	inline float* get_address_of_m_lineOffset_202() { return &___m_lineOffset_202; }
	inline void set_m_lineOffset_202(float value)
	{
		___m_lineOffset_202 = value;
	}

	inline static int32_t get_offset_of_m_meshExtents_203() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_meshExtents_203)); }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  get_m_meshExtents_203() const { return ___m_meshExtents_203; }
	inline Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 * get_address_of_m_meshExtents_203() { return &___m_meshExtents_203; }
	inline void set_m_meshExtents_203(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3  value)
	{
		___m_meshExtents_203 = value;
	}

	inline static int32_t get_offset_of_m_htmlColor_204() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_htmlColor_204)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_m_htmlColor_204() const { return ___m_htmlColor_204; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_m_htmlColor_204() { return &___m_htmlColor_204; }
	inline void set_m_htmlColor_204(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___m_htmlColor_204 = value;
	}

	inline static int32_t get_offset_of_m_colorStack_205() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorStack_205)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_colorStack_205() const { return ___m_colorStack_205; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_colorStack_205() { return &___m_colorStack_205; }
	inline void set_m_colorStack_205(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_colorStack_205 = value;
	}

	inline static int32_t get_offset_of_m_underlineColorStack_206() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_underlineColorStack_206)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_underlineColorStack_206() const { return ___m_underlineColorStack_206; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_underlineColorStack_206() { return &___m_underlineColorStack_206; }
	inline void set_m_underlineColorStack_206(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_underlineColorStack_206 = value;
	}

	inline static int32_t get_offset_of_m_strikethroughColorStack_207() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_strikethroughColorStack_207)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_strikethroughColorStack_207() const { return ___m_strikethroughColorStack_207; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_strikethroughColorStack_207() { return &___m_strikethroughColorStack_207; }
	inline void set_m_strikethroughColorStack_207(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_strikethroughColorStack_207 = value;
	}

	inline static int32_t get_offset_of_m_highlightColorStack_208() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_highlightColorStack_208)); }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  get_m_highlightColorStack_208() const { return ___m_highlightColorStack_208; }
	inline TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0 * get_address_of_m_highlightColorStack_208() { return &___m_highlightColorStack_208; }
	inline void set_m_highlightColorStack_208(TMP_RichTextTagStack_1_tDAE1C182F153530A3E6A3ADC1803919A380BCDF0  value)
	{
		___m_highlightColorStack_208 = value;
	}

	inline static int32_t get_offset_of_m_colorGradientPreset_209() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorGradientPreset_209)); }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * get_m_colorGradientPreset_209() const { return ___m_colorGradientPreset_209; }
	inline TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 ** get_address_of_m_colorGradientPreset_209() { return &___m_colorGradientPreset_209; }
	inline void set_m_colorGradientPreset_209(TMP_ColorGradient_tEA29C4736B1786301A803B6C0FB30107A10D79B7 * value)
	{
		___m_colorGradientPreset_209 = value;
		Il2CppCodeGenWriteBarrier((&___m_colorGradientPreset_209), value);
	}

	inline static int32_t get_offset_of_m_colorGradientStack_210() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_colorGradientStack_210)); }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  get_m_colorGradientStack_210() const { return ___m_colorGradientStack_210; }
	inline TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D * get_address_of_m_colorGradientStack_210() { return &___m_colorGradientStack_210; }
	inline void set_m_colorGradientStack_210(TMP_RichTextTagStack_1_tF73231072FB2CD9EBFCAF3C4D7E41E2221B9ED1D  value)
	{
		___m_colorGradientStack_210 = value;
	}

	inline static int32_t get_offset_of_m_tabSpacing_211() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_tabSpacing_211)); }
	inline float get_m_tabSpacing_211() const { return ___m_tabSpacing_211; }
	inline float* get_address_of_m_tabSpacing_211() { return &___m_tabSpacing_211; }
	inline void set_m_tabSpacing_211(float value)
	{
		___m_tabSpacing_211 = value;
	}

	inline static int32_t get_offset_of_m_spacing_212() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spacing_212)); }
	inline float get_m_spacing_212() const { return ___m_spacing_212; }
	inline float* get_address_of_m_spacing_212() { return &___m_spacing_212; }
	inline void set_m_spacing_212(float value)
	{
		___m_spacing_212 = value;
	}

	inline static int32_t get_offset_of_m_styleStack_213() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_styleStack_213)); }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  get_m_styleStack_213() const { return ___m_styleStack_213; }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F * get_address_of_m_styleStack_213() { return &___m_styleStack_213; }
	inline void set_m_styleStack_213(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  value)
	{
		___m_styleStack_213 = value;
	}

	inline static int32_t get_offset_of_m_actionStack_214() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_actionStack_214)); }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  get_m_actionStack_214() const { return ___m_actionStack_214; }
	inline TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F * get_address_of_m_actionStack_214() { return &___m_actionStack_214; }
	inline void set_m_actionStack_214(TMP_RichTextTagStack_1_t629E00E06021AA51A5AD8607BAFC61DB099F2D7F  value)
	{
		___m_actionStack_214 = value;
	}

	inline static int32_t get_offset_of_m_padding_215() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_padding_215)); }
	inline float get_m_padding_215() const { return ___m_padding_215; }
	inline float* get_address_of_m_padding_215() { return &___m_padding_215; }
	inline void set_m_padding_215(float value)
	{
		___m_padding_215 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffset_216() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_baselineOffset_216)); }
	inline float get_m_baselineOffset_216() const { return ___m_baselineOffset_216; }
	inline float* get_address_of_m_baselineOffset_216() { return &___m_baselineOffset_216; }
	inline void set_m_baselineOffset_216(float value)
	{
		___m_baselineOffset_216 = value;
	}

	inline static int32_t get_offset_of_m_baselineOffsetStack_217() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_baselineOffsetStack_217)); }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  get_m_baselineOffsetStack_217() const { return ___m_baselineOffsetStack_217; }
	inline TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3 * get_address_of_m_baselineOffsetStack_217() { return &___m_baselineOffsetStack_217; }
	inline void set_m_baselineOffsetStack_217(TMP_RichTextTagStack_1_t221674BAE112F99AB4BDB4D127F20A021FF50CA3  value)
	{
		___m_baselineOffsetStack_217 = value;
	}

	inline static int32_t get_offset_of_m_xAdvance_218() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_xAdvance_218)); }
	inline float get_m_xAdvance_218() const { return ___m_xAdvance_218; }
	inline float* get_address_of_m_xAdvance_218() { return &___m_xAdvance_218; }
	inline void set_m_xAdvance_218(float value)
	{
		___m_xAdvance_218 = value;
	}

	inline static int32_t get_offset_of_m_textElementType_219() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_textElementType_219)); }
	inline int32_t get_m_textElementType_219() const { return ___m_textElementType_219; }
	inline int32_t* get_address_of_m_textElementType_219() { return &___m_textElementType_219; }
	inline void set_m_textElementType_219(int32_t value)
	{
		___m_textElementType_219 = value;
	}

	inline static int32_t get_offset_of_m_cached_TextElement_220() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_TextElement_220)); }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * get_m_cached_TextElement_220() const { return ___m_cached_TextElement_220; }
	inline TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 ** get_address_of_m_cached_TextElement_220() { return &___m_cached_TextElement_220; }
	inline void set_m_cached_TextElement_220(TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344 * value)
	{
		___m_cached_TextElement_220 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_TextElement_220), value);
	}

	inline static int32_t get_offset_of_m_cached_Underline_Character_221() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_Underline_Character_221)); }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D * get_m_cached_Underline_Character_221() const { return ___m_cached_Underline_Character_221; }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D ** get_address_of_m_cached_Underline_Character_221() { return &___m_cached_Underline_Character_221; }
	inline void set_m_cached_Underline_Character_221(TMP_Character_t1875AACA978396521498D6A699052C187903553D * value)
	{
		___m_cached_Underline_Character_221 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Underline_Character_221), value);
	}

	inline static int32_t get_offset_of_m_cached_Ellipsis_Character_222() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_cached_Ellipsis_Character_222)); }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D * get_m_cached_Ellipsis_Character_222() const { return ___m_cached_Ellipsis_Character_222; }
	inline TMP_Character_t1875AACA978396521498D6A699052C187903553D ** get_address_of_m_cached_Ellipsis_Character_222() { return &___m_cached_Ellipsis_Character_222; }
	inline void set_m_cached_Ellipsis_Character_222(TMP_Character_t1875AACA978396521498D6A699052C187903553D * value)
	{
		___m_cached_Ellipsis_Character_222 = value;
		Il2CppCodeGenWriteBarrier((&___m_cached_Ellipsis_Character_222), value);
	}

	inline static int32_t get_offset_of_m_defaultSpriteAsset_223() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_defaultSpriteAsset_223)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_defaultSpriteAsset_223() const { return ___m_defaultSpriteAsset_223; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_defaultSpriteAsset_223() { return &___m_defaultSpriteAsset_223; }
	inline void set_m_defaultSpriteAsset_223(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_defaultSpriteAsset_223 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultSpriteAsset_223), value);
	}

	inline static int32_t get_offset_of_m_currentSpriteAsset_224() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_currentSpriteAsset_224)); }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * get_m_currentSpriteAsset_224() const { return ___m_currentSpriteAsset_224; }
	inline TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 ** get_address_of_m_currentSpriteAsset_224() { return &___m_currentSpriteAsset_224; }
	inline void set_m_currentSpriteAsset_224(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487 * value)
	{
		___m_currentSpriteAsset_224 = value;
		Il2CppCodeGenWriteBarrier((&___m_currentSpriteAsset_224), value);
	}

	inline static int32_t get_offset_of_m_spriteCount_225() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteCount_225)); }
	inline int32_t get_m_spriteCount_225() const { return ___m_spriteCount_225; }
	inline int32_t* get_address_of_m_spriteCount_225() { return &___m_spriteCount_225; }
	inline void set_m_spriteCount_225(int32_t value)
	{
		___m_spriteCount_225 = value;
	}

	inline static int32_t get_offset_of_m_spriteIndex_226() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteIndex_226)); }
	inline int32_t get_m_spriteIndex_226() const { return ___m_spriteIndex_226; }
	inline int32_t* get_address_of_m_spriteIndex_226() { return &___m_spriteIndex_226; }
	inline void set_m_spriteIndex_226(int32_t value)
	{
		___m_spriteIndex_226 = value;
	}

	inline static int32_t get_offset_of_m_spriteAnimationID_227() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_spriteAnimationID_227)); }
	inline int32_t get_m_spriteAnimationID_227() const { return ___m_spriteAnimationID_227; }
	inline int32_t* get_address_of_m_spriteAnimationID_227() { return &___m_spriteAnimationID_227; }
	inline void set_m_spriteAnimationID_227(int32_t value)
	{
		___m_spriteAnimationID_227 = value;
	}

	inline static int32_t get_offset_of_m_ignoreActiveState_228() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___m_ignoreActiveState_228)); }
	inline bool get_m_ignoreActiveState_228() const { return ___m_ignoreActiveState_228; }
	inline bool* get_address_of_m_ignoreActiveState_228() { return &___m_ignoreActiveState_228; }
	inline void set_m_ignoreActiveState_228(bool value)
	{
		___m_ignoreActiveState_228 = value;
	}

	inline static int32_t get_offset_of_k_Power_229() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7, ___k_Power_229)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_k_Power_229() const { return ___k_Power_229; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_k_Power_229() { return &___k_Power_229; }
	inline void set_k_Power_229(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___k_Power_229 = value;
		Il2CppCodeGenWriteBarrier((&___k_Power_229), value);
	}
};

struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields
{
public:
	// UnityEngine.Color32 TMPro.TMP_Text::s_colorWhite
	Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___s_colorWhite_47;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargePositiveVector2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_LargePositiveVector2_230;
	// UnityEngine.Vector2 TMPro.TMP_Text::k_LargeNegativeVector2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___k_LargeNegativeVector2_231;
	// System.Single TMPro.TMP_Text::k_LargePositiveFloat
	float ___k_LargePositiveFloat_232;
	// System.Single TMPro.TMP_Text::k_LargeNegativeFloat
	float ___k_LargeNegativeFloat_233;
	// System.Int32 TMPro.TMP_Text::k_LargePositiveInt
	int32_t ___k_LargePositiveInt_234;
	// System.Int32 TMPro.TMP_Text::k_LargeNegativeInt
	int32_t ___k_LargeNegativeInt_235;

public:
	inline static int32_t get_offset_of_s_colorWhite_47() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___s_colorWhite_47)); }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  get_s_colorWhite_47() const { return ___s_colorWhite_47; }
	inline Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * get_address_of_s_colorWhite_47() { return &___s_colorWhite_47; }
	inline void set_s_colorWhite_47(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  value)
	{
		___s_colorWhite_47 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveVector2_230() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveVector2_230)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_LargePositiveVector2_230() const { return ___k_LargePositiveVector2_230; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_LargePositiveVector2_230() { return &___k_LargePositiveVector2_230; }
	inline void set_k_LargePositiveVector2_230(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_LargePositiveVector2_230 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeVector2_231() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeVector2_231)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_k_LargeNegativeVector2_231() const { return ___k_LargeNegativeVector2_231; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_k_LargeNegativeVector2_231() { return &___k_LargeNegativeVector2_231; }
	inline void set_k_LargeNegativeVector2_231(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___k_LargeNegativeVector2_231 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveFloat_232() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveFloat_232)); }
	inline float get_k_LargePositiveFloat_232() const { return ___k_LargePositiveFloat_232; }
	inline float* get_address_of_k_LargePositiveFloat_232() { return &___k_LargePositiveFloat_232; }
	inline void set_k_LargePositiveFloat_232(float value)
	{
		___k_LargePositiveFloat_232 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeFloat_233() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeFloat_233)); }
	inline float get_k_LargeNegativeFloat_233() const { return ___k_LargeNegativeFloat_233; }
	inline float* get_address_of_k_LargeNegativeFloat_233() { return &___k_LargeNegativeFloat_233; }
	inline void set_k_LargeNegativeFloat_233(float value)
	{
		___k_LargeNegativeFloat_233 = value;
	}

	inline static int32_t get_offset_of_k_LargePositiveInt_234() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargePositiveInt_234)); }
	inline int32_t get_k_LargePositiveInt_234() const { return ___k_LargePositiveInt_234; }
	inline int32_t* get_address_of_k_LargePositiveInt_234() { return &___k_LargePositiveInt_234; }
	inline void set_k_LargePositiveInt_234(int32_t value)
	{
		___k_LargePositiveInt_234 = value;
	}

	inline static int32_t get_offset_of_k_LargeNegativeInt_235() { return static_cast<int32_t>(offsetof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields, ___k_LargeNegativeInt_235)); }
	inline int32_t get_k_LargeNegativeInt_235() const { return ___k_LargeNegativeInt_235; }
	inline int32_t* get_address_of_k_LargeNegativeInt_235() { return &___k_LargeNegativeInt_235; }
	inline void set_k_LargeNegativeInt_235(int32_t value)
	{
		___k_LargeNegativeInt_235 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TMP_TEXT_T7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6400 = { sizeof (LineBreakingTable_tB80E0533075B07F5080E99393CEF91FDC8C2AB25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6400[2] = 
{
	LineBreakingTable_tB80E0533075B07F5080E99393CEF91FDC8C2AB25::get_offset_of_leadingCharacters_0(),
	LineBreakingTable_tB80E0533075B07F5080E99393CEF91FDC8C2AB25::get_offset_of_followingCharacters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6401 = { sizeof (ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8), -1, sizeof(ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6401[63] = 
{
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_MainTex_0(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_FaceTex_1(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_FaceColor_2(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_FaceDilate_3(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_Shininess_4(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlayColor_5(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlayOffsetX_6(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlayOffsetY_7(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlayDilate_8(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UnderlaySoftness_9(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_WeightNormal_10(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_WeightBold_11(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_OutlineTex_12(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_OutlineWidth_13(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_OutlineSoftness_14(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_OutlineColor_15(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_Padding_16(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GradientScale_17(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleX_18(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleY_19(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_PerspectiveFilter_20(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_Sharpness_21(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_TextureWidth_22(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_TextureHeight_23(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_BevelAmount_24(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GlowColor_25(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GlowOffset_26(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GlowPower_27(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_GlowOuter_28(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_LightAngle_29(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_EnvMap_30(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_EnvMatrix_31(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_EnvMatrixRotation_32(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_MaskCoord_33(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ClipRect_34(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_MaskSoftnessX_35(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_MaskSoftnessY_36(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_VertexOffsetX_37(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_VertexOffsetY_38(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_UseClipRect_39(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilID_40(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilOp_41(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilComp_42(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilReadMask_43(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_StencilWriteMask_44(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ShaderFlags_45(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleRatio_A_46(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleRatio_B_47(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ID_ScaleRatio_C_48(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Bevel_49(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Glow_50(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Underlay_51(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Ratios_52(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_MASK_SOFT_53(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_MASK_HARD_54(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_MASK_TEX_55(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_Keyword_Outline_56(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ShaderTag_ZTestMode_57(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_ShaderTag_CullMode_58(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_m_clamp_59(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_isInitialized_60(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_k_ShaderRef_MobileSDF_61(),
	ShaderUtilities_t94FED29CB763EEA57E3BBCA7B305F9A3CB1180B8_StaticFields::get_offset_of_k_ShaderRef_MobileBitmap_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6402 = { sizeof (TMP_Sprite_t8D26AE7781056AB44B074C80FFC74AFB076E1353), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6402[5] = 
{
	TMP_Sprite_t8D26AE7781056AB44B074C80FFC74AFB076E1353::get_offset_of_name_9(),
	TMP_Sprite_t8D26AE7781056AB44B074C80FFC74AFB076E1353::get_offset_of_hashCode_10(),
	TMP_Sprite_t8D26AE7781056AB44B074C80FFC74AFB076E1353::get_offset_of_unicode_11(),
	TMP_Sprite_t8D26AE7781056AB44B074C80FFC74AFB076E1353::get_offset_of_pivot_12(),
	TMP_Sprite_t8D26AE7781056AB44B074C80FFC74AFB076E1353::get_offset_of_sprite_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6403 = { sizeof (TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6403[2] = 
{
	TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17::get_offset_of_m_animations_4(),
	TMP_SpriteAnimator_tEB1A22D4A88DC5AAC3EFBDD8FD10B2A02C7B0D17::get_offset_of_m_TextComponent_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6404 = { sizeof (U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6404[15] = 
{
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_U3CU3E1__state_0(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_U3CU3E2__current_1(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_U3CU3E4__this_2(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_start_3(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_end_4(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_spriteAsset_5(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_currentCharacter_6(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_framerate_7(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_U3CcurrentFrameU3E5__2_8(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_U3CcharInfoU3E5__3_9(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_U3CmaterialIndexU3E5__4_10(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_U3CvertexIndexU3E5__5_11(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_U3CmeshInfoU3E5__6_12(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_U3CelapsedTimeU3E5__7_13(),
	U3CDoSpriteAnimationInternalU3Ed__7_t31ADE0DE24AE64EC437AED7B01D5749E9248C2AC::get_offset_of_U3CtargetTimeU3E5__8_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6405 = { sizeof (TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487), -1, sizeof(TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6405[11] = 
{
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487::get_offset_of_m_UnicodeLookup_7(),
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487::get_offset_of_m_NameLookup_8(),
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487::get_offset_of_m_GlyphIndexLookup_9(),
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487::get_offset_of_m_Version_10(),
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487::get_offset_of_spriteSheet_11(),
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487::get_offset_of_m_SpriteCharacterTable_12(),
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487::get_offset_of_m_SpriteGlyphTable_13(),
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487::get_offset_of_spriteInfoList_14(),
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487::get_offset_of_fallbackSpriteAssets_15(),
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487::get_offset_of_m_IsSpriteAssetLookupTablesDirty_16(),
	TMP_SpriteAsset_tF896FFED2AA9395D6BC40FFEAC6DE7555A27A487_StaticFields::get_offset_of_k_searchedSpriteAssets_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6406 = { sizeof (U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346), -1, sizeof(U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6406[3] = 
{
	U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346_StaticFields::get_offset_of_U3CU3E9__32_0_1(),
	U3CU3Ec_t685586B0F954FF1162ABB09FB424BB1AED63C346_StaticFields::get_offset_of_U3CU3E9__33_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6407 = { sizeof (TMP_SpriteCharacter_tDFDF0D32E583270A561804076B01146C324F4D33), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6407[2] = 
{
	TMP_SpriteCharacter_tDFDF0D32E583270A561804076B01146C324F4D33::get_offset_of_m_Name_5(),
	TMP_SpriteCharacter_tDFDF0D32E583270A561804076B01146C324F4D33::get_offset_of_m_HashCode_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6408 = { sizeof (TMP_SpriteGlyph_t423E5984649351521A513FDF257D33C67116BF9B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6408[1] = 
{
	TMP_SpriteGlyph_t423E5984649351521A513FDF257D33C67116BF9B::get_offset_of_sprite_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6409 = { sizeof (TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6409[6] = 
{
	TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD::get_offset_of_m_Name_0(),
	TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD::get_offset_of_m_HashCode_1(),
	TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD::get_offset_of_m_OpeningDefinition_2(),
	TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD::get_offset_of_m_ClosingDefinition_3(),
	TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD::get_offset_of_m_OpeningTagArray_4(),
	TMP_Style_t9FD01084B9E3F1D4B92E87114C454C98BA20FBAD::get_offset_of_m_ClosingTagArray_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6410 = { sizeof (TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04), -1, sizeof(TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6410[3] = 
{
	TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04_StaticFields::get_offset_of_s_Instance_4(),
	TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04::get_offset_of_m_StyleList_5(),
	TMP_StyleSheet_tC6C45E5B0EC8EF4BA7BB147712516656B0D26C04::get_offset_of_m_StyleDictionary_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6411 = { sizeof (TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6411[13] = 
{
	TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D::get_offset_of_m_fontAsset_4(),
	TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D::get_offset_of_m_spriteAsset_5(),
	TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D::get_offset_of_m_material_6(),
	TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D::get_offset_of_m_sharedMaterial_7(),
	TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D::get_offset_of_m_fallbackMaterial_8(),
	TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D::get_offset_of_m_fallbackSourceMaterial_9(),
	TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D::get_offset_of_m_isDefaultMaterial_10(),
	TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D::get_offset_of_m_padding_11(),
	TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D::get_offset_of_m_renderer_12(),
	TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D::get_offset_of_m_meshFilter_13(),
	TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D::get_offset_of_m_mesh_14(),
	TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D::get_offset_of_m_TextComponent_15(),
	TMP_SubMesh_tB9C2AFAA42A17F92D31845EEFCD99B144867A96D::get_offset_of_m_isRegisteredForEvents_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6412 = { sizeof (TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6412[14] = 
{
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_fontAsset_30(),
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_spriteAsset_31(),
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_material_32(),
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_sharedMaterial_33(),
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_fallbackMaterial_34(),
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_fallbackSourceMaterial_35(),
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_isDefaultMaterial_36(),
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_padding_37(),
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_canvasRenderer_38(),
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_mesh_39(),
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_TextComponent_40(),
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_isRegisteredForEvents_41(),
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_materialDirty_42(),
	TMP_SubMeshUI_tA1CA59D5820CBD2494E1A1562E02FE4D4272A2A1::get_offset_of_m_materialReferenceIndex_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6413 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6414 = { sizeof (TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6414[37] = 
{
	TextAlignmentOptions_t4BEB3BA6EE897B5127FFBABD7E36B1A024EE5337::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6415 = { sizeof (_HorizontalAlignmentOptions_tAB8CD906738706E3798D087C5C993333187F8882)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6415[7] = 
{
	_HorizontalAlignmentOptions_tAB8CD906738706E3798D087C5C993333187F8882::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6416 = { sizeof (_VerticalAlignmentOptions_t1E4A066C44CB004C8AA7F96631DE179C296CC20C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6416[7] = 
{
	_VerticalAlignmentOptions_t1E4A066C44CB004C8AA7F96631DE179C296CC20C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6417 = { sizeof (TextRenderFlags_t29165355D5674BAEF40359B740631503FA9C0B56)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6417[3] = 
{
	TextRenderFlags_t29165355D5674BAEF40359B740631503FA9C0B56::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6418 = { sizeof (TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6418[3] = 
{
	TMP_TextElementType_tBF2553FA730CC21CF99473E591C33DC52360D509::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6419 = { sizeof (MaskingTypes_t37B6F292739A890CF34EA024D24A5BFA88579086)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6419[4] = 
{
	MaskingTypes_t37B6F292739A890CF34EA024D24A5BFA88579086::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6420 = { sizeof (TextOverflowModes_tC4F820014333ECAF4D52B02F75171FD9E52B9D76)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6420[8] = 
{
	TextOverflowModes_tC4F820014333ECAF4D52B02F75171FD9E52B9D76::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6421 = { sizeof (MaskingOffsetMode_t2EC14EE19A8002F3390C5706568D36A3C7E0C7DA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6421[3] = 
{
	MaskingOffsetMode_t2EC14EE19A8002F3390C5706568D36A3C7E0C7DA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6422 = { sizeof (TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6422[5] = 
{
	TextureMappingOptions_tAC77A218D6DF5F386DA38AEAF3D9C943F084BD10::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6423 = { sizeof (FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6423[12] = 
{
	FontStyles_t31B880C817B2DF0BF3B60AC4D187A3E7BE5D8893::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6424 = { sizeof (FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6424[10] = 
{
	FontWeight_tE551C56E6C7CCAFCC6519C65D03AAA340E9FF35C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6425 = { sizeof (TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7), -1, sizeof(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6425[206] = 
{
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_text_30(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isRightToLeft_31(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontAsset_32(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_currentFontAsset_33(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isSDFShader_34(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_sharedMaterial_35(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_currentMaterial_36(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_materialReferences_37(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_materialReferenceIndexLookup_38(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_materialReferenceStack_39(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_currentMaterialIndex_40(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontSharedMaterials_41(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontMaterial_42(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontMaterials_43(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isMaterialDirty_44(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontColor32_45(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontColor_46(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_s_colorWhite_47(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_underlineColor_48(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_strikethroughColor_49(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_highlightColor_50(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_highlightPadding_51(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_enableVertexGradient_52(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_colorMode_53(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontColorGradient_54(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontColorGradientPreset_55(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spriteAsset_56(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_tintAllSprites_57(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_tintSprite_58(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spriteColor_59(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_overrideHtmlColors_60(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_faceColor_61(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_outlineColor_62(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_outlineWidth_63(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontSize_64(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_currentFontSize_65(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontSizeBase_66(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_sizeStack_67(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontWeight_68(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_FontWeightInternal_69(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_FontWeightStack_70(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_enableAutoSizing_71(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxFontSize_72(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_minFontSize_73(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontSizeMin_74(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontSizeMax_75(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontStyle_76(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_FontStyleInternal_77(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontStyleStack_78(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isUsingBold_79(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_textAlignment_80(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineJustification_81(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineJustificationStack_82(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_textContainerLocalCorners_83(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_characterSpacing_84(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_cSpacing_85(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_monoSpacing_86(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_wordSpacing_87(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineSpacing_88(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineSpacingDelta_89(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineHeight_90(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineSpacingMax_91(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_paragraphSpacing_92(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_charWidthMaxAdj_93(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_charWidthAdjDelta_94(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_enableWordWrapping_95(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isCharacterWrappingEnabled_96(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isNonBreakingSpace_97(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isIgnoringAlignment_98(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_wordWrappingRatios_99(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_overflowMode_100(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_firstOverflowCharacterIndex_101(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_linkedTextComponent_102(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isLinkedTextComponent_103(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isTextTruncated_104(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_enableKerning_105(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_enableExtraPadding_106(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_checkPaddingRequired_107(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isRichText_108(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_parseCtrlCharacters_109(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isOverlay_110(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isOrthographic_111(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isCullingEnabled_112(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_ignoreRectMaskCulling_113(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_ignoreCulling_114(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_horizontalMapping_115(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_verticalMapping_116(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_uvLineOffset_117(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_renderMode_118(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_geometrySortingOrder_119(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_VertexBufferAutoSizeReduction_120(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_firstVisibleCharacter_121(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxVisibleCharacters_122(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxVisibleWords_123(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxVisibleLines_124(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_useMaxVisibleDescender_125(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_pageToDisplay_126(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isNewPage_127(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_margin_128(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_marginLeft_129(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_marginRight_130(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_marginWidth_131(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_marginHeight_132(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_width_133(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_textInfo_134(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_havePropertiesChanged_135(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isUsingLegacyAnimationComponent_136(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_transform_137(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_rectTransform_138(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_139(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_autoSizeTextContainer_140(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_mesh_141(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isVolumetricText_142(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spriteAnimator_143(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_flexibleHeight_144(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_flexibleWidth_145(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_minWidth_146(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_minHeight_147(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxWidth_148(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxHeight_149(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_LayoutElement_150(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_preferredWidth_151(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_renderedWidth_152(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isPreferredWidthDirty_153(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_preferredHeight_154(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_renderedHeight_155(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isPreferredHeightDirty_156(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isCalculatingPreferredValues_157(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_recursiveCount_158(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_layoutPriority_159(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isCalculateSizeRequired_160(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isLayoutDirty_161(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_verticesAlreadyDirty_162(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_layoutAlreadyDirty_163(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isAwake_164(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isWaitingOnResourceLoad_165(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isInputParsingRequired_166(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_inputSource_167(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_old_text_168(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontScale_169(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_fontScaleMultiplier_170(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_htmlTag_171(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_xmlAttribute_172(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_attributeParameterValues_173(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_tag_LineIndent_174(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_tag_Indent_175(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_indentStack_176(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_tag_NoParsing_177(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isParsingText_178(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_FXMatrix_179(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_isFXMatrixSet_180(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_TextParsingBuffer_181(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_internalCharacterInfo_182(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_input_CharArray_183(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_charArray_Length_184(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_totalCharacterCount_185(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_SavedWordWrapState_186(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_SavedLineState_187(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_characterCount_188(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_firstCharacterOfLine_189(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_firstVisibleCharacterOfLine_190(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lastCharacterOfLine_191(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lastVisibleCharacterOfLine_192(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineNumber_193(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineVisibleCharacterCount_194(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_pageNumber_195(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxAscender_196(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxCapHeight_197(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxDescender_198(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxLineAscender_199(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_maxLineDescender_200(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_startOfLineAscender_201(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_lineOffset_202(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_meshExtents_203(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_htmlColor_204(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_colorStack_205(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_underlineColorStack_206(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_strikethroughColorStack_207(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_highlightColorStack_208(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_colorGradientPreset_209(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_colorGradientStack_210(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_tabSpacing_211(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spacing_212(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_styleStack_213(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_actionStack_214(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_padding_215(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_baselineOffset_216(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_baselineOffsetStack_217(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_xAdvance_218(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_textElementType_219(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_cached_TextElement_220(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_cached_Underline_Character_221(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_cached_Ellipsis_Character_222(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_defaultSpriteAsset_223(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_currentSpriteAsset_224(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spriteCount_225(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spriteIndex_226(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_spriteAnimationID_227(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_m_ignoreActiveState_228(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7::get_offset_of_k_Power_229(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_k_LargePositiveVector2_230(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_k_LargeNegativeVector2_231(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_k_LargePositiveFloat_232(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_k_LargeNegativeFloat_233(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_k_LargePositiveInt_234(),
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7_StaticFields::get_offset_of_k_LargeNegativeInt_235(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6426 = { sizeof (TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6426[5] = 
{
	TextInputSources_t08C2D3664AE99CBF6ED41C9DB8F4E9E8FC8E54B4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6427 = { sizeof (UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A)+ sizeof (RuntimeObject), sizeof(UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A ), 0, 0 };
extern const int32_t g_FieldOffsetTable6427[3] = 
{
	UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A::get_offset_of_unicode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A::get_offset_of_stringIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UnicodeChar_t29383F22AA9A3AA4A2061312113FDF2887834F2A::get_offset_of_length_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6428 = { sizeof (TextElementType_t3C95010E28DAFD09E9C361EEB679937475CEE857)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6428[3] = 
{
	TextElementType_t3C95010E28DAFD09E9C361EEB679937475CEE857::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6429 = { sizeof (TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6429[5] = 
{
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344::get_offset_of_m_ElementType_0(),
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344::get_offset_of_m_Unicode_1(),
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344::get_offset_of_m_Glyph_2(),
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344::get_offset_of_m_GlyphIndex_3(),
	TMP_TextElement_tB9A6A361BB93487BD07DDDA37A368819DA46C344::get_offset_of_m_Scale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6430 = { sizeof (TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6430[9] = 
{
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_id_0(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_x_1(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_y_2(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_width_3(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_height_4(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_xOffset_5(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_yOffset_6(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_xAdvance_7(),
	TMP_TextElement_Legacy_t020BAF673D3D29BC2682AEA5717411BFB13C6D05::get_offset_of_scale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6431 = { sizeof (TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181), -1, sizeof(TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6431[18] = 
{
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181_StaticFields::get_offset_of_k_InfinityVectorPositive_0(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181_StaticFields::get_offset_of_k_InfinityVectorNegative_1(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_textComponent_2(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_characterCount_3(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_spriteCount_4(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_spaceCount_5(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_wordCount_6(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_linkCount_7(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_lineCount_8(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_pageCount_9(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_materialCount_10(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_characterInfo_11(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_wordInfo_12(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_linkInfo_13(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_lineInfo_14(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_pageInfo_15(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_meshInfo_16(),
	TMP_TextInfo_tC40DAAB47C5BD5AD21B3F456D860474D96D9C181::get_offset_of_m_CachedMeshInfo_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6432 = { sizeof (TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35), -1, sizeof(TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6432[3] = 
{
	TMP_TextParsingUtilities_tA5D4616296766ECCFF80C5F3A800D7B92155AD35_StaticFields::get_offset_of_s_Instance_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6433 = { sizeof (CaretPosition_t6781B2FD769974C40ECDB58E3BCBA4ADC1B34483)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6433[4] = 
{
	CaretPosition_t6781B2FD769974C40ECDB58E3BCBA4ADC1B34483::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6434 = { sizeof (CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708)+ sizeof (RuntimeObject), sizeof(CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6434[2] = 
{
	CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708::get_offset_of_index_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CaretInfo_tA8526784E8AE82A9BA08B9D5C28483AE2416F708::get_offset_of_position_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6435 = { sizeof (TMP_TextUtilities_t0C64120E363A3DA0CB859D321248294080076A45), -1, sizeof(TMP_TextUtilities_t0C64120E363A3DA0CB859D321248294080076A45_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6435[3] = 
{
	TMP_TextUtilities_t0C64120E363A3DA0CB859D321248294080076A45_StaticFields::get_offset_of_m_rectWorldCorners_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6436 = { sizeof (LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D)+ sizeof (RuntimeObject), sizeof(LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D ), 0, 0 };
extern const int32_t g_FieldOffsetTable6436[2] = 
{
	LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D::get_offset_of_Point1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LineSegment_t27893280FED499A0FC1183D56B9F8BC56D42D84D::get_offset_of_Point2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6437 = { sizeof (TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B), -1, sizeof(TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6437[7] = 
{
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B::get_offset_of_m_GraphicQueueLookup_4(),
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B::get_offset_of_m_InternalUpdateQueue_5(),
	TMP_UpdateManager_t0982D5C74E5439571872EB41119F1FFFE74DEF4B::get_offset_of_m_InternalUpdateLookup_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6438 = { sizeof (TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC), -1, sizeof(TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6438[5] = 
{
	TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateRegistry_tB9639A31383CB6B5941A2CE791DA2B4EB23F3FDC::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6439 = { sizeof (Compute_DistanceTransform_EventTypes_t7D7D951983E29A63EA0B4AE2EBC021B801291E50)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6439[3] = 
{
	Compute_DistanceTransform_EventTypes_t7D7D951983E29A63EA0B4AE2EBC021B801291E50::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6440 = { sizeof (TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D), -1, sizeof(TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6440[13] = 
{
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_COMPUTE_DT_EVENT_0(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_MATERIAL_PROPERTY_EVENT_1(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_FONT_PROPERTY_EVENT_2(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_RESOURCE_LOAD_EVENT_9(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_10(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_OnPreRenderObject_Event_11(),
	TMPro_EventManager_t0831C7F02A59F06755AFFF94AAF826EEE77E516D_StaticFields::get_offset_of_TEXT_CHANGED_EVENT_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6441 = { sizeof (Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6441[3] = 
{
	Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11::get_offset_of_EventType_0(),
	Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11::get_offset_of_ProgressPercentage_1(),
	Compute_DT_EventArgs_t17A56E99E621F8C211A13BE81BEAE18806DA7B11::get_offset_of_Colors_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6442 = { sizeof (TMPro_ExtensionMethods_t2A24D41EAD2F3568C5617941DE7558790B9B1835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6443 = { sizeof (TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44), -1, sizeof(TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6443[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields::get_offset_of_MAX_16BIT_6(),
	TMP_Math_t074FB253662A213E8905642B8B29473EC794FE44_StaticFields::get_offset_of_MIN_16BIT_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6444 = { sizeof (TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6444[8] = 
{
	TMP_VertexDataUpdateFlags_tBEFA6E84F629CD5F4B1B040D55F7AB11B1AD6142::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6445 = { sizeof (VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A)+ sizeof (RuntimeObject), sizeof(VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A ), 0, 0 };
extern const int32_t g_FieldOffsetTable6445[4] = 
{
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_topLeft_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_topRight_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_bottomLeft_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VertexGradient_tDDAAE14E70CADA44B1B69F228CFF837C67EF6F9A::get_offset_of_bottomRight_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6446 = { sizeof (TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24)+ sizeof (RuntimeObject), sizeof(TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6446[5] = 
{
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_firstCharacterIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_lastCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_ascender_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_baseLine_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_PageInfo_t5D305B11116379997CA9649E8D87B3D7162ABB24::get_offset_of_descender_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6447 = { sizeof (TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6447[7] = 
{
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_hashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkIdFirstCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkIdLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkTextfirstCharacterIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkTextLength_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_LinkInfo_t7F4B699290A975144DF7094667825BCD52594468::get_offset_of_linkID_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6448 = { sizeof (TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6448[4] = 
{
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_firstCharacterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_lastCharacterIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_WordInfo_t856E4994B49881E370B28E1D0C35EEDA56120D90::get_offset_of_characterCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6449 = { sizeof (TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB)+ sizeof (RuntimeObject), sizeof(TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB ), 0, 0 };
extern const int32_t g_FieldOffsetTable6449[3] = 
{
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_spriteIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_characterIndex_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TMP_SpriteInfo_t55432612FE0D00F32826D0F817E8462F66CBABBB::get_offset_of_vertexIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6450 = { sizeof (Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3)+ sizeof (RuntimeObject), sizeof(Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6450[2] = 
{
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Extents_tB63A1FF929CAEBC8E097EF426A8B6F91442B0EA3::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6451 = { sizeof (Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8)+ sizeof (RuntimeObject), sizeof(Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6451[2] = 
{
	Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8::get_offset_of_min_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mesh_Extents_t4A76BB0AD89A24EA8AE86F4EC31081DB86F7E7E8::get_offset_of_max_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6452 = { sizeof (WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6452[55] = 
{
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previous_WordBreak_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_total_CharacterCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_CharacterCount_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_SpriteCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_visible_LinkCount_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lastVisibleCharIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineNumber_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxCapHeight_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxAscender_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxDescender_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxLineAscender_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_maxLineDescender_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previousLineAscender_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_xAdvance_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_preferredWidth_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_preferredHeight_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_previousLineScale_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_wordCount_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontStyle_21() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontScale_22() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontScaleMultiplier_23() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentFontSize_24() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_baselineOffset_25() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineOffset_26() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_textInfo_27() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineInfo_28() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_vertexColor_29() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_basicStyleStack_33() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_colorStack_34() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_underlineColorStack_35() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_strikethroughColorStack_36() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_highlightColorStack_37() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_colorGradientStack_38() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_sizeStack_39() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_indentStack_40() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_fontWeightStack_41() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_styleStack_42() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_baselineStack_43() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_actionStack_44() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_materialReferenceStack_45() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_lineJustificationStack_46() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_spriteAnimationID_47() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentFontAsset_48() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentSpriteAsset_49() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentMaterial_50() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_currentMaterialIndex_51() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_meshExtents_52() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_tagNoParsing_53() + static_cast<int32_t>(sizeof(RuntimeObject)),
	WordWrapState_t415B8622774DD094A9CD7447D298B33B7365A557::get_offset_of_isNonBreakingSpace_54() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6453 = { sizeof (TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5)+ sizeof (RuntimeObject), sizeof(TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6453[3] = 
{
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_startIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_length_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagAttribute_t76F6882B9F1A6E98098CFBB2D4B933BE36B543E5::get_offset_of_hashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6454 = { sizeof (RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98)+ sizeof (RuntimeObject), sizeof(RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6454[6] = 
{
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_nameHashCode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_valueHashCode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_valueType_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_valueStartIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_valueLength_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RichTextTagAttribute_t381E96CA7820A787C5D88B6DA0181DFA85ADBA98::get_offset_of_unitType_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6455 = { sizeof (SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6455[3] = 
{
	SpriteAssetImportFormats_t9FCB9C130D6E1B767AE636CB050AD8AFC9CF297E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6456 = { sizeof (TexturePacker_t2549189919276EB833F83EA937AB992420E1B199), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6457 = { sizeof (SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04)+ sizeof (RuntimeObject), sizeof(SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6457[4] = 
{
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_w_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteFrame_tDB681A7461FA0C10DA42E9A984BDDD0199AB2C04::get_offset_of_h_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6458 = { sizeof (SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E)+ sizeof (RuntimeObject), sizeof(SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E ), 0, 0 };
extern const int32_t g_FieldOffsetTable6458[2] = 
{
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E::get_offset_of_w_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteSize_t143F23923B1D48E84CB38DCDD532F408936AB67E::get_offset_of_h_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6459 = { sizeof (SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728)+ sizeof (RuntimeObject), sizeof(SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6459[7] = 
{
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_filename_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_frame_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_rotated_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_trimmed_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_spriteSourceSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_sourceSize_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SpriteData_t8ADAD39500AF244CC913ED9B1F964DF04B4E5728::get_offset_of_pivot_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6460 = { sizeof (SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6460[1] = 
{
	SpriteDataObject_t3AF64B9ABF7A11F88E442474D5F8FEB46E428C6E::get_offset_of_frames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6461 = { sizeof (U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380), -1, sizeof(U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6461[2] = 
{
	U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields::get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t2BDD5686E87BA22996C7749A9C7A7F86EB589380_StaticFields::get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6462 = { sizeof (__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D12_t02D4D72A8B5221D32AB6F2F2DD8D17E86997BF22 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6463 = { sizeof (__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D40_tB9EB85DC7184311C5BB54C1E612DC4F2D7A15F5D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6464 = { sizeof (U3CModuleU3E_t57A3C2925E0AB089D4622AEA90D6704E6A4ED13D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6465 = { sizeof (Base64Utils_t79BFAC462546502E6252BEEEEF2C394BF13FC1E1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6466 = { sizeof (RandomUtils_t5051C16386B1EAA11EBD541E7D2591056760608B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6467 = { sizeof (StringUtils_t15FD11EEF0ACC9FA70BC4A7B4746B6ECED981AE4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6468 = { sizeof (ThreadSafeRandom_t73C8BC7D28AB4C56E5EA2130E90E6D88C4ED5D41), -1, sizeof(ThreadSafeRandom_t73C8BC7D28AB4C56E5EA2130E90E6D88C4ED5D41_StaticFields), sizeof(ThreadSafeRandom_t73C8BC7D28AB4C56E5EA2130E90E6D88C4ED5D41_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable6468[2] = 
{
	ThreadSafeRandom_t73C8BC7D28AB4C56E5EA2130E90E6D88C4ED5D41_StaticFields::get_offset_of_Global_0(),
	ThreadSafeRandom_t73C8BC7D28AB4C56E5EA2130E90E6D88C4ED5D41_ThreadStaticFields::get_offset_of_local_1() | THREAD_LOCAL_STATIC_MASK,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6469 = { sizeof (TimeUtils_t4A0F1B90D14DDC898D601A4F397D051BF305C3CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6469[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6470 = { sizeof (xxHash_t31A700F9BD226933F618C4C4665F640CE97C2A51), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6470[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6471 = { sizeof (SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C), -1, sizeof(SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6471[12] = 
{
	SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields::get_offset_of_inited_7(),
	SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields::get_offset_of_speedHackDetected_8(),
	SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields::get_offset_of_reliableTime_9(),
	SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields::get_offset_of_reliableDeltaTime_10(),
	SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields::get_offset_of_reliableUnscaledTime_11(),
	SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields::get_offset_of_reliableUnscaledDeltaTime_12(),
	SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields::get_offset_of_reliableRealtimeSinceStartup_13(),
	SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields::get_offset_of_reliableTimeSinceLevelLoad_14(),
	SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C_StaticFields::get_offset_of_warningShot_15(),
	SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C::get_offset_of_currentReliableTicks_16(),
	SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C::get_offset_of_lastFrameReliableTicks_17(),
	SpeedHackProofTime_tE628F6372CF4274190B0BF833F0E10AF9D47E23C::get_offset_of_reliableTicksDelta_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6472 = { sizeof (ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75), -1, sizeof(ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6472[27] = 
{
	0,
	0,
	0,
	0,
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_alterationReported_4(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_foreignSavesReported_5(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_deviceId_6(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_cryptoKeyObsolete_7(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_cryptoKeyObsoleteForMigration_8(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_deviceIdHash_9(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_OnAlterationDetected_10(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_OnPossibleForeignSavesDetected_11(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_preservePlayerPrefs_12(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_lockToDevice_13(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_readForeignSaves_14(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_emergencyMode_15(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_IntType_16(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_UIntType_17(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_StringType_18(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_FloatType_19(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_DoubleType_20(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_DecimalType_21(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_LongType_22(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_ULongType_23(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_BoolType_24(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_generatedCryptoKey_25(),
	ObscuredPrefs_tECD4A8C13FF0745D8C958CDA7541FD55AB775C75_StaticFields::get_offset_of_migratingFromACTkV1_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6473 = { sizeof (DataType_t239D221E2F1F3CB5396A74F07DA04CF862D61F23)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6473[17] = 
{
	DataType_t239D221E2F1F3CB5396A74F07DA04CF862D61F23::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6474 = { sizeof (DeviceLockLevel_t343109F2BB7200998C3CB19E58B09AA719CA84C0)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6474[4] = 
{
	DeviceLockLevel_t343109F2BB7200998C3CB19E58B09AA719CA84C0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6475 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6476 = { sizeof (ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452)+ sizeof (RuntimeObject), sizeof(ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6476[5] = 
{
	ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452::get_offset_of_currentCryptoKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452::get_offset_of_hiddenValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452::get_offset_of_inited_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredBool_t2CC3647BFB7D4EE9035407A3B3579A429F018452::get_offset_of_fakeValueActive_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6477 = { sizeof (ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB)+ sizeof (RuntimeObject), sizeof(ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6477[5] = 
{
	ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB::get_offset_of_currentCryptoKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB::get_offset_of_hiddenValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB::get_offset_of_inited_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredByte_tAE91D60FE3D8F76F2E15B86CEEEB8660FCD317AB::get_offset_of_fakeValueActive_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6478 = { sizeof (ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266)+ sizeof (RuntimeObject), sizeof(ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6478[5] = 
{
	ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266::get_offset_of_currentCryptoKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266::get_offset_of_hiddenValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266::get_offset_of_inited_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredChar_t2A92A080202CC0C24D08B780B2F39CED1A3BA266::get_offset_of_fakeValueActive_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6479 = { sizeof (ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C)+ sizeof (RuntimeObject), sizeof(ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6479[5] = 
{
	ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C::get_offset_of_currentCryptoKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C::get_offset_of_hiddenValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C::get_offset_of_inited_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredDecimal_t8FDFD59D7553624B2DC58962B8254E274B6E301C::get_offset_of_fakeValueActive_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6480 = { sizeof (DecimalLongBytesUnion_t950EAF510D7465C08CCE3AA133D59507B66A06B9)+ sizeof (RuntimeObject), sizeof(DecimalLongBytesUnion_t950EAF510D7465C08CCE3AA133D59507B66A06B9 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6480[4] = 
{
	DecimalLongBytesUnion_t950EAF510D7465C08CCE3AA133D59507B66A06B9::get_offset_of_d_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalLongBytesUnion_t950EAF510D7465C08CCE3AA133D59507B66A06B9::get_offset_of_l1_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalLongBytesUnion_t950EAF510D7465C08CCE3AA133D59507B66A06B9::get_offset_of_l2_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DecimalLongBytesUnion_t950EAF510D7465C08CCE3AA133D59507B66A06B9::get_offset_of_b16_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6481 = { sizeof (ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B)+ sizeof (RuntimeObject), sizeof(ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6481[6] = 
{
	ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B::get_offset_of_currentCryptoKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B::get_offset_of_hiddenValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B::get_offset_of_hiddenValueOldByte8_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B::get_offset_of_inited_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B::get_offset_of_fakeValue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredDouble_t482D3EFF59CAD02A1890E031E5215115ED10F71B::get_offset_of_fakeValueActive_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6482 = { sizeof (DoubleLongBytesUnion_t052DF0AE955CA3ABEBD3C19ED4CD222D89F74458)+ sizeof (RuntimeObject), sizeof(DoubleLongBytesUnion_t052DF0AE955CA3ABEBD3C19ED4CD222D89F74458 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6482[3] = 
{
	DoubleLongBytesUnion_t052DF0AE955CA3ABEBD3C19ED4CD222D89F74458::get_offset_of_d_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleLongBytesUnion_t052DF0AE955CA3ABEBD3C19ED4CD222D89F74458::get_offset_of_l_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DoubleLongBytesUnion_t052DF0AE955CA3ABEBD3C19ED4CD222D89F74458::get_offset_of_b8_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6483 = { sizeof (ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077)+ sizeof (RuntimeObject), sizeof(ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6483[6] = 
{
	ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077::get_offset_of_currentCryptoKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077::get_offset_of_hiddenValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077::get_offset_of_hiddenValueOldByte4_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077::get_offset_of_inited_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077::get_offset_of_fakeValue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredFloat_t4B3D789586D1D401EB5B7976A5C4CAC9AAF0A077::get_offset_of_fakeValueActive_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6484 = { sizeof (FloatIntBytesUnion_tD999463DD3670D86407DB05972540430AB56D42B)+ sizeof (RuntimeObject), sizeof(FloatIntBytesUnion_tD999463DD3670D86407DB05972540430AB56D42B ), 0, 0 };
extern const int32_t g_FieldOffsetTable6484[3] = 
{
	FloatIntBytesUnion_tD999463DD3670D86407DB05972540430AB56D42B::get_offset_of_f_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatIntBytesUnion_tD999463DD3670D86407DB05972540430AB56D42B::get_offset_of_i_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FloatIntBytesUnion_tD999463DD3670D86407DB05972540430AB56D42B::get_offset_of_b4_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6485 = { sizeof (ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489)+ sizeof (RuntimeObject), sizeof(ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6485[5] = 
{
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489::get_offset_of_currentCryptoKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489::get_offset_of_hiddenValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489::get_offset_of_inited_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredInt_tBF7AD58D75DF4170E88029FF34F72A59951A2489::get_offset_of_fakeValueActive_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6486 = { sizeof (ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609)+ sizeof (RuntimeObject), sizeof(ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6486[5] = 
{
	ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609::get_offset_of_currentCryptoKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609::get_offset_of_hiddenValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609::get_offset_of_inited_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredLong_t3A2E6B6B36061DD41638EAFD4FE975FC66CCE609::get_offset_of_fakeValueActive_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6487 = { sizeof (ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A)+ sizeof (RuntimeObject), sizeof(ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A_marshaled_pinvoke), sizeof(ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6487[6] = 
{
	ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A_StaticFields::get_offset_of_Identity_0(),
	ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A::get_offset_of_inited_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A::get_offset_of_fakeValue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredQuaternion_tAE6966A12AB044F8E660DEEB7CED4EC2550A257A::get_offset_of_fakeValueActive_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6488 = { sizeof (RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7)+ sizeof (RuntimeObject), sizeof(RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6488[4] = 
{
	RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RawEncryptedQuaternion_t51F35AA06331DDA720BFC1CC0A84E6A8E4245DE7::get_offset_of_w_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6489 = { sizeof (ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5)+ sizeof (RuntimeObject), sizeof(ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6489[5] = 
{
	ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5::get_offset_of_currentCryptoKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5::get_offset_of_hiddenValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5::get_offset_of_inited_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredSByte_tBFFB3C503276E226BA65C81689A69D3AEBC803A5::get_offset_of_fakeValueActive_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6490 = { sizeof (ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F)+ sizeof (RuntimeObject), sizeof(ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6490[5] = 
{
	ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F::get_offset_of_currentCryptoKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F::get_offset_of_hiddenValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F::get_offset_of_inited_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredShort_t3CE260C58A3B8F811BFB8FF5FD852F45C8BCB78F::get_offset_of_fakeValueActive_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6491 = { sizeof (ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6491[7] = 
{
	ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301::get_offset_of_currentCryptoKey_0(),
	ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301::get_offset_of_hiddenValue_1(),
	ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301::get_offset_of_cryptoKey_2(),
	ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301::get_offset_of_hiddenChars_3(),
	ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301::get_offset_of_inited_4(),
	ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301::get_offset_of_fakeValue_5(),
	ObscuredString_t40CD1B096CD406BAD6EE65842C59A7814BE0B301::get_offset_of_fakeValueActive_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6492 = { sizeof (ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473)+ sizeof (RuntimeObject), sizeof(ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6492[5] = 
{
	ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473::get_offset_of_currentCryptoKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473::get_offset_of_hiddenValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473::get_offset_of_inited_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredUInt_t473890BB340A9E2E57D22A4B0599FA3104D3B473::get_offset_of_fakeValueActive_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6493 = { sizeof (ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB)+ sizeof (RuntimeObject), sizeof(ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6493[5] = 
{
	ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB::get_offset_of_currentCryptoKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB::get_offset_of_hiddenValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB::get_offset_of_inited_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredULong_t690D7C9B998CA88E496894395B7CBAB00B652ECB::get_offset_of_fakeValueActive_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6494 = { sizeof (ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF)+ sizeof (RuntimeObject), sizeof(ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6494[5] = 
{
	ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF::get_offset_of_currentCryptoKey_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF::get_offset_of_hiddenValue_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF::get_offset_of_inited_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF::get_offset_of_fakeValue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredUShort_t0DC9F5CCECED350EFE386F1AF70ACE885C1333DF::get_offset_of_fakeValueActive_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6495 = { sizeof (ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855)+ sizeof (RuntimeObject), sizeof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855_marshaled_pinvoke), sizeof(ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6495[6] = 
{
	ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855_StaticFields::get_offset_of_Zero_0(),
	ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855::get_offset_of_inited_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855::get_offset_of_fakeValue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector2_t11B55619F5729BB5B30E922263095D6D862D5855::get_offset_of_fakeValueActive_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6496 = { sizeof (RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC)+ sizeof (RuntimeObject), sizeof(RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC ), 0, 0 };
extern const int32_t g_FieldOffsetTable6496[2] = 
{
	RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RawEncryptedVector2_tFF1E132A3237D22895804BE30F9011B94E9BF5CC::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6497 = { sizeof (ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A)+ sizeof (RuntimeObject), sizeof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_marshaled_pinvoke), sizeof(ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6497[6] = 
{
	ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A_StaticFields::get_offset_of_Zero_0(),
	ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A::get_offset_of_inited_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A::get_offset_of_fakeValue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector2Int_t195E442A5BB393AC56E8F1C0A984EBBE0911CA8A::get_offset_of_fakeValueActive_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6498 = { sizeof (RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D)+ sizeof (RuntimeObject), sizeof(RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D ), 0, 0 };
extern const int32_t g_FieldOffsetTable6498[2] = 
{
	RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RawEncryptedVector2Int_t7355329D63C8B123505CCAFD38B6F28BD9093F7D::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6499 = { sizeof (ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781)+ sizeof (RuntimeObject), sizeof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781_marshaled_pinvoke), sizeof(ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6499[6] = 
{
	ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781_StaticFields::get_offset_of_Zero_0(),
	ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781::get_offset_of_inited_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781::get_offset_of_fakeValue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector3_t78946FDA503DFD5D6975074F4408EDCF397E1781::get_offset_of_fakeValueActive_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
