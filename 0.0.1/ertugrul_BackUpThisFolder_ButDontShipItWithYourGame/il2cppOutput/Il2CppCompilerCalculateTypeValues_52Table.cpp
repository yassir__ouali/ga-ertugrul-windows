﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7;
// Org.BouncyCastle.Crypto.IBlockCipher
struct IBlockCipher_tB1BAE99FDCBFA6193F91C083240BC00B7D01A612;
// Org.BouncyCastle.Crypto.IMac
struct IMac_tE3D6999115C3CC3F0F9D0E03C6DD9003F350FB24;
// Org.BouncyCastle.Crypto.IStreamCipher
struct IStreamCipher_t6D976F6B56B231A900E4D822447A09780C29A22A;
// Org.BouncyCastle.Crypto.Parameters.DHParameters
struct DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513;
// Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters
struct DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C;
// Org.BouncyCastle.Crypto.Parameters.DHPublicKeyParameters
struct DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA;
// Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters
struct ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F;
// Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters
struct ECPublicKeyParameters_t84FD118FD3407D284EF46E8358AE185634537702;
// Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters
struct RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308;
// Org.BouncyCastle.Crypto.Prng.IRandomGenerator
struct IRandomGenerator_tEFF6415248F4D37A24017AB5D51691A43740A6CB;
// Org.BouncyCastle.Crypto.Tls.ByteQueue
struct ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26;
// Org.BouncyCastle.Crypto.Tls.Certificate
struct Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D;
// Org.BouncyCastle.Crypto.Tls.CertificateRequest
struct CertificateRequest_t2D3D094933C7C24EA9ED681B96C590E0488E0105;
// Org.BouncyCastle.Crypto.Tls.CertificateStatus
struct CertificateStatus_t1F391A4805FD1E6E500F7F5AD46A312FF7057AEA;
// Org.BouncyCastle.Crypto.Tls.ProtocolVersion
struct ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D;
// Org.BouncyCastle.Crypto.Tls.RecordStream
struct RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19;
// Org.BouncyCastle.Crypto.Tls.SecurityParameters
struct SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB;
// Org.BouncyCastle.Crypto.Tls.SessionParameters
struct SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115;
// Org.BouncyCastle.Crypto.Tls.TlsAgreementCredentials
struct TlsAgreementCredentials_tB2CCD6B665914E3999901CA7DCAAC89E161776C0;
// Org.BouncyCastle.Crypto.Tls.TlsAuthentication
struct TlsAuthentication_tED2CA615DF2DC335E5EA36897B9A59ECF191F150;
// Org.BouncyCastle.Crypto.Tls.TlsClient
struct TlsClient_tD830BE66E6EAB2D6ED2342EEB4D84A343B783A12;
// Org.BouncyCastle.Crypto.Tls.TlsClientContextImpl
struct TlsClientContextImpl_t24CB6216F1148B76E46DB71DF497691FC2179BD4;
// Org.BouncyCastle.Crypto.Tls.TlsContext
struct TlsContext_tF27CFFAEDC150666C5502AF25C2966952F7EEAE6;
// Org.BouncyCastle.Crypto.Tls.TlsKeyExchange
struct TlsKeyExchange_t73BF0ED4A740832D02CF087BE43F1AD996EC08AF;
// Org.BouncyCastle.Crypto.Tls.TlsMac
struct TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194;
// Org.BouncyCastle.Crypto.Tls.TlsProtocol
struct TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931;
// Org.BouncyCastle.Crypto.Tls.TlsSession
struct TlsSession_t7F9585B1263657CC2C8F2CDDE578D0D14C15C079;
// Org.BouncyCastle.Crypto.Tls.TlsSigner
struct TlsSigner_tFF481A309C0450B549040BA606E18083819B8212;
// Org.BouncyCastle.Crypto.Tls.TlsStream
struct TlsStream_t5204899C7CE492C3A11C1A16C01BFEE1D4D6D124;
// Org.BouncyCastle.Math.BigInteger
struct BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91;
// Org.BouncyCastle.Math.BigInteger[]
struct BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311;
// Org.BouncyCastle.Math.EC.Abc.ZTauElement[]
struct ZTauElementU5BU5D_t0F50BD7EC8CDA6D7979BA0BB4A181311AC67852D;
// Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519Point
struct Curve25519Point_tB1B0391C34923E88E9E7E2B1A69BC886F173954B;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Point
struct SecP128R1Point_t80EF8EC90E8CF9D611D823201274CF9C28D77772;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Point
struct SecP160K1Point_tE15730C753F99380C34AD223AA068D1C959F8F86;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Point
struct SecP160R1Point_t9D5C7DB7BC9CF68839652EC9D2DC5D4A95A30943;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Point
struct SecP160R2Point_t5F4EE8D61DD9048C48BD4FDE87BD37C51E6970AF;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Point
struct SecP192K1Point_tA3A3258416DE02E73000D80D445C6627A91A4B77;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Point
struct SecP192R1Point_tF263B963714C357592112834E6767394E3560E9E;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Point
struct SecP224K1Point_t6DCA6A3B396FB10CA6D991B1354C748BC4F9235F;
// Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Point
struct SecP224R1Point_t3DD809782D037568CA3A200FC33A8DA7390405CD;
// Org.BouncyCastle.Math.EC.ECCurve
struct ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95;
// Org.BouncyCastle.Math.EC.ECFieldElement
struct ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C;
// Org.BouncyCastle.Math.EC.ECFieldElement[]
struct ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609;
// Org.BouncyCastle.Math.EC.Endo.ECEndomorphism
struct ECEndomorphism_t1C3A93A26892C9712CFA1B9ACAD2ABA7E367425C;
// Org.BouncyCastle.Math.EC.F2mPoint
struct F2mPoint_t7E9FECF441D8AA8E095B486D519BDBD04B0D66B7;
// Org.BouncyCastle.Math.EC.FpPoint
struct FpPoint_t7EA8AC711BD693B5C8A3B62DCFB98976FDC9779B;
// Org.BouncyCastle.Math.EC.LongArray
struct LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96;
// Org.BouncyCastle.Math.EC.Multiplier.ECMultiplier
struct ECMultiplier_tD7F75847287D50B6E100B15DF8A7C4DC6C40753B;
// Org.BouncyCastle.Math.Field.IFiniteField
struct IFiniteField_t3875D72597A5D0021445EA9C8402D94F7B904229;
// Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0;
// Org.BouncyCastle.Utilities.Zlib.ZStream
struct ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int16[]
struct Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43;
// System.Int64[]
struct Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.SByte[][]
struct SByteU5BU5DU5BU5D_t99E68B8F036DFF48A8903ACA82EF9B4A4C77E3DF;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87;
// System.UInt16[]
struct UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ABSTRACTTLSCONTEXT_T6443BAD6549F9D29772C07D635DD73AB72E2D418_H
#define ABSTRACTTLSCONTEXT_T6443BAD6549F9D29772C07D635DD73AB72E2D418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AbstractTlsContext
struct  AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Prng.IRandomGenerator Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mNonceRandom
	RuntimeObject* ___mNonceRandom_1;
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSecureRandom
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___mSecureRandom_2;
	// Org.BouncyCastle.Crypto.Tls.SecurityParameters Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSecurityParameters
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB * ___mSecurityParameters_3;
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mClientVersion
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * ___mClientVersion_4;
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mServerVersion
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * ___mServerVersion_5;
	// Org.BouncyCastle.Crypto.Tls.TlsSession Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSession
	RuntimeObject* ___mSession_6;

public:
	inline static int32_t get_offset_of_mNonceRandom_1() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418, ___mNonceRandom_1)); }
	inline RuntimeObject* get_mNonceRandom_1() const { return ___mNonceRandom_1; }
	inline RuntimeObject** get_address_of_mNonceRandom_1() { return &___mNonceRandom_1; }
	inline void set_mNonceRandom_1(RuntimeObject* value)
	{
		___mNonceRandom_1 = value;
		Il2CppCodeGenWriteBarrier((&___mNonceRandom_1), value);
	}

	inline static int32_t get_offset_of_mSecureRandom_2() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418, ___mSecureRandom_2)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_mSecureRandom_2() const { return ___mSecureRandom_2; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_mSecureRandom_2() { return &___mSecureRandom_2; }
	inline void set_mSecureRandom_2(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___mSecureRandom_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSecureRandom_2), value);
	}

	inline static int32_t get_offset_of_mSecurityParameters_3() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418, ___mSecurityParameters_3)); }
	inline SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB * get_mSecurityParameters_3() const { return ___mSecurityParameters_3; }
	inline SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB ** get_address_of_mSecurityParameters_3() { return &___mSecurityParameters_3; }
	inline void set_mSecurityParameters_3(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB * value)
	{
		___mSecurityParameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSecurityParameters_3), value);
	}

	inline static int32_t get_offset_of_mClientVersion_4() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418, ___mClientVersion_4)); }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * get_mClientVersion_4() const { return ___mClientVersion_4; }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D ** get_address_of_mClientVersion_4() { return &___mClientVersion_4; }
	inline void set_mClientVersion_4(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * value)
	{
		___mClientVersion_4 = value;
		Il2CppCodeGenWriteBarrier((&___mClientVersion_4), value);
	}

	inline static int32_t get_offset_of_mServerVersion_5() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418, ___mServerVersion_5)); }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * get_mServerVersion_5() const { return ___mServerVersion_5; }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D ** get_address_of_mServerVersion_5() { return &___mServerVersion_5; }
	inline void set_mServerVersion_5(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * value)
	{
		___mServerVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___mServerVersion_5), value);
	}

	inline static int32_t get_offset_of_mSession_6() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418, ___mSession_6)); }
	inline RuntimeObject* get_mSession_6() const { return ___mSession_6; }
	inline RuntimeObject** get_address_of_mSession_6() { return &___mSession_6; }
	inline void set_mSession_6(RuntimeObject* value)
	{
		___mSession_6 = value;
		Il2CppCodeGenWriteBarrier((&___mSession_6), value);
	}
};

struct AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418_StaticFields
{
public:
	// System.Int64 Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::counter
	int64_t ___counter_0;

public:
	inline static int32_t get_offset_of_counter_0() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418_StaticFields, ___counter_0)); }
	inline int64_t get_counter_0() const { return ___counter_0; }
	inline int64_t* get_address_of_counter_0() { return &___counter_0; }
	inline void set_counter_0(int64_t value)
	{
		___counter_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCONTEXT_T6443BAD6549F9D29772C07D635DD73AB72E2D418_H
#ifndef ABSTRACTTLSKEYEXCHANGE_T1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35_H
#define ABSTRACTTLSKEYEXCHANGE_T1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange
struct  AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mKeyExchange
	int32_t ___mKeyExchange_0;
	// System.Collections.IList Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_1;
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mContext
	RuntimeObject* ___mContext_2;

public:
	inline static int32_t get_offset_of_mKeyExchange_0() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35, ___mKeyExchange_0)); }
	inline int32_t get_mKeyExchange_0() const { return ___mKeyExchange_0; }
	inline int32_t* get_address_of_mKeyExchange_0() { return &___mKeyExchange_0; }
	inline void set_mKeyExchange_0(int32_t value)
	{
		___mKeyExchange_0 = value;
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_1() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35, ___mSupportedSignatureAlgorithms_1)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_1() const { return ___mSupportedSignatureAlgorithms_1; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_1() { return &___mSupportedSignatureAlgorithms_1; }
	inline void set_mSupportedSignatureAlgorithms_1(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_1), value);
	}

	inline static int32_t get_offset_of_mContext_2() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35, ___mContext_2)); }
	inline RuntimeObject* get_mContext_2() const { return ___mContext_2; }
	inline RuntimeObject** get_address_of_mContext_2() { return &___mContext_2; }
	inline void set_mContext_2(RuntimeObject* value)
	{
		___mContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSKEYEXCHANGE_T1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35_H
#ifndef ABSTRACTTLSSIGNER_T69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE_H
#define ABSTRACTTLSSIGNER_T69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AbstractTlsSigner
struct  AbstractTlsSigner_t69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.AbstractTlsSigner::mContext
	RuntimeObject* ___mContext_0;

public:
	inline static int32_t get_offset_of_mContext_0() { return static_cast<int32_t>(offsetof(AbstractTlsSigner_t69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE, ___mContext_0)); }
	inline RuntimeObject* get_mContext_0() const { return ___mContext_0; }
	inline RuntimeObject** get_address_of_mContext_0() { return &___mContext_0; }
	inline void set_mContext_0(RuntimeObject* value)
	{
		___mContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSSIGNER_T69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE_H
#ifndef TLSBLOCKCIPHER_T51395AD0E411C7A37D1F10F06C93F4C5814E4F72_H
#define TLSBLOCKCIPHER_T51395AD0E411C7A37D1F10F06C93F4C5814E4F72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsBlockCipher
struct  TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::context
	RuntimeObject* ___context_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::randomData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___randomData_1;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::useExplicitIV
	bool ___useExplicitIV_2;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::encryptThenMac
	bool ___encryptThenMac_3;
	// Org.BouncyCastle.Crypto.IBlockCipher Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::encryptCipher
	RuntimeObject* ___encryptCipher_4;
	// Org.BouncyCastle.Crypto.IBlockCipher Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::decryptCipher
	RuntimeObject* ___decryptCipher_5;
	// Org.BouncyCastle.Crypto.Tls.TlsMac Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::mWriteMac
	TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * ___mWriteMac_6;
	// Org.BouncyCastle.Crypto.Tls.TlsMac Org.BouncyCastle.Crypto.Tls.TlsBlockCipher::mReadMac
	TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * ___mReadMac_7;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72, ___context_0)); }
	inline RuntimeObject* get_context_0() const { return ___context_0; }
	inline RuntimeObject** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(RuntimeObject* value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}

	inline static int32_t get_offset_of_randomData_1() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72, ___randomData_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_randomData_1() const { return ___randomData_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_randomData_1() { return &___randomData_1; }
	inline void set_randomData_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___randomData_1 = value;
		Il2CppCodeGenWriteBarrier((&___randomData_1), value);
	}

	inline static int32_t get_offset_of_useExplicitIV_2() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72, ___useExplicitIV_2)); }
	inline bool get_useExplicitIV_2() const { return ___useExplicitIV_2; }
	inline bool* get_address_of_useExplicitIV_2() { return &___useExplicitIV_2; }
	inline void set_useExplicitIV_2(bool value)
	{
		___useExplicitIV_2 = value;
	}

	inline static int32_t get_offset_of_encryptThenMac_3() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72, ___encryptThenMac_3)); }
	inline bool get_encryptThenMac_3() const { return ___encryptThenMac_3; }
	inline bool* get_address_of_encryptThenMac_3() { return &___encryptThenMac_3; }
	inline void set_encryptThenMac_3(bool value)
	{
		___encryptThenMac_3 = value;
	}

	inline static int32_t get_offset_of_encryptCipher_4() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72, ___encryptCipher_4)); }
	inline RuntimeObject* get_encryptCipher_4() const { return ___encryptCipher_4; }
	inline RuntimeObject** get_address_of_encryptCipher_4() { return &___encryptCipher_4; }
	inline void set_encryptCipher_4(RuntimeObject* value)
	{
		___encryptCipher_4 = value;
		Il2CppCodeGenWriteBarrier((&___encryptCipher_4), value);
	}

	inline static int32_t get_offset_of_decryptCipher_5() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72, ___decryptCipher_5)); }
	inline RuntimeObject* get_decryptCipher_5() const { return ___decryptCipher_5; }
	inline RuntimeObject** get_address_of_decryptCipher_5() { return &___decryptCipher_5; }
	inline void set_decryptCipher_5(RuntimeObject* value)
	{
		___decryptCipher_5 = value;
		Il2CppCodeGenWriteBarrier((&___decryptCipher_5), value);
	}

	inline static int32_t get_offset_of_mWriteMac_6() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72, ___mWriteMac_6)); }
	inline TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * get_mWriteMac_6() const { return ___mWriteMac_6; }
	inline TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 ** get_address_of_mWriteMac_6() { return &___mWriteMac_6; }
	inline void set_mWriteMac_6(TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * value)
	{
		___mWriteMac_6 = value;
		Il2CppCodeGenWriteBarrier((&___mWriteMac_6), value);
	}

	inline static int32_t get_offset_of_mReadMac_7() { return static_cast<int32_t>(offsetof(TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72, ___mReadMac_7)); }
	inline TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * get_mReadMac_7() const { return ___mReadMac_7; }
	inline TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 ** get_address_of_mReadMac_7() { return &___mReadMac_7; }
	inline void set_mReadMac_7(TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * value)
	{
		___mReadMac_7 = value;
		Il2CppCodeGenWriteBarrier((&___mReadMac_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSBLOCKCIPHER_T51395AD0E411C7A37D1F10F06C93F4C5814E4F72_H
#ifndef TLSDHUTILITIES_TEF9B825C23804AFA8D4031006206C0549BCBF880_H
#define TLSDHUTILITIES_TEF9B825C23804AFA8D4031006206C0549BCBF880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsDHUtilities
struct  TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880  : public RuntimeObject
{
public:

public:
};

struct TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::Two
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Two_0;
	// System.String Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe2432_p
	String_t* ___draft_ffdhe2432_p_1;
	// Org.BouncyCastle.Crypto.Parameters.DHParameters Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe2432
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * ___draft_ffdhe2432_2;
	// System.String Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe3072_p
	String_t* ___draft_ffdhe3072_p_3;
	// Org.BouncyCastle.Crypto.Parameters.DHParameters Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe3072
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * ___draft_ffdhe3072_4;
	// System.String Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe4096_p
	String_t* ___draft_ffdhe4096_p_5;
	// Org.BouncyCastle.Crypto.Parameters.DHParameters Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe4096
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * ___draft_ffdhe4096_6;
	// System.String Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe6144_p
	String_t* ___draft_ffdhe6144_p_7;
	// Org.BouncyCastle.Crypto.Parameters.DHParameters Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe6144
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * ___draft_ffdhe6144_8;
	// System.String Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe8192_p
	String_t* ___draft_ffdhe8192_p_9;
	// Org.BouncyCastle.Crypto.Parameters.DHParameters Org.BouncyCastle.Crypto.Tls.TlsDHUtilities::draft_ffdhe8192
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * ___draft_ffdhe8192_10;

public:
	inline static int32_t get_offset_of_Two_0() { return static_cast<int32_t>(offsetof(TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields, ___Two_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Two_0() const { return ___Two_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Two_0() { return &___Two_0; }
	inline void set_Two_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Two_0 = value;
		Il2CppCodeGenWriteBarrier((&___Two_0), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe2432_p_1() { return static_cast<int32_t>(offsetof(TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields, ___draft_ffdhe2432_p_1)); }
	inline String_t* get_draft_ffdhe2432_p_1() const { return ___draft_ffdhe2432_p_1; }
	inline String_t** get_address_of_draft_ffdhe2432_p_1() { return &___draft_ffdhe2432_p_1; }
	inline void set_draft_ffdhe2432_p_1(String_t* value)
	{
		___draft_ffdhe2432_p_1 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe2432_p_1), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe2432_2() { return static_cast<int32_t>(offsetof(TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields, ___draft_ffdhe2432_2)); }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * get_draft_ffdhe2432_2() const { return ___draft_ffdhe2432_2; }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 ** get_address_of_draft_ffdhe2432_2() { return &___draft_ffdhe2432_2; }
	inline void set_draft_ffdhe2432_2(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * value)
	{
		___draft_ffdhe2432_2 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe2432_2), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe3072_p_3() { return static_cast<int32_t>(offsetof(TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields, ___draft_ffdhe3072_p_3)); }
	inline String_t* get_draft_ffdhe3072_p_3() const { return ___draft_ffdhe3072_p_3; }
	inline String_t** get_address_of_draft_ffdhe3072_p_3() { return &___draft_ffdhe3072_p_3; }
	inline void set_draft_ffdhe3072_p_3(String_t* value)
	{
		___draft_ffdhe3072_p_3 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe3072_p_3), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe3072_4() { return static_cast<int32_t>(offsetof(TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields, ___draft_ffdhe3072_4)); }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * get_draft_ffdhe3072_4() const { return ___draft_ffdhe3072_4; }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 ** get_address_of_draft_ffdhe3072_4() { return &___draft_ffdhe3072_4; }
	inline void set_draft_ffdhe3072_4(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * value)
	{
		___draft_ffdhe3072_4 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe3072_4), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe4096_p_5() { return static_cast<int32_t>(offsetof(TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields, ___draft_ffdhe4096_p_5)); }
	inline String_t* get_draft_ffdhe4096_p_5() const { return ___draft_ffdhe4096_p_5; }
	inline String_t** get_address_of_draft_ffdhe4096_p_5() { return &___draft_ffdhe4096_p_5; }
	inline void set_draft_ffdhe4096_p_5(String_t* value)
	{
		___draft_ffdhe4096_p_5 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe4096_p_5), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe4096_6() { return static_cast<int32_t>(offsetof(TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields, ___draft_ffdhe4096_6)); }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * get_draft_ffdhe4096_6() const { return ___draft_ffdhe4096_6; }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 ** get_address_of_draft_ffdhe4096_6() { return &___draft_ffdhe4096_6; }
	inline void set_draft_ffdhe4096_6(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * value)
	{
		___draft_ffdhe4096_6 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe4096_6), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe6144_p_7() { return static_cast<int32_t>(offsetof(TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields, ___draft_ffdhe6144_p_7)); }
	inline String_t* get_draft_ffdhe6144_p_7() const { return ___draft_ffdhe6144_p_7; }
	inline String_t** get_address_of_draft_ffdhe6144_p_7() { return &___draft_ffdhe6144_p_7; }
	inline void set_draft_ffdhe6144_p_7(String_t* value)
	{
		___draft_ffdhe6144_p_7 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe6144_p_7), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe6144_8() { return static_cast<int32_t>(offsetof(TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields, ___draft_ffdhe6144_8)); }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * get_draft_ffdhe6144_8() const { return ___draft_ffdhe6144_8; }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 ** get_address_of_draft_ffdhe6144_8() { return &___draft_ffdhe6144_8; }
	inline void set_draft_ffdhe6144_8(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * value)
	{
		___draft_ffdhe6144_8 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe6144_8), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe8192_p_9() { return static_cast<int32_t>(offsetof(TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields, ___draft_ffdhe8192_p_9)); }
	inline String_t* get_draft_ffdhe8192_p_9() const { return ___draft_ffdhe8192_p_9; }
	inline String_t** get_address_of_draft_ffdhe8192_p_9() { return &___draft_ffdhe8192_p_9; }
	inline void set_draft_ffdhe8192_p_9(String_t* value)
	{
		___draft_ffdhe8192_p_9 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe8192_p_9), value);
	}

	inline static int32_t get_offset_of_draft_ffdhe8192_10() { return static_cast<int32_t>(offsetof(TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields, ___draft_ffdhe8192_10)); }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * get_draft_ffdhe8192_10() const { return ___draft_ffdhe8192_10; }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 ** get_address_of_draft_ffdhe8192_10() { return &___draft_ffdhe8192_10; }
	inline void set_draft_ffdhe8192_10(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * value)
	{
		___draft_ffdhe8192_10 = value;
		Il2CppCodeGenWriteBarrier((&___draft_ffdhe8192_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSDHUTILITIES_TEF9B825C23804AFA8D4031006206C0549BCBF880_H
#ifndef TLSDEFLATECOMPRESSION_TAAE821D33B93FE40361ABA030C699CBBC2C513CB_H
#define TLSDEFLATECOMPRESSION_TAAE821D33B93FE40361ABA030C699CBBC2C513CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsDeflateCompression
struct  TlsDeflateCompression_tAAE821D33B93FE40361ABA030C699CBBC2C513CB  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Utilities.Zlib.ZStream Org.BouncyCastle.Crypto.Tls.TlsDeflateCompression::zIn
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * ___zIn_0;
	// Org.BouncyCastle.Utilities.Zlib.ZStream Org.BouncyCastle.Crypto.Tls.TlsDeflateCompression::zOut
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * ___zOut_1;

public:
	inline static int32_t get_offset_of_zIn_0() { return static_cast<int32_t>(offsetof(TlsDeflateCompression_tAAE821D33B93FE40361ABA030C699CBBC2C513CB, ___zIn_0)); }
	inline ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * get_zIn_0() const { return ___zIn_0; }
	inline ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB ** get_address_of_zIn_0() { return &___zIn_0; }
	inline void set_zIn_0(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * value)
	{
		___zIn_0 = value;
		Il2CppCodeGenWriteBarrier((&___zIn_0), value);
	}

	inline static int32_t get_offset_of_zOut_1() { return static_cast<int32_t>(offsetof(TlsDeflateCompression_tAAE821D33B93FE40361ABA030C699CBBC2C513CB, ___zOut_1)); }
	inline ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * get_zOut_1() const { return ___zOut_1; }
	inline ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB ** get_address_of_zOut_1() { return &___zOut_1; }
	inline void set_zOut_1(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * value)
	{
		___zOut_1 = value;
		Il2CppCodeGenWriteBarrier((&___zOut_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSDEFLATECOMPRESSION_TAAE821D33B93FE40361ABA030C699CBBC2C513CB_H
#ifndef TLSECCUTILITIES_TC54E1B1B6C214ED9D6B6C0599DD8AC4BD33F8E5D_H
#define TLSECCUTILITIES_TC54E1B1B6C214ED9D6B6C0599DD8AC4BD33F8E5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsEccUtilities
struct  TlsEccUtilities_tC54E1B1B6C214ED9D6B6C0599DD8AC4BD33F8E5D  : public RuntimeObject
{
public:

public:
};

struct TlsEccUtilities_tC54E1B1B6C214ED9D6B6C0599DD8AC4BD33F8E5D_StaticFields
{
public:
	// System.String[] Org.BouncyCastle.Crypto.Tls.TlsEccUtilities::CurveNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___CurveNames_0;

public:
	inline static int32_t get_offset_of_CurveNames_0() { return static_cast<int32_t>(offsetof(TlsEccUtilities_tC54E1B1B6C214ED9D6B6C0599DD8AC4BD33F8E5D_StaticFields, ___CurveNames_0)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_CurveNames_0() const { return ___CurveNames_0; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_CurveNames_0() { return &___CurveNames_0; }
	inline void set_CurveNames_0(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___CurveNames_0 = value;
		Il2CppCodeGenWriteBarrier((&___CurveNames_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSECCUTILITIES_TC54E1B1B6C214ED9D6B6C0599DD8AC4BD33F8E5D_H
#ifndef TLSEXTENSIONSUTILITIES_T83B52EEB5DCB95209232DF1C8D557B31231CEA0C_H
#define TLSEXTENSIONSUTILITIES_T83B52EEB5DCB95209232DF1C8D557B31231CEA0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsExtensionsUtilities
struct  TlsExtensionsUtilities_t83B52EEB5DCB95209232DF1C8D557B31231CEA0C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSEXTENSIONSUTILITIES_T83B52EEB5DCB95209232DF1C8D557B31231CEA0C_H
#ifndef TLSMAC_TA2D5BC2A61001B9745A1849D4DC2398036773194_H
#define TLSMAC_TA2D5BC2A61001B9745A1849D4DC2398036773194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsMac
struct  TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.TlsMac::context
	RuntimeObject* ___context_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsMac::secret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___secret_1;
	// Org.BouncyCastle.Crypto.IMac Org.BouncyCastle.Crypto.Tls.TlsMac::mac
	RuntimeObject* ___mac_2;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.TlsMac::digestBlockSize
	int32_t ___digestBlockSize_3;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.TlsMac::digestOverhead
	int32_t ___digestOverhead_4;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.TlsMac::macLength
	int32_t ___macLength_5;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194, ___context_0)); }
	inline RuntimeObject* get_context_0() const { return ___context_0; }
	inline RuntimeObject** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(RuntimeObject* value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}

	inline static int32_t get_offset_of_secret_1() { return static_cast<int32_t>(offsetof(TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194, ___secret_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_secret_1() const { return ___secret_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_secret_1() { return &___secret_1; }
	inline void set_secret_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___secret_1 = value;
		Il2CppCodeGenWriteBarrier((&___secret_1), value);
	}

	inline static int32_t get_offset_of_mac_2() { return static_cast<int32_t>(offsetof(TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194, ___mac_2)); }
	inline RuntimeObject* get_mac_2() const { return ___mac_2; }
	inline RuntimeObject** get_address_of_mac_2() { return &___mac_2; }
	inline void set_mac_2(RuntimeObject* value)
	{
		___mac_2 = value;
		Il2CppCodeGenWriteBarrier((&___mac_2), value);
	}

	inline static int32_t get_offset_of_digestBlockSize_3() { return static_cast<int32_t>(offsetof(TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194, ___digestBlockSize_3)); }
	inline int32_t get_digestBlockSize_3() const { return ___digestBlockSize_3; }
	inline int32_t* get_address_of_digestBlockSize_3() { return &___digestBlockSize_3; }
	inline void set_digestBlockSize_3(int32_t value)
	{
		___digestBlockSize_3 = value;
	}

	inline static int32_t get_offset_of_digestOverhead_4() { return static_cast<int32_t>(offsetof(TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194, ___digestOverhead_4)); }
	inline int32_t get_digestOverhead_4() const { return ___digestOverhead_4; }
	inline int32_t* get_address_of_digestOverhead_4() { return &___digestOverhead_4; }
	inline void set_digestOverhead_4(int32_t value)
	{
		___digestOverhead_4 = value;
	}

	inline static int32_t get_offset_of_macLength_5() { return static_cast<int32_t>(offsetof(TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194, ___macLength_5)); }
	inline int32_t get_macLength_5() const { return ___macLength_5; }
	inline int32_t* get_address_of_macLength_5() { return &___macLength_5; }
	inline void set_macLength_5(int32_t value)
	{
		___macLength_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSMAC_TA2D5BC2A61001B9745A1849D4DC2398036773194_H
#ifndef TLSNULLCIPHER_TC202BD474327CCFC9499EC7478E83E45DB4093C5_H
#define TLSNULLCIPHER_TC202BD474327CCFC9499EC7478E83E45DB4093C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsNullCipher
struct  TlsNullCipher_tC202BD474327CCFC9499EC7478E83E45DB4093C5  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.TlsNullCipher::context
	RuntimeObject* ___context_0;
	// Org.BouncyCastle.Crypto.Tls.TlsMac Org.BouncyCastle.Crypto.Tls.TlsNullCipher::writeMac
	TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * ___writeMac_1;
	// Org.BouncyCastle.Crypto.Tls.TlsMac Org.BouncyCastle.Crypto.Tls.TlsNullCipher::readMac
	TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * ___readMac_2;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(TlsNullCipher_tC202BD474327CCFC9499EC7478E83E45DB4093C5, ___context_0)); }
	inline RuntimeObject* get_context_0() const { return ___context_0; }
	inline RuntimeObject** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(RuntimeObject* value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}

	inline static int32_t get_offset_of_writeMac_1() { return static_cast<int32_t>(offsetof(TlsNullCipher_tC202BD474327CCFC9499EC7478E83E45DB4093C5, ___writeMac_1)); }
	inline TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * get_writeMac_1() const { return ___writeMac_1; }
	inline TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 ** get_address_of_writeMac_1() { return &___writeMac_1; }
	inline void set_writeMac_1(TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * value)
	{
		___writeMac_1 = value;
		Il2CppCodeGenWriteBarrier((&___writeMac_1), value);
	}

	inline static int32_t get_offset_of_readMac_2() { return static_cast<int32_t>(offsetof(TlsNullCipher_tC202BD474327CCFC9499EC7478E83E45DB4093C5, ___readMac_2)); }
	inline TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * get_readMac_2() const { return ___readMac_2; }
	inline TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 ** get_address_of_readMac_2() { return &___readMac_2; }
	inline void set_readMac_2(TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * value)
	{
		___readMac_2 = value;
		Il2CppCodeGenWriteBarrier((&___readMac_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSNULLCIPHER_TC202BD474327CCFC9499EC7478E83E45DB4093C5_H
#ifndef TLSNULLCOMPRESSION_T3D5BA7EB28D8E5D9CAA2C8C6C7D61A5E6651A969_H
#define TLSNULLCOMPRESSION_T3D5BA7EB28D8E5D9CAA2C8C6C7D61A5E6651A969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsNullCompression
struct  TlsNullCompression_t3D5BA7EB28D8E5D9CAA2C8C6C7D61A5E6651A969  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSNULLCOMPRESSION_T3D5BA7EB28D8E5D9CAA2C8C6C7D61A5E6651A969_H
#ifndef TLSRSAUTILITIES_T7C7D2198FE7F727B1666CE72D3DC82EABDEE3B4B_H
#define TLSRSAUTILITIES_T7C7D2198FE7F727B1666CE72D3DC82EABDEE3B4B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsRsaUtilities
struct  TlsRsaUtilities_t7C7D2198FE7F727B1666CE72D3DC82EABDEE3B4B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSRSAUTILITIES_T7C7D2198FE7F727B1666CE72D3DC82EABDEE3B4B_H
#ifndef TLSSESSIONIMPL_T937CCFC5FCDED7C43715D0FA6400A1A528AC19E0_H
#define TLSSESSIONIMPL_T937CCFC5FCDED7C43715D0FA6400A1A528AC19E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsSessionImpl
struct  TlsSessionImpl_t937CCFC5FCDED7C43715D0FA6400A1A528AC19E0  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsSessionImpl::mSessionID
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSessionID_0;
	// Org.BouncyCastle.Crypto.Tls.SessionParameters Org.BouncyCastle.Crypto.Tls.TlsSessionImpl::mSessionParameters
	SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115 * ___mSessionParameters_1;

public:
	inline static int32_t get_offset_of_mSessionID_0() { return static_cast<int32_t>(offsetof(TlsSessionImpl_t937CCFC5FCDED7C43715D0FA6400A1A528AC19E0, ___mSessionID_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSessionID_0() const { return ___mSessionID_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSessionID_0() { return &___mSessionID_0; }
	inline void set_mSessionID_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSessionID_0 = value;
		Il2CppCodeGenWriteBarrier((&___mSessionID_0), value);
	}

	inline static int32_t get_offset_of_mSessionParameters_1() { return static_cast<int32_t>(offsetof(TlsSessionImpl_t937CCFC5FCDED7C43715D0FA6400A1A528AC19E0, ___mSessionParameters_1)); }
	inline SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115 * get_mSessionParameters_1() const { return ___mSessionParameters_1; }
	inline SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115 ** get_address_of_mSessionParameters_1() { return &___mSessionParameters_1; }
	inline void set_mSessionParameters_1(SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115 * value)
	{
		___mSessionParameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSessionParameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSESSIONIMPL_T937CCFC5FCDED7C43715D0FA6400A1A528AC19E0_H
#ifndef TLSSTREAMCIPHER_TAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E_H
#define TLSSTREAMCIPHER_TAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsStreamCipher
struct  TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.TlsStreamCipher::context
	RuntimeObject* ___context_0;
	// Org.BouncyCastle.Crypto.IStreamCipher Org.BouncyCastle.Crypto.Tls.TlsStreamCipher::encryptCipher
	RuntimeObject* ___encryptCipher_1;
	// Org.BouncyCastle.Crypto.IStreamCipher Org.BouncyCastle.Crypto.Tls.TlsStreamCipher::decryptCipher
	RuntimeObject* ___decryptCipher_2;
	// Org.BouncyCastle.Crypto.Tls.TlsMac Org.BouncyCastle.Crypto.Tls.TlsStreamCipher::writeMac
	TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * ___writeMac_3;
	// Org.BouncyCastle.Crypto.Tls.TlsMac Org.BouncyCastle.Crypto.Tls.TlsStreamCipher::readMac
	TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * ___readMac_4;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsStreamCipher::usesNonce
	bool ___usesNonce_5;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E, ___context_0)); }
	inline RuntimeObject* get_context_0() const { return ___context_0; }
	inline RuntimeObject** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(RuntimeObject* value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}

	inline static int32_t get_offset_of_encryptCipher_1() { return static_cast<int32_t>(offsetof(TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E, ___encryptCipher_1)); }
	inline RuntimeObject* get_encryptCipher_1() const { return ___encryptCipher_1; }
	inline RuntimeObject** get_address_of_encryptCipher_1() { return &___encryptCipher_1; }
	inline void set_encryptCipher_1(RuntimeObject* value)
	{
		___encryptCipher_1 = value;
		Il2CppCodeGenWriteBarrier((&___encryptCipher_1), value);
	}

	inline static int32_t get_offset_of_decryptCipher_2() { return static_cast<int32_t>(offsetof(TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E, ___decryptCipher_2)); }
	inline RuntimeObject* get_decryptCipher_2() const { return ___decryptCipher_2; }
	inline RuntimeObject** get_address_of_decryptCipher_2() { return &___decryptCipher_2; }
	inline void set_decryptCipher_2(RuntimeObject* value)
	{
		___decryptCipher_2 = value;
		Il2CppCodeGenWriteBarrier((&___decryptCipher_2), value);
	}

	inline static int32_t get_offset_of_writeMac_3() { return static_cast<int32_t>(offsetof(TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E, ___writeMac_3)); }
	inline TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * get_writeMac_3() const { return ___writeMac_3; }
	inline TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 ** get_address_of_writeMac_3() { return &___writeMac_3; }
	inline void set_writeMac_3(TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * value)
	{
		___writeMac_3 = value;
		Il2CppCodeGenWriteBarrier((&___writeMac_3), value);
	}

	inline static int32_t get_offset_of_readMac_4() { return static_cast<int32_t>(offsetof(TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E, ___readMac_4)); }
	inline TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * get_readMac_4() const { return ___readMac_4; }
	inline TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 ** get_address_of_readMac_4() { return &___readMac_4; }
	inline void set_readMac_4(TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194 * value)
	{
		___readMac_4 = value;
		Il2CppCodeGenWriteBarrier((&___readMac_4), value);
	}

	inline static int32_t get_offset_of_usesNonce_5() { return static_cast<int32_t>(offsetof(TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E, ___usesNonce_5)); }
	inline bool get_usesNonce_5() const { return ___usesNonce_5; }
	inline bool* get_address_of_usesNonce_5() { return &___usesNonce_5; }
	inline void set_usesNonce_5(bool value)
	{
		___usesNonce_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSTREAMCIPHER_TAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E_H
#ifndef TLSUTILITIES_TE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_H
#define TLSUTILITIES_TE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsUtilities
struct  TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5  : public RuntimeObject
{
public:

public:
};

struct TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsUtilities::EmptyBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___EmptyBytes_0;
	// System.Int16[] Org.BouncyCastle.Crypto.Tls.TlsUtilities::EmptyShorts
	Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* ___EmptyShorts_1;
	// System.Int32[] Org.BouncyCastle.Crypto.Tls.TlsUtilities::EmptyInts
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___EmptyInts_2;
	// System.Int64[] Org.BouncyCastle.Crypto.Tls.TlsUtilities::EmptyLongs
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___EmptyLongs_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsUtilities::SSL_CLIENT
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___SSL_CLIENT_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsUtilities::SSL_SERVER
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___SSL_SERVER_5;
	// System.Byte[][] Org.BouncyCastle.Crypto.Tls.TlsUtilities::SSL3_CONST
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___SSL3_CONST_6;

public:
	inline static int32_t get_offset_of_EmptyBytes_0() { return static_cast<int32_t>(offsetof(TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields, ___EmptyBytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_EmptyBytes_0() const { return ___EmptyBytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_EmptyBytes_0() { return &___EmptyBytes_0; }
	inline void set_EmptyBytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___EmptyBytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyBytes_0), value);
	}

	inline static int32_t get_offset_of_EmptyShorts_1() { return static_cast<int32_t>(offsetof(TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields, ___EmptyShorts_1)); }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* get_EmptyShorts_1() const { return ___EmptyShorts_1; }
	inline Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28** get_address_of_EmptyShorts_1() { return &___EmptyShorts_1; }
	inline void set_EmptyShorts_1(Int16U5BU5D_tDA0F0B2730337F72E44DB024BE9818FA8EDE8D28* value)
	{
		___EmptyShorts_1 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyShorts_1), value);
	}

	inline static int32_t get_offset_of_EmptyInts_2() { return static_cast<int32_t>(offsetof(TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields, ___EmptyInts_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_EmptyInts_2() const { return ___EmptyInts_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_EmptyInts_2() { return &___EmptyInts_2; }
	inline void set_EmptyInts_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___EmptyInts_2 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyInts_2), value);
	}

	inline static int32_t get_offset_of_EmptyLongs_3() { return static_cast<int32_t>(offsetof(TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields, ___EmptyLongs_3)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_EmptyLongs_3() const { return ___EmptyLongs_3; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_EmptyLongs_3() { return &___EmptyLongs_3; }
	inline void set_EmptyLongs_3(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___EmptyLongs_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyLongs_3), value);
	}

	inline static int32_t get_offset_of_SSL_CLIENT_4() { return static_cast<int32_t>(offsetof(TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields, ___SSL_CLIENT_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_SSL_CLIENT_4() const { return ___SSL_CLIENT_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_SSL_CLIENT_4() { return &___SSL_CLIENT_4; }
	inline void set_SSL_CLIENT_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___SSL_CLIENT_4 = value;
		Il2CppCodeGenWriteBarrier((&___SSL_CLIENT_4), value);
	}

	inline static int32_t get_offset_of_SSL_SERVER_5() { return static_cast<int32_t>(offsetof(TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields, ___SSL_SERVER_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_SSL_SERVER_5() const { return ___SSL_SERVER_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_SSL_SERVER_5() { return &___SSL_SERVER_5; }
	inline void set_SSL_SERVER_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___SSL_SERVER_5 = value;
		Il2CppCodeGenWriteBarrier((&___SSL_SERVER_5), value);
	}

	inline static int32_t get_offset_of_SSL3_CONST_6() { return static_cast<int32_t>(offsetof(TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields, ___SSL3_CONST_6)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_SSL3_CONST_6() const { return ___SSL3_CONST_6; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_SSL3_CONST_6() { return &___SSL3_CONST_6; }
	inline void set_SSL3_CONST_6(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___SSL3_CONST_6 = value;
		Il2CppCodeGenWriteBarrier((&___SSL3_CONST_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSUTILITIES_TE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_H
#ifndef PACK_T8F8B4E883896536870B1B5DE5151562DB66EE8C1_H
#define PACK_T8F8B4E883896536870B1B5DE5151562DB66EE8C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Utilities.Pack
struct  Pack_t8F8B4E883896536870B1B5DE5151562DB66EE8C1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PACK_T8F8B4E883896536870B1B5DE5151562DB66EE8C1_H
#ifndef BIGINTEGER_T882A86470CD3652F725EAF0F521C5AF9A00F8D91_H
#define BIGINTEGER_T882A86470CD3652F725EAF0F521C5AF9A00F8D91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.BigInteger
struct  BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91  : public RuntimeObject
{
public:
	// System.Int32[] Org.BouncyCastle.Math.BigInteger::magnitude
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___magnitude_21;
	// System.Int32 Org.BouncyCastle.Math.BigInteger::sign
	int32_t ___sign_22;
	// System.Int32 Org.BouncyCastle.Math.BigInteger::nBits
	int32_t ___nBits_23;
	// System.Int32 Org.BouncyCastle.Math.BigInteger::nBitLength
	int32_t ___nBitLength_24;
	// System.Int32 Org.BouncyCastle.Math.BigInteger::mQuote
	int32_t ___mQuote_25;

public:
	inline static int32_t get_offset_of_magnitude_21() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91, ___magnitude_21)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_magnitude_21() const { return ___magnitude_21; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_magnitude_21() { return &___magnitude_21; }
	inline void set_magnitude_21(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___magnitude_21 = value;
		Il2CppCodeGenWriteBarrier((&___magnitude_21), value);
	}

	inline static int32_t get_offset_of_sign_22() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91, ___sign_22)); }
	inline int32_t get_sign_22() const { return ___sign_22; }
	inline int32_t* get_address_of_sign_22() { return &___sign_22; }
	inline void set_sign_22(int32_t value)
	{
		___sign_22 = value;
	}

	inline static int32_t get_offset_of_nBits_23() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91, ___nBits_23)); }
	inline int32_t get_nBits_23() const { return ___nBits_23; }
	inline int32_t* get_address_of_nBits_23() { return &___nBits_23; }
	inline void set_nBits_23(int32_t value)
	{
		___nBits_23 = value;
	}

	inline static int32_t get_offset_of_nBitLength_24() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91, ___nBitLength_24)); }
	inline int32_t get_nBitLength_24() const { return ___nBitLength_24; }
	inline int32_t* get_address_of_nBitLength_24() { return &___nBitLength_24; }
	inline void set_nBitLength_24(int32_t value)
	{
		___nBitLength_24 = value;
	}

	inline static int32_t get_offset_of_mQuote_25() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91, ___mQuote_25)); }
	inline int32_t get_mQuote_25() const { return ___mQuote_25; }
	inline int32_t* get_address_of_mQuote_25() { return &___mQuote_25; }
	inline void set_mQuote_25(int32_t value)
	{
		___mQuote_25 = value;
	}
};

struct BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields
{
public:
	// System.Int32[][] Org.BouncyCastle.Math.BigInteger::primeLists
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___primeLists_0;
	// System.Int32[] Org.BouncyCastle.Math.BigInteger::primeProducts
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___primeProducts_1;
	// System.Int32[] Org.BouncyCastle.Math.BigInteger::ZeroMagnitude
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ZeroMagnitude_2;
	// System.Byte[] Org.BouncyCastle.Math.BigInteger::ZeroEncoding
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___ZeroEncoding_3;
	// Org.BouncyCastle.Math.BigInteger[] Org.BouncyCastle.Math.BigInteger::SMALL_CONSTANTS
	BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* ___SMALL_CONSTANTS_4;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.BigInteger::Zero
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Zero_5;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.BigInteger::One
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___One_6;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.BigInteger::Two
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Two_7;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.BigInteger::Three
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Three_8;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.BigInteger::Ten
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Ten_9;
	// System.Byte[] Org.BouncyCastle.Math.BigInteger::BitLengthTable
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___BitLengthTable_10;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.BigInteger::radix2
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___radix2_11;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.BigInteger::radix2E
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___radix2E_12;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.BigInteger::radix8
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___radix8_13;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.BigInteger::radix8E
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___radix8E_14;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.BigInteger::radix10
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___radix10_15;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.BigInteger::radix10E
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___radix10E_16;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.BigInteger::radix16
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___radix16_17;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.BigInteger::radix16E
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___radix16E_18;
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Math.BigInteger::RandomSource
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___RandomSource_19;
	// System.Int32[] Org.BouncyCastle.Math.BigInteger::ExpWindowThresholds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ExpWindowThresholds_20;

public:
	inline static int32_t get_offset_of_primeLists_0() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___primeLists_0)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_primeLists_0() const { return ___primeLists_0; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_primeLists_0() { return &___primeLists_0; }
	inline void set_primeLists_0(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___primeLists_0 = value;
		Il2CppCodeGenWriteBarrier((&___primeLists_0), value);
	}

	inline static int32_t get_offset_of_primeProducts_1() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___primeProducts_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_primeProducts_1() const { return ___primeProducts_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_primeProducts_1() { return &___primeProducts_1; }
	inline void set_primeProducts_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___primeProducts_1 = value;
		Il2CppCodeGenWriteBarrier((&___primeProducts_1), value);
	}

	inline static int32_t get_offset_of_ZeroMagnitude_2() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___ZeroMagnitude_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ZeroMagnitude_2() const { return ___ZeroMagnitude_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ZeroMagnitude_2() { return &___ZeroMagnitude_2; }
	inline void set_ZeroMagnitude_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ZeroMagnitude_2 = value;
		Il2CppCodeGenWriteBarrier((&___ZeroMagnitude_2), value);
	}

	inline static int32_t get_offset_of_ZeroEncoding_3() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___ZeroEncoding_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_ZeroEncoding_3() const { return ___ZeroEncoding_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_ZeroEncoding_3() { return &___ZeroEncoding_3; }
	inline void set_ZeroEncoding_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___ZeroEncoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___ZeroEncoding_3), value);
	}

	inline static int32_t get_offset_of_SMALL_CONSTANTS_4() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___SMALL_CONSTANTS_4)); }
	inline BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* get_SMALL_CONSTANTS_4() const { return ___SMALL_CONSTANTS_4; }
	inline BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311** get_address_of_SMALL_CONSTANTS_4() { return &___SMALL_CONSTANTS_4; }
	inline void set_SMALL_CONSTANTS_4(BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* value)
	{
		___SMALL_CONSTANTS_4 = value;
		Il2CppCodeGenWriteBarrier((&___SMALL_CONSTANTS_4), value);
	}

	inline static int32_t get_offset_of_Zero_5() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___Zero_5)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Zero_5() const { return ___Zero_5; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Zero_5() { return &___Zero_5; }
	inline void set_Zero_5(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Zero_5 = value;
		Il2CppCodeGenWriteBarrier((&___Zero_5), value);
	}

	inline static int32_t get_offset_of_One_6() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___One_6)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_One_6() const { return ___One_6; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_One_6() { return &___One_6; }
	inline void set_One_6(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___One_6 = value;
		Il2CppCodeGenWriteBarrier((&___One_6), value);
	}

	inline static int32_t get_offset_of_Two_7() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___Two_7)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Two_7() const { return ___Two_7; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Two_7() { return &___Two_7; }
	inline void set_Two_7(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Two_7 = value;
		Il2CppCodeGenWriteBarrier((&___Two_7), value);
	}

	inline static int32_t get_offset_of_Three_8() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___Three_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Three_8() const { return ___Three_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Three_8() { return &___Three_8; }
	inline void set_Three_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Three_8 = value;
		Il2CppCodeGenWriteBarrier((&___Three_8), value);
	}

	inline static int32_t get_offset_of_Ten_9() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___Ten_9)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Ten_9() const { return ___Ten_9; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Ten_9() { return &___Ten_9; }
	inline void set_Ten_9(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Ten_9 = value;
		Il2CppCodeGenWriteBarrier((&___Ten_9), value);
	}

	inline static int32_t get_offset_of_BitLengthTable_10() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___BitLengthTable_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_BitLengthTable_10() const { return ___BitLengthTable_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_BitLengthTable_10() { return &___BitLengthTable_10; }
	inline void set_BitLengthTable_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___BitLengthTable_10 = value;
		Il2CppCodeGenWriteBarrier((&___BitLengthTable_10), value);
	}

	inline static int32_t get_offset_of_radix2_11() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___radix2_11)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_radix2_11() const { return ___radix2_11; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_radix2_11() { return &___radix2_11; }
	inline void set_radix2_11(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___radix2_11 = value;
		Il2CppCodeGenWriteBarrier((&___radix2_11), value);
	}

	inline static int32_t get_offset_of_radix2E_12() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___radix2E_12)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_radix2E_12() const { return ___radix2E_12; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_radix2E_12() { return &___radix2E_12; }
	inline void set_radix2E_12(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___radix2E_12 = value;
		Il2CppCodeGenWriteBarrier((&___radix2E_12), value);
	}

	inline static int32_t get_offset_of_radix8_13() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___radix8_13)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_radix8_13() const { return ___radix8_13; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_radix8_13() { return &___radix8_13; }
	inline void set_radix8_13(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___radix8_13 = value;
		Il2CppCodeGenWriteBarrier((&___radix8_13), value);
	}

	inline static int32_t get_offset_of_radix8E_14() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___radix8E_14)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_radix8E_14() const { return ___radix8E_14; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_radix8E_14() { return &___radix8E_14; }
	inline void set_radix8E_14(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___radix8E_14 = value;
		Il2CppCodeGenWriteBarrier((&___radix8E_14), value);
	}

	inline static int32_t get_offset_of_radix10_15() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___radix10_15)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_radix10_15() const { return ___radix10_15; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_radix10_15() { return &___radix10_15; }
	inline void set_radix10_15(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___radix10_15 = value;
		Il2CppCodeGenWriteBarrier((&___radix10_15), value);
	}

	inline static int32_t get_offset_of_radix10E_16() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___radix10E_16)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_radix10E_16() const { return ___radix10E_16; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_radix10E_16() { return &___radix10E_16; }
	inline void set_radix10E_16(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___radix10E_16 = value;
		Il2CppCodeGenWriteBarrier((&___radix10E_16), value);
	}

	inline static int32_t get_offset_of_radix16_17() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___radix16_17)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_radix16_17() const { return ___radix16_17; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_radix16_17() { return &___radix16_17; }
	inline void set_radix16_17(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___radix16_17 = value;
		Il2CppCodeGenWriteBarrier((&___radix16_17), value);
	}

	inline static int32_t get_offset_of_radix16E_18() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___radix16E_18)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_radix16E_18() const { return ___radix16E_18; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_radix16E_18() { return &___radix16E_18; }
	inline void set_radix16E_18(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___radix16E_18 = value;
		Il2CppCodeGenWriteBarrier((&___radix16E_18), value);
	}

	inline static int32_t get_offset_of_RandomSource_19() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___RandomSource_19)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_RandomSource_19() const { return ___RandomSource_19; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_RandomSource_19() { return &___RandomSource_19; }
	inline void set_RandomSource_19(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___RandomSource_19 = value;
		Il2CppCodeGenWriteBarrier((&___RandomSource_19), value);
	}

	inline static int32_t get_offset_of_ExpWindowThresholds_20() { return static_cast<int32_t>(offsetof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields, ___ExpWindowThresholds_20)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ExpWindowThresholds_20() const { return ___ExpWindowThresholds_20; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ExpWindowThresholds_20() { return &___ExpWindowThresholds_20; }
	inline void set_ExpWindowThresholds_20(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ExpWindowThresholds_20 = value;
		Il2CppCodeGenWriteBarrier((&___ExpWindowThresholds_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGINTEGER_T882A86470CD3652F725EAF0F521C5AF9A00F8D91_H
#ifndef SIMPLEBIGDECIMAL_T5F847D702AAC67554EB95938A6D0B7896F9AE820_H
#define SIMPLEBIGDECIMAL_T5F847D702AAC67554EB95938A6D0B7896F9AE820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Abc.SimpleBigDecimal
struct  SimpleBigDecimal_t5F847D702AAC67554EB95938A6D0B7896F9AE820  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Abc.SimpleBigDecimal::bigInt
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___bigInt_0;
	// System.Int32 Org.BouncyCastle.Math.EC.Abc.SimpleBigDecimal::scale
	int32_t ___scale_1;

public:
	inline static int32_t get_offset_of_bigInt_0() { return static_cast<int32_t>(offsetof(SimpleBigDecimal_t5F847D702AAC67554EB95938A6D0B7896F9AE820, ___bigInt_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_bigInt_0() const { return ___bigInt_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_bigInt_0() { return &___bigInt_0; }
	inline void set_bigInt_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___bigInt_0 = value;
		Il2CppCodeGenWriteBarrier((&___bigInt_0), value);
	}

	inline static int32_t get_offset_of_scale_1() { return static_cast<int32_t>(offsetof(SimpleBigDecimal_t5F847D702AAC67554EB95938A6D0B7896F9AE820, ___scale_1)); }
	inline int32_t get_scale_1() const { return ___scale_1; }
	inline int32_t* get_address_of_scale_1() { return &___scale_1; }
	inline void set_scale_1(int32_t value)
	{
		___scale_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEBIGDECIMAL_T5F847D702AAC67554EB95938A6D0B7896F9AE820_H
#ifndef TNAF_T168C52378E08CDA5C228B34F24B29B80C373A2A2_H
#define TNAF_T168C52378E08CDA5C228B34F24B29B80C373A2A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Abc.Tnaf
struct  Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2  : public RuntimeObject
{
public:

public:
};

struct Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Abc.Tnaf::MinusOne
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___MinusOne_0;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Abc.Tnaf::MinusTwo
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___MinusTwo_1;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Abc.Tnaf::MinusThree
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___MinusThree_2;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Abc.Tnaf::Four
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Four_3;
	// Org.BouncyCastle.Math.EC.Abc.ZTauElement[] Org.BouncyCastle.Math.EC.Abc.Tnaf::Alpha0
	ZTauElementU5BU5D_t0F50BD7EC8CDA6D7979BA0BB4A181311AC67852D* ___Alpha0_4;
	// System.SByte[][] Org.BouncyCastle.Math.EC.Abc.Tnaf::Alpha0Tnaf
	SByteU5BU5DU5BU5D_t99E68B8F036DFF48A8903ACA82EF9B4A4C77E3DF* ___Alpha0Tnaf_5;
	// Org.BouncyCastle.Math.EC.Abc.ZTauElement[] Org.BouncyCastle.Math.EC.Abc.Tnaf::Alpha1
	ZTauElementU5BU5D_t0F50BD7EC8CDA6D7979BA0BB4A181311AC67852D* ___Alpha1_6;
	// System.SByte[][] Org.BouncyCastle.Math.EC.Abc.Tnaf::Alpha1Tnaf
	SByteU5BU5DU5BU5D_t99E68B8F036DFF48A8903ACA82EF9B4A4C77E3DF* ___Alpha1Tnaf_7;

public:
	inline static int32_t get_offset_of_MinusOne_0() { return static_cast<int32_t>(offsetof(Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields, ___MinusOne_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_MinusOne_0() const { return ___MinusOne_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_MinusOne_0() { return &___MinusOne_0; }
	inline void set_MinusOne_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___MinusOne_0 = value;
		Il2CppCodeGenWriteBarrier((&___MinusOne_0), value);
	}

	inline static int32_t get_offset_of_MinusTwo_1() { return static_cast<int32_t>(offsetof(Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields, ___MinusTwo_1)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_MinusTwo_1() const { return ___MinusTwo_1; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_MinusTwo_1() { return &___MinusTwo_1; }
	inline void set_MinusTwo_1(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___MinusTwo_1 = value;
		Il2CppCodeGenWriteBarrier((&___MinusTwo_1), value);
	}

	inline static int32_t get_offset_of_MinusThree_2() { return static_cast<int32_t>(offsetof(Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields, ___MinusThree_2)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_MinusThree_2() const { return ___MinusThree_2; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_MinusThree_2() { return &___MinusThree_2; }
	inline void set_MinusThree_2(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___MinusThree_2 = value;
		Il2CppCodeGenWriteBarrier((&___MinusThree_2), value);
	}

	inline static int32_t get_offset_of_Four_3() { return static_cast<int32_t>(offsetof(Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields, ___Four_3)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Four_3() const { return ___Four_3; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Four_3() { return &___Four_3; }
	inline void set_Four_3(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Four_3 = value;
		Il2CppCodeGenWriteBarrier((&___Four_3), value);
	}

	inline static int32_t get_offset_of_Alpha0_4() { return static_cast<int32_t>(offsetof(Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields, ___Alpha0_4)); }
	inline ZTauElementU5BU5D_t0F50BD7EC8CDA6D7979BA0BB4A181311AC67852D* get_Alpha0_4() const { return ___Alpha0_4; }
	inline ZTauElementU5BU5D_t0F50BD7EC8CDA6D7979BA0BB4A181311AC67852D** get_address_of_Alpha0_4() { return &___Alpha0_4; }
	inline void set_Alpha0_4(ZTauElementU5BU5D_t0F50BD7EC8CDA6D7979BA0BB4A181311AC67852D* value)
	{
		___Alpha0_4 = value;
		Il2CppCodeGenWriteBarrier((&___Alpha0_4), value);
	}

	inline static int32_t get_offset_of_Alpha0Tnaf_5() { return static_cast<int32_t>(offsetof(Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields, ___Alpha0Tnaf_5)); }
	inline SByteU5BU5DU5BU5D_t99E68B8F036DFF48A8903ACA82EF9B4A4C77E3DF* get_Alpha0Tnaf_5() const { return ___Alpha0Tnaf_5; }
	inline SByteU5BU5DU5BU5D_t99E68B8F036DFF48A8903ACA82EF9B4A4C77E3DF** get_address_of_Alpha0Tnaf_5() { return &___Alpha0Tnaf_5; }
	inline void set_Alpha0Tnaf_5(SByteU5BU5DU5BU5D_t99E68B8F036DFF48A8903ACA82EF9B4A4C77E3DF* value)
	{
		___Alpha0Tnaf_5 = value;
		Il2CppCodeGenWriteBarrier((&___Alpha0Tnaf_5), value);
	}

	inline static int32_t get_offset_of_Alpha1_6() { return static_cast<int32_t>(offsetof(Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields, ___Alpha1_6)); }
	inline ZTauElementU5BU5D_t0F50BD7EC8CDA6D7979BA0BB4A181311AC67852D* get_Alpha1_6() const { return ___Alpha1_6; }
	inline ZTauElementU5BU5D_t0F50BD7EC8CDA6D7979BA0BB4A181311AC67852D** get_address_of_Alpha1_6() { return &___Alpha1_6; }
	inline void set_Alpha1_6(ZTauElementU5BU5D_t0F50BD7EC8CDA6D7979BA0BB4A181311AC67852D* value)
	{
		___Alpha1_6 = value;
		Il2CppCodeGenWriteBarrier((&___Alpha1_6), value);
	}

	inline static int32_t get_offset_of_Alpha1Tnaf_7() { return static_cast<int32_t>(offsetof(Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields, ___Alpha1Tnaf_7)); }
	inline SByteU5BU5DU5BU5D_t99E68B8F036DFF48A8903ACA82EF9B4A4C77E3DF* get_Alpha1Tnaf_7() const { return ___Alpha1Tnaf_7; }
	inline SByteU5BU5DU5BU5D_t99E68B8F036DFF48A8903ACA82EF9B4A4C77E3DF** get_address_of_Alpha1Tnaf_7() { return &___Alpha1Tnaf_7; }
	inline void set_Alpha1Tnaf_7(SByteU5BU5DU5BU5D_t99E68B8F036DFF48A8903ACA82EF9B4A4C77E3DF* value)
	{
		___Alpha1Tnaf_7 = value;
		Il2CppCodeGenWriteBarrier((&___Alpha1Tnaf_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TNAF_T168C52378E08CDA5C228B34F24B29B80C373A2A2_H
#ifndef ZTAUELEMENT_TE4CF2031E00A980402C6ADDE27125B185259C5E2_H
#define ZTAUELEMENT_TE4CF2031E00A980402C6ADDE27125B185259C5E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Abc.ZTauElement
struct  ZTauElement_tE4CF2031E00A980402C6ADDE27125B185259C5E2  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Abc.ZTauElement::u
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___u_0;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Abc.ZTauElement::v
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___v_1;

public:
	inline static int32_t get_offset_of_u_0() { return static_cast<int32_t>(offsetof(ZTauElement_tE4CF2031E00A980402C6ADDE27125B185259C5E2, ___u_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_u_0() const { return ___u_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_u_0() { return &___u_0; }
	inline void set_u_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___u_0 = value;
		Il2CppCodeGenWriteBarrier((&___u_0), value);
	}

	inline static int32_t get_offset_of_v_1() { return static_cast<int32_t>(offsetof(ZTauElement_tE4CF2031E00A980402C6ADDE27125B185259C5E2, ___v_1)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_v_1() const { return ___v_1; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_v_1() { return &___v_1; }
	inline void set_v_1(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___v_1 = value;
		Il2CppCodeGenWriteBarrier((&___v_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZTAUELEMENT_TE4CF2031E00A980402C6ADDE27125B185259C5E2_H
#ifndef CURVE25519FIELD_T6CDE8842C1F9F85D0FABBD58E1DEB639AE25BB2D_H
#define CURVE25519FIELD_T6CDE8842C1F9F85D0FABBD58E1DEB639AE25BB2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519Field
struct  Curve25519Field_t6CDE8842C1F9F85D0FABBD58E1DEB639AE25BB2D  : public RuntimeObject
{
public:

public:
};

struct Curve25519Field_t6CDE8842C1F9F85D0FABBD58E1DEB639AE25BB2D_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(Curve25519Field_t6CDE8842C1F9F85D0FABBD58E1DEB639AE25BB2D_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(Curve25519Field_t6CDE8842C1F9F85D0FABBD58E1DEB639AE25BB2D_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE25519FIELD_T6CDE8842C1F9F85D0FABBD58E1DEB639AE25BB2D_H
#ifndef SECP128R1FIELD_T9194B57C204A7DFD50F6D4A8C7C26ACFAE94FD78_H
#define SECP128R1FIELD_T9194B57C204A7DFD50F6D4A8C7C26ACFAE94FD78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Field
struct  SecP128R1Field_t9194B57C204A7DFD50F6D4A8C7C26ACFAE94FD78  : public RuntimeObject
{
public:

public:
};

struct SecP128R1Field_t9194B57C204A7DFD50F6D4A8C7C26ACFAE94FD78_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP128R1Field_t9194B57C204A7DFD50F6D4A8C7C26ACFAE94FD78_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP128R1Field_t9194B57C204A7DFD50F6D4A8C7C26ACFAE94FD78_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP128R1Field_t9194B57C204A7DFD50F6D4A8C7C26ACFAE94FD78_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R1FIELD_T9194B57C204A7DFD50F6D4A8C7C26ACFAE94FD78_H
#ifndef SECP160R1FIELD_T71332E2F61AD4355A4FE9C30B60AA30E59ADD5E6_H
#define SECP160R1FIELD_T71332E2F61AD4355A4FE9C30B60AA30E59ADD5E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Field
struct  SecP160R1Field_t71332E2F61AD4355A4FE9C30B60AA30E59ADD5E6  : public RuntimeObject
{
public:

public:
};

struct SecP160R1Field_t71332E2F61AD4355A4FE9C30B60AA30E59ADD5E6_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP160R1Field_t71332E2F61AD4355A4FE9C30B60AA30E59ADD5E6_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP160R1Field_t71332E2F61AD4355A4FE9C30B60AA30E59ADD5E6_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP160R1Field_t71332E2F61AD4355A4FE9C30B60AA30E59ADD5E6_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R1FIELD_T71332E2F61AD4355A4FE9C30B60AA30E59ADD5E6_H
#ifndef SECP160R2FIELD_T93C679FE3A2AD633DA332D98AA224B68168345A0_H
#define SECP160R2FIELD_T93C679FE3A2AD633DA332D98AA224B68168345A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Field
struct  SecP160R2Field_t93C679FE3A2AD633DA332D98AA224B68168345A0  : public RuntimeObject
{
public:

public:
};

struct SecP160R2Field_t93C679FE3A2AD633DA332D98AA224B68168345A0_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP160R2Field_t93C679FE3A2AD633DA332D98AA224B68168345A0_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP160R2Field_t93C679FE3A2AD633DA332D98AA224B68168345A0_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP160R2Field_t93C679FE3A2AD633DA332D98AA224B68168345A0_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R2FIELD_T93C679FE3A2AD633DA332D98AA224B68168345A0_H
#ifndef SECP192K1FIELD_TE5705643F2E91AD17AD18862A6F74574451B33AD_H
#define SECP192K1FIELD_TE5705643F2E91AD17AD18862A6F74574451B33AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Field
struct  SecP192K1Field_tE5705643F2E91AD17AD18862A6F74574451B33AD  : public RuntimeObject
{
public:

public:
};

struct SecP192K1Field_tE5705643F2E91AD17AD18862A6F74574451B33AD_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP192K1Field_tE5705643F2E91AD17AD18862A6F74574451B33AD_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP192K1Field_tE5705643F2E91AD17AD18862A6F74574451B33AD_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP192K1Field_tE5705643F2E91AD17AD18862A6F74574451B33AD_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192K1FIELD_TE5705643F2E91AD17AD18862A6F74574451B33AD_H
#ifndef SECP192R1FIELD_TA0FFED9B79046F48E1EAA8C38612DDEAC1A8DD4A_H
#define SECP192R1FIELD_TA0FFED9B79046F48E1EAA8C38612DDEAC1A8DD4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Field
struct  SecP192R1Field_tA0FFED9B79046F48E1EAA8C38612DDEAC1A8DD4A  : public RuntimeObject
{
public:

public:
};

struct SecP192R1Field_tA0FFED9B79046F48E1EAA8C38612DDEAC1A8DD4A_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP192R1Field_tA0FFED9B79046F48E1EAA8C38612DDEAC1A8DD4A_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP192R1Field_tA0FFED9B79046F48E1EAA8C38612DDEAC1A8DD4A_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP192R1Field_tA0FFED9B79046F48E1EAA8C38612DDEAC1A8DD4A_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192R1FIELD_TA0FFED9B79046F48E1EAA8C38612DDEAC1A8DD4A_H
#ifndef SECP224K1FIELD_TE2EF028B701BDD8E82D19B9EB6ADB663037507FF_H
#define SECP224K1FIELD_TE2EF028B701BDD8E82D19B9EB6ADB663037507FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Field
struct  SecP224K1Field_tE2EF028B701BDD8E82D19B9EB6ADB663037507FF  : public RuntimeObject
{
public:

public:
};

struct SecP224K1Field_tE2EF028B701BDD8E82D19B9EB6ADB663037507FF_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP224K1Field_tE2EF028B701BDD8E82D19B9EB6ADB663037507FF_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP224K1Field_tE2EF028B701BDD8E82D19B9EB6ADB663037507FF_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP224K1Field_tE2EF028B701BDD8E82D19B9EB6ADB663037507FF_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224K1FIELD_TE2EF028B701BDD8E82D19B9EB6ADB663037507FF_H
#ifndef SECP224R1FIELD_T095BB2376F5A8235BDD20A204A8378F790BB7B14_H
#define SECP224R1FIELD_T095BB2376F5A8235BDD20A204A8378F790BB7B14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Field
struct  SecP224R1Field_t095BB2376F5A8235BDD20A204A8378F790BB7B14  : public RuntimeObject
{
public:

public:
};

struct SecP224R1Field_t095BB2376F5A8235BDD20A204A8378F790BB7B14_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Field::P
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___P_0;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Field::PExt
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExt_1;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Field::PExtInv
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PExtInv_2;

public:
	inline static int32_t get_offset_of_P_0() { return static_cast<int32_t>(offsetof(SecP224R1Field_t095BB2376F5A8235BDD20A204A8378F790BB7B14_StaticFields, ___P_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_P_0() const { return ___P_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_P_0() { return &___P_0; }
	inline void set_P_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___P_0 = value;
		Il2CppCodeGenWriteBarrier((&___P_0), value);
	}

	inline static int32_t get_offset_of_PExt_1() { return static_cast<int32_t>(offsetof(SecP224R1Field_t095BB2376F5A8235BDD20A204A8378F790BB7B14_StaticFields, ___PExt_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExt_1() const { return ___PExt_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExt_1() { return &___PExt_1; }
	inline void set_PExt_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExt_1 = value;
		Il2CppCodeGenWriteBarrier((&___PExt_1), value);
	}

	inline static int32_t get_offset_of_PExtInv_2() { return static_cast<int32_t>(offsetof(SecP224R1Field_t095BB2376F5A8235BDD20A204A8378F790BB7B14_StaticFields, ___PExtInv_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PExtInv_2() const { return ___PExtInv_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PExtInv_2() { return &___PExtInv_2; }
	inline void set_PExtInv_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PExtInv_2 = value;
		Il2CppCodeGenWriteBarrier((&___PExtInv_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224R1FIELD_T095BB2376F5A8235BDD20A204A8378F790BB7B14_H
#ifndef ECALGORITHMS_TFA1D868A76B0FD79B886A99696087DFED87FE94C_H
#define ECALGORITHMS_TFA1D868A76B0FD79B886A99696087DFED87FE94C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.ECAlgorithms
struct  ECAlgorithms_tFA1D868A76B0FD79B886A99696087DFED87FE94C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECALGORITHMS_TFA1D868A76B0FD79B886A99696087DFED87FE94C_H
#ifndef ECCURVE_T08AD8E722914B0B03C49C2D27B79B1D4E4770D95_H
#define ECCURVE_T08AD8E722914B0B03C49C2D27B79B1D4E4770D95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.ECCurve
struct  ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.Field.IFiniteField Org.BouncyCastle.Math.EC.ECCurve::m_field
	RuntimeObject* ___m_field_0;
	// Org.BouncyCastle.Math.EC.ECFieldElement Org.BouncyCastle.Math.EC.ECCurve::m_a
	ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * ___m_a_1;
	// Org.BouncyCastle.Math.EC.ECFieldElement Org.BouncyCastle.Math.EC.ECCurve::m_b
	ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * ___m_b_2;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.ECCurve::m_order
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___m_order_3;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.ECCurve::m_cofactor
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___m_cofactor_4;
	// System.Int32 Org.BouncyCastle.Math.EC.ECCurve::m_coord
	int32_t ___m_coord_5;
	// Org.BouncyCastle.Math.EC.Endo.ECEndomorphism Org.BouncyCastle.Math.EC.ECCurve::m_endomorphism
	RuntimeObject* ___m_endomorphism_6;
	// Org.BouncyCastle.Math.EC.Multiplier.ECMultiplier Org.BouncyCastle.Math.EC.ECCurve::m_multiplier
	RuntimeObject* ___m_multiplier_7;

public:
	inline static int32_t get_offset_of_m_field_0() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_field_0)); }
	inline RuntimeObject* get_m_field_0() const { return ___m_field_0; }
	inline RuntimeObject** get_address_of_m_field_0() { return &___m_field_0; }
	inline void set_m_field_0(RuntimeObject* value)
	{
		___m_field_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_field_0), value);
	}

	inline static int32_t get_offset_of_m_a_1() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_a_1)); }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * get_m_a_1() const { return ___m_a_1; }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C ** get_address_of_m_a_1() { return &___m_a_1; }
	inline void set_m_a_1(ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * value)
	{
		___m_a_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_a_1), value);
	}

	inline static int32_t get_offset_of_m_b_2() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_b_2)); }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * get_m_b_2() const { return ___m_b_2; }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C ** get_address_of_m_b_2() { return &___m_b_2; }
	inline void set_m_b_2(ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * value)
	{
		___m_b_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_b_2), value);
	}

	inline static int32_t get_offset_of_m_order_3() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_order_3)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_m_order_3() const { return ___m_order_3; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_m_order_3() { return &___m_order_3; }
	inline void set_m_order_3(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___m_order_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_order_3), value);
	}

	inline static int32_t get_offset_of_m_cofactor_4() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_cofactor_4)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_m_cofactor_4() const { return ___m_cofactor_4; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_m_cofactor_4() { return &___m_cofactor_4; }
	inline void set_m_cofactor_4(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___m_cofactor_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_cofactor_4), value);
	}

	inline static int32_t get_offset_of_m_coord_5() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_coord_5)); }
	inline int32_t get_m_coord_5() const { return ___m_coord_5; }
	inline int32_t* get_address_of_m_coord_5() { return &___m_coord_5; }
	inline void set_m_coord_5(int32_t value)
	{
		___m_coord_5 = value;
	}

	inline static int32_t get_offset_of_m_endomorphism_6() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_endomorphism_6)); }
	inline RuntimeObject* get_m_endomorphism_6() const { return ___m_endomorphism_6; }
	inline RuntimeObject** get_address_of_m_endomorphism_6() { return &___m_endomorphism_6; }
	inline void set_m_endomorphism_6(RuntimeObject* value)
	{
		___m_endomorphism_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_endomorphism_6), value);
	}

	inline static int32_t get_offset_of_m_multiplier_7() { return static_cast<int32_t>(offsetof(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95, ___m_multiplier_7)); }
	inline RuntimeObject* get_m_multiplier_7() const { return ___m_multiplier_7; }
	inline RuntimeObject** get_address_of_m_multiplier_7() { return &___m_multiplier_7; }
	inline void set_m_multiplier_7(RuntimeObject* value)
	{
		___m_multiplier_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_multiplier_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECCURVE_T08AD8E722914B0B03C49C2D27B79B1D4E4770D95_H
#ifndef CONFIG_TB8033A9496C70BD7A0CBA6D41F8FAC15216135A5_H
#define CONFIG_TB8033A9496C70BD7A0CBA6D41F8FAC15216135A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.ECCurve_Config
struct  Config_tB8033A9496C70BD7A0CBA6D41F8FAC15216135A5  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.EC.ECCurve Org.BouncyCastle.Math.EC.ECCurve_Config::outer
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * ___outer_0;
	// System.Int32 Org.BouncyCastle.Math.EC.ECCurve_Config::coord
	int32_t ___coord_1;
	// Org.BouncyCastle.Math.EC.Endo.ECEndomorphism Org.BouncyCastle.Math.EC.ECCurve_Config::endomorphism
	RuntimeObject* ___endomorphism_2;
	// Org.BouncyCastle.Math.EC.Multiplier.ECMultiplier Org.BouncyCastle.Math.EC.ECCurve_Config::multiplier
	RuntimeObject* ___multiplier_3;

public:
	inline static int32_t get_offset_of_outer_0() { return static_cast<int32_t>(offsetof(Config_tB8033A9496C70BD7A0CBA6D41F8FAC15216135A5, ___outer_0)); }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * get_outer_0() const { return ___outer_0; }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 ** get_address_of_outer_0() { return &___outer_0; }
	inline void set_outer_0(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * value)
	{
		___outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___outer_0), value);
	}

	inline static int32_t get_offset_of_coord_1() { return static_cast<int32_t>(offsetof(Config_tB8033A9496C70BD7A0CBA6D41F8FAC15216135A5, ___coord_1)); }
	inline int32_t get_coord_1() const { return ___coord_1; }
	inline int32_t* get_address_of_coord_1() { return &___coord_1; }
	inline void set_coord_1(int32_t value)
	{
		___coord_1 = value;
	}

	inline static int32_t get_offset_of_endomorphism_2() { return static_cast<int32_t>(offsetof(Config_tB8033A9496C70BD7A0CBA6D41F8FAC15216135A5, ___endomorphism_2)); }
	inline RuntimeObject* get_endomorphism_2() const { return ___endomorphism_2; }
	inline RuntimeObject** get_address_of_endomorphism_2() { return &___endomorphism_2; }
	inline void set_endomorphism_2(RuntimeObject* value)
	{
		___endomorphism_2 = value;
		Il2CppCodeGenWriteBarrier((&___endomorphism_2), value);
	}

	inline static int32_t get_offset_of_multiplier_3() { return static_cast<int32_t>(offsetof(Config_tB8033A9496C70BD7A0CBA6D41F8FAC15216135A5, ___multiplier_3)); }
	inline RuntimeObject* get_multiplier_3() const { return ___multiplier_3; }
	inline RuntimeObject** get_address_of_multiplier_3() { return &___multiplier_3; }
	inline void set_multiplier_3(RuntimeObject* value)
	{
		___multiplier_3 = value;
		Il2CppCodeGenWriteBarrier((&___multiplier_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIG_TB8033A9496C70BD7A0CBA6D41F8FAC15216135A5_H
#ifndef ECFIELDELEMENT_TE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C_H
#define ECFIELDELEMENT_TE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.ECFieldElement
struct  ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECFIELDELEMENT_TE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C_H
#ifndef ECPOINT_TCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399_H
#define ECPOINT_TCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.ECPoint
struct  ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.EC.ECCurve Org.BouncyCastle.Math.EC.ECPoint::m_curve
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * ___m_curve_1;
	// Org.BouncyCastle.Math.EC.ECFieldElement Org.BouncyCastle.Math.EC.ECPoint::m_x
	ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * ___m_x_2;
	// Org.BouncyCastle.Math.EC.ECFieldElement Org.BouncyCastle.Math.EC.ECPoint::m_y
	ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * ___m_y_3;
	// Org.BouncyCastle.Math.EC.ECFieldElement[] Org.BouncyCastle.Math.EC.ECPoint::m_zs
	ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609* ___m_zs_4;
	// System.Boolean Org.BouncyCastle.Math.EC.ECPoint::m_withCompression
	bool ___m_withCompression_5;
	// System.Collections.IDictionary Org.BouncyCastle.Math.EC.ECPoint::m_preCompTable
	RuntimeObject* ___m_preCompTable_6;

public:
	inline static int32_t get_offset_of_m_curve_1() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399, ___m_curve_1)); }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * get_m_curve_1() const { return ___m_curve_1; }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 ** get_address_of_m_curve_1() { return &___m_curve_1; }
	inline void set_m_curve_1(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * value)
	{
		___m_curve_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_curve_1), value);
	}

	inline static int32_t get_offset_of_m_x_2() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399, ___m_x_2)); }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * get_m_x_2() const { return ___m_x_2; }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C ** get_address_of_m_x_2() { return &___m_x_2; }
	inline void set_m_x_2(ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * value)
	{
		___m_x_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_x_2), value);
	}

	inline static int32_t get_offset_of_m_y_3() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399, ___m_y_3)); }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * get_m_y_3() const { return ___m_y_3; }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C ** get_address_of_m_y_3() { return &___m_y_3; }
	inline void set_m_y_3(ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * value)
	{
		___m_y_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_y_3), value);
	}

	inline static int32_t get_offset_of_m_zs_4() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399, ___m_zs_4)); }
	inline ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609* get_m_zs_4() const { return ___m_zs_4; }
	inline ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609** get_address_of_m_zs_4() { return &___m_zs_4; }
	inline void set_m_zs_4(ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609* value)
	{
		___m_zs_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_zs_4), value);
	}

	inline static int32_t get_offset_of_m_withCompression_5() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399, ___m_withCompression_5)); }
	inline bool get_m_withCompression_5() const { return ___m_withCompression_5; }
	inline bool* get_address_of_m_withCompression_5() { return &___m_withCompression_5; }
	inline void set_m_withCompression_5(bool value)
	{
		___m_withCompression_5 = value;
	}

	inline static int32_t get_offset_of_m_preCompTable_6() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399, ___m_preCompTable_6)); }
	inline RuntimeObject* get_m_preCompTable_6() const { return ___m_preCompTable_6; }
	inline RuntimeObject** get_address_of_m_preCompTable_6() { return &___m_preCompTable_6; }
	inline void set_m_preCompTable_6(RuntimeObject* value)
	{
		___m_preCompTable_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_preCompTable_6), value);
	}
};

struct ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399_StaticFields
{
public:
	// Org.BouncyCastle.Math.EC.ECFieldElement[] Org.BouncyCastle.Math.EC.ECPoint::EMPTY_ZS
	ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609* ___EMPTY_ZS_0;

public:
	inline static int32_t get_offset_of_EMPTY_ZS_0() { return static_cast<int32_t>(offsetof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399_StaticFields, ___EMPTY_ZS_0)); }
	inline ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609* get_EMPTY_ZS_0() const { return ___EMPTY_ZS_0; }
	inline ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609** get_address_of_EMPTY_ZS_0() { return &___EMPTY_ZS_0; }
	inline void set_EMPTY_ZS_0(ECFieldElementU5BU5D_tD63CAB58E0E42F9436F12569071F0663360A7609* value)
	{
		___EMPTY_ZS_0 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_ZS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPOINT_TCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399_H
#ifndef LONGARRAY_T360A75AF402B702E603C322FABD46E76E1D20C96_H
#define LONGARRAY_T360A75AF402B702E603C322FABD46E76E1D20C96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.LongArray
struct  LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96  : public RuntimeObject
{
public:
	// System.Int64[] Org.BouncyCastle.Math.EC.LongArray::m_ints
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___m_ints_6;

public:
	inline static int32_t get_offset_of_m_ints_6() { return static_cast<int32_t>(offsetof(LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96, ___m_ints_6)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_m_ints_6() const { return ___m_ints_6; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_m_ints_6() { return &___m_ints_6; }
	inline void set_m_ints_6(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___m_ints_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ints_6), value);
	}
};

struct LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields
{
public:
	// System.UInt16[] Org.BouncyCastle.Math.EC.LongArray::INTERLEAVE2_TABLE
	UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E* ___INTERLEAVE2_TABLE_0;
	// System.Int32[] Org.BouncyCastle.Math.EC.LongArray::INTERLEAVE3_TABLE
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___INTERLEAVE3_TABLE_1;
	// System.Int32[] Org.BouncyCastle.Math.EC.LongArray::INTERLEAVE4_TABLE
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___INTERLEAVE4_TABLE_2;
	// System.Int32[] Org.BouncyCastle.Math.EC.LongArray::INTERLEAVE5_TABLE
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___INTERLEAVE5_TABLE_3;
	// System.Int64[] Org.BouncyCastle.Math.EC.LongArray::INTERLEAVE7_TABLE
	Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* ___INTERLEAVE7_TABLE_4;
	// System.Byte[] Org.BouncyCastle.Math.EC.LongArray::BitLengths
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___BitLengths_5;

public:
	inline static int32_t get_offset_of_INTERLEAVE2_TABLE_0() { return static_cast<int32_t>(offsetof(LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields, ___INTERLEAVE2_TABLE_0)); }
	inline UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E* get_INTERLEAVE2_TABLE_0() const { return ___INTERLEAVE2_TABLE_0; }
	inline UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E** get_address_of_INTERLEAVE2_TABLE_0() { return &___INTERLEAVE2_TABLE_0; }
	inline void set_INTERLEAVE2_TABLE_0(UInt16U5BU5D_t2D4BB1F8C486FF4359FFA7E4A76A8708A684543E* value)
	{
		___INTERLEAVE2_TABLE_0 = value;
		Il2CppCodeGenWriteBarrier((&___INTERLEAVE2_TABLE_0), value);
	}

	inline static int32_t get_offset_of_INTERLEAVE3_TABLE_1() { return static_cast<int32_t>(offsetof(LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields, ___INTERLEAVE3_TABLE_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_INTERLEAVE3_TABLE_1() const { return ___INTERLEAVE3_TABLE_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_INTERLEAVE3_TABLE_1() { return &___INTERLEAVE3_TABLE_1; }
	inline void set_INTERLEAVE3_TABLE_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___INTERLEAVE3_TABLE_1 = value;
		Il2CppCodeGenWriteBarrier((&___INTERLEAVE3_TABLE_1), value);
	}

	inline static int32_t get_offset_of_INTERLEAVE4_TABLE_2() { return static_cast<int32_t>(offsetof(LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields, ___INTERLEAVE4_TABLE_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_INTERLEAVE4_TABLE_2() const { return ___INTERLEAVE4_TABLE_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_INTERLEAVE4_TABLE_2() { return &___INTERLEAVE4_TABLE_2; }
	inline void set_INTERLEAVE4_TABLE_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___INTERLEAVE4_TABLE_2 = value;
		Il2CppCodeGenWriteBarrier((&___INTERLEAVE4_TABLE_2), value);
	}

	inline static int32_t get_offset_of_INTERLEAVE5_TABLE_3() { return static_cast<int32_t>(offsetof(LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields, ___INTERLEAVE5_TABLE_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_INTERLEAVE5_TABLE_3() const { return ___INTERLEAVE5_TABLE_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_INTERLEAVE5_TABLE_3() { return &___INTERLEAVE5_TABLE_3; }
	inline void set_INTERLEAVE5_TABLE_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___INTERLEAVE5_TABLE_3 = value;
		Il2CppCodeGenWriteBarrier((&___INTERLEAVE5_TABLE_3), value);
	}

	inline static int32_t get_offset_of_INTERLEAVE7_TABLE_4() { return static_cast<int32_t>(offsetof(LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields, ___INTERLEAVE7_TABLE_4)); }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* get_INTERLEAVE7_TABLE_4() const { return ___INTERLEAVE7_TABLE_4; }
	inline Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F** get_address_of_INTERLEAVE7_TABLE_4() { return &___INTERLEAVE7_TABLE_4; }
	inline void set_INTERLEAVE7_TABLE_4(Int64U5BU5D_tE04A3DEF6AF1C852A43B98A24EFB715806B37F5F* value)
	{
		___INTERLEAVE7_TABLE_4 = value;
		Il2CppCodeGenWriteBarrier((&___INTERLEAVE7_TABLE_4), value);
	}

	inline static int32_t get_offset_of_BitLengths_5() { return static_cast<int32_t>(offsetof(LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields, ___BitLengths_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_BitLengths_5() const { return ___BitLengths_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_BitLengths_5() { return &___BitLengths_5; }
	inline void set_BitLengths_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___BitLengths_5 = value;
		Il2CppCodeGenWriteBarrier((&___BitLengths_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGARRAY_T360A75AF402B702E603C322FABD46E76E1D20C96_H
#ifndef SCALEXPOINTMAP_TA757D388174A99CA7F87D2E553FBBF056B053EE9_H
#define SCALEXPOINTMAP_TA757D388174A99CA7F87D2E553FBBF056B053EE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.ScaleXPointMap
struct  ScaleXPointMap_tA757D388174A99CA7F87D2E553FBBF056B053EE9  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.EC.ECFieldElement Org.BouncyCastle.Math.EC.ScaleXPointMap::scale
	ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * ___scale_0;

public:
	inline static int32_t get_offset_of_scale_0() { return static_cast<int32_t>(offsetof(ScaleXPointMap_tA757D388174A99CA7F87D2E553FBBF056B053EE9, ___scale_0)); }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * get_scale_0() const { return ___scale_0; }
	inline ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C ** get_address_of_scale_0() { return &___scale_0; }
	inline void set_scale_0(ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C * value)
	{
		___scale_0 = value;
		Il2CppCodeGenWriteBarrier((&___scale_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEXPOINTMAP_TA757D388174A99CA7F87D2E553FBBF056B053EE9_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef TLSCLIENTCONTEXTIMPL_T24CB6216F1148B76E46DB71DF497691FC2179BD4_H
#define TLSCLIENTCONTEXTIMPL_T24CB6216F1148B76E46DB71DF497691FC2179BD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsClientContextImpl
struct  TlsClientContextImpl_t24CB6216F1148B76E46DB71DF497691FC2179BD4  : public AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTCONTEXTIMPL_T24CB6216F1148B76E46DB71DF497691FC2179BD4_H
#ifndef TLSDHKEYEXCHANGE_T6DB9948CBA1DAEF0F7687207352642DE11F64D1D_H
#define TLSDHKEYEXCHANGE_T6DB9948CBA1DAEF0F7687207352642DE11F64D1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange
struct  TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D  : public AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsSigner Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange::mTlsSigner
	RuntimeObject* ___mTlsSigner_3;
	// Org.BouncyCastle.Crypto.Parameters.DHParameters Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange::mDHParameters
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * ___mDHParameters_4;
	// Org.BouncyCastle.Crypto.AsymmetricKeyParameter Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange::mServerPublicKey
	AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * ___mServerPublicKey_5;
	// Org.BouncyCastle.Crypto.Tls.TlsAgreementCredentials Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange::mAgreementCredentials
	RuntimeObject* ___mAgreementCredentials_6;
	// Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange::mDHAgreePrivateKey
	DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C * ___mDHAgreePrivateKey_7;
	// Org.BouncyCastle.Crypto.Parameters.DHPublicKeyParameters Org.BouncyCastle.Crypto.Tls.TlsDHKeyExchange::mDHAgreePublicKey
	DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA * ___mDHAgreePublicKey_8;

public:
	inline static int32_t get_offset_of_mTlsSigner_3() { return static_cast<int32_t>(offsetof(TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D, ___mTlsSigner_3)); }
	inline RuntimeObject* get_mTlsSigner_3() const { return ___mTlsSigner_3; }
	inline RuntimeObject** get_address_of_mTlsSigner_3() { return &___mTlsSigner_3; }
	inline void set_mTlsSigner_3(RuntimeObject* value)
	{
		___mTlsSigner_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsSigner_3), value);
	}

	inline static int32_t get_offset_of_mDHParameters_4() { return static_cast<int32_t>(offsetof(TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D, ___mDHParameters_4)); }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * get_mDHParameters_4() const { return ___mDHParameters_4; }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 ** get_address_of_mDHParameters_4() { return &___mDHParameters_4; }
	inline void set_mDHParameters_4(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * value)
	{
		___mDHParameters_4 = value;
		Il2CppCodeGenWriteBarrier((&___mDHParameters_4), value);
	}

	inline static int32_t get_offset_of_mServerPublicKey_5() { return static_cast<int32_t>(offsetof(TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D, ___mServerPublicKey_5)); }
	inline AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * get_mServerPublicKey_5() const { return ___mServerPublicKey_5; }
	inline AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 ** get_address_of_mServerPublicKey_5() { return &___mServerPublicKey_5; }
	inline void set_mServerPublicKey_5(AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * value)
	{
		___mServerPublicKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___mServerPublicKey_5), value);
	}

	inline static int32_t get_offset_of_mAgreementCredentials_6() { return static_cast<int32_t>(offsetof(TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D, ___mAgreementCredentials_6)); }
	inline RuntimeObject* get_mAgreementCredentials_6() const { return ___mAgreementCredentials_6; }
	inline RuntimeObject** get_address_of_mAgreementCredentials_6() { return &___mAgreementCredentials_6; }
	inline void set_mAgreementCredentials_6(RuntimeObject* value)
	{
		___mAgreementCredentials_6 = value;
		Il2CppCodeGenWriteBarrier((&___mAgreementCredentials_6), value);
	}

	inline static int32_t get_offset_of_mDHAgreePrivateKey_7() { return static_cast<int32_t>(offsetof(TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D, ___mDHAgreePrivateKey_7)); }
	inline DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C * get_mDHAgreePrivateKey_7() const { return ___mDHAgreePrivateKey_7; }
	inline DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C ** get_address_of_mDHAgreePrivateKey_7() { return &___mDHAgreePrivateKey_7; }
	inline void set_mDHAgreePrivateKey_7(DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C * value)
	{
		___mDHAgreePrivateKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___mDHAgreePrivateKey_7), value);
	}

	inline static int32_t get_offset_of_mDHAgreePublicKey_8() { return static_cast<int32_t>(offsetof(TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D, ___mDHAgreePublicKey_8)); }
	inline DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA * get_mDHAgreePublicKey_8() const { return ___mDHAgreePublicKey_8; }
	inline DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA ** get_address_of_mDHAgreePublicKey_8() { return &___mDHAgreePublicKey_8; }
	inline void set_mDHAgreePublicKey_8(DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA * value)
	{
		___mDHAgreePublicKey_8 = value;
		Il2CppCodeGenWriteBarrier((&___mDHAgreePublicKey_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSDHKEYEXCHANGE_T6DB9948CBA1DAEF0F7687207352642DE11F64D1D_H
#ifndef TLSDSASIGNER_T6365A671792FD78F0CE5212C8B4CB48E29DF8B70_H
#define TLSDSASIGNER_T6365A671792FD78F0CE5212C8B4CB48E29DF8B70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsDsaSigner
struct  TlsDsaSigner_t6365A671792FD78F0CE5212C8B4CB48E29DF8B70  : public AbstractTlsSigner_t69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSDSASIGNER_T6365A671792FD78F0CE5212C8B4CB48E29DF8B70_H
#ifndef TLSECDHKEYEXCHANGE_T64475B160C7265859959F209765AADDAD9BE3E05_H
#define TLSECDHKEYEXCHANGE_T64475B160C7265859959F209765AADDAD9BE3E05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange
struct  TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05  : public AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsSigner Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mTlsSigner
	RuntimeObject* ___mTlsSigner_3;
	// System.Int32[] Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mNamedCurves
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mNamedCurves_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mClientECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mClientECPointFormats_5;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mServerECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mServerECPointFormats_6;
	// Org.BouncyCastle.Crypto.AsymmetricKeyParameter Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mServerPublicKey
	AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * ___mServerPublicKey_7;
	// Org.BouncyCastle.Crypto.Tls.TlsAgreementCredentials Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mAgreementCredentials
	RuntimeObject* ___mAgreementCredentials_8;
	// Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mECAgreePrivateKey
	ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F * ___mECAgreePrivateKey_9;
	// Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters Org.BouncyCastle.Crypto.Tls.TlsECDHKeyExchange::mECAgreePublicKey
	ECPublicKeyParameters_t84FD118FD3407D284EF46E8358AE185634537702 * ___mECAgreePublicKey_10;

public:
	inline static int32_t get_offset_of_mTlsSigner_3() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05, ___mTlsSigner_3)); }
	inline RuntimeObject* get_mTlsSigner_3() const { return ___mTlsSigner_3; }
	inline RuntimeObject** get_address_of_mTlsSigner_3() { return &___mTlsSigner_3; }
	inline void set_mTlsSigner_3(RuntimeObject* value)
	{
		___mTlsSigner_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsSigner_3), value);
	}

	inline static int32_t get_offset_of_mNamedCurves_4() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05, ___mNamedCurves_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mNamedCurves_4() const { return ___mNamedCurves_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mNamedCurves_4() { return &___mNamedCurves_4; }
	inline void set_mNamedCurves_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mNamedCurves_4 = value;
		Il2CppCodeGenWriteBarrier((&___mNamedCurves_4), value);
	}

	inline static int32_t get_offset_of_mClientECPointFormats_5() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05, ___mClientECPointFormats_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mClientECPointFormats_5() const { return ___mClientECPointFormats_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mClientECPointFormats_5() { return &___mClientECPointFormats_5; }
	inline void set_mClientECPointFormats_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mClientECPointFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___mClientECPointFormats_5), value);
	}

	inline static int32_t get_offset_of_mServerECPointFormats_6() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05, ___mServerECPointFormats_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mServerECPointFormats_6() const { return ___mServerECPointFormats_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mServerECPointFormats_6() { return &___mServerECPointFormats_6; }
	inline void set_mServerECPointFormats_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mServerECPointFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___mServerECPointFormats_6), value);
	}

	inline static int32_t get_offset_of_mServerPublicKey_7() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05, ___mServerPublicKey_7)); }
	inline AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * get_mServerPublicKey_7() const { return ___mServerPublicKey_7; }
	inline AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 ** get_address_of_mServerPublicKey_7() { return &___mServerPublicKey_7; }
	inline void set_mServerPublicKey_7(AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * value)
	{
		___mServerPublicKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___mServerPublicKey_7), value);
	}

	inline static int32_t get_offset_of_mAgreementCredentials_8() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05, ___mAgreementCredentials_8)); }
	inline RuntimeObject* get_mAgreementCredentials_8() const { return ___mAgreementCredentials_8; }
	inline RuntimeObject** get_address_of_mAgreementCredentials_8() { return &___mAgreementCredentials_8; }
	inline void set_mAgreementCredentials_8(RuntimeObject* value)
	{
		___mAgreementCredentials_8 = value;
		Il2CppCodeGenWriteBarrier((&___mAgreementCredentials_8), value);
	}

	inline static int32_t get_offset_of_mECAgreePrivateKey_9() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05, ___mECAgreePrivateKey_9)); }
	inline ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F * get_mECAgreePrivateKey_9() const { return ___mECAgreePrivateKey_9; }
	inline ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F ** get_address_of_mECAgreePrivateKey_9() { return &___mECAgreePrivateKey_9; }
	inline void set_mECAgreePrivateKey_9(ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F * value)
	{
		___mECAgreePrivateKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___mECAgreePrivateKey_9), value);
	}

	inline static int32_t get_offset_of_mECAgreePublicKey_10() { return static_cast<int32_t>(offsetof(TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05, ___mECAgreePublicKey_10)); }
	inline ECPublicKeyParameters_t84FD118FD3407D284EF46E8358AE185634537702 * get_mECAgreePublicKey_10() const { return ___mECAgreePublicKey_10; }
	inline ECPublicKeyParameters_t84FD118FD3407D284EF46E8358AE185634537702 ** get_address_of_mECAgreePublicKey_10() { return &___mECAgreePublicKey_10; }
	inline void set_mECAgreePublicKey_10(ECPublicKeyParameters_t84FD118FD3407D284EF46E8358AE185634537702 * value)
	{
		___mECAgreePublicKey_10 = value;
		Il2CppCodeGenWriteBarrier((&___mECAgreePublicKey_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSECDHKEYEXCHANGE_T64475B160C7265859959F209765AADDAD9BE3E05_H
#ifndef TLSRSAKEYEXCHANGE_T40BDD2D90BAD3A806C0EBF311AAD0FC12B1B67FA_H
#define TLSRSAKEYEXCHANGE_T40BDD2D90BAD3A806C0EBF311AAD0FC12B1B67FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsRsaKeyExchange
struct  TlsRsaKeyExchange_t40BDD2D90BAD3A806C0EBF311AAD0FC12B1B67FA  : public AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35
{
public:
	// Org.BouncyCastle.Crypto.AsymmetricKeyParameter Org.BouncyCastle.Crypto.Tls.TlsRsaKeyExchange::mServerPublicKey
	AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * ___mServerPublicKey_3;
	// Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters Org.BouncyCastle.Crypto.Tls.TlsRsaKeyExchange::mRsaServerPublicKey
	RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308 * ___mRsaServerPublicKey_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsRsaKeyExchange::mPremasterSecret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPremasterSecret_5;

public:
	inline static int32_t get_offset_of_mServerPublicKey_3() { return static_cast<int32_t>(offsetof(TlsRsaKeyExchange_t40BDD2D90BAD3A806C0EBF311AAD0FC12B1B67FA, ___mServerPublicKey_3)); }
	inline AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * get_mServerPublicKey_3() const { return ___mServerPublicKey_3; }
	inline AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 ** get_address_of_mServerPublicKey_3() { return &___mServerPublicKey_3; }
	inline void set_mServerPublicKey_3(AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7 * value)
	{
		___mServerPublicKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___mServerPublicKey_3), value);
	}

	inline static int32_t get_offset_of_mRsaServerPublicKey_4() { return static_cast<int32_t>(offsetof(TlsRsaKeyExchange_t40BDD2D90BAD3A806C0EBF311AAD0FC12B1B67FA, ___mRsaServerPublicKey_4)); }
	inline RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308 * get_mRsaServerPublicKey_4() const { return ___mRsaServerPublicKey_4; }
	inline RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308 ** get_address_of_mRsaServerPublicKey_4() { return &___mRsaServerPublicKey_4; }
	inline void set_mRsaServerPublicKey_4(RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308 * value)
	{
		___mRsaServerPublicKey_4 = value;
		Il2CppCodeGenWriteBarrier((&___mRsaServerPublicKey_4), value);
	}

	inline static int32_t get_offset_of_mPremasterSecret_5() { return static_cast<int32_t>(offsetof(TlsRsaKeyExchange_t40BDD2D90BAD3A806C0EBF311AAD0FC12B1B67FA, ___mPremasterSecret_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPremasterSecret_5() const { return ___mPremasterSecret_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPremasterSecret_5() { return &___mPremasterSecret_5; }
	inline void set_mPremasterSecret_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPremasterSecret_5 = value;
		Il2CppCodeGenWriteBarrier((&___mPremasterSecret_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSRSAKEYEXCHANGE_T40BDD2D90BAD3A806C0EBF311AAD0FC12B1B67FA_H
#ifndef TLSRSASIGNER_T1DDB99DFB8BCDBDDCEFDB4107CA224C991D5B798_H
#define TLSRSASIGNER_T1DDB99DFB8BCDBDDCEFDB4107CA224C991D5B798_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsRsaSigner
struct  TlsRsaSigner_t1DDB99DFB8BCDBDDCEFDB4107CA224C991D5B798  : public AbstractTlsSigner_t69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSRSASIGNER_T1DDB99DFB8BCDBDDCEFDB4107CA224C991D5B798_H
#ifndef ABSTRACTF2MCURVE_T4AA98C03B9F426713CE47E921AA07B9623F34391_H
#define ABSTRACTF2MCURVE_T4AA98C03B9F426713CE47E921AA07B9623F34391_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.AbstractF2mCurve
struct  AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391  : public ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95
{
public:
	// Org.BouncyCastle.Math.BigInteger[] Org.BouncyCastle.Math.EC.AbstractF2mCurve::si
	BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* ___si_8;

public:
	inline static int32_t get_offset_of_si_8() { return static_cast<int32_t>(offsetof(AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391, ___si_8)); }
	inline BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* get_si_8() const { return ___si_8; }
	inline BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311** get_address_of_si_8() { return &___si_8; }
	inline void set_si_8(BigIntegerU5BU5D_tE1C878446A6F27F09B57A1E5ECEBEF841F575311* value)
	{
		___si_8 = value;
		Il2CppCodeGenWriteBarrier((&___si_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTF2MCURVE_T4AA98C03B9F426713CE47E921AA07B9623F34391_H
#ifndef ABSTRACTFPCURVE_TC851390204D76E2F3E8CBDC56187D3375F05FF75_H
#define ABSTRACTFPCURVE_TC851390204D76E2F3E8CBDC56187D3375F05FF75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.AbstractFpCurve
struct  AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75  : public ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTFPCURVE_TC851390204D76E2F3E8CBDC56187D3375F05FF75_H
#ifndef CURVE25519FIELDELEMENT_TCF4CD14C737564C90BA315743AEF0A58F4EC428F_H
#define CURVE25519FIELDELEMENT_TCF4CD14C737564C90BA315743AEF0A58F4EC428F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519FieldElement
struct  Curve25519FieldElement_tCF4CD14C737564C90BA315743AEF0A58F4EC428F  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_2;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Curve25519FieldElement_tCF4CD14C737564C90BA315743AEF0A58F4EC428F, ___x_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_2() const { return ___x_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_2 = value;
		Il2CppCodeGenWriteBarrier((&___x_2), value);
	}
};

struct Curve25519FieldElement_tCF4CD14C737564C90BA315743AEF0A58F4EC428F_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519FieldElement::Q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Q_0;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519FieldElement::PRECOMP_POW2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PRECOMP_POW2_1;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(Curve25519FieldElement_tCF4CD14C737564C90BA315743AEF0A58F4EC428F_StaticFields, ___Q_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}

	inline static int32_t get_offset_of_PRECOMP_POW2_1() { return static_cast<int32_t>(offsetof(Curve25519FieldElement_tCF4CD14C737564C90BA315743AEF0A58F4EC428F_StaticFields, ___PRECOMP_POW2_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PRECOMP_POW2_1() const { return ___PRECOMP_POW2_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PRECOMP_POW2_1() { return &___PRECOMP_POW2_1; }
	inline void set_PRECOMP_POW2_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PRECOMP_POW2_1 = value;
		Il2CppCodeGenWriteBarrier((&___PRECOMP_POW2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE25519FIELDELEMENT_TCF4CD14C737564C90BA315743AEF0A58F4EC428F_H
#ifndef SECP128R1FIELDELEMENT_T5E5D4A5F2D02945E83ABB85D70E668F09405FA15_H
#define SECP128R1FIELDELEMENT_T5E5D4A5F2D02945E83ABB85D70E668F09405FA15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1FieldElement
struct  SecP128R1FieldElement_t5E5D4A5F2D02945E83ABB85D70E668F09405FA15  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP128R1FieldElement_t5E5D4A5F2D02945E83ABB85D70E668F09405FA15, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP128R1FieldElement_t5E5D4A5F2D02945E83ABB85D70E668F09405FA15_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1FieldElement::Q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP128R1FieldElement_t5E5D4A5F2D02945E83ABB85D70E668F09405FA15_StaticFields, ___Q_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R1FIELDELEMENT_T5E5D4A5F2D02945E83ABB85D70E668F09405FA15_H
#ifndef SECP160R1FIELDELEMENT_TC55ACA6B93214270A21D765D7DB22E3A6C3D162E_H
#define SECP160R1FIELDELEMENT_TC55ACA6B93214270A21D765D7DB22E3A6C3D162E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1FieldElement
struct  SecP160R1FieldElement_tC55ACA6B93214270A21D765D7DB22E3A6C3D162E  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP160R1FieldElement_tC55ACA6B93214270A21D765D7DB22E3A6C3D162E, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP160R1FieldElement_tC55ACA6B93214270A21D765D7DB22E3A6C3D162E_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1FieldElement::Q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP160R1FieldElement_tC55ACA6B93214270A21D765D7DB22E3A6C3D162E_StaticFields, ___Q_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R1FIELDELEMENT_TC55ACA6B93214270A21D765D7DB22E3A6C3D162E_H
#ifndef SECP160R2FIELDELEMENT_T6D924740F01EFC8CF4A1E744377337261325B0EE_H
#define SECP160R2FIELDELEMENT_T6D924740F01EFC8CF4A1E744377337261325B0EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2FieldElement
struct  SecP160R2FieldElement_t6D924740F01EFC8CF4A1E744377337261325B0EE  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP160R2FieldElement_t6D924740F01EFC8CF4A1E744377337261325B0EE, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP160R2FieldElement_t6D924740F01EFC8CF4A1E744377337261325B0EE_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2FieldElement::Q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP160R2FieldElement_t6D924740F01EFC8CF4A1E744377337261325B0EE_StaticFields, ___Q_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R2FIELDELEMENT_T6D924740F01EFC8CF4A1E744377337261325B0EE_H
#ifndef SECP192K1FIELDELEMENT_T39F77052EFE0260D066A58E53DFAC80E8D70FF94_H
#define SECP192K1FIELDELEMENT_T39F77052EFE0260D066A58E53DFAC80E8D70FF94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1FieldElement
struct  SecP192K1FieldElement_t39F77052EFE0260D066A58E53DFAC80E8D70FF94  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP192K1FieldElement_t39F77052EFE0260D066A58E53DFAC80E8D70FF94, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP192K1FieldElement_t39F77052EFE0260D066A58E53DFAC80E8D70FF94_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1FieldElement::Q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP192K1FieldElement_t39F77052EFE0260D066A58E53DFAC80E8D70FF94_StaticFields, ___Q_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192K1FIELDELEMENT_T39F77052EFE0260D066A58E53DFAC80E8D70FF94_H
#ifndef SECP192R1FIELDELEMENT_TBA727BC9C951D1CEEAB8D3A1F0D41F45D31F56DA_H
#define SECP192R1FIELDELEMENT_TBA727BC9C951D1CEEAB8D3A1F0D41F45D31F56DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1FieldElement
struct  SecP192R1FieldElement_tBA727BC9C951D1CEEAB8D3A1F0D41F45D31F56DA  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP192R1FieldElement_tBA727BC9C951D1CEEAB8D3A1F0D41F45D31F56DA, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP192R1FieldElement_tBA727BC9C951D1CEEAB8D3A1F0D41F45D31F56DA_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1FieldElement::Q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP192R1FieldElement_tBA727BC9C951D1CEEAB8D3A1F0D41F45D31F56DA_StaticFields, ___Q_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192R1FIELDELEMENT_TBA727BC9C951D1CEEAB8D3A1F0D41F45D31F56DA_H
#ifndef SECP224K1FIELDELEMENT_T262966EDAD2A42C7546FB344398250AEFAAC204C_H
#define SECP224K1FIELDELEMENT_T262966EDAD2A42C7546FB344398250AEFAAC204C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1FieldElement
struct  SecP224K1FieldElement_t262966EDAD2A42C7546FB344398250AEFAAC204C  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_2;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(SecP224K1FieldElement_t262966EDAD2A42C7546FB344398250AEFAAC204C, ___x_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_2() const { return ___x_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_2 = value;
		Il2CppCodeGenWriteBarrier((&___x_2), value);
	}
};

struct SecP224K1FieldElement_t262966EDAD2A42C7546FB344398250AEFAAC204C_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1FieldElement::Q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Q_0;
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1FieldElement::PRECOMP_POW2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___PRECOMP_POW2_1;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP224K1FieldElement_t262966EDAD2A42C7546FB344398250AEFAAC204C_StaticFields, ___Q_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}

	inline static int32_t get_offset_of_PRECOMP_POW2_1() { return static_cast<int32_t>(offsetof(SecP224K1FieldElement_t262966EDAD2A42C7546FB344398250AEFAAC204C_StaticFields, ___PRECOMP_POW2_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_PRECOMP_POW2_1() const { return ___PRECOMP_POW2_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_PRECOMP_POW2_1() { return &___PRECOMP_POW2_1; }
	inline void set_PRECOMP_POW2_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___PRECOMP_POW2_1 = value;
		Il2CppCodeGenWriteBarrier((&___PRECOMP_POW2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224K1FIELDELEMENT_T262966EDAD2A42C7546FB344398250AEFAAC204C_H
#ifndef SECP224R1FIELDELEMENT_T4EEC5D62E1475AE516D67CD6F141AB0068ED6CD2_H
#define SECP224R1FIELDELEMENT_T4EEC5D62E1475AE516D67CD6F141AB0068ED6CD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1FieldElement
struct  SecP224R1FieldElement_t4EEC5D62E1475AE516D67CD6F141AB0068ED6CD2  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.UInt32[] Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1FieldElement::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_1;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(SecP224R1FieldElement_t4EEC5D62E1475AE516D67CD6F141AB0068ED6CD2, ___x_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_1() const { return ___x_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_1 = value;
		Il2CppCodeGenWriteBarrier((&___x_1), value);
	}
};

struct SecP224R1FieldElement_t4EEC5D62E1475AE516D67CD6F141AB0068ED6CD2_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1FieldElement::Q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Q_0;

public:
	inline static int32_t get_offset_of_Q_0() { return static_cast<int32_t>(offsetof(SecP224R1FieldElement_t4EEC5D62E1475AE516D67CD6F141AB0068ED6CD2_StaticFields, ___Q_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Q_0() const { return ___Q_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Q_0() { return &___Q_0; }
	inline void set_Q_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Q_0 = value;
		Il2CppCodeGenWriteBarrier((&___Q_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224R1FIELDELEMENT_T4EEC5D62E1475AE516D67CD6F141AB0068ED6CD2_H
#ifndef ECPOINTBASE_T7EE8D65EFE4A2748B20490039B5F88DFD8D8BF73_H
#define ECPOINTBASE_T7EE8D65EFE4A2748B20490039B5F88DFD8D8BF73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.ECPointBase
struct  ECPointBase_t7EE8D65EFE4A2748B20490039B5F88DFD8D8BF73  : public ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPOINTBASE_T7EE8D65EFE4A2748B20490039B5F88DFD8D8BF73_H
#ifndef F2MFIELDELEMENT_T98FE446EDD24A224827EE617D105A10E70A3B036_H
#define F2MFIELDELEMENT_T98FE446EDD24A224827EE617D105A10E70A3B036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.F2mFieldElement
struct  F2mFieldElement_t98FE446EDD24A224827EE617D105A10E70A3B036  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// System.Int32 Org.BouncyCastle.Math.EC.F2mFieldElement::representation
	int32_t ___representation_0;
	// System.Int32 Org.BouncyCastle.Math.EC.F2mFieldElement::m
	int32_t ___m_1;
	// System.Int32[] Org.BouncyCastle.Math.EC.F2mFieldElement::ks
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ks_2;
	// Org.BouncyCastle.Math.EC.LongArray Org.BouncyCastle.Math.EC.F2mFieldElement::x
	LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96 * ___x_3;

public:
	inline static int32_t get_offset_of_representation_0() { return static_cast<int32_t>(offsetof(F2mFieldElement_t98FE446EDD24A224827EE617D105A10E70A3B036, ___representation_0)); }
	inline int32_t get_representation_0() const { return ___representation_0; }
	inline int32_t* get_address_of_representation_0() { return &___representation_0; }
	inline void set_representation_0(int32_t value)
	{
		___representation_0 = value;
	}

	inline static int32_t get_offset_of_m_1() { return static_cast<int32_t>(offsetof(F2mFieldElement_t98FE446EDD24A224827EE617D105A10E70A3B036, ___m_1)); }
	inline int32_t get_m_1() const { return ___m_1; }
	inline int32_t* get_address_of_m_1() { return &___m_1; }
	inline void set_m_1(int32_t value)
	{
		___m_1 = value;
	}

	inline static int32_t get_offset_of_ks_2() { return static_cast<int32_t>(offsetof(F2mFieldElement_t98FE446EDD24A224827EE617D105A10E70A3B036, ___ks_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ks_2() const { return ___ks_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ks_2() { return &___ks_2; }
	inline void set_ks_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ks_2 = value;
		Il2CppCodeGenWriteBarrier((&___ks_2), value);
	}

	inline static int32_t get_offset_of_x_3() { return static_cast<int32_t>(offsetof(F2mFieldElement_t98FE446EDD24A224827EE617D105A10E70A3B036, ___x_3)); }
	inline LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96 * get_x_3() const { return ___x_3; }
	inline LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96 ** get_address_of_x_3() { return &___x_3; }
	inline void set_x_3(LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96 * value)
	{
		___x_3 = value;
		Il2CppCodeGenWriteBarrier((&___x_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // F2MFIELDELEMENT_T98FE446EDD24A224827EE617D105A10E70A3B036_H
#ifndef FPFIELDELEMENT_T0CC160EA07417AF2B1DDB358F9A3A896C84D1265_H
#define FPFIELDELEMENT_T0CC160EA07417AF2B1DDB358F9A3A896C84D1265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.FpFieldElement
struct  FpFieldElement_t0CC160EA07417AF2B1DDB358F9A3A896C84D1265  : public ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.FpFieldElement::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_0;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.FpFieldElement::r
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___r_1;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.FpFieldElement::x
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___x_2;

public:
	inline static int32_t get_offset_of_q_0() { return static_cast<int32_t>(offsetof(FpFieldElement_t0CC160EA07417AF2B1DDB358F9A3A896C84D1265, ___q_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_0() const { return ___q_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_0() { return &___q_0; }
	inline void set_q_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_0 = value;
		Il2CppCodeGenWriteBarrier((&___q_0), value);
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(FpFieldElement_t0CC160EA07417AF2B1DDB358F9A3A896C84D1265, ___r_1)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_r_1() const { return ___r_1; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___r_1 = value;
		Il2CppCodeGenWriteBarrier((&___r_1), value);
	}

	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(FpFieldElement_t0CC160EA07417AF2B1DDB358F9A3A896C84D1265, ___x_2)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_x_2() const { return ___x_2; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___x_2 = value;
		Il2CppCodeGenWriteBarrier((&___x_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPFIELDELEMENT_T0CC160EA07417AF2B1DDB358F9A3A896C84D1265_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef TLSDHEKEYEXCHANGE_T03AAF59B2915690D0264B74BA27B4D080674E316_H
#define TLSDHEKEYEXCHANGE_T03AAF59B2915690D0264B74BA27B4D080674E316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsDheKeyExchange
struct  TlsDheKeyExchange_t03AAF59B2915690D0264B74BA27B4D080674E316  : public TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSDHEKEYEXCHANGE_T03AAF59B2915690D0264B74BA27B4D080674E316_H
#ifndef TLSDSSSIGNER_T671A13B3796D8582E983195785CAF90C9BBA58AD_H
#define TLSDSSSIGNER_T671A13B3796D8582E983195785CAF90C9BBA58AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsDssSigner
struct  TlsDssSigner_t671A13B3796D8582E983195785CAF90C9BBA58AD  : public TlsDsaSigner_t6365A671792FD78F0CE5212C8B4CB48E29DF8B70
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSDSSSIGNER_T671A13B3796D8582E983195785CAF90C9BBA58AD_H
#ifndef TLSECDHEKEYEXCHANGE_T4A7941AD262D7BB0788609C98BF273BC5927E575_H
#define TLSECDHEKEYEXCHANGE_T4A7941AD262D7BB0788609C98BF273BC5927E575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsECDheKeyExchange
struct  TlsECDheKeyExchange_t4A7941AD262D7BB0788609C98BF273BC5927E575  : public TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSECDHEKEYEXCHANGE_T4A7941AD262D7BB0788609C98BF273BC5927E575_H
#ifndef TLSECDSASIGNER_T6C2C91B263D795D796B1B7BBE6575A0F5898CDFC_H
#define TLSECDSASIGNER_T6C2C91B263D795D796B1B7BBE6575A0F5898CDFC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsECDsaSigner
struct  TlsECDsaSigner_t6C2C91B263D795D796B1B7BBE6575A0F5898CDFC  : public TlsDsaSigner_t6365A671792FD78F0CE5212C8B4CB48E29DF8B70
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSECDSASIGNER_T6C2C91B263D795D796B1B7BBE6575A0F5898CDFC_H
#ifndef TLSPROTOCOL_T1304FF9D35D801AFF1B66BA125FD0087ACF55931_H
#define TLSPROTOCOL_T1304FF9D35D801AFF1B66BA125FD0087ACF55931_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsProtocol
struct  TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.ByteQueue Org.BouncyCastle.Crypto.Tls.TlsProtocol::mApplicationDataQueue
	ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26 * ___mApplicationDataQueue_1;
	// Org.BouncyCastle.Crypto.Tls.ByteQueue Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAlertQueue
	ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26 * ___mAlertQueue_2;
	// Org.BouncyCastle.Crypto.Tls.ByteQueue Org.BouncyCastle.Crypto.Tls.TlsProtocol::mHandshakeQueue
	ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26 * ___mHandshakeQueue_3;
	// Org.BouncyCastle.Crypto.Tls.RecordStream Org.BouncyCastle.Crypto.Tls.TlsProtocol::mRecordStream
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19 * ___mRecordStream_4;
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSecureRandom
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___mSecureRandom_5;
	// Org.BouncyCastle.Crypto.Tls.TlsStream Org.BouncyCastle.Crypto.Tls.TlsProtocol::mTlsStream
	TlsStream_t5204899C7CE492C3A11C1A16C01BFEE1D4D6D124 * ___mTlsStream_6;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Org.BouncyCastle.Crypto.Tls.TlsProtocol::mClosed
	bool ___mClosed_7;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Org.BouncyCastle.Crypto.Tls.TlsProtocol::mFailedWithError
	bool ___mFailedWithError_8;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAppDataReady
	bool ___mAppDataReady_9;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSplitApplicationDataRecords
	bool ___mSplitApplicationDataRecords_10;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsProtocol::mExpectedVerifyData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mExpectedVerifyData_11;
	// Org.BouncyCastle.Crypto.Tls.TlsSession Org.BouncyCastle.Crypto.Tls.TlsProtocol::mTlsSession
	RuntimeObject* ___mTlsSession_12;
	// Org.BouncyCastle.Crypto.Tls.SessionParameters Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSessionParameters
	SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115 * ___mSessionParameters_13;
	// Org.BouncyCastle.Crypto.Tls.SecurityParameters Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSecurityParameters
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB * ___mSecurityParameters_14;
	// Org.BouncyCastle.Crypto.Tls.Certificate Org.BouncyCastle.Crypto.Tls.TlsProtocol::mPeerCertificate
	Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D * ___mPeerCertificate_15;
	// System.Int32[] Org.BouncyCastle.Crypto.Tls.TlsProtocol::mOfferedCipherSuites
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mOfferedCipherSuites_16;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsProtocol::mOfferedCompressionMethods
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mOfferedCompressionMethods_17;
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.Tls.TlsProtocol::mClientExtensions
	RuntimeObject* ___mClientExtensions_18;
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.Tls.TlsProtocol::mServerExtensions
	RuntimeObject* ___mServerExtensions_19;
	// System.Int16 Org.BouncyCastle.Crypto.Tls.TlsProtocol::mConnectionState
	int16_t ___mConnectionState_20;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsProtocol::mResumedSession
	bool ___mResumedSession_21;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsProtocol::mReceivedChangeCipherSpec
	bool ___mReceivedChangeCipherSpec_22;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsProtocol::mSecureRenegotiation
	bool ___mSecureRenegotiation_23;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsProtocol::mAllowCertificateStatus
	bool ___mAllowCertificateStatus_24;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsProtocol::mExpectSessionTicket
	bool ___mExpectSessionTicket_25;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.TlsProtocol::mBlocking
	bool ___mBlocking_26;

public:
	inline static int32_t get_offset_of_mApplicationDataQueue_1() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mApplicationDataQueue_1)); }
	inline ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26 * get_mApplicationDataQueue_1() const { return ___mApplicationDataQueue_1; }
	inline ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26 ** get_address_of_mApplicationDataQueue_1() { return &___mApplicationDataQueue_1; }
	inline void set_mApplicationDataQueue_1(ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26 * value)
	{
		___mApplicationDataQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___mApplicationDataQueue_1), value);
	}

	inline static int32_t get_offset_of_mAlertQueue_2() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mAlertQueue_2)); }
	inline ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26 * get_mAlertQueue_2() const { return ___mAlertQueue_2; }
	inline ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26 ** get_address_of_mAlertQueue_2() { return &___mAlertQueue_2; }
	inline void set_mAlertQueue_2(ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26 * value)
	{
		___mAlertQueue_2 = value;
		Il2CppCodeGenWriteBarrier((&___mAlertQueue_2), value);
	}

	inline static int32_t get_offset_of_mHandshakeQueue_3() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mHandshakeQueue_3)); }
	inline ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26 * get_mHandshakeQueue_3() const { return ___mHandshakeQueue_3; }
	inline ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26 ** get_address_of_mHandshakeQueue_3() { return &___mHandshakeQueue_3; }
	inline void set_mHandshakeQueue_3(ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26 * value)
	{
		___mHandshakeQueue_3 = value;
		Il2CppCodeGenWriteBarrier((&___mHandshakeQueue_3), value);
	}

	inline static int32_t get_offset_of_mRecordStream_4() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mRecordStream_4)); }
	inline RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19 * get_mRecordStream_4() const { return ___mRecordStream_4; }
	inline RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19 ** get_address_of_mRecordStream_4() { return &___mRecordStream_4; }
	inline void set_mRecordStream_4(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19 * value)
	{
		___mRecordStream_4 = value;
		Il2CppCodeGenWriteBarrier((&___mRecordStream_4), value);
	}

	inline static int32_t get_offset_of_mSecureRandom_5() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mSecureRandom_5)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_mSecureRandom_5() const { return ___mSecureRandom_5; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_mSecureRandom_5() { return &___mSecureRandom_5; }
	inline void set_mSecureRandom_5(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___mSecureRandom_5 = value;
		Il2CppCodeGenWriteBarrier((&___mSecureRandom_5), value);
	}

	inline static int32_t get_offset_of_mTlsStream_6() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mTlsStream_6)); }
	inline TlsStream_t5204899C7CE492C3A11C1A16C01BFEE1D4D6D124 * get_mTlsStream_6() const { return ___mTlsStream_6; }
	inline TlsStream_t5204899C7CE492C3A11C1A16C01BFEE1D4D6D124 ** get_address_of_mTlsStream_6() { return &___mTlsStream_6; }
	inline void set_mTlsStream_6(TlsStream_t5204899C7CE492C3A11C1A16C01BFEE1D4D6D124 * value)
	{
		___mTlsStream_6 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsStream_6), value);
	}

	inline static int32_t get_offset_of_mClosed_7() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mClosed_7)); }
	inline bool get_mClosed_7() const { return ___mClosed_7; }
	inline bool* get_address_of_mClosed_7() { return &___mClosed_7; }
	inline void set_mClosed_7(bool value)
	{
		___mClosed_7 = value;
	}

	inline static int32_t get_offset_of_mFailedWithError_8() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mFailedWithError_8)); }
	inline bool get_mFailedWithError_8() const { return ___mFailedWithError_8; }
	inline bool* get_address_of_mFailedWithError_8() { return &___mFailedWithError_8; }
	inline void set_mFailedWithError_8(bool value)
	{
		___mFailedWithError_8 = value;
	}

	inline static int32_t get_offset_of_mAppDataReady_9() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mAppDataReady_9)); }
	inline bool get_mAppDataReady_9() const { return ___mAppDataReady_9; }
	inline bool* get_address_of_mAppDataReady_9() { return &___mAppDataReady_9; }
	inline void set_mAppDataReady_9(bool value)
	{
		___mAppDataReady_9 = value;
	}

	inline static int32_t get_offset_of_mSplitApplicationDataRecords_10() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mSplitApplicationDataRecords_10)); }
	inline bool get_mSplitApplicationDataRecords_10() const { return ___mSplitApplicationDataRecords_10; }
	inline bool* get_address_of_mSplitApplicationDataRecords_10() { return &___mSplitApplicationDataRecords_10; }
	inline void set_mSplitApplicationDataRecords_10(bool value)
	{
		___mSplitApplicationDataRecords_10 = value;
	}

	inline static int32_t get_offset_of_mExpectedVerifyData_11() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mExpectedVerifyData_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mExpectedVerifyData_11() const { return ___mExpectedVerifyData_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mExpectedVerifyData_11() { return &___mExpectedVerifyData_11; }
	inline void set_mExpectedVerifyData_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mExpectedVerifyData_11 = value;
		Il2CppCodeGenWriteBarrier((&___mExpectedVerifyData_11), value);
	}

	inline static int32_t get_offset_of_mTlsSession_12() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mTlsSession_12)); }
	inline RuntimeObject* get_mTlsSession_12() const { return ___mTlsSession_12; }
	inline RuntimeObject** get_address_of_mTlsSession_12() { return &___mTlsSession_12; }
	inline void set_mTlsSession_12(RuntimeObject* value)
	{
		___mTlsSession_12 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsSession_12), value);
	}

	inline static int32_t get_offset_of_mSessionParameters_13() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mSessionParameters_13)); }
	inline SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115 * get_mSessionParameters_13() const { return ___mSessionParameters_13; }
	inline SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115 ** get_address_of_mSessionParameters_13() { return &___mSessionParameters_13; }
	inline void set_mSessionParameters_13(SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115 * value)
	{
		___mSessionParameters_13 = value;
		Il2CppCodeGenWriteBarrier((&___mSessionParameters_13), value);
	}

	inline static int32_t get_offset_of_mSecurityParameters_14() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mSecurityParameters_14)); }
	inline SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB * get_mSecurityParameters_14() const { return ___mSecurityParameters_14; }
	inline SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB ** get_address_of_mSecurityParameters_14() { return &___mSecurityParameters_14; }
	inline void set_mSecurityParameters_14(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB * value)
	{
		___mSecurityParameters_14 = value;
		Il2CppCodeGenWriteBarrier((&___mSecurityParameters_14), value);
	}

	inline static int32_t get_offset_of_mPeerCertificate_15() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mPeerCertificate_15)); }
	inline Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D * get_mPeerCertificate_15() const { return ___mPeerCertificate_15; }
	inline Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D ** get_address_of_mPeerCertificate_15() { return &___mPeerCertificate_15; }
	inline void set_mPeerCertificate_15(Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D * value)
	{
		___mPeerCertificate_15 = value;
		Il2CppCodeGenWriteBarrier((&___mPeerCertificate_15), value);
	}

	inline static int32_t get_offset_of_mOfferedCipherSuites_16() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mOfferedCipherSuites_16)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mOfferedCipherSuites_16() const { return ___mOfferedCipherSuites_16; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mOfferedCipherSuites_16() { return &___mOfferedCipherSuites_16; }
	inline void set_mOfferedCipherSuites_16(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mOfferedCipherSuites_16 = value;
		Il2CppCodeGenWriteBarrier((&___mOfferedCipherSuites_16), value);
	}

	inline static int32_t get_offset_of_mOfferedCompressionMethods_17() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mOfferedCompressionMethods_17)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mOfferedCompressionMethods_17() const { return ___mOfferedCompressionMethods_17; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mOfferedCompressionMethods_17() { return &___mOfferedCompressionMethods_17; }
	inline void set_mOfferedCompressionMethods_17(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mOfferedCompressionMethods_17 = value;
		Il2CppCodeGenWriteBarrier((&___mOfferedCompressionMethods_17), value);
	}

	inline static int32_t get_offset_of_mClientExtensions_18() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mClientExtensions_18)); }
	inline RuntimeObject* get_mClientExtensions_18() const { return ___mClientExtensions_18; }
	inline RuntimeObject** get_address_of_mClientExtensions_18() { return &___mClientExtensions_18; }
	inline void set_mClientExtensions_18(RuntimeObject* value)
	{
		___mClientExtensions_18 = value;
		Il2CppCodeGenWriteBarrier((&___mClientExtensions_18), value);
	}

	inline static int32_t get_offset_of_mServerExtensions_19() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mServerExtensions_19)); }
	inline RuntimeObject* get_mServerExtensions_19() const { return ___mServerExtensions_19; }
	inline RuntimeObject** get_address_of_mServerExtensions_19() { return &___mServerExtensions_19; }
	inline void set_mServerExtensions_19(RuntimeObject* value)
	{
		___mServerExtensions_19 = value;
		Il2CppCodeGenWriteBarrier((&___mServerExtensions_19), value);
	}

	inline static int32_t get_offset_of_mConnectionState_20() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mConnectionState_20)); }
	inline int16_t get_mConnectionState_20() const { return ___mConnectionState_20; }
	inline int16_t* get_address_of_mConnectionState_20() { return &___mConnectionState_20; }
	inline void set_mConnectionState_20(int16_t value)
	{
		___mConnectionState_20 = value;
	}

	inline static int32_t get_offset_of_mResumedSession_21() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mResumedSession_21)); }
	inline bool get_mResumedSession_21() const { return ___mResumedSession_21; }
	inline bool* get_address_of_mResumedSession_21() { return &___mResumedSession_21; }
	inline void set_mResumedSession_21(bool value)
	{
		___mResumedSession_21 = value;
	}

	inline static int32_t get_offset_of_mReceivedChangeCipherSpec_22() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mReceivedChangeCipherSpec_22)); }
	inline bool get_mReceivedChangeCipherSpec_22() const { return ___mReceivedChangeCipherSpec_22; }
	inline bool* get_address_of_mReceivedChangeCipherSpec_22() { return &___mReceivedChangeCipherSpec_22; }
	inline void set_mReceivedChangeCipherSpec_22(bool value)
	{
		___mReceivedChangeCipherSpec_22 = value;
	}

	inline static int32_t get_offset_of_mSecureRenegotiation_23() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mSecureRenegotiation_23)); }
	inline bool get_mSecureRenegotiation_23() const { return ___mSecureRenegotiation_23; }
	inline bool* get_address_of_mSecureRenegotiation_23() { return &___mSecureRenegotiation_23; }
	inline void set_mSecureRenegotiation_23(bool value)
	{
		___mSecureRenegotiation_23 = value;
	}

	inline static int32_t get_offset_of_mAllowCertificateStatus_24() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mAllowCertificateStatus_24)); }
	inline bool get_mAllowCertificateStatus_24() const { return ___mAllowCertificateStatus_24; }
	inline bool* get_address_of_mAllowCertificateStatus_24() { return &___mAllowCertificateStatus_24; }
	inline void set_mAllowCertificateStatus_24(bool value)
	{
		___mAllowCertificateStatus_24 = value;
	}

	inline static int32_t get_offset_of_mExpectSessionTicket_25() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mExpectSessionTicket_25)); }
	inline bool get_mExpectSessionTicket_25() const { return ___mExpectSessionTicket_25; }
	inline bool* get_address_of_mExpectSessionTicket_25() { return &___mExpectSessionTicket_25; }
	inline void set_mExpectSessionTicket_25(bool value)
	{
		___mExpectSessionTicket_25 = value;
	}

	inline static int32_t get_offset_of_mBlocking_26() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931, ___mBlocking_26)); }
	inline bool get_mBlocking_26() const { return ___mBlocking_26; }
	inline bool* get_address_of_mBlocking_26() { return &___mBlocking_26; }
	inline void set_mBlocking_26(bool value)
	{
		___mBlocking_26 = value;
	}
};

struct TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931_StaticFields
{
public:
	// System.String Org.BouncyCastle.Crypto.Tls.TlsProtocol::TLS_ERROR_MESSAGE
	String_t* ___TLS_ERROR_MESSAGE_0;

public:
	inline static int32_t get_offset_of_TLS_ERROR_MESSAGE_0() { return static_cast<int32_t>(offsetof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931_StaticFields, ___TLS_ERROR_MESSAGE_0)); }
	inline String_t* get_TLS_ERROR_MESSAGE_0() const { return ___TLS_ERROR_MESSAGE_0; }
	inline String_t** get_address_of_TLS_ERROR_MESSAGE_0() { return &___TLS_ERROR_MESSAGE_0; }
	inline void set_TLS_ERROR_MESSAGE_0(String_t* value)
	{
		___TLS_ERROR_MESSAGE_0 = value;
		Il2CppCodeGenWriteBarrier((&___TLS_ERROR_MESSAGE_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSPROTOCOL_T1304FF9D35D801AFF1B66BA125FD0087ACF55931_H
#ifndef TLSSTREAM_T5204899C7CE492C3A11C1A16C01BFEE1D4D6D124_H
#define TLSSTREAM_T5204899C7CE492C3A11C1A16C01BFEE1D4D6D124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsStream
struct  TlsStream_t5204899C7CE492C3A11C1A16C01BFEE1D4D6D124  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsProtocol Org.BouncyCastle.Crypto.Tls.TlsStream::handler
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931 * ___handler_5;

public:
	inline static int32_t get_offset_of_handler_5() { return static_cast<int32_t>(offsetof(TlsStream_t5204899C7CE492C3A11C1A16C01BFEE1D4D6D124, ___handler_5)); }
	inline TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931 * get_handler_5() const { return ___handler_5; }
	inline TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931 ** get_address_of_handler_5() { return &___handler_5; }
	inline void set_handler_5(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931 * value)
	{
		___handler_5 = value;
		Il2CppCodeGenWriteBarrier((&___handler_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSSTREAM_T5204899C7CE492C3A11C1A16C01BFEE1D4D6D124_H
#ifndef ABSTRACTF2MPOINT_T31951E0CF9009420711A3FF51DE270702A830FAD_H
#define ABSTRACTF2MPOINT_T31951E0CF9009420711A3FF51DE270702A830FAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.AbstractF2mPoint
struct  AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD  : public ECPointBase_t7EE8D65EFE4A2748B20490039B5F88DFD8D8BF73
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTF2MPOINT_T31951E0CF9009420711A3FF51DE270702A830FAD_H
#ifndef ABSTRACTFPPOINT_T3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7_H
#define ABSTRACTFPPOINT_T3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.AbstractFpPoint
struct  AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7  : public ECPointBase_t7EE8D65EFE4A2748B20490039B5F88DFD8D8BF73
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTFPPOINT_T3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7_H
#ifndef CURVE25519_T3DA44F98AF086280BCBED992B1766AFAA24BCEE7_H
#define CURVE25519_T3DA44F98AF086280BCBED992B1766AFAA24BCEE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519
struct  Curve25519_t3DA44F98AF086280BCBED992B1766AFAA24BCEE7  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519Point Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519::m_infinity
	Curve25519Point_tB1B0391C34923E88E9E7E2B1A69BC886F173954B * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(Curve25519_t3DA44F98AF086280BCBED992B1766AFAA24BCEE7, ___m_infinity_9)); }
	inline Curve25519Point_tB1B0391C34923E88E9E7E2B1A69BC886F173954B * get_m_infinity_9() const { return ___m_infinity_9; }
	inline Curve25519Point_tB1B0391C34923E88E9E7E2B1A69BC886F173954B ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(Curve25519Point_tB1B0391C34923E88E9E7E2B1A69BC886F173954B * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct Curve25519_t3DA44F98AF086280BCBED992B1766AFAA24BCEE7_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_8;

public:
	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(Curve25519_t3DA44F98AF086280BCBED992B1766AFAA24BCEE7_StaticFields, ___q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_8() const { return ___q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE25519_T3DA44F98AF086280BCBED992B1766AFAA24BCEE7_H
#ifndef SECP128R1CURVE_TDC61BFFD5B2994100AA197B2216DCC3D030123A1_H
#define SECP128R1CURVE_TDC61BFFD5B2994100AA197B2216DCC3D030123A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Curve
struct  SecP128R1Curve_tDC61BFFD5B2994100AA197B2216DCC3D030123A1  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Curve::m_infinity
	SecP128R1Point_t80EF8EC90E8CF9D611D823201274CF9C28D77772 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecP128R1Curve_tDC61BFFD5B2994100AA197B2216DCC3D030123A1, ___m_infinity_9)); }
	inline SecP128R1Point_t80EF8EC90E8CF9D611D823201274CF9C28D77772 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecP128R1Point_t80EF8EC90E8CF9D611D823201274CF9C28D77772 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecP128R1Point_t80EF8EC90E8CF9D611D823201274CF9C28D77772 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct SecP128R1Curve_tDC61BFFD5B2994100AA197B2216DCC3D030123A1_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Curve::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_8;

public:
	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(SecP128R1Curve_tDC61BFFD5B2994100AA197B2216DCC3D030123A1_StaticFields, ___q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_8() const { return ___q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R1CURVE_TDC61BFFD5B2994100AA197B2216DCC3D030123A1_H
#ifndef SECP160K1CURVE_T8623BE417DD7CFBAC8D4B807AE0920AD85920EF0_H
#define SECP160K1CURVE_T8623BE417DD7CFBAC8D4B807AE0920AD85920EF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Curve
struct  SecP160K1Curve_t8623BE417DD7CFBAC8D4B807AE0920AD85920EF0  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Curve::m_infinity
	SecP160K1Point_tE15730C753F99380C34AD223AA068D1C959F8F86 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecP160K1Curve_t8623BE417DD7CFBAC8D4B807AE0920AD85920EF0, ___m_infinity_9)); }
	inline SecP160K1Point_tE15730C753F99380C34AD223AA068D1C959F8F86 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecP160K1Point_tE15730C753F99380C34AD223AA068D1C959F8F86 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecP160K1Point_tE15730C753F99380C34AD223AA068D1C959F8F86 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct SecP160K1Curve_t8623BE417DD7CFBAC8D4B807AE0920AD85920EF0_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Curve::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_8;

public:
	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(SecP160K1Curve_t8623BE417DD7CFBAC8D4B807AE0920AD85920EF0_StaticFields, ___q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_8() const { return ___q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160K1CURVE_T8623BE417DD7CFBAC8D4B807AE0920AD85920EF0_H
#ifndef SECP160R1CURVE_TE0DD248545E06465BA0437A4D00E27F90B55D0C3_H
#define SECP160R1CURVE_TE0DD248545E06465BA0437A4D00E27F90B55D0C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Curve
struct  SecP160R1Curve_tE0DD248545E06465BA0437A4D00E27F90B55D0C3  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Curve::m_infinity
	SecP160R1Point_t9D5C7DB7BC9CF68839652EC9D2DC5D4A95A30943 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecP160R1Curve_tE0DD248545E06465BA0437A4D00E27F90B55D0C3, ___m_infinity_9)); }
	inline SecP160R1Point_t9D5C7DB7BC9CF68839652EC9D2DC5D4A95A30943 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecP160R1Point_t9D5C7DB7BC9CF68839652EC9D2DC5D4A95A30943 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecP160R1Point_t9D5C7DB7BC9CF68839652EC9D2DC5D4A95A30943 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct SecP160R1Curve_tE0DD248545E06465BA0437A4D00E27F90B55D0C3_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Curve::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_8;

public:
	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(SecP160R1Curve_tE0DD248545E06465BA0437A4D00E27F90B55D0C3_StaticFields, ___q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_8() const { return ___q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R1CURVE_TE0DD248545E06465BA0437A4D00E27F90B55D0C3_H
#ifndef SECP160R2CURVE_T3E7212B61C964D6BF25F1CA460CB53F17496B676_H
#define SECP160R2CURVE_T3E7212B61C964D6BF25F1CA460CB53F17496B676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Curve
struct  SecP160R2Curve_t3E7212B61C964D6BF25F1CA460CB53F17496B676  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Point Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Curve::m_infinity
	SecP160R2Point_t5F4EE8D61DD9048C48BD4FDE87BD37C51E6970AF * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecP160R2Curve_t3E7212B61C964D6BF25F1CA460CB53F17496B676, ___m_infinity_9)); }
	inline SecP160R2Point_t5F4EE8D61DD9048C48BD4FDE87BD37C51E6970AF * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecP160R2Point_t5F4EE8D61DD9048C48BD4FDE87BD37C51E6970AF ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecP160R2Point_t5F4EE8D61DD9048C48BD4FDE87BD37C51E6970AF * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct SecP160R2Curve_t3E7212B61C964D6BF25F1CA460CB53F17496B676_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Curve::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_8;

public:
	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(SecP160R2Curve_t3E7212B61C964D6BF25F1CA460CB53F17496B676_StaticFields, ___q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_8() const { return ___q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R2CURVE_T3E7212B61C964D6BF25F1CA460CB53F17496B676_H
#ifndef SECP192K1CURVE_T4D5B599D14DF0FB21F1475B2294E88633F258162_H
#define SECP192K1CURVE_T4D5B599D14DF0FB21F1475B2294E88633F258162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Curve
struct  SecP192K1Curve_t4D5B599D14DF0FB21F1475B2294E88633F258162  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Curve::m_infinity
	SecP192K1Point_tA3A3258416DE02E73000D80D445C6627A91A4B77 * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecP192K1Curve_t4D5B599D14DF0FB21F1475B2294E88633F258162, ___m_infinity_9)); }
	inline SecP192K1Point_tA3A3258416DE02E73000D80D445C6627A91A4B77 * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecP192K1Point_tA3A3258416DE02E73000D80D445C6627A91A4B77 ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecP192K1Point_tA3A3258416DE02E73000D80D445C6627A91A4B77 * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct SecP192K1Curve_t4D5B599D14DF0FB21F1475B2294E88633F258162_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Curve::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_8;

public:
	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(SecP192K1Curve_t4D5B599D14DF0FB21F1475B2294E88633F258162_StaticFields, ___q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_8() const { return ___q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192K1CURVE_T4D5B599D14DF0FB21F1475B2294E88633F258162_H
#ifndef SECP192R1CURVE_TAE78F5E00B3DF9014039D27F5C79E29FAD7B34A6_H
#define SECP192R1CURVE_TAE78F5E00B3DF9014039D27F5C79E29FAD7B34A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Curve
struct  SecP192R1Curve_tAE78F5E00B3DF9014039D27F5C79E29FAD7B34A6  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Curve::m_infinity
	SecP192R1Point_tF263B963714C357592112834E6767394E3560E9E * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecP192R1Curve_tAE78F5E00B3DF9014039D27F5C79E29FAD7B34A6, ___m_infinity_9)); }
	inline SecP192R1Point_tF263B963714C357592112834E6767394E3560E9E * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecP192R1Point_tF263B963714C357592112834E6767394E3560E9E ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecP192R1Point_tF263B963714C357592112834E6767394E3560E9E * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct SecP192R1Curve_tAE78F5E00B3DF9014039D27F5C79E29FAD7B34A6_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Curve::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_8;

public:
	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(SecP192R1Curve_tAE78F5E00B3DF9014039D27F5C79E29FAD7B34A6_StaticFields, ___q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_8() const { return ___q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192R1CURVE_TAE78F5E00B3DF9014039D27F5C79E29FAD7B34A6_H
#ifndef SECP224K1CURVE_TCDDE14B9160269BD161F84E662BD2E0FBF1BC301_H
#define SECP224K1CURVE_TCDDE14B9160269BD161F84E662BD2E0FBF1BC301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Curve
struct  SecP224K1Curve_tCDDE14B9160269BD161F84E662BD2E0FBF1BC301  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Curve::m_infinity
	SecP224K1Point_t6DCA6A3B396FB10CA6D991B1354C748BC4F9235F * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecP224K1Curve_tCDDE14B9160269BD161F84E662BD2E0FBF1BC301, ___m_infinity_9)); }
	inline SecP224K1Point_t6DCA6A3B396FB10CA6D991B1354C748BC4F9235F * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecP224K1Point_t6DCA6A3B396FB10CA6D991B1354C748BC4F9235F ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecP224K1Point_t6DCA6A3B396FB10CA6D991B1354C748BC4F9235F * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct SecP224K1Curve_tCDDE14B9160269BD161F84E662BD2E0FBF1BC301_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Curve::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_8;

public:
	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(SecP224K1Curve_tCDDE14B9160269BD161F84E662BD2E0FBF1BC301_StaticFields, ___q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_8() const { return ___q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224K1CURVE_TCDDE14B9160269BD161F84E662BD2E0FBF1BC301_H
#ifndef SECP224R1CURVE_T80302359431CCFEA4889C230F76C674D45800F31_H
#define SECP224R1CURVE_T80302359431CCFEA4889C230F76C674D45800F31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Curve
struct  SecP224R1Curve_t80302359431CCFEA4889C230F76C674D45800F31  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Point Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Curve::m_infinity
	SecP224R1Point_t3DD809782D037568CA3A200FC33A8DA7390405CD * ___m_infinity_9;

public:
	inline static int32_t get_offset_of_m_infinity_9() { return static_cast<int32_t>(offsetof(SecP224R1Curve_t80302359431CCFEA4889C230F76C674D45800F31, ___m_infinity_9)); }
	inline SecP224R1Point_t3DD809782D037568CA3A200FC33A8DA7390405CD * get_m_infinity_9() const { return ___m_infinity_9; }
	inline SecP224R1Point_t3DD809782D037568CA3A200FC33A8DA7390405CD ** get_address_of_m_infinity_9() { return &___m_infinity_9; }
	inline void set_m_infinity_9(SecP224R1Point_t3DD809782D037568CA3A200FC33A8DA7390405CD * value)
	{
		___m_infinity_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_9), value);
	}
};

struct SecP224R1Curve_t80302359431CCFEA4889C230F76C674D45800F31_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.Custom.Sec.SecP224R1Curve::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_8;

public:
	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(SecP224R1Curve_t80302359431CCFEA4889C230F76C674D45800F31_StaticFields, ___q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_8() const { return ___q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224R1CURVE_T80302359431CCFEA4889C230F76C674D45800F31_H
#ifndef F2MCURVE_TC80A1D59D2E853794A1538482EE94A7BE489B45D_H
#define F2MCURVE_TC80A1D59D2E853794A1538482EE94A7BE489B45D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.F2mCurve
struct  F2mCurve_tC80A1D59D2E853794A1538482EE94A7BE489B45D  : public AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391
{
public:
	// System.Int32 Org.BouncyCastle.Math.EC.F2mCurve::m
	int32_t ___m_9;
	// System.Int32 Org.BouncyCastle.Math.EC.F2mCurve::k1
	int32_t ___k1_10;
	// System.Int32 Org.BouncyCastle.Math.EC.F2mCurve::k2
	int32_t ___k2_11;
	// System.Int32 Org.BouncyCastle.Math.EC.F2mCurve::k3
	int32_t ___k3_12;
	// Org.BouncyCastle.Math.EC.F2mPoint Org.BouncyCastle.Math.EC.F2mCurve::m_infinity
	F2mPoint_t7E9FECF441D8AA8E095B486D519BDBD04B0D66B7 * ___m_infinity_13;

public:
	inline static int32_t get_offset_of_m_9() { return static_cast<int32_t>(offsetof(F2mCurve_tC80A1D59D2E853794A1538482EE94A7BE489B45D, ___m_9)); }
	inline int32_t get_m_9() const { return ___m_9; }
	inline int32_t* get_address_of_m_9() { return &___m_9; }
	inline void set_m_9(int32_t value)
	{
		___m_9 = value;
	}

	inline static int32_t get_offset_of_k1_10() { return static_cast<int32_t>(offsetof(F2mCurve_tC80A1D59D2E853794A1538482EE94A7BE489B45D, ___k1_10)); }
	inline int32_t get_k1_10() const { return ___k1_10; }
	inline int32_t* get_address_of_k1_10() { return &___k1_10; }
	inline void set_k1_10(int32_t value)
	{
		___k1_10 = value;
	}

	inline static int32_t get_offset_of_k2_11() { return static_cast<int32_t>(offsetof(F2mCurve_tC80A1D59D2E853794A1538482EE94A7BE489B45D, ___k2_11)); }
	inline int32_t get_k2_11() const { return ___k2_11; }
	inline int32_t* get_address_of_k2_11() { return &___k2_11; }
	inline void set_k2_11(int32_t value)
	{
		___k2_11 = value;
	}

	inline static int32_t get_offset_of_k3_12() { return static_cast<int32_t>(offsetof(F2mCurve_tC80A1D59D2E853794A1538482EE94A7BE489B45D, ___k3_12)); }
	inline int32_t get_k3_12() const { return ___k3_12; }
	inline int32_t* get_address_of_k3_12() { return &___k3_12; }
	inline void set_k3_12(int32_t value)
	{
		___k3_12 = value;
	}

	inline static int32_t get_offset_of_m_infinity_13() { return static_cast<int32_t>(offsetof(F2mCurve_tC80A1D59D2E853794A1538482EE94A7BE489B45D, ___m_infinity_13)); }
	inline F2mPoint_t7E9FECF441D8AA8E095B486D519BDBD04B0D66B7 * get_m_infinity_13() const { return ___m_infinity_13; }
	inline F2mPoint_t7E9FECF441D8AA8E095B486D519BDBD04B0D66B7 ** get_address_of_m_infinity_13() { return &___m_infinity_13; }
	inline void set_m_infinity_13(F2mPoint_t7E9FECF441D8AA8E095B486D519BDBD04B0D66B7 * value)
	{
		___m_infinity_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // F2MCURVE_TC80A1D59D2E853794A1538482EE94A7BE489B45D_H
#ifndef FPCURVE_TD40864B7DC010952C5BB06EC76CE95B5929B5BD8_H
#define FPCURVE_TD40864B7DC010952C5BB06EC76CE95B5929B5BD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.FpCurve
struct  FpCurve_tD40864B7DC010952C5BB06EC76CE95B5929B5BD8  : public AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.FpCurve::m_q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___m_q_8;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Math.EC.FpCurve::m_r
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___m_r_9;
	// Org.BouncyCastle.Math.EC.FpPoint Org.BouncyCastle.Math.EC.FpCurve::m_infinity
	FpPoint_t7EA8AC711BD693B5C8A3B62DCFB98976FDC9779B * ___m_infinity_10;

public:
	inline static int32_t get_offset_of_m_q_8() { return static_cast<int32_t>(offsetof(FpCurve_tD40864B7DC010952C5BB06EC76CE95B5929B5BD8, ___m_q_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_m_q_8() const { return ___m_q_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_m_q_8() { return &___m_q_8; }
	inline void set_m_q_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___m_q_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_q_8), value);
	}

	inline static int32_t get_offset_of_m_r_9() { return static_cast<int32_t>(offsetof(FpCurve_tD40864B7DC010952C5BB06EC76CE95B5929B5BD8, ___m_r_9)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_m_r_9() const { return ___m_r_9; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_m_r_9() { return &___m_r_9; }
	inline void set_m_r_9(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___m_r_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_r_9), value);
	}

	inline static int32_t get_offset_of_m_infinity_10() { return static_cast<int32_t>(offsetof(FpCurve_tD40864B7DC010952C5BB06EC76CE95B5929B5BD8, ___m_infinity_10)); }
	inline FpPoint_t7EA8AC711BD693B5C8A3B62DCFB98976FDC9779B * get_m_infinity_10() const { return ___m_infinity_10; }
	inline FpPoint_t7EA8AC711BD693B5C8A3B62DCFB98976FDC9779B ** get_address_of_m_infinity_10() { return &___m_infinity_10; }
	inline void set_m_infinity_10(FpPoint_t7EA8AC711BD693B5C8A3B62DCFB98976FDC9779B * value)
	{
		___m_infinity_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_infinity_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPCURVE_TD40864B7DC010952C5BB06EC76CE95B5929B5BD8_H
#ifndef ZOUTPUTSTREAM_TD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425_H
#define ZOUTPUTSTREAM_TD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.Zlib.ZOutputStream
struct  ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// Org.BouncyCastle.Utilities.Zlib.ZStream Org.BouncyCastle.Utilities.Zlib.ZOutputStream::z
	ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * ___z_5;
	// System.Int32 Org.BouncyCastle.Utilities.Zlib.ZOutputStream::flushLevel
	int32_t ___flushLevel_6;
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.ZOutputStream::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_7;
	// System.Byte[] Org.BouncyCastle.Utilities.Zlib.ZOutputStream::buf1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf1_8;
	// System.Boolean Org.BouncyCastle.Utilities.Zlib.ZOutputStream::compress
	bool ___compress_9;
	// System.IO.Stream Org.BouncyCastle.Utilities.Zlib.ZOutputStream::output
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___output_10;
	// System.Boolean Org.BouncyCastle.Utilities.Zlib.ZOutputStream::closed
	bool ___closed_11;

public:
	inline static int32_t get_offset_of_z_5() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___z_5)); }
	inline ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * get_z_5() const { return ___z_5; }
	inline ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB ** get_address_of_z_5() { return &___z_5; }
	inline void set_z_5(ZStream_t0C4F57B016EBBC9D9BFB731D796AD0E88BB116AB * value)
	{
		___z_5 = value;
		Il2CppCodeGenWriteBarrier((&___z_5), value);
	}

	inline static int32_t get_offset_of_flushLevel_6() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___flushLevel_6)); }
	inline int32_t get_flushLevel_6() const { return ___flushLevel_6; }
	inline int32_t* get_address_of_flushLevel_6() { return &___flushLevel_6; }
	inline void set_flushLevel_6(int32_t value)
	{
		___flushLevel_6 = value;
	}

	inline static int32_t get_offset_of_buf_7() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___buf_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_7() const { return ___buf_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_7() { return &___buf_7; }
	inline void set_buf_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_7 = value;
		Il2CppCodeGenWriteBarrier((&___buf_7), value);
	}

	inline static int32_t get_offset_of_buf1_8() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___buf1_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf1_8() const { return ___buf1_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf1_8() { return &___buf1_8; }
	inline void set_buf1_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf1_8 = value;
		Il2CppCodeGenWriteBarrier((&___buf1_8), value);
	}

	inline static int32_t get_offset_of_compress_9() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___compress_9)); }
	inline bool get_compress_9() const { return ___compress_9; }
	inline bool* get_address_of_compress_9() { return &___compress_9; }
	inline void set_compress_9(bool value)
	{
		___compress_9 = value;
	}

	inline static int32_t get_offset_of_output_10() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___output_10)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_output_10() const { return ___output_10; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_output_10() { return &___output_10; }
	inline void set_output_10(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___output_10 = value;
		Il2CppCodeGenWriteBarrier((&___output_10), value);
	}

	inline static int32_t get_offset_of_closed_11() { return static_cast<int32_t>(offsetof(ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425, ___closed_11)); }
	inline bool get_closed_11() const { return ___closed_11; }
	inline bool* get_address_of_closed_11() { return &___closed_11; }
	inline void set_closed_11(bool value)
	{
		___closed_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOUTPUTSTREAM_TD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425_H
#ifndef IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#define IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.IOException
struct  IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.IO.IOException::_maybeFullPath
	String_t* ____maybeFullPath_17;

public:
	inline static int32_t get_offset_of__maybeFullPath_17() { return static_cast<int32_t>(offsetof(IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA, ____maybeFullPath_17)); }
	inline String_t* get__maybeFullPath_17() const { return ____maybeFullPath_17; }
	inline String_t** get_address_of__maybeFullPath_17() { return &____maybeFullPath_17; }
	inline void set__maybeFullPath_17(String_t* value)
	{
		____maybeFullPath_17 = value;
		Il2CppCodeGenWriteBarrier((&____maybeFullPath_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOEXCEPTION_T60E052020EDE4D3075F57A1DCC224FF8864354BA_H
#ifndef MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#define MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MemoryStream
struct  MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buffer_5;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_6;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_7;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_8;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_9;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_10;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_11;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_12;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_13;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * ____lastReadTask_14;

public:
	inline static int32_t get_offset_of__buffer_5() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____buffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buffer_5() const { return ____buffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buffer_5() { return &____buffer_5; }
	inline void set__buffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_5), value);
	}

	inline static int32_t get_offset_of__origin_6() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____origin_6)); }
	inline int32_t get__origin_6() const { return ____origin_6; }
	inline int32_t* get_address_of__origin_6() { return &____origin_6; }
	inline void set__origin_6(int32_t value)
	{
		____origin_6 = value;
	}

	inline static int32_t get_offset_of__position_7() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____position_7)); }
	inline int32_t get__position_7() const { return ____position_7; }
	inline int32_t* get_address_of__position_7() { return &____position_7; }
	inline void set__position_7(int32_t value)
	{
		____position_7 = value;
	}

	inline static int32_t get_offset_of__length_8() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____length_8)); }
	inline int32_t get__length_8() const { return ____length_8; }
	inline int32_t* get_address_of__length_8() { return &____length_8; }
	inline void set__length_8(int32_t value)
	{
		____length_8 = value;
	}

	inline static int32_t get_offset_of__capacity_9() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____capacity_9)); }
	inline int32_t get__capacity_9() const { return ____capacity_9; }
	inline int32_t* get_address_of__capacity_9() { return &____capacity_9; }
	inline void set__capacity_9(int32_t value)
	{
		____capacity_9 = value;
	}

	inline static int32_t get_offset_of__expandable_10() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____expandable_10)); }
	inline bool get__expandable_10() const { return ____expandable_10; }
	inline bool* get_address_of__expandable_10() { return &____expandable_10; }
	inline void set__expandable_10(bool value)
	{
		____expandable_10 = value;
	}

	inline static int32_t get_offset_of__writable_11() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____writable_11)); }
	inline bool get__writable_11() const { return ____writable_11; }
	inline bool* get_address_of__writable_11() { return &____writable_11; }
	inline void set__writable_11(bool value)
	{
		____writable_11 = value;
	}

	inline static int32_t get_offset_of__exposable_12() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____exposable_12)); }
	inline bool get__exposable_12() const { return ____exposable_12; }
	inline bool* get_address_of__exposable_12() { return &____exposable_12; }
	inline void set__exposable_12(bool value)
	{
		____exposable_12 = value;
	}

	inline static int32_t get_offset_of__isOpen_13() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____isOpen_13)); }
	inline bool get__isOpen_13() const { return ____isOpen_13; }
	inline bool* get_address_of__isOpen_13() { return &____isOpen_13; }
	inline void set__isOpen_13(bool value)
	{
		____isOpen_13 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_14() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____lastReadTask_14)); }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * get__lastReadTask_14() const { return ____lastReadTask_14; }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 ** get_address_of__lastReadTask_14() { return &____lastReadTask_14; }
	inline void set__lastReadTask_14(Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * value)
	{
		____lastReadTask_14 = value;
		Il2CppCodeGenWriteBarrier((&____lastReadTask_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifndef TLSCLIENTPROTOCOL_T6120DE25172A18DBE2DB91EDBE63D97ACD15F611_H
#define TLSCLIENTPROTOCOL_T6120DE25172A18DBE2DB91EDBE63D97ACD15F611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsClientProtocol
struct  TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611  : public TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsClient Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mTlsClient
	RuntimeObject* ___mTlsClient_27;
	// Org.BouncyCastle.Crypto.Tls.TlsClientContextImpl Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mTlsClientContext
	TlsClientContextImpl_t24CB6216F1148B76E46DB71DF497691FC2179BD4 * ___mTlsClientContext_28;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mSelectedSessionID
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSelectedSessionID_29;
	// Org.BouncyCastle.Crypto.Tls.TlsKeyExchange Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mKeyExchange
	RuntimeObject* ___mKeyExchange_30;
	// Org.BouncyCastle.Crypto.Tls.TlsAuthentication Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mAuthentication
	RuntimeObject* ___mAuthentication_31;
	// Org.BouncyCastle.Crypto.Tls.CertificateStatus Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mCertificateStatus
	CertificateStatus_t1F391A4805FD1E6E500F7F5AD46A312FF7057AEA * ___mCertificateStatus_32;
	// Org.BouncyCastle.Crypto.Tls.CertificateRequest Org.BouncyCastle.Crypto.Tls.TlsClientProtocol::mCertificateRequest
	CertificateRequest_t2D3D094933C7C24EA9ED681B96C590E0488E0105 * ___mCertificateRequest_33;

public:
	inline static int32_t get_offset_of_mTlsClient_27() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611, ___mTlsClient_27)); }
	inline RuntimeObject* get_mTlsClient_27() const { return ___mTlsClient_27; }
	inline RuntimeObject** get_address_of_mTlsClient_27() { return &___mTlsClient_27; }
	inline void set_mTlsClient_27(RuntimeObject* value)
	{
		___mTlsClient_27 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsClient_27), value);
	}

	inline static int32_t get_offset_of_mTlsClientContext_28() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611, ___mTlsClientContext_28)); }
	inline TlsClientContextImpl_t24CB6216F1148B76E46DB71DF497691FC2179BD4 * get_mTlsClientContext_28() const { return ___mTlsClientContext_28; }
	inline TlsClientContextImpl_t24CB6216F1148B76E46DB71DF497691FC2179BD4 ** get_address_of_mTlsClientContext_28() { return &___mTlsClientContext_28; }
	inline void set_mTlsClientContext_28(TlsClientContextImpl_t24CB6216F1148B76E46DB71DF497691FC2179BD4 * value)
	{
		___mTlsClientContext_28 = value;
		Il2CppCodeGenWriteBarrier((&___mTlsClientContext_28), value);
	}

	inline static int32_t get_offset_of_mSelectedSessionID_29() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611, ___mSelectedSessionID_29)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSelectedSessionID_29() const { return ___mSelectedSessionID_29; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSelectedSessionID_29() { return &___mSelectedSessionID_29; }
	inline void set_mSelectedSessionID_29(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSelectedSessionID_29 = value;
		Il2CppCodeGenWriteBarrier((&___mSelectedSessionID_29), value);
	}

	inline static int32_t get_offset_of_mKeyExchange_30() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611, ___mKeyExchange_30)); }
	inline RuntimeObject* get_mKeyExchange_30() const { return ___mKeyExchange_30; }
	inline RuntimeObject** get_address_of_mKeyExchange_30() { return &___mKeyExchange_30; }
	inline void set_mKeyExchange_30(RuntimeObject* value)
	{
		___mKeyExchange_30 = value;
		Il2CppCodeGenWriteBarrier((&___mKeyExchange_30), value);
	}

	inline static int32_t get_offset_of_mAuthentication_31() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611, ___mAuthentication_31)); }
	inline RuntimeObject* get_mAuthentication_31() const { return ___mAuthentication_31; }
	inline RuntimeObject** get_address_of_mAuthentication_31() { return &___mAuthentication_31; }
	inline void set_mAuthentication_31(RuntimeObject* value)
	{
		___mAuthentication_31 = value;
		Il2CppCodeGenWriteBarrier((&___mAuthentication_31), value);
	}

	inline static int32_t get_offset_of_mCertificateStatus_32() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611, ___mCertificateStatus_32)); }
	inline CertificateStatus_t1F391A4805FD1E6E500F7F5AD46A312FF7057AEA * get_mCertificateStatus_32() const { return ___mCertificateStatus_32; }
	inline CertificateStatus_t1F391A4805FD1E6E500F7F5AD46A312FF7057AEA ** get_address_of_mCertificateStatus_32() { return &___mCertificateStatus_32; }
	inline void set_mCertificateStatus_32(CertificateStatus_t1F391A4805FD1E6E500F7F5AD46A312FF7057AEA * value)
	{
		___mCertificateStatus_32 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificateStatus_32), value);
	}

	inline static int32_t get_offset_of_mCertificateRequest_33() { return static_cast<int32_t>(offsetof(TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611, ___mCertificateRequest_33)); }
	inline CertificateRequest_t2D3D094933C7C24EA9ED681B96C590E0488E0105 * get_mCertificateRequest_33() const { return ___mCertificateRequest_33; }
	inline CertificateRequest_t2D3D094933C7C24EA9ED681B96C590E0488E0105 ** get_address_of_mCertificateRequest_33() { return &___mCertificateRequest_33; }
	inline void set_mCertificateRequest_33(CertificateRequest_t2D3D094933C7C24EA9ED681B96C590E0488E0105 * value)
	{
		___mCertificateRequest_33 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificateRequest_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSCLIENTPROTOCOL_T6120DE25172A18DBE2DB91EDBE63D97ACD15F611_H
#ifndef DEFLATEOUTPUTSTREAM_T7AAC769D0CC9A6F5AE40ECDA415DD6AB1C3492F8_H
#define DEFLATEOUTPUTSTREAM_T7AAC769D0CC9A6F5AE40ECDA415DD6AB1C3492F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsDeflateCompression_DeflateOutputStream
struct  DeflateOutputStream_t7AAC769D0CC9A6F5AE40ECDA415DD6AB1C3492F8  : public ZOutputStream_tD57631B7DC3EC4C6F62BA64EABA8ADF71D49A425
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFLATEOUTPUTSTREAM_T7AAC769D0CC9A6F5AE40ECDA415DD6AB1C3492F8_H
#ifndef TLSFATALALERT_T7B8EAEE9602D81D91774358C6852E53BF283A649_H
#define TLSFATALALERT_T7B8EAEE9602D81D91774358C6852E53BF283A649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsFatalAlert
struct  TlsFatalAlert_t7B8EAEE9602D81D91774358C6852E53BF283A649  : public IOException_t60E052020EDE4D3075F57A1DCC224FF8864354BA
{
public:
	// System.Byte Org.BouncyCastle.Crypto.Tls.TlsFatalAlert::alertDescription
	uint8_t ___alertDescription_18;

public:
	inline static int32_t get_offset_of_alertDescription_18() { return static_cast<int32_t>(offsetof(TlsFatalAlert_t7B8EAEE9602D81D91774358C6852E53BF283A649, ___alertDescription_18)); }
	inline uint8_t get_alertDescription_18() const { return ___alertDescription_18; }
	inline uint8_t* get_address_of_alertDescription_18() { return &___alertDescription_18; }
	inline void set_alertDescription_18(uint8_t value)
	{
		___alertDescription_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSFATALALERT_T7B8EAEE9602D81D91774358C6852E53BF283A649_H
#ifndef HANDSHAKEMESSAGE_TC25B55BAB53E026D4FEB1E133DC022F031165B37_H
#define HANDSHAKEMESSAGE_TC25B55BAB53E026D4FEB1E133DC022F031165B37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsProtocol_HandshakeMessage
struct  HandshakeMessage_tC25B55BAB53E026D4FEB1E133DC022F031165B37  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HANDSHAKEMESSAGE_TC25B55BAB53E026D4FEB1E133DC022F031165B37_H
#ifndef CURVE25519POINT_TB1B0391C34923E88E9E7E2B1A69BC886F173954B_H
#define CURVE25519POINT_TB1B0391C34923E88E9E7E2B1A69BC886F173954B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Djb.Curve25519Point
struct  Curve25519Point_tB1B0391C34923E88E9E7E2B1A69BC886F173954B  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVE25519POINT_TB1B0391C34923E88E9E7E2B1A69BC886F173954B_H
#ifndef SECP128R1POINT_T80EF8EC90E8CF9D611D823201274CF9C28D77772_H
#define SECP128R1POINT_T80EF8EC90E8CF9D611D823201274CF9C28D77772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP128R1Point
struct  SecP128R1Point_t80EF8EC90E8CF9D611D823201274CF9C28D77772  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP128R1POINT_T80EF8EC90E8CF9D611D823201274CF9C28D77772_H
#ifndef SECP160K1POINT_TE15730C753F99380C34AD223AA068D1C959F8F86_H
#define SECP160K1POINT_TE15730C753F99380C34AD223AA068D1C959F8F86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160K1Point
struct  SecP160K1Point_tE15730C753F99380C34AD223AA068D1C959F8F86  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160K1POINT_TE15730C753F99380C34AD223AA068D1C959F8F86_H
#ifndef SECP160R1POINT_T9D5C7DB7BC9CF68839652EC9D2DC5D4A95A30943_H
#define SECP160R1POINT_T9D5C7DB7BC9CF68839652EC9D2DC5D4A95A30943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R1Point
struct  SecP160R1Point_t9D5C7DB7BC9CF68839652EC9D2DC5D4A95A30943  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R1POINT_T9D5C7DB7BC9CF68839652EC9D2DC5D4A95A30943_H
#ifndef SECP160R2POINT_T5F4EE8D61DD9048C48BD4FDE87BD37C51E6970AF_H
#define SECP160R2POINT_T5F4EE8D61DD9048C48BD4FDE87BD37C51E6970AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP160R2Point
struct  SecP160R2Point_t5F4EE8D61DD9048C48BD4FDE87BD37C51E6970AF  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP160R2POINT_T5F4EE8D61DD9048C48BD4FDE87BD37C51E6970AF_H
#ifndef SECP192K1POINT_TA3A3258416DE02E73000D80D445C6627A91A4B77_H
#define SECP192K1POINT_TA3A3258416DE02E73000D80D445C6627A91A4B77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP192K1Point
struct  SecP192K1Point_tA3A3258416DE02E73000D80D445C6627A91A4B77  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192K1POINT_TA3A3258416DE02E73000D80D445C6627A91A4B77_H
#ifndef SECP192R1POINT_TF263B963714C357592112834E6767394E3560E9E_H
#define SECP192R1POINT_TF263B963714C357592112834E6767394E3560E9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP192R1Point
struct  SecP192R1Point_tF263B963714C357592112834E6767394E3560E9E  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP192R1POINT_TF263B963714C357592112834E6767394E3560E9E_H
#ifndef SECP224K1POINT_T6DCA6A3B396FB10CA6D991B1354C748BC4F9235F_H
#define SECP224K1POINT_T6DCA6A3B396FB10CA6D991B1354C748BC4F9235F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.Custom.Sec.SecP224K1Point
struct  SecP224K1Point_t6DCA6A3B396FB10CA6D991B1354C748BC4F9235F  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECP224K1POINT_T6DCA6A3B396FB10CA6D991B1354C748BC4F9235F_H
#ifndef F2MPOINT_T7E9FECF441D8AA8E095B486D519BDBD04B0D66B7_H
#define F2MPOINT_T7E9FECF441D8AA8E095B486D519BDBD04B0D66B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.F2mPoint
struct  F2mPoint_t7E9FECF441D8AA8E095B486D519BDBD04B0D66B7  : public AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // F2MPOINT_T7E9FECF441D8AA8E095B486D519BDBD04B0D66B7_H
#ifndef FPPOINT_T7EA8AC711BD693B5C8A3B62DCFB98976FDC9779B_H
#define FPPOINT_T7EA8AC711BD693B5C8A3B62DCFB98976FDC9779B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Math.EC.FpPoint
struct  FpPoint_t7EA8AC711BD693B5C8A3B62DCFB98976FDC9779B  : public AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPPOINT_T7EA8AC711BD693B5C8A3B62DCFB98976FDC9779B_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5200 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5201 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5202 = { sizeof (TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5202[8] = 
{
	TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72::get_offset_of_context_0(),
	TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72::get_offset_of_randomData_1(),
	TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72::get_offset_of_useExplicitIV_2(),
	TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72::get_offset_of_encryptThenMac_3(),
	TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72::get_offset_of_encryptCipher_4(),
	TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72::get_offset_of_decryptCipher_5(),
	TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72::get_offset_of_mWriteMac_6(),
	TlsBlockCipher_t51395AD0E411C7A37D1F10F06C93F4C5814E4F72::get_offset_of_mReadMac_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5203 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5204 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5205 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5206 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5207 = { sizeof (TlsClientContextImpl_t24CB6216F1148B76E46DB71DF497691FC2179BD4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5208 = { sizeof (TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5208[7] = 
{
	TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611::get_offset_of_mTlsClient_27(),
	TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611::get_offset_of_mTlsClientContext_28(),
	TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611::get_offset_of_mSelectedSessionID_29(),
	TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611::get_offset_of_mKeyExchange_30(),
	TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611::get_offset_of_mAuthentication_31(),
	TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611::get_offset_of_mCertificateStatus_32(),
	TlsClientProtocol_t6120DE25172A18DBE2DB91EDBE63D97ACD15F611::get_offset_of_mCertificateRequest_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5209 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5210 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5211 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5212 = { sizeof (TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5212[6] = 
{
	TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D::get_offset_of_mTlsSigner_3(),
	TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D::get_offset_of_mDHParameters_4(),
	TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D::get_offset_of_mServerPublicKey_5(),
	TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D::get_offset_of_mAgreementCredentials_6(),
	TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D::get_offset_of_mDHAgreePrivateKey_7(),
	TlsDHKeyExchange_t6DB9948CBA1DAEF0F7687207352642DE11F64D1D::get_offset_of_mDHAgreePublicKey_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5213 = { sizeof (TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880), -1, sizeof(TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5213[11] = 
{
	TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields::get_offset_of_Two_0(),
	TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields::get_offset_of_draft_ffdhe2432_p_1(),
	TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields::get_offset_of_draft_ffdhe2432_2(),
	TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields::get_offset_of_draft_ffdhe3072_p_3(),
	TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields::get_offset_of_draft_ffdhe3072_4(),
	TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields::get_offset_of_draft_ffdhe4096_p_5(),
	TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields::get_offset_of_draft_ffdhe4096_6(),
	TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields::get_offset_of_draft_ffdhe6144_p_7(),
	TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields::get_offset_of_draft_ffdhe6144_8(),
	TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields::get_offset_of_draft_ffdhe8192_p_9(),
	TlsDHUtilities_tEF9B825C23804AFA8D4031006206C0549BCBF880_StaticFields::get_offset_of_draft_ffdhe8192_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5214 = { sizeof (TlsDeflateCompression_tAAE821D33B93FE40361ABA030C699CBBC2C513CB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5214[2] = 
{
	TlsDeflateCompression_tAAE821D33B93FE40361ABA030C699CBBC2C513CB::get_offset_of_zIn_0(),
	TlsDeflateCompression_tAAE821D33B93FE40361ABA030C699CBBC2C513CB::get_offset_of_zOut_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5215 = { sizeof (DeflateOutputStream_t7AAC769D0CC9A6F5AE40ECDA415DD6AB1C3492F8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5216 = { sizeof (TlsDheKeyExchange_t03AAF59B2915690D0264B74BA27B4D080674E316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5217 = { sizeof (TlsDsaSigner_t6365A671792FD78F0CE5212C8B4CB48E29DF8B70), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5218 = { sizeof (TlsDssSigner_t671A13B3796D8582E983195785CAF90C9BBA58AD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5219 = { sizeof (TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5219[8] = 
{
	TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05::get_offset_of_mTlsSigner_3(),
	TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05::get_offset_of_mNamedCurves_4(),
	TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05::get_offset_of_mClientECPointFormats_5(),
	TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05::get_offset_of_mServerECPointFormats_6(),
	TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05::get_offset_of_mServerPublicKey_7(),
	TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05::get_offset_of_mAgreementCredentials_8(),
	TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05::get_offset_of_mECAgreePrivateKey_9(),
	TlsECDHKeyExchange_t64475B160C7265859959F209765AADDAD9BE3E05::get_offset_of_mECAgreePublicKey_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5220 = { sizeof (TlsECDheKeyExchange_t4A7941AD262D7BB0788609C98BF273BC5927E575), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5221 = { sizeof (TlsECDsaSigner_t6C2C91B263D795D796B1B7BBE6575A0F5898CDFC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5222 = { sizeof (TlsEccUtilities_tC54E1B1B6C214ED9D6B6C0599DD8AC4BD33F8E5D), -1, sizeof(TlsEccUtilities_tC54E1B1B6C214ED9D6B6C0599DD8AC4BD33F8E5D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5222[1] = 
{
	TlsEccUtilities_tC54E1B1B6C214ED9D6B6C0599DD8AC4BD33F8E5D_StaticFields::get_offset_of_CurveNames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5223 = { sizeof (TlsExtensionsUtilities_t83B52EEB5DCB95209232DF1C8D557B31231CEA0C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5224 = { sizeof (TlsFatalAlert_t7B8EAEE9602D81D91774358C6852E53BF283A649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5224[1] = 
{
	TlsFatalAlert_t7B8EAEE9602D81D91774358C6852E53BF283A649::get_offset_of_alertDescription_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5225 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5226 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5227 = { sizeof (TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5227[6] = 
{
	TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194::get_offset_of_context_0(),
	TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194::get_offset_of_secret_1(),
	TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194::get_offset_of_mac_2(),
	TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194::get_offset_of_digestBlockSize_3(),
	TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194::get_offset_of_digestOverhead_4(),
	TlsMac_tA2D5BC2A61001B9745A1849D4DC2398036773194::get_offset_of_macLength_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5228 = { sizeof (TlsNullCipher_tC202BD474327CCFC9499EC7478E83E45DB4093C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5228[3] = 
{
	TlsNullCipher_tC202BD474327CCFC9499EC7478E83E45DB4093C5::get_offset_of_context_0(),
	TlsNullCipher_tC202BD474327CCFC9499EC7478E83E45DB4093C5::get_offset_of_writeMac_1(),
	TlsNullCipher_tC202BD474327CCFC9499EC7478E83E45DB4093C5::get_offset_of_readMac_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5229 = { sizeof (TlsNullCompression_t3D5BA7EB28D8E5D9CAA2C8C6C7D61A5E6651A969), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5230 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5231 = { sizeof (TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931), -1, sizeof(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5231[27] = 
{
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931_StaticFields::get_offset_of_TLS_ERROR_MESSAGE_0(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mApplicationDataQueue_1(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mAlertQueue_2(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mHandshakeQueue_3(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mRecordStream_4(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mSecureRandom_5(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mTlsStream_6(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mClosed_7(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mFailedWithError_8(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mAppDataReady_9(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mSplitApplicationDataRecords_10(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mExpectedVerifyData_11(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mTlsSession_12(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mSessionParameters_13(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mSecurityParameters_14(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mPeerCertificate_15(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mOfferedCipherSuites_16(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mOfferedCompressionMethods_17(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mClientExtensions_18(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mServerExtensions_19(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mConnectionState_20(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mResumedSession_21(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mReceivedChangeCipherSpec_22(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mSecureRenegotiation_23(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mAllowCertificateStatus_24(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mExpectSessionTicket_25(),
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931::get_offset_of_mBlocking_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5232 = { sizeof (HandshakeMessage_tC25B55BAB53E026D4FEB1E133DC022F031165B37), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5233 = { sizeof (TlsRsaKeyExchange_t40BDD2D90BAD3A806C0EBF311AAD0FC12B1B67FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5233[3] = 
{
	TlsRsaKeyExchange_t40BDD2D90BAD3A806C0EBF311AAD0FC12B1B67FA::get_offset_of_mServerPublicKey_3(),
	TlsRsaKeyExchange_t40BDD2D90BAD3A806C0EBF311AAD0FC12B1B67FA::get_offset_of_mRsaServerPublicKey_4(),
	TlsRsaKeyExchange_t40BDD2D90BAD3A806C0EBF311AAD0FC12B1B67FA::get_offset_of_mPremasterSecret_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5234 = { sizeof (TlsRsaSigner_t1DDB99DFB8BCDBDDCEFDB4107CA224C991D5B798), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5235 = { sizeof (TlsRsaUtilities_t7C7D2198FE7F727B1666CE72D3DC82EABDEE3B4B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5236 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5237 = { sizeof (TlsSessionImpl_t937CCFC5FCDED7C43715D0FA6400A1A528AC19E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5237[2] = 
{
	TlsSessionImpl_t937CCFC5FCDED7C43715D0FA6400A1A528AC19E0::get_offset_of_mSessionID_0(),
	TlsSessionImpl_t937CCFC5FCDED7C43715D0FA6400A1A528AC19E0::get_offset_of_mSessionParameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5238 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5239 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5240 = { sizeof (TlsStream_t5204899C7CE492C3A11C1A16C01BFEE1D4D6D124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5240[1] = 
{
	TlsStream_t5204899C7CE492C3A11C1A16C01BFEE1D4D6D124::get_offset_of_handler_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5241 = { sizeof (TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5241[6] = 
{
	TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E::get_offset_of_context_0(),
	TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E::get_offset_of_encryptCipher_1(),
	TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E::get_offset_of_decryptCipher_2(),
	TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E::get_offset_of_writeMac_3(),
	TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E::get_offset_of_readMac_4(),
	TlsStreamCipher_tAC61C30DC9193A760E9CACA5DCC9D4B3C4714E1E::get_offset_of_usesNonce_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5242 = { sizeof (TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5), -1, sizeof(TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5242[7] = 
{
	TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields::get_offset_of_EmptyBytes_0(),
	TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields::get_offset_of_EmptyShorts_1(),
	TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields::get_offset_of_EmptyInts_2(),
	TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields::get_offset_of_EmptyLongs_3(),
	TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields::get_offset_of_SSL_CLIENT_4(),
	TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields::get_offset_of_SSL_SERVER_5(),
	TlsUtilities_tE7F0880CC6F208DAB22A2B9FD2E2E7F37AA427A5_StaticFields::get_offset_of_SSL3_CONST_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5243 = { sizeof (Pack_t8F8B4E883896536870B1B5DE5151562DB66EE8C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5244 = { sizeof (BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91), -1, sizeof(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5244[26] = 
{
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_primeLists_0(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_primeProducts_1(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_ZeroMagnitude_2(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_ZeroEncoding_3(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_SMALL_CONSTANTS_4(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_Zero_5(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_One_6(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_Two_7(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_Three_8(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_Ten_9(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_BitLengthTable_10(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_radix2_11(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_radix2E_12(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_radix8_13(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_radix8E_14(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_radix10_15(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_radix10E_16(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_radix16_17(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_radix16E_18(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_RandomSource_19(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91_StaticFields::get_offset_of_ExpWindowThresholds_20(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91::get_offset_of_magnitude_21(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91::get_offset_of_sign_22(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91::get_offset_of_nBits_23(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91::get_offset_of_nBitLength_24(),
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91::get_offset_of_mQuote_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5245 = { sizeof (ECAlgorithms_tFA1D868A76B0FD79B886A99696087DFED87FE94C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5246 = { sizeof (ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5246[8] = 
{
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95::get_offset_of_m_field_0(),
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95::get_offset_of_m_a_1(),
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95::get_offset_of_m_b_2(),
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95::get_offset_of_m_order_3(),
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95::get_offset_of_m_cofactor_4(),
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95::get_offset_of_m_coord_5(),
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95::get_offset_of_m_endomorphism_6(),
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95::get_offset_of_m_multiplier_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5247 = { sizeof (Config_tB8033A9496C70BD7A0CBA6D41F8FAC15216135A5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5247[4] = 
{
	Config_tB8033A9496C70BD7A0CBA6D41F8FAC15216135A5::get_offset_of_outer_0(),
	Config_tB8033A9496C70BD7A0CBA6D41F8FAC15216135A5::get_offset_of_coord_1(),
	Config_tB8033A9496C70BD7A0CBA6D41F8FAC15216135A5::get_offset_of_endomorphism_2(),
	Config_tB8033A9496C70BD7A0CBA6D41F8FAC15216135A5::get_offset_of_multiplier_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5248 = { sizeof (AbstractFpCurve_tC851390204D76E2F3E8CBDC56187D3375F05FF75), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5249 = { sizeof (FpCurve_tD40864B7DC010952C5BB06EC76CE95B5929B5BD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5249[3] = 
{
	FpCurve_tD40864B7DC010952C5BB06EC76CE95B5929B5BD8::get_offset_of_m_q_8(),
	FpCurve_tD40864B7DC010952C5BB06EC76CE95B5929B5BD8::get_offset_of_m_r_9(),
	FpCurve_tD40864B7DC010952C5BB06EC76CE95B5929B5BD8::get_offset_of_m_infinity_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5250 = { sizeof (AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5250[1] = 
{
	AbstractF2mCurve_t4AA98C03B9F426713CE47E921AA07B9623F34391::get_offset_of_si_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5251 = { sizeof (F2mCurve_tC80A1D59D2E853794A1538482EE94A7BE489B45D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5251[5] = 
{
	F2mCurve_tC80A1D59D2E853794A1538482EE94A7BE489B45D::get_offset_of_m_9(),
	F2mCurve_tC80A1D59D2E853794A1538482EE94A7BE489B45D::get_offset_of_k1_10(),
	F2mCurve_tC80A1D59D2E853794A1538482EE94A7BE489B45D::get_offset_of_k2_11(),
	F2mCurve_tC80A1D59D2E853794A1538482EE94A7BE489B45D::get_offset_of_k3_12(),
	F2mCurve_tC80A1D59D2E853794A1538482EE94A7BE489B45D::get_offset_of_m_infinity_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5252 = { sizeof (ECFieldElement_tE2FF8892A60D0ACBC3FB665E41B0DEAA0308514C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5253 = { sizeof (FpFieldElement_t0CC160EA07417AF2B1DDB358F9A3A896C84D1265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5253[3] = 
{
	FpFieldElement_t0CC160EA07417AF2B1DDB358F9A3A896C84D1265::get_offset_of_q_0(),
	FpFieldElement_t0CC160EA07417AF2B1DDB358F9A3A896C84D1265::get_offset_of_r_1(),
	FpFieldElement_t0CC160EA07417AF2B1DDB358F9A3A896C84D1265::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5254 = { sizeof (F2mFieldElement_t98FE446EDD24A224827EE617D105A10E70A3B036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5254[4] = 
{
	F2mFieldElement_t98FE446EDD24A224827EE617D105A10E70A3B036::get_offset_of_representation_0(),
	F2mFieldElement_t98FE446EDD24A224827EE617D105A10E70A3B036::get_offset_of_m_1(),
	F2mFieldElement_t98FE446EDD24A224827EE617D105A10E70A3B036::get_offset_of_ks_2(),
	F2mFieldElement_t98FE446EDD24A224827EE617D105A10E70A3B036::get_offset_of_x_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5255 = { sizeof (ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399), -1, sizeof(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5255[7] = 
{
	ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399_StaticFields::get_offset_of_EMPTY_ZS_0(),
	ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399::get_offset_of_m_curve_1(),
	ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399::get_offset_of_m_x_2(),
	ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399::get_offset_of_m_y_3(),
	ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399::get_offset_of_m_zs_4(),
	ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399::get_offset_of_m_withCompression_5(),
	ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399::get_offset_of_m_preCompTable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5256 = { sizeof (ECPointBase_t7EE8D65EFE4A2748B20490039B5F88DFD8D8BF73), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5257 = { sizeof (AbstractFpPoint_t3AC6C98A238F7A4B7FFAE487A33FCB48BCE6C0F7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5258 = { sizeof (FpPoint_t7EA8AC711BD693B5C8A3B62DCFB98976FDC9779B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5259 = { sizeof (AbstractF2mPoint_t31951E0CF9009420711A3FF51DE270702A830FAD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5260 = { sizeof (F2mPoint_t7E9FECF441D8AA8E095B486D519BDBD04B0D66B7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5261 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5262 = { sizeof (LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96), -1, sizeof(LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5262[7] = 
{
	LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields::get_offset_of_INTERLEAVE2_TABLE_0(),
	LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields::get_offset_of_INTERLEAVE3_TABLE_1(),
	LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields::get_offset_of_INTERLEAVE4_TABLE_2(),
	LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields::get_offset_of_INTERLEAVE5_TABLE_3(),
	LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields::get_offset_of_INTERLEAVE7_TABLE_4(),
	LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96_StaticFields::get_offset_of_BitLengths_5(),
	LongArray_t360A75AF402B702E603C322FABD46E76E1D20C96::get_offset_of_m_ints_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5263 = { sizeof (ScaleXPointMap_tA757D388174A99CA7F87D2E553FBBF056B053EE9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5263[1] = 
{
	ScaleXPointMap_tA757D388174A99CA7F87D2E553FBBF056B053EE9::get_offset_of_scale_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5264 = { sizeof (SimpleBigDecimal_t5F847D702AAC67554EB95938A6D0B7896F9AE820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5264[2] = 
{
	SimpleBigDecimal_t5F847D702AAC67554EB95938A6D0B7896F9AE820::get_offset_of_bigInt_0(),
	SimpleBigDecimal_t5F847D702AAC67554EB95938A6D0B7896F9AE820::get_offset_of_scale_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5265 = { sizeof (Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2), -1, sizeof(Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5265[8] = 
{
	Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields::get_offset_of_MinusOne_0(),
	Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields::get_offset_of_MinusTwo_1(),
	Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields::get_offset_of_MinusThree_2(),
	Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields::get_offset_of_Four_3(),
	Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields::get_offset_of_Alpha0_4(),
	Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields::get_offset_of_Alpha0Tnaf_5(),
	Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields::get_offset_of_Alpha1_6(),
	Tnaf_t168C52378E08CDA5C228B34F24B29B80C373A2A2_StaticFields::get_offset_of_Alpha1Tnaf_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5266 = { sizeof (ZTauElement_tE4CF2031E00A980402C6ADDE27125B185259C5E2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5266[2] = 
{
	ZTauElement_tE4CF2031E00A980402C6ADDE27125B185259C5E2::get_offset_of_u_0(),
	ZTauElement_tE4CF2031E00A980402C6ADDE27125B185259C5E2::get_offset_of_v_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5267 = { sizeof (Curve25519_t3DA44F98AF086280BCBED992B1766AFAA24BCEE7), -1, sizeof(Curve25519_t3DA44F98AF086280BCBED992B1766AFAA24BCEE7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5267[2] = 
{
	Curve25519_t3DA44F98AF086280BCBED992B1766AFAA24BCEE7_StaticFields::get_offset_of_q_8(),
	Curve25519_t3DA44F98AF086280BCBED992B1766AFAA24BCEE7::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5268 = { sizeof (Curve25519Field_t6CDE8842C1F9F85D0FABBD58E1DEB639AE25BB2D), -1, sizeof(Curve25519Field_t6CDE8842C1F9F85D0FABBD58E1DEB639AE25BB2D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5268[2] = 
{
	Curve25519Field_t6CDE8842C1F9F85D0FABBD58E1DEB639AE25BB2D_StaticFields::get_offset_of_P_0(),
	Curve25519Field_t6CDE8842C1F9F85D0FABBD58E1DEB639AE25BB2D_StaticFields::get_offset_of_PExt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5269 = { sizeof (Curve25519FieldElement_tCF4CD14C737564C90BA315743AEF0A58F4EC428F), -1, sizeof(Curve25519FieldElement_tCF4CD14C737564C90BA315743AEF0A58F4EC428F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5269[3] = 
{
	Curve25519FieldElement_tCF4CD14C737564C90BA315743AEF0A58F4EC428F_StaticFields::get_offset_of_Q_0(),
	Curve25519FieldElement_tCF4CD14C737564C90BA315743AEF0A58F4EC428F_StaticFields::get_offset_of_PRECOMP_POW2_1(),
	Curve25519FieldElement_tCF4CD14C737564C90BA315743AEF0A58F4EC428F::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5270 = { sizeof (Curve25519Point_tB1B0391C34923E88E9E7E2B1A69BC886F173954B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5271 = { sizeof (SecP128R1Curve_tDC61BFFD5B2994100AA197B2216DCC3D030123A1), -1, sizeof(SecP128R1Curve_tDC61BFFD5B2994100AA197B2216DCC3D030123A1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5271[2] = 
{
	SecP128R1Curve_tDC61BFFD5B2994100AA197B2216DCC3D030123A1_StaticFields::get_offset_of_q_8(),
	SecP128R1Curve_tDC61BFFD5B2994100AA197B2216DCC3D030123A1::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5272 = { sizeof (SecP128R1Field_t9194B57C204A7DFD50F6D4A8C7C26ACFAE94FD78), -1, sizeof(SecP128R1Field_t9194B57C204A7DFD50F6D4A8C7C26ACFAE94FD78_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5272[3] = 
{
	SecP128R1Field_t9194B57C204A7DFD50F6D4A8C7C26ACFAE94FD78_StaticFields::get_offset_of_P_0(),
	SecP128R1Field_t9194B57C204A7DFD50F6D4A8C7C26ACFAE94FD78_StaticFields::get_offset_of_PExt_1(),
	SecP128R1Field_t9194B57C204A7DFD50F6D4A8C7C26ACFAE94FD78_StaticFields::get_offset_of_PExtInv_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5273 = { sizeof (SecP128R1FieldElement_t5E5D4A5F2D02945E83ABB85D70E668F09405FA15), -1, sizeof(SecP128R1FieldElement_t5E5D4A5F2D02945E83ABB85D70E668F09405FA15_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5273[2] = 
{
	SecP128R1FieldElement_t5E5D4A5F2D02945E83ABB85D70E668F09405FA15_StaticFields::get_offset_of_Q_0(),
	SecP128R1FieldElement_t5E5D4A5F2D02945E83ABB85D70E668F09405FA15::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5274 = { sizeof (SecP128R1Point_t80EF8EC90E8CF9D611D823201274CF9C28D77772), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5275 = { sizeof (SecP160K1Curve_t8623BE417DD7CFBAC8D4B807AE0920AD85920EF0), -1, sizeof(SecP160K1Curve_t8623BE417DD7CFBAC8D4B807AE0920AD85920EF0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5275[2] = 
{
	SecP160K1Curve_t8623BE417DD7CFBAC8D4B807AE0920AD85920EF0_StaticFields::get_offset_of_q_8(),
	SecP160K1Curve_t8623BE417DD7CFBAC8D4B807AE0920AD85920EF0::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5276 = { sizeof (SecP160K1Point_tE15730C753F99380C34AD223AA068D1C959F8F86), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5277 = { sizeof (SecP160R1Curve_tE0DD248545E06465BA0437A4D00E27F90B55D0C3), -1, sizeof(SecP160R1Curve_tE0DD248545E06465BA0437A4D00E27F90B55D0C3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5277[2] = 
{
	SecP160R1Curve_tE0DD248545E06465BA0437A4D00E27F90B55D0C3_StaticFields::get_offset_of_q_8(),
	SecP160R1Curve_tE0DD248545E06465BA0437A4D00E27F90B55D0C3::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5278 = { sizeof (SecP160R1Field_t71332E2F61AD4355A4FE9C30B60AA30E59ADD5E6), -1, sizeof(SecP160R1Field_t71332E2F61AD4355A4FE9C30B60AA30E59ADD5E6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5278[3] = 
{
	SecP160R1Field_t71332E2F61AD4355A4FE9C30B60AA30E59ADD5E6_StaticFields::get_offset_of_P_0(),
	SecP160R1Field_t71332E2F61AD4355A4FE9C30B60AA30E59ADD5E6_StaticFields::get_offset_of_PExt_1(),
	SecP160R1Field_t71332E2F61AD4355A4FE9C30B60AA30E59ADD5E6_StaticFields::get_offset_of_PExtInv_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5279 = { sizeof (SecP160R1FieldElement_tC55ACA6B93214270A21D765D7DB22E3A6C3D162E), -1, sizeof(SecP160R1FieldElement_tC55ACA6B93214270A21D765D7DB22E3A6C3D162E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5279[2] = 
{
	SecP160R1FieldElement_tC55ACA6B93214270A21D765D7DB22E3A6C3D162E_StaticFields::get_offset_of_Q_0(),
	SecP160R1FieldElement_tC55ACA6B93214270A21D765D7DB22E3A6C3D162E::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5280 = { sizeof (SecP160R1Point_t9D5C7DB7BC9CF68839652EC9D2DC5D4A95A30943), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5281 = { sizeof (SecP160R2Curve_t3E7212B61C964D6BF25F1CA460CB53F17496B676), -1, sizeof(SecP160R2Curve_t3E7212B61C964D6BF25F1CA460CB53F17496B676_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5281[2] = 
{
	SecP160R2Curve_t3E7212B61C964D6BF25F1CA460CB53F17496B676_StaticFields::get_offset_of_q_8(),
	SecP160R2Curve_t3E7212B61C964D6BF25F1CA460CB53F17496B676::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5282 = { sizeof (SecP160R2Field_t93C679FE3A2AD633DA332D98AA224B68168345A0), -1, sizeof(SecP160R2Field_t93C679FE3A2AD633DA332D98AA224B68168345A0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5282[3] = 
{
	SecP160R2Field_t93C679FE3A2AD633DA332D98AA224B68168345A0_StaticFields::get_offset_of_P_0(),
	SecP160R2Field_t93C679FE3A2AD633DA332D98AA224B68168345A0_StaticFields::get_offset_of_PExt_1(),
	SecP160R2Field_t93C679FE3A2AD633DA332D98AA224B68168345A0_StaticFields::get_offset_of_PExtInv_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5283 = { sizeof (SecP160R2FieldElement_t6D924740F01EFC8CF4A1E744377337261325B0EE), -1, sizeof(SecP160R2FieldElement_t6D924740F01EFC8CF4A1E744377337261325B0EE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5283[2] = 
{
	SecP160R2FieldElement_t6D924740F01EFC8CF4A1E744377337261325B0EE_StaticFields::get_offset_of_Q_0(),
	SecP160R2FieldElement_t6D924740F01EFC8CF4A1E744377337261325B0EE::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5284 = { sizeof (SecP160R2Point_t5F4EE8D61DD9048C48BD4FDE87BD37C51E6970AF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5285 = { sizeof (SecP192K1Curve_t4D5B599D14DF0FB21F1475B2294E88633F258162), -1, sizeof(SecP192K1Curve_t4D5B599D14DF0FB21F1475B2294E88633F258162_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5285[2] = 
{
	SecP192K1Curve_t4D5B599D14DF0FB21F1475B2294E88633F258162_StaticFields::get_offset_of_q_8(),
	SecP192K1Curve_t4D5B599D14DF0FB21F1475B2294E88633F258162::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5286 = { sizeof (SecP192K1Field_tE5705643F2E91AD17AD18862A6F74574451B33AD), -1, sizeof(SecP192K1Field_tE5705643F2E91AD17AD18862A6F74574451B33AD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5286[3] = 
{
	SecP192K1Field_tE5705643F2E91AD17AD18862A6F74574451B33AD_StaticFields::get_offset_of_P_0(),
	SecP192K1Field_tE5705643F2E91AD17AD18862A6F74574451B33AD_StaticFields::get_offset_of_PExt_1(),
	SecP192K1Field_tE5705643F2E91AD17AD18862A6F74574451B33AD_StaticFields::get_offset_of_PExtInv_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5287 = { sizeof (SecP192K1FieldElement_t39F77052EFE0260D066A58E53DFAC80E8D70FF94), -1, sizeof(SecP192K1FieldElement_t39F77052EFE0260D066A58E53DFAC80E8D70FF94_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5287[2] = 
{
	SecP192K1FieldElement_t39F77052EFE0260D066A58E53DFAC80E8D70FF94_StaticFields::get_offset_of_Q_0(),
	SecP192K1FieldElement_t39F77052EFE0260D066A58E53DFAC80E8D70FF94::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5288 = { sizeof (SecP192K1Point_tA3A3258416DE02E73000D80D445C6627A91A4B77), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5289 = { sizeof (SecP192R1Curve_tAE78F5E00B3DF9014039D27F5C79E29FAD7B34A6), -1, sizeof(SecP192R1Curve_tAE78F5E00B3DF9014039D27F5C79E29FAD7B34A6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5289[2] = 
{
	SecP192R1Curve_tAE78F5E00B3DF9014039D27F5C79E29FAD7B34A6_StaticFields::get_offset_of_q_8(),
	SecP192R1Curve_tAE78F5E00B3DF9014039D27F5C79E29FAD7B34A6::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5290 = { sizeof (SecP192R1Field_tA0FFED9B79046F48E1EAA8C38612DDEAC1A8DD4A), -1, sizeof(SecP192R1Field_tA0FFED9B79046F48E1EAA8C38612DDEAC1A8DD4A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5290[3] = 
{
	SecP192R1Field_tA0FFED9B79046F48E1EAA8C38612DDEAC1A8DD4A_StaticFields::get_offset_of_P_0(),
	SecP192R1Field_tA0FFED9B79046F48E1EAA8C38612DDEAC1A8DD4A_StaticFields::get_offset_of_PExt_1(),
	SecP192R1Field_tA0FFED9B79046F48E1EAA8C38612DDEAC1A8DD4A_StaticFields::get_offset_of_PExtInv_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5291 = { sizeof (SecP192R1FieldElement_tBA727BC9C951D1CEEAB8D3A1F0D41F45D31F56DA), -1, sizeof(SecP192R1FieldElement_tBA727BC9C951D1CEEAB8D3A1F0D41F45D31F56DA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5291[2] = 
{
	SecP192R1FieldElement_tBA727BC9C951D1CEEAB8D3A1F0D41F45D31F56DA_StaticFields::get_offset_of_Q_0(),
	SecP192R1FieldElement_tBA727BC9C951D1CEEAB8D3A1F0D41F45D31F56DA::get_offset_of_x_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5292 = { sizeof (SecP192R1Point_tF263B963714C357592112834E6767394E3560E9E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5293 = { sizeof (SecP224K1Curve_tCDDE14B9160269BD161F84E662BD2E0FBF1BC301), -1, sizeof(SecP224K1Curve_tCDDE14B9160269BD161F84E662BD2E0FBF1BC301_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5293[2] = 
{
	SecP224K1Curve_tCDDE14B9160269BD161F84E662BD2E0FBF1BC301_StaticFields::get_offset_of_q_8(),
	SecP224K1Curve_tCDDE14B9160269BD161F84E662BD2E0FBF1BC301::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5294 = { sizeof (SecP224K1Field_tE2EF028B701BDD8E82D19B9EB6ADB663037507FF), -1, sizeof(SecP224K1Field_tE2EF028B701BDD8E82D19B9EB6ADB663037507FF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5294[3] = 
{
	SecP224K1Field_tE2EF028B701BDD8E82D19B9EB6ADB663037507FF_StaticFields::get_offset_of_P_0(),
	SecP224K1Field_tE2EF028B701BDD8E82D19B9EB6ADB663037507FF_StaticFields::get_offset_of_PExt_1(),
	SecP224K1Field_tE2EF028B701BDD8E82D19B9EB6ADB663037507FF_StaticFields::get_offset_of_PExtInv_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5295 = { sizeof (SecP224K1FieldElement_t262966EDAD2A42C7546FB344398250AEFAAC204C), -1, sizeof(SecP224K1FieldElement_t262966EDAD2A42C7546FB344398250AEFAAC204C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5295[3] = 
{
	SecP224K1FieldElement_t262966EDAD2A42C7546FB344398250AEFAAC204C_StaticFields::get_offset_of_Q_0(),
	SecP224K1FieldElement_t262966EDAD2A42C7546FB344398250AEFAAC204C_StaticFields::get_offset_of_PRECOMP_POW2_1(),
	SecP224K1FieldElement_t262966EDAD2A42C7546FB344398250AEFAAC204C::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5296 = { sizeof (SecP224K1Point_t6DCA6A3B396FB10CA6D991B1354C748BC4F9235F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5297 = { sizeof (SecP224R1Curve_t80302359431CCFEA4889C230F76C674D45800F31), -1, sizeof(SecP224R1Curve_t80302359431CCFEA4889C230F76C674D45800F31_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5297[2] = 
{
	SecP224R1Curve_t80302359431CCFEA4889C230F76C674D45800F31_StaticFields::get_offset_of_q_8(),
	SecP224R1Curve_t80302359431CCFEA4889C230F76C674D45800F31::get_offset_of_m_infinity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5298 = { sizeof (SecP224R1Field_t095BB2376F5A8235BDD20A204A8378F790BB7B14), -1, sizeof(SecP224R1Field_t095BB2376F5A8235BDD20A204A8378F790BB7B14_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5298[3] = 
{
	SecP224R1Field_t095BB2376F5A8235BDD20A204A8378F790BB7B14_StaticFields::get_offset_of_P_0(),
	SecP224R1Field_t095BB2376F5A8235BDD20A204A8378F790BB7B14_StaticFields::get_offset_of_PExt_1(),
	SecP224R1Field_t095BB2376F5A8235BDD20A204A8378F790BB7B14_StaticFields::get_offset_of_PExtInv_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5299 = { sizeof (SecP224R1FieldElement_t4EEC5D62E1475AE516D67CD6F141AB0068ED6CD2), -1, sizeof(SecP224R1FieldElement_t4EEC5D62E1475AE516D67CD6F141AB0068ED6CD2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5299[2] = 
{
	SecP224R1FieldElement_t4EEC5D62E1475AE516D67CD6F141AB0068ED6CD2_StaticFields::get_offset_of_Q_0(),
	SecP224R1FieldElement_t4EEC5D62E1475AE516D67CD6F141AB0068ED6CD2::get_offset_of_x_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
