﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BattleSettingsSO
struct BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE;
// ChaptersSO
struct ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1;
// DesperateDevs.Utils.ObjectPool`1<System.Collections.Generic.List`1<Entitas.GroupChanged`1<GameEntity>>>
struct ObjectPool_1_tB799227ADF820B9D3BB3FF02CC0DEAFA4C317463;
// Entitas.ContextEntityChanged
struct ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1;
// Entitas.ContextGroupChanged
struct ContextGroupChanged_tDF404FDADBE283C90826E021920179C9C5986728;
// Entitas.ContextInfo
struct ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED;
// Entitas.EntityComponentChanged
struct EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A;
// Entitas.EntityComponentReplaced
struct EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57;
// Entitas.EntityEvent
struct EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6;
// Entitas.ICollector`1<GameEntity>
struct ICollector_1_tDA9721194F1438993CA7EC5D39C69B898FBBB26A;
// Entitas.IGroup`1<GameEntity>
struct IGroup_1_tF4940889845236B5907C50AD6A40AD6CE69EC680;
// GameContext
struct GameContext_t7DA531FCF38581666B3642E3E737E5B057494978;
// GameEntity[]
struct GameEntityU5BU5D_tDC5EC08114DD197DC35905B963D07B3E073F05E3;
// ItemsSO
struct ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A;
// SpellsSO
struct SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Entitas.IMatcher`1<GameEntity>,Entitas.IGroup`1<GameEntity>>
struct Dictionary_2_t50A00884B3EAE184A96FD38FB9C4F54D823C395D;
// System.Collections.Generic.Dictionary`2<System.String,Entitas.IEntityIndex>
struct Dictionary_2_t8C31B2198A4B55E3CD0B09E8EFCB67CC4D283E0C;
// System.Collections.Generic.Dictionary`2<System.String,GameEntity>
struct Dictionary_2_t70CAAE41AA21DBE49EE58820FA11623347BDFA5D;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.HashSet`1<GameEntity>>
struct Dictionary_2_t8444F0CD0B98C58A471C68AF61E67B249C17DAE4;
// System.Collections.Generic.HashSet`1<GameEntity>
struct HashSet_1_t063A3379F7914EEA90E4FE6E7CA418473331A29C;
// System.Collections.Generic.List`1<EffectModel>
struct List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB;
// System.Collections.Generic.List`1<Entitas.IGroup`1<GameEntity>>[]
struct List_1U5BU5D_tE003DE61ED7636D16B6D9BA21BDBCDFB723F5A92;
// System.Collections.Generic.List`1<GameEntity>
struct List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.Stack`1<Entitas.IComponent>[]
struct Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272;
// System.Collections.Generic.Stack`1<GameEntity>
struct Stack_1_t0F7CA9F21180ACC53849C63DB13E6B0D29C78AE6;
// System.Func`1<GameEntity>
struct Func_1_t80B4C2C3FEA10713ACD97FF7EB221B48BF634166;
// System.Func`2<Entitas.IEntity,Entitas.IAERC>
struct Func_2_t17A06C13140486C05B62E73F4900E90CD6CD5172;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,ItemGenerationModel>,System.Int32>
struct Func_2_t4B9FEEC317C915D3F80083FD7B83B6071B64860D;
// System.Func`3<GameEntity,Entitas.IComponent,System.String>
struct Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1;
// System.Func`3<GameEntity,Entitas.IComponent,System.String[]>
struct Func_3_tC179A8DB3DC1F41CFC7E52AB5999F0F276F0AF05;
// System.Random
struct Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F;
// System.String
struct String_t;
// Tayr.ILibrary
struct ILibrary_tCBD3DE1F66AD92DAB1112B8018EBA4A4BD23767C;
// UnitsSO
struct UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UserVO
struct UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94;
// Zenject.DiContainer
struct DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef AGGROCOMPONENT_TC4E7DDFC08A5DBA09324B31CADF7056BB194AD70_H
#define AGGROCOMPONENT_TC4E7DDFC08A5DBA09324B31CADF7056BB194AD70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AggroComponent
struct  AggroComponent_tC4E7DDFC08A5DBA09324B31CADF7056BB194AD70  : public RuntimeObject
{
public:
	// System.String AggroComponent::AggroSource
	String_t* ___AggroSource_0;
	// System.String AggroComponent::AggroTarget
	String_t* ___AggroTarget_1;

public:
	inline static int32_t get_offset_of_AggroSource_0() { return static_cast<int32_t>(offsetof(AggroComponent_tC4E7DDFC08A5DBA09324B31CADF7056BB194AD70, ___AggroSource_0)); }
	inline String_t* get_AggroSource_0() const { return ___AggroSource_0; }
	inline String_t** get_address_of_AggroSource_0() { return &___AggroSource_0; }
	inline void set_AggroSource_0(String_t* value)
	{
		___AggroSource_0 = value;
		Il2CppCodeGenWriteBarrier((&___AggroSource_0), value);
	}

	inline static int32_t get_offset_of_AggroTarget_1() { return static_cast<int32_t>(offsetof(AggroComponent_tC4E7DDFC08A5DBA09324B31CADF7056BB194AD70, ___AggroTarget_1)); }
	inline String_t* get_AggroTarget_1() const { return ___AggroTarget_1; }
	inline String_t** get_address_of_AggroTarget_1() { return &___AggroTarget_1; }
	inline void set_AggroTarget_1(String_t* value)
	{
		___AggroTarget_1 = value;
		Il2CppCodeGenWriteBarrier((&___AggroTarget_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGROCOMPONENT_TC4E7DDFC08A5DBA09324B31CADF7056BB194AD70_H
#ifndef AGGRODISABLEDCOMPONENT_T7D0F3FF57D659CAF0C81F2D14851E44388B01278_H
#define AGGRODISABLEDCOMPONENT_T7D0F3FF57D659CAF0C81F2D14851E44388B01278_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AggroDisabledComponent
struct  AggroDisabledComponent_t7D0F3FF57D659CAF0C81F2D14851E44388B01278  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGRODISABLEDCOMPONENT_T7D0F3FF57D659CAF0C81F2D14851E44388B01278_H
#ifndef U3CU3EC_T73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836_H
#define U3CU3EC_T73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AggroEntityIndex_<>c
struct  U3CU3Ec_t73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836_StaticFields
{
public:
	// AggroEntityIndex_<>c AggroEntityIndex_<>c::<>9
	U3CU3Ec_t73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836 * ___U3CU3E9_0;
	// System.Func`3<GameEntity,Entitas.IComponent,System.String> AggroEntityIndex_<>c::<>9__0_0
	Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836_H
#ifndef AGGROHEIGHTCOMPONENT_T7E39BE68860E4F396567CC6F766CE6A365970EE9_H
#define AGGROHEIGHTCOMPONENT_T7E39BE68860E4F396567CC6F766CE6A365970EE9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AggroHeightComponent
struct  AggroHeightComponent_t7E39BE68860E4F396567CC6F766CE6A365970EE9  : public RuntimeObject
{
public:
	// System.Single AggroHeightComponent::Height
	float ___Height_0;

public:
	inline static int32_t get_offset_of_Height_0() { return static_cast<int32_t>(offsetof(AggroHeightComponent_t7E39BE68860E4F396567CC6F766CE6A365970EE9, ___Height_0)); }
	inline float get_Height_0() const { return ___Height_0; }
	inline float* get_address_of_Height_0() { return &___Height_0; }
	inline void set_Height_0(float value)
	{
		___Height_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGROHEIGHTCOMPONENT_T7E39BE68860E4F396567CC6F766CE6A365970EE9_H
#ifndef AGGRORANGECOMPONENT_T1234630F54F9674A43A4CEBC601AAAAFDF5855BD_H
#define AGGRORANGECOMPONENT_T1234630F54F9674A43A4CEBC601AAAAFDF5855BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AggroRangeComponent
struct  AggroRangeComponent_t1234630F54F9674A43A4CEBC601AAAAFDF5855BD  : public RuntimeObject
{
public:
	// System.Single AggroRangeComponent::Range
	float ___Range_0;

public:
	inline static int32_t get_offset_of_Range_0() { return static_cast<int32_t>(offsetof(AggroRangeComponent_t1234630F54F9674A43A4CEBC601AAAAFDF5855BD, ___Range_0)); }
	inline float get_Range_0() const { return ___Range_0; }
	inline float* get_address_of_Range_0() { return &___Range_0; }
	inline void set_Range_0(float value)
	{
		___Range_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGRORANGECOMPONENT_T1234630F54F9674A43A4CEBC601AAAAFDF5855BD_H
#ifndef AGGROSYSTEM_TE4911ABA2708115BE8340DE3610B2FD7739D257F_H
#define AGGROSYSTEM_TE4911ABA2708115BE8340DE3610B2FD7739D257F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AggroSystem
struct  AggroSystem_tE4911ABA2708115BE8340DE3610B2FD7739D257F  : public RuntimeObject
{
public:
	// Entitas.IGroup`1<GameEntity> AggroSystem::_entities
	RuntimeObject* ____entities_0;
	// GameContext AggroSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_1;

public:
	inline static int32_t get_offset_of__entities_0() { return static_cast<int32_t>(offsetof(AggroSystem_tE4911ABA2708115BE8340DE3610B2FD7739D257F, ____entities_0)); }
	inline RuntimeObject* get__entities_0() const { return ____entities_0; }
	inline RuntimeObject** get_address_of__entities_0() { return &____entities_0; }
	inline void set__entities_0(RuntimeObject* value)
	{
		____entities_0 = value;
		Il2CppCodeGenWriteBarrier((&____entities_0), value);
	}

	inline static int32_t get_offset_of__context_1() { return static_cast<int32_t>(offsetof(AggroSystem_tE4911ABA2708115BE8340DE3610B2FD7739D257F, ____context_1)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_1() const { return ____context_1; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_1() { return &____context_1; }
	inline void set__context_1(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_1 = value;
		Il2CppCodeGenWriteBarrier((&____context_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGROSYSTEM_TE4911ABA2708115BE8340DE3610B2FD7739D257F_H
#ifndef AGGROWIDTHCOMPONENT_TB130BE6C7343E564DD714CD81487E1E22E4F1CF5_H
#define AGGROWIDTHCOMPONENT_TB130BE6C7343E564DD714CD81487E1E22E4F1CF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AggroWidthComponent
struct  AggroWidthComponent_tB130BE6C7343E564DD714CD81487E1E22E4F1CF5  : public RuntimeObject
{
public:
	// System.Single AggroWidthComponent::Width
	float ___Width_0;

public:
	inline static int32_t get_offset_of_Width_0() { return static_cast<int32_t>(offsetof(AggroWidthComponent_tB130BE6C7343E564DD714CD81487E1E22E4F1CF5, ___Width_0)); }
	inline float get_Width_0() const { return ___Width_0; }
	inline float* get_address_of_Width_0() { return &___Width_0; }
	inline void set_Width_0(float value)
	{
		___Width_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGROWIDTHCOMPONENT_TB130BE6C7343E564DD714CD81487E1E22E4F1CF5_H
#ifndef ATTACKBOOSTCOMPONENT_T9F90E34CD2CF7CED312B3080AD0F59822ED6A45D_H
#define ATTACKBOOSTCOMPONENT_T9F90E34CD2CF7CED312B3080AD0F59822ED6A45D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackBoostComponent
struct  AttackBoostComponent_t9F90E34CD2CF7CED312B3080AD0F59822ED6A45D  : public RuntimeObject
{
public:
	// System.Int32 AttackBoostComponent::Percentage
	int32_t ___Percentage_0;

public:
	inline static int32_t get_offset_of_Percentage_0() { return static_cast<int32_t>(offsetof(AttackBoostComponent_t9F90E34CD2CF7CED312B3080AD0F59822ED6A45D, ___Percentage_0)); }
	inline int32_t get_Percentage_0() const { return ___Percentage_0; }
	inline int32_t* get_address_of_Percentage_0() { return &___Percentage_0; }
	inline void set_Percentage_0(int32_t value)
	{
		___Percentage_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACKBOOSTCOMPONENT_T9F90E34CD2CF7CED312B3080AD0F59822ED6A45D_H
#ifndef ATTACKFREQUENCYCOMPONENT_T05854E6C4265683677299D54D58D11DFD00B0615_H
#define ATTACKFREQUENCYCOMPONENT_T05854E6C4265683677299D54D58D11DFD00B0615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackFrequencyComponent
struct  AttackFrequencyComponent_t05854E6C4265683677299D54D58D11DFD00B0615  : public RuntimeObject
{
public:
	// System.Single AttackFrequencyComponent::Frequency
	float ___Frequency_0;

public:
	inline static int32_t get_offset_of_Frequency_0() { return static_cast<int32_t>(offsetof(AttackFrequencyComponent_t05854E6C4265683677299D54D58D11DFD00B0615, ___Frequency_0)); }
	inline float get_Frequency_0() const { return ___Frequency_0; }
	inline float* get_address_of_Frequency_0() { return &___Frequency_0; }
	inline void set_Frequency_0(float value)
	{
		___Frequency_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACKFREQUENCYCOMPONENT_T05854E6C4265683677299D54D58D11DFD00B0615_H
#ifndef BATTLEUTILS_TD58E1F3D1195331D9E9EA6A797CDE15BC69F74FD_H
#define BATTLEUTILS_TD58E1F3D1195331D9E9EA6A797CDE15BC69F74FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleUtils
struct  BattleUtils_tD58E1F3D1195331D9E9EA6A797CDE15BC69F74FD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEUTILS_TD58E1F3D1195331D9E9EA6A797CDE15BC69F74FD_H
#ifndef COLLECTABLECOMPONENT_T0BD7C71F3589B884C11505B8D6D784632FCF1A5A_H
#define COLLECTABLECOMPONENT_T0BD7C71F3589B884C11505B8D6D784632FCF1A5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectableComponent
struct  CollectableComponent_t0BD7C71F3589B884C11505B8D6D784632FCF1A5A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTABLECOMPONENT_T0BD7C71F3589B884C11505B8D6D784632FCF1A5A_H
#ifndef COLLECTEDCOMPONENT_T4D9F276C5E67C78D80D5C0BCC78D160B2BE7E9D1_H
#define COLLECTEDCOMPONENT_T4D9F276C5E67C78D80D5C0BCC78D160B2BE7E9D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectedComponent
struct  CollectedComponent_t4D9F276C5E67C78D80D5C0BCC78D160B2BE7E9D1  : public RuntimeObject
{
public:
	// System.String CollectedComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(CollectedComponent_t4D9F276C5E67C78D80D5C0BCC78D160B2BE7E9D1, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTEDCOMPONENT_T4D9F276C5E67C78D80D5C0BCC78D160B2BE7E9D1_H
#ifndef CREATEAOEEFFECTAFTERTIMESYSTEM_TC9DD91E68CBE6F52E1E80D8C538303F37E238919_H
#define CREATEAOEEFFECTAFTERTIMESYSTEM_TC9DD91E68CBE6F52E1E80D8C538303F37E238919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CreateAoeEffectAfterTimeSystem
struct  CreateAoeEffectAfterTimeSystem_tC9DD91E68CBE6F52E1E80D8C538303F37E238919  : public RuntimeObject
{
public:
	// GameContext CreateAoeEffectAfterTimeSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_0;
	// Entitas.IGroup`1<GameEntity> CreateAoeEffectAfterTimeSystem::_entities
	RuntimeObject* ____entities_1;

public:
	inline static int32_t get_offset_of__context_0() { return static_cast<int32_t>(offsetof(CreateAoeEffectAfterTimeSystem_tC9DD91E68CBE6F52E1E80D8C538303F37E238919, ____context_0)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_0() const { return ____context_0; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_0() { return &____context_0; }
	inline void set__context_0(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_0 = value;
		Il2CppCodeGenWriteBarrier((&____context_0), value);
	}

	inline static int32_t get_offset_of__entities_1() { return static_cast<int32_t>(offsetof(CreateAoeEffectAfterTimeSystem_tC9DD91E68CBE6F52E1E80D8C538303F37E238919, ____entities_1)); }
	inline RuntimeObject* get__entities_1() const { return ____entities_1; }
	inline RuntimeObject** get_address_of__entities_1() { return &____entities_1; }
	inline void set__entities_1(RuntimeObject* value)
	{
		____entities_1 = value;
		Il2CppCodeGenWriteBarrier((&____entities_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATEAOEEFFECTAFTERTIMESYSTEM_TC9DD91E68CBE6F52E1E80D8C538303F37E238919_H
#ifndef DAMAGECOMPONENT_TC393EB56208D744022CC7AA17CD9356F0393BEE1_H
#define DAMAGECOMPONENT_TC393EB56208D744022CC7AA17CD9356F0393BEE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DamageComponent
struct  DamageComponent_tC393EB56208D744022CC7AA17CD9356F0393BEE1  : public RuntimeObject
{
public:
	// System.Int32 DamageComponent::Value
	int32_t ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(DamageComponent_tC393EB56208D744022CC7AA17CD9356F0393BEE1, ___Value_0)); }
	inline int32_t get_Value_0() const { return ___Value_0; }
	inline int32_t* get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(int32_t value)
	{
		___Value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMAGECOMPONENT_TC393EB56208D744022CC7AA17CD9356F0393BEE1_H
#ifndef DAMAGEUTILS_T5E9925494E0670ADB27EDF53BC0AA1CAFEBAB411_H
#define DAMAGEUTILS_T5E9925494E0670ADB27EDF53BC0AA1CAFEBAB411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DamageUtils
struct  DamageUtils_t5E9925494E0670ADB27EDF53BC0AA1CAFEBAB411  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAMAGEUTILS_T5E9925494E0670ADB27EDF53BC0AA1CAFEBAB411_H
#ifndef DESTROYABLECOMPONENT_T0AC1C3F5D08F64BCAB2FE13A8187CD3B4C72C2AC_H
#define DESTROYABLECOMPONENT_T0AC1C3F5D08F64BCAB2FE13A8187CD3B4C72C2AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyableComponent
struct  DestroyableComponent_t0AC1C3F5D08F64BCAB2FE13A8187CD3B4C72C2AC  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYABLECOMPONENT_T0AC1C3F5D08F64BCAB2FE13A8187CD3B4C72C2AC_H
#ifndef DISPOSEAFTERTIMECOMPONENT_TA6153BD55D472EE89FDC5B31D185A5CEC2459918_H
#define DISPOSEAFTERTIMECOMPONENT_TA6153BD55D472EE89FDC5B31D185A5CEC2459918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisposeAfterTimeComponent
struct  DisposeAfterTimeComponent_tA6153BD55D472EE89FDC5B31D185A5CEC2459918  : public RuntimeObject
{
public:
	// System.Single DisposeAfterTimeComponent::DisposeTime
	float ___DisposeTime_0;

public:
	inline static int32_t get_offset_of_DisposeTime_0() { return static_cast<int32_t>(offsetof(DisposeAfterTimeComponent_tA6153BD55D472EE89FDC5B31D185A5CEC2459918, ___DisposeTime_0)); }
	inline float get_DisposeTime_0() const { return ___DisposeTime_0; }
	inline float* get_address_of_DisposeTime_0() { return &___DisposeTime_0; }
	inline void set_DisposeTime_0(float value)
	{
		___DisposeTime_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPOSEAFTERTIMECOMPONENT_TA6153BD55D472EE89FDC5B31D185A5CEC2459918_H
#ifndef DISPOSEAFTERTIMESYSTEM_T285CCA2194110C702896043B2A01E7EA07C57449_H
#define DISPOSEAFTERTIMESYSTEM_T285CCA2194110C702896043B2A01E7EA07C57449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisposeAfterTimeSystem
struct  DisposeAfterTimeSystem_t285CCA2194110C702896043B2A01E7EA07C57449  : public RuntimeObject
{
public:
	// GameContext DisposeAfterTimeSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_0;
	// Entitas.IGroup`1<GameEntity> DisposeAfterTimeSystem::_group
	RuntimeObject* ____group_1;

public:
	inline static int32_t get_offset_of__context_0() { return static_cast<int32_t>(offsetof(DisposeAfterTimeSystem_t285CCA2194110C702896043B2A01E7EA07C57449, ____context_0)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_0() const { return ____context_0; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_0() { return &____context_0; }
	inline void set__context_0(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_0 = value;
		Il2CppCodeGenWriteBarrier((&____context_0), value);
	}

	inline static int32_t get_offset_of__group_1() { return static_cast<int32_t>(offsetof(DisposeAfterTimeSystem_t285CCA2194110C702896043B2A01E7EA07C57449, ____group_1)); }
	inline RuntimeObject* get__group_1() const { return ____group_1; }
	inline RuntimeObject** get_address_of__group_1() { return &____group_1; }
	inline void set__group_1(RuntimeObject* value)
	{
		____group_1 = value;
		Il2CppCodeGenWriteBarrier((&____group_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPOSEAFTERTIMESYSTEM_T285CCA2194110C702896043B2A01E7EA07C57449_H
#ifndef DISPOSECOMPONENT_T9E94EC36DF1BD2E19774C102A3F0D68C63D09B4E_H
#define DISPOSECOMPONENT_T9E94EC36DF1BD2E19774C102A3F0D68C63D09B4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisposeComponent
struct  DisposeComponent_t9E94EC36DF1BD2E19774C102A3F0D68C63D09B4E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPOSECOMPONENT_T9E94EC36DF1BD2E19774C102A3F0D68C63D09B4E_H
#ifndef DISPOSESYSTEM_T664F1BB636A63202F0F23F228A16DCEDDDCE4396_H
#define DISPOSESYSTEM_T664F1BB636A63202F0F23F228A16DCEDDDCE4396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DisposeSystem
struct  DisposeSystem_t664F1BB636A63202F0F23F228A16DCEDDDCE4396  : public RuntimeObject
{
public:
	// Entitas.IGroup`1<GameEntity> DisposeSystem::_entities
	RuntimeObject* ____entities_0;
	// GameContext DisposeSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_1;

public:
	inline static int32_t get_offset_of__entities_0() { return static_cast<int32_t>(offsetof(DisposeSystem_t664F1BB636A63202F0F23F228A16DCEDDDCE4396, ____entities_0)); }
	inline RuntimeObject* get__entities_0() const { return ____entities_0; }
	inline RuntimeObject** get_address_of__entities_0() { return &____entities_0; }
	inline void set__entities_0(RuntimeObject* value)
	{
		____entities_0 = value;
		Il2CppCodeGenWriteBarrier((&____entities_0), value);
	}

	inline static int32_t get_offset_of__context_1() { return static_cast<int32_t>(offsetof(DisposeSystem_t664F1BB636A63202F0F23F228A16DCEDDDCE4396, ____context_1)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_1() const { return ____context_1; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_1() { return &____context_1; }
	inline void set__context_1(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_1 = value;
		Il2CppCodeGenWriteBarrier((&____context_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPOSESYSTEM_T664F1BB636A63202F0F23F228A16DCEDDDCE4396_H
#ifndef DROPCOLLECTABLE_TCF8D31AC9C54CDE3184EC34CC9902FC39950B108_H
#define DROPCOLLECTABLE_TCF8D31AC9C54CDE3184EC34CC9902FC39950B108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DropCollectable
struct  DropCollectable_tCF8D31AC9C54CDE3184EC34CC9902FC39950B108  : public RuntimeObject
{
public:
	// System.String DropCollectable::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(DropCollectable_tCF8D31AC9C54CDE3184EC34CC9902FC39950B108, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPCOLLECTABLE_TCF8D31AC9C54CDE3184EC34CC9902FC39950B108_H
#ifndef DROPCOLLECTABLESYSTEM_T97A7F164425D8F02D672597EE5D05D82DC294592_H
#define DROPCOLLECTABLESYSTEM_T97A7F164425D8F02D672597EE5D05D82DC294592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DropCollectableSystem
struct  DropCollectableSystem_t97A7F164425D8F02D672597EE5D05D82DC294592  : public RuntimeObject
{
public:
	// GameContext DropCollectableSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_0;

public:
	inline static int32_t get_offset_of__context_0() { return static_cast<int32_t>(offsetof(DropCollectableSystem_t97A7F164425D8F02D672597EE5D05D82DC294592, ____context_0)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_0() const { return ____context_0; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_0() { return &____context_0; }
	inline void set__context_0(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_0 = value;
		Il2CppCodeGenWriteBarrier((&____context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DROPCOLLECTABLESYSTEM_T97A7F164425D8F02D672597EE5D05D82DC294592_H
#ifndef DURATIONCOMPONENT_T5EA49F82756EB70253987896EEBB49DEFCF17BA8_H
#define DURATIONCOMPONENT_T5EA49F82756EB70253987896EEBB49DEFCF17BA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DurationComponent
struct  DurationComponent_t5EA49F82756EB70253987896EEBB49DEFCF17BA8  : public RuntimeObject
{
public:
	// System.Single DurationComponent::Duration
	float ___Duration_0;

public:
	inline static int32_t get_offset_of_Duration_0() { return static_cast<int32_t>(offsetof(DurationComponent_t5EA49F82756EB70253987896EEBB49DEFCF17BA8, ___Duration_0)); }
	inline float get_Duration_0() const { return ___Duration_0; }
	inline float* get_address_of_Duration_0() { return &___Duration_0; }
	inline void set_Duration_0(float value)
	{
		___Duration_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DURATIONCOMPONENT_T5EA49F82756EB70253987896EEBB49DEFCF17BA8_H
#ifndef U3CU3EC_T4F94D0E01716A541C3D9FD34514A044B2B842AF7_H
#define U3CU3EC_T4F94D0E01716A541C3D9FD34514A044B2B842AF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectEntityIndex_<>c
struct  U3CU3Ec_t4F94D0E01716A541C3D9FD34514A044B2B842AF7  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4F94D0E01716A541C3D9FD34514A044B2B842AF7_StaticFields
{
public:
	// EffectEntityIndex_<>c EffectEntityIndex_<>c::<>9
	U3CU3Ec_t4F94D0E01716A541C3D9FD34514A044B2B842AF7 * ___U3CU3E9_0;
	// System.Func`3<GameEntity,Entitas.IComponent,System.String> EffectEntityIndex_<>c::<>9__0_0
	Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4F94D0E01716A541C3D9FD34514A044B2B842AF7_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4F94D0E01716A541C3D9FD34514A044B2B842AF7 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4F94D0E01716A541C3D9FD34514A044B2B842AF7 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4F94D0E01716A541C3D9FD34514A044B2B842AF7 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4F94D0E01716A541C3D9FD34514A044B2B842AF7_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4F94D0E01716A541C3D9FD34514A044B2B842AF7_H
#ifndef EFFECTSOURCECOMPONENT_T9B444B16C43EE277876EECF4B4A22EEF194D7926_H
#define EFFECTSOURCECOMPONENT_T9B444B16C43EE277876EECF4B4A22EEF194D7926_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectSourceComponent
struct  EffectSourceComponent_t9B444B16C43EE277876EECF4B4A22EEF194D7926  : public RuntimeObject
{
public:
	// System.String EffectSourceComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(EffectSourceComponent_t9B444B16C43EE277876EECF4B4A22EEF194D7926, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTSOURCECOMPONENT_T9B444B16C43EE277876EECF4B4A22EEF194D7926_H
#ifndef EFFECTSTARTTIMECOMPONENT_TE0A38FBD16E344E9F0413E139A2FD73B3AABE953_H
#define EFFECTSTARTTIMECOMPONENT_TE0A38FBD16E344E9F0413E139A2FD73B3AABE953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectStartTimeComponent
struct  EffectStartTimeComponent_tE0A38FBD16E344E9F0413E139A2FD73B3AABE953  : public RuntimeObject
{
public:
	// System.Single EffectStartTimeComponent::StartTime
	float ___StartTime_0;

public:
	inline static int32_t get_offset_of_StartTime_0() { return static_cast<int32_t>(offsetof(EffectStartTimeComponent_tE0A38FBD16E344E9F0413E139A2FD73B3AABE953, ___StartTime_0)); }
	inline float get_StartTime_0() const { return ___StartTime_0; }
	inline float* get_address_of_StartTime_0() { return &___StartTime_0; }
	inline void set_StartTime_0(float value)
	{
		___StartTime_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTSTARTTIMECOMPONENT_TE0A38FBD16E344E9F0413E139A2FD73B3AABE953_H
#ifndef EFFECTSTARTEDCOMPONENT_TA442FC7E8A7696782A04C2786BEBE9EF91669E4A_H
#define EFFECTSTARTEDCOMPONENT_TA442FC7E8A7696782A04C2786BEBE9EF91669E4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectStartedComponent
struct  EffectStartedComponent_tA442FC7E8A7696782A04C2786BEBE9EF91669E4A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTSTARTEDCOMPONENT_TA442FC7E8A7696782A04C2786BEBE9EF91669E4A_H
#ifndef EFFECTVISUALCOMPONENT_T396C04D6E6EBD847ED82F26CDB95BAFFC23744EE_H
#define EFFECTVISUALCOMPONENT_T396C04D6E6EBD847ED82F26CDB95BAFFC23744EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisualComponent
struct  EffectVisualComponent_t396C04D6E6EBD847ED82F26CDB95BAFFC23744EE  : public RuntimeObject
{
public:
	// System.String EffectVisualComponent::VisualId
	String_t* ___VisualId_0;

public:
	inline static int32_t get_offset_of_VisualId_0() { return static_cast<int32_t>(offsetof(EffectVisualComponent_t396C04D6E6EBD847ED82F26CDB95BAFFC23744EE, ___VisualId_0)); }
	inline String_t* get_VisualId_0() const { return ___VisualId_0; }
	inline String_t** get_address_of_VisualId_0() { return &___VisualId_0; }
	inline void set_VisualId_0(String_t* value)
	{
		___VisualId_0 = value;
		Il2CppCodeGenWriteBarrier((&___VisualId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTVISUALCOMPONENT_T396C04D6E6EBD847ED82F26CDB95BAFFC23744EE_H
#ifndef U3CU3EC_T35ED9DBC98F6098490FE486A7ECDC5175A9E0766_H
#define U3CU3EC_T35ED9DBC98F6098490FE486A7ECDC5175A9E0766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisualEntityIndex_<>c
struct  U3CU3Ec_t35ED9DBC98F6098490FE486A7ECDC5175A9E0766  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t35ED9DBC98F6098490FE486A7ECDC5175A9E0766_StaticFields
{
public:
	// EffectVisualEntityIndex_<>c EffectVisualEntityIndex_<>c::<>9
	U3CU3Ec_t35ED9DBC98F6098490FE486A7ECDC5175A9E0766 * ___U3CU3E9_0;
	// System.Func`3<GameEntity,Entitas.IComponent,System.String> EffectVisualEntityIndex_<>c::<>9__0_0
	Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t35ED9DBC98F6098490FE486A7ECDC5175A9E0766_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t35ED9DBC98F6098490FE486A7ECDC5175A9E0766 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t35ED9DBC98F6098490FE486A7ECDC5175A9E0766 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t35ED9DBC98F6098490FE486A7ECDC5175A9E0766 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t35ED9DBC98F6098490FE486A7ECDC5175A9E0766_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T35ED9DBC98F6098490FE486A7ECDC5175A9E0766_H
#ifndef ABSTRACTENTITYINDEX_2_T21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA_H
#define ABSTRACTENTITYINDEX_2_T21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.AbstractEntityIndex`2<GameEntity,System.String>
struct  AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA  : public RuntimeObject
{
public:
	// System.String Entitas.AbstractEntityIndex`2::_name
	String_t* ____name_0;
	// Entitas.IGroup`1<TEntity> Entitas.AbstractEntityIndex`2::_group
	RuntimeObject* ____group_1;
	// System.Func`3<TEntity,Entitas.IComponent,TKey> Entitas.AbstractEntityIndex`2::_getKey
	Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * ____getKey_2;
	// System.Func`3<TEntity,Entitas.IComponent,TKey[]> Entitas.AbstractEntityIndex`2::_getKeys
	Func_3_tC179A8DB3DC1F41CFC7E52AB5999F0F276F0AF05 * ____getKeys_3;
	// System.Boolean Entitas.AbstractEntityIndex`2::_isSingleKey
	bool ____isSingleKey_4;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__group_1() { return static_cast<int32_t>(offsetof(AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA, ____group_1)); }
	inline RuntimeObject* get__group_1() const { return ____group_1; }
	inline RuntimeObject** get_address_of__group_1() { return &____group_1; }
	inline void set__group_1(RuntimeObject* value)
	{
		____group_1 = value;
		Il2CppCodeGenWriteBarrier((&____group_1), value);
	}

	inline static int32_t get_offset_of__getKey_2() { return static_cast<int32_t>(offsetof(AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA, ____getKey_2)); }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * get__getKey_2() const { return ____getKey_2; }
	inline Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 ** get_address_of__getKey_2() { return &____getKey_2; }
	inline void set__getKey_2(Func_3_tC499949CBF645C3D2C44549A0A930BF6F9485FA1 * value)
	{
		____getKey_2 = value;
		Il2CppCodeGenWriteBarrier((&____getKey_2), value);
	}

	inline static int32_t get_offset_of__getKeys_3() { return static_cast<int32_t>(offsetof(AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA, ____getKeys_3)); }
	inline Func_3_tC179A8DB3DC1F41CFC7E52AB5999F0F276F0AF05 * get__getKeys_3() const { return ____getKeys_3; }
	inline Func_3_tC179A8DB3DC1F41CFC7E52AB5999F0F276F0AF05 ** get_address_of__getKeys_3() { return &____getKeys_3; }
	inline void set__getKeys_3(Func_3_tC179A8DB3DC1F41CFC7E52AB5999F0F276F0AF05 * value)
	{
		____getKeys_3 = value;
		Il2CppCodeGenWriteBarrier((&____getKeys_3), value);
	}

	inline static int32_t get_offset_of__isSingleKey_4() { return static_cast<int32_t>(offsetof(AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA, ____isSingleKey_4)); }
	inline bool get__isSingleKey_4() const { return ____isSingleKey_4; }
	inline bool* get_address_of__isSingleKey_4() { return &____isSingleKey_4; }
	inline void set__isSingleKey_4(bool value)
	{
		____isSingleKey_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTENTITYINDEX_2_T21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA_H
#ifndef CONTEXT_1_T51CD1A3566ACEF172A6D9EBA8D27910919BFFED5_H
#define CONTEXT_1_T51CD1A3566ACEF172A6D9EBA8D27910919BFFED5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.Context`1<GameEntity>
struct  Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5  : public RuntimeObject
{
public:
	// Entitas.ContextEntityChanged Entitas.Context`1::OnEntityCreated
	ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 * ___OnEntityCreated_0;
	// Entitas.ContextEntityChanged Entitas.Context`1::OnEntityWillBeDestroyed
	ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 * ___OnEntityWillBeDestroyed_1;
	// Entitas.ContextEntityChanged Entitas.Context`1::OnEntityDestroyed
	ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 * ___OnEntityDestroyed_2;
	// Entitas.ContextGroupChanged Entitas.Context`1::OnGroupCreated
	ContextGroupChanged_tDF404FDADBE283C90826E021920179C9C5986728 * ___OnGroupCreated_3;
	// System.Int32 Entitas.Context`1::_totalComponents
	int32_t ____totalComponents_4;
	// System.Collections.Generic.Stack`1<Entitas.IComponent>[] Entitas.Context`1::_componentPools
	Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* ____componentPools_5;
	// Entitas.ContextInfo Entitas.Context`1::_contextInfo
	ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * ____contextInfo_6;
	// System.Func`2<Entitas.IEntity,Entitas.IAERC> Entitas.Context`1::_aercFactory
	Func_2_t17A06C13140486C05B62E73F4900E90CD6CD5172 * ____aercFactory_7;
	// System.Func`1<TEntity> Entitas.Context`1::_entityFactory
	Func_1_t80B4C2C3FEA10713ACD97FF7EB221B48BF634166 * ____entityFactory_8;
	// System.Collections.Generic.HashSet`1<TEntity> Entitas.Context`1::_entities
	HashSet_1_t063A3379F7914EEA90E4FE6E7CA418473331A29C * ____entities_9;
	// System.Collections.Generic.Stack`1<TEntity> Entitas.Context`1::_reusableEntities
	Stack_1_t0F7CA9F21180ACC53849C63DB13E6B0D29C78AE6 * ____reusableEntities_10;
	// System.Collections.Generic.HashSet`1<TEntity> Entitas.Context`1::_retainedEntities
	HashSet_1_t063A3379F7914EEA90E4FE6E7CA418473331A29C * ____retainedEntities_11;
	// System.Collections.Generic.Dictionary`2<Entitas.IMatcher`1<TEntity>,Entitas.IGroup`1<TEntity>> Entitas.Context`1::_groups
	Dictionary_2_t50A00884B3EAE184A96FD38FB9C4F54D823C395D * ____groups_12;
	// System.Collections.Generic.List`1<Entitas.IGroup`1<TEntity>>[] Entitas.Context`1::_groupsForIndex
	List_1U5BU5D_tE003DE61ED7636D16B6D9BA21BDBCDFB723F5A92* ____groupsForIndex_13;
	// DesperateDevs.Utils.ObjectPool`1<System.Collections.Generic.List`1<Entitas.GroupChanged`1<TEntity>>> Entitas.Context`1::_groupChangedListPool
	ObjectPool_1_tB799227ADF820B9D3BB3FF02CC0DEAFA4C317463 * ____groupChangedListPool_14;
	// System.Collections.Generic.Dictionary`2<System.String,Entitas.IEntityIndex> Entitas.Context`1::_entityIndices
	Dictionary_2_t8C31B2198A4B55E3CD0B09E8EFCB67CC4D283E0C * ____entityIndices_15;
	// System.Int32 Entitas.Context`1::_creationIndex
	int32_t ____creationIndex_16;
	// TEntity[] Entitas.Context`1::_entitiesCache
	GameEntityU5BU5D_tDC5EC08114DD197DC35905B963D07B3E073F05E3* ____entitiesCache_17;
	// Entitas.EntityComponentChanged Entitas.Context`1::_cachedEntityChanged
	EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * ____cachedEntityChanged_18;
	// Entitas.EntityComponentReplaced Entitas.Context`1::_cachedComponentReplaced
	EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * ____cachedComponentReplaced_19;
	// Entitas.EntityEvent Entitas.Context`1::_cachedEntityReleased
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * ____cachedEntityReleased_20;
	// Entitas.EntityEvent Entitas.Context`1::_cachedDestroyEntity
	EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * ____cachedDestroyEntity_21;

public:
	inline static int32_t get_offset_of_OnEntityCreated_0() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ___OnEntityCreated_0)); }
	inline ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 * get_OnEntityCreated_0() const { return ___OnEntityCreated_0; }
	inline ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 ** get_address_of_OnEntityCreated_0() { return &___OnEntityCreated_0; }
	inline void set_OnEntityCreated_0(ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 * value)
	{
		___OnEntityCreated_0 = value;
		Il2CppCodeGenWriteBarrier((&___OnEntityCreated_0), value);
	}

	inline static int32_t get_offset_of_OnEntityWillBeDestroyed_1() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ___OnEntityWillBeDestroyed_1)); }
	inline ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 * get_OnEntityWillBeDestroyed_1() const { return ___OnEntityWillBeDestroyed_1; }
	inline ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 ** get_address_of_OnEntityWillBeDestroyed_1() { return &___OnEntityWillBeDestroyed_1; }
	inline void set_OnEntityWillBeDestroyed_1(ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 * value)
	{
		___OnEntityWillBeDestroyed_1 = value;
		Il2CppCodeGenWriteBarrier((&___OnEntityWillBeDestroyed_1), value);
	}

	inline static int32_t get_offset_of_OnEntityDestroyed_2() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ___OnEntityDestroyed_2)); }
	inline ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 * get_OnEntityDestroyed_2() const { return ___OnEntityDestroyed_2; }
	inline ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 ** get_address_of_OnEntityDestroyed_2() { return &___OnEntityDestroyed_2; }
	inline void set_OnEntityDestroyed_2(ContextEntityChanged_t1C8C691CEE56C008F944E90383D7D7400E111EC1 * value)
	{
		___OnEntityDestroyed_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnEntityDestroyed_2), value);
	}

	inline static int32_t get_offset_of_OnGroupCreated_3() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ___OnGroupCreated_3)); }
	inline ContextGroupChanged_tDF404FDADBE283C90826E021920179C9C5986728 * get_OnGroupCreated_3() const { return ___OnGroupCreated_3; }
	inline ContextGroupChanged_tDF404FDADBE283C90826E021920179C9C5986728 ** get_address_of_OnGroupCreated_3() { return &___OnGroupCreated_3; }
	inline void set_OnGroupCreated_3(ContextGroupChanged_tDF404FDADBE283C90826E021920179C9C5986728 * value)
	{
		___OnGroupCreated_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnGroupCreated_3), value);
	}

	inline static int32_t get_offset_of__totalComponents_4() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____totalComponents_4)); }
	inline int32_t get__totalComponents_4() const { return ____totalComponents_4; }
	inline int32_t* get_address_of__totalComponents_4() { return &____totalComponents_4; }
	inline void set__totalComponents_4(int32_t value)
	{
		____totalComponents_4 = value;
	}

	inline static int32_t get_offset_of__componentPools_5() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____componentPools_5)); }
	inline Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* get__componentPools_5() const { return ____componentPools_5; }
	inline Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272** get_address_of__componentPools_5() { return &____componentPools_5; }
	inline void set__componentPools_5(Stack_1U5BU5D_t959DEE82B18D275AF34FB16A9E6547FDD3ACF272* value)
	{
		____componentPools_5 = value;
		Il2CppCodeGenWriteBarrier((&____componentPools_5), value);
	}

	inline static int32_t get_offset_of__contextInfo_6() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____contextInfo_6)); }
	inline ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * get__contextInfo_6() const { return ____contextInfo_6; }
	inline ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED ** get_address_of__contextInfo_6() { return &____contextInfo_6; }
	inline void set__contextInfo_6(ContextInfo_tC2E42F30007CB6FEAE0F1E8B6EE4240270D59CED * value)
	{
		____contextInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&____contextInfo_6), value);
	}

	inline static int32_t get_offset_of__aercFactory_7() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____aercFactory_7)); }
	inline Func_2_t17A06C13140486C05B62E73F4900E90CD6CD5172 * get__aercFactory_7() const { return ____aercFactory_7; }
	inline Func_2_t17A06C13140486C05B62E73F4900E90CD6CD5172 ** get_address_of__aercFactory_7() { return &____aercFactory_7; }
	inline void set__aercFactory_7(Func_2_t17A06C13140486C05B62E73F4900E90CD6CD5172 * value)
	{
		____aercFactory_7 = value;
		Il2CppCodeGenWriteBarrier((&____aercFactory_7), value);
	}

	inline static int32_t get_offset_of__entityFactory_8() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____entityFactory_8)); }
	inline Func_1_t80B4C2C3FEA10713ACD97FF7EB221B48BF634166 * get__entityFactory_8() const { return ____entityFactory_8; }
	inline Func_1_t80B4C2C3FEA10713ACD97FF7EB221B48BF634166 ** get_address_of__entityFactory_8() { return &____entityFactory_8; }
	inline void set__entityFactory_8(Func_1_t80B4C2C3FEA10713ACD97FF7EB221B48BF634166 * value)
	{
		____entityFactory_8 = value;
		Il2CppCodeGenWriteBarrier((&____entityFactory_8), value);
	}

	inline static int32_t get_offset_of__entities_9() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____entities_9)); }
	inline HashSet_1_t063A3379F7914EEA90E4FE6E7CA418473331A29C * get__entities_9() const { return ____entities_9; }
	inline HashSet_1_t063A3379F7914EEA90E4FE6E7CA418473331A29C ** get_address_of__entities_9() { return &____entities_9; }
	inline void set__entities_9(HashSet_1_t063A3379F7914EEA90E4FE6E7CA418473331A29C * value)
	{
		____entities_9 = value;
		Il2CppCodeGenWriteBarrier((&____entities_9), value);
	}

	inline static int32_t get_offset_of__reusableEntities_10() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____reusableEntities_10)); }
	inline Stack_1_t0F7CA9F21180ACC53849C63DB13E6B0D29C78AE6 * get__reusableEntities_10() const { return ____reusableEntities_10; }
	inline Stack_1_t0F7CA9F21180ACC53849C63DB13E6B0D29C78AE6 ** get_address_of__reusableEntities_10() { return &____reusableEntities_10; }
	inline void set__reusableEntities_10(Stack_1_t0F7CA9F21180ACC53849C63DB13E6B0D29C78AE6 * value)
	{
		____reusableEntities_10 = value;
		Il2CppCodeGenWriteBarrier((&____reusableEntities_10), value);
	}

	inline static int32_t get_offset_of__retainedEntities_11() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____retainedEntities_11)); }
	inline HashSet_1_t063A3379F7914EEA90E4FE6E7CA418473331A29C * get__retainedEntities_11() const { return ____retainedEntities_11; }
	inline HashSet_1_t063A3379F7914EEA90E4FE6E7CA418473331A29C ** get_address_of__retainedEntities_11() { return &____retainedEntities_11; }
	inline void set__retainedEntities_11(HashSet_1_t063A3379F7914EEA90E4FE6E7CA418473331A29C * value)
	{
		____retainedEntities_11 = value;
		Il2CppCodeGenWriteBarrier((&____retainedEntities_11), value);
	}

	inline static int32_t get_offset_of__groups_12() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____groups_12)); }
	inline Dictionary_2_t50A00884B3EAE184A96FD38FB9C4F54D823C395D * get__groups_12() const { return ____groups_12; }
	inline Dictionary_2_t50A00884B3EAE184A96FD38FB9C4F54D823C395D ** get_address_of__groups_12() { return &____groups_12; }
	inline void set__groups_12(Dictionary_2_t50A00884B3EAE184A96FD38FB9C4F54D823C395D * value)
	{
		____groups_12 = value;
		Il2CppCodeGenWriteBarrier((&____groups_12), value);
	}

	inline static int32_t get_offset_of__groupsForIndex_13() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____groupsForIndex_13)); }
	inline List_1U5BU5D_tE003DE61ED7636D16B6D9BA21BDBCDFB723F5A92* get__groupsForIndex_13() const { return ____groupsForIndex_13; }
	inline List_1U5BU5D_tE003DE61ED7636D16B6D9BA21BDBCDFB723F5A92** get_address_of__groupsForIndex_13() { return &____groupsForIndex_13; }
	inline void set__groupsForIndex_13(List_1U5BU5D_tE003DE61ED7636D16B6D9BA21BDBCDFB723F5A92* value)
	{
		____groupsForIndex_13 = value;
		Il2CppCodeGenWriteBarrier((&____groupsForIndex_13), value);
	}

	inline static int32_t get_offset_of__groupChangedListPool_14() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____groupChangedListPool_14)); }
	inline ObjectPool_1_tB799227ADF820B9D3BB3FF02CC0DEAFA4C317463 * get__groupChangedListPool_14() const { return ____groupChangedListPool_14; }
	inline ObjectPool_1_tB799227ADF820B9D3BB3FF02CC0DEAFA4C317463 ** get_address_of__groupChangedListPool_14() { return &____groupChangedListPool_14; }
	inline void set__groupChangedListPool_14(ObjectPool_1_tB799227ADF820B9D3BB3FF02CC0DEAFA4C317463 * value)
	{
		____groupChangedListPool_14 = value;
		Il2CppCodeGenWriteBarrier((&____groupChangedListPool_14), value);
	}

	inline static int32_t get_offset_of__entityIndices_15() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____entityIndices_15)); }
	inline Dictionary_2_t8C31B2198A4B55E3CD0B09E8EFCB67CC4D283E0C * get__entityIndices_15() const { return ____entityIndices_15; }
	inline Dictionary_2_t8C31B2198A4B55E3CD0B09E8EFCB67CC4D283E0C ** get_address_of__entityIndices_15() { return &____entityIndices_15; }
	inline void set__entityIndices_15(Dictionary_2_t8C31B2198A4B55E3CD0B09E8EFCB67CC4D283E0C * value)
	{
		____entityIndices_15 = value;
		Il2CppCodeGenWriteBarrier((&____entityIndices_15), value);
	}

	inline static int32_t get_offset_of__creationIndex_16() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____creationIndex_16)); }
	inline int32_t get__creationIndex_16() const { return ____creationIndex_16; }
	inline int32_t* get_address_of__creationIndex_16() { return &____creationIndex_16; }
	inline void set__creationIndex_16(int32_t value)
	{
		____creationIndex_16 = value;
	}

	inline static int32_t get_offset_of__entitiesCache_17() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____entitiesCache_17)); }
	inline GameEntityU5BU5D_tDC5EC08114DD197DC35905B963D07B3E073F05E3* get__entitiesCache_17() const { return ____entitiesCache_17; }
	inline GameEntityU5BU5D_tDC5EC08114DD197DC35905B963D07B3E073F05E3** get_address_of__entitiesCache_17() { return &____entitiesCache_17; }
	inline void set__entitiesCache_17(GameEntityU5BU5D_tDC5EC08114DD197DC35905B963D07B3E073F05E3* value)
	{
		____entitiesCache_17 = value;
		Il2CppCodeGenWriteBarrier((&____entitiesCache_17), value);
	}

	inline static int32_t get_offset_of__cachedEntityChanged_18() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____cachedEntityChanged_18)); }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * get__cachedEntityChanged_18() const { return ____cachedEntityChanged_18; }
	inline EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A ** get_address_of__cachedEntityChanged_18() { return &____cachedEntityChanged_18; }
	inline void set__cachedEntityChanged_18(EntityComponentChanged_t326B845F3277940DACBBC8836618A83196065E1A * value)
	{
		____cachedEntityChanged_18 = value;
		Il2CppCodeGenWriteBarrier((&____cachedEntityChanged_18), value);
	}

	inline static int32_t get_offset_of__cachedComponentReplaced_19() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____cachedComponentReplaced_19)); }
	inline EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * get__cachedComponentReplaced_19() const { return ____cachedComponentReplaced_19; }
	inline EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 ** get_address_of__cachedComponentReplaced_19() { return &____cachedComponentReplaced_19; }
	inline void set__cachedComponentReplaced_19(EntityComponentReplaced_tECE10545D16DE6AECA3B03B47FA2582FE7A11B57 * value)
	{
		____cachedComponentReplaced_19 = value;
		Il2CppCodeGenWriteBarrier((&____cachedComponentReplaced_19), value);
	}

	inline static int32_t get_offset_of__cachedEntityReleased_20() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____cachedEntityReleased_20)); }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * get__cachedEntityReleased_20() const { return ____cachedEntityReleased_20; }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 ** get_address_of__cachedEntityReleased_20() { return &____cachedEntityReleased_20; }
	inline void set__cachedEntityReleased_20(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * value)
	{
		____cachedEntityReleased_20 = value;
		Il2CppCodeGenWriteBarrier((&____cachedEntityReleased_20), value);
	}

	inline static int32_t get_offset_of__cachedDestroyEntity_21() { return static_cast<int32_t>(offsetof(Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5, ____cachedDestroyEntity_21)); }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * get__cachedDestroyEntity_21() const { return ____cachedDestroyEntity_21; }
	inline EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 ** get_address_of__cachedDestroyEntity_21() { return &____cachedDestroyEntity_21; }
	inline void set__cachedDestroyEntity_21(EntityEvent_tE7578C70965872DD79E91A748EBBCD674AEAE1D6 * value)
	{
		____cachedDestroyEntity_21 = value;
		Il2CppCodeGenWriteBarrier((&____cachedDestroyEntity_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXT_1_T51CD1A3566ACEF172A6D9EBA8D27910919BFFED5_H
#ifndef REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#define REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.ReactiveSystem`1<GameEntity>
struct  ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9  : public RuntimeObject
{
public:
	// Entitas.ICollector`1<TEntity> Entitas.ReactiveSystem`1::_collector
	RuntimeObject* ____collector_0;
	// System.Collections.Generic.List`1<TEntity> Entitas.ReactiveSystem`1::_buffer
	List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * ____buffer_1;
	// System.String Entitas.ReactiveSystem`1::_toStringCache
	String_t* ____toStringCache_2;

public:
	inline static int32_t get_offset_of__collector_0() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____collector_0)); }
	inline RuntimeObject* get__collector_0() const { return ____collector_0; }
	inline RuntimeObject** get_address_of__collector_0() { return &____collector_0; }
	inline void set__collector_0(RuntimeObject* value)
	{
		____collector_0 = value;
		Il2CppCodeGenWriteBarrier((&____collector_0), value);
	}

	inline static int32_t get_offset_of__buffer_1() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____buffer_1)); }
	inline List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * get__buffer_1() const { return ____buffer_1; }
	inline List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 ** get_address_of__buffer_1() { return &____buffer_1; }
	inline void set__buffer_1(List_1_t37F4C7CF3D1202E4756C2E10D18F5A665A14FA29 * value)
	{
		____buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_1), value);
	}

	inline static int32_t get_offset_of__toStringCache_2() { return static_cast<int32_t>(offsetof(ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9, ____toStringCache_2)); }
	inline String_t* get__toStringCache_2() const { return ____toStringCache_2; }
	inline String_t** get_address_of__toStringCache_2() { return &____toStringCache_2; }
	inline void set__toStringCache_2(String_t* value)
	{
		____toStringCache_2 = value;
		Il2CppCodeGenWriteBarrier((&____toStringCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVESYSTEM_1_TD4874D09441436F0E3DB74FACC6C25A1F78AE0F9_H
#ifndef FREEINVENTORY_TAF72E5E048627F4747D2262169130958BB47C6F7_H
#define FREEINVENTORY_TAF72E5E048627F4747D2262169130958BB47C6F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FreeInventory
struct  FreeInventory_tAF72E5E048627F4747D2262169130958BB47C6F7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FREEINVENTORY_TAF72E5E048627F4747D2262169130958BB47C6F7_H
#ifndef U3CU3EC_T50CA66C109446C18409AC5FFBF85FD47BE10ECA6_H
#define U3CU3EC_T50CA66C109446C18409AC5FFBF85FD47BE10ECA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameContext_<>c
struct  U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6_StaticFields
{
public:
	// GameContext_<>c GameContext_<>c::<>9
	U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6 * ___U3CU3E9_0;
	// System.Func`2<Entitas.IEntity,Entitas.IAERC> GameContext_<>c::<>9__34_0
	Func_2_t17A06C13140486C05B62E73F4900E90CD6CD5172 * ___U3CU3E9__34_0_1;
	// System.Func`1<GameEntity> GameContext_<>c::<>9__34_1
	Func_1_t80B4C2C3FEA10713ACD97FF7EB221B48BF634166 * ___U3CU3E9__34_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6_StaticFields, ___U3CU3E9__34_0_1)); }
	inline Func_2_t17A06C13140486C05B62E73F4900E90CD6CD5172 * get_U3CU3E9__34_0_1() const { return ___U3CU3E9__34_0_1; }
	inline Func_2_t17A06C13140486C05B62E73F4900E90CD6CD5172 ** get_address_of_U3CU3E9__34_0_1() { return &___U3CU3E9__34_0_1; }
	inline void set_U3CU3E9__34_0_1(Func_2_t17A06C13140486C05B62E73F4900E90CD6CD5172 * value)
	{
		___U3CU3E9__34_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6_StaticFields, ___U3CU3E9__34_1_2)); }
	inline Func_1_t80B4C2C3FEA10713ACD97FF7EB221B48BF634166 * get_U3CU3E9__34_1_2() const { return ___U3CU3E9__34_1_2; }
	inline Func_1_t80B4C2C3FEA10713ACD97FF7EB221B48BF634166 ** get_address_of_U3CU3E9__34_1_2() { return &___U3CU3E9__34_1_2; }
	inline void set_U3CU3E9__34_1_2(Func_1_t80B4C2C3FEA10713ACD97FF7EB221B48BF634166 * value)
	{
		___U3CU3E9__34_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T50CA66C109446C18409AC5FFBF85FD47BE10ECA6_H
#ifndef GROUPCOMPONENT_TDBD3E652B21077AAEB65B6E3EC66A7DE368CD9E6_H
#define GROUPCOMPONENT_TDBD3E652B21077AAEB65B6E3EC66A7DE368CD9E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GroupComponent
struct  GroupComponent_tDBD3E652B21077AAEB65B6E3EC66A7DE368CD9E6  : public RuntimeObject
{
public:
	// System.String GroupComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(GroupComponent_tDBD3E652B21077AAEB65B6E3EC66A7DE368CD9E6, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCOMPONENT_TDBD3E652B21077AAEB65B6E3EC66A7DE368CD9E6_H
#ifndef HPBOOSTCOMPONENT_TDE83D6C582D78D86372B11F0AB2E4F28F300A280_H
#define HPBOOSTCOMPONENT_TDE83D6C582D78D86372B11F0AB2E4F28F300A280_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HPBoostComponent
struct  HPBoostComponent_tDE83D6C582D78D86372B11F0AB2E4F28F300A280  : public RuntimeObject
{
public:
	// System.Int32 HPBoostComponent::Percentage
	int32_t ___Percentage_0;

public:
	inline static int32_t get_offset_of_Percentage_0() { return static_cast<int32_t>(offsetof(HPBoostComponent_tDE83D6C582D78D86372B11F0AB2E4F28F300A280, ___Percentage_0)); }
	inline int32_t get_Percentage_0() const { return ___Percentage_0; }
	inline int32_t* get_address_of_Percentage_0() { return &___Percentage_0; }
	inline void set_Percentage_0(int32_t value)
	{
		___Percentage_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HPBOOSTCOMPONENT_TDE83D6C582D78D86372B11F0AB2E4F28F300A280_H
#ifndef HPCOMPONENT_TD0E8C55BCF778CABB9AAD595E51321D549CEE55E_H
#define HPCOMPONENT_TD0E8C55BCF778CABB9AAD595E51321D549CEE55E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HPComponent
struct  HPComponent_tD0E8C55BCF778CABB9AAD595E51321D549CEE55E  : public RuntimeObject
{
public:
	// System.Int32 HPComponent::Value
	int32_t ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(HPComponent_tD0E8C55BCF778CABB9AAD595E51321D549CEE55E, ___Value_0)); }
	inline int32_t get_Value_0() const { return ___Value_0; }
	inline int32_t* get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(int32_t value)
	{
		___Value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HPCOMPONENT_TD0E8C55BCF778CABB9AAD595E51321D549CEE55E_H
#ifndef HEROMAINATTACKSYSTEM_TFA14FDA086F42BCA97E038E1EE1DBA26B30FB540_H
#define HEROMAINATTACKSYSTEM_TFA14FDA086F42BCA97E038E1EE1DBA26B30FB540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroMainAttackSystem
struct  HeroMainAttackSystem_tFA14FDA086F42BCA97E038E1EE1DBA26B30FB540  : public RuntimeObject
{
public:
	// SpellsSO HeroMainAttackSystem::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_0;
	// GameContext HeroMainAttackSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_1;

public:
	inline static int32_t get_offset_of__spellsSO_0() { return static_cast<int32_t>(offsetof(HeroMainAttackSystem_tFA14FDA086F42BCA97E038E1EE1DBA26B30FB540, ____spellsSO_0)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_0() const { return ____spellsSO_0; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_0() { return &____spellsSO_0; }
	inline void set__spellsSO_0(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_0 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_0), value);
	}

	inline static int32_t get_offset_of__context_1() { return static_cast<int32_t>(offsetof(HeroMainAttackSystem_tFA14FDA086F42BCA97E038E1EE1DBA26B30FB540, ____context_1)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_1() const { return ____context_1; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_1() { return &____context_1; }
	inline void set__context_1(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_1 = value;
		Il2CppCodeGenWriteBarrier((&____context_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROMAINATTACKSYSTEM_TFA14FDA086F42BCA97E038E1EE1DBA26B30FB540_H
#ifndef HEROMOVEMENTSYSTEM_TD86CE8646EA52823A31A6D2E5FB3BDF2AF9CACC4_H
#define HEROMOVEMENTSYSTEM_TD86CE8646EA52823A31A6D2E5FB3BDF2AF9CACC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroMovementSystem
struct  HeroMovementSystem_tD86CE8646EA52823A31A6D2E5FB3BDF2AF9CACC4  : public RuntimeObject
{
public:
	// GameContext HeroMovementSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_0;

public:
	inline static int32_t get_offset_of__context_0() { return static_cast<int32_t>(offsetof(HeroMovementSystem_tD86CE8646EA52823A31A6D2E5FB3BDF2AF9CACC4, ____context_0)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_0() const { return ____context_0; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_0() { return &____context_0; }
	inline void set__context_0(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_0 = value;
		Il2CppCodeGenWriteBarrier((&____context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROMOVEMENTSYSTEM_TD86CE8646EA52823A31A6D2E5FB3BDF2AF9CACC4_H
#ifndef IDCOMPONENT_TF5656CA226F5AC319A7997E076D307136C3A1AD0_H
#define IDCOMPONENT_TF5656CA226F5AC319A7997E076D307136C3A1AD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// IdComponent
struct  IdComponent_tF5656CA226F5AC319A7997E076D307136C3A1AD0  : public RuntimeObject
{
public:
	// System.String IdComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(IdComponent_tF5656CA226F5AC319A7997E076D307136C3A1AD0, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDCOMPONENT_TF5656CA226F5AC319A7997E076D307136C3A1AD0_H
#ifndef INSTANTEFFECTCOMPONENT_T208614FDD7E97EAFE28016B6B16193058AFFFFCD_H
#define INSTANTEFFECTCOMPONENT_T208614FDD7E97EAFE28016B6B16193058AFFFFCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantEffectComponent
struct  InstantEffectComponent_t208614FDD7E97EAFE28016B6B16193058AFFFFCD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTEFFECTCOMPONENT_T208614FDD7E97EAFE28016B6B16193058AFFFFCD_H
#ifndef INSTANTEFFECTSYSTEM_TCC7E89D58693487A0E400269BDAFB24AFEBE35B8_H
#define INSTANTEFFECTSYSTEM_TCC7E89D58693487A0E400269BDAFB24AFEBE35B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantEffectSystem
struct  InstantEffectSystem_tCC7E89D58693487A0E400269BDAFB24AFEBE35B8  : public RuntimeObject
{
public:
	// Entitas.IGroup`1<GameEntity> InstantEffectSystem::_entities
	RuntimeObject* ____entities_0;
	// GameContext InstantEffectSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_1;

public:
	inline static int32_t get_offset_of__entities_0() { return static_cast<int32_t>(offsetof(InstantEffectSystem_tCC7E89D58693487A0E400269BDAFB24AFEBE35B8, ____entities_0)); }
	inline RuntimeObject* get__entities_0() const { return ____entities_0; }
	inline RuntimeObject** get_address_of__entities_0() { return &____entities_0; }
	inline void set__entities_0(RuntimeObject* value)
	{
		____entities_0 = value;
		Il2CppCodeGenWriteBarrier((&____entities_0), value);
	}

	inline static int32_t get_offset_of__context_1() { return static_cast<int32_t>(offsetof(InstantEffectSystem_tCC7E89D58693487A0E400269BDAFB24AFEBE35B8, ____context_1)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_1() const { return ____context_1; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_1() { return &____context_1; }
	inline void set__context_1(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_1 = value;
		Il2CppCodeGenWriteBarrier((&____context_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTEFFECTSYSTEM_TCC7E89D58693487A0E400269BDAFB24AFEBE35B8_H
#ifndef INVENTORYCOMPONENT_TEC537ECD5B1A9A2CDE9CBC13D9BD7AC836B8D3AE_H
#define INVENTORYCOMPONENT_TEC537ECD5B1A9A2CDE9CBC13D9BD7AC836B8D3AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryComponent
struct  InventoryComponent_tEC537ECD5B1A9A2CDE9CBC13D9BD7AC836B8D3AE  : public RuntimeObject
{
public:
	// System.Int32 InventoryComponent::MaxSize
	int32_t ___MaxSize_0;

public:
	inline static int32_t get_offset_of_MaxSize_0() { return static_cast<int32_t>(offsetof(InventoryComponent_tEC537ECD5B1A9A2CDE9CBC13D9BD7AC836B8D3AE, ___MaxSize_0)); }
	inline int32_t get_MaxSize_0() const { return ___MaxSize_0; }
	inline int32_t* get_address_of_MaxSize_0() { return &___MaxSize_0; }
	inline void set_MaxSize_0(int32_t value)
	{
		___MaxSize_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYCOMPONENT_TEC537ECD5B1A9A2CDE9CBC13D9BD7AC836B8D3AE_H
#ifndef INVENTORYINDEXCOMPONENT_T03BD28736D2BA341CDE29FC3111FCB278A08047A_H
#define INVENTORYINDEXCOMPONENT_T03BD28736D2BA341CDE29FC3111FCB278A08047A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryIndexComponent
struct  InventoryIndexComponent_t03BD28736D2BA341CDE29FC3111FCB278A08047A  : public RuntimeObject
{
public:
	// System.Int32 InventoryIndexComponent::Index
	int32_t ___Index_0;

public:
	inline static int32_t get_offset_of_Index_0() { return static_cast<int32_t>(offsetof(InventoryIndexComponent_t03BD28736D2BA341CDE29FC3111FCB278A08047A, ___Index_0)); }
	inline int32_t get_Index_0() const { return ___Index_0; }
	inline int32_t* get_address_of_Index_0() { return &___Index_0; }
	inline void set_Index_0(int32_t value)
	{
		___Index_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYINDEXCOMPONENT_T03BD28736D2BA341CDE29FC3111FCB278A08047A_H
#ifndef INVENTORYITEMCOMPONENT_T4DCE5075CF2CDD4ECDB7BBEF04C3A6A65E49DBA5_H
#define INVENTORYITEMCOMPONENT_T4DCE5075CF2CDD4ECDB7BBEF04C3A6A65E49DBA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryItemComponent
struct  InventoryItemComponent_t4DCE5075CF2CDD4ECDB7BBEF04C3A6A65E49DBA5  : public RuntimeObject
{
public:
	// System.String InventoryItemComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(InventoryItemComponent_t4DCE5075CF2CDD4ECDB7BBEF04C3A6A65E49DBA5, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYITEMCOMPONENT_T4DCE5075CF2CDD4ECDB7BBEF04C3A6A65E49DBA5_H
#ifndef INVENTORYITEMSCOMPONENT_TDA26609A9DCDAF43BBAD2ECF90E2C2E7C2BD5C95_H
#define INVENTORYITEMSCOMPONENT_TDA26609A9DCDAF43BBAD2ECF90E2C2E7C2BD5C95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryItemsComponent
struct  InventoryItemsComponent_tDA26609A9DCDAF43BBAD2ECF90E2C2E7C2BD5C95  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> InventoryItemsComponent::Items
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___Items_0;

public:
	inline static int32_t get_offset_of_Items_0() { return static_cast<int32_t>(offsetof(InventoryItemsComponent_tDA26609A9DCDAF43BBAD2ECF90E2C2E7C2BD5C95, ___Items_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_Items_0() const { return ___Items_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_Items_0() { return &___Items_0; }
	inline void set_Items_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___Items_0 = value;
		Il2CppCodeGenWriteBarrier((&___Items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYITEMSCOMPONENT_TDA26609A9DCDAF43BBAD2ECF90E2C2E7C2BD5C95_H
#ifndef INVENTORYMEGABOOSTREADY_TB4A18852E6C6C84F1C2B9F9029C48398522FBBE3_H
#define INVENTORYMEGABOOSTREADY_TB4A18852E6C6C84F1C2B9F9029C48398522FBBE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryMegaBoostReady
struct  InventoryMegaBoostReady_tB4A18852E6C6C84F1C2B9F9029C48398522FBBE3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYMEGABOOSTREADY_TB4A18852E6C6C84F1C2B9F9029C48398522FBBE3_H
#ifndef INVENTORYNORMALBOOSTREADY_TA102FD9FD0D4930E683BB4ACCC2BF89AB9BE11A7_H
#define INVENTORYNORMALBOOSTREADY_TA102FD9FD0D4930E683BB4ACCC2BF89AB9BE11A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InventoryNormalBoostReady
struct  InventoryNormalBoostReady_tA102FD9FD0D4930E683BB4ACCC2BF89AB9BE11A7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORYNORMALBOOSTREADY_TA102FD9FD0D4930E683BB4ACCC2BF89AB9BE11A7_H
#ifndef ITEMCOMPONENT_T44957705DF3CCE8E1D7FCBFE244B1DF5D455824D_H
#define ITEMCOMPONENT_T44957705DF3CCE8E1D7FCBFE244B1DF5D455824D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemComponent
struct  ItemComponent_t44957705DF3CCE8E1D7FCBFE244B1DF5D455824D  : public RuntimeObject
{
public:
	// System.String ItemComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(ItemComponent_t44957705DF3CCE8E1D7FCBFE244B1DF5D455824D, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMCOMPONENT_T44957705DF3CCE8E1D7FCBFE244B1DF5D455824D_H
#ifndef ITEMGENERATORCOMPONENT_T3D2A4D8706A3C2B269E69E7420E56D34751C44A9_H
#define ITEMGENERATORCOMPONENT_T3D2A4D8706A3C2B269E69E7420E56D34751C44A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemGeneratorComponent
struct  ItemGeneratorComponent_t3D2A4D8706A3C2B269E69E7420E56D34751C44A9  : public RuntimeObject
{
public:
	// System.Single ItemGeneratorComponent::DisposeTime
	float ___DisposeTime_0;

public:
	inline static int32_t get_offset_of_DisposeTime_0() { return static_cast<int32_t>(offsetof(ItemGeneratorComponent_t3D2A4D8706A3C2B269E69E7420E56D34751C44A9, ___DisposeTime_0)); }
	inline float get_DisposeTime_0() const { return ___DisposeTime_0; }
	inline float* get_address_of_DisposeTime_0() { return &___DisposeTime_0; }
	inline void set_DisposeTime_0(float value)
	{
		___DisposeTime_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMGENERATORCOMPONENT_T3D2A4D8706A3C2B269E69E7420E56D34751C44A9_H
#ifndef ITEMSGENERATORECSSYSTEM_TFB46C0F734490821A2D2E757DF1CD49D98B0FCA4_H
#define ITEMSGENERATORECSSYSTEM_TFB46C0F734490821A2D2E757DF1CD49D98B0FCA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemsGeneratorEcsSystem
struct  ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4  : public RuntimeObject
{
public:
	// Zenject.DiContainer ItemsGeneratorEcsSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_0;
	// ChaptersSO ItemsGeneratorEcsSystem::_chaptersSO
	ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * ____chaptersSO_1;
	// UserVO ItemsGeneratorEcsSystem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_2;
	// ItemsSO ItemsGeneratorEcsSystem::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_3;
	// GameContext ItemsGeneratorEcsSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;
	// Entitas.IGroup`1<GameEntity> ItemsGeneratorEcsSystem::_entities
	RuntimeObject* ____entities_5;

public:
	inline static int32_t get_offset_of__diContainer_0() { return static_cast<int32_t>(offsetof(ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4, ____diContainer_0)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_0() const { return ____diContainer_0; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_0() { return &____diContainer_0; }
	inline void set__diContainer_0(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_0), value);
	}

	inline static int32_t get_offset_of__chaptersSO_1() { return static_cast<int32_t>(offsetof(ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4, ____chaptersSO_1)); }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * get__chaptersSO_1() const { return ____chaptersSO_1; }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 ** get_address_of__chaptersSO_1() { return &____chaptersSO_1; }
	inline void set__chaptersSO_1(ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * value)
	{
		____chaptersSO_1 = value;
		Il2CppCodeGenWriteBarrier((&____chaptersSO_1), value);
	}

	inline static int32_t get_offset_of__userVO_2() { return static_cast<int32_t>(offsetof(ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4, ____userVO_2)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_2() const { return ____userVO_2; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_2() { return &____userVO_2; }
	inline void set__userVO_2(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_2 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_2), value);
	}

	inline static int32_t get_offset_of__itemsSO_3() { return static_cast<int32_t>(offsetof(ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4, ____itemsSO_3)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_3() const { return ____itemsSO_3; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_3() { return &____itemsSO_3; }
	inline void set__itemsSO_3(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}

	inline static int32_t get_offset_of__entities_5() { return static_cast<int32_t>(offsetof(ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4, ____entities_5)); }
	inline RuntimeObject* get__entities_5() const { return ____entities_5; }
	inline RuntimeObject** get_address_of__entities_5() { return &____entities_5; }
	inline void set__entities_5(RuntimeObject* value)
	{
		____entities_5 = value;
		Il2CppCodeGenWriteBarrier((&____entities_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMSGENERATORECSSYSTEM_TFB46C0F734490821A2D2E757DF1CD49D98B0FCA4_H
#ifndef ITEMSSPAWNPOINTS_TA7338A523B7E4DEEE324D582A64392620946C24B_H
#define ITEMSSPAWNPOINTS_TA7338A523B7E4DEEE324D582A64392620946C24B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemsSpawnPoints
struct  ItemsSpawnPoints_tA7338A523B7E4DEEE324D582A64392620946C24B  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> ItemsSpawnPoints::SpawnPoint
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___SpawnPoint_0;

public:
	inline static int32_t get_offset_of_SpawnPoint_0() { return static_cast<int32_t>(offsetof(ItemsSpawnPoints_tA7338A523B7E4DEEE324D582A64392620946C24B, ___SpawnPoint_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_SpawnPoint_0() const { return ___SpawnPoint_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_SpawnPoint_0() { return &___SpawnPoint_0; }
	inline void set_SpawnPoint_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___SpawnPoint_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpawnPoint_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMSSPAWNPOINTS_TA7338A523B7E4DEEE324D582A64392620946C24B_H
#ifndef ITEMSSPAWNUTILS_T2C8768B3A6C4F84485AA9A2B280FEA790AB926AE_H
#define ITEMSSPAWNUTILS_T2C8768B3A6C4F84485AA9A2B280FEA790AB926AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemsSpawnUtils
struct  ItemsSpawnUtils_t2C8768B3A6C4F84485AA9A2B280FEA790AB926AE  : public RuntimeObject
{
public:

public:
};

struct ItemsSpawnUtils_t2C8768B3A6C4F84485AA9A2B280FEA790AB926AE_StaticFields
{
public:
	// System.Random ItemsSpawnUtils::rnd
	Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * ___rnd_0;

public:
	inline static int32_t get_offset_of_rnd_0() { return static_cast<int32_t>(offsetof(ItemsSpawnUtils_t2C8768B3A6C4F84485AA9A2B280FEA790AB926AE_StaticFields, ___rnd_0)); }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * get_rnd_0() const { return ___rnd_0; }
	inline Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F ** get_address_of_rnd_0() { return &___rnd_0; }
	inline void set_rnd_0(Random_t18A28484F67EFA289C256F508A5C71D9E6DEE09F * value)
	{
		___rnd_0 = value;
		Il2CppCodeGenWriteBarrier((&___rnd_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMSSPAWNUTILS_T2C8768B3A6C4F84485AA9A2B280FEA790AB926AE_H
#ifndef U3CU3EC_T469F2B1F6038A2F75A223568B858C3122D0D3793_H
#define U3CU3EC_T469F2B1F6038A2F75A223568B858C3122D0D3793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemsSpawnUtils_<>c
struct  U3CU3Ec_t469F2B1F6038A2F75A223568B858C3122D0D3793  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t469F2B1F6038A2F75A223568B858C3122D0D3793_StaticFields
{
public:
	// ItemsSpawnUtils_<>c ItemsSpawnUtils_<>c::<>9
	U3CU3Ec_t469F2B1F6038A2F75A223568B858C3122D0D3793 * ___U3CU3E9_0;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,ItemGenerationModel>,System.Int32> ItemsSpawnUtils_<>c::<>9__2_0
	Func_2_t4B9FEEC317C915D3F80083FD7B83B6071B64860D * ___U3CU3E9__2_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t469F2B1F6038A2F75A223568B858C3122D0D3793_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t469F2B1F6038A2F75A223568B858C3122D0D3793 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t469F2B1F6038A2F75A223568B858C3122D0D3793 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t469F2B1F6038A2F75A223568B858C3122D0D3793 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t469F2B1F6038A2F75A223568B858C3122D0D3793_StaticFields, ___U3CU3E9__2_0_1)); }
	inline Func_2_t4B9FEEC317C915D3F80083FD7B83B6071B64860D * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline Func_2_t4B9FEEC317C915D3F80083FD7B83B6071B64860D ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(Func_2_t4B9FEEC317C915D3F80083FD7B83B6071B64860D * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T469F2B1F6038A2F75A223568B858C3122D0D3793_H
#ifndef LASTATTACKTIMECOMPONENT_T9094D0E33E52610B5BC2A4EBA93EFA0C85F5A5C7_H
#define LASTATTACKTIMECOMPONENT_T9094D0E33E52610B5BC2A4EBA93EFA0C85F5A5C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LastAttackTimeComponent
struct  LastAttackTimeComponent_t9094D0E33E52610B5BC2A4EBA93EFA0C85F5A5C7  : public RuntimeObject
{
public:
	// System.Single LastAttackTimeComponent::LastAttackTime
	float ___LastAttackTime_0;

public:
	inline static int32_t get_offset_of_LastAttackTime_0() { return static_cast<int32_t>(offsetof(LastAttackTimeComponent_t9094D0E33E52610B5BC2A4EBA93EFA0C85F5A5C7, ___LastAttackTime_0)); }
	inline float get_LastAttackTime_0() const { return ___LastAttackTime_0; }
	inline float* get_address_of_LastAttackTime_0() { return &___LastAttackTime_0; }
	inline void set_LastAttackTime_0(float value)
	{
		___LastAttackTime_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LASTATTACKTIMECOMPONENT_T9094D0E33E52610B5BC2A4EBA93EFA0C85F5A5C7_H
#ifndef LEFTHANDCOMPONENT_TE33AB8CAB06C15A6A157653A52A3C1964681A287_H
#define LEFTHANDCOMPONENT_TE33AB8CAB06C15A6A157653A52A3C1964681A287_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeftHandComponent
struct  LeftHandComponent_tE33AB8CAB06C15A6A157653A52A3C1964681A287  : public RuntimeObject
{
public:
	// UnityEngine.GameObject LeftHandComponent::GameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___GameObject_0;

public:
	inline static int32_t get_offset_of_GameObject_0() { return static_cast<int32_t>(offsetof(LeftHandComponent_tE33AB8CAB06C15A6A157653A52A3C1964681A287, ___GameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_GameObject_0() const { return ___GameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_GameObject_0() { return &___GameObject_0; }
	inline void set_GameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___GameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___GameObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEFTHANDCOMPONENT_TE33AB8CAB06C15A6A157653A52A3C1964681A287_H
#ifndef LIFETIMEEFFECTCOMPONENT_T98B4DA278030322AAB0EDCB13EFFEFCA6510AFC5_H
#define LIFETIMEEFFECTCOMPONENT_T98B4DA278030322AAB0EDCB13EFFEFCA6510AFC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LifetimeEffectComponent
struct  LifetimeEffectComponent_t98B4DA278030322AAB0EDCB13EFFEFCA6510AFC5  : public RuntimeObject
{
public:
	// System.Single LifetimeEffectComponent::Duration
	float ___Duration_0;

public:
	inline static int32_t get_offset_of_Duration_0() { return static_cast<int32_t>(offsetof(LifetimeEffectComponent_t98B4DA278030322AAB0EDCB13EFFEFCA6510AFC5, ___Duration_0)); }
	inline float get_Duration_0() const { return ___Duration_0; }
	inline float* get_address_of_Duration_0() { return &___Duration_0; }
	inline void set_Duration_0(float value)
	{
		___Duration_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIFETIMEEFFECTCOMPONENT_T98B4DA278030322AAB0EDCB13EFFEFCA6510AFC5_H
#ifndef LIFETIMEEFFECTSYSTEM_T662707447DE8CB6CE7FBD64935D372759142AC58_H
#define LIFETIMEEFFECTSYSTEM_T662707447DE8CB6CE7FBD64935D372759142AC58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LifetimeEffectSystem
struct  LifetimeEffectSystem_t662707447DE8CB6CE7FBD64935D372759142AC58  : public RuntimeObject
{
public:
	// Entitas.IGroup`1<GameEntity> LifetimeEffectSystem::_entities
	RuntimeObject* ____entities_0;
	// GameContext LifetimeEffectSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_1;

public:
	inline static int32_t get_offset_of__entities_0() { return static_cast<int32_t>(offsetof(LifetimeEffectSystem_t662707447DE8CB6CE7FBD64935D372759142AC58, ____entities_0)); }
	inline RuntimeObject* get__entities_0() const { return ____entities_0; }
	inline RuntimeObject** get_address_of__entities_0() { return &____entities_0; }
	inline void set__entities_0(RuntimeObject* value)
	{
		____entities_0 = value;
		Il2CppCodeGenWriteBarrier((&____entities_0), value);
	}

	inline static int32_t get_offset_of__context_1() { return static_cast<int32_t>(offsetof(LifetimeEffectSystem_t662707447DE8CB6CE7FBD64935D372759142AC58, ____context_1)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_1() const { return ____context_1; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_1() { return &____context_1; }
	inline void set__context_1(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_1 = value;
		Il2CppCodeGenWriteBarrier((&____context_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIFETIMEEFFECTSYSTEM_T662707447DE8CB6CE7FBD64935D372759142AC58_H
#ifndef MAXHPCOMPONENT_T45EB581E73D7696CE765C8DA5DA0407DFAEF6FC7_H
#define MAXHPCOMPONENT_T45EB581E73D7696CE765C8DA5DA0407DFAEF6FC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MaxHPComponent
struct  MaxHPComponent_t45EB581E73D7696CE765C8DA5DA0407DFAEF6FC7  : public RuntimeObject
{
public:
	// System.Int32 MaxHPComponent::Value
	int32_t ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(MaxHPComponent_t45EB581E73D7696CE765C8DA5DA0407DFAEF6FC7, ___Value_0)); }
	inline int32_t get_Value_0() const { return ___Value_0; }
	inline int32_t* get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(int32_t value)
	{
		___Value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAXHPCOMPONENT_T45EB581E73D7696CE765C8DA5DA0407DFAEF6FC7_H
#ifndef PARENTCOMPONENT_T6693F80E87AF972F7FFB2046BFE1D4C574EE00B1_H
#define PARENTCOMPONENT_T6693F80E87AF972F7FFB2046BFE1D4C574EE00B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParentComponent
struct  ParentComponent_t6693F80E87AF972F7FFB2046BFE1D4C574EE00B1  : public RuntimeObject
{
public:
	// System.String ParentComponent::ParentId
	String_t* ___ParentId_0;

public:
	inline static int32_t get_offset_of_ParentId_0() { return static_cast<int32_t>(offsetof(ParentComponent_t6693F80E87AF972F7FFB2046BFE1D4C574EE00B1, ___ParentId_0)); }
	inline String_t* get_ParentId_0() const { return ___ParentId_0; }
	inline String_t** get_address_of_ParentId_0() { return &___ParentId_0; }
	inline void set_ParentId_0(String_t* value)
	{
		___ParentId_0 = value;
		Il2CppCodeGenWriteBarrier((&___ParentId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARENTCOMPONENT_T6693F80E87AF972F7FFB2046BFE1D4C574EE00B1_H
#ifndef PROJECTILECOLLISIONSYSTEM_TE66FF20613979A88CEBE2DB9036BFA5426B8B906_H
#define PROJECTILECOLLISIONSYSTEM_TE66FF20613979A88CEBE2DB9036BFA5426B8B906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileCollisionSystem
struct  ProjectileCollisionSystem_tE66FF20613979A88CEBE2DB9036BFA5426B8B906  : public RuntimeObject
{
public:
	// SpellsSO ProjectileCollisionSystem::_spellSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellSO_0;
	// Entitas.IGroup`1<GameEntity> ProjectileCollisionSystem::_entities
	RuntimeObject* ____entities_1;
	// GameContext ProjectileCollisionSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_2;

public:
	inline static int32_t get_offset_of__spellSO_0() { return static_cast<int32_t>(offsetof(ProjectileCollisionSystem_tE66FF20613979A88CEBE2DB9036BFA5426B8B906, ____spellSO_0)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellSO_0() const { return ____spellSO_0; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellSO_0() { return &____spellSO_0; }
	inline void set__spellSO_0(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellSO_0 = value;
		Il2CppCodeGenWriteBarrier((&____spellSO_0), value);
	}

	inline static int32_t get_offset_of__entities_1() { return static_cast<int32_t>(offsetof(ProjectileCollisionSystem_tE66FF20613979A88CEBE2DB9036BFA5426B8B906, ____entities_1)); }
	inline RuntimeObject* get__entities_1() const { return ____entities_1; }
	inline RuntimeObject** get_address_of__entities_1() { return &____entities_1; }
	inline void set__entities_1(RuntimeObject* value)
	{
		____entities_1 = value;
		Il2CppCodeGenWriteBarrier((&____entities_1), value);
	}

	inline static int32_t get_offset_of__context_2() { return static_cast<int32_t>(offsetof(ProjectileCollisionSystem_tE66FF20613979A88CEBE2DB9036BFA5426B8B906, ____context_2)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_2() const { return ____context_2; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_2() { return &____context_2; }
	inline void set__context_2(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_2 = value;
		Il2CppCodeGenWriteBarrier((&____context_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILECOLLISIONSYSTEM_TE66FF20613979A88CEBE2DB9036BFA5426B8B906_H
#ifndef PROJECTILESOURCECOMPONENT_T3B42D5D24633292F27113C80CFC5023973860457_H
#define PROJECTILESOURCECOMPONENT_T3B42D5D24633292F27113C80CFC5023973860457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileSourceComponent
struct  ProjectileSourceComponent_t3B42D5D24633292F27113C80CFC5023973860457  : public RuntimeObject
{
public:
	// System.String ProjectileSourceComponent::SourceId
	String_t* ___SourceId_0;

public:
	inline static int32_t get_offset_of_SourceId_0() { return static_cast<int32_t>(offsetof(ProjectileSourceComponent_t3B42D5D24633292F27113C80CFC5023973860457, ___SourceId_0)); }
	inline String_t* get_SourceId_0() const { return ___SourceId_0; }
	inline String_t** get_address_of_SourceId_0() { return &___SourceId_0; }
	inline void set_SourceId_0(String_t* value)
	{
		___SourceId_0 = value;
		Il2CppCodeGenWriteBarrier((&___SourceId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILESOURCECOMPONENT_T3B42D5D24633292F27113C80CFC5023973860457_H
#ifndef PROJECTILESPELLCOMPONENT_T0705BDDD9C71C99B26399E40FC8B9C588A829266_H
#define PROJECTILESPELLCOMPONENT_T0705BDDD9C71C99B26399E40FC8B9C588A829266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileSpellComponent
struct  ProjectileSpellComponent_t0705BDDD9C71C99B26399E40FC8B9C588A829266  : public RuntimeObject
{
public:
	// System.String ProjectileSpellComponent::SpellId
	String_t* ___SpellId_0;

public:
	inline static int32_t get_offset_of_SpellId_0() { return static_cast<int32_t>(offsetof(ProjectileSpellComponent_t0705BDDD9C71C99B26399E40FC8B9C588A829266, ___SpellId_0)); }
	inline String_t* get_SpellId_0() const { return ___SpellId_0; }
	inline String_t** get_address_of_SpellId_0() { return &___SpellId_0; }
	inline void set_SpellId_0(String_t* value)
	{
		___SpellId_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpellId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILESPELLCOMPONENT_T0705BDDD9C71C99B26399E40FC8B9C588A829266_H
#ifndef PROJECTILETARGETENTITYCOMPONENT_TB6079DABE86C7E40E686F8A0B71DAAFB64E54B7D_H
#define PROJECTILETARGETENTITYCOMPONENT_TB6079DABE86C7E40E686F8A0B71DAAFB64E54B7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileTargetEntityComponent
struct  ProjectileTargetEntityComponent_tB6079DABE86C7E40E686F8A0B71DAAFB64E54B7D  : public RuntimeObject
{
public:
	// System.String ProjectileTargetEntityComponent::Id
	String_t* ___Id_0;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(ProjectileTargetEntityComponent_tB6079DABE86C7E40E686F8A0B71DAAFB64E54B7D, ___Id_0)); }
	inline String_t* get_Id_0() const { return ___Id_0; }
	inline String_t** get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(String_t* value)
	{
		___Id_0 = value;
		Il2CppCodeGenWriteBarrier((&___Id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILETARGETENTITYCOMPONENT_TB6079DABE86C7E40E686F8A0B71DAAFB64E54B7D_H
#ifndef RESISTCOMPONENT_T7477E2D3BFDF6A316439E4260B7D657F92CFBCB8_H
#define RESISTCOMPONENT_T7477E2D3BFDF6A316439E4260B7D657F92CFBCB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResistComponent
struct  ResistComponent_t7477E2D3BFDF6A316439E4260B7D657F92CFBCB8  : public RuntimeObject
{
public:
	// System.Int32 ResistComponent::Percentage
	int32_t ___Percentage_0;

public:
	inline static int32_t get_offset_of_Percentage_0() { return static_cast<int32_t>(offsetof(ResistComponent_t7477E2D3BFDF6A316439E4260B7D657F92CFBCB8, ___Percentage_0)); }
	inline int32_t get_Percentage_0() const { return ___Percentage_0; }
	inline int32_t* get_address_of_Percentage_0() { return &___Percentage_0; }
	inline void set_Percentage_0(int32_t value)
	{
		___Percentage_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESISTCOMPONENT_T7477E2D3BFDF6A316439E4260B7D657F92CFBCB8_H
#ifndef SLOWSPEEDCOMPONENT_TABDA51B9E7584F28AE97E6045E67BEF778537113_H
#define SLOWSPEEDCOMPONENT_TABDA51B9E7584F28AE97E6045E67BEF778537113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SlowSpeedComponent
struct  SlowSpeedComponent_tABDA51B9E7584F28AE97E6045E67BEF778537113  : public RuntimeObject
{
public:
	// System.Single SlowSpeedComponent::Multiplier
	float ___Multiplier_0;

public:
	inline static int32_t get_offset_of_Multiplier_0() { return static_cast<int32_t>(offsetof(SlowSpeedComponent_tABDA51B9E7584F28AE97E6045E67BEF778537113, ___Multiplier_0)); }
	inline float get_Multiplier_0() const { return ___Multiplier_0; }
	inline float* get_address_of_Multiplier_0() { return &___Multiplier_0; }
	inline void set_Multiplier_0(float value)
	{
		___Multiplier_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOWSPEEDCOMPONENT_TABDA51B9E7584F28AE97E6045E67BEF778537113_H
#ifndef SPEEDBOOSTCOMPONENT_T7C7CA4CBFFB61152862651967A60BD4CCD1BA7DD_H
#define SPEEDBOOSTCOMPONENT_T7C7CA4CBFFB61152862651967A60BD4CCD1BA7DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpeedBoostComponent
struct  SpeedBoostComponent_t7C7CA4CBFFB61152862651967A60BD4CCD1BA7DD  : public RuntimeObject
{
public:
	// System.Single SpeedBoostComponent::Multiplier
	float ___Multiplier_0;

public:
	inline static int32_t get_offset_of_Multiplier_0() { return static_cast<int32_t>(offsetof(SpeedBoostComponent_t7C7CA4CBFFB61152862651967A60BD4CCD1BA7DD, ___Multiplier_0)); }
	inline float get_Multiplier_0() const { return ___Multiplier_0; }
	inline float* get_address_of_Multiplier_0() { return &___Multiplier_0; }
	inline void set_Multiplier_0(float value)
	{
		___Multiplier_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEEDBOOSTCOMPONENT_T7C7CA4CBFFB61152862651967A60BD4CCD1BA7DD_H
#ifndef SPEEDCOMPONENT_T175BE06C0E94BC2E2C40329096447D4606ED27B7_H
#define SPEEDCOMPONENT_T175BE06C0E94BC2E2C40329096447D4606ED27B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpeedComponent
struct  SpeedComponent_t175BE06C0E94BC2E2C40329096447D4606ED27B7  : public RuntimeObject
{
public:
	// System.Single SpeedComponent::Speed
	float ___Speed_0;

public:
	inline static int32_t get_offset_of_Speed_0() { return static_cast<int32_t>(offsetof(SpeedComponent_t175BE06C0E94BC2E2C40329096447D4606ED27B7, ___Speed_0)); }
	inline float get_Speed_0() const { return ___Speed_0; }
	inline float* get_address_of_Speed_0() { return &___Speed_0; }
	inline void set_Speed_0(float value)
	{
		___Speed_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEEDCOMPONENT_T175BE06C0E94BC2E2C40329096447D4606ED27B7_H
#ifndef STARTTIMECOMPONENT_T0A824916666C94123FC59BF33037692B17702341_H
#define STARTTIMECOMPONENT_T0A824916666C94123FC59BF33037692B17702341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartTimeComponent
struct  StartTimeComponent_t0A824916666C94123FC59BF33037692B17702341  : public RuntimeObject
{
public:
	// System.Single StartTimeComponent::Time
	float ___Time_0;

public:
	inline static int32_t get_offset_of_Time_0() { return static_cast<int32_t>(offsetof(StartTimeComponent_t0A824916666C94123FC59BF33037692B17702341, ___Time_0)); }
	inline float get_Time_0() const { return ___Time_0; }
	inline float* get_address_of_Time_0() { return &___Time_0; }
	inline void set_Time_0(float value)
	{
		___Time_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTTIMECOMPONENT_T0A824916666C94123FC59BF33037692B17702341_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef VISUALCOMPONENT_T983B7D5B711F9CBF41022CD3EFD0212607D948EE_H
#define VISUALCOMPONENT_T983B7D5B711F9CBF41022CD3EFD0212607D948EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VisualComponent
struct  VisualComponent_t983B7D5B711F9CBF41022CD3EFD0212607D948EE  : public RuntimeObject
{
public:
	// System.String VisualComponent::Prefab
	String_t* ___Prefab_0;
	// System.String VisualComponent::Bundle
	String_t* ___Bundle_1;

public:
	inline static int32_t get_offset_of_Prefab_0() { return static_cast<int32_t>(offsetof(VisualComponent_t983B7D5B711F9CBF41022CD3EFD0212607D948EE, ___Prefab_0)); }
	inline String_t* get_Prefab_0() const { return ___Prefab_0; }
	inline String_t** get_address_of_Prefab_0() { return &___Prefab_0; }
	inline void set_Prefab_0(String_t* value)
	{
		___Prefab_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_0), value);
	}

	inline static int32_t get_offset_of_Bundle_1() { return static_cast<int32_t>(offsetof(VisualComponent_t983B7D5B711F9CBF41022CD3EFD0212607D948EE, ___Bundle_1)); }
	inline String_t* get_Bundle_1() const { return ___Bundle_1; }
	inline String_t** get_address_of_Bundle_1() { return &___Bundle_1; }
	inline void set_Bundle_1(String_t* value)
	{
		___Bundle_1 = value;
		Il2CppCodeGenWriteBarrier((&___Bundle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VISUALCOMPONENT_T983B7D5B711F9CBF41022CD3EFD0212607D948EE_H
#ifndef ATTACKBOOSTSYSTEM_TA8E999E5111F0EAFD3906573E8C1750BEA9DDD73_H
#define ATTACKBOOSTSYSTEM_TA8E999E5111F0EAFD3906573E8C1750BEA9DDD73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackBoostSystem
struct  AttackBoostSystem_tA8E999E5111F0EAFD3906573E8C1750BEA9DDD73  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext AttackBoostSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(AttackBoostSystem_tA8E999E5111F0EAFD3906573E8C1750BEA9DDD73, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACKBOOSTSYSTEM_TA8E999E5111F0EAFD3906573E8C1750BEA9DDD73_H
#ifndef BATTLEINVENTORYSYSTEM_T0388841BABEC0CD0D81917E7A76C29E1D03C6173_H
#define BATTLEINVENTORYSYSTEM_T0388841BABEC0CD0D81917E7A76C29E1D03C6173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BattleInventorySystem
struct  BattleInventorySystem_t0388841BABEC0CD0D81917E7A76C29E1D03C6173  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// BattleSettingsSO BattleInventorySystem::_battleSettingsSO
	BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * ____battleSettingsSO_3;
	// ItemsSO BattleInventorySystem::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_4;
	// SpellsSO BattleInventorySystem::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_5;
	// GameContext BattleInventorySystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_6;

public:
	inline static int32_t get_offset_of__battleSettingsSO_3() { return static_cast<int32_t>(offsetof(BattleInventorySystem_t0388841BABEC0CD0D81917E7A76C29E1D03C6173, ____battleSettingsSO_3)); }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * get__battleSettingsSO_3() const { return ____battleSettingsSO_3; }
	inline BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE ** get_address_of__battleSettingsSO_3() { return &____battleSettingsSO_3; }
	inline void set__battleSettingsSO_3(BattleSettingsSO_t49214DEA165282C3E2BD7F72B6223E67DF19A3FE * value)
	{
		____battleSettingsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____battleSettingsSO_3), value);
	}

	inline static int32_t get_offset_of__itemsSO_4() { return static_cast<int32_t>(offsetof(BattleInventorySystem_t0388841BABEC0CD0D81917E7A76C29E1D03C6173, ____itemsSO_4)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_4() const { return ____itemsSO_4; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_4() { return &____itemsSO_4; }
	inline void set__itemsSO_4(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_4), value);
	}

	inline static int32_t get_offset_of__spellsSO_5() { return static_cast<int32_t>(offsetof(BattleInventorySystem_t0388841BABEC0CD0D81917E7A76C29E1D03C6173, ____spellsSO_5)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_5() const { return ____spellsSO_5; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_5() { return &____spellsSO_5; }
	inline void set__spellsSO_5(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_5 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_5), value);
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(BattleInventorySystem_t0388841BABEC0CD0D81917E7A76C29E1D03C6173, ____context_6)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_6() const { return ____context_6; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATTLEINVENTORYSYSTEM_T0388841BABEC0CD0D81917E7A76C29E1D03C6173_H
#ifndef CARTSIMULATIONINITSYSTEM_TD68B118F9AA4BF58BBF6C2EBD0770ED20648F0CD_H
#define CARTSIMULATIONINITSYSTEM_TD68B118F9AA4BF58BBF6C2EBD0770ED20648F0CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CartSimulationInitSystem
struct  CartSimulationInitSystem_tD68B118F9AA4BF58BBF6C2EBD0770ED20648F0CD  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext CartSimulationInitSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(CartSimulationInitSystem_tD68B118F9AA4BF58BBF6C2EBD0770ED20648F0CD, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARTSIMULATIONINITSYSTEM_TD68B118F9AA4BF58BBF6C2EBD0770ED20648F0CD_H
#ifndef COLLECTABLEDISPOSESYSTEM_T65DA70D4DE910A23164EE6A4F0C934C368AD923F_H
#define COLLECTABLEDISPOSESYSTEM_T65DA70D4DE910A23164EE6A4F0C934C368AD923F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectableDisposeSystem
struct  CollectableDisposeSystem_t65DA70D4DE910A23164EE6A4F0C934C368AD923F  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary CollectableDisposeSystem::_sfxLibrary
	RuntimeObject* ____sfxLibrary_3;
	// ItemsSO CollectableDisposeSystem::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_4;
	// GameContext CollectableDisposeSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__sfxLibrary_3() { return static_cast<int32_t>(offsetof(CollectableDisposeSystem_t65DA70D4DE910A23164EE6A4F0C934C368AD923F, ____sfxLibrary_3)); }
	inline RuntimeObject* get__sfxLibrary_3() const { return ____sfxLibrary_3; }
	inline RuntimeObject** get_address_of__sfxLibrary_3() { return &____sfxLibrary_3; }
	inline void set__sfxLibrary_3(RuntimeObject* value)
	{
		____sfxLibrary_3 = value;
		Il2CppCodeGenWriteBarrier((&____sfxLibrary_3), value);
	}

	inline static int32_t get_offset_of__itemsSO_4() { return static_cast<int32_t>(offsetof(CollectableDisposeSystem_t65DA70D4DE910A23164EE6A4F0C934C368AD923F, ____itemsSO_4)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_4() const { return ____itemsSO_4; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_4() { return &____itemsSO_4; }
	inline void set__itemsSO_4(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(CollectableDisposeSystem_t65DA70D4DE910A23164EE6A4F0C934C368AD923F, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTABLEDISPOSESYSTEM_T65DA70D4DE910A23164EE6A4F0C934C368AD923F_H
#ifndef COLLECTABLEHPSYSTEM_T26E4C78BD41D3AE2794769F1E52DC279628C4899_H
#define COLLECTABLEHPSYSTEM_T26E4C78BD41D3AE2794769F1E52DC279628C4899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollectableHPSystem
struct  CollectableHPSystem_t26E4C78BD41D3AE2794769F1E52DC279628C4899  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext CollectableHPSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(CollectableHPSystem_t26E4C78BD41D3AE2794769F1E52DC279628C4899, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTABLEHPSYSTEM_T26E4C78BD41D3AE2794769F1E52DC279628C4899_H
#ifndef DESTROYABLEDISPOSESYSTEM_TB9FF98ED1D2F72BF5187280E0D238E78BD59CC7D_H
#define DESTROYABLEDISPOSESYSTEM_TB9FF98ED1D2F72BF5187280E0D238E78BD59CC7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyableDisposeSystem
struct  DestroyableDisposeSystem_tB9FF98ED1D2F72BF5187280E0D238E78BD59CC7D  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Tayr.ILibrary DestroyableDisposeSystem::_sfxLibrary
	RuntimeObject* ____sfxLibrary_3;
	// ItemsSO DestroyableDisposeSystem::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_4;
	// GameContext DestroyableDisposeSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__sfxLibrary_3() { return static_cast<int32_t>(offsetof(DestroyableDisposeSystem_tB9FF98ED1D2F72BF5187280E0D238E78BD59CC7D, ____sfxLibrary_3)); }
	inline RuntimeObject* get__sfxLibrary_3() const { return ____sfxLibrary_3; }
	inline RuntimeObject** get_address_of__sfxLibrary_3() { return &____sfxLibrary_3; }
	inline void set__sfxLibrary_3(RuntimeObject* value)
	{
		____sfxLibrary_3 = value;
		Il2CppCodeGenWriteBarrier((&____sfxLibrary_3), value);
	}

	inline static int32_t get_offset_of__itemsSO_4() { return static_cast<int32_t>(offsetof(DestroyableDisposeSystem_tB9FF98ED1D2F72BF5187280E0D238E78BD59CC7D, ____itemsSO_4)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_4() const { return ____itemsSO_4; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_4() { return &____itemsSO_4; }
	inline void set__itemsSO_4(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(DestroyableDisposeSystem_tB9FF98ED1D2F72BF5187280E0D238E78BD59CC7D, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYABLEDISPOSESYSTEM_TB9FF98ED1D2F72BF5187280E0D238E78BD59CC7D_H
#ifndef DESTROYABLEHPSYSTEM_T4F253C30559F4F9F7B2E0471215BFFAE30180F95_H
#define DESTROYABLEHPSYSTEM_T4F253C30559F4F9F7B2E0471215BFFAE30180F95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyableHPSystem
struct  DestroyableHPSystem_t4F253C30559F4F9F7B2E0471215BFFAE30180F95  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext DestroyableHPSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(DestroyableHPSystem_t4F253C30559F4F9F7B2E0471215BFFAE30180F95, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYABLEHPSYSTEM_T4F253C30559F4F9F7B2E0471215BFFAE30180F95_H
#ifndef DESTROYABLERENDERSYSTEM_T8FF82FC1446C41D0292725921649E780D977A197_H
#define DESTROYABLERENDERSYSTEM_T8FF82FC1446C41D0292725921649E780D977A197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestroyableRenderSystem
struct  DestroyableRenderSystem_t8FF82FC1446C41D0292725921649E780D977A197  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Zenject.DiContainer DestroyableRenderSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_3;
	// ItemsSO DestroyableRenderSystem::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_4;
	// GameContext DestroyableRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__diContainer_3() { return static_cast<int32_t>(offsetof(DestroyableRenderSystem_t8FF82FC1446C41D0292725921649E780D977A197, ____diContainer_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_3() const { return ____diContainer_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_3() { return &____diContainer_3; }
	inline void set__diContainer_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_3), value);
	}

	inline static int32_t get_offset_of__itemsSO_4() { return static_cast<int32_t>(offsetof(DestroyableRenderSystem_t8FF82FC1446C41D0292725921649E780D977A197, ____itemsSO_4)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_4() const { return ____itemsSO_4; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_4() { return &____itemsSO_4; }
	inline void set__itemsSO_4(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(DestroyableRenderSystem_t8FF82FC1446C41D0292725921649E780D977A197, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTROYABLERENDERSYSTEM_T8FF82FC1446C41D0292725921649E780D977A197_H
#ifndef ENTITYINDEX_2_T0D431661D35F22FAE77B67E69AAB0C7232D99DFD_H
#define ENTITYINDEX_2_T0D431661D35F22FAE77B67E69AAB0C7232D99DFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.EntityIndex`2<GameEntity,System.String>
struct  EntityIndex_2_t0D431661D35F22FAE77B67E69AAB0C7232D99DFD  : public AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,System.Collections.Generic.HashSet`1<TEntity>> Entitas.EntityIndex`2::_index
	Dictionary_2_t8444F0CD0B98C58A471C68AF61E67B249C17DAE4 * ____index_5;

public:
	inline static int32_t get_offset_of__index_5() { return static_cast<int32_t>(offsetof(EntityIndex_2_t0D431661D35F22FAE77B67E69AAB0C7232D99DFD, ____index_5)); }
	inline Dictionary_2_t8444F0CD0B98C58A471C68AF61E67B249C17DAE4 * get__index_5() const { return ____index_5; }
	inline Dictionary_2_t8444F0CD0B98C58A471C68AF61E67B249C17DAE4 ** get_address_of__index_5() { return &____index_5; }
	inline void set__index_5(Dictionary_2_t8444F0CD0B98C58A471C68AF61E67B249C17DAE4 * value)
	{
		____index_5 = value;
		Il2CppCodeGenWriteBarrier((&____index_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYINDEX_2_T0D431661D35F22FAE77B67E69AAB0C7232D99DFD_H
#ifndef PRIMARYENTITYINDEX_2_T17175B118C000A97322D66364138713F2177167A_H
#define PRIMARYENTITYINDEX_2_T17175B118C000A97322D66364138713F2177167A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entitas.PrimaryEntityIndex`2<GameEntity,System.String>
struct  PrimaryEntityIndex_2_t17175B118C000A97322D66364138713F2177167A  : public AbstractEntityIndex_2_t21CBBEC4E85C6B3DF1A910297A15C295BA77FBEA
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TEntity> Entitas.PrimaryEntityIndex`2::_index
	Dictionary_2_t70CAAE41AA21DBE49EE58820FA11623347BDFA5D * ____index_5;

public:
	inline static int32_t get_offset_of__index_5() { return static_cast<int32_t>(offsetof(PrimaryEntityIndex_2_t17175B118C000A97322D66364138713F2177167A, ____index_5)); }
	inline Dictionary_2_t70CAAE41AA21DBE49EE58820FA11623347BDFA5D * get__index_5() const { return ____index_5; }
	inline Dictionary_2_t70CAAE41AA21DBE49EE58820FA11623347BDFA5D ** get_address_of__index_5() { return &____index_5; }
	inline void set__index_5(Dictionary_2_t70CAAE41AA21DBE49EE58820FA11623347BDFA5D * value)
	{
		____index_5 = value;
		Il2CppCodeGenWriteBarrier((&____index_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMARYENTITYINDEX_2_T17175B118C000A97322D66364138713F2177167A_H
#ifndef FREEINVENTORYSYSTEM_T707A738D5998E781FCA5DF90ED9C9DB819141CA9_H
#define FREEINVENTORYSYSTEM_T707A738D5998E781FCA5DF90ED9C9DB819141CA9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FreeInventorySystem
struct  FreeInventorySystem_t707A738D5998E781FCA5DF90ED9C9DB819141CA9  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext FreeInventorySystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(FreeInventorySystem_t707A738D5998E781FCA5DF90ED9C9DB819141CA9, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FREEINVENTORYSYSTEM_T707A738D5998E781FCA5DF90ED9C9DB819141CA9_H
#ifndef GAMECONTEXT_T7DA531FCF38581666B3642E3E737E5B057494978_H
#define GAMECONTEXT_T7DA531FCF38581666B3642E3E737E5B057494978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameContext
struct  GameContext_t7DA531FCF38581666B3642E3E737E5B057494978  : public Context_1_t51CD1A3566ACEF172A6D9EBA8D27910919BFFED5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMECONTEXT_T7DA531FCF38581666B3642E3E737E5B057494978_H
#ifndef HPBOOSTSYSTEM_TB75ED28170A5C07F2B5C323075C87C9B86EC8902_H
#define HPBOOSTSYSTEM_TB75ED28170A5C07F2B5C323075C87C9B86EC8902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HPBoostSystem
struct  HPBoostSystem_tB75ED28170A5C07F2B5C323075C87C9B86EC8902  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// GameContext HPBoostSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_3;

public:
	inline static int32_t get_offset_of__context_3() { return static_cast<int32_t>(offsetof(HPBoostSystem_tB75ED28170A5C07F2B5C323075C87C9B86EC8902, ____context_3)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_3() const { return ____context_3; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_3() { return &____context_3; }
	inline void set__context_3(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_3 = value;
		Il2CppCodeGenWriteBarrier((&____context_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HPBOOSTSYSTEM_TB75ED28170A5C07F2B5C323075C87C9B86EC8902_H
#ifndef HEROSIMULATIONINITSYSTEM_T79FB6A4327BF96063AB2F2C2C515F649F34ABAB7_H
#define HEROSIMULATIONINITSYSTEM_T79FB6A4327BF96063AB2F2C2C515F649F34ABAB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroSimulationInitSystem
struct  HeroSimulationInitSystem_t79FB6A4327BF96063AB2F2C2C515F649F34ABAB7  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// UnitsSO HeroSimulationInitSystem::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_3;
	// GameContext HeroSimulationInitSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_4;

public:
	inline static int32_t get_offset_of__unitsSO_3() { return static_cast<int32_t>(offsetof(HeroSimulationInitSystem_t79FB6A4327BF96063AB2F2C2C515F649F34ABAB7, ____unitsSO_3)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_3() const { return ____unitsSO_3; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_3() { return &____unitsSO_3; }
	inline void set__unitsSO_3(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(HeroSimulationInitSystem_t79FB6A4327BF96063AB2F2C2C515F649F34ABAB7, ____context_4)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_4() const { return ____context_4; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROSIMULATIONINITSYSTEM_T79FB6A4327BF96063AB2F2C2C515F649F34ABAB7_H
#ifndef ITEMINITIALIZERSYSTEM_T682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA_H
#define ITEMINITIALIZERSYSTEM_T682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ItemInitializerSystem
struct  ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Zenject.DiContainer ItemInitializerSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_3;
	// ChaptersSO ItemInitializerSystem::_chaptersSO
	ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * ____chaptersSO_4;
	// UserVO ItemInitializerSystem::_userVO
	UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * ____userVO_5;
	// ItemsSO ItemInitializerSystem::_itemsSO
	ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * ____itemsSO_6;
	// GameContext ItemInitializerSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_7;
	// System.Boolean ItemInitializerSystem::_initialized
	bool ____initialized_8;

public:
	inline static int32_t get_offset_of__diContainer_3() { return static_cast<int32_t>(offsetof(ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA, ____diContainer_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_3() const { return ____diContainer_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_3() { return &____diContainer_3; }
	inline void set__diContainer_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_3), value);
	}

	inline static int32_t get_offset_of__chaptersSO_4() { return static_cast<int32_t>(offsetof(ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA, ____chaptersSO_4)); }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * get__chaptersSO_4() const { return ____chaptersSO_4; }
	inline ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 ** get_address_of__chaptersSO_4() { return &____chaptersSO_4; }
	inline void set__chaptersSO_4(ChaptersSO_t6D25AB47CA5A386B1455B167AA805B0CAB2B24C1 * value)
	{
		____chaptersSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____chaptersSO_4), value);
	}

	inline static int32_t get_offset_of__userVO_5() { return static_cast<int32_t>(offsetof(ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA, ____userVO_5)); }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * get__userVO_5() const { return ____userVO_5; }
	inline UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 ** get_address_of__userVO_5() { return &____userVO_5; }
	inline void set__userVO_5(UserVO_tA99AB6795F202AC868B7E38E52B808DFAAD4DB94 * value)
	{
		____userVO_5 = value;
		Il2CppCodeGenWriteBarrier((&____userVO_5), value);
	}

	inline static int32_t get_offset_of__itemsSO_6() { return static_cast<int32_t>(offsetof(ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA, ____itemsSO_6)); }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * get__itemsSO_6() const { return ____itemsSO_6; }
	inline ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A ** get_address_of__itemsSO_6() { return &____itemsSO_6; }
	inline void set__itemsSO_6(ItemsSO_tD6DC35CB6DA44C6E8DB8EE3DC1795E5FE672F69A * value)
	{
		____itemsSO_6 = value;
		Il2CppCodeGenWriteBarrier((&____itemsSO_6), value);
	}

	inline static int32_t get_offset_of__context_7() { return static_cast<int32_t>(offsetof(ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA, ____context_7)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_7() const { return ____context_7; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_7() { return &____context_7; }
	inline void set__context_7(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_7 = value;
		Il2CppCodeGenWriteBarrier((&____context_7), value);
	}

	inline static int32_t get_offset_of__initialized_8() { return static_cast<int32_t>(offsetof(ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA, ____initialized_8)); }
	inline bool get__initialized_8() const { return ____initialized_8; }
	inline bool* get_address_of__initialized_8() { return &____initialized_8; }
	inline void set__initialized_8(bool value)
	{
		____initialized_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMINITIALIZERSYSTEM_T682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA_H
#ifndef PARENTSYSTEM_T99E0EB4A1E783D2C5B85440B6314253875C031CD_H
#define PARENTSYSTEM_T99E0EB4A1E783D2C5B85440B6314253875C031CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParentSystem
struct  ParentSystem_t99E0EB4A1E783D2C5B85440B6314253875C031CD  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// SpellsSO ParentSystem::_spellsSO
	SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * ____spellsSO_3;
	// UnitsSO ParentSystem::_unitsSO
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsSO_4;
	// GameContext ParentSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__spellsSO_3() { return static_cast<int32_t>(offsetof(ParentSystem_t99E0EB4A1E783D2C5B85440B6314253875C031CD, ____spellsSO_3)); }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * get__spellsSO_3() const { return ____spellsSO_3; }
	inline SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 ** get_address_of__spellsSO_3() { return &____spellsSO_3; }
	inline void set__spellsSO_3(SpellsSO_tD87C28C149EA2BE80535490B95DC914E94705439 * value)
	{
		____spellsSO_3 = value;
		Il2CppCodeGenWriteBarrier((&____spellsSO_3), value);
	}

	inline static int32_t get_offset_of__unitsSO_4() { return static_cast<int32_t>(offsetof(ParentSystem_t99E0EB4A1E783D2C5B85440B6314253875C031CD, ____unitsSO_4)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsSO_4() const { return ____unitsSO_4; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsSO_4() { return &____unitsSO_4; }
	inline void set__unitsSO_4(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsSO_4 = value;
		Il2CppCodeGenWriteBarrier((&____unitsSO_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(ParentSystem_t99E0EB4A1E783D2C5B85440B6314253875C031CD, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARENTSYSTEM_T99E0EB4A1E783D2C5B85440B6314253875C031CD_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef WALLSRENDERSYSTEM_TA2CB14676AB9B202260C1280B85F7BB48C576E09_H
#define WALLSRENDERSYSTEM_TA2CB14676AB9B202260C1280B85F7BB48C576E09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WallsRenderSystem
struct  WallsRenderSystem_tA2CB14676AB9B202260C1280B85F7BB48C576E09  : public ReactiveSystem_1_tD4874D09441436F0E3DB74FACC6C25A1F78AE0F9
{
public:
	// Zenject.DiContainer WallsRenderSystem::_diContainer
	DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * ____diContainer_3;
	// UnitsSO WallsRenderSystem::_unitsConf
	UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * ____unitsConf_4;
	// GameContext WallsRenderSystem::_context
	GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * ____context_5;

public:
	inline static int32_t get_offset_of__diContainer_3() { return static_cast<int32_t>(offsetof(WallsRenderSystem_tA2CB14676AB9B202260C1280B85F7BB48C576E09, ____diContainer_3)); }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * get__diContainer_3() const { return ____diContainer_3; }
	inline DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 ** get_address_of__diContainer_3() { return &____diContainer_3; }
	inline void set__diContainer_3(DiContainer_t7619E999A5CE72FEE4D2419403214E62D95FFFD5 * value)
	{
		____diContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&____diContainer_3), value);
	}

	inline static int32_t get_offset_of__unitsConf_4() { return static_cast<int32_t>(offsetof(WallsRenderSystem_tA2CB14676AB9B202260C1280B85F7BB48C576E09, ____unitsConf_4)); }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * get__unitsConf_4() const { return ____unitsConf_4; }
	inline UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 ** get_address_of__unitsConf_4() { return &____unitsConf_4; }
	inline void set__unitsConf_4(UnitsSO_t8A4174A0E155362BFFAA11BE367BD31F8D001423 * value)
	{
		____unitsConf_4 = value;
		Il2CppCodeGenWriteBarrier((&____unitsConf_4), value);
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(WallsRenderSystem_tA2CB14676AB9B202260C1280B85F7BB48C576E09, ____context_5)); }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * get__context_5() const { return ____context_5; }
	inline GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(GameContext_t7DA531FCF38581666B3642E3E737E5B057494978 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WALLSRENDERSYSTEM_TA2CB14676AB9B202260C1280B85F7BB48C576E09_H
#ifndef AGGROENTITYINDEX_TFD5B6C84243A68B34C6C72CD8E7F44197E3D95B6_H
#define AGGROENTITYINDEX_TFD5B6C84243A68B34C6C72CD8E7F44197E3D95B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AggroEntityIndex
struct  AggroEntityIndex_tFD5B6C84243A68B34C6C72CD8E7F44197E3D95B6  : public PrimaryEntityIndex_2_t17175B118C000A97322D66364138713F2177167A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGROENTITYINDEX_TFD5B6C84243A68B34C6C72CD8E7F44197E3D95B6_H
#ifndef AGGROSTATUS_T689854CB92C0114F54178D224750D28A7AC98792_H
#define AGGROSTATUS_T689854CB92C0114F54178D224750D28A7AC98792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AggroStatus
struct  AggroStatus_t689854CB92C0114F54178D224750D28A7AC98792 
{
public:
	// System.Int32 AggroStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AggroStatus_t689854CB92C0114F54178D224750D28A7AC98792, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGROSTATUS_T689854CB92C0114F54178D224750D28A7AC98792_H
#ifndef AGGROTYPE_T95F9D6050E6C7958C2641D26B5B14F62CCB402DE_H
#define AGGROTYPE_T95F9D6050E6C7958C2641D26B5B14F62CCB402DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AggroType
struct  AggroType_t95F9D6050E6C7958C2641D26B5B14F62CCB402DE 
{
public:
	// System.Int32 AggroType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AggroType_t95F9D6050E6C7958C2641D26B5B14F62CCB402DE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGROTYPE_T95F9D6050E6C7958C2641D26B5B14F62CCB402DE_H
#ifndef EFFECTBRAIN_T72F5BBABEE27713003E0583D6C88E6A84FE3DDFF_H
#define EFFECTBRAIN_T72F5BBABEE27713003E0583D6C88E6A84FE3DDFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectBrain
struct  EffectBrain_t72F5BBABEE27713003E0583D6C88E6A84FE3DDFF 
{
public:
	// System.Int32 EffectBrain::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EffectBrain_t72F5BBABEE27713003E0583D6C88E6A84FE3DDFF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTBRAIN_T72F5BBABEE27713003E0583D6C88E6A84FE3DDFF_H
#ifndef EFFECTENTITYINDEX_T59A025DFD4DA19386FABD7AF10ADAACA1EA5DB8D_H
#define EFFECTENTITYINDEX_T59A025DFD4DA19386FABD7AF10ADAACA1EA5DB8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectEntityIndex
struct  EffectEntityIndex_t59A025DFD4DA19386FABD7AF10ADAACA1EA5DB8D  : public EntityIndex_2_t0D431661D35F22FAE77B67E69AAB0C7232D99DFD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTENTITYINDEX_T59A025DFD4DA19386FABD7AF10ADAACA1EA5DB8D_H
#ifndef EFFECTTYPE_T001314E778DA097AFE0D47BF52477494AF02E0DF_H
#define EFFECTTYPE_T001314E778DA097AFE0D47BF52477494AF02E0DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectType
struct  EffectType_t001314E778DA097AFE0D47BF52477494AF02E0DF 
{
public:
	// System.Int32 EffectType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EffectType_t001314E778DA097AFE0D47BF52477494AF02E0DF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTTYPE_T001314E778DA097AFE0D47BF52477494AF02E0DF_H
#ifndef EFFECTVISUALENTITYINDEX_T0DAA3CD024E551E8FE984A555385BAEBBF21C7B7_H
#define EFFECTVISUALENTITYINDEX_T0DAA3CD024E551E8FE984A555385BAEBBF21C7B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectVisualEntityIndex
struct  EffectVisualEntityIndex_t0DAA3CD024E551E8FE984A555385BAEBBF21C7B7  : public EntityIndex_2_t0D431661D35F22FAE77B67E69AAB0C7232D99DFD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTVISUALENTITYINDEX_T0DAA3CD024E551E8FE984A555385BAEBBF21C7B7_H
#ifndef HEROSTATE_T0239366EFFF7A8EE7394B29AD8030D45334FD5BF_H
#define HEROSTATE_T0239366EFFF7A8EE7394B29AD8030D45334FD5BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroState
struct  HeroState_t0239366EFFF7A8EE7394B29AD8030D45334FD5BF 
{
public:
	// System.Int32 HeroState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HeroState_t0239366EFFF7A8EE7394B29AD8030D45334FD5BF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROSTATE_T0239366EFFF7A8EE7394B29AD8030D45334FD5BF_H
#ifndef INITPOSITIONCOMPONENT_TD76FE6A7C6932550717B63157DF3D9C9B524592D_H
#define INITPOSITIONCOMPONENT_TD76FE6A7C6932550717B63157DF3D9C9B524592D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitPositionComponent
struct  InitPositionComponent_tD76FE6A7C6932550717B63157DF3D9C9B524592D  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 InitPositionComponent::Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_0;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(InitPositionComponent_tD76FE6A7C6932550717B63157DF3D9C9B524592D, ___Position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Position_0() const { return ___Position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Position_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITPOSITIONCOMPONENT_TD76FE6A7C6932550717B63157DF3D9C9B524592D_H
#ifndef POSITIONCOMPONENT_TF4289DBFC54686B2E169B344877F548B633F923F_H
#define POSITIONCOMPONENT_TF4289DBFC54686B2E169B344877F548B633F923F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PositionComponent
struct  PositionComponent_tF4289DBFC54686B2E169B344877F548B633F923F  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 PositionComponent::Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_0;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(PositionComponent_tF4289DBFC54686B2E169B344877F548B633F923F, ___Position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Position_0() const { return ___Position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Position_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONCOMPONENT_TF4289DBFC54686B2E169B344877F548B633F923F_H
#ifndef PROJECTILECOMPONENT_TF75C5C97DB61835512EF17B45EF1025CF7B8279A_H
#define PROJECTILECOMPONENT_TF75C5C97DB61835512EF17B45EF1025CF7B8279A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileComponent
struct  ProjectileComponent_tF75C5C97DB61835512EF17B45EF1025CF7B8279A  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 ProjectileComponent::StartPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___StartPosition_0;
	// UnityEngine.Vector3 ProjectileComponent::EndPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___EndPosition_1;
	// System.Single ProjectileComponent::Speed
	float ___Speed_2;

public:
	inline static int32_t get_offset_of_StartPosition_0() { return static_cast<int32_t>(offsetof(ProjectileComponent_tF75C5C97DB61835512EF17B45EF1025CF7B8279A, ___StartPosition_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_StartPosition_0() const { return ___StartPosition_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_StartPosition_0() { return &___StartPosition_0; }
	inline void set_StartPosition_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___StartPosition_0 = value;
	}

	inline static int32_t get_offset_of_EndPosition_1() { return static_cast<int32_t>(offsetof(ProjectileComponent_tF75C5C97DB61835512EF17B45EF1025CF7B8279A, ___EndPosition_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_EndPosition_1() const { return ___EndPosition_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_EndPosition_1() { return &___EndPosition_1; }
	inline void set_EndPosition_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___EndPosition_1 = value;
	}

	inline static int32_t get_offset_of_Speed_2() { return static_cast<int32_t>(offsetof(ProjectileComponent_tF75C5C97DB61835512EF17B45EF1025CF7B8279A, ___Speed_2)); }
	inline float get_Speed_2() const { return ___Speed_2; }
	inline float* get_address_of_Speed_2() { return &___Speed_2; }
	inline void set_Speed_2(float value)
	{
		___Speed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILECOMPONENT_TF75C5C97DB61835512EF17B45EF1025CF7B8279A_H
#ifndef PROJECTILESTARTPOSITIONCOMPONENT_T05ACF56B1866B2968D14D8171ABA3774E697B9D5_H
#define PROJECTILESTARTPOSITIONCOMPONENT_T05ACF56B1866B2968D14D8171ABA3774E697B9D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProjectileStartPositionComponent
struct  ProjectileStartPositionComponent_t05ACF56B1866B2968D14D8171ABA3774E697B9D5  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 ProjectileStartPositionComponent::Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_0;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(ProjectileStartPositionComponent_t05ACF56B1866B2968D14D8171ABA3774E697B9D5, ___Position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Position_0() const { return ___Position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Position_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILESTARTPOSITIONCOMPONENT_T05ACF56B1866B2968D14D8171ABA3774E697B9D5_H
#ifndef ROTATIONCOMPONENT_T227795A451188F6642FA72A0AB4428BDAF8402E6_H
#define ROTATIONCOMPONENT_T227795A451188F6642FA72A0AB4428BDAF8402E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotationComponent
struct  RotationComponent_t227795A451188F6642FA72A0AB4428BDAF8402E6  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 RotationComponent::Rotation
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Rotation_0;

public:
	inline static int32_t get_offset_of_Rotation_0() { return static_cast<int32_t>(offsetof(RotationComponent_t227795A451188F6642FA72A0AB4428BDAF8402E6, ___Rotation_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Rotation_0() const { return ___Rotation_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Rotation_0() { return &___Rotation_0; }
	inline void set_Rotation_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Rotation_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONCOMPONENT_T227795A451188F6642FA72A0AB4428BDAF8402E6_H
#ifndef SPHERECOLLIDERCOMPONENT_T6893215877F78218811A9C9E11BF0CAB9F4AD965_H
#define SPHERECOLLIDERCOMPONENT_T6893215877F78218811A9C9E11BF0CAB9F4AD965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SphereColliderComponent
struct  SphereColliderComponent_t6893215877F78218811A9C9E11BF0CAB9F4AD965  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 SphereColliderComponent::Center
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Center_0;
	// System.Single SphereColliderComponent::Radius
	float ___Radius_1;

public:
	inline static int32_t get_offset_of_Center_0() { return static_cast<int32_t>(offsetof(SphereColliderComponent_t6893215877F78218811A9C9E11BF0CAB9F4AD965, ___Center_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Center_0() const { return ___Center_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Center_0() { return &___Center_0; }
	inline void set_Center_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Center_0 = value;
	}

	inline static int32_t get_offset_of_Radius_1() { return static_cast<int32_t>(offsetof(SphereColliderComponent_t6893215877F78218811A9C9E11BF0CAB9F4AD965, ___Radius_1)); }
	inline float get_Radius_1() const { return ___Radius_1; }
	inline float* get_address_of_Radius_1() { return &___Radius_1; }
	inline void set_Radius_1(float value)
	{
		___Radius_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPHERECOLLIDERCOMPONENT_T6893215877F78218811A9C9E11BF0CAB9F4AD965_H
#ifndef AGGROSTATUSCOMPONENT_TDA93EA23767E53BA592C8E7BCC44301615595200_H
#define AGGROSTATUSCOMPONENT_TDA93EA23767E53BA592C8E7BCC44301615595200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AggroStatusComponent
struct  AggroStatusComponent_tDA93EA23767E53BA592C8E7BCC44301615595200  : public RuntimeObject
{
public:
	// AggroStatus AggroStatusComponent::AggroStatus
	int32_t ___AggroStatus_0;

public:
	inline static int32_t get_offset_of_AggroStatus_0() { return static_cast<int32_t>(offsetof(AggroStatusComponent_tDA93EA23767E53BA592C8E7BCC44301615595200, ___AggroStatus_0)); }
	inline int32_t get_AggroStatus_0() const { return ___AggroStatus_0; }
	inline int32_t* get_address_of_AggroStatus_0() { return &___AggroStatus_0; }
	inline void set_AggroStatus_0(int32_t value)
	{
		___AggroStatus_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGROSTATUSCOMPONENT_TDA93EA23767E53BA592C8E7BCC44301615595200_H
#ifndef AGGROTYPECOMPONENT_T8CCBA4ED1B9DABD2CA36178888AD642D30B152D9_H
#define AGGROTYPECOMPONENT_T8CCBA4ED1B9DABD2CA36178888AD642D30B152D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AggroTypeComponent
struct  AggroTypeComponent_t8CCBA4ED1B9DABD2CA36178888AD642D30B152D9  : public RuntimeObject
{
public:
	// AggroType AggroTypeComponent::Type
	int32_t ___Type_0;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(AggroTypeComponent_t8CCBA4ED1B9DABD2CA36178888AD642D30B152D9, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGGROTYPECOMPONENT_T8CCBA4ED1B9DABD2CA36178888AD642D30B152D9_H
#ifndef AOEEFFECTAFTERTIMECOMPONENT_T0C05925B356F014E57064B8360F6503E2A1223FB_H
#define AOEEFFECTAFTERTIMECOMPONENT_T0C05925B356F014E57064B8360F6503E2A1223FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AoeEffectAfterTimeComponent
struct  AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB  : public RuntimeObject
{
public:
	// System.String AoeEffectAfterTimeComponent::SourceId
	String_t* ___SourceId_0;
	// System.Collections.Generic.List`1<EffectModel> AoeEffectAfterTimeComponent::Effects
	List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB * ___Effects_1;
	// System.Single AoeEffectAfterTimeComponent::StartTime
	float ___StartTime_2;
	// AggroType AoeEffectAfterTimeComponent::AggroType
	int32_t ___AggroType_3;
	// UnityEngine.Vector3 AoeEffectAfterTimeComponent::Position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Position_4;
	// System.Single AoeEffectAfterTimeComponent::Radius
	float ___Radius_5;

public:
	inline static int32_t get_offset_of_SourceId_0() { return static_cast<int32_t>(offsetof(AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB, ___SourceId_0)); }
	inline String_t* get_SourceId_0() const { return ___SourceId_0; }
	inline String_t** get_address_of_SourceId_0() { return &___SourceId_0; }
	inline void set_SourceId_0(String_t* value)
	{
		___SourceId_0 = value;
		Il2CppCodeGenWriteBarrier((&___SourceId_0), value);
	}

	inline static int32_t get_offset_of_Effects_1() { return static_cast<int32_t>(offsetof(AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB, ___Effects_1)); }
	inline List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB * get_Effects_1() const { return ___Effects_1; }
	inline List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB ** get_address_of_Effects_1() { return &___Effects_1; }
	inline void set_Effects_1(List_1_t659C06110FB03B1EF961FEA5BF138ABD36CE9ABB * value)
	{
		___Effects_1 = value;
		Il2CppCodeGenWriteBarrier((&___Effects_1), value);
	}

	inline static int32_t get_offset_of_StartTime_2() { return static_cast<int32_t>(offsetof(AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB, ___StartTime_2)); }
	inline float get_StartTime_2() const { return ___StartTime_2; }
	inline float* get_address_of_StartTime_2() { return &___StartTime_2; }
	inline void set_StartTime_2(float value)
	{
		___StartTime_2 = value;
	}

	inline static int32_t get_offset_of_AggroType_3() { return static_cast<int32_t>(offsetof(AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB, ___AggroType_3)); }
	inline int32_t get_AggroType_3() const { return ___AggroType_3; }
	inline int32_t* get_address_of_AggroType_3() { return &___AggroType_3; }
	inline void set_AggroType_3(int32_t value)
	{
		___AggroType_3 = value;
	}

	inline static int32_t get_offset_of_Position_4() { return static_cast<int32_t>(offsetof(AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB, ___Position_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Position_4() const { return ___Position_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Position_4() { return &___Position_4; }
	inline void set_Position_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Position_4 = value;
	}

	inline static int32_t get_offset_of_Radius_5() { return static_cast<int32_t>(offsetof(AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB, ___Radius_5)); }
	inline float get_Radius_5() const { return ___Radius_5; }
	inline float* get_address_of_Radius_5() { return &___Radius_5; }
	inline void set_Radius_5(float value)
	{
		___Radius_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AOEEFFECTAFTERTIMECOMPONENT_T0C05925B356F014E57064B8360F6503E2A1223FB_H
#ifndef EFFECTCOMPONENT_T3AC520D6D5C4DD39EB7389A679804E6DC6DF3710_H
#define EFFECTCOMPONENT_T3AC520D6D5C4DD39EB7389A679804E6DC6DF3710_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectComponent
struct  EffectComponent_t3AC520D6D5C4DD39EB7389A679804E6DC6DF3710  : public RuntimeObject
{
public:
	// System.String EffectComponent::EffectTargetId
	String_t* ___EffectTargetId_0;
	// EffectType EffectComponent::Type
	int32_t ___Type_1;

public:
	inline static int32_t get_offset_of_EffectTargetId_0() { return static_cast<int32_t>(offsetof(EffectComponent_t3AC520D6D5C4DD39EB7389A679804E6DC6DF3710, ___EffectTargetId_0)); }
	inline String_t* get_EffectTargetId_0() const { return ___EffectTargetId_0; }
	inline String_t** get_address_of_EffectTargetId_0() { return &___EffectTargetId_0; }
	inline void set_EffectTargetId_0(String_t* value)
	{
		___EffectTargetId_0 = value;
		Il2CppCodeGenWriteBarrier((&___EffectTargetId_0), value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(EffectComponent_t3AC520D6D5C4DD39EB7389A679804E6DC6DF3710, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTCOMPONENT_T3AC520D6D5C4DD39EB7389A679804E6DC6DF3710_H
#ifndef HEROSTATECOMPONENT_T032E8FC6E1F74CC1F7AC2B2C5C105FB0732103C7_H
#define HEROSTATECOMPONENT_T032E8FC6E1F74CC1F7AC2B2C5C105FB0732103C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HeroStateComponent
struct  HeroStateComponent_t032E8FC6E1F74CC1F7AC2B2C5C105FB0732103C7  : public RuntimeObject
{
public:
	// HeroState HeroStateComponent::HeroState
	int32_t ___HeroState_0;

public:
	inline static int32_t get_offset_of_HeroState_0() { return static_cast<int32_t>(offsetof(HeroStateComponent_t032E8FC6E1F74CC1F7AC2B2C5C105FB0732103C7, ___HeroState_0)); }
	inline int32_t get_HeroState_0() const { return ___HeroState_0; }
	inline int32_t* get_address_of_HeroState_0() { return &___HeroState_0; }
	inline void set_HeroState_0(int32_t value)
	{
		___HeroState_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEROSTATECOMPONENT_T032E8FC6E1F74CC1F7AC2B2C5C105FB0732103C7_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8200 = { sizeof (DestroyableRenderSystem_t8FF82FC1446C41D0292725921649E780D977A197), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8200[3] = 
{
	DestroyableRenderSystem_t8FF82FC1446C41D0292725921649E780D977A197::get_offset_of__diContainer_3(),
	DestroyableRenderSystem_t8FF82FC1446C41D0292725921649E780D977A197::get_offset_of__itemsSO_4(),
	DestroyableRenderSystem_t8FF82FC1446C41D0292725921649E780D977A197::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8201 = { sizeof (WallsRenderSystem_tA2CB14676AB9B202260C1280B85F7BB48C576E09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8201[3] = 
{
	WallsRenderSystem_tA2CB14676AB9B202260C1280B85F7BB48C576E09::get_offset_of__diContainer_3(),
	WallsRenderSystem_tA2CB14676AB9B202260C1280B85F7BB48C576E09::get_offset_of__unitsConf_4(),
	WallsRenderSystem_tA2CB14676AB9B202260C1280B85F7BB48C576E09::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8202 = { sizeof (AggroComponent_tC4E7DDFC08A5DBA09324B31CADF7056BB194AD70), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8202[2] = 
{
	AggroComponent_tC4E7DDFC08A5DBA09324B31CADF7056BB194AD70::get_offset_of_AggroSource_0(),
	AggroComponent_tC4E7DDFC08A5DBA09324B31CADF7056BB194AD70::get_offset_of_AggroTarget_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8203 = { sizeof (AggroTypeComponent_t8CCBA4ED1B9DABD2CA36178888AD642D30B152D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8203[1] = 
{
	AggroTypeComponent_t8CCBA4ED1B9DABD2CA36178888AD642D30B152D9::get_offset_of_Type_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8204 = { sizeof (AggroStatusComponent_tDA93EA23767E53BA592C8E7BCC44301615595200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8204[1] = 
{
	AggroStatusComponent_tDA93EA23767E53BA592C8E7BCC44301615595200::get_offset_of_AggroStatus_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8205 = { sizeof (AggroRangeComponent_t1234630F54F9674A43A4CEBC601AAAAFDF5855BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8205[1] = 
{
	AggroRangeComponent_t1234630F54F9674A43A4CEBC601AAAAFDF5855BD::get_offset_of_Range_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8206 = { sizeof (AggroWidthComponent_tB130BE6C7343E564DD714CD81487E1E22E4F1CF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8206[1] = 
{
	AggroWidthComponent_tB130BE6C7343E564DD714CD81487E1E22E4F1CF5::get_offset_of_Width_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8207 = { sizeof (AggroHeightComponent_t7E39BE68860E4F396567CC6F766CE6A365970EE9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8207[1] = 
{
	AggroHeightComponent_t7E39BE68860E4F396567CC6F766CE6A365970EE9::get_offset_of_Height_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8208 = { sizeof (AggroDisabledComponent_t7D0F3FF57D659CAF0C81F2D14851E44388B01278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8209 = { sizeof (AggroType_t95F9D6050E6C7958C2641D26B5B14F62CCB402DE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8209[7] = 
{
	AggroType_t95F9D6050E6C7958C2641D26B5B14F62CCB402DE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8210 = { sizeof (AggroStatus_t689854CB92C0114F54178D224750D28A7AC98792)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8210[3] = 
{
	AggroStatus_t689854CB92C0114F54178D224750D28A7AC98792::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8211 = { sizeof (AggroEntityIndex_tFD5B6C84243A68B34C6C72CD8E7F44197E3D95B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8212 = { sizeof (U3CU3Ec_t73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836), -1, sizeof(U3CU3Ec_t73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8212[2] = 
{
	U3CU3Ec_t73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t73B4D33BEFB4E5CCD303BAFA9F521CF1AB344836_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8213 = { sizeof (AggroSystem_tE4911ABA2708115BE8340DE3610B2FD7739D257F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8213[2] = 
{
	AggroSystem_tE4911ABA2708115BE8340DE3610B2FD7739D257F::get_offset_of__entities_0(),
	AggroSystem_tE4911ABA2708115BE8340DE3610B2FD7739D257F::get_offset_of__context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8214 = { sizeof (BattleUtils_tD58E1F3D1195331D9E9EA6A797CDE15BC69F74FD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8215 = { sizeof (CartSimulationInitSystem_tD68B118F9AA4BF58BBF6C2EBD0770ED20648F0CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8215[1] = 
{
	CartSimulationInitSystem_tD68B118F9AA4BF58BBF6C2EBD0770ED20648F0CD::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8216 = { sizeof (AttackFrequencyComponent_t05854E6C4265683677299D54D58D11DFD00B0615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8216[1] = 
{
	AttackFrequencyComponent_t05854E6C4265683677299D54D58D11DFD00B0615::get_offset_of_Frequency_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8217 = { sizeof (LastAttackTimeComponent_t9094D0E33E52610B5BC2A4EBA93EFA0C85F5A5C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8217[1] = 
{
	LastAttackTimeComponent_t9094D0E33E52610B5BC2A4EBA93EFA0C85F5A5C7::get_offset_of_LastAttackTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8218 = { sizeof (SphereColliderComponent_t6893215877F78218811A9C9E11BF0CAB9F4AD965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8218[2] = 
{
	SphereColliderComponent_t6893215877F78218811A9C9E11BF0CAB9F4AD965::get_offset_of_Center_0(),
	SphereColliderComponent_t6893215877F78218811A9C9E11BF0CAB9F4AD965::get_offset_of_Radius_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8219 = { sizeof (DurationComponent_t5EA49F82756EB70253987896EEBB49DEFCF17BA8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8219[1] = 
{
	DurationComponent_t5EA49F82756EB70253987896EEBB49DEFCF17BA8::get_offset_of_Duration_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8220 = { sizeof (GroupComponent_tDBD3E652B21077AAEB65B6E3EC66A7DE368CD9E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8220[1] = 
{
	GroupComponent_tDBD3E652B21077AAEB65B6E3EC66A7DE368CD9E6::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8221 = { sizeof (IdComponent_tF5656CA226F5AC319A7997E076D307136C3A1AD0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8221[1] = 
{
	IdComponent_tF5656CA226F5AC319A7997E076D307136C3A1AD0::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8222 = { sizeof (StartTimeComponent_t0A824916666C94123FC59BF33037692B17702341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8222[1] = 
{
	StartTimeComponent_t0A824916666C94123FC59BF33037692B17702341::get_offset_of_Time_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8223 = { sizeof (DisposeComponent_t9E94EC36DF1BD2E19774C102A3F0D68C63D09B4E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8224 = { sizeof (DisposeAfterTimeComponent_tA6153BD55D472EE89FDC5B31D185A5CEC2459918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8224[1] = 
{
	DisposeAfterTimeComponent_tA6153BD55D472EE89FDC5B31D185A5CEC2459918::get_offset_of_DisposeTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8225 = { sizeof (DisposeAfterTimeSystem_t285CCA2194110C702896043B2A01E7EA07C57449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8225[2] = 
{
	DisposeAfterTimeSystem_t285CCA2194110C702896043B2A01E7EA07C57449::get_offset_of__context_0(),
	DisposeAfterTimeSystem_t285CCA2194110C702896043B2A01E7EA07C57449::get_offset_of__group_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8226 = { sizeof (DisposeSystem_t664F1BB636A63202F0F23F228A16DCEDDDCE4396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8226[2] = 
{
	DisposeSystem_t664F1BB636A63202F0F23F228A16DCEDDDCE4396::get_offset_of__entities_0(),
	DisposeSystem_t664F1BB636A63202F0F23F228A16DCEDDDCE4396::get_offset_of__context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8227 = { sizeof (AttackBoostComponent_t9F90E34CD2CF7CED312B3080AD0F59822ED6A45D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8227[1] = 
{
	AttackBoostComponent_t9F90E34CD2CF7CED312B3080AD0F59822ED6A45D::get_offset_of_Percentage_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8228 = { sizeof (DamageComponent_tC393EB56208D744022CC7AA17CD9356F0393BEE1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8228[1] = 
{
	DamageComponent_tC393EB56208D744022CC7AA17CD9356F0393BEE1::get_offset_of_Value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8229 = { sizeof (EffectComponent_t3AC520D6D5C4DD39EB7389A679804E6DC6DF3710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8229[2] = 
{
	EffectComponent_t3AC520D6D5C4DD39EB7389A679804E6DC6DF3710::get_offset_of_EffectTargetId_0(),
	EffectComponent_t3AC520D6D5C4DD39EB7389A679804E6DC6DF3710::get_offset_of_Type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8230 = { sizeof (EffectSourceComponent_t9B444B16C43EE277876EECF4B4A22EEF194D7926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8230[1] = 
{
	EffectSourceComponent_t9B444B16C43EE277876EECF4B4A22EEF194D7926::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8231 = { sizeof (EffectStartTimeComponent_tE0A38FBD16E344E9F0413E139A2FD73B3AABE953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8231[1] = 
{
	EffectStartTimeComponent_tE0A38FBD16E344E9F0413E139A2FD73B3AABE953::get_offset_of_StartTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8232 = { sizeof (InstantEffectComponent_t208614FDD7E97EAFE28016B6B16193058AFFFFCD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8233 = { sizeof (LifetimeEffectComponent_t98B4DA278030322AAB0EDCB13EFFEFCA6510AFC5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8233[1] = 
{
	LifetimeEffectComponent_t98B4DA278030322AAB0EDCB13EFFEFCA6510AFC5::get_offset_of_Duration_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8234 = { sizeof (EffectStartedComponent_tA442FC7E8A7696782A04C2786BEBE9EF91669E4A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8235 = { sizeof (AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8235[6] = 
{
	AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB::get_offset_of_SourceId_0(),
	AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB::get_offset_of_Effects_1(),
	AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB::get_offset_of_StartTime_2(),
	AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB::get_offset_of_AggroType_3(),
	AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB::get_offset_of_Position_4(),
	AoeEffectAfterTimeComponent_t0C05925B356F014E57064B8360F6503E2A1223FB::get_offset_of_Radius_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8236 = { sizeof (EffectVisualComponent_t396C04D6E6EBD847ED82F26CDB95BAFFC23744EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8236[1] = 
{
	EffectVisualComponent_t396C04D6E6EBD847ED82F26CDB95BAFFC23744EE::get_offset_of_VisualId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8237 = { sizeof (EffectBrain_t72F5BBABEE27713003E0583D6C88E6A84FE3DDFF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8237[3] = 
{
	EffectBrain_t72F5BBABEE27713003E0583D6C88E6A84FE3DDFF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8238 = { sizeof (EffectEntityIndex_t59A025DFD4DA19386FABD7AF10ADAACA1EA5DB8D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8239 = { sizeof (U3CU3Ec_t4F94D0E01716A541C3D9FD34514A044B2B842AF7), -1, sizeof(U3CU3Ec_t4F94D0E01716A541C3D9FD34514A044B2B842AF7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8239[2] = 
{
	U3CU3Ec_t4F94D0E01716A541C3D9FD34514A044B2B842AF7_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4F94D0E01716A541C3D9FD34514A044B2B842AF7_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8240 = { sizeof (EffectVisualEntityIndex_t0DAA3CD024E551E8FE984A555385BAEBBF21C7B7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8241 = { sizeof (U3CU3Ec_t35ED9DBC98F6098490FE486A7ECDC5175A9E0766), -1, sizeof(U3CU3Ec_t35ED9DBC98F6098490FE486A7ECDC5175A9E0766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8241[2] = 
{
	U3CU3Ec_t35ED9DBC98F6098490FE486A7ECDC5175A9E0766_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t35ED9DBC98F6098490FE486A7ECDC5175A9E0766_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8242 = { sizeof (HPBoostComponent_tDE83D6C582D78D86372B11F0AB2E4F28F300A280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8242[1] = 
{
	HPBoostComponent_tDE83D6C582D78D86372B11F0AB2E4F28F300A280::get_offset_of_Percentage_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8243 = { sizeof (HPComponent_tD0E8C55BCF778CABB9AAD595E51321D549CEE55E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8243[1] = 
{
	HPComponent_tD0E8C55BCF778CABB9AAD595E51321D549CEE55E::get_offset_of_Value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8244 = { sizeof (MaxHPComponent_t45EB581E73D7696CE765C8DA5DA0407DFAEF6FC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8244[1] = 
{
	MaxHPComponent_t45EB581E73D7696CE765C8DA5DA0407DFAEF6FC7::get_offset_of_Value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8245 = { sizeof (ResistComponent_t7477E2D3BFDF6A316439E4260B7D657F92CFBCB8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8245[1] = 
{
	ResistComponent_t7477E2D3BFDF6A316439E4260B7D657F92CFBCB8::get_offset_of_Percentage_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8246 = { sizeof (SlowSpeedComponent_tABDA51B9E7584F28AE97E6045E67BEF778537113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8246[1] = 
{
	SlowSpeedComponent_tABDA51B9E7584F28AE97E6045E67BEF778537113::get_offset_of_Multiplier_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8247 = { sizeof (SpeedBoostComponent_t7C7CA4CBFFB61152862651967A60BD4CCD1BA7DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8247[1] = 
{
	SpeedBoostComponent_t7C7CA4CBFFB61152862651967A60BD4CCD1BA7DD::get_offset_of_Multiplier_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8248 = { sizeof (AttackBoostSystem_tA8E999E5111F0EAFD3906573E8C1750BEA9DDD73), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8248[1] = 
{
	AttackBoostSystem_tA8E999E5111F0EAFD3906573E8C1750BEA9DDD73::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8249 = { sizeof (InstantEffectSystem_tCC7E89D58693487A0E400269BDAFB24AFEBE35B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8249[2] = 
{
	InstantEffectSystem_tCC7E89D58693487A0E400269BDAFB24AFEBE35B8::get_offset_of__entities_0(),
	InstantEffectSystem_tCC7E89D58693487A0E400269BDAFB24AFEBE35B8::get_offset_of__context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8250 = { sizeof (LifetimeEffectSystem_t662707447DE8CB6CE7FBD64935D372759142AC58), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8250[2] = 
{
	LifetimeEffectSystem_t662707447DE8CB6CE7FBD64935D372759142AC58::get_offset_of__entities_0(),
	LifetimeEffectSystem_t662707447DE8CB6CE7FBD64935D372759142AC58::get_offset_of__context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8251 = { sizeof (CreateAoeEffectAfterTimeSystem_tC9DD91E68CBE6F52E1E80D8C538303F37E238919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8251[2] = 
{
	CreateAoeEffectAfterTimeSystem_tC9DD91E68CBE6F52E1E80D8C538303F37E238919::get_offset_of__context_0(),
	CreateAoeEffectAfterTimeSystem_tC9DD91E68CBE6F52E1E80D8C538303F37E238919::get_offset_of__entities_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8252 = { sizeof (HPBoostSystem_tB75ED28170A5C07F2B5C323075C87C9B86EC8902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8252[1] = 
{
	HPBoostSystem_tB75ED28170A5C07F2B5C323075C87C9B86EC8902::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8253 = { sizeof (DamageUtils_t5E9925494E0670ADB27EDF53BC0AA1CAFEBAB411), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8254 = { sizeof (GameContext_t7DA531FCF38581666B3642E3E737E5B057494978), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8255 = { sizeof (U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6), -1, sizeof(U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8255[3] = 
{
	U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6_StaticFields::get_offset_of_U3CU3E9__34_0_1(),
	U3CU3Ec_t50CA66C109446C18409AC5FFBF85FD47BE10ECA6_StaticFields::get_offset_of_U3CU3E9__34_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8256 = { sizeof (HeroSimulationInitSystem_t79FB6A4327BF96063AB2F2C2C515F649F34ABAB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8256[2] = 
{
	HeroSimulationInitSystem_t79FB6A4327BF96063AB2F2C2C515F649F34ABAB7::get_offset_of__unitsSO_3(),
	HeroSimulationInitSystem_t79FB6A4327BF96063AB2F2C2C515F649F34ABAB7::get_offset_of__context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8257 = { sizeof (LeftHandComponent_tE33AB8CAB06C15A6A157653A52A3C1964681A287), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8257[1] = 
{
	LeftHandComponent_tE33AB8CAB06C15A6A157653A52A3C1964681A287::get_offset_of_GameObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8258 = { sizeof (SpeedComponent_t175BE06C0E94BC2E2C40329096447D4606ED27B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8258[1] = 
{
	SpeedComponent_t175BE06C0E94BC2E2C40329096447D4606ED27B7::get_offset_of_Speed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8259 = { sizeof (HeroStateComponent_t032E8FC6E1F74CC1F7AC2B2C5C105FB0732103C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8259[1] = 
{
	HeroStateComponent_t032E8FC6E1F74CC1F7AC2B2C5C105FB0732103C7::get_offset_of_HeroState_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8260 = { sizeof (HeroState_t0239366EFFF7A8EE7394B29AD8030D45334FD5BF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8260[3] = 
{
	HeroState_t0239366EFFF7A8EE7394B29AD8030D45334FD5BF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8261 = { sizeof (HeroMovementSystem_tD86CE8646EA52823A31A6D2E5FB3BDF2AF9CACC4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8261[1] = 
{
	HeroMovementSystem_tD86CE8646EA52823A31A6D2E5FB3BDF2AF9CACC4::get_offset_of__context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8262 = { sizeof (DropCollectableSystem_t97A7F164425D8F02D672597EE5D05D82DC294592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8262[1] = 
{
	DropCollectableSystem_t97A7F164425D8F02D672597EE5D05D82DC294592::get_offset_of__context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8263 = { sizeof (HeroMainAttackSystem_tFA14FDA086F42BCA97E038E1EE1DBA26B30FB540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8263[2] = 
{
	HeroMainAttackSystem_tFA14FDA086F42BCA97E038E1EE1DBA26B30FB540::get_offset_of__spellsSO_0(),
	HeroMainAttackSystem_tFA14FDA086F42BCA97E038E1EE1DBA26B30FB540::get_offset_of__context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8264 = { sizeof (InventoryComponent_tEC537ECD5B1A9A2CDE9CBC13D9BD7AC836B8D3AE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8264[1] = 
{
	InventoryComponent_tEC537ECD5B1A9A2CDE9CBC13D9BD7AC836B8D3AE::get_offset_of_MaxSize_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8265 = { sizeof (InventoryItemsComponent_tDA26609A9DCDAF43BBAD2ECF90E2C2E7C2BD5C95), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8265[1] = 
{
	InventoryItemsComponent_tDA26609A9DCDAF43BBAD2ECF90E2C2E7C2BD5C95::get_offset_of_Items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8266 = { sizeof (InventoryItemComponent_t4DCE5075CF2CDD4ECDB7BBEF04C3A6A65E49DBA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8266[1] = 
{
	InventoryItemComponent_t4DCE5075CF2CDD4ECDB7BBEF04C3A6A65E49DBA5::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8267 = { sizeof (InventoryIndexComponent_t03BD28736D2BA341CDE29FC3111FCB278A08047A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8267[1] = 
{
	InventoryIndexComponent_t03BD28736D2BA341CDE29FC3111FCB278A08047A::get_offset_of_Index_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8268 = { sizeof (FreeInventory_tAF72E5E048627F4747D2262169130958BB47C6F7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8269 = { sizeof (InventoryNormalBoostReady_tA102FD9FD0D4930E683BB4ACCC2BF89AB9BE11A7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8270 = { sizeof (InventoryMegaBoostReady_tB4A18852E6C6C84F1C2B9F9029C48398522FBBE3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8271 = { sizeof (BattleInventorySystem_t0388841BABEC0CD0D81917E7A76C29E1D03C6173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8271[4] = 
{
	BattleInventorySystem_t0388841BABEC0CD0D81917E7A76C29E1D03C6173::get_offset_of__battleSettingsSO_3(),
	BattleInventorySystem_t0388841BABEC0CD0D81917E7A76C29E1D03C6173::get_offset_of__itemsSO_4(),
	BattleInventorySystem_t0388841BABEC0CD0D81917E7A76C29E1D03C6173::get_offset_of__spellsSO_5(),
	BattleInventorySystem_t0388841BABEC0CD0D81917E7A76C29E1D03C6173::get_offset_of__context_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8272 = { sizeof (FreeInventorySystem_t707A738D5998E781FCA5DF90ED9C9DB819141CA9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8272[1] = 
{
	FreeInventorySystem_t707A738D5998E781FCA5DF90ED9C9DB819141CA9::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8273 = { sizeof (ItemComponent_t44957705DF3CCE8E1D7FCBFE244B1DF5D455824D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8273[1] = 
{
	ItemComponent_t44957705DF3CCE8E1D7FCBFE244B1DF5D455824D::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8274 = { sizeof (CollectableComponent_t0BD7C71F3589B884C11505B8D6D784632FCF1A5A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8275 = { sizeof (CollectedComponent_t4D9F276C5E67C78D80D5C0BCC78D160B2BE7E9D1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8275[1] = 
{
	CollectedComponent_t4D9F276C5E67C78D80D5C0BCC78D160B2BE7E9D1::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8276 = { sizeof (DropCollectable_tCF8D31AC9C54CDE3184EC34CC9902FC39950B108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8276[1] = 
{
	DropCollectable_tCF8D31AC9C54CDE3184EC34CC9902FC39950B108::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8277 = { sizeof (DestroyableComponent_t0AC1C3F5D08F64BCAB2FE13A8187CD3B4C72C2AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8278 = { sizeof (ItemGeneratorComponent_t3D2A4D8706A3C2B269E69E7420E56D34751C44A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8278[1] = 
{
	ItemGeneratorComponent_t3D2A4D8706A3C2B269E69E7420E56D34751C44A9::get_offset_of_DisposeTime_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8279 = { sizeof (ItemsSpawnPoints_tA7338A523B7E4DEEE324D582A64392620946C24B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8279[1] = 
{
	ItemsSpawnPoints_tA7338A523B7E4DEEE324D582A64392620946C24B::get_offset_of_SpawnPoint_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8280 = { sizeof (CollectableDisposeSystem_t65DA70D4DE910A23164EE6A4F0C934C368AD923F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8280[3] = 
{
	CollectableDisposeSystem_t65DA70D4DE910A23164EE6A4F0C934C368AD923F::get_offset_of__sfxLibrary_3(),
	CollectableDisposeSystem_t65DA70D4DE910A23164EE6A4F0C934C368AD923F::get_offset_of__itemsSO_4(),
	CollectableDisposeSystem_t65DA70D4DE910A23164EE6A4F0C934C368AD923F::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8281 = { sizeof (CollectableHPSystem_t26E4C78BD41D3AE2794769F1E52DC279628C4899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8281[1] = 
{
	CollectableHPSystem_t26E4C78BD41D3AE2794769F1E52DC279628C4899::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8282 = { sizeof (DestroyableDisposeSystem_tB9FF98ED1D2F72BF5187280E0D238E78BD59CC7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8282[3] = 
{
	DestroyableDisposeSystem_tB9FF98ED1D2F72BF5187280E0D238E78BD59CC7D::get_offset_of__sfxLibrary_3(),
	DestroyableDisposeSystem_tB9FF98ED1D2F72BF5187280E0D238E78BD59CC7D::get_offset_of__itemsSO_4(),
	DestroyableDisposeSystem_tB9FF98ED1D2F72BF5187280E0D238E78BD59CC7D::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8283 = { sizeof (DestroyableHPSystem_t4F253C30559F4F9F7B2E0471215BFFAE30180F95), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8283[1] = 
{
	DestroyableHPSystem_t4F253C30559F4F9F7B2E0471215BFFAE30180F95::get_offset_of__context_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8284 = { sizeof (ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8284[6] = 
{
	ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA::get_offset_of__diContainer_3(),
	ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA::get_offset_of__chaptersSO_4(),
	ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA::get_offset_of__userVO_5(),
	ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA::get_offset_of__itemsSO_6(),
	ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA::get_offset_of__context_7(),
	ItemInitializerSystem_t682B2FF1373E1E7D009D7BD0C14003E0EA9C35BA::get_offset_of__initialized_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8285 = { sizeof (ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8285[6] = 
{
	ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4::get_offset_of__diContainer_0(),
	ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4::get_offset_of__chaptersSO_1(),
	ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4::get_offset_of__userVO_2(),
	ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4::get_offset_of__itemsSO_3(),
	ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4::get_offset_of__context_4(),
	ItemsGeneratorEcsSystem_tFB46C0F734490821A2D2E757DF1CD49D98B0FCA4::get_offset_of__entities_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8286 = { sizeof (ItemsSpawnUtils_t2C8768B3A6C4F84485AA9A2B280FEA790AB926AE), -1, sizeof(ItemsSpawnUtils_t2C8768B3A6C4F84485AA9A2B280FEA790AB926AE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8286[1] = 
{
	ItemsSpawnUtils_t2C8768B3A6C4F84485AA9A2B280FEA790AB926AE_StaticFields::get_offset_of_rnd_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8287 = { sizeof (U3CU3Ec_t469F2B1F6038A2F75A223568B858C3122D0D3793), -1, sizeof(U3CU3Ec_t469F2B1F6038A2F75A223568B858C3122D0D3793_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8287[2] = 
{
	U3CU3Ec_t469F2B1F6038A2F75A223568B858C3122D0D3793_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t469F2B1F6038A2F75A223568B858C3122D0D3793_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8288 = { sizeof (PositionComponent_tF4289DBFC54686B2E169B344877F548B633F923F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8288[1] = 
{
	PositionComponent_tF4289DBFC54686B2E169B344877F548B633F923F::get_offset_of_Position_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8289 = { sizeof (InitPositionComponent_tD76FE6A7C6932550717B63157DF3D9C9B524592D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8289[1] = 
{
	InitPositionComponent_tD76FE6A7C6932550717B63157DF3D9C9B524592D::get_offset_of_Position_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8290 = { sizeof (RotationComponent_t227795A451188F6642FA72A0AB4428BDAF8402E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8290[1] = 
{
	RotationComponent_t227795A451188F6642FA72A0AB4428BDAF8402E6::get_offset_of_Rotation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8291 = { sizeof (ParentComponent_t6693F80E87AF972F7FFB2046BFE1D4C574EE00B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8291[1] = 
{
	ParentComponent_t6693F80E87AF972F7FFB2046BFE1D4C574EE00B1::get_offset_of_ParentId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8292 = { sizeof (ParentSystem_t99E0EB4A1E783D2C5B85440B6314253875C031CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8292[3] = 
{
	ParentSystem_t99E0EB4A1E783D2C5B85440B6314253875C031CD::get_offset_of__spellsSO_3(),
	ParentSystem_t99E0EB4A1E783D2C5B85440B6314253875C031CD::get_offset_of__unitsSO_4(),
	ParentSystem_t99E0EB4A1E783D2C5B85440B6314253875C031CD::get_offset_of__context_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8293 = { sizeof (ProjectileComponent_tF75C5C97DB61835512EF17B45EF1025CF7B8279A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8293[3] = 
{
	ProjectileComponent_tF75C5C97DB61835512EF17B45EF1025CF7B8279A::get_offset_of_StartPosition_0(),
	ProjectileComponent_tF75C5C97DB61835512EF17B45EF1025CF7B8279A::get_offset_of_EndPosition_1(),
	ProjectileComponent_tF75C5C97DB61835512EF17B45EF1025CF7B8279A::get_offset_of_Speed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8294 = { sizeof (ProjectileStartPositionComponent_t05ACF56B1866B2968D14D8171ABA3774E697B9D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8294[1] = 
{
	ProjectileStartPositionComponent_t05ACF56B1866B2968D14D8171ABA3774E697B9D5::get_offset_of_Position_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8295 = { sizeof (ProjectileTargetEntityComponent_tB6079DABE86C7E40E686F8A0B71DAAFB64E54B7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8295[1] = 
{
	ProjectileTargetEntityComponent_tB6079DABE86C7E40E686F8A0B71DAAFB64E54B7D::get_offset_of_Id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8296 = { sizeof (ProjectileSpellComponent_t0705BDDD9C71C99B26399E40FC8B9C588A829266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8296[1] = 
{
	ProjectileSpellComponent_t0705BDDD9C71C99B26399E40FC8B9C588A829266::get_offset_of_SpellId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8297 = { sizeof (ProjectileSourceComponent_t3B42D5D24633292F27113C80CFC5023973860457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8297[1] = 
{
	ProjectileSourceComponent_t3B42D5D24633292F27113C80CFC5023973860457::get_offset_of_SourceId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8298 = { sizeof (VisualComponent_t983B7D5B711F9CBF41022CD3EFD0212607D948EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8298[2] = 
{
	VisualComponent_t983B7D5B711F9CBF41022CD3EFD0212607D948EE::get_offset_of_Prefab_0(),
	VisualComponent_t983B7D5B711F9CBF41022CD3EFD0212607D948EE::get_offset_of_Bundle_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8299 = { sizeof (ProjectileCollisionSystem_tE66FF20613979A88CEBE2DB9036BFA5426B8B906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8299[4] = 
{
	ProjectileCollisionSystem_tE66FF20613979A88CEBE2DB9036BFA5426B8B906::get_offset_of__spellSO_0(),
	ProjectileCollisionSystem_tE66FF20613979A88CEBE2DB9036BFA5426B8B906::get_offset_of__entities_1(),
	ProjectileCollisionSystem_tE66FF20613979A88CEBE2DB9036BFA5426B8B906::get_offset_of__context_2(),
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
