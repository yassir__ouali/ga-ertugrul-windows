﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Facebook.Unity.CallbackManager
struct CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8;
// Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler
struct ICanvasFacebookCallbackHandler_t7A8ED3BD74D3719EBB8A7147D44A6423F14E86ED;
// Facebook.Unity.Canvas.ICanvasJSWrapper
struct ICanvasJSWrapper_t4F89ADBBE032CEB94CCDD92BBB2BAF572C235A7B;
// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_tFFB6D624086048B6FFFB85D9BC1946920684385B;
// Facebook.Unity.IFacebookImplementation
struct IFacebookImplementation_tCFB270E1A589730204AC2530B5A8524F5ABD905A;
// Facebook.Unity.InitDelegate
struct InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47;
// Facebook.Unity.Mobile.Android.IAndroidWrapper
struct IAndroidWrapper_t985E1CC9E0F96B6973FDFB4CAEFDC6189D5F2163;
// Facebook.Unity.Mobile.IOS.IIOSWrapper
struct IIOSWrapper_tC3E7DDD29AC9B2707F04E9C4AD7102FE2E54E790;
// Facebook.Unity.ResultContainer
struct ResultContainer_t19BFBF119CCF533782EEA08E523DCFB899453602;
// Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>
struct Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887;
// GameSparks.Core.GSData
struct GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937;
// GameSparks.RT.IRTSession
struct IRTSession_t72226F530281A73FC4B198F0ED8984FF6AF19244;
// GameSparksSettings
struct GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4;
// GameSparksTestUI
struct GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027;
// Joystick
struct Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B;
// RotaryHeart.Lib.DataBaseExample/AC_S
struct AC_S_t83967940B4F21697907028815B64B5E0E7893239;
// RotaryHeart.Lib.DataBaseExample/AdvanGeneric_String
struct AdvanGeneric_String_tC1B14FFB5530AA2D7D67CF242DBBDA106722CDDB;
// RotaryHeart.Lib.DataBaseExample/C_Int
struct C_Int_t3809E5AEA51F61B739425178D50CB479D2481E0B;
// RotaryHeart.Lib.DataBaseExample/ChildTest[]
struct ChildTestU5BU5D_tAADA55A3E4B19CDA5403BD0B45E2388E846F1154;
// RotaryHeart.Lib.DataBaseExample/Enum_String
struct Enum_String_t567A643096104B5698404A0754A1F42C7622AFBE;
// RotaryHeart.Lib.DataBaseExample/GO_I
struct GO_I_t9747DD551B9783018C8A9BCFBB45B0E0E739B958;
// RotaryHeart.Lib.DataBaseExample/GO_S
struct GO_S_t82033CC353B905E23DF7E43F5B8E1FC929ADBC54;
// RotaryHeart.Lib.DataBaseExample/G_Int
struct G_Int_t83ADF384081ED34E8132235BB2520189A5626291;
// RotaryHeart.Lib.DataBaseExample/Generic_Generic
struct Generic_Generic_tC1C606DD304D8B4975F7100A94883D309C89A3C4;
// RotaryHeart.Lib.DataBaseExample/Generic_String
struct Generic_String_tCF72708E4A84958EC2F36570DE29BAD94F59EE49;
// RotaryHeart.Lib.DataBaseExample/I_GO
struct I_GO_tE66786805611B9866539EC70755473A6AD7D961C;
// RotaryHeart.Lib.DataBaseExample/I_GenericDictionary
struct I_GenericDictionary_t16BC9886BA6F437767D8D7C0E3A17A82788BF0D6;
// RotaryHeart.Lib.DataBaseExample/Int_IntArray
struct Int_IntArray_t09AD7421347872BAAED8837E37A5018B662D23FD;
// RotaryHeart.Lib.DataBaseExample/Mat_S
struct Mat_S_tB6FD4BBFE1F09B0858D9CBFE77379B544084092A;
// RotaryHeart.Lib.DataBaseExample/Q_V3
struct Q_V3_tF2917CB976081AF5F996E700E7E8206C99F739DE;
// RotaryHeart.Lib.DataBaseExample/S_AC
struct S_AC_tC55B8F185DC515189810B1B8D114C456697FD3B5;
// RotaryHeart.Lib.DataBaseExample/S_GO
struct S_GO_tC66061788F2FA9D3CDAE398F0CAC5E81BD4C6369;
// RotaryHeart.Lib.DataBaseExample/S_GenericDictionary
struct S_GenericDictionary_t13EDCAD9036EB9CEA44B16E4AF15DF661432493C;
// RotaryHeart.Lib.DataBaseExample/S_Mat
struct S_Mat_tD5CCE3FD00251BA71449066AEE6E59387977C668;
// RotaryHeart.Lib.DataBaseExample/V3_Q
struct V3_Q_t334647532EC7E76E279772A7E1454D957F7E6FFD;
// RotaryHeart.Lib.MainDict
struct MainDict_tABB8831EB5CF88193E3BF7F318A84053BFE42682;
// RotaryHeart.Lib.Nested2Dict
struct Nested2Dict_t92B279CB96D0DFAEBA143A7267846B5A04933628;
// RotaryHeart.Lib.NestedDict
struct NestedDict_t786E4BDEE0E5CB8B389F8248722E4854CFABCD18;
// RotaryHeart.Lib.SerializableDictionary.ReorderableList
struct ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0;
// RotaryHeart.Lib.SerializableDictionary.RequiredReferences
struct RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0;
// SimpleJson2.IJsonSerializerStrategy
struct IJsonSerializerStrategy_t195743869EDDD046EFCC4CAD8E4E61F652EE8E4A;
// SimpleJson2.PocoJsonSerializerStrategy
struct PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846;
// SimpleJson2.Reflection.ReflectionUtils/GetDelegate
struct GetDelegate_tCBB4FA7AF97922763652DB4A13A5CC671C239658;
// System.Action`1<GameSparks.Api.Responses.AuthenticationResponse>
struct Action_1_t73AF17B1A2EBCDF742913C0515C2B32171B53EA5;
// System.Action`1<GameSparks.Api.Responses.LeaderboardDataResponse>
struct Action_1_tCFEC2B8E12502D76C48FE6320ADFAFAF7F2E74B5;
// System.Action`1<GameSparks.RT.RTPacket>
struct Action_1_t5CF6CC028A59A60A315AF5E2DB24AC3DEC655C8D;
// System.Action`1<System.Boolean>
struct Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD;
// System.Action`1<System.Exception>
struct Action_1_t114FA5D778AE43CC5A12F16A8CCD137F67ED6962;
// System.Action`1<System.Int32>
struct Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B;
// System.Action`2<System.Object,System.Object>
struct Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<RotaryHeart.Lib.DataBaseExample/AdvancedGenericClass,System.String>
struct Dictionary_2_t81FE26C6650BAA689B729D08B4290D5CB879A1A8;
// System.Collections.Generic.Dictionary`2<RotaryHeart.Lib.DataBaseExample/ClassTest,RotaryHeart.Lib.DataBaseExample/ClassTest>
struct Dictionary_2_t161799848E67B8AC48DCF5502DDC8CA3F61549E4;
// System.Collections.Generic.Dictionary`2<RotaryHeart.Lib.DataBaseExample/ClassTest,System.String>
struct Dictionary_2_t8FF731C4650354DDA77DC668BA0897CCDDF12464;
// System.Collections.Generic.Dictionary`2<RotaryHeart.Lib.DataBaseExample/EnumExample,System.String>
struct Dictionary_2_tAD91837F8573D567EAED8298F204D4602B34B4B9;
// System.Collections.Generic.Dictionary`2<System.Char,System.Int32>
struct Dictionary_2_tD824C6AA50393E55534B6616C544907A7EE94E58;
// System.Collections.Generic.Dictionary`2<System.Int32,RotaryHeart.Lib.DataBaseExample/ArrayTest>
struct Dictionary_2_t49F16597D652BA226A411B6AB80DECD1EEDD9BF4;
// System.Collections.Generic.Dictionary`2<System.Int32,RotaryHeart.Lib.DataBaseExample/ClassTest>
struct Dictionary_2_tBD161D2E9667459A55A4444FBBD6BF1CA902D10E;
// System.Collections.Generic.Dictionary`2<System.Int32,RotaryHeart.Lib.NestedExample>
struct Dictionary_2_t0420B24667EE5A4C7B9E9282CD612D88A0F1F481;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject>
struct Dictionary_2_t1DA4B446B1C6B709FEA5D9DAB5F606BE0CC4FD2D;
// System.Collections.Generic.Dictionary`2<System.String,RotaryHeart.Lib.DataBaseExample/ClassTest>
struct Dictionary_2_t0DCCC9FD10C6B9438ED2F2275A8B58B2E18505A9;
// System.Collections.Generic.Dictionary`2<System.String,RotaryHeart.Lib.Example>
struct Dictionary_2_t38129F23BD62C870C4474E1800ED8E832FC24C82;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AudioClip>
struct Dictionary_2_tBC4ABC1740BC695EC6AE67BA579E0743A89373D6;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>
struct Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Sprite>
struct Dictionary_2_t08107DE97D3661D374D54356092C44C550883EA9;
// System.Collections.Generic.Dictionary`2<UnityEngine.AudioClip,System.String>
struct Dictionary_2_tBB446FB0CDF4BB22336D7FF077278F8E508E82B6;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.Int32>
struct Dictionary_2_t96FB2F26C7CE603F75E00CA02CCD843EA785C29D;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.String>
struct Dictionary_2_t50497CD2E34AB82809A66864767B8A6BB9A354DA;
// System.Collections.Generic.Dictionary`2<UnityEngine.Gradient,System.Int32>
struct Dictionary_2_tA0816A5D7E1B085B03EBDB113C4BE44A30D8FBE5;
// System.Collections.Generic.Dictionary`2<UnityEngine.Material,System.String>
struct Dictionary_2_t09233B7D878DE2EAEA5F1457E153270B46308364;
// System.Collections.Generic.Dictionary`2<UnityEngine.Quaternion,UnityEngine.Vector3>
struct Dictionary_2_tC3E5167C8C46E8FB66C88021C5A1EC72F6E5AAF9;
// System.Collections.Generic.Dictionary`2<UnityEngine.QueryTriggerInteraction,System.String>
struct Dictionary_2_t6DA0A4193816CE464E6B64C19A04A5F4E75FCC0A;
// System.Collections.Generic.Dictionary`2<UnityEngine.Vector3,UnityEngine.Quaternion>
struct Dictionary_2_t8ACD821C092AB3D97DE34E70615B13E72AAA2805;
// System.Collections.Generic.IDictionary`2<System.Type,SimpleJson2.Reflection.ReflectionUtils/ConstructorDelegate>
struct IDictionary_2_t04990058610F1F8791F08FC8858AA635810293C6;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson2.Reflection.ReflectionUtils/GetDelegate>>
struct IDictionary_2_t0465D1ED253C60BD3A04E90C7767C5ABA761106B;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson2.Reflection.ReflectionUtils/SetDelegate>>>
struct IDictionary_2_tCADF95104206306B6C58807803080EB5C59EE160;
// System.Collections.Generic.List`1<RotaryHeart.Lib.DataBaseExample/AdvancedGenericClass>
struct List_1_t77282BCE8E9EA3D14015DFE9137D583DD325FE1B;
// System.Collections.Generic.List`1<RotaryHeart.Lib.DataBaseExample/ArrayTest>
struct List_1_t6EB6DDE4C420711C1C57DEC763D0631C37A99FDE;
// System.Collections.Generic.List`1<RotaryHeart.Lib.DataBaseExample/ClassTest>
struct List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC;
// System.Collections.Generic.List`1<RotaryHeart.Lib.DataBaseExample/EnumExample>
struct List_1_t0F98B738728348B8BD34C9F8899D2DCE3F4029E0;
// System.Collections.Generic.List`1<RotaryHeart.Lib.Example>
struct List_1_tC80F0C11C3B192C702CB0A1A7A8AF82A017F93C6;
// System.Collections.Generic.List`1<RotaryHeart.Lib.NestedExample>
struct List_1_tC666E449BA887A32F53E5586940AEE7F01378E06;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t028AAE01C4834286B7892F4498364F964CD8B316;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0;
// System.Collections.Generic.List`1<UnityEngine.Gradient>
struct List_1_tE3A3FBC3263837B20EB0B0E537706BA254A2675B;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t6A61046573B0BC4E12950B90305C189DD041D786;
// System.Collections.Generic.List`1<UnityEngine.Quaternion>
struct List_1_t74314CADF5799483B2C71785AC688DD9618023F4;
// System.Collections.Generic.List`1<UnityEngine.QueryTriggerInteraction>
struct List_1_tB78E0FB5C13F06BAADE39A8FD64F6672E3D7FAEF;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t18801B80DBA94AAA8DF21D6ED1C15CF944BCA6CC;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.Queue`1<System.String>
struct Queue_1_t234B58D376F3C134441C47D5A9EF7789374EE172;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<System.Object,System.Object>
struct Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4;
// System.Func`2<System.Object[],System.Object>
struct Func_2_t19CE90F40AA42D4BC00A006DCF4F2AD0783FA97F;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Gradient
struct Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_TF109B2B0745E38DCB0076B99BD7FB2404E195578_H
#define U3CMODULEU3E_TF109B2B0745E38DCB0076B99BD7FB2404E195578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tF109B2B0745E38DCB0076B99BD7FB2404E195578 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TF109B2B0745E38DCB0076B99BD7FB2404E195578_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC_TAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5_H
#define U3CU3EC_TAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Canvas.CanvasFacebook_<>c
struct  U3CU3Ec_tAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5_StaticFields
{
public:
	// Facebook.Unity.Canvas.CanvasFacebook_<>c Facebook.Unity.Canvas.CanvasFacebook_<>c::<>9
	U3CU3Ec_tAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5 * ___U3CU3E9_0;
	// Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer> Facebook.Unity.Canvas.CanvasFacebook_<>c::<>9__40_0
	Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887 * ___U3CU3E9__40_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5_StaticFields, ___U3CU3E9__40_0_1)); }
	inline Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887 * get_U3CU3E9__40_0_1() const { return ___U3CU3E9__40_0_1; }
	inline Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887 ** get_address_of_U3CU3E9__40_0_1() { return &___U3CU3E9__40_0_1; }
	inline void set_U3CU3E9__40_0_1(Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887 * value)
	{
		___U3CU3E9__40_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5_H
#ifndef U3CU3EC__DISPLAYCLASS47_0_T419FE32629C2F150E6F9F7221BF40D6C0C519ECB_H
#define U3CU3EC__DISPLAYCLASS47_0_T419FE32629C2F150E6F9F7221BF40D6C0C519ECB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Canvas.CanvasFacebook_<>c__DisplayClass47_0
struct  U3CU3Ec__DisplayClass47_0_t419FE32629C2F150E6F9F7221BF40D6C0C519ECB  : public RuntimeObject
{
public:
	// Facebook.Unity.ResultContainer Facebook.Unity.Canvas.CanvasFacebook_<>c__DisplayClass47_0::result
	ResultContainer_t19BFBF119CCF533782EEA08E523DCFB899453602 * ___result_0;
	// Facebook.Unity.Utilities_Callback`1<Facebook.Unity.ResultContainer> Facebook.Unity.Canvas.CanvasFacebook_<>c__DisplayClass47_0::callback
	Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887 * ___callback_1;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass47_0_t419FE32629C2F150E6F9F7221BF40D6C0C519ECB, ___result_0)); }
	inline ResultContainer_t19BFBF119CCF533782EEA08E523DCFB899453602 * get_result_0() const { return ___result_0; }
	inline ResultContainer_t19BFBF119CCF533782EEA08E523DCFB899453602 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(ResultContainer_t19BFBF119CCF533782EEA08E523DCFB899453602 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier((&___result_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass47_0_t419FE32629C2F150E6F9F7221BF40D6C0C519ECB, ___callback_1)); }
	inline Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887 * get_callback_1() const { return ___callback_1; }
	inline Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Callback_1_t9200E2A35184E330F144E9195F3882E126DA2887 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS47_0_T419FE32629C2F150E6F9F7221BF40D6C0C519ECB_H
#ifndef FACEBOOKBASE_T89F93EF12353405D2CEFAC7B59E860123FB472C7_H
#define FACEBOOKBASE_T89F93EF12353405D2CEFAC7B59E860123FB472C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookBase
struct  FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7  : public RuntimeObject
{
public:
	// Facebook.Unity.InitDelegate Facebook.Unity.FacebookBase::onInitCompleteDelegate
	InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47 * ___onInitCompleteDelegate_0;
	// System.Boolean Facebook.Unity.FacebookBase::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_1;
	// Facebook.Unity.CallbackManager Facebook.Unity.FacebookBase::<CallbackManager>k__BackingField
	CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8 * ___U3CCallbackManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_onInitCompleteDelegate_0() { return static_cast<int32_t>(offsetof(FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7, ___onInitCompleteDelegate_0)); }
	inline InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47 * get_onInitCompleteDelegate_0() const { return ___onInitCompleteDelegate_0; }
	inline InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47 ** get_address_of_onInitCompleteDelegate_0() { return &___onInitCompleteDelegate_0; }
	inline void set_onInitCompleteDelegate_0(InitDelegate_tD9FF06C3949FD06E97E275F72D8F43AF4F496F47 * value)
	{
		___onInitCompleteDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___onInitCompleteDelegate_0), value);
	}

	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7, ___U3CInitializedU3Ek__BackingField_1)); }
	inline bool get_U3CInitializedU3Ek__BackingField_1() const { return ___U3CInitializedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_1() { return &___U3CInitializedU3Ek__BackingField_1; }
	inline void set_U3CInitializedU3Ek__BackingField_1(bool value)
	{
		___U3CInitializedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCallbackManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7, ___U3CCallbackManagerU3Ek__BackingField_2)); }
	inline CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8 * get_U3CCallbackManagerU3Ek__BackingField_2() const { return ___U3CCallbackManagerU3Ek__BackingField_2; }
	inline CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8 ** get_address_of_U3CCallbackManagerU3Ek__BackingField_2() { return &___U3CCallbackManagerU3Ek__BackingField_2; }
	inline void set_U3CCallbackManagerU3Ek__BackingField_2(CallbackManager_t3ED952CB7A3597AAC84A6A973ADAE06A3E832BF8 * value)
	{
		___U3CCallbackManagerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackManagerU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKBASE_T89F93EF12353405D2CEFAC7B59E860123FB472C7_H
#ifndef NATIVEDICT_T902E3B1188FE573A691FAFD826F5C9F866A9CFFA_H
#define NATIVEDICT_T902E3B1188FE573A691FAFD826F5C9F866A9CFFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict
struct  NativeDict_t902E3B1188FE573A691FAFD826F5C9F866A9CFFA  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::<NumEntries>k__BackingField
	int32_t ___U3CNumEntriesU3Ek__BackingField_0;
	// System.String[] Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::<Keys>k__BackingField
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___U3CKeysU3Ek__BackingField_1;
	// System.String[] Facebook.Unity.Mobile.IOS.IOSFacebook_NativeDict::<Values>k__BackingField
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___U3CValuesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CNumEntriesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NativeDict_t902E3B1188FE573A691FAFD826F5C9F866A9CFFA, ___U3CNumEntriesU3Ek__BackingField_0)); }
	inline int32_t get_U3CNumEntriesU3Ek__BackingField_0() const { return ___U3CNumEntriesU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CNumEntriesU3Ek__BackingField_0() { return &___U3CNumEntriesU3Ek__BackingField_0; }
	inline void set_U3CNumEntriesU3Ek__BackingField_0(int32_t value)
	{
		___U3CNumEntriesU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CKeysU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NativeDict_t902E3B1188FE573A691FAFD826F5C9F866A9CFFA, ___U3CKeysU3Ek__BackingField_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_U3CKeysU3Ek__BackingField_1() const { return ___U3CKeysU3Ek__BackingField_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_U3CKeysU3Ek__BackingField_1() { return &___U3CKeysU3Ek__BackingField_1; }
	inline void set_U3CKeysU3Ek__BackingField_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___U3CKeysU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeysU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CValuesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NativeDict_t902E3B1188FE573A691FAFD826F5C9F866A9CFFA, ___U3CValuesU3Ek__BackingField_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_U3CValuesU3Ek__BackingField_2() const { return ___U3CValuesU3Ek__BackingField_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_U3CValuesU3Ek__BackingField_2() { return &___U3CValuesU3Ek__BackingField_2; }
	inline void set_U3CValuesU3Ek__BackingField_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___U3CValuesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValuesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEDICT_T902E3B1188FE573A691FAFD826F5C9F866A9CFFA_H
#ifndef U3CU3EC_TA9A69283F64527B1C7F429072435A855B20DC6AC_H
#define U3CU3EC_TA9A69283F64527B1C7F429072435A855B20DC6AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparksTestUI_<>c
struct  U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC_StaticFields
{
public:
	// GameSparksTestUI_<>c GameSparksTestUI_<>c::<>9
	U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC * ___U3CU3E9_0;
	// System.Action`1<GameSparks.Api.Responses.AuthenticationResponse> GameSparksTestUI_<>c::<>9__17_7
	Action_1_t73AF17B1A2EBCDF742913C0515C2B32171B53EA5 * ___U3CU3E9__17_7_1;
	// System.Action`1<GameSparks.Api.Responses.LeaderboardDataResponse> GameSparksTestUI_<>c::<>9__17_8
	Action_1_tCFEC2B8E12502D76C48FE6320ADFAFAF7F2E74B5 * ___U3CU3E9__17_8_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_7_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC_StaticFields, ___U3CU3E9__17_7_1)); }
	inline Action_1_t73AF17B1A2EBCDF742913C0515C2B32171B53EA5 * get_U3CU3E9__17_7_1() const { return ___U3CU3E9__17_7_1; }
	inline Action_1_t73AF17B1A2EBCDF742913C0515C2B32171B53EA5 ** get_address_of_U3CU3E9__17_7_1() { return &___U3CU3E9__17_7_1; }
	inline void set_U3CU3E9__17_7_1(Action_1_t73AF17B1A2EBCDF742913C0515C2B32171B53EA5 * value)
	{
		___U3CU3E9__17_7_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_7_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__17_8_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC_StaticFields, ___U3CU3E9__17_8_2)); }
	inline Action_1_tCFEC2B8E12502D76C48FE6320ADFAFAF7F2E74B5 * get_U3CU3E9__17_8_2() const { return ___U3CU3E9__17_8_2; }
	inline Action_1_tCFEC2B8E12502D76C48FE6320ADFAFAF7F2E74B5 ** get_address_of_U3CU3E9__17_8_2() { return &___U3CU3E9__17_8_2; }
	inline void set_U3CU3E9__17_8_2(Action_1_tCFEC2B8E12502D76C48FE6320ADFAFAF7F2E74B5 * value)
	{
		___U3CU3E9__17_8_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__17_8_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TA9A69283F64527B1C7F429072435A855B20DC6AC_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_TC67E67197EC95281396577A19BFBF46CA70F1AE6_H
#define U3CU3EC__DISPLAYCLASS15_0_TC67E67197EC95281396577A19BFBF46CA70F1AE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparksTestUI_<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_tC67E67197EC95281396577A19BFBF46CA70F1AE6  : public RuntimeObject
{
public:
	// GameSparksTestUI GameSparksTestUI_<>c__DisplayClass15_0::<>4__this
	GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027 * ___U3CU3E4__this_0;
	// System.String GameSparksTestUI_<>c__DisplayClass15_0::logString
	String_t* ___logString_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_tC67E67197EC95281396577A19BFBF46CA70F1AE6, ___U3CU3E4__this_0)); }
	inline GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_logString_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_tC67E67197EC95281396577A19BFBF46CA70F1AE6, ___logString_1)); }
	inline String_t* get_logString_1() const { return ___logString_1; }
	inline String_t** get_address_of_logString_1() { return &___logString_1; }
	inline void set_logString_1(String_t* value)
	{
		___logString_1 = value;
		Il2CppCodeGenWriteBarrier((&___logString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_TC67E67197EC95281396577A19BFBF46CA70F1AE6_H
#ifndef RTDATAEXTENSIONS_TEEE8BAA609AB023F463DDF2C7541AA2B59911EDA_H
#define RTDATAEXTENSIONS_TEEE8BAA609AB023F463DDF2C7541AA2B59911EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RTDataExtensions
struct  RTDataExtensions_tEEE8BAA609AB023F463DDF2C7541AA2B59911EDA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RTDATAEXTENSIONS_TEEE8BAA609AB023F463DDF2C7541AA2B59911EDA_H
#ifndef ADVANCEDGENERICCLASS_TC09D58B7902B02C8C31ACD1DD128EDCB13721D44_H
#define ADVANCEDGENERICCLASS_TC09D58B7902B02C8C31ACD1DD128EDCB13721D44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_AdvancedGenericClass
struct  AdvancedGenericClass_tC09D58B7902B02C8C31ACD1DD128EDCB13721D44  : public RuntimeObject
{
public:
	// System.Single RotaryHeart.Lib.DataBaseExample_AdvancedGenericClass::value
	float ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(AdvancedGenericClass_tC09D58B7902B02C8C31ACD1DD128EDCB13721D44, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANCEDGENERICCLASS_TC09D58B7902B02C8C31ACD1DD128EDCB13721D44_H
#ifndef ARRAYTEST_TC68B49DBCCAFCE5D3DB0BD59DAC0ED1B8D95F6D0_H
#define ARRAYTEST_TC68B49DBCCAFCE5D3DB0BD59DAC0ED1B8D95F6D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_ArrayTest
struct  ArrayTest_tC68B49DBCCAFCE5D3DB0BD59DAC0ED1B8D95F6D0  : public RuntimeObject
{
public:
	// System.Int32[] RotaryHeart.Lib.DataBaseExample_ArrayTest::myArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___myArray_0;

public:
	inline static int32_t get_offset_of_myArray_0() { return static_cast<int32_t>(offsetof(ArrayTest_tC68B49DBCCAFCE5D3DB0BD59DAC0ED1B8D95F6D0, ___myArray_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_myArray_0() const { return ___myArray_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_myArray_0() { return &___myArray_0; }
	inline void set_myArray_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___myArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___myArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYTEST_TC68B49DBCCAFCE5D3DB0BD59DAC0ED1B8D95F6D0_H
#ifndef DRAWABLEDICTIONARY_T5EE13D6B57AC0486747142D13BD761EE7C87F78F_H
#define DRAWABLEDICTIONARY_T5EE13D6B57AC0486747142D13BD761EE7C87F78F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.DrawableDictionary
struct  DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F  : public RuntimeObject
{
public:
	// RotaryHeart.Lib.SerializableDictionary.ReorderableList RotaryHeart.Lib.SerializableDictionary.DrawableDictionary::reorderableList
	ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0 * ___reorderableList_0;
	// RotaryHeart.Lib.SerializableDictionary.RequiredReferences RotaryHeart.Lib.SerializableDictionary.DrawableDictionary::reqReferences
	RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0 * ___reqReferences_1;
	// System.Boolean RotaryHeart.Lib.SerializableDictionary.DrawableDictionary::isExpanded
	bool ___isExpanded_2;

public:
	inline static int32_t get_offset_of_reorderableList_0() { return static_cast<int32_t>(offsetof(DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F, ___reorderableList_0)); }
	inline ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0 * get_reorderableList_0() const { return ___reorderableList_0; }
	inline ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0 ** get_address_of_reorderableList_0() { return &___reorderableList_0; }
	inline void set_reorderableList_0(ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0 * value)
	{
		___reorderableList_0 = value;
		Il2CppCodeGenWriteBarrier((&___reorderableList_0), value);
	}

	inline static int32_t get_offset_of_reqReferences_1() { return static_cast<int32_t>(offsetof(DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F, ___reqReferences_1)); }
	inline RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0 * get_reqReferences_1() const { return ___reqReferences_1; }
	inline RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0 ** get_address_of_reqReferences_1() { return &___reqReferences_1; }
	inline void set_reqReferences_1(RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0 * value)
	{
		___reqReferences_1 = value;
		Il2CppCodeGenWriteBarrier((&___reqReferences_1), value);
	}

	inline static int32_t get_offset_of_isExpanded_2() { return static_cast<int32_t>(offsetof(DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F, ___isExpanded_2)); }
	inline bool get_isExpanded_2() const { return ___isExpanded_2; }
	inline bool* get_address_of_isExpanded_2() { return &___isExpanded_2; }
	inline void set_isExpanded_2(bool value)
	{
		___isExpanded_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWABLEDICTIONARY_T5EE13D6B57AC0486747142D13BD761EE7C87F78F_H
#ifndef REORDERABLELIST_TE9342433971E09B8B941B8BDB654FBCACD8839D0_H
#define REORDERABLELIST_TE9342433971E09B8B941B8BDB654FBCACD8839D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.ReorderableList
struct  ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REORDERABLELIST_TE9342433971E09B8B941B8BDB654FBCACD8839D0_H
#ifndef JSONOBJECT_T70E68CAB3D5CC3117C77D86FE0A9D37E2BAD1C66_H
#define JSONOBJECT_T70E68CAB3D5CC3117C77D86FE0A9D37E2BAD1C66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.JsonObject
struct  JsonObject_t70E68CAB3D5CC3117C77D86FE0A9D37E2BAD1C66  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> SimpleJson2.JsonObject::_members
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ____members_0;

public:
	inline static int32_t get_offset_of__members_0() { return static_cast<int32_t>(offsetof(JsonObject_t70E68CAB3D5CC3117C77D86FE0A9D37E2BAD1C66, ____members_0)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get__members_0() const { return ____members_0; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of__members_0() { return &____members_0; }
	inline void set__members_0(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		____members_0 = value;
		Il2CppCodeGenWriteBarrier((&____members_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECT_T70E68CAB3D5CC3117C77D86FE0A9D37E2BAD1C66_H
#ifndef POCOJSONSERIALIZERSTRATEGY_T3F266902BA6143911D4174997CAD590053320846_H
#define POCOJSONSERIALIZERSTRATEGY_T3F266902BA6143911D4174997CAD590053320846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.PocoJsonSerializerStrategy
struct  PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.Type,SimpleJson2.Reflection.ReflectionUtils_ConstructorDelegate> SimpleJson2.PocoJsonSerializerStrategy::ConstructorCache
	RuntimeObject* ___ConstructorCache_0;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson2.Reflection.ReflectionUtils_GetDelegate>> SimpleJson2.PocoJsonSerializerStrategy::GetCache
	RuntimeObject* ___GetCache_1;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson2.Reflection.ReflectionUtils_SetDelegate>>> SimpleJson2.PocoJsonSerializerStrategy::SetCache
	RuntimeObject* ___SetCache_2;

public:
	inline static int32_t get_offset_of_ConstructorCache_0() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846, ___ConstructorCache_0)); }
	inline RuntimeObject* get_ConstructorCache_0() const { return ___ConstructorCache_0; }
	inline RuntimeObject** get_address_of_ConstructorCache_0() { return &___ConstructorCache_0; }
	inline void set_ConstructorCache_0(RuntimeObject* value)
	{
		___ConstructorCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorCache_0), value);
	}

	inline static int32_t get_offset_of_GetCache_1() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846, ___GetCache_1)); }
	inline RuntimeObject* get_GetCache_1() const { return ___GetCache_1; }
	inline RuntimeObject** get_address_of_GetCache_1() { return &___GetCache_1; }
	inline void set_GetCache_1(RuntimeObject* value)
	{
		___GetCache_1 = value;
		Il2CppCodeGenWriteBarrier((&___GetCache_1), value);
	}

	inline static int32_t get_offset_of_SetCache_2() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846, ___SetCache_2)); }
	inline RuntimeObject* get_SetCache_2() const { return ___SetCache_2; }
	inline RuntimeObject** get_address_of_SetCache_2() { return &___SetCache_2; }
	inline void set_SetCache_2(RuntimeObject* value)
	{
		___SetCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___SetCache_2), value);
	}
};

struct PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846_StaticFields
{
public:
	// System.Type[] SimpleJson2.PocoJsonSerializerStrategy::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_3;
	// System.Type[] SimpleJson2.PocoJsonSerializerStrategy::ArrayConstructorParameterTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___ArrayConstructorParameterTypes_4;
	// System.String[] SimpleJson2.PocoJsonSerializerStrategy::Iso8601Format
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___Iso8601Format_5;

public:
	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_ArrayConstructorParameterTypes_4() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846_StaticFields, ___ArrayConstructorParameterTypes_4)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_ArrayConstructorParameterTypes_4() const { return ___ArrayConstructorParameterTypes_4; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_ArrayConstructorParameterTypes_4() { return &___ArrayConstructorParameterTypes_4; }
	inline void set_ArrayConstructorParameterTypes_4(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___ArrayConstructorParameterTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___ArrayConstructorParameterTypes_4), value);
	}

	inline static int32_t get_offset_of_Iso8601Format_5() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846_StaticFields, ___Iso8601Format_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_Iso8601Format_5() const { return ___Iso8601Format_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_Iso8601Format_5() { return &___Iso8601Format_5; }
	inline void set_Iso8601Format_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___Iso8601Format_5 = value;
		Il2CppCodeGenWriteBarrier((&___Iso8601Format_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POCOJSONSERIALIZERSTRATEGY_T3F266902BA6143911D4174997CAD590053320846_H
#ifndef REFLECTIONUTILS_T0190AC5C7C75DB42BCBC2038C5752BAFBF80240F_H
#define REFLECTIONUTILS_T0190AC5C7C75DB42BCBC2038C5752BAFBF80240F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils
struct  ReflectionUtils_t0190AC5C7C75DB42BCBC2038C5752BAFBF80240F  : public RuntimeObject
{
public:

public:
};

struct ReflectionUtils_t0190AC5C7C75DB42BCBC2038C5752BAFBF80240F_StaticFields
{
public:
	// System.Object[] SimpleJson2.Reflection.ReflectionUtils::EmptyObjects
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___EmptyObjects_0;

public:
	inline static int32_t get_offset_of_EmptyObjects_0() { return static_cast<int32_t>(offsetof(ReflectionUtils_t0190AC5C7C75DB42BCBC2038C5752BAFBF80240F_StaticFields, ___EmptyObjects_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_EmptyObjects_0() const { return ___EmptyObjects_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_EmptyObjects_0() { return &___EmptyObjects_0; }
	inline void set_EmptyObjects_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___EmptyObjects_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyObjects_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONUTILS_T0190AC5C7C75DB42BCBC2038C5752BAFBF80240F_H
#ifndef U3CU3EC__DISPLAYCLASS25_0_TFDE962807434FD897F7BB134F96B3819ED072776_H
#define U3CU3EC__DISPLAYCLASS25_0_TFDE962807434FD897F7BB134F96B3819ED072776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass25_0
struct  U3CU3Ec__DisplayClass25_0_tFDE962807434FD897F7BB134F96B3819ED072776  : public RuntimeObject
{
public:
	// System.Reflection.ConstructorInfo SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass25_0::constructorInfo
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ___constructorInfo_0;

public:
	inline static int32_t get_offset_of_constructorInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_tFDE962807434FD897F7BB134F96B3819ED072776, ___constructorInfo_0)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get_constructorInfo_0() const { return ___constructorInfo_0; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of_constructorInfo_0() { return &___constructorInfo_0; }
	inline void set_constructorInfo_0(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		___constructorInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___constructorInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS25_0_TFDE962807434FD897F7BB134F96B3819ED072776_H
#ifndef U3CU3EC__DISPLAYCLASS27_0_T96CB1C9ADB807F01DDEDCA4E7A248AB8FDD908E1_H
#define U3CU3EC__DISPLAYCLASS27_0_T96CB1C9ADB807F01DDEDCA4E7A248AB8FDD908E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass27_0
struct  U3CU3Ec__DisplayClass27_0_t96CB1C9ADB807F01DDEDCA4E7A248AB8FDD908E1  : public RuntimeObject
{
public:
	// System.Func`2<System.Object[],System.Object> SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass27_0::compiledLambda
	Func_2_t19CE90F40AA42D4BC00A006DCF4F2AD0783FA97F * ___compiledLambda_0;

public:
	inline static int32_t get_offset_of_compiledLambda_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_t96CB1C9ADB807F01DDEDCA4E7A248AB8FDD908E1, ___compiledLambda_0)); }
	inline Func_2_t19CE90F40AA42D4BC00A006DCF4F2AD0783FA97F * get_compiledLambda_0() const { return ___compiledLambda_0; }
	inline Func_2_t19CE90F40AA42D4BC00A006DCF4F2AD0783FA97F ** get_address_of_compiledLambda_0() { return &___compiledLambda_0; }
	inline void set_compiledLambda_0(Func_2_t19CE90F40AA42D4BC00A006DCF4F2AD0783FA97F * value)
	{
		___compiledLambda_0 = value;
		Il2CppCodeGenWriteBarrier((&___compiledLambda_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS27_0_T96CB1C9ADB807F01DDEDCA4E7A248AB8FDD908E1_H
#ifndef U3CU3EC__DISPLAYCLASS31_0_TF06A6BF3C9672CD9EC0F4D0AB173152F0BAC2054_H
#define U3CU3EC__DISPLAYCLASS31_0_TF06A6BF3C9672CD9EC0F4D0AB173152F0BAC2054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_tF06A6BF3C9672CD9EC0F4D0AB173152F0BAC2054  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass31_0::methodInfo
	MethodInfo_t * ___methodInfo_0;

public:
	inline static int32_t get_offset_of_methodInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_tF06A6BF3C9672CD9EC0F4D0AB173152F0BAC2054, ___methodInfo_0)); }
	inline MethodInfo_t * get_methodInfo_0() const { return ___methodInfo_0; }
	inline MethodInfo_t ** get_address_of_methodInfo_0() { return &___methodInfo_0; }
	inline void set_methodInfo_0(MethodInfo_t * value)
	{
		___methodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_0_TF06A6BF3C9672CD9EC0F4D0AB173152F0BAC2054_H
#ifndef U3CU3EC__DISPLAYCLASS32_0_T328438FE1174FC2DECBEA48E033ADDD6572EE2C8_H
#define U3CU3EC__DISPLAYCLASS32_0_T328438FE1174FC2DECBEA48E033ADDD6572EE2C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass32_0
struct  U3CU3Ec__DisplayClass32_0_t328438FE1174FC2DECBEA48E033ADDD6572EE2C8  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass32_0::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t328438FE1174FC2DECBEA48E033ADDD6572EE2C8, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS32_0_T328438FE1174FC2DECBEA48E033ADDD6572EE2C8_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_T72A740B08AAD1E56D2483BB84B5D34A0723A8A7D_H
#define U3CU3EC__DISPLAYCLASS33_0_T72A740B08AAD1E56D2483BB84B5D34A0723A8A7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t72A740B08AAD1E56D2483BB84B5D34A0723A8A7D  : public RuntimeObject
{
public:
	// System.Func`2<System.Object,System.Object> SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass33_0::compiled
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___compiled_0;

public:
	inline static int32_t get_offset_of_compiled_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t72A740B08AAD1E56D2483BB84B5D34A0723A8A7D, ___compiled_0)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_compiled_0() const { return ___compiled_0; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_compiled_0() { return &___compiled_0; }
	inline void set_compiled_0(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___compiled_0 = value;
		Il2CppCodeGenWriteBarrier((&___compiled_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_T72A740B08AAD1E56D2483BB84B5D34A0723A8A7D_H
#ifndef U3CU3EC__DISPLAYCLASS34_0_T6CC93C129B6EE0711AF4C7F55B2C02D434DB44C3_H
#define U3CU3EC__DISPLAYCLASS34_0_T6CC93C129B6EE0711AF4C7F55B2C02D434DB44C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass34_0
struct  U3CU3Ec__DisplayClass34_0_t6CC93C129B6EE0711AF4C7F55B2C02D434DB44C3  : public RuntimeObject
{
public:
	// SimpleJson2.Reflection.ReflectionUtils_GetDelegate SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass34_0::compiled
	GetDelegate_tCBB4FA7AF97922763652DB4A13A5CC671C239658 * ___compiled_0;

public:
	inline static int32_t get_offset_of_compiled_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_t6CC93C129B6EE0711AF4C7F55B2C02D434DB44C3, ___compiled_0)); }
	inline GetDelegate_tCBB4FA7AF97922763652DB4A13A5CC671C239658 * get_compiled_0() const { return ___compiled_0; }
	inline GetDelegate_tCBB4FA7AF97922763652DB4A13A5CC671C239658 ** get_address_of_compiled_0() { return &___compiled_0; }
	inline void set_compiled_0(GetDelegate_tCBB4FA7AF97922763652DB4A13A5CC671C239658 * value)
	{
		___compiled_0 = value;
		Il2CppCodeGenWriteBarrier((&___compiled_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS34_0_T6CC93C129B6EE0711AF4C7F55B2C02D434DB44C3_H
#ifndef U3CU3EC__DISPLAYCLASS37_0_T7C7C24CE855DC92C85BAB88B559F9EC2C091949C_H
#define U3CU3EC__DISPLAYCLASS37_0_T7C7C24CE855DC92C85BAB88B559F9EC2C091949C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass37_0
struct  U3CU3Ec__DisplayClass37_0_t7C7C24CE855DC92C85BAB88B559F9EC2C091949C  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass37_0::methodInfo
	MethodInfo_t * ___methodInfo_0;

public:
	inline static int32_t get_offset_of_methodInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t7C7C24CE855DC92C85BAB88B559F9EC2C091949C, ___methodInfo_0)); }
	inline MethodInfo_t * get_methodInfo_0() const { return ___methodInfo_0; }
	inline MethodInfo_t ** get_address_of_methodInfo_0() { return &___methodInfo_0; }
	inline void set_methodInfo_0(MethodInfo_t * value)
	{
		___methodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___methodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS37_0_T7C7C24CE855DC92C85BAB88B559F9EC2C091949C_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_TCA4117735671192C5D0A92ED0548E1B9183FD4D3_H
#define U3CU3EC__DISPLAYCLASS38_0_TCA4117735671192C5D0A92ED0548E1B9183FD4D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_tCA4117735671192C5D0A92ED0548E1B9183FD4D3  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass38_0::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_tCA4117735671192C5D0A92ED0548E1B9183FD4D3, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_TCA4117735671192C5D0A92ED0548E1B9183FD4D3_H
#ifndef U3CU3EC__DISPLAYCLASS39_0_TDAD1CC06874ABE9BCF34AD5FDCC08BC4FA801617_H
#define U3CU3EC__DISPLAYCLASS39_0_TDAD1CC06874ABE9BCF34AD5FDCC08BC4FA801617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_tDAD1CC06874ABE9BCF34AD5FDCC08BC4FA801617  : public RuntimeObject
{
public:
	// System.Action`2<System.Object,System.Object> SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass39_0::compiled
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___compiled_0;

public:
	inline static int32_t get_offset_of_compiled_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_tDAD1CC06874ABE9BCF34AD5FDCC08BC4FA801617, ___compiled_0)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_compiled_0() const { return ___compiled_0; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_compiled_0() { return &___compiled_0; }
	inline void set_compiled_0(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___compiled_0 = value;
		Il2CppCodeGenWriteBarrier((&___compiled_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS39_0_TDAD1CC06874ABE9BCF34AD5FDCC08BC4FA801617_H
#ifndef U3CU3EC__DISPLAYCLASS40_0_T0847F22B61CB73CB40902CB2A1F0056CD08ACA41_H
#define U3CU3EC__DISPLAYCLASS40_0_T0847F22B61CB73CB40902CB2A1F0056CD08ACA41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass40_0
struct  U3CU3Ec__DisplayClass40_0_t0847F22B61CB73CB40902CB2A1F0056CD08ACA41  : public RuntimeObject
{
public:
	// System.Action`2<System.Object,System.Object> SimpleJson2.Reflection.ReflectionUtils_<>c__DisplayClass40_0::compiled
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___compiled_0;

public:
	inline static int32_t get_offset_of_compiled_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t0847F22B61CB73CB40902CB2A1F0056CD08ACA41, ___compiled_0)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_compiled_0() const { return ___compiled_0; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_compiled_0() { return &___compiled_0; }
	inline void set_compiled_0(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___compiled_0 = value;
		Il2CppCodeGenWriteBarrier((&___compiled_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS40_0_T0847F22B61CB73CB40902CB2A1F0056CD08ACA41_H
#ifndef SIMPLEJSON2_T485F125050CE1D1EA00641E8D922AC6E41EF1454_H
#define SIMPLEJSON2_T485F125050CE1D1EA00641E8D922AC6E41EF1454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.SimpleJson2
struct  SimpleJson2_t485F125050CE1D1EA00641E8D922AC6E41EF1454  : public RuntimeObject
{
public:

public:
};

struct SimpleJson2_t485F125050CE1D1EA00641E8D922AC6E41EF1454_StaticFields
{
public:
	// System.Char[] SimpleJson2.SimpleJson2::EscapeTable
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___EscapeTable_13;
	// System.Char[] SimpleJson2.SimpleJson2::EscapeCharacters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___EscapeCharacters_14;
	// SimpleJson2.IJsonSerializerStrategy SimpleJson2.SimpleJson2::_currentJsonSerializerStrategy
	RuntimeObject* ____currentJsonSerializerStrategy_15;
	// SimpleJson2.PocoJsonSerializerStrategy SimpleJson2.SimpleJson2::_pocoJsonSerializerStrategy
	PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846 * ____pocoJsonSerializerStrategy_16;

public:
	inline static int32_t get_offset_of_EscapeTable_13() { return static_cast<int32_t>(offsetof(SimpleJson2_t485F125050CE1D1EA00641E8D922AC6E41EF1454_StaticFields, ___EscapeTable_13)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_EscapeTable_13() const { return ___EscapeTable_13; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_EscapeTable_13() { return &___EscapeTable_13; }
	inline void set_EscapeTable_13(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___EscapeTable_13 = value;
		Il2CppCodeGenWriteBarrier((&___EscapeTable_13), value);
	}

	inline static int32_t get_offset_of_EscapeCharacters_14() { return static_cast<int32_t>(offsetof(SimpleJson2_t485F125050CE1D1EA00641E8D922AC6E41EF1454_StaticFields, ___EscapeCharacters_14)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_EscapeCharacters_14() const { return ___EscapeCharacters_14; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_EscapeCharacters_14() { return &___EscapeCharacters_14; }
	inline void set_EscapeCharacters_14(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___EscapeCharacters_14 = value;
		Il2CppCodeGenWriteBarrier((&___EscapeCharacters_14), value);
	}

	inline static int32_t get_offset_of__currentJsonSerializerStrategy_15() { return static_cast<int32_t>(offsetof(SimpleJson2_t485F125050CE1D1EA00641E8D922AC6E41EF1454_StaticFields, ____currentJsonSerializerStrategy_15)); }
	inline RuntimeObject* get__currentJsonSerializerStrategy_15() const { return ____currentJsonSerializerStrategy_15; }
	inline RuntimeObject** get_address_of__currentJsonSerializerStrategy_15() { return &____currentJsonSerializerStrategy_15; }
	inline void set__currentJsonSerializerStrategy_15(RuntimeObject* value)
	{
		____currentJsonSerializerStrategy_15 = value;
		Il2CppCodeGenWriteBarrier((&____currentJsonSerializerStrategy_15), value);
	}

	inline static int32_t get_offset_of__pocoJsonSerializerStrategy_16() { return static_cast<int32_t>(offsetof(SimpleJson2_t485F125050CE1D1EA00641E8D922AC6E41EF1454_StaticFields, ____pocoJsonSerializerStrategy_16)); }
	inline PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846 * get__pocoJsonSerializerStrategy_16() const { return ____pocoJsonSerializerStrategy_16; }
	inline PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846 ** get_address_of__pocoJsonSerializerStrategy_16() { return &____pocoJsonSerializerStrategy_16; }
	inline void set__pocoJsonSerializerStrategy_16(PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846 * value)
	{
		____pocoJsonSerializerStrategy_16 = value;
		Il2CppCodeGenWriteBarrier((&____pocoJsonSerializerStrategy_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEJSON2_T485F125050CE1D1EA00641E8D922AC6E41EF1454_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef LIST_1_T05CC3C859AB5E6024394EF9A42E3E696628CA02D_H
#define LIST_1_T05CC3C859AB5E6024394EF9A42E3E696628CA02D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T05CC3C859AB5E6024394EF9A42E3E696628CA02D_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef CANVASFACEBOOK_TA57CA846ABDD0B8AEAD345296B5F9B6818BA8818_H
#define CANVASFACEBOOK_TA57CA846ABDD0B8AEAD345296B5F9B6818BA8818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Canvas.CanvasFacebook
struct  CanvasFacebook_tA57CA846ABDD0B8AEAD345296B5F9B6818BA8818  : public FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7
{
public:
	// System.String Facebook.Unity.Canvas.CanvasFacebook::appId
	String_t* ___appId_3;
	// System.String Facebook.Unity.Canvas.CanvasFacebook::appLinkUrl
	String_t* ___appLinkUrl_4;
	// Facebook.Unity.Canvas.ICanvasJSWrapper Facebook.Unity.Canvas.CanvasFacebook::canvasJSWrapper
	RuntimeObject* ___canvasJSWrapper_5;
	// Facebook.Unity.HideUnityDelegate Facebook.Unity.Canvas.CanvasFacebook::onHideUnityDelegate
	HideUnityDelegate_tFFB6D624086048B6FFFB85D9BC1946920684385B * ___onHideUnityDelegate_6;
	// System.Boolean Facebook.Unity.Canvas.CanvasFacebook::<LimitEventUsage>k__BackingField
	bool ___U3CLimitEventUsageU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_appId_3() { return static_cast<int32_t>(offsetof(CanvasFacebook_tA57CA846ABDD0B8AEAD345296B5F9B6818BA8818, ___appId_3)); }
	inline String_t* get_appId_3() const { return ___appId_3; }
	inline String_t** get_address_of_appId_3() { return &___appId_3; }
	inline void set_appId_3(String_t* value)
	{
		___appId_3 = value;
		Il2CppCodeGenWriteBarrier((&___appId_3), value);
	}

	inline static int32_t get_offset_of_appLinkUrl_4() { return static_cast<int32_t>(offsetof(CanvasFacebook_tA57CA846ABDD0B8AEAD345296B5F9B6818BA8818, ___appLinkUrl_4)); }
	inline String_t* get_appLinkUrl_4() const { return ___appLinkUrl_4; }
	inline String_t** get_address_of_appLinkUrl_4() { return &___appLinkUrl_4; }
	inline void set_appLinkUrl_4(String_t* value)
	{
		___appLinkUrl_4 = value;
		Il2CppCodeGenWriteBarrier((&___appLinkUrl_4), value);
	}

	inline static int32_t get_offset_of_canvasJSWrapper_5() { return static_cast<int32_t>(offsetof(CanvasFacebook_tA57CA846ABDD0B8AEAD345296B5F9B6818BA8818, ___canvasJSWrapper_5)); }
	inline RuntimeObject* get_canvasJSWrapper_5() const { return ___canvasJSWrapper_5; }
	inline RuntimeObject** get_address_of_canvasJSWrapper_5() { return &___canvasJSWrapper_5; }
	inline void set_canvasJSWrapper_5(RuntimeObject* value)
	{
		___canvasJSWrapper_5 = value;
		Il2CppCodeGenWriteBarrier((&___canvasJSWrapper_5), value);
	}

	inline static int32_t get_offset_of_onHideUnityDelegate_6() { return static_cast<int32_t>(offsetof(CanvasFacebook_tA57CA846ABDD0B8AEAD345296B5F9B6818BA8818, ___onHideUnityDelegate_6)); }
	inline HideUnityDelegate_tFFB6D624086048B6FFFB85D9BC1946920684385B * get_onHideUnityDelegate_6() const { return ___onHideUnityDelegate_6; }
	inline HideUnityDelegate_tFFB6D624086048B6FFFB85D9BC1946920684385B ** get_address_of_onHideUnityDelegate_6() { return &___onHideUnityDelegate_6; }
	inline void set_onHideUnityDelegate_6(HideUnityDelegate_tFFB6D624086048B6FFFB85D9BC1946920684385B * value)
	{
		___onHideUnityDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((&___onHideUnityDelegate_6), value);
	}

	inline static int32_t get_offset_of_U3CLimitEventUsageU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CanvasFacebook_tA57CA846ABDD0B8AEAD345296B5F9B6818BA8818, ___U3CLimitEventUsageU3Ek__BackingField_7)); }
	inline bool get_U3CLimitEventUsageU3Ek__BackingField_7() const { return ___U3CLimitEventUsageU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CLimitEventUsageU3Ek__BackingField_7() { return &___U3CLimitEventUsageU3Ek__BackingField_7; }
	inline void set_U3CLimitEventUsageU3Ek__BackingField_7(bool value)
	{
		___U3CLimitEventUsageU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASFACEBOOK_TA57CA846ABDD0B8AEAD345296B5F9B6818BA8818_H
#ifndef DRAWKEYASPROPERTYATTRIBUTE_T27D94881A50E07EE6B35F3E8743E632B8BC244C0_H
#define DRAWKEYASPROPERTYATTRIBUTE_T27D94881A50E07EE6B35F3E8743E632B8BC244C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.DrawKeyAsPropertyAttribute
struct  DrawKeyAsPropertyAttribute_t27D94881A50E07EE6B35F3E8743E632B8BC244C0  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWKEYASPROPERTYATTRIBUTE_T27D94881A50E07EE6B35F3E8743E632B8BC244C0_H
#ifndef IDATTRIBUTE_TDC78DB59FF3B891950A3ECA925159AFEC3B7CB50_H
#define IDATTRIBUTE_TDC78DB59FF3B891950A3ECA925159AFEC3B7CB50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.IDAttribute
struct  IDAttribute_tDC78DB59FF3B891950A3ECA925159AFEC3B7CB50  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String RotaryHeart.Lib.SerializableDictionary.IDAttribute::_id
	String_t* ____id_0;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(IDAttribute_tDC78DB59FF3B891950A3ECA925159AFEC3B7CB50, ____id_0)); }
	inline String_t* get__id_0() const { return ____id_0; }
	inline String_t** get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(String_t* value)
	{
		____id_0 = value;
		Il2CppCodeGenWriteBarrier((&____id_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDATTRIBUTE_TDC78DB59FF3B891950A3ECA925159AFEC3B7CB50_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_TACD7F1C652F364EC4D35594A9EFB8D4C6AE8F4E8_H
#define SERIALIZABLEDICTIONARYBASE_2_TACD7F1C652F364EC4D35594A9EFB8D4C6AE8F4E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<RotaryHeart.Lib.DataBaseExample_AdvancedGenericClass,System.String>
struct  SerializableDictionaryBase_2_tACD7F1C652F364EC4D35594A9EFB8D4C6AE8F4E8  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t81FE26C6650BAA689B729D08B4290D5CB879A1A8 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_t77282BCE8E9EA3D14015DFE9137D583DD325FE1B * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_t77282BCE8E9EA3D14015DFE9137D583DD325FE1B * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tACD7F1C652F364EC4D35594A9EFB8D4C6AE8F4E8, ____dict_3)); }
	inline Dictionary_2_t81FE26C6650BAA689B729D08B4290D5CB879A1A8 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t81FE26C6650BAA689B729D08B4290D5CB879A1A8 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t81FE26C6650BAA689B729D08B4290D5CB879A1A8 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tACD7F1C652F364EC4D35594A9EFB8D4C6AE8F4E8, ____keyValues_5)); }
	inline List_1_t77282BCE8E9EA3D14015DFE9137D583DD325FE1B * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_t77282BCE8E9EA3D14015DFE9137D583DD325FE1B ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_t77282BCE8E9EA3D14015DFE9137D583DD325FE1B * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tACD7F1C652F364EC4D35594A9EFB8D4C6AE8F4E8, ____keys_6)); }
	inline List_1_t77282BCE8E9EA3D14015DFE9137D583DD325FE1B * get__keys_6() const { return ____keys_6; }
	inline List_1_t77282BCE8E9EA3D14015DFE9137D583DD325FE1B ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_t77282BCE8E9EA3D14015DFE9137D583DD325FE1B * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tACD7F1C652F364EC4D35594A9EFB8D4C6AE8F4E8, ____values_7)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__values_7() const { return ____values_7; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_tACD7F1C652F364EC4D35594A9EFB8D4C6AE8F4E8_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t81FE26C6650BAA689B729D08B4290D5CB879A1A8 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tACD7F1C652F364EC4D35594A9EFB8D4C6AE8F4E8_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t81FE26C6650BAA689B729D08B4290D5CB879A1A8 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t81FE26C6650BAA689B729D08B4290D5CB879A1A8 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t81FE26C6650BAA689B729D08B4290D5CB879A1A8 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_TACD7F1C652F364EC4D35594A9EFB8D4C6AE8F4E8_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_TE87416068BD8CE69FA4EA85E90946D6686555057_H
#define SERIALIZABLEDICTIONARYBASE_2_TE87416068BD8CE69FA4EA85E90946D6686555057_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<RotaryHeart.Lib.DataBaseExample_ClassTest,RotaryHeart.Lib.DataBaseExample_ClassTest>
struct  SerializableDictionaryBase_2_tE87416068BD8CE69FA4EA85E90946D6686555057  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t161799848E67B8AC48DCF5502DDC8CA3F61549E4 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tE87416068BD8CE69FA4EA85E90946D6686555057, ____dict_3)); }
	inline Dictionary_2_t161799848E67B8AC48DCF5502DDC8CA3F61549E4 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t161799848E67B8AC48DCF5502DDC8CA3F61549E4 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t161799848E67B8AC48DCF5502DDC8CA3F61549E4 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tE87416068BD8CE69FA4EA85E90946D6686555057, ____keyValues_5)); }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tE87416068BD8CE69FA4EA85E90946D6686555057, ____keys_6)); }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * get__keys_6() const { return ____keys_6; }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tE87416068BD8CE69FA4EA85E90946D6686555057, ____values_7)); }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * get__values_7() const { return ____values_7; }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_tE87416068BD8CE69FA4EA85E90946D6686555057_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t161799848E67B8AC48DCF5502DDC8CA3F61549E4 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tE87416068BD8CE69FA4EA85E90946D6686555057_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t161799848E67B8AC48DCF5502DDC8CA3F61549E4 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t161799848E67B8AC48DCF5502DDC8CA3F61549E4 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t161799848E67B8AC48DCF5502DDC8CA3F61549E4 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_TE87416068BD8CE69FA4EA85E90946D6686555057_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T5F4B41346E67EFDB471821FB3E9612259FCBF971_H
#define SERIALIZABLEDICTIONARYBASE_2_T5F4B41346E67EFDB471821FB3E9612259FCBF971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<RotaryHeart.Lib.DataBaseExample_ClassTest,System.String>
struct  SerializableDictionaryBase_2_t5F4B41346E67EFDB471821FB3E9612259FCBF971  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t8FF731C4650354DDA77DC668BA0897CCDDF12464 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t5F4B41346E67EFDB471821FB3E9612259FCBF971, ____dict_3)); }
	inline Dictionary_2_t8FF731C4650354DDA77DC668BA0897CCDDF12464 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t8FF731C4650354DDA77DC668BA0897CCDDF12464 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t8FF731C4650354DDA77DC668BA0897CCDDF12464 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t5F4B41346E67EFDB471821FB3E9612259FCBF971, ____keyValues_5)); }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t5F4B41346E67EFDB471821FB3E9612259FCBF971, ____keys_6)); }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * get__keys_6() const { return ____keys_6; }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t5F4B41346E67EFDB471821FB3E9612259FCBF971, ____values_7)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__values_7() const { return ____values_7; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t5F4B41346E67EFDB471821FB3E9612259FCBF971_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t8FF731C4650354DDA77DC668BA0897CCDDF12464 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t5F4B41346E67EFDB471821FB3E9612259FCBF971_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t8FF731C4650354DDA77DC668BA0897CCDDF12464 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t8FF731C4650354DDA77DC668BA0897CCDDF12464 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t8FF731C4650354DDA77DC668BA0897CCDDF12464 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T5F4B41346E67EFDB471821FB3E9612259FCBF971_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T7F050F7FC8A742CEDFDCE14AB3BDB733ACF132CA_H
#define SERIALIZABLEDICTIONARYBASE_2_T7F050F7FC8A742CEDFDCE14AB3BDB733ACF132CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<RotaryHeart.Lib.DataBaseExample_EnumExample,System.String>
struct  SerializableDictionaryBase_2_t7F050F7FC8A742CEDFDCE14AB3BDB733ACF132CA  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_tAD91837F8573D567EAED8298F204D4602B34B4B9 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_t0F98B738728348B8BD34C9F8899D2DCE3F4029E0 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_t0F98B738728348B8BD34C9F8899D2DCE3F4029E0 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t7F050F7FC8A742CEDFDCE14AB3BDB733ACF132CA, ____dict_3)); }
	inline Dictionary_2_tAD91837F8573D567EAED8298F204D4602B34B4B9 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_tAD91837F8573D567EAED8298F204D4602B34B4B9 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_tAD91837F8573D567EAED8298F204D4602B34B4B9 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t7F050F7FC8A742CEDFDCE14AB3BDB733ACF132CA, ____keyValues_5)); }
	inline List_1_t0F98B738728348B8BD34C9F8899D2DCE3F4029E0 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_t0F98B738728348B8BD34C9F8899D2DCE3F4029E0 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_t0F98B738728348B8BD34C9F8899D2DCE3F4029E0 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t7F050F7FC8A742CEDFDCE14AB3BDB733ACF132CA, ____keys_6)); }
	inline List_1_t0F98B738728348B8BD34C9F8899D2DCE3F4029E0 * get__keys_6() const { return ____keys_6; }
	inline List_1_t0F98B738728348B8BD34C9F8899D2DCE3F4029E0 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_t0F98B738728348B8BD34C9F8899D2DCE3F4029E0 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t7F050F7FC8A742CEDFDCE14AB3BDB733ACF132CA, ____values_7)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__values_7() const { return ____values_7; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t7F050F7FC8A742CEDFDCE14AB3BDB733ACF132CA_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_tAD91837F8573D567EAED8298F204D4602B34B4B9 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t7F050F7FC8A742CEDFDCE14AB3BDB733ACF132CA_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_tAD91837F8573D567EAED8298F204D4602B34B4B9 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_tAD91837F8573D567EAED8298F204D4602B34B4B9 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_tAD91837F8573D567EAED8298F204D4602B34B4B9 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T7F050F7FC8A742CEDFDCE14AB3BDB733ACF132CA_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T6B59A17E43941D182A35D46DE6F79219F3291D6C_H
#define SERIALIZABLEDICTIONARYBASE_2_T6B59A17E43941D182A35D46DE6F79219F3291D6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.Char,System.Int32>
struct  SerializableDictionaryBase_2_t6B59A17E43941D182A35D46DE6F79219F3291D6C  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_tD824C6AA50393E55534B6616C544907A7EE94E58 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t6B59A17E43941D182A35D46DE6F79219F3291D6C, ____dict_3)); }
	inline Dictionary_2_tD824C6AA50393E55534B6616C544907A7EE94E58 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_tD824C6AA50393E55534B6616C544907A7EE94E58 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_tD824C6AA50393E55534B6616C544907A7EE94E58 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t6B59A17E43941D182A35D46DE6F79219F3291D6C, ____keyValues_5)); }
	inline List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_t028AAE01C4834286B7892F4498364F964CD8B316 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t6B59A17E43941D182A35D46DE6F79219F3291D6C, ____keys_6)); }
	inline List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * get__keys_6() const { return ____keys_6; }
	inline List_1_t028AAE01C4834286B7892F4498364F964CD8B316 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t6B59A17E43941D182A35D46DE6F79219F3291D6C, ____values_7)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__values_7() const { return ____values_7; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t6B59A17E43941D182A35D46DE6F79219F3291D6C_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_tD824C6AA50393E55534B6616C544907A7EE94E58 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t6B59A17E43941D182A35D46DE6F79219F3291D6C_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_tD824C6AA50393E55534B6616C544907A7EE94E58 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_tD824C6AA50393E55534B6616C544907A7EE94E58 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_tD824C6AA50393E55534B6616C544907A7EE94E58 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T6B59A17E43941D182A35D46DE6F79219F3291D6C_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_TFFDF0C38CB7AF78D1EBBFD7A390DBA9A8C3FF625_H
#define SERIALIZABLEDICTIONARYBASE_2_TFFDF0C38CB7AF78D1EBBFD7A390DBA9A8C3FF625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.Int32,RotaryHeart.Lib.DataBaseExample_ArrayTest>
struct  SerializableDictionaryBase_2_tFFDF0C38CB7AF78D1EBBFD7A390DBA9A8C3FF625  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t49F16597D652BA226A411B6AB80DECD1EEDD9BF4 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_t6EB6DDE4C420711C1C57DEC763D0631C37A99FDE * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tFFDF0C38CB7AF78D1EBBFD7A390DBA9A8C3FF625, ____dict_3)); }
	inline Dictionary_2_t49F16597D652BA226A411B6AB80DECD1EEDD9BF4 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t49F16597D652BA226A411B6AB80DECD1EEDD9BF4 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t49F16597D652BA226A411B6AB80DECD1EEDD9BF4 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tFFDF0C38CB7AF78D1EBBFD7A390DBA9A8C3FF625, ____keyValues_5)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tFFDF0C38CB7AF78D1EBBFD7A390DBA9A8C3FF625, ____keys_6)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tFFDF0C38CB7AF78D1EBBFD7A390DBA9A8C3FF625, ____values_7)); }
	inline List_1_t6EB6DDE4C420711C1C57DEC763D0631C37A99FDE * get__values_7() const { return ____values_7; }
	inline List_1_t6EB6DDE4C420711C1C57DEC763D0631C37A99FDE ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_t6EB6DDE4C420711C1C57DEC763D0631C37A99FDE * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_tFFDF0C38CB7AF78D1EBBFD7A390DBA9A8C3FF625_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t49F16597D652BA226A411B6AB80DECD1EEDD9BF4 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tFFDF0C38CB7AF78D1EBBFD7A390DBA9A8C3FF625_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t49F16597D652BA226A411B6AB80DECD1EEDD9BF4 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t49F16597D652BA226A411B6AB80DECD1EEDD9BF4 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t49F16597D652BA226A411B6AB80DECD1EEDD9BF4 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_TFFDF0C38CB7AF78D1EBBFD7A390DBA9A8C3FF625_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_TB1D4518FD301A9EBD647F1C40DD8C8A7EAE0A244_H
#define SERIALIZABLEDICTIONARYBASE_2_TB1D4518FD301A9EBD647F1C40DD8C8A7EAE0A244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.Int32,RotaryHeart.Lib.DataBaseExample_ClassTest>
struct  SerializableDictionaryBase_2_tB1D4518FD301A9EBD647F1C40DD8C8A7EAE0A244  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_tBD161D2E9667459A55A4444FBBD6BF1CA902D10E * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tB1D4518FD301A9EBD647F1C40DD8C8A7EAE0A244, ____dict_3)); }
	inline Dictionary_2_tBD161D2E9667459A55A4444FBBD6BF1CA902D10E * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_tBD161D2E9667459A55A4444FBBD6BF1CA902D10E ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_tBD161D2E9667459A55A4444FBBD6BF1CA902D10E * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tB1D4518FD301A9EBD647F1C40DD8C8A7EAE0A244, ____keyValues_5)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tB1D4518FD301A9EBD647F1C40DD8C8A7EAE0A244, ____keys_6)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tB1D4518FD301A9EBD647F1C40DD8C8A7EAE0A244, ____values_7)); }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * get__values_7() const { return ____values_7; }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_tB1D4518FD301A9EBD647F1C40DD8C8A7EAE0A244_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_tBD161D2E9667459A55A4444FBBD6BF1CA902D10E * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tB1D4518FD301A9EBD647F1C40DD8C8A7EAE0A244_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_tBD161D2E9667459A55A4444FBBD6BF1CA902D10E * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_tBD161D2E9667459A55A4444FBBD6BF1CA902D10E ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_tBD161D2E9667459A55A4444FBBD6BF1CA902D10E * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_TB1D4518FD301A9EBD647F1C40DD8C8A7EAE0A244_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_TAE61515978EBDB44DC99991B7A454224317B7C10_H
#define SERIALIZABLEDICTIONARYBASE_2_TAE61515978EBDB44DC99991B7A454224317B7C10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.Int32,RotaryHeart.Lib.NestedExample>
struct  SerializableDictionaryBase_2_tAE61515978EBDB44DC99991B7A454224317B7C10  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t0420B24667EE5A4C7B9E9282CD612D88A0F1F481 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tC666E449BA887A32F53E5586940AEE7F01378E06 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tAE61515978EBDB44DC99991B7A454224317B7C10, ____dict_3)); }
	inline Dictionary_2_t0420B24667EE5A4C7B9E9282CD612D88A0F1F481 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t0420B24667EE5A4C7B9E9282CD612D88A0F1F481 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t0420B24667EE5A4C7B9E9282CD612D88A0F1F481 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tAE61515978EBDB44DC99991B7A454224317B7C10, ____keyValues_5)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tAE61515978EBDB44DC99991B7A454224317B7C10, ____keys_6)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tAE61515978EBDB44DC99991B7A454224317B7C10, ____values_7)); }
	inline List_1_tC666E449BA887A32F53E5586940AEE7F01378E06 * get__values_7() const { return ____values_7; }
	inline List_1_tC666E449BA887A32F53E5586940AEE7F01378E06 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tC666E449BA887A32F53E5586940AEE7F01378E06 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_tAE61515978EBDB44DC99991B7A454224317B7C10_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t0420B24667EE5A4C7B9E9282CD612D88A0F1F481 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tAE61515978EBDB44DC99991B7A454224317B7C10_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t0420B24667EE5A4C7B9E9282CD612D88A0F1F481 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t0420B24667EE5A4C7B9E9282CD612D88A0F1F481 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t0420B24667EE5A4C7B9E9282CD612D88A0F1F481 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_TAE61515978EBDB44DC99991B7A454224317B7C10_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_TA9B1FA789C08F26EAFD2F2A241ED8BD42965BE3A_H
#define SERIALIZABLEDICTIONARYBASE_2_TA9B1FA789C08F26EAFD2F2A241ED8BD42965BE3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.Int32,UnityEngine.GameObject>
struct  SerializableDictionaryBase_2_tA9B1FA789C08F26EAFD2F2A241ED8BD42965BE3A  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t1DA4B446B1C6B709FEA5D9DAB5F606BE0CC4FD2D * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tA9B1FA789C08F26EAFD2F2A241ED8BD42965BE3A, ____dict_3)); }
	inline Dictionary_2_t1DA4B446B1C6B709FEA5D9DAB5F606BE0CC4FD2D * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t1DA4B446B1C6B709FEA5D9DAB5F606BE0CC4FD2D ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t1DA4B446B1C6B709FEA5D9DAB5F606BE0CC4FD2D * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tA9B1FA789C08F26EAFD2F2A241ED8BD42965BE3A, ____keyValues_5)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tA9B1FA789C08F26EAFD2F2A241ED8BD42965BE3A, ____keys_6)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tA9B1FA789C08F26EAFD2F2A241ED8BD42965BE3A, ____values_7)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get__values_7() const { return ____values_7; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_tA9B1FA789C08F26EAFD2F2A241ED8BD42965BE3A_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t1DA4B446B1C6B709FEA5D9DAB5F606BE0CC4FD2D * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tA9B1FA789C08F26EAFD2F2A241ED8BD42965BE3A_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t1DA4B446B1C6B709FEA5D9DAB5F606BE0CC4FD2D * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t1DA4B446B1C6B709FEA5D9DAB5F606BE0CC4FD2D ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t1DA4B446B1C6B709FEA5D9DAB5F606BE0CC4FD2D * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_TA9B1FA789C08F26EAFD2F2A241ED8BD42965BE3A_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T44F515EFA995D9ACB8AA4FE67BF16854EC1C23FC_H
#define SERIALIZABLEDICTIONARYBASE_2_T44F515EFA995D9ACB8AA4FE67BF16854EC1C23FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.String,RotaryHeart.Lib.DataBaseExample_ClassTest>
struct  SerializableDictionaryBase_2_t44F515EFA995D9ACB8AA4FE67BF16854EC1C23FC  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t0DCCC9FD10C6B9438ED2F2275A8B58B2E18505A9 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t44F515EFA995D9ACB8AA4FE67BF16854EC1C23FC, ____dict_3)); }
	inline Dictionary_2_t0DCCC9FD10C6B9438ED2F2275A8B58B2E18505A9 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t0DCCC9FD10C6B9438ED2F2275A8B58B2E18505A9 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t0DCCC9FD10C6B9438ED2F2275A8B58B2E18505A9 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t44F515EFA995D9ACB8AA4FE67BF16854EC1C23FC, ____keyValues_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t44F515EFA995D9ACB8AA4FE67BF16854EC1C23FC, ____keys_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t44F515EFA995D9ACB8AA4FE67BF16854EC1C23FC, ____values_7)); }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * get__values_7() const { return ____values_7; }
	inline List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_t3A0B15C0953E95802105DAA511A346F5DEBFC0FC * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t44F515EFA995D9ACB8AA4FE67BF16854EC1C23FC_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t0DCCC9FD10C6B9438ED2F2275A8B58B2E18505A9 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t44F515EFA995D9ACB8AA4FE67BF16854EC1C23FC_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t0DCCC9FD10C6B9438ED2F2275A8B58B2E18505A9 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t0DCCC9FD10C6B9438ED2F2275A8B58B2E18505A9 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t0DCCC9FD10C6B9438ED2F2275A8B58B2E18505A9 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T44F515EFA995D9ACB8AA4FE67BF16854EC1C23FC_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_TEFCBF7B1E04CC17B2D53A587584601A8947A894A_H
#define SERIALIZABLEDICTIONARYBASE_2_TEFCBF7B1E04CC17B2D53A587584601A8947A894A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.String,RotaryHeart.Lib.Example>
struct  SerializableDictionaryBase_2_tEFCBF7B1E04CC17B2D53A587584601A8947A894A  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t38129F23BD62C870C4474E1800ED8E832FC24C82 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tC80F0C11C3B192C702CB0A1A7A8AF82A017F93C6 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tEFCBF7B1E04CC17B2D53A587584601A8947A894A, ____dict_3)); }
	inline Dictionary_2_t38129F23BD62C870C4474E1800ED8E832FC24C82 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t38129F23BD62C870C4474E1800ED8E832FC24C82 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t38129F23BD62C870C4474E1800ED8E832FC24C82 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tEFCBF7B1E04CC17B2D53A587584601A8947A894A, ____keyValues_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tEFCBF7B1E04CC17B2D53A587584601A8947A894A, ____keys_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tEFCBF7B1E04CC17B2D53A587584601A8947A894A, ____values_7)); }
	inline List_1_tC80F0C11C3B192C702CB0A1A7A8AF82A017F93C6 * get__values_7() const { return ____values_7; }
	inline List_1_tC80F0C11C3B192C702CB0A1A7A8AF82A017F93C6 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tC80F0C11C3B192C702CB0A1A7A8AF82A017F93C6 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_tEFCBF7B1E04CC17B2D53A587584601A8947A894A_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t38129F23BD62C870C4474E1800ED8E832FC24C82 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tEFCBF7B1E04CC17B2D53A587584601A8947A894A_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t38129F23BD62C870C4474E1800ED8E832FC24C82 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t38129F23BD62C870C4474E1800ED8E832FC24C82 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t38129F23BD62C870C4474E1800ED8E832FC24C82 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_TEFCBF7B1E04CC17B2D53A587584601A8947A894A_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_TEC2815A67CA7F1AF4F2E421A960BBB02B07C158A_H
#define SERIALIZABLEDICTIONARYBASE_2_TEC2815A67CA7F1AF4F2E421A960BBB02B07C158A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.String,UnityEngine.AudioClip>
struct  SerializableDictionaryBase_2_tEC2815A67CA7F1AF4F2E421A960BBB02B07C158A  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_tBC4ABC1740BC695EC6AE67BA579E0743A89373D6 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tEC2815A67CA7F1AF4F2E421A960BBB02B07C158A, ____dict_3)); }
	inline Dictionary_2_tBC4ABC1740BC695EC6AE67BA579E0743A89373D6 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_tBC4ABC1740BC695EC6AE67BA579E0743A89373D6 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_tBC4ABC1740BC695EC6AE67BA579E0743A89373D6 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tEC2815A67CA7F1AF4F2E421A960BBB02B07C158A, ____keyValues_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tEC2815A67CA7F1AF4F2E421A960BBB02B07C158A, ____keys_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tEC2815A67CA7F1AF4F2E421A960BBB02B07C158A, ____values_7)); }
	inline List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F * get__values_7() const { return ____values_7; }
	inline List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_tEC2815A67CA7F1AF4F2E421A960BBB02B07C158A_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_tBC4ABC1740BC695EC6AE67BA579E0743A89373D6 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tEC2815A67CA7F1AF4F2E421A960BBB02B07C158A_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_tBC4ABC1740BC695EC6AE67BA579E0743A89373D6 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_tBC4ABC1740BC695EC6AE67BA579E0743A89373D6 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_tBC4ABC1740BC695EC6AE67BA579E0743A89373D6 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_TEC2815A67CA7F1AF4F2E421A960BBB02B07C158A_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T62F104185D4C98554D457168BDA5520AD50ECC54_H
#define SERIALIZABLEDICTIONARYBASE_2_T62F104185D4C98554D457168BDA5520AD50ECC54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.String,UnityEngine.GameObject>
struct  SerializableDictionaryBase_2_t62F104185D4C98554D457168BDA5520AD50ECC54  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t62F104185D4C98554D457168BDA5520AD50ECC54, ____dict_3)); }
	inline Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t62F104185D4C98554D457168BDA5520AD50ECC54, ____keyValues_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t62F104185D4C98554D457168BDA5520AD50ECC54, ____keys_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t62F104185D4C98554D457168BDA5520AD50ECC54, ____values_7)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get__values_7() const { return ____values_7; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t62F104185D4C98554D457168BDA5520AD50ECC54_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t62F104185D4C98554D457168BDA5520AD50ECC54_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T62F104185D4C98554D457168BDA5520AD50ECC54_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T1DDDEEFB251EF3CB9ADD494275EA4A0147F559C8_H
#define SERIALIZABLEDICTIONARYBASE_2_T1DDDEEFB251EF3CB9ADD494275EA4A0147F559C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.String,UnityEngine.Material>
struct  SerializableDictionaryBase_2_t1DDDEEFB251EF3CB9ADD494275EA4A0147F559C8  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_t6A61046573B0BC4E12950B90305C189DD041D786 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1DDDEEFB251EF3CB9ADD494275EA4A0147F559C8, ____dict_3)); }
	inline Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1DDDEEFB251EF3CB9ADD494275EA4A0147F559C8, ____keyValues_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1DDDEEFB251EF3CB9ADD494275EA4A0147F559C8, ____keys_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1DDDEEFB251EF3CB9ADD494275EA4A0147F559C8, ____values_7)); }
	inline List_1_t6A61046573B0BC4E12950B90305C189DD041D786 * get__values_7() const { return ____values_7; }
	inline List_1_t6A61046573B0BC4E12950B90305C189DD041D786 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_t6A61046573B0BC4E12950B90305C189DD041D786 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t1DDDEEFB251EF3CB9ADD494275EA4A0147F559C8_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1DDDEEFB251EF3CB9ADD494275EA4A0147F559C8_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T1DDDEEFB251EF3CB9ADD494275EA4A0147F559C8_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T483E5ABCAACE81029253C43ABA3B959C7475B01E_H
#define SERIALIZABLEDICTIONARYBASE_2_T483E5ABCAACE81029253C43ABA3B959C7475B01E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<System.String,UnityEngine.Sprite>
struct  SerializableDictionaryBase_2_t483E5ABCAACE81029253C43ABA3B959C7475B01E  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t08107DE97D3661D374D54356092C44C550883EA9 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_t18801B80DBA94AAA8DF21D6ED1C15CF944BCA6CC * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t483E5ABCAACE81029253C43ABA3B959C7475B01E, ____dict_3)); }
	inline Dictionary_2_t08107DE97D3661D374D54356092C44C550883EA9 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t08107DE97D3661D374D54356092C44C550883EA9 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t08107DE97D3661D374D54356092C44C550883EA9 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t483E5ABCAACE81029253C43ABA3B959C7475B01E, ____keyValues_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t483E5ABCAACE81029253C43ABA3B959C7475B01E, ____keys_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__keys_6() const { return ____keys_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t483E5ABCAACE81029253C43ABA3B959C7475B01E, ____values_7)); }
	inline List_1_t18801B80DBA94AAA8DF21D6ED1C15CF944BCA6CC * get__values_7() const { return ____values_7; }
	inline List_1_t18801B80DBA94AAA8DF21D6ED1C15CF944BCA6CC ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_t18801B80DBA94AAA8DF21D6ED1C15CF944BCA6CC * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t483E5ABCAACE81029253C43ABA3B959C7475B01E_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t08107DE97D3661D374D54356092C44C550883EA9 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t483E5ABCAACE81029253C43ABA3B959C7475B01E_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t08107DE97D3661D374D54356092C44C550883EA9 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t08107DE97D3661D374D54356092C44C550883EA9 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t08107DE97D3661D374D54356092C44C550883EA9 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T483E5ABCAACE81029253C43ABA3B959C7475B01E_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T41FCA43FB71C4D9AE1D9E7EB346436803AEE2674_H
#define SERIALIZABLEDICTIONARYBASE_2_T41FCA43FB71C4D9AE1D9E7EB346436803AEE2674_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<UnityEngine.AudioClip,System.String>
struct  SerializableDictionaryBase_2_t41FCA43FB71C4D9AE1D9E7EB346436803AEE2674  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_tBB446FB0CDF4BB22336D7FF077278F8E508E82B6 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t41FCA43FB71C4D9AE1D9E7EB346436803AEE2674, ____dict_3)); }
	inline Dictionary_2_tBB446FB0CDF4BB22336D7FF077278F8E508E82B6 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_tBB446FB0CDF4BB22336D7FF077278F8E508E82B6 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_tBB446FB0CDF4BB22336D7FF077278F8E508E82B6 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t41FCA43FB71C4D9AE1D9E7EB346436803AEE2674, ____keyValues_5)); }
	inline List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t41FCA43FB71C4D9AE1D9E7EB346436803AEE2674, ____keys_6)); }
	inline List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F * get__keys_6() const { return ____keys_6; }
	inline List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE0FA93AA3DC624E2BF3DC417F5FEBE1DBD81256F * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t41FCA43FB71C4D9AE1D9E7EB346436803AEE2674, ____values_7)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__values_7() const { return ____values_7; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t41FCA43FB71C4D9AE1D9E7EB346436803AEE2674_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_tBB446FB0CDF4BB22336D7FF077278F8E508E82B6 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t41FCA43FB71C4D9AE1D9E7EB346436803AEE2674_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_tBB446FB0CDF4BB22336D7FF077278F8E508E82B6 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_tBB446FB0CDF4BB22336D7FF077278F8E508E82B6 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_tBB446FB0CDF4BB22336D7FF077278F8E508E82B6 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T41FCA43FB71C4D9AE1D9E7EB346436803AEE2674_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_TBB60A10022065EC629D16670D449E96E4EA22BF5_H
#define SERIALIZABLEDICTIONARYBASE_2_TBB60A10022065EC629D16670D449E96E4EA22BF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<UnityEngine.GameObject,System.Int32>
struct  SerializableDictionaryBase_2_tBB60A10022065EC629D16670D449E96E4EA22BF5  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t96FB2F26C7CE603F75E00CA02CCD843EA785C29D * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tBB60A10022065EC629D16670D449E96E4EA22BF5, ____dict_3)); }
	inline Dictionary_2_t96FB2F26C7CE603F75E00CA02CCD843EA785C29D * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t96FB2F26C7CE603F75E00CA02CCD843EA785C29D ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t96FB2F26C7CE603F75E00CA02CCD843EA785C29D * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tBB60A10022065EC629D16670D449E96E4EA22BF5, ____keyValues_5)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tBB60A10022065EC629D16670D449E96E4EA22BF5, ____keys_6)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get__keys_6() const { return ____keys_6; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tBB60A10022065EC629D16670D449E96E4EA22BF5, ____values_7)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__values_7() const { return ____values_7; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_tBB60A10022065EC629D16670D449E96E4EA22BF5_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t96FB2F26C7CE603F75E00CA02CCD843EA785C29D * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tBB60A10022065EC629D16670D449E96E4EA22BF5_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t96FB2F26C7CE603F75E00CA02CCD843EA785C29D * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t96FB2F26C7CE603F75E00CA02CCD843EA785C29D ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t96FB2F26C7CE603F75E00CA02CCD843EA785C29D * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_TBB60A10022065EC629D16670D449E96E4EA22BF5_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T1E983AB88E547EC20ACFC9B841B9ECB2FB82344F_H
#define SERIALIZABLEDICTIONARYBASE_2_T1E983AB88E547EC20ACFC9B841B9ECB2FB82344F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<UnityEngine.GameObject,System.String>
struct  SerializableDictionaryBase_2_t1E983AB88E547EC20ACFC9B841B9ECB2FB82344F  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t50497CD2E34AB82809A66864767B8A6BB9A354DA * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1E983AB88E547EC20ACFC9B841B9ECB2FB82344F, ____dict_3)); }
	inline Dictionary_2_t50497CD2E34AB82809A66864767B8A6BB9A354DA * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t50497CD2E34AB82809A66864767B8A6BB9A354DA ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t50497CD2E34AB82809A66864767B8A6BB9A354DA * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1E983AB88E547EC20ACFC9B841B9ECB2FB82344F, ____keyValues_5)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1E983AB88E547EC20ACFC9B841B9ECB2FB82344F, ____keys_6)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get__keys_6() const { return ____keys_6; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1E983AB88E547EC20ACFC9B841B9ECB2FB82344F, ____values_7)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__values_7() const { return ____values_7; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t1E983AB88E547EC20ACFC9B841B9ECB2FB82344F_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t50497CD2E34AB82809A66864767B8A6BB9A354DA * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1E983AB88E547EC20ACFC9B841B9ECB2FB82344F_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t50497CD2E34AB82809A66864767B8A6BB9A354DA * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t50497CD2E34AB82809A66864767B8A6BB9A354DA ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t50497CD2E34AB82809A66864767B8A6BB9A354DA * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T1E983AB88E547EC20ACFC9B841B9ECB2FB82344F_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T460943D8C8C8BBED979A1BCCCC83201E87046397_H
#define SERIALIZABLEDICTIONARYBASE_2_T460943D8C8C8BBED979A1BCCCC83201E87046397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<UnityEngine.Gradient,System.Int32>
struct  SerializableDictionaryBase_2_t460943D8C8C8BBED979A1BCCCC83201E87046397  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_tA0816A5D7E1B085B03EBDB113C4BE44A30D8FBE5 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tE3A3FBC3263837B20EB0B0E537706BA254A2675B * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tE3A3FBC3263837B20EB0B0E537706BA254A2675B * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t460943D8C8C8BBED979A1BCCCC83201E87046397, ____dict_3)); }
	inline Dictionary_2_tA0816A5D7E1B085B03EBDB113C4BE44A30D8FBE5 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_tA0816A5D7E1B085B03EBDB113C4BE44A30D8FBE5 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_tA0816A5D7E1B085B03EBDB113C4BE44A30D8FBE5 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t460943D8C8C8BBED979A1BCCCC83201E87046397, ____keyValues_5)); }
	inline List_1_tE3A3FBC3263837B20EB0B0E537706BA254A2675B * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tE3A3FBC3263837B20EB0B0E537706BA254A2675B ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tE3A3FBC3263837B20EB0B0E537706BA254A2675B * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t460943D8C8C8BBED979A1BCCCC83201E87046397, ____keys_6)); }
	inline List_1_tE3A3FBC3263837B20EB0B0E537706BA254A2675B * get__keys_6() const { return ____keys_6; }
	inline List_1_tE3A3FBC3263837B20EB0B0E537706BA254A2675B ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tE3A3FBC3263837B20EB0B0E537706BA254A2675B * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t460943D8C8C8BBED979A1BCCCC83201E87046397, ____values_7)); }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * get__values_7() const { return ____values_7; }
	inline List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t460943D8C8C8BBED979A1BCCCC83201E87046397_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_tA0816A5D7E1B085B03EBDB113C4BE44A30D8FBE5 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t460943D8C8C8BBED979A1BCCCC83201E87046397_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_tA0816A5D7E1B085B03EBDB113C4BE44A30D8FBE5 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_tA0816A5D7E1B085B03EBDB113C4BE44A30D8FBE5 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_tA0816A5D7E1B085B03EBDB113C4BE44A30D8FBE5 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T460943D8C8C8BBED979A1BCCCC83201E87046397_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T1988DAAFA463263A7102770DC3F95D3A649CA411_H
#define SERIALIZABLEDICTIONARYBASE_2_T1988DAAFA463263A7102770DC3F95D3A649CA411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<UnityEngine.Material,System.String>
struct  SerializableDictionaryBase_2_t1988DAAFA463263A7102770DC3F95D3A649CA411  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t09233B7D878DE2EAEA5F1457E153270B46308364 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_t6A61046573B0BC4E12950B90305C189DD041D786 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_t6A61046573B0BC4E12950B90305C189DD041D786 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1988DAAFA463263A7102770DC3F95D3A649CA411, ____dict_3)); }
	inline Dictionary_2_t09233B7D878DE2EAEA5F1457E153270B46308364 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t09233B7D878DE2EAEA5F1457E153270B46308364 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t09233B7D878DE2EAEA5F1457E153270B46308364 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1988DAAFA463263A7102770DC3F95D3A649CA411, ____keyValues_5)); }
	inline List_1_t6A61046573B0BC4E12950B90305C189DD041D786 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_t6A61046573B0BC4E12950B90305C189DD041D786 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_t6A61046573B0BC4E12950B90305C189DD041D786 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1988DAAFA463263A7102770DC3F95D3A649CA411, ____keys_6)); }
	inline List_1_t6A61046573B0BC4E12950B90305C189DD041D786 * get__keys_6() const { return ____keys_6; }
	inline List_1_t6A61046573B0BC4E12950B90305C189DD041D786 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_t6A61046573B0BC4E12950B90305C189DD041D786 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1988DAAFA463263A7102770DC3F95D3A649CA411, ____values_7)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__values_7() const { return ____values_7; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t1988DAAFA463263A7102770DC3F95D3A649CA411_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t09233B7D878DE2EAEA5F1457E153270B46308364 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t1988DAAFA463263A7102770DC3F95D3A649CA411_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t09233B7D878DE2EAEA5F1457E153270B46308364 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t09233B7D878DE2EAEA5F1457E153270B46308364 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t09233B7D878DE2EAEA5F1457E153270B46308364 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T1988DAAFA463263A7102770DC3F95D3A649CA411_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_TE267F63ABBFAA44DB1D240576C39BB62417AAA66_H
#define SERIALIZABLEDICTIONARYBASE_2_TE267F63ABBFAA44DB1D240576C39BB62417AAA66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<UnityEngine.Quaternion,UnityEngine.Vector3>
struct  SerializableDictionaryBase_2_tE267F63ABBFAA44DB1D240576C39BB62417AAA66  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_tC3E5167C8C46E8FB66C88021C5A1EC72F6E5AAF9 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_t74314CADF5799483B2C71785AC688DD9618023F4 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_t74314CADF5799483B2C71785AC688DD9618023F4 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tE267F63ABBFAA44DB1D240576C39BB62417AAA66, ____dict_3)); }
	inline Dictionary_2_tC3E5167C8C46E8FB66C88021C5A1EC72F6E5AAF9 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_tC3E5167C8C46E8FB66C88021C5A1EC72F6E5AAF9 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_tC3E5167C8C46E8FB66C88021C5A1EC72F6E5AAF9 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tE267F63ABBFAA44DB1D240576C39BB62417AAA66, ____keyValues_5)); }
	inline List_1_t74314CADF5799483B2C71785AC688DD9618023F4 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_t74314CADF5799483B2C71785AC688DD9618023F4 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_t74314CADF5799483B2C71785AC688DD9618023F4 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tE267F63ABBFAA44DB1D240576C39BB62417AAA66, ____keys_6)); }
	inline List_1_t74314CADF5799483B2C71785AC688DD9618023F4 * get__keys_6() const { return ____keys_6; }
	inline List_1_t74314CADF5799483B2C71785AC688DD9618023F4 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_t74314CADF5799483B2C71785AC688DD9618023F4 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tE267F63ABBFAA44DB1D240576C39BB62417AAA66, ____values_7)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get__values_7() const { return ____values_7; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_tE267F63ABBFAA44DB1D240576C39BB62417AAA66_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_tC3E5167C8C46E8FB66C88021C5A1EC72F6E5AAF9 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tE267F63ABBFAA44DB1D240576C39BB62417AAA66_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_tC3E5167C8C46E8FB66C88021C5A1EC72F6E5AAF9 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_tC3E5167C8C46E8FB66C88021C5A1EC72F6E5AAF9 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_tC3E5167C8C46E8FB66C88021C5A1EC72F6E5AAF9 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_TE267F63ABBFAA44DB1D240576C39BB62417AAA66_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_TF9C98B1569ADB35FACDE8F82B3E3007C790EDFB3_H
#define SERIALIZABLEDICTIONARYBASE_2_TF9C98B1569ADB35FACDE8F82B3E3007C790EDFB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<UnityEngine.QueryTriggerInteraction,System.String>
struct  SerializableDictionaryBase_2_tF9C98B1569ADB35FACDE8F82B3E3007C790EDFB3  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t6DA0A4193816CE464E6B64C19A04A5F4E75FCC0A * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tB78E0FB5C13F06BAADE39A8FD64F6672E3D7FAEF * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tB78E0FB5C13F06BAADE39A8FD64F6672E3D7FAEF * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tF9C98B1569ADB35FACDE8F82B3E3007C790EDFB3, ____dict_3)); }
	inline Dictionary_2_t6DA0A4193816CE464E6B64C19A04A5F4E75FCC0A * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t6DA0A4193816CE464E6B64C19A04A5F4E75FCC0A ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t6DA0A4193816CE464E6B64C19A04A5F4E75FCC0A * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tF9C98B1569ADB35FACDE8F82B3E3007C790EDFB3, ____keyValues_5)); }
	inline List_1_tB78E0FB5C13F06BAADE39A8FD64F6672E3D7FAEF * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tB78E0FB5C13F06BAADE39A8FD64F6672E3D7FAEF ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tB78E0FB5C13F06BAADE39A8FD64F6672E3D7FAEF * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tF9C98B1569ADB35FACDE8F82B3E3007C790EDFB3, ____keys_6)); }
	inline List_1_tB78E0FB5C13F06BAADE39A8FD64F6672E3D7FAEF * get__keys_6() const { return ____keys_6; }
	inline List_1_tB78E0FB5C13F06BAADE39A8FD64F6672E3D7FAEF ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tB78E0FB5C13F06BAADE39A8FD64F6672E3D7FAEF * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tF9C98B1569ADB35FACDE8F82B3E3007C790EDFB3, ____values_7)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__values_7() const { return ____values_7; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_tF9C98B1569ADB35FACDE8F82B3E3007C790EDFB3_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t6DA0A4193816CE464E6B64C19A04A5F4E75FCC0A * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_tF9C98B1569ADB35FACDE8F82B3E3007C790EDFB3_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t6DA0A4193816CE464E6B64C19A04A5F4E75FCC0A * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t6DA0A4193816CE464E6B64C19A04A5F4E75FCC0A ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t6DA0A4193816CE464E6B64C19A04A5F4E75FCC0A * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_TF9C98B1569ADB35FACDE8F82B3E3007C790EDFB3_H
#ifndef SERIALIZABLEDICTIONARYBASE_2_T5840D12C5514C4391A61E943573A4F1DB711BE54_H
#define SERIALIZABLEDICTIONARYBASE_2_T5840D12C5514C4391A61E943573A4F1DB711BE54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2<UnityEngine.Vector3,UnityEngine.Quaternion>
struct  SerializableDictionaryBase_2_t5840D12C5514C4391A61E943573A4F1DB711BE54  : public DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_dict
	Dictionary_2_t8ACD821C092AB3D97DE34E70615B13E72AAA2805 * ____dict_3;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keyValues
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ____keyValues_5;
	// System.Collections.Generic.List`1<TKey> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_keys
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ____keys_6;
	// System.Collections.Generic.List`1<TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_values
	List_1_t74314CADF5799483B2C71785AC688DD9618023F4 * ____values_7;

public:
	inline static int32_t get_offset_of__dict_3() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t5840D12C5514C4391A61E943573A4F1DB711BE54, ____dict_3)); }
	inline Dictionary_2_t8ACD821C092AB3D97DE34E70615B13E72AAA2805 * get__dict_3() const { return ____dict_3; }
	inline Dictionary_2_t8ACD821C092AB3D97DE34E70615B13E72AAA2805 ** get_address_of__dict_3() { return &____dict_3; }
	inline void set__dict_3(Dictionary_2_t8ACD821C092AB3D97DE34E70615B13E72AAA2805 * value)
	{
		____dict_3 = value;
		Il2CppCodeGenWriteBarrier((&____dict_3), value);
	}

	inline static int32_t get_offset_of__keyValues_5() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t5840D12C5514C4391A61E943573A4F1DB711BE54, ____keyValues_5)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get__keyValues_5() const { return ____keyValues_5; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of__keyValues_5() { return &____keyValues_5; }
	inline void set__keyValues_5(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		____keyValues_5 = value;
		Il2CppCodeGenWriteBarrier((&____keyValues_5), value);
	}

	inline static int32_t get_offset_of__keys_6() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t5840D12C5514C4391A61E943573A4F1DB711BE54, ____keys_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get__keys_6() const { return ____keys_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of__keys_6() { return &____keys_6; }
	inline void set__keys_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		____keys_6 = value;
		Il2CppCodeGenWriteBarrier((&____keys_6), value);
	}

	inline static int32_t get_offset_of__values_7() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t5840D12C5514C4391A61E943573A4F1DB711BE54, ____values_7)); }
	inline List_1_t74314CADF5799483B2C71785AC688DD9618023F4 * get__values_7() const { return ____values_7; }
	inline List_1_t74314CADF5799483B2C71785AC688DD9618023F4 ** get_address_of__values_7() { return &____values_7; }
	inline void set__values_7(List_1_t74314CADF5799483B2C71785AC688DD9618023F4 * value)
	{
		____values_7 = value;
		Il2CppCodeGenWriteBarrier((&____values_7), value);
	}
};

struct SerializableDictionaryBase_2_t5840D12C5514C4391A61E943573A4F1DB711BE54_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase`2::_staticEmptyDict
	Dictionary_2_t8ACD821C092AB3D97DE34E70615B13E72AAA2805 * ____staticEmptyDict_4;

public:
	inline static int32_t get_offset_of__staticEmptyDict_4() { return static_cast<int32_t>(offsetof(SerializableDictionaryBase_2_t5840D12C5514C4391A61E943573A4F1DB711BE54_StaticFields, ____staticEmptyDict_4)); }
	inline Dictionary_2_t8ACD821C092AB3D97DE34E70615B13E72AAA2805 * get__staticEmptyDict_4() const { return ____staticEmptyDict_4; }
	inline Dictionary_2_t8ACD821C092AB3D97DE34E70615B13E72AAA2805 ** get_address_of__staticEmptyDict_4() { return &____staticEmptyDict_4; }
	inline void set__staticEmptyDict_4(Dictionary_2_t8ACD821C092AB3D97DE34E70615B13E72AAA2805 * value)
	{
		____staticEmptyDict_4 = value;
		Il2CppCodeGenWriteBarrier((&____staticEmptyDict_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEDICTIONARYBASE_2_T5840D12C5514C4391A61E943573A4F1DB711BE54_H
#ifndef JSONARRAY_T1A8231F65BC9EC3C1CD1FEA25DF8A205CF6400CA_H
#define JSONARRAY_T1A8231F65BC9EC3C1CD1FEA25DF8A205CF6400CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.JsonArray
struct  JsonArray_t1A8231F65BC9EC3C1CD1FEA25DF8A205CF6400CA  : public List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAY_T1A8231F65BC9EC3C1CD1FEA25DF8A205CF6400CA_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef SHAREDIALOGMODE_T40A2D9F39025821336E766D7D4FEA16006545BEB_H
#define SHAREDIALOGMODE_T40A2D9F39025821336E766D7D4FEA16006545BEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ShareDialogMode
struct  ShareDialogMode_t40A2D9F39025821336E766D7D4FEA16006545BEB 
{
public:
	// System.Int32 Facebook.Unity.ShareDialogMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShareDialogMode_t40A2D9F39025821336E766D7D4FEA16006545BEB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDIALOGMODE_T40A2D9F39025821336E766D7D4FEA16006545BEB_H
#ifndef JOYSTICKMODE_T4D81E7222D27377A127289ADE6BE889EEBB8E064_H
#define JOYSTICKMODE_T4D81E7222D27377A127289ADE6BE889EEBB8E064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JoystickMode
struct  JoystickMode_t4D81E7222D27377A127289ADE6BE889EEBB8E064 
{
public:
	// System.Int32 JoystickMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JoystickMode_t4D81E7222D27377A127289ADE6BE889EEBB8E064, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICKMODE_T4D81E7222D27377A127289ADE6BE889EEBB8E064_H
#ifndef AC_S_T83967940B4F21697907028815B64B5E0E7893239_H
#define AC_S_T83967940B4F21697907028815B64B5E0E7893239_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_AC_S
struct  AC_S_t83967940B4F21697907028815B64B5E0E7893239  : public SerializableDictionaryBase_2_t41FCA43FB71C4D9AE1D9E7EB346436803AEE2674
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AC_S_T83967940B4F21697907028815B64B5E0E7893239_H
#ifndef ADVANGENERIC_STRING_TC1B14FFB5530AA2D7D67CF242DBBDA106722CDDB_H
#define ADVANGENERIC_STRING_TC1B14FFB5530AA2D7D67CF242DBBDA106722CDDB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_AdvanGeneric_String
struct  AdvanGeneric_String_tC1B14FFB5530AA2D7D67CF242DBBDA106722CDDB  : public SerializableDictionaryBase_2_tACD7F1C652F364EC4D35594A9EFB8D4C6AE8F4E8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADVANGENERIC_STRING_TC1B14FFB5530AA2D7D67CF242DBBDA106722CDDB_H
#ifndef C_INT_T3809E5AEA51F61B739425178D50CB479D2481E0B_H
#define C_INT_T3809E5AEA51F61B739425178D50CB479D2481E0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_C_Int
struct  C_Int_t3809E5AEA51F61B739425178D50CB479D2481E0B  : public SerializableDictionaryBase_2_t6B59A17E43941D182A35D46DE6F79219F3291D6C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // C_INT_T3809E5AEA51F61B739425178D50CB479D2481E0B_H
#ifndef CHILDTEST_TD7B3D6ADADE74A931FE00600949D174BB7E828D4_H
#define CHILDTEST_TD7B3D6ADADE74A931FE00600949D174BB7E828D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_ChildTest
struct  ChildTest_tD7B3D6ADADE74A931FE00600949D174BB7E828D4  : public RuntimeObject
{
public:
	// UnityEngine.Color RotaryHeart.Lib.DataBaseExample_ChildTest::myChildColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___myChildColor_0;
	// System.Boolean RotaryHeart.Lib.DataBaseExample_ChildTest::myChildBool
	bool ___myChildBool_1;
	// UnityEngine.Gradient RotaryHeart.Lib.DataBaseExample_ChildTest::test
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * ___test_2;

public:
	inline static int32_t get_offset_of_myChildColor_0() { return static_cast<int32_t>(offsetof(ChildTest_tD7B3D6ADADE74A931FE00600949D174BB7E828D4, ___myChildColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_myChildColor_0() const { return ___myChildColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_myChildColor_0() { return &___myChildColor_0; }
	inline void set_myChildColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___myChildColor_0 = value;
	}

	inline static int32_t get_offset_of_myChildBool_1() { return static_cast<int32_t>(offsetof(ChildTest_tD7B3D6ADADE74A931FE00600949D174BB7E828D4, ___myChildBool_1)); }
	inline bool get_myChildBool_1() const { return ___myChildBool_1; }
	inline bool* get_address_of_myChildBool_1() { return &___myChildBool_1; }
	inline void set_myChildBool_1(bool value)
	{
		___myChildBool_1 = value;
	}

	inline static int32_t get_offset_of_test_2() { return static_cast<int32_t>(offsetof(ChildTest_tD7B3D6ADADE74A931FE00600949D174BB7E828D4, ___test_2)); }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * get_test_2() const { return ___test_2; }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A ** get_address_of_test_2() { return &___test_2; }
	inline void set_test_2(Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * value)
	{
		___test_2 = value;
		Il2CppCodeGenWriteBarrier((&___test_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHILDTEST_TD7B3D6ADADE74A931FE00600949D174BB7E828D4_H
#ifndef CLASSTEST_TF93526EE6BB3FCA050DBE3883332CB00F8581DB4_H
#define CLASSTEST_TF93526EE6BB3FCA050DBE3883332CB00F8581DB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_ClassTest
struct  ClassTest_tF93526EE6BB3FCA050DBE3883332CB00F8581DB4  : public RuntimeObject
{
public:
	// System.String RotaryHeart.Lib.DataBaseExample_ClassTest::id
	String_t* ___id_0;
	// System.Single RotaryHeart.Lib.DataBaseExample_ClassTest::test
	float ___test_1;
	// System.String RotaryHeart.Lib.DataBaseExample_ClassTest::test2
	String_t* ___test2_2;
	// UnityEngine.Quaternion RotaryHeart.Lib.DataBaseExample_ClassTest::quat
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___quat_3;
	// RotaryHeart.Lib.DataBaseExample_ChildTest[] RotaryHeart.Lib.DataBaseExample_ClassTest::childTest
	ChildTestU5BU5D_tAADA55A3E4B19CDA5403BD0B45E2388E846F1154* ___childTest_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(ClassTest_tF93526EE6BB3FCA050DBE3883332CB00F8581DB4, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_test_1() { return static_cast<int32_t>(offsetof(ClassTest_tF93526EE6BB3FCA050DBE3883332CB00F8581DB4, ___test_1)); }
	inline float get_test_1() const { return ___test_1; }
	inline float* get_address_of_test_1() { return &___test_1; }
	inline void set_test_1(float value)
	{
		___test_1 = value;
	}

	inline static int32_t get_offset_of_test2_2() { return static_cast<int32_t>(offsetof(ClassTest_tF93526EE6BB3FCA050DBE3883332CB00F8581DB4, ___test2_2)); }
	inline String_t* get_test2_2() const { return ___test2_2; }
	inline String_t** get_address_of_test2_2() { return &___test2_2; }
	inline void set_test2_2(String_t* value)
	{
		___test2_2 = value;
		Il2CppCodeGenWriteBarrier((&___test2_2), value);
	}

	inline static int32_t get_offset_of_quat_3() { return static_cast<int32_t>(offsetof(ClassTest_tF93526EE6BB3FCA050DBE3883332CB00F8581DB4, ___quat_3)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_quat_3() const { return ___quat_3; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_quat_3() { return &___quat_3; }
	inline void set_quat_3(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___quat_3 = value;
	}

	inline static int32_t get_offset_of_childTest_4() { return static_cast<int32_t>(offsetof(ClassTest_tF93526EE6BB3FCA050DBE3883332CB00F8581DB4, ___childTest_4)); }
	inline ChildTestU5BU5D_tAADA55A3E4B19CDA5403BD0B45E2388E846F1154* get_childTest_4() const { return ___childTest_4; }
	inline ChildTestU5BU5D_tAADA55A3E4B19CDA5403BD0B45E2388E846F1154** get_address_of_childTest_4() { return &___childTest_4; }
	inline void set_childTest_4(ChildTestU5BU5D_tAADA55A3E4B19CDA5403BD0B45E2388E846F1154* value)
	{
		___childTest_4 = value;
		Il2CppCodeGenWriteBarrier((&___childTest_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLASSTEST_TF93526EE6BB3FCA050DBE3883332CB00F8581DB4_H
#ifndef ENUMEXAMPLE_TCA85F729B8DD845505686B5CDC90D349F6B3CB93_H
#define ENUMEXAMPLE_TCA85F729B8DD845505686B5CDC90D349F6B3CB93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_EnumExample
struct  EnumExample_tCA85F729B8DD845505686B5CDC90D349F6B3CB93 
{
public:
	// System.Int32 RotaryHeart.Lib.DataBaseExample_EnumExample::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EnumExample_tCA85F729B8DD845505686B5CDC90D349F6B3CB93, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMEXAMPLE_TCA85F729B8DD845505686B5CDC90D349F6B3CB93_H
#ifndef ENUM_STRING_T567A643096104B5698404A0754A1F42C7622AFBE_H
#define ENUM_STRING_T567A643096104B5698404A0754A1F42C7622AFBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_Enum_String
struct  Enum_String_t567A643096104B5698404A0754A1F42C7622AFBE  : public SerializableDictionaryBase_2_t7F050F7FC8A742CEDFDCE14AB3BDB733ACF132CA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUM_STRING_T567A643096104B5698404A0754A1F42C7622AFBE_H
#ifndef GO_I_T9747DD551B9783018C8A9BCFBB45B0E0E739B958_H
#define GO_I_T9747DD551B9783018C8A9BCFBB45B0E0E739B958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_GO_I
struct  GO_I_t9747DD551B9783018C8A9BCFBB45B0E0E739B958  : public SerializableDictionaryBase_2_tBB60A10022065EC629D16670D449E96E4EA22BF5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GO_I_T9747DD551B9783018C8A9BCFBB45B0E0E739B958_H
#ifndef GO_S_T82033CC353B905E23DF7E43F5B8E1FC929ADBC54_H
#define GO_S_T82033CC353B905E23DF7E43F5B8E1FC929ADBC54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_GO_S
struct  GO_S_t82033CC353B905E23DF7E43F5B8E1FC929ADBC54  : public SerializableDictionaryBase_2_t1E983AB88E547EC20ACFC9B841B9ECB2FB82344F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GO_S_T82033CC353B905E23DF7E43F5B8E1FC929ADBC54_H
#ifndef G_INT_T83ADF384081ED34E8132235BB2520189A5626291_H
#define G_INT_T83ADF384081ED34E8132235BB2520189A5626291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_G_Int
struct  G_Int_t83ADF384081ED34E8132235BB2520189A5626291  : public SerializableDictionaryBase_2_t460943D8C8C8BBED979A1BCCCC83201E87046397
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // G_INT_T83ADF384081ED34E8132235BB2520189A5626291_H
#ifndef GENERIC_GENERIC_TC1C606DD304D8B4975F7100A94883D309C89A3C4_H
#define GENERIC_GENERIC_TC1C606DD304D8B4975F7100A94883D309C89A3C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_Generic_Generic
struct  Generic_Generic_tC1C606DD304D8B4975F7100A94883D309C89A3C4  : public SerializableDictionaryBase_2_tE87416068BD8CE69FA4EA85E90946D6686555057
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERIC_GENERIC_TC1C606DD304D8B4975F7100A94883D309C89A3C4_H
#ifndef GENERIC_STRING_TCF72708E4A84958EC2F36570DE29BAD94F59EE49_H
#define GENERIC_STRING_TCF72708E4A84958EC2F36570DE29BAD94F59EE49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_Generic_String
struct  Generic_String_tCF72708E4A84958EC2F36570DE29BAD94F59EE49  : public SerializableDictionaryBase_2_t5F4B41346E67EFDB471821FB3E9612259FCBF971
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERIC_STRING_TCF72708E4A84958EC2F36570DE29BAD94F59EE49_H
#ifndef I_GO_TE66786805611B9866539EC70755473A6AD7D961C_H
#define I_GO_TE66786805611B9866539EC70755473A6AD7D961C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_I_GO
struct  I_GO_tE66786805611B9866539EC70755473A6AD7D961C  : public SerializableDictionaryBase_2_tA9B1FA789C08F26EAFD2F2A241ED8BD42965BE3A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // I_GO_TE66786805611B9866539EC70755473A6AD7D961C_H
#ifndef I_GENERICDICTIONARY_T16BC9886BA6F437767D8D7C0E3A17A82788BF0D6_H
#define I_GENERICDICTIONARY_T16BC9886BA6F437767D8D7C0E3A17A82788BF0D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_I_GenericDictionary
struct  I_GenericDictionary_t16BC9886BA6F437767D8D7C0E3A17A82788BF0D6  : public SerializableDictionaryBase_2_tB1D4518FD301A9EBD647F1C40DD8C8A7EAE0A244
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // I_GENERICDICTIONARY_T16BC9886BA6F437767D8D7C0E3A17A82788BF0D6_H
#ifndef INT_INTARRAY_T09AD7421347872BAAED8837E37A5018B662D23FD_H
#define INT_INTARRAY_T09AD7421347872BAAED8837E37A5018B662D23FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_Int_IntArray
struct  Int_IntArray_t09AD7421347872BAAED8837E37A5018B662D23FD  : public SerializableDictionaryBase_2_tFFDF0C38CB7AF78D1EBBFD7A390DBA9A8C3FF625
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT_INTARRAY_T09AD7421347872BAAED8837E37A5018B662D23FD_H
#ifndef MAT_S_TB6FD4BBFE1F09B0858D9CBFE77379B544084092A_H
#define MAT_S_TB6FD4BBFE1F09B0858D9CBFE77379B544084092A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_Mat_S
struct  Mat_S_tB6FD4BBFE1F09B0858D9CBFE77379B544084092A  : public SerializableDictionaryBase_2_t1988DAAFA463263A7102770DC3F95D3A649CA411
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAT_S_TB6FD4BBFE1F09B0858D9CBFE77379B544084092A_H
#ifndef Q_V3_TF2917CB976081AF5F996E700E7E8206C99F739DE_H
#define Q_V3_TF2917CB976081AF5F996E700E7E8206C99F739DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_Q_V3
struct  Q_V3_tF2917CB976081AF5F996E700E7E8206C99F739DE  : public SerializableDictionaryBase_2_tE267F63ABBFAA44DB1D240576C39BB62417AAA66
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // Q_V3_TF2917CB976081AF5F996E700E7E8206C99F739DE_H
#ifndef S_AC_TC55B8F185DC515189810B1B8D114C456697FD3B5_H
#define S_AC_TC55B8F185DC515189810B1B8D114C456697FD3B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_S_AC
struct  S_AC_tC55B8F185DC515189810B1B8D114C456697FD3B5  : public SerializableDictionaryBase_2_tEC2815A67CA7F1AF4F2E421A960BBB02B07C158A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S_AC_TC55B8F185DC515189810B1B8D114C456697FD3B5_H
#ifndef S_GO_TC66061788F2FA9D3CDAE398F0CAC5E81BD4C6369_H
#define S_GO_TC66061788F2FA9D3CDAE398F0CAC5E81BD4C6369_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_S_GO
struct  S_GO_tC66061788F2FA9D3CDAE398F0CAC5E81BD4C6369  : public SerializableDictionaryBase_2_t62F104185D4C98554D457168BDA5520AD50ECC54
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S_GO_TC66061788F2FA9D3CDAE398F0CAC5E81BD4C6369_H
#ifndef S_GENERICDICTIONARY_T13EDCAD9036EB9CEA44B16E4AF15DF661432493C_H
#define S_GENERICDICTIONARY_T13EDCAD9036EB9CEA44B16E4AF15DF661432493C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_S_GenericDictionary
struct  S_GenericDictionary_t13EDCAD9036EB9CEA44B16E4AF15DF661432493C  : public SerializableDictionaryBase_2_t44F515EFA995D9ACB8AA4FE67BF16854EC1C23FC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S_GENERICDICTIONARY_T13EDCAD9036EB9CEA44B16E4AF15DF661432493C_H
#ifndef S_MAT_TD5CCE3FD00251BA71449066AEE6E59387977C668_H
#define S_MAT_TD5CCE3FD00251BA71449066AEE6E59387977C668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_S_Mat
struct  S_Mat_tD5CCE3FD00251BA71449066AEE6E59387977C668  : public SerializableDictionaryBase_2_t1DDDEEFB251EF3CB9ADD494275EA4A0147F559C8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S_MAT_TD5CCE3FD00251BA71449066AEE6E59387977C668_H
#ifndef S_SPRITE_TB14BA0C17CD89E7DDE240DA38384033787A9E422_H
#define S_SPRITE_TB14BA0C17CD89E7DDE240DA38384033787A9E422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_S_Sprite
struct  S_Sprite_tB14BA0C17CD89E7DDE240DA38384033787A9E422  : public SerializableDictionaryBase_2_t483E5ABCAACE81029253C43ABA3B959C7475B01E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // S_SPRITE_TB14BA0C17CD89E7DDE240DA38384033787A9E422_H
#ifndef V3_Q_T334647532EC7E76E279772A7E1454D957F7E6FFD_H
#define V3_Q_T334647532EC7E76E279772A7E1454D957F7E6FFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample_V3_Q
struct  V3_Q_t334647532EC7E76E279772A7E1454D957F7E6FFD  : public SerializableDictionaryBase_2_t5840D12C5514C4391A61E943573A4F1DB711BE54
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // V3_Q_T334647532EC7E76E279772A7E1454D957F7E6FFD_H
#ifndef MAINDICT_TABB8831EB5CF88193E3BF7F318A84053BFE42682_H
#define MAINDICT_TABB8831EB5CF88193E3BF7F318A84053BFE42682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.MainDict
struct  MainDict_tABB8831EB5CF88193E3BF7F318A84053BFE42682  : public SerializableDictionaryBase_2_tEFCBF7B1E04CC17B2D53A587584601A8947A894A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINDICT_TABB8831EB5CF88193E3BF7F318A84053BFE42682_H
#ifndef NESTED2DICT_T92B279CB96D0DFAEBA143A7267846B5A04933628_H
#define NESTED2DICT_T92B279CB96D0DFAEBA143A7267846B5A04933628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.Nested2Dict
struct  Nested2Dict_t92B279CB96D0DFAEBA143A7267846B5A04933628  : public SerializableDictionaryBase_2_tF9C98B1569ADB35FACDE8F82B3E3007C790EDFB3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NESTED2DICT_T92B279CB96D0DFAEBA143A7267846B5A04933628_H
#ifndef NESTEDDICT_T786E4BDEE0E5CB8B389F8248722E4854CFABCD18_H
#define NESTEDDICT_T786E4BDEE0E5CB8B389F8248722E4854CFABCD18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.NestedDict
struct  NestedDict_t786E4BDEE0E5CB8B389F8248722E4854CFABCD18  : public SerializableDictionaryBase_2_tAE61515978EBDB44DC99991B7A454224317B7C10
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NESTEDDICT_T786E4BDEE0E5CB8B389F8248722E4854CFABCD18_H
#ifndef NESTEDEXAMPLE_T7D65EF9A705ECA9B6097ED1276DBB49E72A4F3C5_H
#define NESTEDEXAMPLE_T7D65EF9A705ECA9B6097ED1276DBB49E72A4F3C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.NestedExample
struct  NestedExample_t7D65EF9A705ECA9B6097ED1276DBB49E72A4F3C5  : public RuntimeObject
{
public:
	// UnityEngine.GameObject RotaryHeart.Lib.NestedExample::prefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___prefab_0;
	// System.Single RotaryHeart.Lib.NestedExample::speed
	float ___speed_1;
	// UnityEngine.Color RotaryHeart.Lib.NestedExample::color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___color_2;
	// RotaryHeart.Lib.Nested2Dict RotaryHeart.Lib.NestedExample::deepNested
	Nested2Dict_t92B279CB96D0DFAEBA143A7267846B5A04933628 * ___deepNested_3;

public:
	inline static int32_t get_offset_of_prefab_0() { return static_cast<int32_t>(offsetof(NestedExample_t7D65EF9A705ECA9B6097ED1276DBB49E72A4F3C5, ___prefab_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_prefab_0() const { return ___prefab_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_prefab_0() { return &___prefab_0; }
	inline void set_prefab_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___prefab_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefab_0), value);
	}

	inline static int32_t get_offset_of_speed_1() { return static_cast<int32_t>(offsetof(NestedExample_t7D65EF9A705ECA9B6097ED1276DBB49E72A4F3C5, ___speed_1)); }
	inline float get_speed_1() const { return ___speed_1; }
	inline float* get_address_of_speed_1() { return &___speed_1; }
	inline void set_speed_1(float value)
	{
		___speed_1 = value;
	}

	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(NestedExample_t7D65EF9A705ECA9B6097ED1276DBB49E72A4F3C5, ___color_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_color_2() const { return ___color_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___color_2 = value;
	}

	inline static int32_t get_offset_of_deepNested_3() { return static_cast<int32_t>(offsetof(NestedExample_t7D65EF9A705ECA9B6097ED1276DBB49E72A4F3C5, ___deepNested_3)); }
	inline Nested2Dict_t92B279CB96D0DFAEBA143A7267846B5A04933628 * get_deepNested_3() const { return ___deepNested_3; }
	inline Nested2Dict_t92B279CB96D0DFAEBA143A7267846B5A04933628 ** get_address_of_deepNested_3() { return &___deepNested_3; }
	inline void set_deepNested_3(Nested2Dict_t92B279CB96D0DFAEBA143A7267846B5A04933628 * value)
	{
		___deepNested_3 = value;
		Il2CppCodeGenWriteBarrier((&___deepNested_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NESTEDEXAMPLE_T7D65EF9A705ECA9B6097ED1276DBB49E72A4F3C5_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef QUERYTRIGGERINTERACTION_T56ABDF7B81C5D989BE40455CF39647A532791739_H
#define QUERYTRIGGERINTERACTION_T56ABDF7B81C5D989BE40455CF39647A532791739_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.QueryTriggerInteraction
struct  QueryTriggerInteraction_t56ABDF7B81C5D989BE40455CF39647A532791739 
{
public:
	// System.Int32 UnityEngine.QueryTriggerInteraction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(QueryTriggerInteraction_t56ABDF7B81C5D989BE40455CF39647A532791739, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYTRIGGERINTERACTION_T56ABDF7B81C5D989BE40455CF39647A532791739_H
#ifndef MOBILEFACEBOOK_T27A7B4F062FC7F3935525F591E59A411462EF919_H
#define MOBILEFACEBOOK_T27A7B4F062FC7F3935525F591E59A411462EF919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.MobileFacebook
struct  MobileFacebook_t27A7B4F062FC7F3935525F591E59A411462EF919  : public FacebookBase_t89F93EF12353405D2CEFAC7B59E860123FB472C7
{
public:
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Mobile.MobileFacebook::shareDialogMode
	int32_t ___shareDialogMode_3;

public:
	inline static int32_t get_offset_of_shareDialogMode_3() { return static_cast<int32_t>(offsetof(MobileFacebook_t27A7B4F062FC7F3935525F591E59A411462EF919, ___shareDialogMode_3)); }
	inline int32_t get_shareDialogMode_3() const { return ___shareDialogMode_3; }
	inline int32_t* get_address_of_shareDialogMode_3() { return &___shareDialogMode_3; }
	inline void set_shareDialogMode_3(int32_t value)
	{
		___shareDialogMode_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEFACEBOOK_T27A7B4F062FC7F3935525F591E59A411462EF919_H
#ifndef EXAMPLE_TE7A50FB4A9329B98CCC31246B2D66BC1D341AC84_H
#define EXAMPLE_TE7A50FB4A9329B98CCC31246B2D66BC1D341AC84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.Example
struct  Example_tE7A50FB4A9329B98CCC31246B2D66BC1D341AC84  : public RuntimeObject
{
public:
	// System.String RotaryHeart.Lib.Example::id
	String_t* ___id_0;
	// UnityEngine.QueryTriggerInteraction RotaryHeart.Lib.Example::enumVal
	int32_t ___enumVal_1;
	// RotaryHeart.Lib.NestedDict RotaryHeart.Lib.Example::nestedData
	NestedDict_t786E4BDEE0E5CB8B389F8248722E4854CFABCD18 * ___nestedData_2;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Example_tE7A50FB4A9329B98CCC31246B2D66BC1D341AC84, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_enumVal_1() { return static_cast<int32_t>(offsetof(Example_tE7A50FB4A9329B98CCC31246B2D66BC1D341AC84, ___enumVal_1)); }
	inline int32_t get_enumVal_1() const { return ___enumVal_1; }
	inline int32_t* get_address_of_enumVal_1() { return &___enumVal_1; }
	inline void set_enumVal_1(int32_t value)
	{
		___enumVal_1 = value;
	}

	inline static int32_t get_offset_of_nestedData_2() { return static_cast<int32_t>(offsetof(Example_tE7A50FB4A9329B98CCC31246B2D66BC1D341AC84, ___nestedData_2)); }
	inline NestedDict_t786E4BDEE0E5CB8B389F8248722E4854CFABCD18 * get_nestedData_2() const { return ___nestedData_2; }
	inline NestedDict_t786E4BDEE0E5CB8B389F8248722E4854CFABCD18 ** get_address_of_nestedData_2() { return &___nestedData_2; }
	inline void set_nestedData_2(NestedDict_t786E4BDEE0E5CB8B389F8248722E4854CFABCD18 * value)
	{
		___nestedData_2 = value;
		Il2CppCodeGenWriteBarrier((&___nestedData_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE_TE7A50FB4A9329B98CCC31246B2D66BC1D341AC84_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef ANDROIDFACEBOOK_T392A5DA388638E742F4C61E38AEA820B2C2C08EA_H
#define ANDROIDFACEBOOK_T392A5DA388638E742F4C61E38AEA820B2C2C08EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.Android.AndroidFacebook
struct  AndroidFacebook_t392A5DA388638E742F4C61E38AEA820B2C2C08EA  : public MobileFacebook_t27A7B4F062FC7F3935525F591E59A411462EF919
{
public:
	// System.Boolean Facebook.Unity.Mobile.Android.AndroidFacebook::limitEventUsage
	bool ___limitEventUsage_4;
	// Facebook.Unity.Mobile.Android.IAndroidWrapper Facebook.Unity.Mobile.Android.AndroidFacebook::androidWrapper
	RuntimeObject* ___androidWrapper_5;
	// System.String Facebook.Unity.Mobile.Android.AndroidFacebook::<KeyHash>k__BackingField
	String_t* ___U3CKeyHashU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_limitEventUsage_4() { return static_cast<int32_t>(offsetof(AndroidFacebook_t392A5DA388638E742F4C61E38AEA820B2C2C08EA, ___limitEventUsage_4)); }
	inline bool get_limitEventUsage_4() const { return ___limitEventUsage_4; }
	inline bool* get_address_of_limitEventUsage_4() { return &___limitEventUsage_4; }
	inline void set_limitEventUsage_4(bool value)
	{
		___limitEventUsage_4 = value;
	}

	inline static int32_t get_offset_of_androidWrapper_5() { return static_cast<int32_t>(offsetof(AndroidFacebook_t392A5DA388638E742F4C61E38AEA820B2C2C08EA, ___androidWrapper_5)); }
	inline RuntimeObject* get_androidWrapper_5() const { return ___androidWrapper_5; }
	inline RuntimeObject** get_address_of_androidWrapper_5() { return &___androidWrapper_5; }
	inline void set_androidWrapper_5(RuntimeObject* value)
	{
		___androidWrapper_5 = value;
		Il2CppCodeGenWriteBarrier((&___androidWrapper_5), value);
	}

	inline static int32_t get_offset_of_U3CKeyHashU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AndroidFacebook_t392A5DA388638E742F4C61E38AEA820B2C2C08EA, ___U3CKeyHashU3Ek__BackingField_6)); }
	inline String_t* get_U3CKeyHashU3Ek__BackingField_6() const { return ___U3CKeyHashU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CKeyHashU3Ek__BackingField_6() { return &___U3CKeyHashU3Ek__BackingField_6; }
	inline void set_U3CKeyHashU3Ek__BackingField_6(String_t* value)
	{
		___U3CKeyHashU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyHashU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDFACEBOOK_T392A5DA388638E742F4C61E38AEA820B2C2C08EA_H
#ifndef IOSFACEBOOK_T89B3709B0F51CA70FC1D298F8949D95B38CB7BE6_H
#define IOSFACEBOOK_T89B3709B0F51CA70FC1D298F8949D95B38CB7BE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.IOS.IOSFacebook
struct  IOSFacebook_t89B3709B0F51CA70FC1D298F8949D95B38CB7BE6  : public MobileFacebook_t27A7B4F062FC7F3935525F591E59A411462EF919
{
public:
	// System.Boolean Facebook.Unity.Mobile.IOS.IOSFacebook::limitEventUsage
	bool ___limitEventUsage_4;
	// Facebook.Unity.Mobile.IOS.IIOSWrapper Facebook.Unity.Mobile.IOS.IOSFacebook::iosWrapper
	RuntimeObject* ___iosWrapper_5;

public:
	inline static int32_t get_offset_of_limitEventUsage_4() { return static_cast<int32_t>(offsetof(IOSFacebook_t89B3709B0F51CA70FC1D298F8949D95B38CB7BE6, ___limitEventUsage_4)); }
	inline bool get_limitEventUsage_4() const { return ___limitEventUsage_4; }
	inline bool* get_address_of_limitEventUsage_4() { return &___limitEventUsage_4; }
	inline void set_limitEventUsage_4(bool value)
	{
		___limitEventUsage_4 = value;
	}

	inline static int32_t get_offset_of_iosWrapper_5() { return static_cast<int32_t>(offsetof(IOSFacebook_t89B3709B0F51CA70FC1D298F8949D95B38CB7BE6, ___iosWrapper_5)); }
	inline RuntimeObject* get_iosWrapper_5() const { return ___iosWrapper_5; }
	inline RuntimeObject** get_address_of_iosWrapper_5() { return &___iosWrapper_5; }
	inline void set_iosWrapper_5(RuntimeObject* value)
	{
		___iosWrapper_5 = value;
		Il2CppCodeGenWriteBarrier((&___iosWrapper_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSFACEBOOK_T89B3709B0F51CA70FC1D298F8949D95B38CB7BE6_H
#ifndef GAMESPARKSSETTINGS_T0B74BF11E5E1BB82286302735CFDFF291FE41FC4_H
#define GAMESPARKSSETTINGS_T0B74BF11E5E1BB82286302735CFDFF291FE41FC4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparksSettings
struct  GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.String GameSparksSettings::sdkVersion
	String_t* ___sdkVersion_10;
	// System.String GameSparksSettings::apiKey
	String_t* ___apiKey_11;
	// System.String GameSparksSettings::credential
	String_t* ___credential_12;
	// System.String GameSparksSettings::apiSecret
	String_t* ___apiSecret_13;
	// System.Boolean GameSparksSettings::previewBuild
	bool ___previewBuild_14;
	// System.Boolean GameSparksSettings::debugBuild
	bool ___debugBuild_15;

public:
	inline static int32_t get_offset_of_sdkVersion_10() { return static_cast<int32_t>(offsetof(GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4, ___sdkVersion_10)); }
	inline String_t* get_sdkVersion_10() const { return ___sdkVersion_10; }
	inline String_t** get_address_of_sdkVersion_10() { return &___sdkVersion_10; }
	inline void set_sdkVersion_10(String_t* value)
	{
		___sdkVersion_10 = value;
		Il2CppCodeGenWriteBarrier((&___sdkVersion_10), value);
	}

	inline static int32_t get_offset_of_apiKey_11() { return static_cast<int32_t>(offsetof(GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4, ___apiKey_11)); }
	inline String_t* get_apiKey_11() const { return ___apiKey_11; }
	inline String_t** get_address_of_apiKey_11() { return &___apiKey_11; }
	inline void set_apiKey_11(String_t* value)
	{
		___apiKey_11 = value;
		Il2CppCodeGenWriteBarrier((&___apiKey_11), value);
	}

	inline static int32_t get_offset_of_credential_12() { return static_cast<int32_t>(offsetof(GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4, ___credential_12)); }
	inline String_t* get_credential_12() const { return ___credential_12; }
	inline String_t** get_address_of_credential_12() { return &___credential_12; }
	inline void set_credential_12(String_t* value)
	{
		___credential_12 = value;
		Il2CppCodeGenWriteBarrier((&___credential_12), value);
	}

	inline static int32_t get_offset_of_apiSecret_13() { return static_cast<int32_t>(offsetof(GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4, ___apiSecret_13)); }
	inline String_t* get_apiSecret_13() const { return ___apiSecret_13; }
	inline String_t** get_address_of_apiSecret_13() { return &___apiSecret_13; }
	inline void set_apiSecret_13(String_t* value)
	{
		___apiSecret_13 = value;
		Il2CppCodeGenWriteBarrier((&___apiSecret_13), value);
	}

	inline static int32_t get_offset_of_previewBuild_14() { return static_cast<int32_t>(offsetof(GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4, ___previewBuild_14)); }
	inline bool get_previewBuild_14() const { return ___previewBuild_14; }
	inline bool* get_address_of_previewBuild_14() { return &___previewBuild_14; }
	inline void set_previewBuild_14(bool value)
	{
		___previewBuild_14 = value;
	}

	inline static int32_t get_offset_of_debugBuild_15() { return static_cast<int32_t>(offsetof(GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4, ___debugBuild_15)); }
	inline bool get_debugBuild_15() const { return ___debugBuild_15; }
	inline bool* get_address_of_debugBuild_15() { return &___debugBuild_15; }
	inline void set_debugBuild_15(bool value)
	{
		___debugBuild_15 = value;
	}
};

struct GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4_StaticFields
{
public:
	// System.String GameSparksSettings::liveServiceUrlBase
	String_t* ___liveServiceUrlBase_7;
	// System.String GameSparksSettings::previewServiceUrlBase
	String_t* ___previewServiceUrlBase_8;
	// GameSparksSettings GameSparksSettings::instance
	GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4 * ___instance_9;

public:
	inline static int32_t get_offset_of_liveServiceUrlBase_7() { return static_cast<int32_t>(offsetof(GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4_StaticFields, ___liveServiceUrlBase_7)); }
	inline String_t* get_liveServiceUrlBase_7() const { return ___liveServiceUrlBase_7; }
	inline String_t** get_address_of_liveServiceUrlBase_7() { return &___liveServiceUrlBase_7; }
	inline void set_liveServiceUrlBase_7(String_t* value)
	{
		___liveServiceUrlBase_7 = value;
		Il2CppCodeGenWriteBarrier((&___liveServiceUrlBase_7), value);
	}

	inline static int32_t get_offset_of_previewServiceUrlBase_8() { return static_cast<int32_t>(offsetof(GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4_StaticFields, ___previewServiceUrlBase_8)); }
	inline String_t* get_previewServiceUrlBase_8() const { return ___previewServiceUrlBase_8; }
	inline String_t** get_address_of_previewServiceUrlBase_8() { return &___previewServiceUrlBase_8; }
	inline void set_previewServiceUrlBase_8(String_t* value)
	{
		___previewServiceUrlBase_8 = value;
		Il2CppCodeGenWriteBarrier((&___previewServiceUrlBase_8), value);
	}

	inline static int32_t get_offset_of_instance_9() { return static_cast<int32_t>(offsetof(GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4_StaticFields, ___instance_9)); }
	inline GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4 * get_instance_9() const { return ___instance_9; }
	inline GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4 ** get_address_of_instance_9() { return &___instance_9; }
	inline void set_instance_9(GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4 * value)
	{
		___instance_9 = value;
		Il2CppCodeGenWriteBarrier((&___instance_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESPARKSSETTINGS_T0B74BF11E5E1BB82286302735CFDFF291FE41FC4_H
#ifndef DATABASEEXAMPLE_T21198F7A6479F09A718FA7CE5677AA43CD4AD272_H
#define DATABASEEXAMPLE_T21198F7A6479F09A718FA7CE5677AA43CD4AD272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.DataBaseExample
struct  DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// RotaryHeart.Lib.DataBaseExample_Generic_String RotaryHeart.Lib.DataBaseExample::_genericString
	Generic_String_tCF72708E4A84958EC2F36570DE29BAD94F59EE49 * ____genericString_4;
	// RotaryHeart.Lib.DataBaseExample_Generic_Generic RotaryHeart.Lib.DataBaseExample::_genericGeneric
	Generic_Generic_tC1C606DD304D8B4975F7100A94883D309C89A3C4 * ____genericGeneric_5;
	// RotaryHeart.Lib.DataBaseExample_S_GenericDictionary RotaryHeart.Lib.DataBaseExample::_stringGeneric
	S_GenericDictionary_t13EDCAD9036EB9CEA44B16E4AF15DF661432493C * ____stringGeneric_6;
	// RotaryHeart.Lib.DataBaseExample_I_GenericDictionary RotaryHeart.Lib.DataBaseExample::_intGeneric
	I_GenericDictionary_t16BC9886BA6F437767D8D7C0E3A17A82788BF0D6 * ____intGeneric_7;
	// RotaryHeart.Lib.DataBaseExample_I_GO RotaryHeart.Lib.DataBaseExample::_intGameobject
	I_GO_tE66786805611B9866539EC70755473A6AD7D961C * ____intGameobject_8;
	// RotaryHeart.Lib.DataBaseExample_GO_I RotaryHeart.Lib.DataBaseExample::_gameobjectInt
	GO_I_t9747DD551B9783018C8A9BCFBB45B0E0E739B958 * ____gameobjectInt_9;
	// RotaryHeart.Lib.DataBaseExample_S_GO RotaryHeart.Lib.DataBaseExample::_stringGameobject
	S_GO_tC66061788F2FA9D3CDAE398F0CAC5E81BD4C6369 * ____stringGameobject_10;
	// RotaryHeart.Lib.DataBaseExample_GO_S RotaryHeart.Lib.DataBaseExample::_gameobjectString
	GO_S_t82033CC353B905E23DF7E43F5B8E1FC929ADBC54 * ____gameobjectString_11;
	// RotaryHeart.Lib.DataBaseExample_S_Mat RotaryHeart.Lib.DataBaseExample::_stringMaterial
	S_Mat_tD5CCE3FD00251BA71449066AEE6E59387977C668 * ____stringMaterial_12;
	// RotaryHeart.Lib.DataBaseExample_Mat_S RotaryHeart.Lib.DataBaseExample::_materialString
	Mat_S_tB6FD4BBFE1F09B0858D9CBFE77379B544084092A * ____materialString_13;
	// RotaryHeart.Lib.DataBaseExample_V3_Q RotaryHeart.Lib.DataBaseExample::_vector3Quaternion
	V3_Q_t334647532EC7E76E279772A7E1454D957F7E6FFD * ____vector3Quaternion_14;
	// RotaryHeart.Lib.DataBaseExample_Q_V3 RotaryHeart.Lib.DataBaseExample::_quaternionVector3
	Q_V3_tF2917CB976081AF5F996E700E7E8206C99F739DE * ____quaternionVector3_15;
	// RotaryHeart.Lib.DataBaseExample_S_AC RotaryHeart.Lib.DataBaseExample::_stringAudioClip
	S_AC_tC55B8F185DC515189810B1B8D114C456697FD3B5 * ____stringAudioClip_16;
	// RotaryHeart.Lib.DataBaseExample_AC_S RotaryHeart.Lib.DataBaseExample::_audioClipString
	AC_S_t83967940B4F21697907028815B64B5E0E7893239 * ____audioClipString_17;
	// RotaryHeart.Lib.DataBaseExample_C_Int RotaryHeart.Lib.DataBaseExample::_charInt
	C_Int_t3809E5AEA51F61B739425178D50CB479D2481E0B * ____charInt_18;
	// RotaryHeart.Lib.DataBaseExample_G_Int RotaryHeart.Lib.DataBaseExample::_gradientInt
	G_Int_t83ADF384081ED34E8132235BB2520189A5626291 * ____gradientInt_19;
	// RotaryHeart.Lib.DataBaseExample_Int_IntArray RotaryHeart.Lib.DataBaseExample::_intArray
	Int_IntArray_t09AD7421347872BAAED8837E37A5018B662D23FD * ____intArray_20;
	// RotaryHeart.Lib.DataBaseExample_Enum_String RotaryHeart.Lib.DataBaseExample::_enumString
	Enum_String_t567A643096104B5698404A0754A1F42C7622AFBE * ____enumString_21;
	// RotaryHeart.Lib.DataBaseExample_AdvanGeneric_String RotaryHeart.Lib.DataBaseExample::_advancedGenericKey
	AdvanGeneric_String_tC1B14FFB5530AA2D7D67CF242DBBDA106722CDDB * ____advancedGenericKey_22;

public:
	inline static int32_t get_offset_of__genericString_4() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____genericString_4)); }
	inline Generic_String_tCF72708E4A84958EC2F36570DE29BAD94F59EE49 * get__genericString_4() const { return ____genericString_4; }
	inline Generic_String_tCF72708E4A84958EC2F36570DE29BAD94F59EE49 ** get_address_of__genericString_4() { return &____genericString_4; }
	inline void set__genericString_4(Generic_String_tCF72708E4A84958EC2F36570DE29BAD94F59EE49 * value)
	{
		____genericString_4 = value;
		Il2CppCodeGenWriteBarrier((&____genericString_4), value);
	}

	inline static int32_t get_offset_of__genericGeneric_5() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____genericGeneric_5)); }
	inline Generic_Generic_tC1C606DD304D8B4975F7100A94883D309C89A3C4 * get__genericGeneric_5() const { return ____genericGeneric_5; }
	inline Generic_Generic_tC1C606DD304D8B4975F7100A94883D309C89A3C4 ** get_address_of__genericGeneric_5() { return &____genericGeneric_5; }
	inline void set__genericGeneric_5(Generic_Generic_tC1C606DD304D8B4975F7100A94883D309C89A3C4 * value)
	{
		____genericGeneric_5 = value;
		Il2CppCodeGenWriteBarrier((&____genericGeneric_5), value);
	}

	inline static int32_t get_offset_of__stringGeneric_6() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____stringGeneric_6)); }
	inline S_GenericDictionary_t13EDCAD9036EB9CEA44B16E4AF15DF661432493C * get__stringGeneric_6() const { return ____stringGeneric_6; }
	inline S_GenericDictionary_t13EDCAD9036EB9CEA44B16E4AF15DF661432493C ** get_address_of__stringGeneric_6() { return &____stringGeneric_6; }
	inline void set__stringGeneric_6(S_GenericDictionary_t13EDCAD9036EB9CEA44B16E4AF15DF661432493C * value)
	{
		____stringGeneric_6 = value;
		Il2CppCodeGenWriteBarrier((&____stringGeneric_6), value);
	}

	inline static int32_t get_offset_of__intGeneric_7() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____intGeneric_7)); }
	inline I_GenericDictionary_t16BC9886BA6F437767D8D7C0E3A17A82788BF0D6 * get__intGeneric_7() const { return ____intGeneric_7; }
	inline I_GenericDictionary_t16BC9886BA6F437767D8D7C0E3A17A82788BF0D6 ** get_address_of__intGeneric_7() { return &____intGeneric_7; }
	inline void set__intGeneric_7(I_GenericDictionary_t16BC9886BA6F437767D8D7C0E3A17A82788BF0D6 * value)
	{
		____intGeneric_7 = value;
		Il2CppCodeGenWriteBarrier((&____intGeneric_7), value);
	}

	inline static int32_t get_offset_of__intGameobject_8() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____intGameobject_8)); }
	inline I_GO_tE66786805611B9866539EC70755473A6AD7D961C * get__intGameobject_8() const { return ____intGameobject_8; }
	inline I_GO_tE66786805611B9866539EC70755473A6AD7D961C ** get_address_of__intGameobject_8() { return &____intGameobject_8; }
	inline void set__intGameobject_8(I_GO_tE66786805611B9866539EC70755473A6AD7D961C * value)
	{
		____intGameobject_8 = value;
		Il2CppCodeGenWriteBarrier((&____intGameobject_8), value);
	}

	inline static int32_t get_offset_of__gameobjectInt_9() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____gameobjectInt_9)); }
	inline GO_I_t9747DD551B9783018C8A9BCFBB45B0E0E739B958 * get__gameobjectInt_9() const { return ____gameobjectInt_9; }
	inline GO_I_t9747DD551B9783018C8A9BCFBB45B0E0E739B958 ** get_address_of__gameobjectInt_9() { return &____gameobjectInt_9; }
	inline void set__gameobjectInt_9(GO_I_t9747DD551B9783018C8A9BCFBB45B0E0E739B958 * value)
	{
		____gameobjectInt_9 = value;
		Il2CppCodeGenWriteBarrier((&____gameobjectInt_9), value);
	}

	inline static int32_t get_offset_of__stringGameobject_10() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____stringGameobject_10)); }
	inline S_GO_tC66061788F2FA9D3CDAE398F0CAC5E81BD4C6369 * get__stringGameobject_10() const { return ____stringGameobject_10; }
	inline S_GO_tC66061788F2FA9D3CDAE398F0CAC5E81BD4C6369 ** get_address_of__stringGameobject_10() { return &____stringGameobject_10; }
	inline void set__stringGameobject_10(S_GO_tC66061788F2FA9D3CDAE398F0CAC5E81BD4C6369 * value)
	{
		____stringGameobject_10 = value;
		Il2CppCodeGenWriteBarrier((&____stringGameobject_10), value);
	}

	inline static int32_t get_offset_of__gameobjectString_11() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____gameobjectString_11)); }
	inline GO_S_t82033CC353B905E23DF7E43F5B8E1FC929ADBC54 * get__gameobjectString_11() const { return ____gameobjectString_11; }
	inline GO_S_t82033CC353B905E23DF7E43F5B8E1FC929ADBC54 ** get_address_of__gameobjectString_11() { return &____gameobjectString_11; }
	inline void set__gameobjectString_11(GO_S_t82033CC353B905E23DF7E43F5B8E1FC929ADBC54 * value)
	{
		____gameobjectString_11 = value;
		Il2CppCodeGenWriteBarrier((&____gameobjectString_11), value);
	}

	inline static int32_t get_offset_of__stringMaterial_12() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____stringMaterial_12)); }
	inline S_Mat_tD5CCE3FD00251BA71449066AEE6E59387977C668 * get__stringMaterial_12() const { return ____stringMaterial_12; }
	inline S_Mat_tD5CCE3FD00251BA71449066AEE6E59387977C668 ** get_address_of__stringMaterial_12() { return &____stringMaterial_12; }
	inline void set__stringMaterial_12(S_Mat_tD5CCE3FD00251BA71449066AEE6E59387977C668 * value)
	{
		____stringMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&____stringMaterial_12), value);
	}

	inline static int32_t get_offset_of__materialString_13() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____materialString_13)); }
	inline Mat_S_tB6FD4BBFE1F09B0858D9CBFE77379B544084092A * get__materialString_13() const { return ____materialString_13; }
	inline Mat_S_tB6FD4BBFE1F09B0858D9CBFE77379B544084092A ** get_address_of__materialString_13() { return &____materialString_13; }
	inline void set__materialString_13(Mat_S_tB6FD4BBFE1F09B0858D9CBFE77379B544084092A * value)
	{
		____materialString_13 = value;
		Il2CppCodeGenWriteBarrier((&____materialString_13), value);
	}

	inline static int32_t get_offset_of__vector3Quaternion_14() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____vector3Quaternion_14)); }
	inline V3_Q_t334647532EC7E76E279772A7E1454D957F7E6FFD * get__vector3Quaternion_14() const { return ____vector3Quaternion_14; }
	inline V3_Q_t334647532EC7E76E279772A7E1454D957F7E6FFD ** get_address_of__vector3Quaternion_14() { return &____vector3Quaternion_14; }
	inline void set__vector3Quaternion_14(V3_Q_t334647532EC7E76E279772A7E1454D957F7E6FFD * value)
	{
		____vector3Quaternion_14 = value;
		Il2CppCodeGenWriteBarrier((&____vector3Quaternion_14), value);
	}

	inline static int32_t get_offset_of__quaternionVector3_15() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____quaternionVector3_15)); }
	inline Q_V3_tF2917CB976081AF5F996E700E7E8206C99F739DE * get__quaternionVector3_15() const { return ____quaternionVector3_15; }
	inline Q_V3_tF2917CB976081AF5F996E700E7E8206C99F739DE ** get_address_of__quaternionVector3_15() { return &____quaternionVector3_15; }
	inline void set__quaternionVector3_15(Q_V3_tF2917CB976081AF5F996E700E7E8206C99F739DE * value)
	{
		____quaternionVector3_15 = value;
		Il2CppCodeGenWriteBarrier((&____quaternionVector3_15), value);
	}

	inline static int32_t get_offset_of__stringAudioClip_16() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____stringAudioClip_16)); }
	inline S_AC_tC55B8F185DC515189810B1B8D114C456697FD3B5 * get__stringAudioClip_16() const { return ____stringAudioClip_16; }
	inline S_AC_tC55B8F185DC515189810B1B8D114C456697FD3B5 ** get_address_of__stringAudioClip_16() { return &____stringAudioClip_16; }
	inline void set__stringAudioClip_16(S_AC_tC55B8F185DC515189810B1B8D114C456697FD3B5 * value)
	{
		____stringAudioClip_16 = value;
		Il2CppCodeGenWriteBarrier((&____stringAudioClip_16), value);
	}

	inline static int32_t get_offset_of__audioClipString_17() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____audioClipString_17)); }
	inline AC_S_t83967940B4F21697907028815B64B5E0E7893239 * get__audioClipString_17() const { return ____audioClipString_17; }
	inline AC_S_t83967940B4F21697907028815B64B5E0E7893239 ** get_address_of__audioClipString_17() { return &____audioClipString_17; }
	inline void set__audioClipString_17(AC_S_t83967940B4F21697907028815B64B5E0E7893239 * value)
	{
		____audioClipString_17 = value;
		Il2CppCodeGenWriteBarrier((&____audioClipString_17), value);
	}

	inline static int32_t get_offset_of__charInt_18() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____charInt_18)); }
	inline C_Int_t3809E5AEA51F61B739425178D50CB479D2481E0B * get__charInt_18() const { return ____charInt_18; }
	inline C_Int_t3809E5AEA51F61B739425178D50CB479D2481E0B ** get_address_of__charInt_18() { return &____charInt_18; }
	inline void set__charInt_18(C_Int_t3809E5AEA51F61B739425178D50CB479D2481E0B * value)
	{
		____charInt_18 = value;
		Il2CppCodeGenWriteBarrier((&____charInt_18), value);
	}

	inline static int32_t get_offset_of__gradientInt_19() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____gradientInt_19)); }
	inline G_Int_t83ADF384081ED34E8132235BB2520189A5626291 * get__gradientInt_19() const { return ____gradientInt_19; }
	inline G_Int_t83ADF384081ED34E8132235BB2520189A5626291 ** get_address_of__gradientInt_19() { return &____gradientInt_19; }
	inline void set__gradientInt_19(G_Int_t83ADF384081ED34E8132235BB2520189A5626291 * value)
	{
		____gradientInt_19 = value;
		Il2CppCodeGenWriteBarrier((&____gradientInt_19), value);
	}

	inline static int32_t get_offset_of__intArray_20() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____intArray_20)); }
	inline Int_IntArray_t09AD7421347872BAAED8837E37A5018B662D23FD * get__intArray_20() const { return ____intArray_20; }
	inline Int_IntArray_t09AD7421347872BAAED8837E37A5018B662D23FD ** get_address_of__intArray_20() { return &____intArray_20; }
	inline void set__intArray_20(Int_IntArray_t09AD7421347872BAAED8837E37A5018B662D23FD * value)
	{
		____intArray_20 = value;
		Il2CppCodeGenWriteBarrier((&____intArray_20), value);
	}

	inline static int32_t get_offset_of__enumString_21() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____enumString_21)); }
	inline Enum_String_t567A643096104B5698404A0754A1F42C7622AFBE * get__enumString_21() const { return ____enumString_21; }
	inline Enum_String_t567A643096104B5698404A0754A1F42C7622AFBE ** get_address_of__enumString_21() { return &____enumString_21; }
	inline void set__enumString_21(Enum_String_t567A643096104B5698404A0754A1F42C7622AFBE * value)
	{
		____enumString_21 = value;
		Il2CppCodeGenWriteBarrier((&____enumString_21), value);
	}

	inline static int32_t get_offset_of__advancedGenericKey_22() { return static_cast<int32_t>(offsetof(DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272, ____advancedGenericKey_22)); }
	inline AdvanGeneric_String_tC1B14FFB5530AA2D7D67CF242DBBDA106722CDDB * get__advancedGenericKey_22() const { return ____advancedGenericKey_22; }
	inline AdvanGeneric_String_tC1B14FFB5530AA2D7D67CF242DBBDA106722CDDB ** get_address_of__advancedGenericKey_22() { return &____advancedGenericKey_22; }
	inline void set__advancedGenericKey_22(AdvanGeneric_String_tC1B14FFB5530AA2D7D67CF242DBBDA106722CDDB * value)
	{
		____advancedGenericKey_22 = value;
		Il2CppCodeGenWriteBarrier((&____advancedGenericKey_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATABASEEXAMPLE_T21198F7A6479F09A718FA7CE5677AA43CD4AD272_H
#ifndef NESTEDDB_TF5B5A4419E0610C07A505B4E3DCC42A54C212E14_H
#define NESTEDDB_TF5B5A4419E0610C07A505B4E3DCC42A54C212E14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.NestedDB
struct  NestedDB_tF5B5A4419E0610C07A505B4E3DCC42A54C212E14  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// RotaryHeart.Lib.MainDict RotaryHeart.Lib.NestedDB::nested
	MainDict_tABB8831EB5CF88193E3BF7F318A84053BFE42682 * ___nested_4;

public:
	inline static int32_t get_offset_of_nested_4() { return static_cast<int32_t>(offsetof(NestedDB_tF5B5A4419E0610C07A505B4E3DCC42A54C212E14, ___nested_4)); }
	inline MainDict_tABB8831EB5CF88193E3BF7F318A84053BFE42682 * get_nested_4() const { return ___nested_4; }
	inline MainDict_tABB8831EB5CF88193E3BF7F318A84053BFE42682 ** get_address_of_nested_4() { return &___nested_4; }
	inline void set_nested_4(MainDict_tABB8831EB5CF88193E3BF7F318A84053BFE42682 * value)
	{
		___nested_4 = value;
		Il2CppCodeGenWriteBarrier((&___nested_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NESTEDDB_TF5B5A4419E0610C07A505B4E3DCC42A54C212E14_H
#ifndef REQUIREDREFERENCES_TD502DA241623B0039A32F954E9BB8040D038F5B0_H
#define REQUIREDREFERENCES_TD502DA241623B0039A32F954E9BB8040D038F5B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RotaryHeart.Lib.SerializableDictionary.RequiredReferences
struct  RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.GameObject RotaryHeart.Lib.SerializableDictionary.RequiredReferences::_gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____gameObject_4;
	// UnityEngine.Material RotaryHeart.Lib.SerializableDictionary.RequiredReferences::_material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____material_5;
	// UnityEngine.AudioClip RotaryHeart.Lib.SerializableDictionary.RequiredReferences::_audioClip
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ____audioClip_6;

public:
	inline static int32_t get_offset_of__gameObject_4() { return static_cast<int32_t>(offsetof(RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0, ____gameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__gameObject_4() const { return ____gameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__gameObject_4() { return &____gameObject_4; }
	inline void set__gameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____gameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&____gameObject_4), value);
	}

	inline static int32_t get_offset_of__material_5() { return static_cast<int32_t>(offsetof(RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0, ____material_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__material_5() const { return ____material_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__material_5() { return &____material_5; }
	inline void set__material_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____material_5 = value;
		Il2CppCodeGenWriteBarrier((&____material_5), value);
	}

	inline static int32_t get_offset_of__audioClip_6() { return static_cast<int32_t>(offsetof(RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0, ____audioClip_6)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get__audioClip_6() const { return ____audioClip_6; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of__audioClip_6() { return &____audioClip_6; }
	inline void set__audioClip_6(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		____audioClip_6 = value;
		Il2CppCodeGenWriteBarrier((&____audioClip_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIREDREFERENCES_TD502DA241623B0039A32F954E9BB8040D038F5B0_H
#ifndef CONSTRUCTORDELEGATE_T530B58A311FEE546C679759CFE520EA0D0FDC761_H
#define CONSTRUCTORDELEGATE_T530B58A311FEE546C679759CFE520EA0D0FDC761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils_ConstructorDelegate
struct  ConstructorDelegate_t530B58A311FEE546C679759CFE520EA0D0FDC761  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORDELEGATE_T530B58A311FEE546C679759CFE520EA0D0FDC761_H
#ifndef GETDELEGATE_TCBB4FA7AF97922763652DB4A13A5CC671C239658_H
#define GETDELEGATE_TCBB4FA7AF97922763652DB4A13A5CC671C239658_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils_GetDelegate
struct  GetDelegate_tCBB4FA7AF97922763652DB4A13A5CC671C239658  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETDELEGATE_TCBB4FA7AF97922763652DB4A13A5CC671C239658_H
#ifndef SETDELEGATE_TF514E819028ED8B328796D4A7B3841C57F7D16A2_H
#define SETDELEGATE_TF514E819028ED8B328796D4A7B3841C57F7D16A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJson2.Reflection.ReflectionUtils_SetDelegate
struct  SetDelegate_tF514E819028ED8B328796D4A7B3841C57F7D16A2  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETDELEGATE_TF514E819028ED8B328796D4A7B3841C57F7D16A2_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef JSBRIDGE_T641094D5F227E55A3CE1DBCEC277E73F307454FB_H
#define JSBRIDGE_T641094D5F227E55A3CE1DBCEC277E73F307454FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Canvas.JsBridge
struct  JsBridge_t641094D5F227E55A3CE1DBCEC277E73F307454FB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler Facebook.Unity.Canvas.JsBridge::facebook
	RuntimeObject* ___facebook_4;

public:
	inline static int32_t get_offset_of_facebook_4() { return static_cast<int32_t>(offsetof(JsBridge_t641094D5F227E55A3CE1DBCEC277E73F307454FB, ___facebook_4)); }
	inline RuntimeObject* get_facebook_4() const { return ___facebook_4; }
	inline RuntimeObject** get_address_of_facebook_4() { return &___facebook_4; }
	inline void set_facebook_4(RuntimeObject* value)
	{
		___facebook_4 = value;
		Il2CppCodeGenWriteBarrier((&___facebook_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSBRIDGE_T641094D5F227E55A3CE1DBCEC277E73F307454FB_H
#ifndef COMPILEDFACEBOOKLOADER_T00BC9903E387CA1A268154CB0B8132188627FA4F_H
#define COMPILEDFACEBOOKLOADER_T00BC9903E387CA1A268154CB0B8132188627FA4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB_CompiledFacebookLoader
struct  CompiledFacebookLoader_t00BC9903E387CA1A268154CB0B8132188627FA4F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILEDFACEBOOKLOADER_T00BC9903E387CA1A268154CB0B8132188627FA4F_H
#ifndef FACEBOOKGAMEOBJECT_T0EB6A31799B33368D3236B7D7E3A66E4ED3FF355_H
#define FACEBOOKGAMEOBJECT_T0EB6A31799B33368D3236B7D7E3A66E4ED3FF355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookGameObject
struct  FacebookGameObject_t0EB6A31799B33368D3236B7D7E3A66E4ED3FF355  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Facebook.Unity.IFacebookImplementation Facebook.Unity.FacebookGameObject::<Facebook>k__BackingField
	RuntimeObject* ___U3CFacebookU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CFacebookU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FacebookGameObject_t0EB6A31799B33368D3236B7D7E3A66E4ED3FF355, ___U3CFacebookU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CFacebookU3Ek__BackingField_4() const { return ___U3CFacebookU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CFacebookU3Ek__BackingField_4() { return &___U3CFacebookU3Ek__BackingField_4; }
	inline void set_U3CFacebookU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CFacebookU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFacebookU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKGAMEOBJECT_T0EB6A31799B33368D3236B7D7E3A66E4ED3FF355_H
#ifndef PLATFORMBASE_TCBFDE4E2C964A9DC16353A32B6F7974D5107187B_H
#define PLATFORMBASE_TCBFDE4E2C964A9DC16353A32B6F7974D5107187B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Platforms.PlatformBase
struct  PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<System.Action> GameSparks.Platforms.PlatformBase::_actions
	List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * ____actions_7;
	// System.Collections.Generic.List`1<System.Action> GameSparks.Platforms.PlatformBase::_currentActions
	List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * ____currentActions_8;
	// System.Boolean GameSparks.Platforms.PlatformBase::_allowQuitting
	bool ____allowQuitting_9;
	// System.String GameSparks.Platforms.PlatformBase::<DeviceName>k__BackingField
	String_t* ___U3CDeviceNameU3Ek__BackingField_10;
	// System.String GameSparks.Platforms.PlatformBase::<DeviceType>k__BackingField
	String_t* ___U3CDeviceTypeU3Ek__BackingField_11;
	// GameSparks.Core.GSData GameSparks.Platforms.PlatformBase::<DeviceStats>k__BackingField
	GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * ___U3CDeviceStatsU3Ek__BackingField_12;
	// System.String GameSparks.Platforms.PlatformBase::<DeviceId>k__BackingField
	String_t* ___U3CDeviceIdU3Ek__BackingField_13;
	// System.String GameSparks.Platforms.PlatformBase::<Platform>k__BackingField
	String_t* ___U3CPlatformU3Ek__BackingField_14;
	// System.Boolean GameSparks.Platforms.PlatformBase::<ExtraDebug>k__BackingField
	bool ___U3CExtraDebugU3Ek__BackingField_15;
	// System.String GameSparks.Platforms.PlatformBase::<PersistentDataPath>k__BackingField
	String_t* ___U3CPersistentDataPathU3Ek__BackingField_16;
	// System.String GameSparks.Platforms.PlatformBase::m_authToken
	String_t* ___m_authToken_17;
	// System.String GameSparks.Platforms.PlatformBase::m_userId
	String_t* ___m_userId_18;
	// System.Action`1<System.Exception> GameSparks.Platforms.PlatformBase::<ExceptionReporter>k__BackingField
	Action_1_t114FA5D778AE43CC5A12F16A8CCD137F67ED6962 * ___U3CExceptionReporterU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of__actions_7() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ____actions_7)); }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * get__actions_7() const { return ____actions_7; }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 ** get_address_of__actions_7() { return &____actions_7; }
	inline void set__actions_7(List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * value)
	{
		____actions_7 = value;
		Il2CppCodeGenWriteBarrier((&____actions_7), value);
	}

	inline static int32_t get_offset_of__currentActions_8() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ____currentActions_8)); }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * get__currentActions_8() const { return ____currentActions_8; }
	inline List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 ** get_address_of__currentActions_8() { return &____currentActions_8; }
	inline void set__currentActions_8(List_1_t9E7F53A878191B703A76E69CE36AA90A372800E7 * value)
	{
		____currentActions_8 = value;
		Il2CppCodeGenWriteBarrier((&____currentActions_8), value);
	}

	inline static int32_t get_offset_of__allowQuitting_9() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ____allowQuitting_9)); }
	inline bool get__allowQuitting_9() const { return ____allowQuitting_9; }
	inline bool* get_address_of__allowQuitting_9() { return &____allowQuitting_9; }
	inline void set__allowQuitting_9(bool value)
	{
		____allowQuitting_9 = value;
	}

	inline static int32_t get_offset_of_U3CDeviceNameU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CDeviceNameU3Ek__BackingField_10)); }
	inline String_t* get_U3CDeviceNameU3Ek__BackingField_10() const { return ___U3CDeviceNameU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CDeviceNameU3Ek__BackingField_10() { return &___U3CDeviceNameU3Ek__BackingField_10; }
	inline void set_U3CDeviceNameU3Ek__BackingField_10(String_t* value)
	{
		___U3CDeviceNameU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeviceNameU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CDeviceTypeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CDeviceTypeU3Ek__BackingField_11)); }
	inline String_t* get_U3CDeviceTypeU3Ek__BackingField_11() const { return ___U3CDeviceTypeU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDeviceTypeU3Ek__BackingField_11() { return &___U3CDeviceTypeU3Ek__BackingField_11; }
	inline void set_U3CDeviceTypeU3Ek__BackingField_11(String_t* value)
	{
		___U3CDeviceTypeU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeviceTypeU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CDeviceStatsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CDeviceStatsU3Ek__BackingField_12)); }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * get_U3CDeviceStatsU3Ek__BackingField_12() const { return ___U3CDeviceStatsU3Ek__BackingField_12; }
	inline GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 ** get_address_of_U3CDeviceStatsU3Ek__BackingField_12() { return &___U3CDeviceStatsU3Ek__BackingField_12; }
	inline void set_U3CDeviceStatsU3Ek__BackingField_12(GSData_t122BC20340935FE4E4E6F79E9A4E2F7C48844937 * value)
	{
		___U3CDeviceStatsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeviceStatsU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CDeviceIdU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CDeviceIdU3Ek__BackingField_13)); }
	inline String_t* get_U3CDeviceIdU3Ek__BackingField_13() const { return ___U3CDeviceIdU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CDeviceIdU3Ek__BackingField_13() { return &___U3CDeviceIdU3Ek__BackingField_13; }
	inline void set_U3CDeviceIdU3Ek__BackingField_13(String_t* value)
	{
		___U3CDeviceIdU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeviceIdU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CPlatformU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CPlatformU3Ek__BackingField_14)); }
	inline String_t* get_U3CPlatformU3Ek__BackingField_14() const { return ___U3CPlatformU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CPlatformU3Ek__BackingField_14() { return &___U3CPlatformU3Ek__BackingField_14; }
	inline void set_U3CPlatformU3Ek__BackingField_14(String_t* value)
	{
		___U3CPlatformU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlatformU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CExtraDebugU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CExtraDebugU3Ek__BackingField_15)); }
	inline bool get_U3CExtraDebugU3Ek__BackingField_15() const { return ___U3CExtraDebugU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CExtraDebugU3Ek__BackingField_15() { return &___U3CExtraDebugU3Ek__BackingField_15; }
	inline void set_U3CExtraDebugU3Ek__BackingField_15(bool value)
	{
		___U3CExtraDebugU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CPersistentDataPathU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CPersistentDataPathU3Ek__BackingField_16)); }
	inline String_t* get_U3CPersistentDataPathU3Ek__BackingField_16() const { return ___U3CPersistentDataPathU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CPersistentDataPathU3Ek__BackingField_16() { return &___U3CPersistentDataPathU3Ek__BackingField_16; }
	inline void set_U3CPersistentDataPathU3Ek__BackingField_16(String_t* value)
	{
		___U3CPersistentDataPathU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPersistentDataPathU3Ek__BackingField_16), value);
	}

	inline static int32_t get_offset_of_m_authToken_17() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___m_authToken_17)); }
	inline String_t* get_m_authToken_17() const { return ___m_authToken_17; }
	inline String_t** get_address_of_m_authToken_17() { return &___m_authToken_17; }
	inline void set_m_authToken_17(String_t* value)
	{
		___m_authToken_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_authToken_17), value);
	}

	inline static int32_t get_offset_of_m_userId_18() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___m_userId_18)); }
	inline String_t* get_m_userId_18() const { return ___m_userId_18; }
	inline String_t** get_address_of_m_userId_18() { return &___m_userId_18; }
	inline void set_m_userId_18(String_t* value)
	{
		___m_userId_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_userId_18), value);
	}

	inline static int32_t get_offset_of_U3CExceptionReporterU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B, ___U3CExceptionReporterU3Ek__BackingField_19)); }
	inline Action_1_t114FA5D778AE43CC5A12F16A8CCD137F67ED6962 * get_U3CExceptionReporterU3Ek__BackingField_19() const { return ___U3CExceptionReporterU3Ek__BackingField_19; }
	inline Action_1_t114FA5D778AE43CC5A12F16A8CCD137F67ED6962 ** get_address_of_U3CExceptionReporterU3Ek__BackingField_19() { return &___U3CExceptionReporterU3Ek__BackingField_19; }
	inline void set_U3CExceptionReporterU3Ek__BackingField_19(Action_1_t114FA5D778AE43CC5A12F16A8CCD137F67ED6962 * value)
	{
		___U3CExceptionReporterU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExceptionReporterU3Ek__BackingField_19), value);
	}
};

struct PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B_StaticFields
{
public:
	// System.String GameSparks.Platforms.PlatformBase::PLAYER_PREF_AUTHTOKEN_KEY
	String_t* ___PLAYER_PREF_AUTHTOKEN_KEY_4;
	// System.String GameSparks.Platforms.PlatformBase::PLAYER_PREF_USERID_KEY
	String_t* ___PLAYER_PREF_USERID_KEY_5;
	// System.String GameSparks.Platforms.PlatformBase::PLAYER_PREF_DEVICEID_KEY
	String_t* ___PLAYER_PREF_DEVICEID_KEY_6;

public:
	inline static int32_t get_offset_of_PLAYER_PREF_AUTHTOKEN_KEY_4() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B_StaticFields, ___PLAYER_PREF_AUTHTOKEN_KEY_4)); }
	inline String_t* get_PLAYER_PREF_AUTHTOKEN_KEY_4() const { return ___PLAYER_PREF_AUTHTOKEN_KEY_4; }
	inline String_t** get_address_of_PLAYER_PREF_AUTHTOKEN_KEY_4() { return &___PLAYER_PREF_AUTHTOKEN_KEY_4; }
	inline void set_PLAYER_PREF_AUTHTOKEN_KEY_4(String_t* value)
	{
		___PLAYER_PREF_AUTHTOKEN_KEY_4 = value;
		Il2CppCodeGenWriteBarrier((&___PLAYER_PREF_AUTHTOKEN_KEY_4), value);
	}

	inline static int32_t get_offset_of_PLAYER_PREF_USERID_KEY_5() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B_StaticFields, ___PLAYER_PREF_USERID_KEY_5)); }
	inline String_t* get_PLAYER_PREF_USERID_KEY_5() const { return ___PLAYER_PREF_USERID_KEY_5; }
	inline String_t** get_address_of_PLAYER_PREF_USERID_KEY_5() { return &___PLAYER_PREF_USERID_KEY_5; }
	inline void set_PLAYER_PREF_USERID_KEY_5(String_t* value)
	{
		___PLAYER_PREF_USERID_KEY_5 = value;
		Il2CppCodeGenWriteBarrier((&___PLAYER_PREF_USERID_KEY_5), value);
	}

	inline static int32_t get_offset_of_PLAYER_PREF_DEVICEID_KEY_6() { return static_cast<int32_t>(offsetof(PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B_StaticFields, ___PLAYER_PREF_DEVICEID_KEY_6)); }
	inline String_t* get_PLAYER_PREF_DEVICEID_KEY_6() const { return ___PLAYER_PREF_DEVICEID_KEY_6; }
	inline String_t** get_address_of_PLAYER_PREF_DEVICEID_KEY_6() { return &___PLAYER_PREF_DEVICEID_KEY_6; }
	inline void set_PLAYER_PREF_DEVICEID_KEY_6(String_t* value)
	{
		___PLAYER_PREF_DEVICEID_KEY_6 = value;
		Il2CppCodeGenWriteBarrier((&___PLAYER_PREF_DEVICEID_KEY_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMBASE_TCBFDE4E2C964A9DC16353A32B6F7974D5107187B_H
#ifndef GAMESPARKSRTUNITY_T60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B_H
#define GAMESPARKSRTUNITY_T60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparksRTUnity
struct  GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// GameSparks.RT.IRTSession GameSparksRTUnity::session
	RuntimeObject* ___session_4;
	// System.Action`1<System.Int32> GameSparksRTUnity::m_OnPlayerConnect
	Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B * ___m_OnPlayerConnect_5;
	// System.Action`1<System.Int32> GameSparksRTUnity::m_OnPlayerDisconnect
	Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B * ___m_OnPlayerDisconnect_6;
	// System.Action`1<System.Boolean> GameSparksRTUnity::m_OnReady
	Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * ___m_OnReady_7;
	// System.Action`1<GameSparks.RT.RTPacket> GameSparksRTUnity::m_OnPacket
	Action_1_t5CF6CC028A59A60A315AF5E2DB24AC3DEC655C8D * ___m_OnPacket_8;

public:
	inline static int32_t get_offset_of_session_4() { return static_cast<int32_t>(offsetof(GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B, ___session_4)); }
	inline RuntimeObject* get_session_4() const { return ___session_4; }
	inline RuntimeObject** get_address_of_session_4() { return &___session_4; }
	inline void set_session_4(RuntimeObject* value)
	{
		___session_4 = value;
		Il2CppCodeGenWriteBarrier((&___session_4), value);
	}

	inline static int32_t get_offset_of_m_OnPlayerConnect_5() { return static_cast<int32_t>(offsetof(GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B, ___m_OnPlayerConnect_5)); }
	inline Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B * get_m_OnPlayerConnect_5() const { return ___m_OnPlayerConnect_5; }
	inline Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B ** get_address_of_m_OnPlayerConnect_5() { return &___m_OnPlayerConnect_5; }
	inline void set_m_OnPlayerConnect_5(Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B * value)
	{
		___m_OnPlayerConnect_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnPlayerConnect_5), value);
	}

	inline static int32_t get_offset_of_m_OnPlayerDisconnect_6() { return static_cast<int32_t>(offsetof(GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B, ___m_OnPlayerDisconnect_6)); }
	inline Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B * get_m_OnPlayerDisconnect_6() const { return ___m_OnPlayerDisconnect_6; }
	inline Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B ** get_address_of_m_OnPlayerDisconnect_6() { return &___m_OnPlayerDisconnect_6; }
	inline void set_m_OnPlayerDisconnect_6(Action_1_tF0CF99E3EB61F030882DC0AF501614FF92CFD14B * value)
	{
		___m_OnPlayerDisconnect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnPlayerDisconnect_6), value);
	}

	inline static int32_t get_offset_of_m_OnReady_7() { return static_cast<int32_t>(offsetof(GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B, ___m_OnReady_7)); }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * get_m_OnReady_7() const { return ___m_OnReady_7; }
	inline Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD ** get_address_of_m_OnReady_7() { return &___m_OnReady_7; }
	inline void set_m_OnReady_7(Action_1_tAA0F894C98302D68F7D5034E8104E9AB4763CCAD * value)
	{
		___m_OnReady_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnReady_7), value);
	}

	inline static int32_t get_offset_of_m_OnPacket_8() { return static_cast<int32_t>(offsetof(GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B, ___m_OnPacket_8)); }
	inline Action_1_t5CF6CC028A59A60A315AF5E2DB24AC3DEC655C8D * get_m_OnPacket_8() const { return ___m_OnPacket_8; }
	inline Action_1_t5CF6CC028A59A60A315AF5E2DB24AC3DEC655C8D ** get_address_of_m_OnPacket_8() { return &___m_OnPacket_8; }
	inline void set_m_OnPacket_8(Action_1_t5CF6CC028A59A60A315AF5E2DB24AC3DEC655C8D * value)
	{
		___m_OnPacket_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnPacket_8), value);
	}
};

struct GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B_StaticFields
{
public:
	// GameSparksRTUnity GameSparksRTUnity::instance
	GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B * ___instance_9;

public:
	inline static int32_t get_offset_of_instance_9() { return static_cast<int32_t>(offsetof(GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B_StaticFields, ___instance_9)); }
	inline GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B * get_instance_9() const { return ___instance_9; }
	inline GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B ** get_address_of_instance_9() { return &___instance_9; }
	inline void set_instance_9(GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B * value)
	{
		___instance_9 = value;
		Il2CppCodeGenWriteBarrier((&___instance_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESPARKSRTUNITY_T60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B_H
#ifndef GAMESPARKSTESTUI_TCDE17396E8F0E885438E8A314723444E834F8027_H
#define GAMESPARKSTESTUI_TCDE17396E8F0E885438E8A314723444E834F8027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparksTestUI
struct  GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.Queue`1<System.String> GameSparksTestUI::myLogQueue
	Queue_1_t234B58D376F3C134441C47D5A9EF7789374EE172 * ___myLogQueue_4;
	// System.String GameSparksTestUI::myLog
	String_t* ___myLog_5;
	// System.String GameSparksTestUI::fbToken
	String_t* ___fbToken_6;
	// System.String GameSparksTestUI::dismissMessageId
	String_t* ___dismissMessageId_7;
	// System.Boolean GameSparksTestUI::testing
	bool ___testing_10;
	// System.Boolean GameSparksTestUI::working
	bool ___working_11;
	// System.Boolean GameSparksTestUI::result
	bool ___result_12;
	// System.Int32 GameSparksTestUI::counter
	int32_t ___counter_13;
	// System.Int32 GameSparksTestUI::numTest
	int32_t ___numTest_14;
	// UnityEngine.Texture GameSparksTestUI::cursor
	Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___cursor_15;

public:
	inline static int32_t get_offset_of_myLogQueue_4() { return static_cast<int32_t>(offsetof(GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027, ___myLogQueue_4)); }
	inline Queue_1_t234B58D376F3C134441C47D5A9EF7789374EE172 * get_myLogQueue_4() const { return ___myLogQueue_4; }
	inline Queue_1_t234B58D376F3C134441C47D5A9EF7789374EE172 ** get_address_of_myLogQueue_4() { return &___myLogQueue_4; }
	inline void set_myLogQueue_4(Queue_1_t234B58D376F3C134441C47D5A9EF7789374EE172 * value)
	{
		___myLogQueue_4 = value;
		Il2CppCodeGenWriteBarrier((&___myLogQueue_4), value);
	}

	inline static int32_t get_offset_of_myLog_5() { return static_cast<int32_t>(offsetof(GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027, ___myLog_5)); }
	inline String_t* get_myLog_5() const { return ___myLog_5; }
	inline String_t** get_address_of_myLog_5() { return &___myLog_5; }
	inline void set_myLog_5(String_t* value)
	{
		___myLog_5 = value;
		Il2CppCodeGenWriteBarrier((&___myLog_5), value);
	}

	inline static int32_t get_offset_of_fbToken_6() { return static_cast<int32_t>(offsetof(GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027, ___fbToken_6)); }
	inline String_t* get_fbToken_6() const { return ___fbToken_6; }
	inline String_t** get_address_of_fbToken_6() { return &___fbToken_6; }
	inline void set_fbToken_6(String_t* value)
	{
		___fbToken_6 = value;
		Il2CppCodeGenWriteBarrier((&___fbToken_6), value);
	}

	inline static int32_t get_offset_of_dismissMessageId_7() { return static_cast<int32_t>(offsetof(GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027, ___dismissMessageId_7)); }
	inline String_t* get_dismissMessageId_7() const { return ___dismissMessageId_7; }
	inline String_t** get_address_of_dismissMessageId_7() { return &___dismissMessageId_7; }
	inline void set_dismissMessageId_7(String_t* value)
	{
		___dismissMessageId_7 = value;
		Il2CppCodeGenWriteBarrier((&___dismissMessageId_7), value);
	}

	inline static int32_t get_offset_of_testing_10() { return static_cast<int32_t>(offsetof(GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027, ___testing_10)); }
	inline bool get_testing_10() const { return ___testing_10; }
	inline bool* get_address_of_testing_10() { return &___testing_10; }
	inline void set_testing_10(bool value)
	{
		___testing_10 = value;
	}

	inline static int32_t get_offset_of_working_11() { return static_cast<int32_t>(offsetof(GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027, ___working_11)); }
	inline bool get_working_11() const { return ___working_11; }
	inline bool* get_address_of_working_11() { return &___working_11; }
	inline void set_working_11(bool value)
	{
		___working_11 = value;
	}

	inline static int32_t get_offset_of_result_12() { return static_cast<int32_t>(offsetof(GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027, ___result_12)); }
	inline bool get_result_12() const { return ___result_12; }
	inline bool* get_address_of_result_12() { return &___result_12; }
	inline void set_result_12(bool value)
	{
		___result_12 = value;
	}

	inline static int32_t get_offset_of_counter_13() { return static_cast<int32_t>(offsetof(GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027, ___counter_13)); }
	inline int32_t get_counter_13() const { return ___counter_13; }
	inline int32_t* get_address_of_counter_13() { return &___counter_13; }
	inline void set_counter_13(int32_t value)
	{
		___counter_13 = value;
	}

	inline static int32_t get_offset_of_numTest_14() { return static_cast<int32_t>(offsetof(GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027, ___numTest_14)); }
	inline int32_t get_numTest_14() const { return ___numTest_14; }
	inline int32_t* get_address_of_numTest_14() { return &___numTest_14; }
	inline void set_numTest_14(int32_t value)
	{
		___numTest_14 = value;
	}

	inline static int32_t get_offset_of_cursor_15() { return static_cast<int32_t>(offsetof(GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027, ___cursor_15)); }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * get_cursor_15() const { return ___cursor_15; }
	inline Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 ** get_address_of_cursor_15() { return &___cursor_15; }
	inline void set_cursor_15(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * value)
	{
		___cursor_15 = value;
		Il2CppCodeGenWriteBarrier((&___cursor_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESPARKSTESTUI_TCDE17396E8F0E885438E8A314723444E834F8027_H
#ifndef GAMESPARKSUNITY_T46BBFD28122E29CE88D6268B23ADA333FFE32617_H
#define GAMESPARKSUNITY_T46BBFD28122E29CE88D6268B23ADA333FFE32617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparksUnity
struct  GameSparksUnity_t46BBFD28122E29CE88D6268B23ADA333FFE32617  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// GameSparksSettings GameSparksUnity::settings
	GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4 * ___settings_4;

public:
	inline static int32_t get_offset_of_settings_4() { return static_cast<int32_t>(offsetof(GameSparksUnity_t46BBFD28122E29CE88D6268B23ADA333FFE32617, ___settings_4)); }
	inline GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4 * get_settings_4() const { return ___settings_4; }
	inline GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4 ** get_address_of_settings_4() { return &___settings_4; }
	inline void set_settings_4(GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4 * value)
	{
		___settings_4 = value;
		Il2CppCodeGenWriteBarrier((&___settings_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESPARKSUNITY_T46BBFD28122E29CE88D6268B23ADA333FFE32617_H
#ifndef JOYSTICK_T5A918BD4C98ECB45624B10577F91F1E5FA25F26B_H
#define JOYSTICK_T5A918BD4C98ECB45624B10577F91F1E5FA25F26B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Joystick
struct  Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Joystick::handleLimit
	float ___handleLimit_4;
	// JoystickMode Joystick::joystickMode
	int32_t ___joystickMode_5;
	// UnityEngine.Vector2 Joystick::inputVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___inputVector_6;
	// UnityEngine.RectTransform Joystick::background
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___background_7;
	// UnityEngine.RectTransform Joystick::handle
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___handle_8;

public:
	inline static int32_t get_offset_of_handleLimit_4() { return static_cast<int32_t>(offsetof(Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B, ___handleLimit_4)); }
	inline float get_handleLimit_4() const { return ___handleLimit_4; }
	inline float* get_address_of_handleLimit_4() { return &___handleLimit_4; }
	inline void set_handleLimit_4(float value)
	{
		___handleLimit_4 = value;
	}

	inline static int32_t get_offset_of_joystickMode_5() { return static_cast<int32_t>(offsetof(Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B, ___joystickMode_5)); }
	inline int32_t get_joystickMode_5() const { return ___joystickMode_5; }
	inline int32_t* get_address_of_joystickMode_5() { return &___joystickMode_5; }
	inline void set_joystickMode_5(int32_t value)
	{
		___joystickMode_5 = value;
	}

	inline static int32_t get_offset_of_inputVector_6() { return static_cast<int32_t>(offsetof(Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B, ___inputVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_inputVector_6() const { return ___inputVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_inputVector_6() { return &___inputVector_6; }
	inline void set_inputVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___inputVector_6 = value;
	}

	inline static int32_t get_offset_of_background_7() { return static_cast<int32_t>(offsetof(Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B, ___background_7)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_background_7() const { return ___background_7; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_background_7() { return &___background_7; }
	inline void set_background_7(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___background_7 = value;
		Il2CppCodeGenWriteBarrier((&___background_7), value);
	}

	inline static int32_t get_offset_of_handle_8() { return static_cast<int32_t>(offsetof(Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B, ___handle_8)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_handle_8() const { return ___handle_8; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_handle_8() { return &___handle_8; }
	inline void set_handle_8(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___handle_8 = value;
		Il2CppCodeGenWriteBarrier((&___handle_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOYSTICK_T5A918BD4C98ECB45624B10577F91F1E5FA25F26B_H
#ifndef PLAYER2DEXAMPLE_TA154B77C4FBA8FD6E7A69855547F19F9C110DBFD_H
#define PLAYER2DEXAMPLE_TA154B77C4FBA8FD6E7A69855547F19F9C110DBFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player2DExample
struct  Player2DExample_tA154B77C4FBA8FD6E7A69855547F19F9C110DBFD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Player2DExample::moveSpeed
	float ___moveSpeed_4;
	// Joystick Player2DExample::joystick
	Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B * ___joystick_5;

public:
	inline static int32_t get_offset_of_moveSpeed_4() { return static_cast<int32_t>(offsetof(Player2DExample_tA154B77C4FBA8FD6E7A69855547F19F9C110DBFD, ___moveSpeed_4)); }
	inline float get_moveSpeed_4() const { return ___moveSpeed_4; }
	inline float* get_address_of_moveSpeed_4() { return &___moveSpeed_4; }
	inline void set_moveSpeed_4(float value)
	{
		___moveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_joystick_5() { return static_cast<int32_t>(offsetof(Player2DExample_tA154B77C4FBA8FD6E7A69855547F19F9C110DBFD, ___joystick_5)); }
	inline Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B * get_joystick_5() const { return ___joystick_5; }
	inline Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B ** get_address_of_joystick_5() { return &___joystick_5; }
	inline void set_joystick_5(Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B * value)
	{
		___joystick_5 = value;
		Il2CppCodeGenWriteBarrier((&___joystick_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER2DEXAMPLE_TA154B77C4FBA8FD6E7A69855547F19F9C110DBFD_H
#ifndef PLAYER3DEXAMPLE_T68A1F1513D7ACC10802F65B91D311FC27E8C1085_H
#define PLAYER3DEXAMPLE_T68A1F1513D7ACC10802F65B91D311FC27E8C1085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player3DExample
struct  Player3DExample_t68A1F1513D7ACC10802F65B91D311FC27E8C1085  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Player3DExample::moveSpeed
	float ___moveSpeed_4;
	// Joystick Player3DExample::joystick
	Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B * ___joystick_5;

public:
	inline static int32_t get_offset_of_moveSpeed_4() { return static_cast<int32_t>(offsetof(Player3DExample_t68A1F1513D7ACC10802F65B91D311FC27E8C1085, ___moveSpeed_4)); }
	inline float get_moveSpeed_4() const { return ___moveSpeed_4; }
	inline float* get_address_of_moveSpeed_4() { return &___moveSpeed_4; }
	inline void set_moveSpeed_4(float value)
	{
		___moveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_joystick_5() { return static_cast<int32_t>(offsetof(Player3DExample_t68A1F1513D7ACC10802F65B91D311FC27E8C1085, ___joystick_5)); }
	inline Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B * get_joystick_5() const { return ___joystick_5; }
	inline Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B ** get_address_of_joystick_5() { return &___joystick_5; }
	inline void set_joystick_5(Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B * value)
	{
		___joystick_5 = value;
		Il2CppCodeGenWriteBarrier((&___joystick_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER3DEXAMPLE_T68A1F1513D7ACC10802F65B91D311FC27E8C1085_H
#ifndef CANVASFACEBOOKGAMEOBJECT_T0A0C5A026D4B0C3CE4C3A5D858AB97B0FBDE18EC_H
#define CANVASFACEBOOKGAMEOBJECT_T0A0C5A026D4B0C3CE4C3A5D858AB97B0FBDE18EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Canvas.CanvasFacebookGameObject
struct  CanvasFacebookGameObject_t0A0C5A026D4B0C3CE4C3A5D858AB97B0FBDE18EC  : public FacebookGameObject_t0EB6A31799B33368D3236B7D7E3A66E4ED3FF355
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASFACEBOOKGAMEOBJECT_T0A0C5A026D4B0C3CE4C3A5D858AB97B0FBDE18EC_H
#ifndef CANVASFACEBOOKLOADER_T95B30D5E93D5FB14EF98C9AEB98EAA9F3E127C40_H
#define CANVASFACEBOOKLOADER_T95B30D5E93D5FB14EF98C9AEB98EAA9F3E127C40_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Canvas.CanvasFacebookLoader
struct  CanvasFacebookLoader_t95B30D5E93D5FB14EF98C9AEB98EAA9F3E127C40  : public CompiledFacebookLoader_t00BC9903E387CA1A268154CB0B8132188627FA4F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASFACEBOOKLOADER_T95B30D5E93D5FB14EF98C9AEB98EAA9F3E127C40_H
#ifndef ANDROIDFACEBOOKLOADER_T33C399ECDC89D308AEC07E92A91C5CDE9AD1D8BB_H
#define ANDROIDFACEBOOKLOADER_T33C399ECDC89D308AEC07E92A91C5CDE9AD1D8BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.Android.AndroidFacebookLoader
struct  AndroidFacebookLoader_t33C399ECDC89D308AEC07E92A91C5CDE9AD1D8BB  : public CompiledFacebookLoader_t00BC9903E387CA1A268154CB0B8132188627FA4F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDFACEBOOKLOADER_T33C399ECDC89D308AEC07E92A91C5CDE9AD1D8BB_H
#ifndef IOSFACEBOOKLOADER_TF69D4660C33334921EDE47F118209D4C2A5F9981_H
#define IOSFACEBOOKLOADER_TF69D4660C33334921EDE47F118209D4C2A5F9981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.IOS.IOSFacebookLoader
struct  IOSFacebookLoader_tF69D4660C33334921EDE47F118209D4C2A5F9981  : public CompiledFacebookLoader_t00BC9903E387CA1A268154CB0B8132188627FA4F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSFACEBOOKLOADER_TF69D4660C33334921EDE47F118209D4C2A5F9981_H
#ifndef MOBILEFACEBOOKGAMEOBJECT_TAB54D83E740F41EC4BF47AFC1124B12C3175505B_H
#define MOBILEFACEBOOKGAMEOBJECT_TAB54D83E740F41EC4BF47AFC1124B12C3175505B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.MobileFacebookGameObject
struct  MobileFacebookGameObject_tAB54D83E740F41EC4BF47AFC1124B12C3175505B  : public FacebookGameObject_t0EB6A31799B33368D3236B7D7E3A66E4ED3FF355
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEFACEBOOKGAMEOBJECT_TAB54D83E740F41EC4BF47AFC1124B12C3175505B_H
#ifndef FIXEDJOYSTICK_T7817CAA727F3C01DB8C1E20D5CB79542CF008AF8_H
#define FIXEDJOYSTICK_T7817CAA727F3C01DB8C1E20D5CB79542CF008AF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FixedJoystick
struct  FixedJoystick_t7817CAA727F3C01DB8C1E20D5CB79542CF008AF8  : public Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B
{
public:
	// UnityEngine.Vector2 FixedJoystick::joystickPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___joystickPosition_9;
	// UnityEngine.Camera FixedJoystick::cam
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___cam_10;

public:
	inline static int32_t get_offset_of_joystickPosition_9() { return static_cast<int32_t>(offsetof(FixedJoystick_t7817CAA727F3C01DB8C1E20D5CB79542CF008AF8, ___joystickPosition_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_joystickPosition_9() const { return ___joystickPosition_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_joystickPosition_9() { return &___joystickPosition_9; }
	inline void set_joystickPosition_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___joystickPosition_9 = value;
	}

	inline static int32_t get_offset_of_cam_10() { return static_cast<int32_t>(offsetof(FixedJoystick_t7817CAA727F3C01DB8C1E20D5CB79542CF008AF8, ___cam_10)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_cam_10() const { return ___cam_10; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_cam_10() { return &___cam_10; }
	inline void set_cam_10(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___cam_10 = value;
		Il2CppCodeGenWriteBarrier((&___cam_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDJOYSTICK_T7817CAA727F3C01DB8C1E20D5CB79542CF008AF8_H
#ifndef FLOATINGJOYSTICK_T41C399D884FFC840F0FE3B4A0E6C2EC2B2F8460D_H
#define FLOATINGJOYSTICK_T41C399D884FFC840F0FE3B4A0E6C2EC2B2F8460D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatingJoystick
struct  FloatingJoystick_t41C399D884FFC840F0FE3B4A0E6C2EC2B2F8460D  : public Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B
{
public:
	// UnityEngine.Vector2 FloatingJoystick::joystickCenter
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___joystickCenter_9;

public:
	inline static int32_t get_offset_of_joystickCenter_9() { return static_cast<int32_t>(offsetof(FloatingJoystick_t41C399D884FFC840F0FE3B4A0E6C2EC2B2F8460D, ___joystickCenter_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_joystickCenter_9() const { return ___joystickCenter_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_joystickCenter_9() { return &___joystickCenter_9; }
	inline void set_joystickCenter_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___joystickCenter_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATINGJOYSTICK_T41C399D884FFC840F0FE3B4A0E6C2EC2B2F8460D_H
#ifndef DEFAULTPLATFORM_TC9B39FE4FEC2AA4AE5C111BFCE1F9186C3B2296E_H
#define DEFAULTPLATFORM_TC9B39FE4FEC2AA4AE5C111BFCE1F9186C3B2296E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSparks.Platforms.DefaultPlatform
struct  DefaultPlatform_tC9B39FE4FEC2AA4AE5C111BFCE1F9186C3B2296E  : public PlatformBase_tCBFDE4E2C964A9DC16353A32B6F7974D5107187B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTPLATFORM_TC9B39FE4FEC2AA4AE5C111BFCE1F9186C3B2296E_H
#ifndef VARIABLEJOYSTICK_T041C11ECB97E2CC88858487F79950CBABD06FF7E_H
#define VARIABLEJOYSTICK_T041C11ECB97E2CC88858487F79950CBABD06FF7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VariableJoystick
struct  VariableJoystick_t041C11ECB97E2CC88858487F79950CBABD06FF7E  : public Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B
{
public:
	// System.Boolean VariableJoystick::isFixed
	bool ___isFixed_9;
	// UnityEngine.Vector2 VariableJoystick::fixedScreenPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___fixedScreenPosition_10;
	// UnityEngine.Vector2 VariableJoystick::joystickCenter
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___joystickCenter_11;

public:
	inline static int32_t get_offset_of_isFixed_9() { return static_cast<int32_t>(offsetof(VariableJoystick_t041C11ECB97E2CC88858487F79950CBABD06FF7E, ___isFixed_9)); }
	inline bool get_isFixed_9() const { return ___isFixed_9; }
	inline bool* get_address_of_isFixed_9() { return &___isFixed_9; }
	inline void set_isFixed_9(bool value)
	{
		___isFixed_9 = value;
	}

	inline static int32_t get_offset_of_fixedScreenPosition_10() { return static_cast<int32_t>(offsetof(VariableJoystick_t041C11ECB97E2CC88858487F79950CBABD06FF7E, ___fixedScreenPosition_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_fixedScreenPosition_10() const { return ___fixedScreenPosition_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_fixedScreenPosition_10() { return &___fixedScreenPosition_10; }
	inline void set_fixedScreenPosition_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___fixedScreenPosition_10 = value;
	}

	inline static int32_t get_offset_of_joystickCenter_11() { return static_cast<int32_t>(offsetof(VariableJoystick_t041C11ECB97E2CC88858487F79950CBABD06FF7E, ___joystickCenter_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_joystickCenter_11() const { return ___joystickCenter_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_joystickCenter_11() { return &___joystickCenter_11; }
	inline void set_joystickCenter_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___joystickCenter_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLEJOYSTICK_T041C11ECB97E2CC88858487F79950CBABD06FF7E_H
#ifndef ANDROIDFACEBOOKGAMEOBJECT_T4F341D404D5EDA18C21C8713532F08E88527F145_H
#define ANDROIDFACEBOOKGAMEOBJECT_T4F341D404D5EDA18C21C8713532F08E88527F145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.Android.AndroidFacebookGameObject
struct  AndroidFacebookGameObject_t4F341D404D5EDA18C21C8713532F08E88527F145  : public MobileFacebookGameObject_tAB54D83E740F41EC4BF47AFC1124B12C3175505B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDFACEBOOKGAMEOBJECT_T4F341D404D5EDA18C21C8713532F08E88527F145_H
#ifndef IOSFACEBOOKGAMEOBJECT_TB3C4C71AB4D6F0064D3B834C5E0BB7BA2A26AC7F_H
#define IOSFACEBOOKGAMEOBJECT_TB3C4C71AB4D6F0064D3B834C5E0BB7BA2A26AC7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.IOS.IOSFacebookGameObject
struct  IOSFacebookGameObject_tB3C4C71AB4D6F0064D3B834C5E0BB7BA2A26AC7F  : public MobileFacebookGameObject_tAB54D83E740F41EC4BF47AFC1124B12C3175505B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSFACEBOOKGAMEOBJECT_TB3C4C71AB4D6F0064D3B834C5E0BB7BA2A26AC7F_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6800 = { sizeof (MobileFacebookGameObject_tAB54D83E740F41EC4BF47AFC1124B12C3175505B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6801 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6802 = { sizeof (IOSFacebook_t89B3709B0F51CA70FC1D298F8949D95B38CB7BE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6802[2] = 
{
	IOSFacebook_t89B3709B0F51CA70FC1D298F8949D95B38CB7BE6::get_offset_of_limitEventUsage_4(),
	IOSFacebook_t89B3709B0F51CA70FC1D298F8949D95B38CB7BE6::get_offset_of_iosWrapper_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6803 = { sizeof (NativeDict_t902E3B1188FE573A691FAFD826F5C9F866A9CFFA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6803[3] = 
{
	NativeDict_t902E3B1188FE573A691FAFD826F5C9F866A9CFFA::get_offset_of_U3CNumEntriesU3Ek__BackingField_0(),
	NativeDict_t902E3B1188FE573A691FAFD826F5C9F866A9CFFA::get_offset_of_U3CKeysU3Ek__BackingField_1(),
	NativeDict_t902E3B1188FE573A691FAFD826F5C9F866A9CFFA::get_offset_of_U3CValuesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6804 = { sizeof (IOSFacebookGameObject_tB3C4C71AB4D6F0064D3B834C5E0BB7BA2A26AC7F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6805 = { sizeof (IOSFacebookLoader_tF69D4660C33334921EDE47F118209D4C2A5F9981), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6806 = { sizeof (AndroidFacebook_t392A5DA388638E742F4C61E38AEA820B2C2C08EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6806[3] = 
{
	AndroidFacebook_t392A5DA388638E742F4C61E38AEA820B2C2C08EA::get_offset_of_limitEventUsage_4(),
	AndroidFacebook_t392A5DA388638E742F4C61E38AEA820B2C2C08EA::get_offset_of_androidWrapper_5(),
	AndroidFacebook_t392A5DA388638E742F4C61E38AEA820B2C2C08EA::get_offset_of_U3CKeyHashU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6807 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6807[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6808 = { sizeof (AndroidFacebookGameObject_t4F341D404D5EDA18C21C8713532F08E88527F145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6809 = { sizeof (AndroidFacebookLoader_t33C399ECDC89D308AEC07E92A91C5CDE9AD1D8BB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6810 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6811 = { sizeof (CanvasFacebook_tA57CA846ABDD0B8AEAD345296B5F9B6818BA8818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6811[5] = 
{
	CanvasFacebook_tA57CA846ABDD0B8AEAD345296B5F9B6818BA8818::get_offset_of_appId_3(),
	CanvasFacebook_tA57CA846ABDD0B8AEAD345296B5F9B6818BA8818::get_offset_of_appLinkUrl_4(),
	CanvasFacebook_tA57CA846ABDD0B8AEAD345296B5F9B6818BA8818::get_offset_of_canvasJSWrapper_5(),
	CanvasFacebook_tA57CA846ABDD0B8AEAD345296B5F9B6818BA8818::get_offset_of_onHideUnityDelegate_6(),
	CanvasFacebook_tA57CA846ABDD0B8AEAD345296B5F9B6818BA8818::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6812 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6812[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6813 = { sizeof (U3CU3Ec_tAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5), -1, sizeof(U3CU3Ec_tAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6813[2] = 
{
	U3CU3Ec_tAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tAEEA5250DFB6C16DA6B68FFE19968CF83BFE8CC5_StaticFields::get_offset_of_U3CU3E9__40_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6814 = { sizeof (U3CU3Ec__DisplayClass47_0_t419FE32629C2F150E6F9F7221BF40D6C0C519ECB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6814[2] = 
{
	U3CU3Ec__DisplayClass47_0_t419FE32629C2F150E6F9F7221BF40D6C0C519ECB::get_offset_of_result_0(),
	U3CU3Ec__DisplayClass47_0_t419FE32629C2F150E6F9F7221BF40D6C0C519ECB::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6815 = { sizeof (CanvasFacebookGameObject_t0A0C5A026D4B0C3CE4C3A5D858AB97B0FBDE18EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6816 = { sizeof (CanvasFacebookLoader_t95B30D5E93D5FB14EF98C9AEB98EAA9F3E127C40), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6817 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6818 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6819 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6820 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6821 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6822 = { sizeof (JsBridge_t641094D5F227E55A3CE1DBCEC277E73F307454FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6822[1] = 
{
	JsBridge_t641094D5F227E55A3CE1DBCEC277E73F307454FB::get_offset_of_facebook_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6823 = { sizeof (U3CModuleU3E_tF109B2B0745E38DCB0076B99BD7FB2404E195578), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6824 = { sizeof (GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B), -1, sizeof(GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6824[6] = 
{
	GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B::get_offset_of_session_4(),
	GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B::get_offset_of_m_OnPlayerConnect_5(),
	GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B::get_offset_of_m_OnPlayerDisconnect_6(),
	GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B::get_offset_of_m_OnReady_7(),
	GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B::get_offset_of_m_OnPacket_8(),
	GameSparksRTUnity_t60DB661D0A59A62D9DD2DF7E12988FC9ACC7919B_StaticFields::get_offset_of_instance_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6825 = { sizeof (RTDataExtensions_tEEE8BAA609AB023F463DDF2C7541AA2B59911EDA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6826 = { sizeof (GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4), -1, sizeof(GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6826[12] = 
{
	0,
	0,
	0,
	GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4_StaticFields::get_offset_of_liveServiceUrlBase_7(),
	GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4_StaticFields::get_offset_of_previewServiceUrlBase_8(),
	GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4_StaticFields::get_offset_of_instance_9(),
	GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4::get_offset_of_sdkVersion_10(),
	GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4::get_offset_of_apiKey_11(),
	GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4::get_offset_of_credential_12(),
	GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4::get_offset_of_apiSecret_13(),
	GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4::get_offset_of_previewBuild_14(),
	GameSparksSettings_t0B74BF11E5E1BB82286302735CFDFF291FE41FC4::get_offset_of_debugBuild_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6827 = { sizeof (GameSparksUnity_t46BBFD28122E29CE88D6268B23ADA333FFE32617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6827[1] = 
{
	GameSparksUnity_t46BBFD28122E29CE88D6268B23ADA333FFE32617::get_offset_of_settings_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6828 = { sizeof (GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6828[12] = 
{
	GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027::get_offset_of_myLogQueue_4(),
	GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027::get_offset_of_myLog_5(),
	GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027::get_offset_of_fbToken_6(),
	GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027::get_offset_of_dismissMessageId_7(),
	0,
	0,
	GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027::get_offset_of_testing_10(),
	GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027::get_offset_of_working_11(),
	GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027::get_offset_of_result_12(),
	GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027::get_offset_of_counter_13(),
	GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027::get_offset_of_numTest_14(),
	GameSparksTestUI_tCDE17396E8F0E885438E8A314723444E834F8027::get_offset_of_cursor_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6829 = { sizeof (U3CU3Ec__DisplayClass15_0_tC67E67197EC95281396577A19BFBF46CA70F1AE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6829[2] = 
{
	U3CU3Ec__DisplayClass15_0_tC67E67197EC95281396577A19BFBF46CA70F1AE6::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass15_0_tC67E67197EC95281396577A19BFBF46CA70F1AE6::get_offset_of_logString_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6830 = { sizeof (U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC), -1, sizeof(U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6830[3] = 
{
	U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC_StaticFields::get_offset_of_U3CU3E9__17_7_1(),
	U3CU3Ec_tA9A69283F64527B1C7F429072435A855B20DC6AC_StaticFields::get_offset_of_U3CU3E9__17_8_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6831 = { sizeof (Player2DExample_tA154B77C4FBA8FD6E7A69855547F19F9C110DBFD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6831[2] = 
{
	Player2DExample_tA154B77C4FBA8FD6E7A69855547F19F9C110DBFD::get_offset_of_moveSpeed_4(),
	Player2DExample_tA154B77C4FBA8FD6E7A69855547F19F9C110DBFD::get_offset_of_joystick_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6832 = { sizeof (Player3DExample_t68A1F1513D7ACC10802F65B91D311FC27E8C1085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6832[2] = 
{
	Player3DExample_t68A1F1513D7ACC10802F65B91D311FC27E8C1085::get_offset_of_moveSpeed_4(),
	Player3DExample_t68A1F1513D7ACC10802F65B91D311FC27E8C1085::get_offset_of_joystick_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6833 = { sizeof (Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6833[5] = 
{
	Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B::get_offset_of_handleLimit_4(),
	Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B::get_offset_of_joystickMode_5(),
	Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B::get_offset_of_inputVector_6(),
	Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B::get_offset_of_background_7(),
	Joystick_t5A918BD4C98ECB45624B10577F91F1E5FA25F26B::get_offset_of_handle_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6834 = { sizeof (JoystickMode_t4D81E7222D27377A127289ADE6BE889EEBB8E064)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6834[4] = 
{
	JoystickMode_t4D81E7222D27377A127289ADE6BE889EEBB8E064::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6835 = { sizeof (FixedJoystick_t7817CAA727F3C01DB8C1E20D5CB79542CF008AF8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6835[2] = 
{
	FixedJoystick_t7817CAA727F3C01DB8C1E20D5CB79542CF008AF8::get_offset_of_joystickPosition_9(),
	FixedJoystick_t7817CAA727F3C01DB8C1E20D5CB79542CF008AF8::get_offset_of_cam_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6836 = { sizeof (FloatingJoystick_t41C399D884FFC840F0FE3B4A0E6C2EC2B2F8460D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6836[1] = 
{
	FloatingJoystick_t41C399D884FFC840F0FE3B4A0E6C2EC2B2F8460D::get_offset_of_joystickCenter_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6837 = { sizeof (VariableJoystick_t041C11ECB97E2CC88858487F79950CBABD06FF7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6837[3] = 
{
	VariableJoystick_t041C11ECB97E2CC88858487F79950CBABD06FF7E::get_offset_of_isFixed_9(),
	VariableJoystick_t041C11ECB97E2CC88858487F79950CBABD06FF7E::get_offset_of_fixedScreenPosition_10(),
	VariableJoystick_t041C11ECB97E2CC88858487F79950CBABD06FF7E::get_offset_of_joystickCenter_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6838 = { sizeof (DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6838[19] = 
{
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__genericString_4(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__genericGeneric_5(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__stringGeneric_6(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__intGeneric_7(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__intGameobject_8(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__gameobjectInt_9(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__stringGameobject_10(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__gameobjectString_11(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__stringMaterial_12(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__materialString_13(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__vector3Quaternion_14(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__quaternionVector3_15(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__stringAudioClip_16(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__audioClipString_17(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__charInt_18(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__gradientInt_19(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__intArray_20(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__enumString_21(),
	DataBaseExample_t21198F7A6479F09A718FA7CE5677AA43CD4AD272::get_offset_of__advancedGenericKey_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6839 = { sizeof (EnumExample_tCA85F729B8DD845505686B5CDC90D349F6B3CB93)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6839[8] = 
{
	EnumExample_tCA85F729B8DD845505686B5CDC90D349F6B3CB93::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6840 = { sizeof (ChildTest_tD7B3D6ADADE74A931FE00600949D174BB7E828D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6840[3] = 
{
	ChildTest_tD7B3D6ADADE74A931FE00600949D174BB7E828D4::get_offset_of_myChildColor_0(),
	ChildTest_tD7B3D6ADADE74A931FE00600949D174BB7E828D4::get_offset_of_myChildBool_1(),
	ChildTest_tD7B3D6ADADE74A931FE00600949D174BB7E828D4::get_offset_of_test_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6841 = { sizeof (ClassTest_tF93526EE6BB3FCA050DBE3883332CB00F8581DB4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6841[5] = 
{
	ClassTest_tF93526EE6BB3FCA050DBE3883332CB00F8581DB4::get_offset_of_id_0(),
	ClassTest_tF93526EE6BB3FCA050DBE3883332CB00F8581DB4::get_offset_of_test_1(),
	ClassTest_tF93526EE6BB3FCA050DBE3883332CB00F8581DB4::get_offset_of_test2_2(),
	ClassTest_tF93526EE6BB3FCA050DBE3883332CB00F8581DB4::get_offset_of_quat_3(),
	ClassTest_tF93526EE6BB3FCA050DBE3883332CB00F8581DB4::get_offset_of_childTest_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6842 = { sizeof (ArrayTest_tC68B49DBCCAFCE5D3DB0BD59DAC0ED1B8D95F6D0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6842[1] = 
{
	ArrayTest_tC68B49DBCCAFCE5D3DB0BD59DAC0ED1B8D95F6D0::get_offset_of_myArray_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6843 = { sizeof (AdvancedGenericClass_tC09D58B7902B02C8C31ACD1DD128EDCB13721D44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6843[1] = 
{
	AdvancedGenericClass_tC09D58B7902B02C8C31ACD1DD128EDCB13721D44::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6844 = { sizeof (Generic_String_tCF72708E4A84958EC2F36570DE29BAD94F59EE49), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6845 = { sizeof (Generic_Generic_tC1C606DD304D8B4975F7100A94883D309C89A3C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6846 = { sizeof (C_Int_t3809E5AEA51F61B739425178D50CB479D2481E0B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6847 = { sizeof (G_Int_t83ADF384081ED34E8132235BB2520189A5626291), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6848 = { sizeof (I_GO_tE66786805611B9866539EC70755473A6AD7D961C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6849 = { sizeof (GO_I_t9747DD551B9783018C8A9BCFBB45B0E0E739B958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6850 = { sizeof (S_GO_tC66061788F2FA9D3CDAE398F0CAC5E81BD4C6369), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6851 = { sizeof (GO_S_t82033CC353B905E23DF7E43F5B8E1FC929ADBC54), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6852 = { sizeof (S_Mat_tD5CCE3FD00251BA71449066AEE6E59387977C668), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6853 = { sizeof (Mat_S_tB6FD4BBFE1F09B0858D9CBFE77379B544084092A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6854 = { sizeof (S_AC_tC55B8F185DC515189810B1B8D114C456697FD3B5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6855 = { sizeof (AC_S_t83967940B4F21697907028815B64B5E0E7893239), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6856 = { sizeof (S_Sprite_tB14BA0C17CD89E7DDE240DA38384033787A9E422), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6857 = { sizeof (V3_Q_t334647532EC7E76E279772A7E1454D957F7E6FFD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6858 = { sizeof (Q_V3_tF2917CB976081AF5F996E700E7E8206C99F739DE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6859 = { sizeof (S_GenericDictionary_t13EDCAD9036EB9CEA44B16E4AF15DF661432493C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6860 = { sizeof (I_GenericDictionary_t16BC9886BA6F437767D8D7C0E3A17A82788BF0D6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6861 = { sizeof (Int_IntArray_t09AD7421347872BAAED8837E37A5018B662D23FD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6862 = { sizeof (Enum_String_t567A643096104B5698404A0754A1F42C7622AFBE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6863 = { sizeof (AdvanGeneric_String_tC1B14FFB5530AA2D7D67CF242DBBDA106722CDDB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6864 = { sizeof (NestedDB_tF5B5A4419E0610C07A505B4E3DCC42A54C212E14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6864[1] = 
{
	NestedDB_tF5B5A4419E0610C07A505B4E3DCC42A54C212E14::get_offset_of_nested_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6865 = { sizeof (Example_tE7A50FB4A9329B98CCC31246B2D66BC1D341AC84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6865[3] = 
{
	Example_tE7A50FB4A9329B98CCC31246B2D66BC1D341AC84::get_offset_of_id_0(),
	Example_tE7A50FB4A9329B98CCC31246B2D66BC1D341AC84::get_offset_of_enumVal_1(),
	Example_tE7A50FB4A9329B98CCC31246B2D66BC1D341AC84::get_offset_of_nestedData_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6866 = { sizeof (NestedExample_t7D65EF9A705ECA9B6097ED1276DBB49E72A4F3C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6866[4] = 
{
	NestedExample_t7D65EF9A705ECA9B6097ED1276DBB49E72A4F3C5::get_offset_of_prefab_0(),
	NestedExample_t7D65EF9A705ECA9B6097ED1276DBB49E72A4F3C5::get_offset_of_speed_1(),
	NestedExample_t7D65EF9A705ECA9B6097ED1276DBB49E72A4F3C5::get_offset_of_color_2(),
	NestedExample_t7D65EF9A705ECA9B6097ED1276DBB49E72A4F3C5::get_offset_of_deepNested_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6867 = { sizeof (MainDict_tABB8831EB5CF88193E3BF7F318A84053BFE42682), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6868 = { sizeof (NestedDict_t786E4BDEE0E5CB8B389F8248722E4854CFABCD18), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6869 = { sizeof (Nested2Dict_t92B279CB96D0DFAEBA143A7267846B5A04933628), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6870 = { sizeof (DrawKeyAsPropertyAttribute_t27D94881A50E07EE6B35F3E8743E632B8BC244C0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6871 = { sizeof (IDAttribute_tDC78DB59FF3B891950A3ECA925159AFEC3B7CB50), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6871[1] = 
{
	IDAttribute_tDC78DB59FF3B891950A3ECA925159AFEC3B7CB50::get_offset_of__id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6872 = { sizeof (RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6872[3] = 
{
	RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0::get_offset_of__gameObject_4(),
	RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0::get_offset_of__material_5(),
	RequiredReferences_tD502DA241623B0039A32F954E9BB8040D038F5B0::get_offset_of__audioClip_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6873 = { sizeof (ReorderableList_tE9342433971E09B8B941B8BDB654FBCACD8839D0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6874 = { sizeof (DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6874[3] = 
{
	DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F::get_offset_of_reorderableList_0(),
	DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F::get_offset_of_reqReferences_1(),
	DrawableDictionary_t5EE13D6B57AC0486747142D13BD761EE7C87F78F::get_offset_of_isExpanded_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6875 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6875[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6876 = { sizeof (JsonArray_t1A8231F65BC9EC3C1CD1FEA25DF8A205CF6400CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6877 = { sizeof (JsonObject_t70E68CAB3D5CC3117C77D86FE0A9D37E2BAD1C66), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6877[1] = 
{
	JsonObject_t70E68CAB3D5CC3117C77D86FE0A9D37E2BAD1C66::get_offset_of__members_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6878 = { sizeof (SimpleJson2_t485F125050CE1D1EA00641E8D922AC6E41EF1454), -1, sizeof(SimpleJson2_t485F125050CE1D1EA00641E8D922AC6E41EF1454_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6878[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SimpleJson2_t485F125050CE1D1EA00641E8D922AC6E41EF1454_StaticFields::get_offset_of_EscapeTable_13(),
	SimpleJson2_t485F125050CE1D1EA00641E8D922AC6E41EF1454_StaticFields::get_offset_of_EscapeCharacters_14(),
	SimpleJson2_t485F125050CE1D1EA00641E8D922AC6E41EF1454_StaticFields::get_offset_of__currentJsonSerializerStrategy_15(),
	SimpleJson2_t485F125050CE1D1EA00641E8D922AC6E41EF1454_StaticFields::get_offset_of__pocoJsonSerializerStrategy_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6879 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6880 = { sizeof (PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846), -1, sizeof(PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6880[6] = 
{
	PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846::get_offset_of_ConstructorCache_0(),
	PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846::get_offset_of_GetCache_1(),
	PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846::get_offset_of_SetCache_2(),
	PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846_StaticFields::get_offset_of_EmptyTypes_3(),
	PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846_StaticFields::get_offset_of_ArrayConstructorParameterTypes_4(),
	PocoJsonSerializerStrategy_t3F266902BA6143911D4174997CAD590053320846_StaticFields::get_offset_of_Iso8601Format_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6881 = { sizeof (ReflectionUtils_t0190AC5C7C75DB42BCBC2038C5752BAFBF80240F), -1, sizeof(ReflectionUtils_t0190AC5C7C75DB42BCBC2038C5752BAFBF80240F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6881[1] = 
{
	ReflectionUtils_t0190AC5C7C75DB42BCBC2038C5752BAFBF80240F_StaticFields::get_offset_of_EmptyObjects_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6882 = { sizeof (GetDelegate_tCBB4FA7AF97922763652DB4A13A5CC671C239658), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6883 = { sizeof (SetDelegate_tF514E819028ED8B328796D4A7B3841C57F7D16A2), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6884 = { sizeof (ConstructorDelegate_t530B58A311FEE546C679759CFE520EA0D0FDC761), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6885 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6886 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6887 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6887[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6888 = { sizeof (U3CU3Ec__DisplayClass25_0_tFDE962807434FD897F7BB134F96B3819ED072776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6888[1] = 
{
	U3CU3Ec__DisplayClass25_0_tFDE962807434FD897F7BB134F96B3819ED072776::get_offset_of_constructorInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6889 = { sizeof (U3CU3Ec__DisplayClass27_0_t96CB1C9ADB807F01DDEDCA4E7A248AB8FDD908E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6889[1] = 
{
	U3CU3Ec__DisplayClass27_0_t96CB1C9ADB807F01DDEDCA4E7A248AB8FDD908E1::get_offset_of_compiledLambda_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6890 = { sizeof (U3CU3Ec__DisplayClass31_0_tF06A6BF3C9672CD9EC0F4D0AB173152F0BAC2054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6890[1] = 
{
	U3CU3Ec__DisplayClass31_0_tF06A6BF3C9672CD9EC0F4D0AB173152F0BAC2054::get_offset_of_methodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6891 = { sizeof (U3CU3Ec__DisplayClass32_0_t328438FE1174FC2DECBEA48E033ADDD6572EE2C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6891[1] = 
{
	U3CU3Ec__DisplayClass32_0_t328438FE1174FC2DECBEA48E033ADDD6572EE2C8::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6892 = { sizeof (U3CU3Ec__DisplayClass33_0_t72A740B08AAD1E56D2483BB84B5D34A0723A8A7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6892[1] = 
{
	U3CU3Ec__DisplayClass33_0_t72A740B08AAD1E56D2483BB84B5D34A0723A8A7D::get_offset_of_compiled_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6893 = { sizeof (U3CU3Ec__DisplayClass34_0_t6CC93C129B6EE0711AF4C7F55B2C02D434DB44C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6893[1] = 
{
	U3CU3Ec__DisplayClass34_0_t6CC93C129B6EE0711AF4C7F55B2C02D434DB44C3::get_offset_of_compiled_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6894 = { sizeof (U3CU3Ec__DisplayClass37_0_t7C7C24CE855DC92C85BAB88B559F9EC2C091949C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6894[1] = 
{
	U3CU3Ec__DisplayClass37_0_t7C7C24CE855DC92C85BAB88B559F9EC2C091949C::get_offset_of_methodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6895 = { sizeof (U3CU3Ec__DisplayClass38_0_tCA4117735671192C5D0A92ED0548E1B9183FD4D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6895[1] = 
{
	U3CU3Ec__DisplayClass38_0_tCA4117735671192C5D0A92ED0548E1B9183FD4D3::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6896 = { sizeof (U3CU3Ec__DisplayClass39_0_tDAD1CC06874ABE9BCF34AD5FDCC08BC4FA801617), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6896[1] = 
{
	U3CU3Ec__DisplayClass39_0_tDAD1CC06874ABE9BCF34AD5FDCC08BC4FA801617::get_offset_of_compiled_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6897 = { sizeof (U3CU3Ec__DisplayClass40_0_t0847F22B61CB73CB40902CB2A1F0056CD08ACA41), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6897[1] = 
{
	U3CU3Ec__DisplayClass40_0_t0847F22B61CB73CB40902CB2A1F0056CD08ACA41::get_offset_of_compiled_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6898 = { sizeof (DefaultPlatform_tC9B39FE4FEC2AA4AE5C111BFCE1F9186C3B2296E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6899 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
