﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Org.BouncyCastle.Asn1.DerObjectIdentifier
struct DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441;
// Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier
struct AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB;
// Org.BouncyCastle.Asn1.X509.X509CertificateStructure[]
struct X509CertificateStructureU5BU5D_t059BC84686310964E2F9838556B39AD90F2370B5;
// Org.BouncyCastle.Crypto.Engines.ChaChaEngine
struct ChaChaEngine_t6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2;
// Org.BouncyCastle.Crypto.IAsymmetricBlockCipher
struct IAsymmetricBlockCipher_tA8433E11E9552E327EFAF108F2BDBA0A9F331B29;
// Org.BouncyCastle.Crypto.IBlockCipher
struct IBlockCipher_tB1BAE99FDCBFA6193F91C083240BC00B7D01A612;
// Org.BouncyCastle.Crypto.ICipherParameters
struct ICipherParameters_t19951AB9402D3DD0E1710E31487DB4777DB963C3;
// Org.BouncyCastle.Crypto.IDigest
struct IDigest_t278A7956CB2374197EEBBC1414ACC3EFA4656221;
// Org.BouncyCastle.Crypto.IDsa
struct IDsa_tF3F4E4CFD289CA59E03DAFF520D46D2FB9FB40A5;
// Org.BouncyCastle.Crypto.ISigner
struct ISigner_tEF8DF915B9D0C94203444344724E4456520DB11A;
// Org.BouncyCastle.Crypto.Macs.HMac
struct HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB;
// Org.BouncyCastle.Crypto.Modes.Gcm.IGcmExponentiator
struct IGcmExponentiator_t6CC947C57FD1424298BDA1F8AD1468B685A51E0D;
// Org.BouncyCastle.Crypto.Modes.Gcm.IGcmMultiplier
struct IGcmMultiplier_t97CAE1734613476B7760FE054DC13BF8079FD99A;
// Org.BouncyCastle.Crypto.Modes.IAeadBlockCipher
struct IAeadBlockCipher_t9668BFF2EE71E3575C32A8F7675CF59D99235F19;
// Org.BouncyCastle.Crypto.Paddings.IBlockCipherPadding
struct IBlockCipherPadding_tAF4A890B5F9762C71A810D5E7E2E151573CC5828;
// Org.BouncyCastle.Crypto.Parameters.DHKeyGenerationParameters
struct DHKeyGenerationParameters_t7EFE3D05B481CE4EC6E8B4C9E5EF13E970B64078;
// Org.BouncyCastle.Crypto.Parameters.DHParameters
struct DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513;
// Org.BouncyCastle.Crypto.Parameters.DHPublicKeyParameters
struct DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA;
// Org.BouncyCastle.Crypto.Parameters.DHValidationParameters
struct DHValidationParameters_tC30183B3C1E3CAF234F646697CED1210A4EF4C0E;
// Org.BouncyCastle.Crypto.Parameters.DsaKeyParameters
struct DsaKeyParameters_tBF3E3B9B02243E5C768006D70F4CA496F15E82A6;
// Org.BouncyCastle.Crypto.Parameters.DsaParameters
struct DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807;
// Org.BouncyCastle.Crypto.Parameters.DsaValidationParameters
struct DsaValidationParameters_t2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76;
// Org.BouncyCastle.Crypto.Parameters.ECDomainParameters
struct ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57;
// Org.BouncyCastle.Crypto.Parameters.ECKeyParameters
struct ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1;
// Org.BouncyCastle.Crypto.Parameters.ElGamalParameters
struct ElGamalParameters_tC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA;
// Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters
struct Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD;
// Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters
struct Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98;
// Org.BouncyCastle.Crypto.Parameters.KeyParameter
struct KeyParameter_t969556DA4FAE52672608FEC8F8E2D4E4EC8337A2;
// Org.BouncyCastle.Crypto.Prng.IRandomGenerator
struct IRandomGenerator_tEFF6415248F4D37A24017AB5D51691A43740A6CB;
// Org.BouncyCastle.Crypto.Signers.IDsaKCalculator
struct IDsaKCalculator_tE52F2FE359FB784014850F288D56B2D858D1F560;
// Org.BouncyCastle.Crypto.Tls.Certificate
struct Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D;
// Org.BouncyCastle.Crypto.Tls.DigestInputBuffer
struct DigestInputBuffer_t117CEE4FCA532842726AF5BFE55FFDFC708B811D;
// Org.BouncyCastle.Crypto.Tls.ProtocolVersion
struct ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D;
// Org.BouncyCastle.Crypto.Tls.SecurityParameters
struct SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB;
// Org.BouncyCastle.Crypto.Tls.SignatureAndHashAlgorithm
struct SignatureAndHashAlgorithm_tDEA4E53D4DC207FA196758B3B4848104689854A9;
// Org.BouncyCastle.Crypto.Tls.TlsCipher
struct TlsCipher_tBFB157BEEF09304C48ED8C5D3BE67DAADDE424BE;
// Org.BouncyCastle.Crypto.Tls.TlsCipherFactory
struct TlsCipherFactory_t5995BB8E2767BA0519D3BA1629F238C499C47DE9;
// Org.BouncyCastle.Crypto.Tls.TlsClientContext
struct TlsClientContext_tE01FA93AE9165EE22FA37C2E6B3C32E99C17B989;
// Org.BouncyCastle.Crypto.Tls.TlsCompression
struct TlsCompression_t1B5FC096322AE68CF5FCC22393F25D2E6673F663;
// Org.BouncyCastle.Crypto.Tls.TlsContext
struct TlsContext_tF27CFFAEDC150666C5502AF25C2966952F7EEAE6;
// Org.BouncyCastle.Crypto.Tls.TlsHandshakeHash
struct TlsHandshakeHash_t79E88EE4FDA7FEB109B6FEC35D6E58E595BE2098;
// Org.BouncyCastle.Crypto.Tls.TlsProtocol
struct TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931;
// Org.BouncyCastle.Crypto.Tls.TlsSession
struct TlsSession_t7F9585B1263657CC2C8F2CDDE578D0D14C15C079;
// Org.BouncyCastle.Math.BigInteger
struct BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91;
// Org.BouncyCastle.Math.EC.ECCurve
struct ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95;
// Org.BouncyCastle.Math.EC.ECPoint
struct ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399;
// Org.BouncyCastle.Security.SecureRandom
struct SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0;
// Org.BouncyCastle.Utilities.IMemoable
struct IMemoable_t7B177E682B40D3E92A4513BBDA828B4769F556AB;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IList
struct IList_tA637AB426E16F84F84ACC2813BDCF3A0414AF0AA;
// System.IO.MemoryStream
struct MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.UInt32[][][]
struct UInt32U5BU5DU5BU5DU5BU5D_t14BCCFDA712528AB5C1A9FAD2BCEC1CE47F44D23;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASYMMETRICKEYPARAMETER_TD6918D64A0FB774AF79E386A85415CE53D5EC0D7_H
#define ASYMMETRICKEYPARAMETER_TD6918D64A0FB774AF79E386A85415CE53D5EC0D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.AsymmetricKeyParameter
struct  AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7  : public RuntimeObject
{
public:
	// System.Boolean Org.BouncyCastle.Crypto.AsymmetricKeyParameter::privateKey
	bool ___privateKey_0;

public:
	inline static int32_t get_offset_of_privateKey_0() { return static_cast<int32_t>(offsetof(AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7, ___privateKey_0)); }
	inline bool get_privateKey_0() const { return ___privateKey_0; }
	inline bool* get_address_of_privateKey_0() { return &___privateKey_0; }
	inline void set_privateKey_0(bool value)
	{
		___privateKey_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICKEYPARAMETER_TD6918D64A0FB774AF79E386A85415CE53D5EC0D7_H
#ifndef CIPHERKEYGENERATOR_TB9597B68C6815C63C622AA3320A0E81BF8568CE1_H
#define CIPHERKEYGENERATOR_TB9597B68C6815C63C622AA3320A0E81BF8568CE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.CipherKeyGenerator
struct  CipherKeyGenerator_tB9597B68C6815C63C622AA3320A0E81BF8568CE1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERKEYGENERATOR_TB9597B68C6815C63C622AA3320A0E81BF8568CE1_H
#ifndef SALSA20ENGINE_T5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_H
#define SALSA20ENGINE_T5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Engines.Salsa20Engine
struct  Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Engines.Salsa20Engine::rounds
	int32_t ___rounds_3;
	// System.Int32 Org.BouncyCastle.Crypto.Engines.Salsa20Engine::index
	int32_t ___index_4;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.Salsa20Engine::engineState
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___engineState_5;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.Salsa20Engine::x
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___x_6;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Salsa20Engine::keyStream
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___keyStream_7;
	// System.Boolean Org.BouncyCastle.Crypto.Engines.Salsa20Engine::initialised
	bool ___initialised_8;
	// System.UInt32 Org.BouncyCastle.Crypto.Engines.Salsa20Engine::cW0
	uint32_t ___cW0_9;
	// System.UInt32 Org.BouncyCastle.Crypto.Engines.Salsa20Engine::cW1
	uint32_t ___cW1_10;
	// System.UInt32 Org.BouncyCastle.Crypto.Engines.Salsa20Engine::cW2
	uint32_t ___cW2_11;

public:
	inline static int32_t get_offset_of_rounds_3() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___rounds_3)); }
	inline int32_t get_rounds_3() const { return ___rounds_3; }
	inline int32_t* get_address_of_rounds_3() { return &___rounds_3; }
	inline void set_rounds_3(int32_t value)
	{
		___rounds_3 = value;
	}

	inline static int32_t get_offset_of_index_4() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___index_4)); }
	inline int32_t get_index_4() const { return ___index_4; }
	inline int32_t* get_address_of_index_4() { return &___index_4; }
	inline void set_index_4(int32_t value)
	{
		___index_4 = value;
	}

	inline static int32_t get_offset_of_engineState_5() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___engineState_5)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_engineState_5() const { return ___engineState_5; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_engineState_5() { return &___engineState_5; }
	inline void set_engineState_5(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___engineState_5 = value;
		Il2CppCodeGenWriteBarrier((&___engineState_5), value);
	}

	inline static int32_t get_offset_of_x_6() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___x_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_x_6() const { return ___x_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_x_6() { return &___x_6; }
	inline void set_x_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___x_6 = value;
		Il2CppCodeGenWriteBarrier((&___x_6), value);
	}

	inline static int32_t get_offset_of_keyStream_7() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___keyStream_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_keyStream_7() const { return ___keyStream_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_keyStream_7() { return &___keyStream_7; }
	inline void set_keyStream_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___keyStream_7 = value;
		Il2CppCodeGenWriteBarrier((&___keyStream_7), value);
	}

	inline static int32_t get_offset_of_initialised_8() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___initialised_8)); }
	inline bool get_initialised_8() const { return ___initialised_8; }
	inline bool* get_address_of_initialised_8() { return &___initialised_8; }
	inline void set_initialised_8(bool value)
	{
		___initialised_8 = value;
	}

	inline static int32_t get_offset_of_cW0_9() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___cW0_9)); }
	inline uint32_t get_cW0_9() const { return ___cW0_9; }
	inline uint32_t* get_address_of_cW0_9() { return &___cW0_9; }
	inline void set_cW0_9(uint32_t value)
	{
		___cW0_9 = value;
	}

	inline static int32_t get_offset_of_cW1_10() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___cW1_10)); }
	inline uint32_t get_cW1_10() const { return ___cW1_10; }
	inline uint32_t* get_address_of_cW1_10() { return &___cW1_10; }
	inline void set_cW1_10(uint32_t value)
	{
		___cW1_10 = value;
	}

	inline static int32_t get_offset_of_cW2_11() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C, ___cW2_11)); }
	inline uint32_t get_cW2_11() const { return ___cW2_11; }
	inline uint32_t* get_address_of_cW2_11() { return &___cW2_11; }
	inline void set_cW2_11(uint32_t value)
	{
		___cW2_11 = value;
	}
};

struct Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_StaticFields
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Engines.Salsa20Engine::DEFAULT_ROUNDS
	int32_t ___DEFAULT_ROUNDS_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Salsa20Engine::sigma
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sigma_1;
	// System.Byte[] Org.BouncyCastle.Crypto.Engines.Salsa20Engine::tau
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___tau_2;

public:
	inline static int32_t get_offset_of_DEFAULT_ROUNDS_0() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_StaticFields, ___DEFAULT_ROUNDS_0)); }
	inline int32_t get_DEFAULT_ROUNDS_0() const { return ___DEFAULT_ROUNDS_0; }
	inline int32_t* get_address_of_DEFAULT_ROUNDS_0() { return &___DEFAULT_ROUNDS_0; }
	inline void set_DEFAULT_ROUNDS_0(int32_t value)
	{
		___DEFAULT_ROUNDS_0 = value;
	}

	inline static int32_t get_offset_of_sigma_1() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_StaticFields, ___sigma_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sigma_1() const { return ___sigma_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sigma_1() { return &___sigma_1; }
	inline void set_sigma_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sigma_1 = value;
		Il2CppCodeGenWriteBarrier((&___sigma_1), value);
	}

	inline static int32_t get_offset_of_tau_2() { return static_cast<int32_t>(offsetof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_StaticFields, ___tau_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_tau_2() const { return ___tau_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_tau_2() { return &___tau_2; }
	inline void set_tau_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___tau_2 = value;
		Il2CppCodeGenWriteBarrier((&___tau_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SALSA20ENGINE_T5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_H
#ifndef SEEDENGINE_TCB01373AD403AC5EC3DA75A5B81087253068C62A_H
#define SEEDENGINE_TCB01373AD403AC5EC3DA75A5B81087253068C62A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Engines.SeedEngine
struct  SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A  : public RuntimeObject
{
public:
	// System.Int32[] Org.BouncyCastle.Crypto.Engines.SeedEngine::wKey
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___wKey_5;
	// System.Boolean Org.BouncyCastle.Crypto.Engines.SeedEngine::forEncryption
	bool ___forEncryption_6;

public:
	inline static int32_t get_offset_of_wKey_5() { return static_cast<int32_t>(offsetof(SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A, ___wKey_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_wKey_5() const { return ___wKey_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_wKey_5() { return &___wKey_5; }
	inline void set_wKey_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___wKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___wKey_5), value);
	}

	inline static int32_t get_offset_of_forEncryption_6() { return static_cast<int32_t>(offsetof(SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A, ___forEncryption_6)); }
	inline bool get_forEncryption_6() const { return ___forEncryption_6; }
	inline bool* get_address_of_forEncryption_6() { return &___forEncryption_6; }
	inline void set_forEncryption_6(bool value)
	{
		___forEncryption_6 = value;
	}
};

struct SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.SeedEngine::SS0
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SS0_0;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.SeedEngine::SS1
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SS1_1;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.SeedEngine::SS2
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SS2_2;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.SeedEngine::SS3
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___SS3_3;
	// System.UInt32[] Org.BouncyCastle.Crypto.Engines.SeedEngine::KC
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___KC_4;

public:
	inline static int32_t get_offset_of_SS0_0() { return static_cast<int32_t>(offsetof(SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A_StaticFields, ___SS0_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SS0_0() const { return ___SS0_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SS0_0() { return &___SS0_0; }
	inline void set_SS0_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SS0_0 = value;
		Il2CppCodeGenWriteBarrier((&___SS0_0), value);
	}

	inline static int32_t get_offset_of_SS1_1() { return static_cast<int32_t>(offsetof(SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A_StaticFields, ___SS1_1)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SS1_1() const { return ___SS1_1; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SS1_1() { return &___SS1_1; }
	inline void set_SS1_1(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SS1_1 = value;
		Il2CppCodeGenWriteBarrier((&___SS1_1), value);
	}

	inline static int32_t get_offset_of_SS2_2() { return static_cast<int32_t>(offsetof(SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A_StaticFields, ___SS2_2)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SS2_2() const { return ___SS2_2; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SS2_2() { return &___SS2_2; }
	inline void set_SS2_2(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SS2_2 = value;
		Il2CppCodeGenWriteBarrier((&___SS2_2), value);
	}

	inline static int32_t get_offset_of_SS3_3() { return static_cast<int32_t>(offsetof(SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A_StaticFields, ___SS3_3)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_SS3_3() const { return ___SS3_3; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_SS3_3() { return &___SS3_3; }
	inline void set_SS3_3(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___SS3_3 = value;
		Il2CppCodeGenWriteBarrier((&___SS3_3), value);
	}

	inline static int32_t get_offset_of_KC_4() { return static_cast<int32_t>(offsetof(SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A_StaticFields, ___KC_4)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_KC_4() const { return ___KC_4; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_KC_4() { return &___KC_4; }
	inline void set_KC_4(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___KC_4 = value;
		Il2CppCodeGenWriteBarrier((&___KC_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEEDENGINE_TCB01373AD403AC5EC3DA75A5B81087253068C62A_H
#ifndef DHBASICKEYPAIRGENERATOR_T8665F4F588356BDFED1CE9232B45BBEAEE2688AD_H
#define DHBASICKEYPAIRGENERATOR_T8665F4F588356BDFED1CE9232B45BBEAEE2688AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Generators.DHBasicKeyPairGenerator
struct  DHBasicKeyPairGenerator_t8665F4F588356BDFED1CE9232B45BBEAEE2688AD  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Parameters.DHKeyGenerationParameters Org.BouncyCastle.Crypto.Generators.DHBasicKeyPairGenerator::param
	DHKeyGenerationParameters_t7EFE3D05B481CE4EC6E8B4C9E5EF13E970B64078 * ___param_0;

public:
	inline static int32_t get_offset_of_param_0() { return static_cast<int32_t>(offsetof(DHBasicKeyPairGenerator_t8665F4F588356BDFED1CE9232B45BBEAEE2688AD, ___param_0)); }
	inline DHKeyGenerationParameters_t7EFE3D05B481CE4EC6E8B4C9E5EF13E970B64078 * get_param_0() const { return ___param_0; }
	inline DHKeyGenerationParameters_t7EFE3D05B481CE4EC6E8B4C9E5EF13E970B64078 ** get_address_of_param_0() { return &___param_0; }
	inline void set_param_0(DHKeyGenerationParameters_t7EFE3D05B481CE4EC6E8B4C9E5EF13E970B64078 * value)
	{
		___param_0 = value;
		Il2CppCodeGenWriteBarrier((&___param_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHBASICKEYPAIRGENERATOR_T8665F4F588356BDFED1CE9232B45BBEAEE2688AD_H
#ifndef DHKEYGENERATORHELPER_T53E0C55FAA562861BAF57BA31599D85D6F8AD7D6_H
#define DHKEYGENERATORHELPER_T53E0C55FAA562861BAF57BA31599D85D6F8AD7D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Generators.DHKeyGeneratorHelper
struct  DHKeyGeneratorHelper_t53E0C55FAA562861BAF57BA31599D85D6F8AD7D6  : public RuntimeObject
{
public:

public:
};

struct DHKeyGeneratorHelper_t53E0C55FAA562861BAF57BA31599D85D6F8AD7D6_StaticFields
{
public:
	// Org.BouncyCastle.Crypto.Generators.DHKeyGeneratorHelper Org.BouncyCastle.Crypto.Generators.DHKeyGeneratorHelper::Instance
	DHKeyGeneratorHelper_t53E0C55FAA562861BAF57BA31599D85D6F8AD7D6 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DHKeyGeneratorHelper_t53E0C55FAA562861BAF57BA31599D85D6F8AD7D6_StaticFields, ___Instance_0)); }
	inline DHKeyGeneratorHelper_t53E0C55FAA562861BAF57BA31599D85D6F8AD7D6 * get_Instance_0() const { return ___Instance_0; }
	inline DHKeyGeneratorHelper_t53E0C55FAA562861BAF57BA31599D85D6F8AD7D6 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(DHKeyGeneratorHelper_t53E0C55FAA562861BAF57BA31599D85D6F8AD7D6 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHKEYGENERATORHELPER_T53E0C55FAA562861BAF57BA31599D85D6F8AD7D6_H
#ifndef ECKEYPAIRGENERATOR_TEF1C73B021080F994111392005EA8DAF6BB1D36F_H
#define ECKEYPAIRGENERATOR_TEF1C73B021080F994111392005EA8DAF6BB1D36F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Generators.ECKeyPairGenerator
struct  ECKeyPairGenerator_tEF1C73B021080F994111392005EA8DAF6BB1D36F  : public RuntimeObject
{
public:
	// System.String Org.BouncyCastle.Crypto.Generators.ECKeyPairGenerator::algorithm
	String_t* ___algorithm_0;
	// Org.BouncyCastle.Crypto.Parameters.ECDomainParameters Org.BouncyCastle.Crypto.Generators.ECKeyPairGenerator::parameters
	ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57 * ___parameters_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Crypto.Generators.ECKeyPairGenerator::publicKeyParamSet
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___publicKeyParamSet_2;
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Crypto.Generators.ECKeyPairGenerator::random
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___random_3;

public:
	inline static int32_t get_offset_of_algorithm_0() { return static_cast<int32_t>(offsetof(ECKeyPairGenerator_tEF1C73B021080F994111392005EA8DAF6BB1D36F, ___algorithm_0)); }
	inline String_t* get_algorithm_0() const { return ___algorithm_0; }
	inline String_t** get_address_of_algorithm_0() { return &___algorithm_0; }
	inline void set_algorithm_0(String_t* value)
	{
		___algorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(ECKeyPairGenerator_tEF1C73B021080F994111392005EA8DAF6BB1D36F, ___parameters_1)); }
	inline ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57 * get_parameters_1() const { return ___parameters_1; }
	inline ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}

	inline static int32_t get_offset_of_publicKeyParamSet_2() { return static_cast<int32_t>(offsetof(ECKeyPairGenerator_tEF1C73B021080F994111392005EA8DAF6BB1D36F, ___publicKeyParamSet_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_publicKeyParamSet_2() const { return ___publicKeyParamSet_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_publicKeyParamSet_2() { return &___publicKeyParamSet_2; }
	inline void set_publicKeyParamSet_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___publicKeyParamSet_2 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyParamSet_2), value);
	}

	inline static int32_t get_offset_of_random_3() { return static_cast<int32_t>(offsetof(ECKeyPairGenerator_tEF1C73B021080F994111392005EA8DAF6BB1D36F, ___random_3)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_random_3() const { return ___random_3; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_random_3() { return &___random_3; }
	inline void set_random_3(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___random_3 = value;
		Il2CppCodeGenWriteBarrier((&___random_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECKEYPAIRGENERATOR_TEF1C73B021080F994111392005EA8DAF6BB1D36F_H
#ifndef KEYGENERATIONPARAMETERS_TDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6_H
#define KEYGENERATIONPARAMETERS_TDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.KeyGenerationParameters
struct  KeyGenerationParameters_tDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Crypto.KeyGenerationParameters::random
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___random_0;
	// System.Int32 Org.BouncyCastle.Crypto.KeyGenerationParameters::strength
	int32_t ___strength_1;

public:
	inline static int32_t get_offset_of_random_0() { return static_cast<int32_t>(offsetof(KeyGenerationParameters_tDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6, ___random_0)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_random_0() const { return ___random_0; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_random_0() { return &___random_0; }
	inline void set_random_0(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___random_0 = value;
		Il2CppCodeGenWriteBarrier((&___random_0), value);
	}

	inline static int32_t get_offset_of_strength_1() { return static_cast<int32_t>(offsetof(KeyGenerationParameters_tDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6, ___strength_1)); }
	inline int32_t get_strength_1() const { return ___strength_1; }
	inline int32_t* get_address_of_strength_1() { return &___strength_1; }
	inline void set_strength_1(int32_t value)
	{
		___strength_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYGENERATIONPARAMETERS_TDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6_H
#ifndef CBCBLOCKCIPHERMAC_TF3EEE86C592D72AAB46685074C00F63508623048_H
#define CBCBLOCKCIPHERMAC_TF3EEE86C592D72AAB46685074C00F63508623048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Macs.CbcBlockCipherMac
struct  CbcBlockCipherMac_tF3EEE86C592D72AAB46685074C00F63508623048  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Macs.CbcBlockCipherMac::buf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___buf_0;
	// System.Int32 Org.BouncyCastle.Crypto.Macs.CbcBlockCipherMac::bufOff
	int32_t ___bufOff_1;
	// Org.BouncyCastle.Crypto.IBlockCipher Org.BouncyCastle.Crypto.Macs.CbcBlockCipherMac::cipher
	RuntimeObject* ___cipher_2;
	// Org.BouncyCastle.Crypto.Paddings.IBlockCipherPadding Org.BouncyCastle.Crypto.Macs.CbcBlockCipherMac::padding
	RuntimeObject* ___padding_3;
	// System.Int32 Org.BouncyCastle.Crypto.Macs.CbcBlockCipherMac::macSize
	int32_t ___macSize_4;

public:
	inline static int32_t get_offset_of_buf_0() { return static_cast<int32_t>(offsetof(CbcBlockCipherMac_tF3EEE86C592D72AAB46685074C00F63508623048, ___buf_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_buf_0() const { return ___buf_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_buf_0() { return &___buf_0; }
	inline void set_buf_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___buf_0 = value;
		Il2CppCodeGenWriteBarrier((&___buf_0), value);
	}

	inline static int32_t get_offset_of_bufOff_1() { return static_cast<int32_t>(offsetof(CbcBlockCipherMac_tF3EEE86C592D72AAB46685074C00F63508623048, ___bufOff_1)); }
	inline int32_t get_bufOff_1() const { return ___bufOff_1; }
	inline int32_t* get_address_of_bufOff_1() { return &___bufOff_1; }
	inline void set_bufOff_1(int32_t value)
	{
		___bufOff_1 = value;
	}

	inline static int32_t get_offset_of_cipher_2() { return static_cast<int32_t>(offsetof(CbcBlockCipherMac_tF3EEE86C592D72AAB46685074C00F63508623048, ___cipher_2)); }
	inline RuntimeObject* get_cipher_2() const { return ___cipher_2; }
	inline RuntimeObject** get_address_of_cipher_2() { return &___cipher_2; }
	inline void set_cipher_2(RuntimeObject* value)
	{
		___cipher_2 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_2), value);
	}

	inline static int32_t get_offset_of_padding_3() { return static_cast<int32_t>(offsetof(CbcBlockCipherMac_tF3EEE86C592D72AAB46685074C00F63508623048, ___padding_3)); }
	inline RuntimeObject* get_padding_3() const { return ___padding_3; }
	inline RuntimeObject** get_address_of_padding_3() { return &___padding_3; }
	inline void set_padding_3(RuntimeObject* value)
	{
		___padding_3 = value;
		Il2CppCodeGenWriteBarrier((&___padding_3), value);
	}

	inline static int32_t get_offset_of_macSize_4() { return static_cast<int32_t>(offsetof(CbcBlockCipherMac_tF3EEE86C592D72AAB46685074C00F63508623048, ___macSize_4)); }
	inline int32_t get_macSize_4() const { return ___macSize_4; }
	inline int32_t* get_address_of_macSize_4() { return &___macSize_4; }
	inline void set_macSize_4(int32_t value)
	{
		___macSize_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBCBLOCKCIPHERMAC_TF3EEE86C592D72AAB46685074C00F63508623048_H
#ifndef HMAC_T65D9921BAB421BEF7231F6BE59594F451DC4DFEB_H
#define HMAC_T65D9921BAB421BEF7231F6BE59594F451DC4DFEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Macs.HMac
struct  HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.IDigest Org.BouncyCastle.Crypto.Macs.HMac::digest
	RuntimeObject* ___digest_0;
	// System.Int32 Org.BouncyCastle.Crypto.Macs.HMac::digestSize
	int32_t ___digestSize_1;
	// System.Int32 Org.BouncyCastle.Crypto.Macs.HMac::blockLength
	int32_t ___blockLength_2;
	// Org.BouncyCastle.Utilities.IMemoable Org.BouncyCastle.Crypto.Macs.HMac::ipadState
	RuntimeObject* ___ipadState_3;
	// Org.BouncyCastle.Utilities.IMemoable Org.BouncyCastle.Crypto.Macs.HMac::opadState
	RuntimeObject* ___opadState_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Macs.HMac::inputPad
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___inputPad_5;
	// System.Byte[] Org.BouncyCastle.Crypto.Macs.HMac::outputBuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___outputBuf_6;

public:
	inline static int32_t get_offset_of_digest_0() { return static_cast<int32_t>(offsetof(HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB, ___digest_0)); }
	inline RuntimeObject* get_digest_0() const { return ___digest_0; }
	inline RuntimeObject** get_address_of_digest_0() { return &___digest_0; }
	inline void set_digest_0(RuntimeObject* value)
	{
		___digest_0 = value;
		Il2CppCodeGenWriteBarrier((&___digest_0), value);
	}

	inline static int32_t get_offset_of_digestSize_1() { return static_cast<int32_t>(offsetof(HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB, ___digestSize_1)); }
	inline int32_t get_digestSize_1() const { return ___digestSize_1; }
	inline int32_t* get_address_of_digestSize_1() { return &___digestSize_1; }
	inline void set_digestSize_1(int32_t value)
	{
		___digestSize_1 = value;
	}

	inline static int32_t get_offset_of_blockLength_2() { return static_cast<int32_t>(offsetof(HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB, ___blockLength_2)); }
	inline int32_t get_blockLength_2() const { return ___blockLength_2; }
	inline int32_t* get_address_of_blockLength_2() { return &___blockLength_2; }
	inline void set_blockLength_2(int32_t value)
	{
		___blockLength_2 = value;
	}

	inline static int32_t get_offset_of_ipadState_3() { return static_cast<int32_t>(offsetof(HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB, ___ipadState_3)); }
	inline RuntimeObject* get_ipadState_3() const { return ___ipadState_3; }
	inline RuntimeObject** get_address_of_ipadState_3() { return &___ipadState_3; }
	inline void set_ipadState_3(RuntimeObject* value)
	{
		___ipadState_3 = value;
		Il2CppCodeGenWriteBarrier((&___ipadState_3), value);
	}

	inline static int32_t get_offset_of_opadState_4() { return static_cast<int32_t>(offsetof(HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB, ___opadState_4)); }
	inline RuntimeObject* get_opadState_4() const { return ___opadState_4; }
	inline RuntimeObject** get_address_of_opadState_4() { return &___opadState_4; }
	inline void set_opadState_4(RuntimeObject* value)
	{
		___opadState_4 = value;
		Il2CppCodeGenWriteBarrier((&___opadState_4), value);
	}

	inline static int32_t get_offset_of_inputPad_5() { return static_cast<int32_t>(offsetof(HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB, ___inputPad_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_inputPad_5() const { return ___inputPad_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_inputPad_5() { return &___inputPad_5; }
	inline void set_inputPad_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___inputPad_5 = value;
		Il2CppCodeGenWriteBarrier((&___inputPad_5), value);
	}

	inline static int32_t get_offset_of_outputBuf_6() { return static_cast<int32_t>(offsetof(HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB, ___outputBuf_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_outputBuf_6() const { return ___outputBuf_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_outputBuf_6() { return &___outputBuf_6; }
	inline void set_outputBuf_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___outputBuf_6 = value;
		Il2CppCodeGenWriteBarrier((&___outputBuf_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMAC_T65D9921BAB421BEF7231F6BE59594F451DC4DFEB_H
#ifndef POLY1305_TA7E692335B023AF877C210D99A953921FA39A98E_H
#define POLY1305_TA7E692335B023AF877C210D99A953921FA39A98E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Macs.Poly1305
struct  Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.IBlockCipher Org.BouncyCastle.Crypto.Macs.Poly1305::cipher
	RuntimeObject* ___cipher_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Macs.Poly1305::singleByte
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___singleByte_1;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::r0
	uint32_t ___r0_2;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::r1
	uint32_t ___r1_3;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::r2
	uint32_t ___r2_4;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::r3
	uint32_t ___r3_5;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::r4
	uint32_t ___r4_6;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::s1
	uint32_t ___s1_7;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::s2
	uint32_t ___s2_8;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::s3
	uint32_t ___s3_9;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::s4
	uint32_t ___s4_10;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::k0
	uint32_t ___k0_11;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::k1
	uint32_t ___k1_12;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::k2
	uint32_t ___k2_13;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::k3
	uint32_t ___k3_14;
	// System.Byte[] Org.BouncyCastle.Crypto.Macs.Poly1305::currentBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___currentBlock_15;
	// System.Int32 Org.BouncyCastle.Crypto.Macs.Poly1305::currentBlockOffset
	int32_t ___currentBlockOffset_16;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::h0
	uint32_t ___h0_17;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::h1
	uint32_t ___h1_18;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::h2
	uint32_t ___h2_19;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::h3
	uint32_t ___h3_20;
	// System.UInt32 Org.BouncyCastle.Crypto.Macs.Poly1305::h4
	uint32_t ___h4_21;

public:
	inline static int32_t get_offset_of_cipher_0() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___cipher_0)); }
	inline RuntimeObject* get_cipher_0() const { return ___cipher_0; }
	inline RuntimeObject** get_address_of_cipher_0() { return &___cipher_0; }
	inline void set_cipher_0(RuntimeObject* value)
	{
		___cipher_0 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_0), value);
	}

	inline static int32_t get_offset_of_singleByte_1() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___singleByte_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_singleByte_1() const { return ___singleByte_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_singleByte_1() { return &___singleByte_1; }
	inline void set_singleByte_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___singleByte_1 = value;
		Il2CppCodeGenWriteBarrier((&___singleByte_1), value);
	}

	inline static int32_t get_offset_of_r0_2() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___r0_2)); }
	inline uint32_t get_r0_2() const { return ___r0_2; }
	inline uint32_t* get_address_of_r0_2() { return &___r0_2; }
	inline void set_r0_2(uint32_t value)
	{
		___r0_2 = value;
	}

	inline static int32_t get_offset_of_r1_3() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___r1_3)); }
	inline uint32_t get_r1_3() const { return ___r1_3; }
	inline uint32_t* get_address_of_r1_3() { return &___r1_3; }
	inline void set_r1_3(uint32_t value)
	{
		___r1_3 = value;
	}

	inline static int32_t get_offset_of_r2_4() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___r2_4)); }
	inline uint32_t get_r2_4() const { return ___r2_4; }
	inline uint32_t* get_address_of_r2_4() { return &___r2_4; }
	inline void set_r2_4(uint32_t value)
	{
		___r2_4 = value;
	}

	inline static int32_t get_offset_of_r3_5() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___r3_5)); }
	inline uint32_t get_r3_5() const { return ___r3_5; }
	inline uint32_t* get_address_of_r3_5() { return &___r3_5; }
	inline void set_r3_5(uint32_t value)
	{
		___r3_5 = value;
	}

	inline static int32_t get_offset_of_r4_6() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___r4_6)); }
	inline uint32_t get_r4_6() const { return ___r4_6; }
	inline uint32_t* get_address_of_r4_6() { return &___r4_6; }
	inline void set_r4_6(uint32_t value)
	{
		___r4_6 = value;
	}

	inline static int32_t get_offset_of_s1_7() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___s1_7)); }
	inline uint32_t get_s1_7() const { return ___s1_7; }
	inline uint32_t* get_address_of_s1_7() { return &___s1_7; }
	inline void set_s1_7(uint32_t value)
	{
		___s1_7 = value;
	}

	inline static int32_t get_offset_of_s2_8() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___s2_8)); }
	inline uint32_t get_s2_8() const { return ___s2_8; }
	inline uint32_t* get_address_of_s2_8() { return &___s2_8; }
	inline void set_s2_8(uint32_t value)
	{
		___s2_8 = value;
	}

	inline static int32_t get_offset_of_s3_9() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___s3_9)); }
	inline uint32_t get_s3_9() const { return ___s3_9; }
	inline uint32_t* get_address_of_s3_9() { return &___s3_9; }
	inline void set_s3_9(uint32_t value)
	{
		___s3_9 = value;
	}

	inline static int32_t get_offset_of_s4_10() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___s4_10)); }
	inline uint32_t get_s4_10() const { return ___s4_10; }
	inline uint32_t* get_address_of_s4_10() { return &___s4_10; }
	inline void set_s4_10(uint32_t value)
	{
		___s4_10 = value;
	}

	inline static int32_t get_offset_of_k0_11() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___k0_11)); }
	inline uint32_t get_k0_11() const { return ___k0_11; }
	inline uint32_t* get_address_of_k0_11() { return &___k0_11; }
	inline void set_k0_11(uint32_t value)
	{
		___k0_11 = value;
	}

	inline static int32_t get_offset_of_k1_12() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___k1_12)); }
	inline uint32_t get_k1_12() const { return ___k1_12; }
	inline uint32_t* get_address_of_k1_12() { return &___k1_12; }
	inline void set_k1_12(uint32_t value)
	{
		___k1_12 = value;
	}

	inline static int32_t get_offset_of_k2_13() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___k2_13)); }
	inline uint32_t get_k2_13() const { return ___k2_13; }
	inline uint32_t* get_address_of_k2_13() { return &___k2_13; }
	inline void set_k2_13(uint32_t value)
	{
		___k2_13 = value;
	}

	inline static int32_t get_offset_of_k3_14() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___k3_14)); }
	inline uint32_t get_k3_14() const { return ___k3_14; }
	inline uint32_t* get_address_of_k3_14() { return &___k3_14; }
	inline void set_k3_14(uint32_t value)
	{
		___k3_14 = value;
	}

	inline static int32_t get_offset_of_currentBlock_15() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___currentBlock_15)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_currentBlock_15() const { return ___currentBlock_15; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_currentBlock_15() { return &___currentBlock_15; }
	inline void set_currentBlock_15(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___currentBlock_15 = value;
		Il2CppCodeGenWriteBarrier((&___currentBlock_15), value);
	}

	inline static int32_t get_offset_of_currentBlockOffset_16() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___currentBlockOffset_16)); }
	inline int32_t get_currentBlockOffset_16() const { return ___currentBlockOffset_16; }
	inline int32_t* get_address_of_currentBlockOffset_16() { return &___currentBlockOffset_16; }
	inline void set_currentBlockOffset_16(int32_t value)
	{
		___currentBlockOffset_16 = value;
	}

	inline static int32_t get_offset_of_h0_17() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___h0_17)); }
	inline uint32_t get_h0_17() const { return ___h0_17; }
	inline uint32_t* get_address_of_h0_17() { return &___h0_17; }
	inline void set_h0_17(uint32_t value)
	{
		___h0_17 = value;
	}

	inline static int32_t get_offset_of_h1_18() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___h1_18)); }
	inline uint32_t get_h1_18() const { return ___h1_18; }
	inline uint32_t* get_address_of_h1_18() { return &___h1_18; }
	inline void set_h1_18(uint32_t value)
	{
		___h1_18 = value;
	}

	inline static int32_t get_offset_of_h2_19() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___h2_19)); }
	inline uint32_t get_h2_19() const { return ___h2_19; }
	inline uint32_t* get_address_of_h2_19() { return &___h2_19; }
	inline void set_h2_19(uint32_t value)
	{
		___h2_19 = value;
	}

	inline static int32_t get_offset_of_h3_20() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___h3_20)); }
	inline uint32_t get_h3_20() const { return ___h3_20; }
	inline uint32_t* get_address_of_h3_20() { return &___h3_20; }
	inline void set_h3_20(uint32_t value)
	{
		___h3_20 = value;
	}

	inline static int32_t get_offset_of_h4_21() { return static_cast<int32_t>(offsetof(Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E, ___h4_21)); }
	inline uint32_t get_h4_21() const { return ___h4_21; }
	inline uint32_t* get_address_of_h4_21() { return &___h4_21; }
	inline void set_h4_21(uint32_t value)
	{
		___h4_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLY1305_TA7E692335B023AF877C210D99A953921FA39A98E_H
#ifndef CBCBLOCKCIPHER_T72A1A9225DD378970489A5BEFE255D8B3DDD9E55_H
#define CBCBLOCKCIPHER_T72A1A9225DD378970489A5BEFE255D8B3DDD9E55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Modes.CbcBlockCipher
struct  CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.CbcBlockCipher::IV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.CbcBlockCipher::cbcV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cbcV_1;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.CbcBlockCipher::cbcNextV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___cbcNextV_2;
	// System.Int32 Org.BouncyCastle.Crypto.Modes.CbcBlockCipher::blockSize
	int32_t ___blockSize_3;
	// Org.BouncyCastle.Crypto.IBlockCipher Org.BouncyCastle.Crypto.Modes.CbcBlockCipher::cipher
	RuntimeObject* ___cipher_4;
	// System.Boolean Org.BouncyCastle.Crypto.Modes.CbcBlockCipher::encrypting
	bool ___encrypting_5;

public:
	inline static int32_t get_offset_of_IV_0() { return static_cast<int32_t>(offsetof(CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55, ___IV_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV_0() const { return ___IV_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV_0() { return &___IV_0; }
	inline void set_IV_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV_0 = value;
		Il2CppCodeGenWriteBarrier((&___IV_0), value);
	}

	inline static int32_t get_offset_of_cbcV_1() { return static_cast<int32_t>(offsetof(CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55, ___cbcV_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cbcV_1() const { return ___cbcV_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cbcV_1() { return &___cbcV_1; }
	inline void set_cbcV_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cbcV_1 = value;
		Il2CppCodeGenWriteBarrier((&___cbcV_1), value);
	}

	inline static int32_t get_offset_of_cbcNextV_2() { return static_cast<int32_t>(offsetof(CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55, ___cbcNextV_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_cbcNextV_2() const { return ___cbcNextV_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_cbcNextV_2() { return &___cbcNextV_2; }
	inline void set_cbcNextV_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___cbcNextV_2 = value;
		Il2CppCodeGenWriteBarrier((&___cbcNextV_2), value);
	}

	inline static int32_t get_offset_of_blockSize_3() { return static_cast<int32_t>(offsetof(CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55, ___blockSize_3)); }
	inline int32_t get_blockSize_3() const { return ___blockSize_3; }
	inline int32_t* get_address_of_blockSize_3() { return &___blockSize_3; }
	inline void set_blockSize_3(int32_t value)
	{
		___blockSize_3 = value;
	}

	inline static int32_t get_offset_of_cipher_4() { return static_cast<int32_t>(offsetof(CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55, ___cipher_4)); }
	inline RuntimeObject* get_cipher_4() const { return ___cipher_4; }
	inline RuntimeObject** get_address_of_cipher_4() { return &___cipher_4; }
	inline void set_cipher_4(RuntimeObject* value)
	{
		___cipher_4 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_4), value);
	}

	inline static int32_t get_offset_of_encrypting_5() { return static_cast<int32_t>(offsetof(CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55, ___encrypting_5)); }
	inline bool get_encrypting_5() const { return ___encrypting_5; }
	inline bool* get_address_of_encrypting_5() { return &___encrypting_5; }
	inline void set_encrypting_5(bool value)
	{
		___encrypting_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CBCBLOCKCIPHER_T72A1A9225DD378970489A5BEFE255D8B3DDD9E55_H
#ifndef CCMBLOCKCIPHER_T42A7269184F52AB3243B16F74969015ACD54DA19_H
#define CCMBLOCKCIPHER_T42A7269184F52AB3243B16F74969015ACD54DA19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Modes.CcmBlockCipher
struct  CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.IBlockCipher Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::cipher
	RuntimeObject* ___cipher_1;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::macBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___macBlock_2;
	// System.Boolean Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::forEncryption
	bool ___forEncryption_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::nonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nonce_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::initialAssociatedText
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___initialAssociatedText_5;
	// System.Int32 Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::macSize
	int32_t ___macSize_6;
	// Org.BouncyCastle.Crypto.ICipherParameters Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::keyParam
	RuntimeObject* ___keyParam_7;
	// System.IO.MemoryStream Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::associatedText
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___associatedText_8;
	// System.IO.MemoryStream Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::data
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___data_9;

public:
	inline static int32_t get_offset_of_cipher_1() { return static_cast<int32_t>(offsetof(CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19, ___cipher_1)); }
	inline RuntimeObject* get_cipher_1() const { return ___cipher_1; }
	inline RuntimeObject** get_address_of_cipher_1() { return &___cipher_1; }
	inline void set_cipher_1(RuntimeObject* value)
	{
		___cipher_1 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_1), value);
	}

	inline static int32_t get_offset_of_macBlock_2() { return static_cast<int32_t>(offsetof(CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19, ___macBlock_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_macBlock_2() const { return ___macBlock_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_macBlock_2() { return &___macBlock_2; }
	inline void set_macBlock_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___macBlock_2 = value;
		Il2CppCodeGenWriteBarrier((&___macBlock_2), value);
	}

	inline static int32_t get_offset_of_forEncryption_3() { return static_cast<int32_t>(offsetof(CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19, ___forEncryption_3)); }
	inline bool get_forEncryption_3() const { return ___forEncryption_3; }
	inline bool* get_address_of_forEncryption_3() { return &___forEncryption_3; }
	inline void set_forEncryption_3(bool value)
	{
		___forEncryption_3 = value;
	}

	inline static int32_t get_offset_of_nonce_4() { return static_cast<int32_t>(offsetof(CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19, ___nonce_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nonce_4() const { return ___nonce_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nonce_4() { return &___nonce_4; }
	inline void set_nonce_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nonce_4 = value;
		Il2CppCodeGenWriteBarrier((&___nonce_4), value);
	}

	inline static int32_t get_offset_of_initialAssociatedText_5() { return static_cast<int32_t>(offsetof(CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19, ___initialAssociatedText_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_initialAssociatedText_5() const { return ___initialAssociatedText_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_initialAssociatedText_5() { return &___initialAssociatedText_5; }
	inline void set_initialAssociatedText_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___initialAssociatedText_5 = value;
		Il2CppCodeGenWriteBarrier((&___initialAssociatedText_5), value);
	}

	inline static int32_t get_offset_of_macSize_6() { return static_cast<int32_t>(offsetof(CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19, ___macSize_6)); }
	inline int32_t get_macSize_6() const { return ___macSize_6; }
	inline int32_t* get_address_of_macSize_6() { return &___macSize_6; }
	inline void set_macSize_6(int32_t value)
	{
		___macSize_6 = value;
	}

	inline static int32_t get_offset_of_keyParam_7() { return static_cast<int32_t>(offsetof(CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19, ___keyParam_7)); }
	inline RuntimeObject* get_keyParam_7() const { return ___keyParam_7; }
	inline RuntimeObject** get_address_of_keyParam_7() { return &___keyParam_7; }
	inline void set_keyParam_7(RuntimeObject* value)
	{
		___keyParam_7 = value;
		Il2CppCodeGenWriteBarrier((&___keyParam_7), value);
	}

	inline static int32_t get_offset_of_associatedText_8() { return static_cast<int32_t>(offsetof(CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19, ___associatedText_8)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_associatedText_8() const { return ___associatedText_8; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_associatedText_8() { return &___associatedText_8; }
	inline void set_associatedText_8(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___associatedText_8 = value;
		Il2CppCodeGenWriteBarrier((&___associatedText_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19, ___data_9)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_data_9() const { return ___data_9; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}
};

struct CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19_StaticFields
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Modes.CcmBlockCipher::BlockSize
	int32_t ___BlockSize_0;

public:
	inline static int32_t get_offset_of_BlockSize_0() { return static_cast<int32_t>(offsetof(CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19_StaticFields, ___BlockSize_0)); }
	inline int32_t get_BlockSize_0() const { return ___BlockSize_0; }
	inline int32_t* get_address_of_BlockSize_0() { return &___BlockSize_0; }
	inline void set_BlockSize_0(int32_t value)
	{
		___BlockSize_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CCMBLOCKCIPHER_T42A7269184F52AB3243B16F74969015ACD54DA19_H
#ifndef GCMUTILITIES_T0D3F57F498E8169CC96D3913B9B09C4814078772_H
#define GCMUTILITIES_T0D3F57F498E8169CC96D3913B9B09C4814078772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Modes.Gcm.GcmUtilities
struct  GcmUtilities_t0D3F57F498E8169CC96D3913B9B09C4814078772  : public RuntimeObject
{
public:

public:
};

struct GcmUtilities_t0D3F57F498E8169CC96D3913B9B09C4814078772_StaticFields
{
public:
	// System.UInt32[] Org.BouncyCastle.Crypto.Modes.Gcm.GcmUtilities::LOOKUP
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___LOOKUP_0;

public:
	inline static int32_t get_offset_of_LOOKUP_0() { return static_cast<int32_t>(offsetof(GcmUtilities_t0D3F57F498E8169CC96D3913B9B09C4814078772_StaticFields, ___LOOKUP_0)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_LOOKUP_0() const { return ___LOOKUP_0; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_LOOKUP_0() { return &___LOOKUP_0; }
	inline void set_LOOKUP_0(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___LOOKUP_0 = value;
		Il2CppCodeGenWriteBarrier((&___LOOKUP_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCMUTILITIES_T0D3F57F498E8169CC96D3913B9B09C4814078772_H
#ifndef TABLES1KGCMEXPONENTIATOR_T5A198F1B1859DE1AEB295E643867A6BB4B87888B_H
#define TABLES1KGCMEXPONENTIATOR_T5A198F1B1859DE1AEB295E643867A6BB4B87888B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Modes.Gcm.Tables1kGcmExponentiator
struct  Tables1kGcmExponentiator_t5A198F1B1859DE1AEB295E643867A6BB4B87888B  : public RuntimeObject
{
public:
	// System.Collections.IList Org.BouncyCastle.Crypto.Modes.Gcm.Tables1kGcmExponentiator::lookupPowX2
	RuntimeObject* ___lookupPowX2_0;

public:
	inline static int32_t get_offset_of_lookupPowX2_0() { return static_cast<int32_t>(offsetof(Tables1kGcmExponentiator_t5A198F1B1859DE1AEB295E643867A6BB4B87888B, ___lookupPowX2_0)); }
	inline RuntimeObject* get_lookupPowX2_0() const { return ___lookupPowX2_0; }
	inline RuntimeObject** get_address_of_lookupPowX2_0() { return &___lookupPowX2_0; }
	inline void set_lookupPowX2_0(RuntimeObject* value)
	{
		___lookupPowX2_0 = value;
		Il2CppCodeGenWriteBarrier((&___lookupPowX2_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLES1KGCMEXPONENTIATOR_T5A198F1B1859DE1AEB295E643867A6BB4B87888B_H
#ifndef TABLES8KGCMMULTIPLIER_TDE91C24BB1C4531FC17F3E25C1861733008E2670_H
#define TABLES8KGCMMULTIPLIER_TDE91C24BB1C4531FC17F3E25C1861733008E2670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Modes.Gcm.Tables8kGcmMultiplier
struct  Tables8kGcmMultiplier_tDE91C24BB1C4531FC17F3E25C1861733008E2670  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.Gcm.Tables8kGcmMultiplier::H
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___H_0;
	// System.UInt32[][][] Org.BouncyCastle.Crypto.Modes.Gcm.Tables8kGcmMultiplier::M
	UInt32U5BU5DU5BU5DU5BU5D_t14BCCFDA712528AB5C1A9FAD2BCEC1CE47F44D23* ___M_1;

public:
	inline static int32_t get_offset_of_H_0() { return static_cast<int32_t>(offsetof(Tables8kGcmMultiplier_tDE91C24BB1C4531FC17F3E25C1861733008E2670, ___H_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_H_0() const { return ___H_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_H_0() { return &___H_0; }
	inline void set_H_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___H_0 = value;
		Il2CppCodeGenWriteBarrier((&___H_0), value);
	}

	inline static int32_t get_offset_of_M_1() { return static_cast<int32_t>(offsetof(Tables8kGcmMultiplier_tDE91C24BB1C4531FC17F3E25C1861733008E2670, ___M_1)); }
	inline UInt32U5BU5DU5BU5DU5BU5D_t14BCCFDA712528AB5C1A9FAD2BCEC1CE47F44D23* get_M_1() const { return ___M_1; }
	inline UInt32U5BU5DU5BU5DU5BU5D_t14BCCFDA712528AB5C1A9FAD2BCEC1CE47F44D23** get_address_of_M_1() { return &___M_1; }
	inline void set_M_1(UInt32U5BU5DU5BU5DU5BU5D_t14BCCFDA712528AB5C1A9FAD2BCEC1CE47F44D23* value)
	{
		___M_1 = value;
		Il2CppCodeGenWriteBarrier((&___M_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABLES8KGCMMULTIPLIER_TDE91C24BB1C4531FC17F3E25C1861733008E2670_H
#ifndef GCMBLOCKCIPHER_T9822D875AB4C7C702AC8A87312925B346001DDB3_H
#define GCMBLOCKCIPHER_T9822D875AB4C7C702AC8A87312925B346001DDB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Modes.GcmBlockCipher
struct  GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.IBlockCipher Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::cipher
	RuntimeObject* ___cipher_0;
	// Org.BouncyCastle.Crypto.Modes.Gcm.IGcmMultiplier Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::multiplier
	RuntimeObject* ___multiplier_1;
	// Org.BouncyCastle.Crypto.Modes.Gcm.IGcmExponentiator Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::exp
	RuntimeObject* ___exp_2;
	// System.Boolean Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::forEncryption
	bool ___forEncryption_3;
	// System.Int32 Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::macSize
	int32_t ___macSize_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::nonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nonce_5;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::initialAssociatedText
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___initialAssociatedText_6;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::H
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___H_7;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::J0
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___J0_8;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::bufBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bufBlock_9;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::macBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___macBlock_10;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::S
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_11;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::S_at
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_at_12;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::S_atPre
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___S_atPre_13;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::counter
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___counter_14;
	// System.Int32 Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::bufOff
	int32_t ___bufOff_15;
	// System.UInt64 Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::totalLength
	uint64_t ___totalLength_16;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::atBlock
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___atBlock_17;
	// System.Int32 Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::atBlockPos
	int32_t ___atBlockPos_18;
	// System.UInt64 Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::atLength
	uint64_t ___atLength_19;
	// System.UInt64 Org.BouncyCastle.Crypto.Modes.GcmBlockCipher::atLengthPre
	uint64_t ___atLengthPre_20;

public:
	inline static int32_t get_offset_of_cipher_0() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___cipher_0)); }
	inline RuntimeObject* get_cipher_0() const { return ___cipher_0; }
	inline RuntimeObject** get_address_of_cipher_0() { return &___cipher_0; }
	inline void set_cipher_0(RuntimeObject* value)
	{
		___cipher_0 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_0), value);
	}

	inline static int32_t get_offset_of_multiplier_1() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___multiplier_1)); }
	inline RuntimeObject* get_multiplier_1() const { return ___multiplier_1; }
	inline RuntimeObject** get_address_of_multiplier_1() { return &___multiplier_1; }
	inline void set_multiplier_1(RuntimeObject* value)
	{
		___multiplier_1 = value;
		Il2CppCodeGenWriteBarrier((&___multiplier_1), value);
	}

	inline static int32_t get_offset_of_exp_2() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___exp_2)); }
	inline RuntimeObject* get_exp_2() const { return ___exp_2; }
	inline RuntimeObject** get_address_of_exp_2() { return &___exp_2; }
	inline void set_exp_2(RuntimeObject* value)
	{
		___exp_2 = value;
		Il2CppCodeGenWriteBarrier((&___exp_2), value);
	}

	inline static int32_t get_offset_of_forEncryption_3() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___forEncryption_3)); }
	inline bool get_forEncryption_3() const { return ___forEncryption_3; }
	inline bool* get_address_of_forEncryption_3() { return &___forEncryption_3; }
	inline void set_forEncryption_3(bool value)
	{
		___forEncryption_3 = value;
	}

	inline static int32_t get_offset_of_macSize_4() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___macSize_4)); }
	inline int32_t get_macSize_4() const { return ___macSize_4; }
	inline int32_t* get_address_of_macSize_4() { return &___macSize_4; }
	inline void set_macSize_4(int32_t value)
	{
		___macSize_4 = value;
	}

	inline static int32_t get_offset_of_nonce_5() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___nonce_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nonce_5() const { return ___nonce_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nonce_5() { return &___nonce_5; }
	inline void set_nonce_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nonce_5 = value;
		Il2CppCodeGenWriteBarrier((&___nonce_5), value);
	}

	inline static int32_t get_offset_of_initialAssociatedText_6() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___initialAssociatedText_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_initialAssociatedText_6() const { return ___initialAssociatedText_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_initialAssociatedText_6() { return &___initialAssociatedText_6; }
	inline void set_initialAssociatedText_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___initialAssociatedText_6 = value;
		Il2CppCodeGenWriteBarrier((&___initialAssociatedText_6), value);
	}

	inline static int32_t get_offset_of_H_7() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___H_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_H_7() const { return ___H_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_H_7() { return &___H_7; }
	inline void set_H_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___H_7 = value;
		Il2CppCodeGenWriteBarrier((&___H_7), value);
	}

	inline static int32_t get_offset_of_J0_8() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___J0_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_J0_8() const { return ___J0_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_J0_8() { return &___J0_8; }
	inline void set_J0_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___J0_8 = value;
		Il2CppCodeGenWriteBarrier((&___J0_8), value);
	}

	inline static int32_t get_offset_of_bufBlock_9() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___bufBlock_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bufBlock_9() const { return ___bufBlock_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bufBlock_9() { return &___bufBlock_9; }
	inline void set_bufBlock_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bufBlock_9 = value;
		Il2CppCodeGenWriteBarrier((&___bufBlock_9), value);
	}

	inline static int32_t get_offset_of_macBlock_10() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___macBlock_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_macBlock_10() const { return ___macBlock_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_macBlock_10() { return &___macBlock_10; }
	inline void set_macBlock_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___macBlock_10 = value;
		Il2CppCodeGenWriteBarrier((&___macBlock_10), value);
	}

	inline static int32_t get_offset_of_S_11() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___S_11)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_11() const { return ___S_11; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_11() { return &___S_11; }
	inline void set_S_11(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_11 = value;
		Il2CppCodeGenWriteBarrier((&___S_11), value);
	}

	inline static int32_t get_offset_of_S_at_12() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___S_at_12)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_at_12() const { return ___S_at_12; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_at_12() { return &___S_at_12; }
	inline void set_S_at_12(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_at_12 = value;
		Il2CppCodeGenWriteBarrier((&___S_at_12), value);
	}

	inline static int32_t get_offset_of_S_atPre_13() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___S_atPre_13)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_S_atPre_13() const { return ___S_atPre_13; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_S_atPre_13() { return &___S_atPre_13; }
	inline void set_S_atPre_13(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___S_atPre_13 = value;
		Il2CppCodeGenWriteBarrier((&___S_atPre_13), value);
	}

	inline static int32_t get_offset_of_counter_14() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___counter_14)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_counter_14() const { return ___counter_14; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_counter_14() { return &___counter_14; }
	inline void set_counter_14(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___counter_14 = value;
		Il2CppCodeGenWriteBarrier((&___counter_14), value);
	}

	inline static int32_t get_offset_of_bufOff_15() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___bufOff_15)); }
	inline int32_t get_bufOff_15() const { return ___bufOff_15; }
	inline int32_t* get_address_of_bufOff_15() { return &___bufOff_15; }
	inline void set_bufOff_15(int32_t value)
	{
		___bufOff_15 = value;
	}

	inline static int32_t get_offset_of_totalLength_16() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___totalLength_16)); }
	inline uint64_t get_totalLength_16() const { return ___totalLength_16; }
	inline uint64_t* get_address_of_totalLength_16() { return &___totalLength_16; }
	inline void set_totalLength_16(uint64_t value)
	{
		___totalLength_16 = value;
	}

	inline static int32_t get_offset_of_atBlock_17() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___atBlock_17)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_atBlock_17() const { return ___atBlock_17; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_atBlock_17() { return &___atBlock_17; }
	inline void set_atBlock_17(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___atBlock_17 = value;
		Il2CppCodeGenWriteBarrier((&___atBlock_17), value);
	}

	inline static int32_t get_offset_of_atBlockPos_18() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___atBlockPos_18)); }
	inline int32_t get_atBlockPos_18() const { return ___atBlockPos_18; }
	inline int32_t* get_address_of_atBlockPos_18() { return &___atBlockPos_18; }
	inline void set_atBlockPos_18(int32_t value)
	{
		___atBlockPos_18 = value;
	}

	inline static int32_t get_offset_of_atLength_19() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___atLength_19)); }
	inline uint64_t get_atLength_19() const { return ___atLength_19; }
	inline uint64_t* get_address_of_atLength_19() { return &___atLength_19; }
	inline void set_atLength_19(uint64_t value)
	{
		___atLength_19 = value;
	}

	inline static int32_t get_offset_of_atLengthPre_20() { return static_cast<int32_t>(offsetof(GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3, ___atLengthPre_20)); }
	inline uint64_t get_atLengthPre_20() const { return ___atLengthPre_20; }
	inline uint64_t* get_address_of_atLengthPre_20() { return &___atLengthPre_20; }
	inline void set_atLengthPre_20(uint64_t value)
	{
		___atLengthPre_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GCMBLOCKCIPHER_T9822D875AB4C7C702AC8A87312925B346001DDB3_H
#ifndef SICBLOCKCIPHER_TDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA_H
#define SICBLOCKCIPHER_TDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Modes.SicBlockCipher
struct  SicBlockCipher_tDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.IBlockCipher Org.BouncyCastle.Crypto.Modes.SicBlockCipher::cipher
	RuntimeObject* ___cipher_0;
	// System.Int32 Org.BouncyCastle.Crypto.Modes.SicBlockCipher::blockSize
	int32_t ___blockSize_1;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.SicBlockCipher::counter
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___counter_2;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.SicBlockCipher::counterOut
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___counterOut_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Modes.SicBlockCipher::IV
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IV_4;

public:
	inline static int32_t get_offset_of_cipher_0() { return static_cast<int32_t>(offsetof(SicBlockCipher_tDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA, ___cipher_0)); }
	inline RuntimeObject* get_cipher_0() const { return ___cipher_0; }
	inline RuntimeObject** get_address_of_cipher_0() { return &___cipher_0; }
	inline void set_cipher_0(RuntimeObject* value)
	{
		___cipher_0 = value;
		Il2CppCodeGenWriteBarrier((&___cipher_0), value);
	}

	inline static int32_t get_offset_of_blockSize_1() { return static_cast<int32_t>(offsetof(SicBlockCipher_tDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA, ___blockSize_1)); }
	inline int32_t get_blockSize_1() const { return ___blockSize_1; }
	inline int32_t* get_address_of_blockSize_1() { return &___blockSize_1; }
	inline void set_blockSize_1(int32_t value)
	{
		___blockSize_1 = value;
	}

	inline static int32_t get_offset_of_counter_2() { return static_cast<int32_t>(offsetof(SicBlockCipher_tDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA, ___counter_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_counter_2() const { return ___counter_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_counter_2() { return &___counter_2; }
	inline void set_counter_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___counter_2 = value;
		Il2CppCodeGenWriteBarrier((&___counter_2), value);
	}

	inline static int32_t get_offset_of_counterOut_3() { return static_cast<int32_t>(offsetof(SicBlockCipher_tDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA, ___counterOut_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_counterOut_3() const { return ___counterOut_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_counterOut_3() { return &___counterOut_3; }
	inline void set_counterOut_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___counterOut_3 = value;
		Il2CppCodeGenWriteBarrier((&___counterOut_3), value);
	}

	inline static int32_t get_offset_of_IV_4() { return static_cast<int32_t>(offsetof(SicBlockCipher_tDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA, ___IV_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IV_4() const { return ___IV_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IV_4() { return &___IV_4; }
	inline void set_IV_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IV_4 = value;
		Il2CppCodeGenWriteBarrier((&___IV_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SICBLOCKCIPHER_TDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA_H
#ifndef AEADPARAMETERS_T04E1AE26A3A54261A1893C79598D845FFE575212_H
#define AEADPARAMETERS_T04E1AE26A3A54261A1893C79598D845FFE575212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.AeadParameters
struct  AeadParameters_t04E1AE26A3A54261A1893C79598D845FFE575212  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Parameters.AeadParameters::associatedText
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___associatedText_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Parameters.AeadParameters::nonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___nonce_1;
	// Org.BouncyCastle.Crypto.Parameters.KeyParameter Org.BouncyCastle.Crypto.Parameters.AeadParameters::key
	KeyParameter_t969556DA4FAE52672608FEC8F8E2D4E4EC8337A2 * ___key_2;
	// System.Int32 Org.BouncyCastle.Crypto.Parameters.AeadParameters::macSize
	int32_t ___macSize_3;

public:
	inline static int32_t get_offset_of_associatedText_0() { return static_cast<int32_t>(offsetof(AeadParameters_t04E1AE26A3A54261A1893C79598D845FFE575212, ___associatedText_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_associatedText_0() const { return ___associatedText_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_associatedText_0() { return &___associatedText_0; }
	inline void set_associatedText_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___associatedText_0 = value;
		Il2CppCodeGenWriteBarrier((&___associatedText_0), value);
	}

	inline static int32_t get_offset_of_nonce_1() { return static_cast<int32_t>(offsetof(AeadParameters_t04E1AE26A3A54261A1893C79598D845FFE575212, ___nonce_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_nonce_1() const { return ___nonce_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_nonce_1() { return &___nonce_1; }
	inline void set_nonce_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___nonce_1 = value;
		Il2CppCodeGenWriteBarrier((&___nonce_1), value);
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(AeadParameters_t04E1AE26A3A54261A1893C79598D845FFE575212, ___key_2)); }
	inline KeyParameter_t969556DA4FAE52672608FEC8F8E2D4E4EC8337A2 * get_key_2() const { return ___key_2; }
	inline KeyParameter_t969556DA4FAE52672608FEC8F8E2D4E4EC8337A2 ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(KeyParameter_t969556DA4FAE52672608FEC8F8E2D4E4EC8337A2 * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_macSize_3() { return static_cast<int32_t>(offsetof(AeadParameters_t04E1AE26A3A54261A1893C79598D845FFE575212, ___macSize_3)); }
	inline int32_t get_macSize_3() const { return ___macSize_3; }
	inline int32_t* get_address_of_macSize_3() { return &___macSize_3; }
	inline void set_macSize_3(int32_t value)
	{
		___macSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AEADPARAMETERS_T04E1AE26A3A54261A1893C79598D845FFE575212_H
#ifndef DHPARAMETERS_T15DBCF5CD22F83E806A78FF80CE2853863825513_H
#define DHPARAMETERS_T15DBCF5CD22F83E806A78FF80CE2853863825513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.DHParameters
struct  DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.DHParameters::p
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___p_0;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.DHParameters::g
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___g_1;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.DHParameters::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_2;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.DHParameters::j
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___j_3;
	// System.Int32 Org.BouncyCastle.Crypto.Parameters.DHParameters::m
	int32_t ___m_4;
	// System.Int32 Org.BouncyCastle.Crypto.Parameters.DHParameters::l
	int32_t ___l_5;
	// Org.BouncyCastle.Crypto.Parameters.DHValidationParameters Org.BouncyCastle.Crypto.Parameters.DHParameters::validation
	DHValidationParameters_tC30183B3C1E3CAF234F646697CED1210A4EF4C0E * ___validation_6;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513, ___p_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_p_0() const { return ___p_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513, ___g_1)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_g_1() const { return ___g_1; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___g_1 = value;
		Il2CppCodeGenWriteBarrier((&___g_1), value);
	}

	inline static int32_t get_offset_of_q_2() { return static_cast<int32_t>(offsetof(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513, ___q_2)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_2() const { return ___q_2; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_2() { return &___q_2; }
	inline void set_q_2(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_2 = value;
		Il2CppCodeGenWriteBarrier((&___q_2), value);
	}

	inline static int32_t get_offset_of_j_3() { return static_cast<int32_t>(offsetof(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513, ___j_3)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_j_3() const { return ___j_3; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_j_3() { return &___j_3; }
	inline void set_j_3(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___j_3 = value;
		Il2CppCodeGenWriteBarrier((&___j_3), value);
	}

	inline static int32_t get_offset_of_m_4() { return static_cast<int32_t>(offsetof(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513, ___m_4)); }
	inline int32_t get_m_4() const { return ___m_4; }
	inline int32_t* get_address_of_m_4() { return &___m_4; }
	inline void set_m_4(int32_t value)
	{
		___m_4 = value;
	}

	inline static int32_t get_offset_of_l_5() { return static_cast<int32_t>(offsetof(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513, ___l_5)); }
	inline int32_t get_l_5() const { return ___l_5; }
	inline int32_t* get_address_of_l_5() { return &___l_5; }
	inline void set_l_5(int32_t value)
	{
		___l_5 = value;
	}

	inline static int32_t get_offset_of_validation_6() { return static_cast<int32_t>(offsetof(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513, ___validation_6)); }
	inline DHValidationParameters_tC30183B3C1E3CAF234F646697CED1210A4EF4C0E * get_validation_6() const { return ___validation_6; }
	inline DHValidationParameters_tC30183B3C1E3CAF234F646697CED1210A4EF4C0E ** get_address_of_validation_6() { return &___validation_6; }
	inline void set_validation_6(DHValidationParameters_tC30183B3C1E3CAF234F646697CED1210A4EF4C0E * value)
	{
		___validation_6 = value;
		Il2CppCodeGenWriteBarrier((&___validation_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHPARAMETERS_T15DBCF5CD22F83E806A78FF80CE2853863825513_H
#ifndef DHVALIDATIONPARAMETERS_TC30183B3C1E3CAF234F646697CED1210A4EF4C0E_H
#define DHVALIDATIONPARAMETERS_TC30183B3C1E3CAF234F646697CED1210A4EF4C0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.DHValidationParameters
struct  DHValidationParameters_tC30183B3C1E3CAF234F646697CED1210A4EF4C0E  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Parameters.DHValidationParameters::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_0;
	// System.Int32 Org.BouncyCastle.Crypto.Parameters.DHValidationParameters::counter
	int32_t ___counter_1;

public:
	inline static int32_t get_offset_of_seed_0() { return static_cast<int32_t>(offsetof(DHValidationParameters_tC30183B3C1E3CAF234F646697CED1210A4EF4C0E, ___seed_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_0() const { return ___seed_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_0() { return &___seed_0; }
	inline void set_seed_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_0 = value;
		Il2CppCodeGenWriteBarrier((&___seed_0), value);
	}

	inline static int32_t get_offset_of_counter_1() { return static_cast<int32_t>(offsetof(DHValidationParameters_tC30183B3C1E3CAF234F646697CED1210A4EF4C0E, ___counter_1)); }
	inline int32_t get_counter_1() const { return ___counter_1; }
	inline int32_t* get_address_of_counter_1() { return &___counter_1; }
	inline void set_counter_1(int32_t value)
	{
		___counter_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHVALIDATIONPARAMETERS_TC30183B3C1E3CAF234F646697CED1210A4EF4C0E_H
#ifndef DSAPARAMETERS_TD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807_H
#define DSAPARAMETERS_TD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.DsaParameters
struct  DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.DsaParameters::p
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___p_0;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.DsaParameters::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_1;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.DsaParameters::g
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___g_2;
	// Org.BouncyCastle.Crypto.Parameters.DsaValidationParameters Org.BouncyCastle.Crypto.Parameters.DsaParameters::validation
	DsaValidationParameters_t2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76 * ___validation_3;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807, ___p_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_p_0() const { return ___p_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_q_1() { return static_cast<int32_t>(offsetof(DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807, ___q_1)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_1() const { return ___q_1; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_1() { return &___q_1; }
	inline void set_q_1(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_1 = value;
		Il2CppCodeGenWriteBarrier((&___q_1), value);
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807, ___g_2)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_g_2() const { return ___g_2; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___g_2 = value;
		Il2CppCodeGenWriteBarrier((&___g_2), value);
	}

	inline static int32_t get_offset_of_validation_3() { return static_cast<int32_t>(offsetof(DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807, ___validation_3)); }
	inline DsaValidationParameters_t2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76 * get_validation_3() const { return ___validation_3; }
	inline DsaValidationParameters_t2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76 ** get_address_of_validation_3() { return &___validation_3; }
	inline void set_validation_3(DsaValidationParameters_t2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76 * value)
	{
		___validation_3 = value;
		Il2CppCodeGenWriteBarrier((&___validation_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAPARAMETERS_TD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807_H
#ifndef DSAVALIDATIONPARAMETERS_T2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76_H
#define DSAVALIDATIONPARAMETERS_T2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.DsaValidationParameters
struct  DsaValidationParameters_t2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Parameters.DsaValidationParameters::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_0;
	// System.Int32 Org.BouncyCastle.Crypto.Parameters.DsaValidationParameters::counter
	int32_t ___counter_1;

public:
	inline static int32_t get_offset_of_seed_0() { return static_cast<int32_t>(offsetof(DsaValidationParameters_t2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76, ___seed_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_0() const { return ___seed_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_0() { return &___seed_0; }
	inline void set_seed_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_0 = value;
		Il2CppCodeGenWriteBarrier((&___seed_0), value);
	}

	inline static int32_t get_offset_of_counter_1() { return static_cast<int32_t>(offsetof(DsaValidationParameters_t2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76, ___counter_1)); }
	inline int32_t get_counter_1() const { return ___counter_1; }
	inline int32_t* get_address_of_counter_1() { return &___counter_1; }
	inline void set_counter_1(int32_t value)
	{
		___counter_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAVALIDATIONPARAMETERS_T2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76_H
#ifndef ECDOMAINPARAMETERS_T0960619BC6EBC2C7841671A013C8B7910E71AB57_H
#define ECDOMAINPARAMETERS_T0960619BC6EBC2C7841671A013C8B7910E71AB57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.ECDomainParameters
struct  ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.EC.ECCurve Org.BouncyCastle.Crypto.Parameters.ECDomainParameters::curve
	ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * ___curve_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Parameters.ECDomainParameters::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_1;
	// Org.BouncyCastle.Math.EC.ECPoint Org.BouncyCastle.Crypto.Parameters.ECDomainParameters::g
	ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 * ___g_2;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.ECDomainParameters::n
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___n_3;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.ECDomainParameters::h
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___h_4;

public:
	inline static int32_t get_offset_of_curve_0() { return static_cast<int32_t>(offsetof(ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57, ___curve_0)); }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * get_curve_0() const { return ___curve_0; }
	inline ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 ** get_address_of_curve_0() { return &___curve_0; }
	inline void set_curve_0(ECCurve_t08AD8E722914B0B03C49C2D27B79B1D4E4770D95 * value)
	{
		___curve_0 = value;
		Il2CppCodeGenWriteBarrier((&___curve_0), value);
	}

	inline static int32_t get_offset_of_seed_1() { return static_cast<int32_t>(offsetof(ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57, ___seed_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_1() const { return ___seed_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_1() { return &___seed_1; }
	inline void set_seed_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_1 = value;
		Il2CppCodeGenWriteBarrier((&___seed_1), value);
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57, ___g_2)); }
	inline ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 * get_g_2() const { return ___g_2; }
	inline ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 ** get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 * value)
	{
		___g_2 = value;
		Il2CppCodeGenWriteBarrier((&___g_2), value);
	}

	inline static int32_t get_offset_of_n_3() { return static_cast<int32_t>(offsetof(ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57, ___n_3)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_n_3() const { return ___n_3; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_n_3() { return &___n_3; }
	inline void set_n_3(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___n_3 = value;
		Il2CppCodeGenWriteBarrier((&___n_3), value);
	}

	inline static int32_t get_offset_of_h_4() { return static_cast<int32_t>(offsetof(ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57, ___h_4)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_h_4() const { return ___h_4; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_h_4() { return &___h_4; }
	inline void set_h_4(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___h_4 = value;
		Il2CppCodeGenWriteBarrier((&___h_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECDOMAINPARAMETERS_T0960619BC6EBC2C7841671A013C8B7910E71AB57_H
#ifndef ELGAMALPARAMETERS_TC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA_H
#define ELGAMALPARAMETERS_TC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.ElGamalParameters
struct  ElGamalParameters_tC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.ElGamalParameters::p
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___p_0;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.ElGamalParameters::g
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___g_1;
	// System.Int32 Org.BouncyCastle.Crypto.Parameters.ElGamalParameters::l
	int32_t ___l_2;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(ElGamalParameters_tC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA, ___p_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_p_0() const { return ___p_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(ElGamalParameters_tC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA, ___g_1)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_g_1() const { return ___g_1; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___g_1 = value;
		Il2CppCodeGenWriteBarrier((&___g_1), value);
	}

	inline static int32_t get_offset_of_l_2() { return static_cast<int32_t>(offsetof(ElGamalParameters_tC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA, ___l_2)); }
	inline int32_t get_l_2() const { return ___l_2; }
	inline int32_t* get_address_of_l_2() { return &___l_2; }
	inline void set_l_2(int32_t value)
	{
		___l_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELGAMALPARAMETERS_TC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA_H
#ifndef GOST3410PARAMETERS_T7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD_H
#define GOST3410PARAMETERS_T7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters
struct  Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters::p
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___p_0;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_1;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters::a
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___a_2;
	// Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters::validation
	Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98 * ___validation_3;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD, ___p_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_p_0() const { return ___p_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}

	inline static int32_t get_offset_of_q_1() { return static_cast<int32_t>(offsetof(Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD, ___q_1)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_1() const { return ___q_1; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_1() { return &___q_1; }
	inline void set_q_1(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_1 = value;
		Il2CppCodeGenWriteBarrier((&___q_1), value);
	}

	inline static int32_t get_offset_of_a_2() { return static_cast<int32_t>(offsetof(Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD, ___a_2)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_a_2() const { return ___a_2; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_a_2() { return &___a_2; }
	inline void set_a_2(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___a_2 = value;
		Il2CppCodeGenWriteBarrier((&___a_2), value);
	}

	inline static int32_t get_offset_of_validation_3() { return static_cast<int32_t>(offsetof(Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD, ___validation_3)); }
	inline Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98 * get_validation_3() const { return ___validation_3; }
	inline Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98 ** get_address_of_validation_3() { return &___validation_3; }
	inline void set_validation_3(Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98 * value)
	{
		___validation_3 = value;
		Il2CppCodeGenWriteBarrier((&___validation_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410PARAMETERS_T7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD_H
#ifndef GOST3410VALIDATIONPARAMETERS_TFE9E63B9D9DF4A8E281B2A30C3599055545C2B98_H
#define GOST3410VALIDATIONPARAMETERS_TFE9E63B9D9DF4A8E281B2A30C3599055545C2B98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters
struct  Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters::x0
	int32_t ___x0_0;
	// System.Int32 Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters::c
	int32_t ___c_1;
	// System.Int64 Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters::x0L
	int64_t ___x0L_2;
	// System.Int64 Org.BouncyCastle.Crypto.Parameters.Gost3410ValidationParameters::cL
	int64_t ___cL_3;

public:
	inline static int32_t get_offset_of_x0_0() { return static_cast<int32_t>(offsetof(Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98, ___x0_0)); }
	inline int32_t get_x0_0() const { return ___x0_0; }
	inline int32_t* get_address_of_x0_0() { return &___x0_0; }
	inline void set_x0_0(int32_t value)
	{
		___x0_0 = value;
	}

	inline static int32_t get_offset_of_c_1() { return static_cast<int32_t>(offsetof(Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98, ___c_1)); }
	inline int32_t get_c_1() const { return ___c_1; }
	inline int32_t* get_address_of_c_1() { return &___c_1; }
	inline void set_c_1(int32_t value)
	{
		___c_1 = value;
	}

	inline static int32_t get_offset_of_x0L_2() { return static_cast<int32_t>(offsetof(Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98, ___x0L_2)); }
	inline int64_t get_x0L_2() const { return ___x0L_2; }
	inline int64_t* get_address_of_x0L_2() { return &___x0L_2; }
	inline void set_x0L_2(int64_t value)
	{
		___x0L_2 = value;
	}

	inline static int32_t get_offset_of_cL_3() { return static_cast<int32_t>(offsetof(Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98, ___cL_3)); }
	inline int64_t get_cL_3() const { return ___cL_3; }
	inline int64_t* get_address_of_cL_3() { return &___cL_3; }
	inline void set_cL_3(int64_t value)
	{
		___cL_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410VALIDATIONPARAMETERS_TFE9E63B9D9DF4A8E281B2A30C3599055545C2B98_H
#ifndef KEYPARAMETER_T969556DA4FAE52672608FEC8F8E2D4E4EC8337A2_H
#define KEYPARAMETER_T969556DA4FAE52672608FEC8F8E2D4E4EC8337A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.KeyParameter
struct  KeyParameter_t969556DA4FAE52672608FEC8F8E2D4E4EC8337A2  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Parameters.KeyParameter::key
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyParameter_t969556DA4FAE52672608FEC8F8E2D4E4EC8337A2, ___key_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_key_0() const { return ___key_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYPARAMETER_T969556DA4FAE52672608FEC8F8E2D4E4EC8337A2_H
#ifndef PARAMETERSWITHIV_TD591FD016AE64BD6570FEB2A4A98032A8F0F3572_H
#define PARAMETERSWITHIV_TD591FD016AE64BD6570FEB2A4A98032A8F0F3572_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.ParametersWithIV
struct  ParametersWithIV_tD591FD016AE64BD6570FEB2A4A98032A8F0F3572  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.ICipherParameters Org.BouncyCastle.Crypto.Parameters.ParametersWithIV::parameters
	RuntimeObject* ___parameters_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Parameters.ParametersWithIV::iv
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___iv_1;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(ParametersWithIV_tD591FD016AE64BD6570FEB2A4A98032A8F0F3572, ___parameters_0)); }
	inline RuntimeObject* get_parameters_0() const { return ___parameters_0; }
	inline RuntimeObject** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(RuntimeObject* value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}

	inline static int32_t get_offset_of_iv_1() { return static_cast<int32_t>(offsetof(ParametersWithIV_tD591FD016AE64BD6570FEB2A4A98032A8F0F3572, ___iv_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_iv_1() const { return ___iv_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_iv_1() { return &___iv_1; }
	inline void set_iv_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___iv_1 = value;
		Il2CppCodeGenWriteBarrier((&___iv_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERSWITHIV_TD591FD016AE64BD6570FEB2A4A98032A8F0F3572_H
#ifndef PARAMETERSWITHRANDOM_T70E164400FC8254A14587493D29A4A10292DFFC5_H
#define PARAMETERSWITHRANDOM_T70E164400FC8254A14587493D29A4A10292DFFC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.ParametersWithRandom
struct  ParametersWithRandom_t70E164400FC8254A14587493D29A4A10292DFFC5  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.ICipherParameters Org.BouncyCastle.Crypto.Parameters.ParametersWithRandom::parameters
	RuntimeObject* ___parameters_0;
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Crypto.Parameters.ParametersWithRandom::random
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___random_1;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(ParametersWithRandom_t70E164400FC8254A14587493D29A4A10292DFFC5, ___parameters_0)); }
	inline RuntimeObject* get_parameters_0() const { return ___parameters_0; }
	inline RuntimeObject** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(RuntimeObject* value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}

	inline static int32_t get_offset_of_random_1() { return static_cast<int32_t>(offsetof(ParametersWithRandom_t70E164400FC8254A14587493D29A4A10292DFFC5, ___random_1)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_random_1() const { return ___random_1; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_random_1() { return &___random_1; }
	inline void set_random_1(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___random_1 = value;
		Il2CppCodeGenWriteBarrier((&___random_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERSWITHRANDOM_T70E164400FC8254A14587493D29A4A10292DFFC5_H
#ifndef PARAMETERSWITHSBOX_T9DF7CC8A76EBAD30DC1C35CD5E49B7F54A67AC66_H
#define PARAMETERSWITHSBOX_T9DF7CC8A76EBAD30DC1C35CD5E49B7F54A67AC66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.ParametersWithSBox
struct  ParametersWithSBox_t9DF7CC8A76EBAD30DC1C35CD5E49B7F54A67AC66  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.ICipherParameters Org.BouncyCastle.Crypto.Parameters.ParametersWithSBox::parameters
	RuntimeObject* ___parameters_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Parameters.ParametersWithSBox::sBox
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sBox_1;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(ParametersWithSBox_t9DF7CC8A76EBAD30DC1C35CD5E49B7F54A67AC66, ___parameters_0)); }
	inline RuntimeObject* get_parameters_0() const { return ___parameters_0; }
	inline RuntimeObject** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(RuntimeObject* value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}

	inline static int32_t get_offset_of_sBox_1() { return static_cast<int32_t>(offsetof(ParametersWithSBox_t9DF7CC8A76EBAD30DC1C35CD5E49B7F54A67AC66, ___sBox_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sBox_1() const { return ___sBox_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sBox_1() { return &___sBox_1; }
	inline void set_sBox_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sBox_1 = value;
		Il2CppCodeGenWriteBarrier((&___sBox_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERSWITHSBOX_T9DF7CC8A76EBAD30DC1C35CD5E49B7F54A67AC66_H
#ifndef CRYPTOAPIRANDOMGENERATOR_TF05E7E87DEA4D3ED648264F159B91B54021C26B0_H
#define CRYPTOAPIRANDOMGENERATOR_TF05E7E87DEA4D3ED648264F159B91B54021C26B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Prng.CryptoApiRandomGenerator
struct  CryptoApiRandomGenerator_tF05E7E87DEA4D3ED648264F159B91B54021C26B0  : public RuntimeObject
{
public:
	// System.Security.Cryptography.RandomNumberGenerator Org.BouncyCastle.Crypto.Prng.CryptoApiRandomGenerator::rndProv
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ___rndProv_0;

public:
	inline static int32_t get_offset_of_rndProv_0() { return static_cast<int32_t>(offsetof(CryptoApiRandomGenerator_tF05E7E87DEA4D3ED648264F159B91B54021C26B0, ___rndProv_0)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get_rndProv_0() const { return ___rndProv_0; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of_rndProv_0() { return &___rndProv_0; }
	inline void set_rndProv_0(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		___rndProv_0 = value;
		Il2CppCodeGenWriteBarrier((&___rndProv_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOAPIRANDOMGENERATOR_TF05E7E87DEA4D3ED648264F159B91B54021C26B0_H
#ifndef DIGESTRANDOMGENERATOR_T145115EA116BD2747EB5B1860F1A47A604B1BF59_H
#define DIGESTRANDOMGENERATOR_T145115EA116BD2747EB5B1860F1A47A604B1BF59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Prng.DigestRandomGenerator
struct  DigestRandomGenerator_t145115EA116BD2747EB5B1860F1A47A604B1BF59  : public RuntimeObject
{
public:
	// System.Int64 Org.BouncyCastle.Crypto.Prng.DigestRandomGenerator::stateCounter
	int64_t ___stateCounter_0;
	// System.Int64 Org.BouncyCastle.Crypto.Prng.DigestRandomGenerator::seedCounter
	int64_t ___seedCounter_1;
	// Org.BouncyCastle.Crypto.IDigest Org.BouncyCastle.Crypto.Prng.DigestRandomGenerator::digest
	RuntimeObject* ___digest_2;
	// System.Byte[] Org.BouncyCastle.Crypto.Prng.DigestRandomGenerator::state
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___state_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Prng.DigestRandomGenerator::seed
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___seed_4;

public:
	inline static int32_t get_offset_of_stateCounter_0() { return static_cast<int32_t>(offsetof(DigestRandomGenerator_t145115EA116BD2747EB5B1860F1A47A604B1BF59, ___stateCounter_0)); }
	inline int64_t get_stateCounter_0() const { return ___stateCounter_0; }
	inline int64_t* get_address_of_stateCounter_0() { return &___stateCounter_0; }
	inline void set_stateCounter_0(int64_t value)
	{
		___stateCounter_0 = value;
	}

	inline static int32_t get_offset_of_seedCounter_1() { return static_cast<int32_t>(offsetof(DigestRandomGenerator_t145115EA116BD2747EB5B1860F1A47A604B1BF59, ___seedCounter_1)); }
	inline int64_t get_seedCounter_1() const { return ___seedCounter_1; }
	inline int64_t* get_address_of_seedCounter_1() { return &___seedCounter_1; }
	inline void set_seedCounter_1(int64_t value)
	{
		___seedCounter_1 = value;
	}

	inline static int32_t get_offset_of_digest_2() { return static_cast<int32_t>(offsetof(DigestRandomGenerator_t145115EA116BD2747EB5B1860F1A47A604B1BF59, ___digest_2)); }
	inline RuntimeObject* get_digest_2() const { return ___digest_2; }
	inline RuntimeObject** get_address_of_digest_2() { return &___digest_2; }
	inline void set_digest_2(RuntimeObject* value)
	{
		___digest_2 = value;
		Il2CppCodeGenWriteBarrier((&___digest_2), value);
	}

	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(DigestRandomGenerator_t145115EA116BD2747EB5B1860F1A47A604B1BF59, ___state_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_state_3() const { return ___state_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___state_3 = value;
		Il2CppCodeGenWriteBarrier((&___state_3), value);
	}

	inline static int32_t get_offset_of_seed_4() { return static_cast<int32_t>(offsetof(DigestRandomGenerator_t145115EA116BD2747EB5B1860F1A47A604B1BF59, ___seed_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_seed_4() const { return ___seed_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_seed_4() { return &___seed_4; }
	inline void set_seed_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___seed_4 = value;
		Il2CppCodeGenWriteBarrier((&___seed_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTRANDOMGENERATOR_T145115EA116BD2747EB5B1860F1A47A604B1BF59_H
#ifndef DSADIGESTSIGNER_TF9384890F2D126B4FC63626F0CD56496730BACCE_H
#define DSADIGESTSIGNER_TF9384890F2D126B4FC63626F0CD56496730BACCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Signers.DsaDigestSigner
struct  DsaDigestSigner_tF9384890F2D126B4FC63626F0CD56496730BACCE  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.IDigest Org.BouncyCastle.Crypto.Signers.DsaDigestSigner::digest
	RuntimeObject* ___digest_0;
	// Org.BouncyCastle.Crypto.IDsa Org.BouncyCastle.Crypto.Signers.DsaDigestSigner::dsaSigner
	RuntimeObject* ___dsaSigner_1;
	// System.Boolean Org.BouncyCastle.Crypto.Signers.DsaDigestSigner::forSigning
	bool ___forSigning_2;

public:
	inline static int32_t get_offset_of_digest_0() { return static_cast<int32_t>(offsetof(DsaDigestSigner_tF9384890F2D126B4FC63626F0CD56496730BACCE, ___digest_0)); }
	inline RuntimeObject* get_digest_0() const { return ___digest_0; }
	inline RuntimeObject** get_address_of_digest_0() { return &___digest_0; }
	inline void set_digest_0(RuntimeObject* value)
	{
		___digest_0 = value;
		Il2CppCodeGenWriteBarrier((&___digest_0), value);
	}

	inline static int32_t get_offset_of_dsaSigner_1() { return static_cast<int32_t>(offsetof(DsaDigestSigner_tF9384890F2D126B4FC63626F0CD56496730BACCE, ___dsaSigner_1)); }
	inline RuntimeObject* get_dsaSigner_1() const { return ___dsaSigner_1; }
	inline RuntimeObject** get_address_of_dsaSigner_1() { return &___dsaSigner_1; }
	inline void set_dsaSigner_1(RuntimeObject* value)
	{
		___dsaSigner_1 = value;
		Il2CppCodeGenWriteBarrier((&___dsaSigner_1), value);
	}

	inline static int32_t get_offset_of_forSigning_2() { return static_cast<int32_t>(offsetof(DsaDigestSigner_tF9384890F2D126B4FC63626F0CD56496730BACCE, ___forSigning_2)); }
	inline bool get_forSigning_2() const { return ___forSigning_2; }
	inline bool* get_address_of_forSigning_2() { return &___forSigning_2; }
	inline void set_forSigning_2(bool value)
	{
		___forSigning_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSADIGESTSIGNER_TF9384890F2D126B4FC63626F0CD56496730BACCE_H
#ifndef DSASIGNER_T1B94B42B08AC7EB937B05FC6DCF361A1BA5BFB66_H
#define DSASIGNER_T1B94B42B08AC7EB937B05FC6DCF361A1BA5BFB66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Signers.DsaSigner
struct  DsaSigner_t1B94B42B08AC7EB937B05FC6DCF361A1BA5BFB66  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Signers.IDsaKCalculator Org.BouncyCastle.Crypto.Signers.DsaSigner::kCalculator
	RuntimeObject* ___kCalculator_0;
	// Org.BouncyCastle.Crypto.Parameters.DsaKeyParameters Org.BouncyCastle.Crypto.Signers.DsaSigner::key
	DsaKeyParameters_tBF3E3B9B02243E5C768006D70F4CA496F15E82A6 * ___key_1;
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Crypto.Signers.DsaSigner::random
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___random_2;

public:
	inline static int32_t get_offset_of_kCalculator_0() { return static_cast<int32_t>(offsetof(DsaSigner_t1B94B42B08AC7EB937B05FC6DCF361A1BA5BFB66, ___kCalculator_0)); }
	inline RuntimeObject* get_kCalculator_0() const { return ___kCalculator_0; }
	inline RuntimeObject** get_address_of_kCalculator_0() { return &___kCalculator_0; }
	inline void set_kCalculator_0(RuntimeObject* value)
	{
		___kCalculator_0 = value;
		Il2CppCodeGenWriteBarrier((&___kCalculator_0), value);
	}

	inline static int32_t get_offset_of_key_1() { return static_cast<int32_t>(offsetof(DsaSigner_t1B94B42B08AC7EB937B05FC6DCF361A1BA5BFB66, ___key_1)); }
	inline DsaKeyParameters_tBF3E3B9B02243E5C768006D70F4CA496F15E82A6 * get_key_1() const { return ___key_1; }
	inline DsaKeyParameters_tBF3E3B9B02243E5C768006D70F4CA496F15E82A6 ** get_address_of_key_1() { return &___key_1; }
	inline void set_key_1(DsaKeyParameters_tBF3E3B9B02243E5C768006D70F4CA496F15E82A6 * value)
	{
		___key_1 = value;
		Il2CppCodeGenWriteBarrier((&___key_1), value);
	}

	inline static int32_t get_offset_of_random_2() { return static_cast<int32_t>(offsetof(DsaSigner_t1B94B42B08AC7EB937B05FC6DCF361A1BA5BFB66, ___random_2)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_random_2() const { return ___random_2; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_random_2() { return &___random_2; }
	inline void set_random_2(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___random_2 = value;
		Il2CppCodeGenWriteBarrier((&___random_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSASIGNER_T1B94B42B08AC7EB937B05FC6DCF361A1BA5BFB66_H
#ifndef ECDSASIGNER_T277BCD15E0EC9F82F5399911E2E7BD61D2951AC8_H
#define ECDSASIGNER_T277BCD15E0EC9F82F5399911E2E7BD61D2951AC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Signers.ECDsaSigner
struct  ECDsaSigner_t277BCD15E0EC9F82F5399911E2E7BD61D2951AC8  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Signers.IDsaKCalculator Org.BouncyCastle.Crypto.Signers.ECDsaSigner::kCalculator
	RuntimeObject* ___kCalculator_1;
	// Org.BouncyCastle.Crypto.Parameters.ECKeyParameters Org.BouncyCastle.Crypto.Signers.ECDsaSigner::key
	ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1 * ___key_2;
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Crypto.Signers.ECDsaSigner::random
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___random_3;

public:
	inline static int32_t get_offset_of_kCalculator_1() { return static_cast<int32_t>(offsetof(ECDsaSigner_t277BCD15E0EC9F82F5399911E2E7BD61D2951AC8, ___kCalculator_1)); }
	inline RuntimeObject* get_kCalculator_1() const { return ___kCalculator_1; }
	inline RuntimeObject** get_address_of_kCalculator_1() { return &___kCalculator_1; }
	inline void set_kCalculator_1(RuntimeObject* value)
	{
		___kCalculator_1 = value;
		Il2CppCodeGenWriteBarrier((&___kCalculator_1), value);
	}

	inline static int32_t get_offset_of_key_2() { return static_cast<int32_t>(offsetof(ECDsaSigner_t277BCD15E0EC9F82F5399911E2E7BD61D2951AC8, ___key_2)); }
	inline ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1 * get_key_2() const { return ___key_2; }
	inline ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1 ** get_address_of_key_2() { return &___key_2; }
	inline void set_key_2(ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1 * value)
	{
		___key_2 = value;
		Il2CppCodeGenWriteBarrier((&___key_2), value);
	}

	inline static int32_t get_offset_of_random_3() { return static_cast<int32_t>(offsetof(ECDsaSigner_t277BCD15E0EC9F82F5399911E2E7BD61D2951AC8, ___random_3)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_random_3() const { return ___random_3; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_random_3() { return &___random_3; }
	inline void set_random_3(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___random_3 = value;
		Il2CppCodeGenWriteBarrier((&___random_3), value);
	}
};

struct ECDsaSigner_t277BCD15E0EC9F82F5399911E2E7BD61D2951AC8_StaticFields
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Signers.ECDsaSigner::Eight
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___Eight_0;

public:
	inline static int32_t get_offset_of_Eight_0() { return static_cast<int32_t>(offsetof(ECDsaSigner_t277BCD15E0EC9F82F5399911E2E7BD61D2951AC8_StaticFields, ___Eight_0)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_Eight_0() const { return ___Eight_0; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_Eight_0() { return &___Eight_0; }
	inline void set_Eight_0(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___Eight_0 = value;
		Il2CppCodeGenWriteBarrier((&___Eight_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECDSASIGNER_T277BCD15E0EC9F82F5399911E2E7BD61D2951AC8_H
#ifndef GENERICSIGNER_T1303EADD2989EE6C3E682E0CE5D583104B524406_H
#define GENERICSIGNER_T1303EADD2989EE6C3E682E0CE5D583104B524406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Signers.GenericSigner
struct  GenericSigner_t1303EADD2989EE6C3E682E0CE5D583104B524406  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.IAsymmetricBlockCipher Org.BouncyCastle.Crypto.Signers.GenericSigner::engine
	RuntimeObject* ___engine_0;
	// Org.BouncyCastle.Crypto.IDigest Org.BouncyCastle.Crypto.Signers.GenericSigner::digest
	RuntimeObject* ___digest_1;
	// System.Boolean Org.BouncyCastle.Crypto.Signers.GenericSigner::forSigning
	bool ___forSigning_2;

public:
	inline static int32_t get_offset_of_engine_0() { return static_cast<int32_t>(offsetof(GenericSigner_t1303EADD2989EE6C3E682E0CE5D583104B524406, ___engine_0)); }
	inline RuntimeObject* get_engine_0() const { return ___engine_0; }
	inline RuntimeObject** get_address_of_engine_0() { return &___engine_0; }
	inline void set_engine_0(RuntimeObject* value)
	{
		___engine_0 = value;
		Il2CppCodeGenWriteBarrier((&___engine_0), value);
	}

	inline static int32_t get_offset_of_digest_1() { return static_cast<int32_t>(offsetof(GenericSigner_t1303EADD2989EE6C3E682E0CE5D583104B524406, ___digest_1)); }
	inline RuntimeObject* get_digest_1() const { return ___digest_1; }
	inline RuntimeObject** get_address_of_digest_1() { return &___digest_1; }
	inline void set_digest_1(RuntimeObject* value)
	{
		___digest_1 = value;
		Il2CppCodeGenWriteBarrier((&___digest_1), value);
	}

	inline static int32_t get_offset_of_forSigning_2() { return static_cast<int32_t>(offsetof(GenericSigner_t1303EADD2989EE6C3E682E0CE5D583104B524406, ___forSigning_2)); }
	inline bool get_forSigning_2() const { return ___forSigning_2; }
	inline bool* get_address_of_forSigning_2() { return &___forSigning_2; }
	inline void set_forSigning_2(bool value)
	{
		___forSigning_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICSIGNER_T1303EADD2989EE6C3E682E0CE5D583104B524406_H
#ifndef HMACDSAKCALCULATOR_T13E93BFD5A7F7018A41BDB29E900A2D9CC76A684_H
#define HMACDSAKCALCULATOR_T13E93BFD5A7F7018A41BDB29E900A2D9CC76A684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Signers.HMacDsaKCalculator
struct  HMacDsaKCalculator_t13E93BFD5A7F7018A41BDB29E900A2D9CC76A684  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Macs.HMac Org.BouncyCastle.Crypto.Signers.HMacDsaKCalculator::hMac
	HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB * ___hMac_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Signers.HMacDsaKCalculator::K
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___K_1;
	// System.Byte[] Org.BouncyCastle.Crypto.Signers.HMacDsaKCalculator::V
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___V_2;

public:
	inline static int32_t get_offset_of_hMac_0() { return static_cast<int32_t>(offsetof(HMacDsaKCalculator_t13E93BFD5A7F7018A41BDB29E900A2D9CC76A684, ___hMac_0)); }
	inline HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB * get_hMac_0() const { return ___hMac_0; }
	inline HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB ** get_address_of_hMac_0() { return &___hMac_0; }
	inline void set_hMac_0(HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB * value)
	{
		___hMac_0 = value;
		Il2CppCodeGenWriteBarrier((&___hMac_0), value);
	}

	inline static int32_t get_offset_of_K_1() { return static_cast<int32_t>(offsetof(HMacDsaKCalculator_t13E93BFD5A7F7018A41BDB29E900A2D9CC76A684, ___K_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_K_1() const { return ___K_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_K_1() { return &___K_1; }
	inline void set_K_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___K_1 = value;
		Il2CppCodeGenWriteBarrier((&___K_1), value);
	}

	inline static int32_t get_offset_of_V_2() { return static_cast<int32_t>(offsetof(HMacDsaKCalculator_t13E93BFD5A7F7018A41BDB29E900A2D9CC76A684, ___V_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_V_2() const { return ___V_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_V_2() { return &___V_2; }
	inline void set_V_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___V_2 = value;
		Il2CppCodeGenWriteBarrier((&___V_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMACDSAKCALCULATOR_T13E93BFD5A7F7018A41BDB29E900A2D9CC76A684_H
#ifndef RSADIGESTSIGNER_T41FCA79CA5D0AF546AFA029B417F135C311F75F9_H
#define RSADIGESTSIGNER_T41FCA79CA5D0AF546AFA029B417F135C311F75F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Signers.RsaDigestSigner
struct  RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.IAsymmetricBlockCipher Org.BouncyCastle.Crypto.Signers.RsaDigestSigner::rsaEngine
	RuntimeObject* ___rsaEngine_0;
	// Org.BouncyCastle.Asn1.X509.AlgorithmIdentifier Org.BouncyCastle.Crypto.Signers.RsaDigestSigner::algId
	AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * ___algId_1;
	// Org.BouncyCastle.Crypto.IDigest Org.BouncyCastle.Crypto.Signers.RsaDigestSigner::digest
	RuntimeObject* ___digest_2;
	// System.Boolean Org.BouncyCastle.Crypto.Signers.RsaDigestSigner::forSigning
	bool ___forSigning_3;

public:
	inline static int32_t get_offset_of_rsaEngine_0() { return static_cast<int32_t>(offsetof(RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9, ___rsaEngine_0)); }
	inline RuntimeObject* get_rsaEngine_0() const { return ___rsaEngine_0; }
	inline RuntimeObject** get_address_of_rsaEngine_0() { return &___rsaEngine_0; }
	inline void set_rsaEngine_0(RuntimeObject* value)
	{
		___rsaEngine_0 = value;
		Il2CppCodeGenWriteBarrier((&___rsaEngine_0), value);
	}

	inline static int32_t get_offset_of_algId_1() { return static_cast<int32_t>(offsetof(RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9, ___algId_1)); }
	inline AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * get_algId_1() const { return ___algId_1; }
	inline AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB ** get_address_of_algId_1() { return &___algId_1; }
	inline void set_algId_1(AlgorithmIdentifier_tDE7123E83DAD8C219BBB99DAE8CAAD8A2AE64EFB * value)
	{
		___algId_1 = value;
		Il2CppCodeGenWriteBarrier((&___algId_1), value);
	}

	inline static int32_t get_offset_of_digest_2() { return static_cast<int32_t>(offsetof(RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9, ___digest_2)); }
	inline RuntimeObject* get_digest_2() const { return ___digest_2; }
	inline RuntimeObject** get_address_of_digest_2() { return &___digest_2; }
	inline void set_digest_2(RuntimeObject* value)
	{
		___digest_2 = value;
		Il2CppCodeGenWriteBarrier((&___digest_2), value);
	}

	inline static int32_t get_offset_of_forSigning_3() { return static_cast<int32_t>(offsetof(RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9, ___forSigning_3)); }
	inline bool get_forSigning_3() const { return ___forSigning_3; }
	inline bool* get_address_of_forSigning_3() { return &___forSigning_3; }
	inline void set_forSigning_3(bool value)
	{
		___forSigning_3 = value;
	}
};

struct RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9_StaticFields
{
public:
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.Signers.RsaDigestSigner::oidMap
	RuntimeObject* ___oidMap_4;

public:
	inline static int32_t get_offset_of_oidMap_4() { return static_cast<int32_t>(offsetof(RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9_StaticFields, ___oidMap_4)); }
	inline RuntimeObject* get_oidMap_4() const { return ___oidMap_4; }
	inline RuntimeObject** get_address_of_oidMap_4() { return &___oidMap_4; }
	inline void set_oidMap_4(RuntimeObject* value)
	{
		___oidMap_4 = value;
		Il2CppCodeGenWriteBarrier((&___oidMap_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSADIGESTSIGNER_T41FCA79CA5D0AF546AFA029B417F135C311F75F9_H
#ifndef ABSTRACTTLSCIPHERFACTORY_T95A664781E8103D14B4E9186E35D2826C17C47FB_H
#define ABSTRACTTLSCIPHERFACTORY_T95A664781E8103D14B4E9186E35D2826C17C47FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AbstractTlsCipherFactory
struct  AbstractTlsCipherFactory_t95A664781E8103D14B4E9186E35D2826C17C47FB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCIPHERFACTORY_T95A664781E8103D14B4E9186E35D2826C17C47FB_H
#ifndef ABSTRACTTLSCONTEXT_T6443BAD6549F9D29772C07D635DD73AB72E2D418_H
#define ABSTRACTTLSCONTEXT_T6443BAD6549F9D29772C07D635DD73AB72E2D418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AbstractTlsContext
struct  AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Prng.IRandomGenerator Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mNonceRandom
	RuntimeObject* ___mNonceRandom_1;
	// Org.BouncyCastle.Security.SecureRandom Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSecureRandom
	SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * ___mSecureRandom_2;
	// Org.BouncyCastle.Crypto.Tls.SecurityParameters Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSecurityParameters
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB * ___mSecurityParameters_3;
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mClientVersion
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * ___mClientVersion_4;
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mServerVersion
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * ___mServerVersion_5;
	// Org.BouncyCastle.Crypto.Tls.TlsSession Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::mSession
	RuntimeObject* ___mSession_6;

public:
	inline static int32_t get_offset_of_mNonceRandom_1() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418, ___mNonceRandom_1)); }
	inline RuntimeObject* get_mNonceRandom_1() const { return ___mNonceRandom_1; }
	inline RuntimeObject** get_address_of_mNonceRandom_1() { return &___mNonceRandom_1; }
	inline void set_mNonceRandom_1(RuntimeObject* value)
	{
		___mNonceRandom_1 = value;
		Il2CppCodeGenWriteBarrier((&___mNonceRandom_1), value);
	}

	inline static int32_t get_offset_of_mSecureRandom_2() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418, ___mSecureRandom_2)); }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * get_mSecureRandom_2() const { return ___mSecureRandom_2; }
	inline SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 ** get_address_of_mSecureRandom_2() { return &___mSecureRandom_2; }
	inline void set_mSecureRandom_2(SecureRandom_t0B6C2C3EDF11BF4FD22E44CE8997F6A0D2F142B0 * value)
	{
		___mSecureRandom_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSecureRandom_2), value);
	}

	inline static int32_t get_offset_of_mSecurityParameters_3() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418, ___mSecurityParameters_3)); }
	inline SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB * get_mSecurityParameters_3() const { return ___mSecurityParameters_3; }
	inline SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB ** get_address_of_mSecurityParameters_3() { return &___mSecurityParameters_3; }
	inline void set_mSecurityParameters_3(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB * value)
	{
		___mSecurityParameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___mSecurityParameters_3), value);
	}

	inline static int32_t get_offset_of_mClientVersion_4() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418, ___mClientVersion_4)); }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * get_mClientVersion_4() const { return ___mClientVersion_4; }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D ** get_address_of_mClientVersion_4() { return &___mClientVersion_4; }
	inline void set_mClientVersion_4(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * value)
	{
		___mClientVersion_4 = value;
		Il2CppCodeGenWriteBarrier((&___mClientVersion_4), value);
	}

	inline static int32_t get_offset_of_mServerVersion_5() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418, ___mServerVersion_5)); }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * get_mServerVersion_5() const { return ___mServerVersion_5; }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D ** get_address_of_mServerVersion_5() { return &___mServerVersion_5; }
	inline void set_mServerVersion_5(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * value)
	{
		___mServerVersion_5 = value;
		Il2CppCodeGenWriteBarrier((&___mServerVersion_5), value);
	}

	inline static int32_t get_offset_of_mSession_6() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418, ___mSession_6)); }
	inline RuntimeObject* get_mSession_6() const { return ___mSession_6; }
	inline RuntimeObject** get_address_of_mSession_6() { return &___mSession_6; }
	inline void set_mSession_6(RuntimeObject* value)
	{
		___mSession_6 = value;
		Il2CppCodeGenWriteBarrier((&___mSession_6), value);
	}
};

struct AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418_StaticFields
{
public:
	// System.Int64 Org.BouncyCastle.Crypto.Tls.AbstractTlsContext::counter
	int64_t ___counter_0;

public:
	inline static int32_t get_offset_of_counter_0() { return static_cast<int32_t>(offsetof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418_StaticFields, ___counter_0)); }
	inline int64_t get_counter_0() const { return ___counter_0; }
	inline int64_t* get_address_of_counter_0() { return &___counter_0; }
	inline void set_counter_0(int64_t value)
	{
		___counter_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCONTEXT_T6443BAD6549F9D29772C07D635DD73AB72E2D418_H
#ifndef ABSTRACTTLSKEYEXCHANGE_T1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35_H
#define ABSTRACTTLSKEYEXCHANGE_T1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange
struct  AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mKeyExchange
	int32_t ___mKeyExchange_0;
	// System.Collections.IList Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_1;
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.AbstractTlsKeyExchange::mContext
	RuntimeObject* ___mContext_2;

public:
	inline static int32_t get_offset_of_mKeyExchange_0() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35, ___mKeyExchange_0)); }
	inline int32_t get_mKeyExchange_0() const { return ___mKeyExchange_0; }
	inline int32_t* get_address_of_mKeyExchange_0() { return &___mKeyExchange_0; }
	inline void set_mKeyExchange_0(int32_t value)
	{
		___mKeyExchange_0 = value;
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_1() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35, ___mSupportedSignatureAlgorithms_1)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_1() const { return ___mSupportedSignatureAlgorithms_1; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_1() { return &___mSupportedSignatureAlgorithms_1; }
	inline void set_mSupportedSignatureAlgorithms_1(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_1), value);
	}

	inline static int32_t get_offset_of_mContext_2() { return static_cast<int32_t>(offsetof(AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35, ___mContext_2)); }
	inline RuntimeObject* get_mContext_2() const { return ___mContext_2; }
	inline RuntimeObject** get_address_of_mContext_2() { return &___mContext_2; }
	inline void set_mContext_2(RuntimeObject* value)
	{
		___mContext_2 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSKEYEXCHANGE_T1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35_H
#ifndef ABSTRACTTLSPEER_T26CA3EF032C693BEB7331E2A0972A8620596CC9F_H
#define ABSTRACTTLSPEER_T26CA3EF032C693BEB7331E2A0972A8620596CC9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AbstractTlsPeer
struct  AbstractTlsPeer_t26CA3EF032C693BEB7331E2A0972A8620596CC9F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSPEER_T26CA3EF032C693BEB7331E2A0972A8620596CC9F_H
#ifndef ABSTRACTTLSSIGNER_T69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE_H
#define ABSTRACTTLSSIGNER_T69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AbstractTlsSigner
struct  AbstractTlsSigner_t69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.AbstractTlsSigner::mContext
	RuntimeObject* ___mContext_0;

public:
	inline static int32_t get_offset_of_mContext_0() { return static_cast<int32_t>(offsetof(AbstractTlsSigner_t69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE, ___mContext_0)); }
	inline RuntimeObject* get_mContext_0() const { return ___mContext_0; }
	inline RuntimeObject** get_address_of_mContext_0() { return &___mContext_0; }
	inline void set_mContext_0(RuntimeObject* value)
	{
		___mContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSSIGNER_T69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE_H
#ifndef ALERTDESCRIPTION_TA8DB3E28E891DC1EAD532B6131B48917E4C3B2D3_H
#define ALERTDESCRIPTION_TA8DB3E28E891DC1EAD532B6131B48917E4C3B2D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AlertDescription
struct  AlertDescription_tA8DB3E28E891DC1EAD532B6131B48917E4C3B2D3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTDESCRIPTION_TA8DB3E28E891DC1EAD532B6131B48917E4C3B2D3_H
#ifndef BYTEQUEUE_TBA06263CBE5934691B2E38C8E4B5523329D92B26_H
#define BYTEQUEUE_TBA06263CBE5934691B2E38C8E4B5523329D92B26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.ByteQueue
struct  ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.ByteQueue::databuf
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___databuf_0;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.ByteQueue::skipped
	int32_t ___skipped_1;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.ByteQueue::available
	int32_t ___available_2;

public:
	inline static int32_t get_offset_of_databuf_0() { return static_cast<int32_t>(offsetof(ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26, ___databuf_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_databuf_0() const { return ___databuf_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_databuf_0() { return &___databuf_0; }
	inline void set_databuf_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___databuf_0 = value;
		Il2CppCodeGenWriteBarrier((&___databuf_0), value);
	}

	inline static int32_t get_offset_of_skipped_1() { return static_cast<int32_t>(offsetof(ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26, ___skipped_1)); }
	inline int32_t get_skipped_1() const { return ___skipped_1; }
	inline int32_t* get_address_of_skipped_1() { return &___skipped_1; }
	inline void set_skipped_1(int32_t value)
	{
		___skipped_1 = value;
	}

	inline static int32_t get_offset_of_available_2() { return static_cast<int32_t>(offsetof(ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26, ___available_2)); }
	inline int32_t get_available_2() const { return ___available_2; }
	inline int32_t* get_address_of_available_2() { return &___available_2; }
	inline void set_available_2(int32_t value)
	{
		___available_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEQUEUE_TBA06263CBE5934691B2E38C8E4B5523329D92B26_H
#ifndef CERTIFICATE_T609B55043542EA8131A286ABB40D1471185F0E8D_H
#define CERTIFICATE_T609B55043542EA8131A286ABB40D1471185F0E8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.Certificate
struct  Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Asn1.X509.X509CertificateStructure[] Org.BouncyCastle.Crypto.Tls.Certificate::mCertificateList
	X509CertificateStructureU5BU5D_t059BC84686310964E2F9838556B39AD90F2370B5* ___mCertificateList_1;

public:
	inline static int32_t get_offset_of_mCertificateList_1() { return static_cast<int32_t>(offsetof(Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D, ___mCertificateList_1)); }
	inline X509CertificateStructureU5BU5D_t059BC84686310964E2F9838556B39AD90F2370B5* get_mCertificateList_1() const { return ___mCertificateList_1; }
	inline X509CertificateStructureU5BU5D_t059BC84686310964E2F9838556B39AD90F2370B5** get_address_of_mCertificateList_1() { return &___mCertificateList_1; }
	inline void set_mCertificateList_1(X509CertificateStructureU5BU5D_t059BC84686310964E2F9838556B39AD90F2370B5* value)
	{
		___mCertificateList_1 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificateList_1), value);
	}
};

struct Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D_StaticFields
{
public:
	// Org.BouncyCastle.Crypto.Tls.Certificate Org.BouncyCastle.Crypto.Tls.Certificate::EmptyChain
	Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D * ___EmptyChain_0;

public:
	inline static int32_t get_offset_of_EmptyChain_0() { return static_cast<int32_t>(offsetof(Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D_StaticFields, ___EmptyChain_0)); }
	inline Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D * get_EmptyChain_0() const { return ___EmptyChain_0; }
	inline Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D ** get_address_of_EmptyChain_0() { return &___EmptyChain_0; }
	inline void set_EmptyChain_0(Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D * value)
	{
		___EmptyChain_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyChain_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATE_T609B55043542EA8131A286ABB40D1471185F0E8D_H
#ifndef CERTIFICATEREQUEST_T2D3D094933C7C24EA9ED681B96C590E0488E0105_H
#define CERTIFICATEREQUEST_T2D3D094933C7C24EA9ED681B96C590E0488E0105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.CertificateRequest
struct  CertificateRequest_t2D3D094933C7C24EA9ED681B96C590E0488E0105  : public RuntimeObject
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.CertificateRequest::mCertificateTypes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mCertificateTypes_0;
	// System.Collections.IList Org.BouncyCastle.Crypto.Tls.CertificateRequest::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_1;
	// System.Collections.IList Org.BouncyCastle.Crypto.Tls.CertificateRequest::mCertificateAuthorities
	RuntimeObject* ___mCertificateAuthorities_2;

public:
	inline static int32_t get_offset_of_mCertificateTypes_0() { return static_cast<int32_t>(offsetof(CertificateRequest_t2D3D094933C7C24EA9ED681B96C590E0488E0105, ___mCertificateTypes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mCertificateTypes_0() const { return ___mCertificateTypes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mCertificateTypes_0() { return &___mCertificateTypes_0; }
	inline void set_mCertificateTypes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mCertificateTypes_0 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificateTypes_0), value);
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_1() { return static_cast<int32_t>(offsetof(CertificateRequest_t2D3D094933C7C24EA9ED681B96C590E0488E0105, ___mSupportedSignatureAlgorithms_1)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_1() const { return ___mSupportedSignatureAlgorithms_1; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_1() { return &___mSupportedSignatureAlgorithms_1; }
	inline void set_mSupportedSignatureAlgorithms_1(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_1), value);
	}

	inline static int32_t get_offset_of_mCertificateAuthorities_2() { return static_cast<int32_t>(offsetof(CertificateRequest_t2D3D094933C7C24EA9ED681B96C590E0488E0105, ___mCertificateAuthorities_2)); }
	inline RuntimeObject* get_mCertificateAuthorities_2() const { return ___mCertificateAuthorities_2; }
	inline RuntimeObject** get_address_of_mCertificateAuthorities_2() { return &___mCertificateAuthorities_2; }
	inline void set_mCertificateAuthorities_2(RuntimeObject* value)
	{
		___mCertificateAuthorities_2 = value;
		Il2CppCodeGenWriteBarrier((&___mCertificateAuthorities_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATEREQUEST_T2D3D094933C7C24EA9ED681B96C590E0488E0105_H
#ifndef CERTIFICATESTATUS_T1F391A4805FD1E6E500F7F5AD46A312FF7057AEA_H
#define CERTIFICATESTATUS_T1F391A4805FD1E6E500F7F5AD46A312FF7057AEA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.CertificateStatus
struct  CertificateStatus_t1F391A4805FD1E6E500F7F5AD46A312FF7057AEA  : public RuntimeObject
{
public:
	// System.Byte Org.BouncyCastle.Crypto.Tls.CertificateStatus::mStatusType
	uint8_t ___mStatusType_0;
	// System.Object Org.BouncyCastle.Crypto.Tls.CertificateStatus::mResponse
	RuntimeObject * ___mResponse_1;

public:
	inline static int32_t get_offset_of_mStatusType_0() { return static_cast<int32_t>(offsetof(CertificateStatus_t1F391A4805FD1E6E500F7F5AD46A312FF7057AEA, ___mStatusType_0)); }
	inline uint8_t get_mStatusType_0() const { return ___mStatusType_0; }
	inline uint8_t* get_address_of_mStatusType_0() { return &___mStatusType_0; }
	inline void set_mStatusType_0(uint8_t value)
	{
		___mStatusType_0 = value;
	}

	inline static int32_t get_offset_of_mResponse_1() { return static_cast<int32_t>(offsetof(CertificateStatus_t1F391A4805FD1E6E500F7F5AD46A312FF7057AEA, ___mResponse_1)); }
	inline RuntimeObject * get_mResponse_1() const { return ___mResponse_1; }
	inline RuntimeObject ** get_address_of_mResponse_1() { return &___mResponse_1; }
	inline void set_mResponse_1(RuntimeObject * value)
	{
		___mResponse_1 = value;
		Il2CppCodeGenWriteBarrier((&___mResponse_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTIFICATESTATUS_T1F391A4805FD1E6E500F7F5AD46A312FF7057AEA_H
#ifndef CHACHA20POLY1305_T3B7CD0A35BE52CEE0E3D54B5792D9A6305E77FF4_H
#define CHACHA20POLY1305_T3B7CD0A35BE52CEE0E3D54B5792D9A6305E77FF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305
struct  Chacha20Poly1305_t3B7CD0A35BE52CEE0E3D54B5792D9A6305E77FF4  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::context
	RuntimeObject* ___context_0;
	// Org.BouncyCastle.Crypto.Engines.ChaChaEngine Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::encryptCipher
	ChaChaEngine_t6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2 * ___encryptCipher_1;
	// Org.BouncyCastle.Crypto.Engines.ChaChaEngine Org.BouncyCastle.Crypto.Tls.Chacha20Poly1305::decryptCipher
	ChaChaEngine_t6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2 * ___decryptCipher_2;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_t3B7CD0A35BE52CEE0E3D54B5792D9A6305E77FF4, ___context_0)); }
	inline RuntimeObject* get_context_0() const { return ___context_0; }
	inline RuntimeObject** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(RuntimeObject* value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}

	inline static int32_t get_offset_of_encryptCipher_1() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_t3B7CD0A35BE52CEE0E3D54B5792D9A6305E77FF4, ___encryptCipher_1)); }
	inline ChaChaEngine_t6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2 * get_encryptCipher_1() const { return ___encryptCipher_1; }
	inline ChaChaEngine_t6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2 ** get_address_of_encryptCipher_1() { return &___encryptCipher_1; }
	inline void set_encryptCipher_1(ChaChaEngine_t6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2 * value)
	{
		___encryptCipher_1 = value;
		Il2CppCodeGenWriteBarrier((&___encryptCipher_1), value);
	}

	inline static int32_t get_offset_of_decryptCipher_2() { return static_cast<int32_t>(offsetof(Chacha20Poly1305_t3B7CD0A35BE52CEE0E3D54B5792D9A6305E77FF4, ___decryptCipher_2)); }
	inline ChaChaEngine_t6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2 * get_decryptCipher_2() const { return ___decryptCipher_2; }
	inline ChaChaEngine_t6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2 ** get_address_of_decryptCipher_2() { return &___decryptCipher_2; }
	inline void set_decryptCipher_2(ChaChaEngine_t6CC53FC5D18C19BC24AF41B6A6D0B073CA6A36B2 * value)
	{
		___decryptCipher_2 = value;
		Il2CppCodeGenWriteBarrier((&___decryptCipher_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHACHA20POLY1305_T3B7CD0A35BE52CEE0E3D54B5792D9A6305E77FF4_H
#ifndef CIPHERSUITE_T002556B7A727BFF16214821A1AB5E42FFD19447E_H
#define CIPHERSUITE_T002556B7A727BFF16214821A1AB5E42FFD19447E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.CipherSuite
struct  CipherSuite_t002556B7A727BFF16214821A1AB5E42FFD19447E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERSUITE_T002556B7A727BFF16214821A1AB5E42FFD19447E_H
#ifndef COMBINEDHASH_TA06AFB6B3C4AD80E8570D303A27982F3860D6EDA_H
#define COMBINEDHASH_TA06AFB6B3C4AD80E8570D303A27982F3860D6EDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.CombinedHash
struct  CombinedHash_tA06AFB6B3C4AD80E8570D303A27982F3860D6EDA  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.CombinedHash::mContext
	RuntimeObject* ___mContext_0;
	// Org.BouncyCastle.Crypto.IDigest Org.BouncyCastle.Crypto.Tls.CombinedHash::mMd5
	RuntimeObject* ___mMd5_1;
	// Org.BouncyCastle.Crypto.IDigest Org.BouncyCastle.Crypto.Tls.CombinedHash::mSha1
	RuntimeObject* ___mSha1_2;

public:
	inline static int32_t get_offset_of_mContext_0() { return static_cast<int32_t>(offsetof(CombinedHash_tA06AFB6B3C4AD80E8570D303A27982F3860D6EDA, ___mContext_0)); }
	inline RuntimeObject* get_mContext_0() const { return ___mContext_0; }
	inline RuntimeObject** get_address_of_mContext_0() { return &___mContext_0; }
	inline void set_mContext_0(RuntimeObject* value)
	{
		___mContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_0), value);
	}

	inline static int32_t get_offset_of_mMd5_1() { return static_cast<int32_t>(offsetof(CombinedHash_tA06AFB6B3C4AD80E8570D303A27982F3860D6EDA, ___mMd5_1)); }
	inline RuntimeObject* get_mMd5_1() const { return ___mMd5_1; }
	inline RuntimeObject** get_address_of_mMd5_1() { return &___mMd5_1; }
	inline void set_mMd5_1(RuntimeObject* value)
	{
		___mMd5_1 = value;
		Il2CppCodeGenWriteBarrier((&___mMd5_1), value);
	}

	inline static int32_t get_offset_of_mSha1_2() { return static_cast<int32_t>(offsetof(CombinedHash_tA06AFB6B3C4AD80E8570D303A27982F3860D6EDA, ___mSha1_2)); }
	inline RuntimeObject* get_mSha1_2() const { return ___mSha1_2; }
	inline RuntimeObject** get_address_of_mSha1_2() { return &___mSha1_2; }
	inline void set_mSha1_2(RuntimeObject* value)
	{
		___mSha1_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSha1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMBINEDHASH_TA06AFB6B3C4AD80E8570D303A27982F3860D6EDA_H
#ifndef DEFERREDHASH_T9ADE64D47806150A3D10D105378AE6C23E6B70BF_H
#define DEFERREDHASH_T9ADE64D47806150A3D10D105378AE6C23E6B70BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.DeferredHash
struct  DeferredHash_t9ADE64D47806150A3D10D105378AE6C23E6B70BF  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.DeferredHash::mContext
	RuntimeObject* ___mContext_0;
	// Org.BouncyCastle.Crypto.Tls.DigestInputBuffer Org.BouncyCastle.Crypto.Tls.DeferredHash::mBuf
	DigestInputBuffer_t117CEE4FCA532842726AF5BFE55FFDFC708B811D * ___mBuf_1;
	// System.Collections.IDictionary Org.BouncyCastle.Crypto.Tls.DeferredHash::mHashes
	RuntimeObject* ___mHashes_2;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.DeferredHash::mPrfHashAlgorithm
	int32_t ___mPrfHashAlgorithm_3;

public:
	inline static int32_t get_offset_of_mContext_0() { return static_cast<int32_t>(offsetof(DeferredHash_t9ADE64D47806150A3D10D105378AE6C23E6B70BF, ___mContext_0)); }
	inline RuntimeObject* get_mContext_0() const { return ___mContext_0; }
	inline RuntimeObject** get_address_of_mContext_0() { return &___mContext_0; }
	inline void set_mContext_0(RuntimeObject* value)
	{
		___mContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_0), value);
	}

	inline static int32_t get_offset_of_mBuf_1() { return static_cast<int32_t>(offsetof(DeferredHash_t9ADE64D47806150A3D10D105378AE6C23E6B70BF, ___mBuf_1)); }
	inline DigestInputBuffer_t117CEE4FCA532842726AF5BFE55FFDFC708B811D * get_mBuf_1() const { return ___mBuf_1; }
	inline DigestInputBuffer_t117CEE4FCA532842726AF5BFE55FFDFC708B811D ** get_address_of_mBuf_1() { return &___mBuf_1; }
	inline void set_mBuf_1(DigestInputBuffer_t117CEE4FCA532842726AF5BFE55FFDFC708B811D * value)
	{
		___mBuf_1 = value;
		Il2CppCodeGenWriteBarrier((&___mBuf_1), value);
	}

	inline static int32_t get_offset_of_mHashes_2() { return static_cast<int32_t>(offsetof(DeferredHash_t9ADE64D47806150A3D10D105378AE6C23E6B70BF, ___mHashes_2)); }
	inline RuntimeObject* get_mHashes_2() const { return ___mHashes_2; }
	inline RuntimeObject** get_address_of_mHashes_2() { return &___mHashes_2; }
	inline void set_mHashes_2(RuntimeObject* value)
	{
		___mHashes_2 = value;
		Il2CppCodeGenWriteBarrier((&___mHashes_2), value);
	}

	inline static int32_t get_offset_of_mPrfHashAlgorithm_3() { return static_cast<int32_t>(offsetof(DeferredHash_t9ADE64D47806150A3D10D105378AE6C23E6B70BF, ___mPrfHashAlgorithm_3)); }
	inline int32_t get_mPrfHashAlgorithm_3() const { return ___mPrfHashAlgorithm_3; }
	inline int32_t* get_address_of_mPrfHashAlgorithm_3() { return &___mPrfHashAlgorithm_3; }
	inline void set_mPrfHashAlgorithm_3(int32_t value)
	{
		___mPrfHashAlgorithm_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFERREDHASH_T9ADE64D47806150A3D10D105378AE6C23E6B70BF_H
#ifndef DIGITALLYSIGNED_T9DFC1BA43FD4F9AF87418E4E7EF5149EA31ED8C7_H
#define DIGITALLYSIGNED_T9DFC1BA43FD4F9AF87418E4E7EF5149EA31ED8C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.DigitallySigned
struct  DigitallySigned_t9DFC1BA43FD4F9AF87418E4E7EF5149EA31ED8C7  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.SignatureAndHashAlgorithm Org.BouncyCastle.Crypto.Tls.DigitallySigned::mAlgorithm
	SignatureAndHashAlgorithm_tDEA4E53D4DC207FA196758B3B4848104689854A9 * ___mAlgorithm_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.DigitallySigned::mSignature
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSignature_1;

public:
	inline static int32_t get_offset_of_mAlgorithm_0() { return static_cast<int32_t>(offsetof(DigitallySigned_t9DFC1BA43FD4F9AF87418E4E7EF5149EA31ED8C7, ___mAlgorithm_0)); }
	inline SignatureAndHashAlgorithm_tDEA4E53D4DC207FA196758B3B4848104689854A9 * get_mAlgorithm_0() const { return ___mAlgorithm_0; }
	inline SignatureAndHashAlgorithm_tDEA4E53D4DC207FA196758B3B4848104689854A9 ** get_address_of_mAlgorithm_0() { return &___mAlgorithm_0; }
	inline void set_mAlgorithm_0(SignatureAndHashAlgorithm_tDEA4E53D4DC207FA196758B3B4848104689854A9 * value)
	{
		___mAlgorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&___mAlgorithm_0), value);
	}

	inline static int32_t get_offset_of_mSignature_1() { return static_cast<int32_t>(offsetof(DigitallySigned_t9DFC1BA43FD4F9AF87418E4E7EF5149EA31ED8C7, ___mSignature_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSignature_1() const { return ___mSignature_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSignature_1() { return &___mSignature_1; }
	inline void set_mSignature_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSignature_1 = value;
		Il2CppCodeGenWriteBarrier((&___mSignature_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGITALLYSIGNED_T9DFC1BA43FD4F9AF87418E4E7EF5149EA31ED8C7_H
#ifndef ECBASISTYPE_TF8EDD91568D009AA5CFB6DA005F76AB2B90DB5BB_H
#define ECBASISTYPE_TF8EDD91568D009AA5CFB6DA005F76AB2B90DB5BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.ECBasisType
struct  ECBasisType_tF8EDD91568D009AA5CFB6DA005F76AB2B90DB5BB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECBASISTYPE_TF8EDD91568D009AA5CFB6DA005F76AB2B90DB5BB_H
#ifndef EXPORTERLABEL_T64DE4AE70A2960BAF4CF5081306E3B2AB94F13A9_H
#define EXPORTERLABEL_T64DE4AE70A2960BAF4CF5081306E3B2AB94F13A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.ExporterLabel
struct  ExporterLabel_t64DE4AE70A2960BAF4CF5081306E3B2AB94F13A9  : public RuntimeObject
{
public:

public:
};

struct ExporterLabel_t64DE4AE70A2960BAF4CF5081306E3B2AB94F13A9_StaticFields
{
public:
	// System.String Org.BouncyCastle.Crypto.Tls.ExporterLabel::extended_master_secret
	String_t* ___extended_master_secret_0;

public:
	inline static int32_t get_offset_of_extended_master_secret_0() { return static_cast<int32_t>(offsetof(ExporterLabel_t64DE4AE70A2960BAF4CF5081306E3B2AB94F13A9_StaticFields, ___extended_master_secret_0)); }
	inline String_t* get_extended_master_secret_0() const { return ___extended_master_secret_0; }
	inline String_t** get_address_of_extended_master_secret_0() { return &___extended_master_secret_0; }
	inline void set_extended_master_secret_0(String_t* value)
	{
		___extended_master_secret_0 = value;
		Il2CppCodeGenWriteBarrier((&___extended_master_secret_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPORTERLABEL_T64DE4AE70A2960BAF4CF5081306E3B2AB94F13A9_H
#ifndef HASHALGORITHM_TEE6B968B3B1FD999C0F7BCDF55FD8ADD48DC52B6_H
#define HASHALGORITHM_TEE6B968B3B1FD999C0F7BCDF55FD8ADD48DC52B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.HashAlgorithm
struct  HashAlgorithm_tEE6B968B3B1FD999C0F7BCDF55FD8ADD48DC52B6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHM_TEE6B968B3B1FD999C0F7BCDF55FD8ADD48DC52B6_H
#ifndef MAXFRAGMENTLENGTH_T0C5987E0DE30D0E3C97DA86A6A5BDF4DB5AEA55D_H
#define MAXFRAGMENTLENGTH_T0C5987E0DE30D0E3C97DA86A6A5BDF4DB5AEA55D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.MaxFragmentLength
struct  MaxFragmentLength_t0C5987E0DE30D0E3C97DA86A6A5BDF4DB5AEA55D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAXFRAGMENTLENGTH_T0C5987E0DE30D0E3C97DA86A6A5BDF4DB5AEA55D_H
#ifndef NAMEDCURVE_TC950AF5C8967482E9CB78149686A9B394CB82AE6_H
#define NAMEDCURVE_TC950AF5C8967482E9CB78149686A9B394CB82AE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.NamedCurve
struct  NamedCurve_tC950AF5C8967482E9CB78149686A9B394CB82AE6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEDCURVE_TC950AF5C8967482E9CB78149686A9B394CB82AE6_H
#ifndef NEWSESSIONTICKET_T02775DC80796C90B1E39930FDE0CBADA5B34160D_H
#define NEWSESSIONTICKET_T02775DC80796C90B1E39930FDE0CBADA5B34160D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.NewSessionTicket
struct  NewSessionTicket_t02775DC80796C90B1E39930FDE0CBADA5B34160D  : public RuntimeObject
{
public:
	// System.Int64 Org.BouncyCastle.Crypto.Tls.NewSessionTicket::mTicketLifetimeHint
	int64_t ___mTicketLifetimeHint_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.NewSessionTicket::mTicket
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mTicket_1;

public:
	inline static int32_t get_offset_of_mTicketLifetimeHint_0() { return static_cast<int32_t>(offsetof(NewSessionTicket_t02775DC80796C90B1E39930FDE0CBADA5B34160D, ___mTicketLifetimeHint_0)); }
	inline int64_t get_mTicketLifetimeHint_0() const { return ___mTicketLifetimeHint_0; }
	inline int64_t* get_address_of_mTicketLifetimeHint_0() { return &___mTicketLifetimeHint_0; }
	inline void set_mTicketLifetimeHint_0(int64_t value)
	{
		___mTicketLifetimeHint_0 = value;
	}

	inline static int32_t get_offset_of_mTicket_1() { return static_cast<int32_t>(offsetof(NewSessionTicket_t02775DC80796C90B1E39930FDE0CBADA5B34160D, ___mTicket_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mTicket_1() const { return ___mTicket_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mTicket_1() { return &___mTicket_1; }
	inline void set_mTicket_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mTicket_1 = value;
		Il2CppCodeGenWriteBarrier((&___mTicket_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWSESSIONTICKET_T02775DC80796C90B1E39930FDE0CBADA5B34160D_H
#ifndef PROTOCOLVERSION_T8D17D19D186E01F44206B2B93FCF7454B146D09D_H
#define PROTOCOLVERSION_T8D17D19D186E01F44206B2B93FCF7454B146D09D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.ProtocolVersion
struct  ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Tls.ProtocolVersion::version
	int32_t ___version_6;
	// System.String Org.BouncyCastle.Crypto.Tls.ProtocolVersion::name
	String_t* ___name_7;

public:
	inline static int32_t get_offset_of_version_6() { return static_cast<int32_t>(offsetof(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D, ___version_6)); }
	inline int32_t get_version_6() const { return ___version_6; }
	inline int32_t* get_address_of_version_6() { return &___version_6; }
	inline void set_version_6(int32_t value)
	{
		___version_6 = value;
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}
};

struct ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields
{
public:
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.ProtocolVersion::SSLv3
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * ___SSLv3_0;
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.ProtocolVersion::TLSv10
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * ___TLSv10_1;
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.ProtocolVersion::TLSv11
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * ___TLSv11_2;
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.ProtocolVersion::TLSv12
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * ___TLSv12_3;
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.ProtocolVersion::DTLSv10
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * ___DTLSv10_4;
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.ProtocolVersion::DTLSv12
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * ___DTLSv12_5;

public:
	inline static int32_t get_offset_of_SSLv3_0() { return static_cast<int32_t>(offsetof(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields, ___SSLv3_0)); }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * get_SSLv3_0() const { return ___SSLv3_0; }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D ** get_address_of_SSLv3_0() { return &___SSLv3_0; }
	inline void set_SSLv3_0(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * value)
	{
		___SSLv3_0 = value;
		Il2CppCodeGenWriteBarrier((&___SSLv3_0), value);
	}

	inline static int32_t get_offset_of_TLSv10_1() { return static_cast<int32_t>(offsetof(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields, ___TLSv10_1)); }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * get_TLSv10_1() const { return ___TLSv10_1; }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D ** get_address_of_TLSv10_1() { return &___TLSv10_1; }
	inline void set_TLSv10_1(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * value)
	{
		___TLSv10_1 = value;
		Il2CppCodeGenWriteBarrier((&___TLSv10_1), value);
	}

	inline static int32_t get_offset_of_TLSv11_2() { return static_cast<int32_t>(offsetof(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields, ___TLSv11_2)); }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * get_TLSv11_2() const { return ___TLSv11_2; }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D ** get_address_of_TLSv11_2() { return &___TLSv11_2; }
	inline void set_TLSv11_2(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * value)
	{
		___TLSv11_2 = value;
		Il2CppCodeGenWriteBarrier((&___TLSv11_2), value);
	}

	inline static int32_t get_offset_of_TLSv12_3() { return static_cast<int32_t>(offsetof(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields, ___TLSv12_3)); }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * get_TLSv12_3() const { return ___TLSv12_3; }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D ** get_address_of_TLSv12_3() { return &___TLSv12_3; }
	inline void set_TLSv12_3(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * value)
	{
		___TLSv12_3 = value;
		Il2CppCodeGenWriteBarrier((&___TLSv12_3), value);
	}

	inline static int32_t get_offset_of_DTLSv10_4() { return static_cast<int32_t>(offsetof(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields, ___DTLSv10_4)); }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * get_DTLSv10_4() const { return ___DTLSv10_4; }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D ** get_address_of_DTLSv10_4() { return &___DTLSv10_4; }
	inline void set_DTLSv10_4(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * value)
	{
		___DTLSv10_4 = value;
		Il2CppCodeGenWriteBarrier((&___DTLSv10_4), value);
	}

	inline static int32_t get_offset_of_DTLSv12_5() { return static_cast<int32_t>(offsetof(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields, ___DTLSv12_5)); }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * get_DTLSv12_5() const { return ___DTLSv12_5; }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D ** get_address_of_DTLSv12_5() { return &___DTLSv12_5; }
	inline void set_DTLSv12_5(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * value)
	{
		___DTLSv12_5 = value;
		Il2CppCodeGenWriteBarrier((&___DTLSv12_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTOCOLVERSION_T8D17D19D186E01F44206B2B93FCF7454B146D09D_H
#ifndef RECORDSTREAM_T7308B15B7B671878E7160CCB6F28DDFAB4277A19_H
#define RECORDSTREAM_T7308B15B7B671878E7160CCB6F28DDFAB4277A19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.RecordStream
struct  RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsProtocol Org.BouncyCastle.Crypto.Tls.RecordStream::mHandler
	TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931 * ___mHandler_0;
	// System.IO.Stream Org.BouncyCastle.Crypto.Tls.RecordStream::mInput
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___mInput_1;
	// System.IO.Stream Org.BouncyCastle.Crypto.Tls.RecordStream::mOutput
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___mOutput_2;
	// Org.BouncyCastle.Crypto.Tls.TlsCompression Org.BouncyCastle.Crypto.Tls.RecordStream::mPendingCompression
	RuntimeObject* ___mPendingCompression_3;
	// Org.BouncyCastle.Crypto.Tls.TlsCompression Org.BouncyCastle.Crypto.Tls.RecordStream::mReadCompression
	RuntimeObject* ___mReadCompression_4;
	// Org.BouncyCastle.Crypto.Tls.TlsCompression Org.BouncyCastle.Crypto.Tls.RecordStream::mWriteCompression
	RuntimeObject* ___mWriteCompression_5;
	// Org.BouncyCastle.Crypto.Tls.TlsCipher Org.BouncyCastle.Crypto.Tls.RecordStream::mPendingCipher
	RuntimeObject* ___mPendingCipher_6;
	// Org.BouncyCastle.Crypto.Tls.TlsCipher Org.BouncyCastle.Crypto.Tls.RecordStream::mReadCipher
	RuntimeObject* ___mReadCipher_7;
	// Org.BouncyCastle.Crypto.Tls.TlsCipher Org.BouncyCastle.Crypto.Tls.RecordStream::mWriteCipher
	RuntimeObject* ___mWriteCipher_8;
	// System.Int64 Org.BouncyCastle.Crypto.Tls.RecordStream::mReadSeqNo
	int64_t ___mReadSeqNo_9;
	// System.Int64 Org.BouncyCastle.Crypto.Tls.RecordStream::mWriteSeqNo
	int64_t ___mWriteSeqNo_10;
	// System.IO.MemoryStream Org.BouncyCastle.Crypto.Tls.RecordStream::mBuffer
	MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * ___mBuffer_11;
	// Org.BouncyCastle.Crypto.Tls.TlsHandshakeHash Org.BouncyCastle.Crypto.Tls.RecordStream::mHandshakeHash
	RuntimeObject* ___mHandshakeHash_12;
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.RecordStream::mReadVersion
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * ___mReadVersion_13;
	// Org.BouncyCastle.Crypto.Tls.ProtocolVersion Org.BouncyCastle.Crypto.Tls.RecordStream::mWriteVersion
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * ___mWriteVersion_14;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.RecordStream::mRestrictReadVersion
	bool ___mRestrictReadVersion_15;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.RecordStream::mPlaintextLimit
	int32_t ___mPlaintextLimit_16;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.RecordStream::mCompressedLimit
	int32_t ___mCompressedLimit_17;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.RecordStream::mCiphertextLimit
	int32_t ___mCiphertextLimit_18;

public:
	inline static int32_t get_offset_of_mHandler_0() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mHandler_0)); }
	inline TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931 * get_mHandler_0() const { return ___mHandler_0; }
	inline TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931 ** get_address_of_mHandler_0() { return &___mHandler_0; }
	inline void set_mHandler_0(TlsProtocol_t1304FF9D35D801AFF1B66BA125FD0087ACF55931 * value)
	{
		___mHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___mHandler_0), value);
	}

	inline static int32_t get_offset_of_mInput_1() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mInput_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_mInput_1() const { return ___mInput_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_mInput_1() { return &___mInput_1; }
	inline void set_mInput_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___mInput_1 = value;
		Il2CppCodeGenWriteBarrier((&___mInput_1), value);
	}

	inline static int32_t get_offset_of_mOutput_2() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mOutput_2)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_mOutput_2() const { return ___mOutput_2; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_mOutput_2() { return &___mOutput_2; }
	inline void set_mOutput_2(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___mOutput_2 = value;
		Il2CppCodeGenWriteBarrier((&___mOutput_2), value);
	}

	inline static int32_t get_offset_of_mPendingCompression_3() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mPendingCompression_3)); }
	inline RuntimeObject* get_mPendingCompression_3() const { return ___mPendingCompression_3; }
	inline RuntimeObject** get_address_of_mPendingCompression_3() { return &___mPendingCompression_3; }
	inline void set_mPendingCompression_3(RuntimeObject* value)
	{
		___mPendingCompression_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPendingCompression_3), value);
	}

	inline static int32_t get_offset_of_mReadCompression_4() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mReadCompression_4)); }
	inline RuntimeObject* get_mReadCompression_4() const { return ___mReadCompression_4; }
	inline RuntimeObject** get_address_of_mReadCompression_4() { return &___mReadCompression_4; }
	inline void set_mReadCompression_4(RuntimeObject* value)
	{
		___mReadCompression_4 = value;
		Il2CppCodeGenWriteBarrier((&___mReadCompression_4), value);
	}

	inline static int32_t get_offset_of_mWriteCompression_5() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mWriteCompression_5)); }
	inline RuntimeObject* get_mWriteCompression_5() const { return ___mWriteCompression_5; }
	inline RuntimeObject** get_address_of_mWriteCompression_5() { return &___mWriteCompression_5; }
	inline void set_mWriteCompression_5(RuntimeObject* value)
	{
		___mWriteCompression_5 = value;
		Il2CppCodeGenWriteBarrier((&___mWriteCompression_5), value);
	}

	inline static int32_t get_offset_of_mPendingCipher_6() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mPendingCipher_6)); }
	inline RuntimeObject* get_mPendingCipher_6() const { return ___mPendingCipher_6; }
	inline RuntimeObject** get_address_of_mPendingCipher_6() { return &___mPendingCipher_6; }
	inline void set_mPendingCipher_6(RuntimeObject* value)
	{
		___mPendingCipher_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPendingCipher_6), value);
	}

	inline static int32_t get_offset_of_mReadCipher_7() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mReadCipher_7)); }
	inline RuntimeObject* get_mReadCipher_7() const { return ___mReadCipher_7; }
	inline RuntimeObject** get_address_of_mReadCipher_7() { return &___mReadCipher_7; }
	inline void set_mReadCipher_7(RuntimeObject* value)
	{
		___mReadCipher_7 = value;
		Il2CppCodeGenWriteBarrier((&___mReadCipher_7), value);
	}

	inline static int32_t get_offset_of_mWriteCipher_8() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mWriteCipher_8)); }
	inline RuntimeObject* get_mWriteCipher_8() const { return ___mWriteCipher_8; }
	inline RuntimeObject** get_address_of_mWriteCipher_8() { return &___mWriteCipher_8; }
	inline void set_mWriteCipher_8(RuntimeObject* value)
	{
		___mWriteCipher_8 = value;
		Il2CppCodeGenWriteBarrier((&___mWriteCipher_8), value);
	}

	inline static int32_t get_offset_of_mReadSeqNo_9() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mReadSeqNo_9)); }
	inline int64_t get_mReadSeqNo_9() const { return ___mReadSeqNo_9; }
	inline int64_t* get_address_of_mReadSeqNo_9() { return &___mReadSeqNo_9; }
	inline void set_mReadSeqNo_9(int64_t value)
	{
		___mReadSeqNo_9 = value;
	}

	inline static int32_t get_offset_of_mWriteSeqNo_10() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mWriteSeqNo_10)); }
	inline int64_t get_mWriteSeqNo_10() const { return ___mWriteSeqNo_10; }
	inline int64_t* get_address_of_mWriteSeqNo_10() { return &___mWriteSeqNo_10; }
	inline void set_mWriteSeqNo_10(int64_t value)
	{
		___mWriteSeqNo_10 = value;
	}

	inline static int32_t get_offset_of_mBuffer_11() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mBuffer_11)); }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * get_mBuffer_11() const { return ___mBuffer_11; }
	inline MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C ** get_address_of_mBuffer_11() { return &___mBuffer_11; }
	inline void set_mBuffer_11(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C * value)
	{
		___mBuffer_11 = value;
		Il2CppCodeGenWriteBarrier((&___mBuffer_11), value);
	}

	inline static int32_t get_offset_of_mHandshakeHash_12() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mHandshakeHash_12)); }
	inline RuntimeObject* get_mHandshakeHash_12() const { return ___mHandshakeHash_12; }
	inline RuntimeObject** get_address_of_mHandshakeHash_12() { return &___mHandshakeHash_12; }
	inline void set_mHandshakeHash_12(RuntimeObject* value)
	{
		___mHandshakeHash_12 = value;
		Il2CppCodeGenWriteBarrier((&___mHandshakeHash_12), value);
	}

	inline static int32_t get_offset_of_mReadVersion_13() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mReadVersion_13)); }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * get_mReadVersion_13() const { return ___mReadVersion_13; }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D ** get_address_of_mReadVersion_13() { return &___mReadVersion_13; }
	inline void set_mReadVersion_13(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * value)
	{
		___mReadVersion_13 = value;
		Il2CppCodeGenWriteBarrier((&___mReadVersion_13), value);
	}

	inline static int32_t get_offset_of_mWriteVersion_14() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mWriteVersion_14)); }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * get_mWriteVersion_14() const { return ___mWriteVersion_14; }
	inline ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D ** get_address_of_mWriteVersion_14() { return &___mWriteVersion_14; }
	inline void set_mWriteVersion_14(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D * value)
	{
		___mWriteVersion_14 = value;
		Il2CppCodeGenWriteBarrier((&___mWriteVersion_14), value);
	}

	inline static int32_t get_offset_of_mRestrictReadVersion_15() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mRestrictReadVersion_15)); }
	inline bool get_mRestrictReadVersion_15() const { return ___mRestrictReadVersion_15; }
	inline bool* get_address_of_mRestrictReadVersion_15() { return &___mRestrictReadVersion_15; }
	inline void set_mRestrictReadVersion_15(bool value)
	{
		___mRestrictReadVersion_15 = value;
	}

	inline static int32_t get_offset_of_mPlaintextLimit_16() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mPlaintextLimit_16)); }
	inline int32_t get_mPlaintextLimit_16() const { return ___mPlaintextLimit_16; }
	inline int32_t* get_address_of_mPlaintextLimit_16() { return &___mPlaintextLimit_16; }
	inline void set_mPlaintextLimit_16(int32_t value)
	{
		___mPlaintextLimit_16 = value;
	}

	inline static int32_t get_offset_of_mCompressedLimit_17() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mCompressedLimit_17)); }
	inline int32_t get_mCompressedLimit_17() const { return ___mCompressedLimit_17; }
	inline int32_t* get_address_of_mCompressedLimit_17() { return &___mCompressedLimit_17; }
	inline void set_mCompressedLimit_17(int32_t value)
	{
		___mCompressedLimit_17 = value;
	}

	inline static int32_t get_offset_of_mCiphertextLimit_18() { return static_cast<int32_t>(offsetof(RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19, ___mCiphertextLimit_18)); }
	inline int32_t get_mCiphertextLimit_18() const { return ___mCiphertextLimit_18; }
	inline int32_t* get_address_of_mCiphertextLimit_18() { return &___mCiphertextLimit_18; }
	inline void set_mCiphertextLimit_18(int32_t value)
	{
		___mCiphertextLimit_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECORDSTREAM_T7308B15B7B671878E7160CCB6F28DDFAB4277A19_H
#ifndef SECURITYPARAMETERS_TF44CC3B939DF8F2A7BA70B4CD9166F51399031DB_H
#define SECURITYPARAMETERS_TF44CC3B939DF8F2A7BA70B4CD9166F51399031DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.SecurityParameters
struct  SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Tls.SecurityParameters::entity
	int32_t ___entity_0;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.SecurityParameters::cipherSuite
	int32_t ___cipherSuite_1;
	// System.Byte Org.BouncyCastle.Crypto.Tls.SecurityParameters::compressionAlgorithm
	uint8_t ___compressionAlgorithm_2;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.SecurityParameters::prfAlgorithm
	int32_t ___prfAlgorithm_3;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.SecurityParameters::verifyDataLength
	int32_t ___verifyDataLength_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SecurityParameters::masterSecret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___masterSecret_5;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SecurityParameters::clientRandom
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___clientRandom_6;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SecurityParameters::serverRandom
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___serverRandom_7;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SecurityParameters::sessionHash
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___sessionHash_8;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SecurityParameters::pskIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___pskIdentity_9;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SecurityParameters::srpIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___srpIdentity_10;
	// System.Int16 Org.BouncyCastle.Crypto.Tls.SecurityParameters::maxFragmentLength
	int16_t ___maxFragmentLength_11;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.SecurityParameters::truncatedHMac
	bool ___truncatedHMac_12;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.SecurityParameters::encryptThenMac
	bool ___encryptThenMac_13;
	// System.Boolean Org.BouncyCastle.Crypto.Tls.SecurityParameters::extendedMasterSecret
	bool ___extendedMasterSecret_14;

public:
	inline static int32_t get_offset_of_entity_0() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___entity_0)); }
	inline int32_t get_entity_0() const { return ___entity_0; }
	inline int32_t* get_address_of_entity_0() { return &___entity_0; }
	inline void set_entity_0(int32_t value)
	{
		___entity_0 = value;
	}

	inline static int32_t get_offset_of_cipherSuite_1() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___cipherSuite_1)); }
	inline int32_t get_cipherSuite_1() const { return ___cipherSuite_1; }
	inline int32_t* get_address_of_cipherSuite_1() { return &___cipherSuite_1; }
	inline void set_cipherSuite_1(int32_t value)
	{
		___cipherSuite_1 = value;
	}

	inline static int32_t get_offset_of_compressionAlgorithm_2() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___compressionAlgorithm_2)); }
	inline uint8_t get_compressionAlgorithm_2() const { return ___compressionAlgorithm_2; }
	inline uint8_t* get_address_of_compressionAlgorithm_2() { return &___compressionAlgorithm_2; }
	inline void set_compressionAlgorithm_2(uint8_t value)
	{
		___compressionAlgorithm_2 = value;
	}

	inline static int32_t get_offset_of_prfAlgorithm_3() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___prfAlgorithm_3)); }
	inline int32_t get_prfAlgorithm_3() const { return ___prfAlgorithm_3; }
	inline int32_t* get_address_of_prfAlgorithm_3() { return &___prfAlgorithm_3; }
	inline void set_prfAlgorithm_3(int32_t value)
	{
		___prfAlgorithm_3 = value;
	}

	inline static int32_t get_offset_of_verifyDataLength_4() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___verifyDataLength_4)); }
	inline int32_t get_verifyDataLength_4() const { return ___verifyDataLength_4; }
	inline int32_t* get_address_of_verifyDataLength_4() { return &___verifyDataLength_4; }
	inline void set_verifyDataLength_4(int32_t value)
	{
		___verifyDataLength_4 = value;
	}

	inline static int32_t get_offset_of_masterSecret_5() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___masterSecret_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_masterSecret_5() const { return ___masterSecret_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_masterSecret_5() { return &___masterSecret_5; }
	inline void set_masterSecret_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___masterSecret_5 = value;
		Il2CppCodeGenWriteBarrier((&___masterSecret_5), value);
	}

	inline static int32_t get_offset_of_clientRandom_6() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___clientRandom_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_clientRandom_6() const { return ___clientRandom_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_clientRandom_6() { return &___clientRandom_6; }
	inline void set_clientRandom_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___clientRandom_6 = value;
		Il2CppCodeGenWriteBarrier((&___clientRandom_6), value);
	}

	inline static int32_t get_offset_of_serverRandom_7() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___serverRandom_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_serverRandom_7() const { return ___serverRandom_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_serverRandom_7() { return &___serverRandom_7; }
	inline void set_serverRandom_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___serverRandom_7 = value;
		Il2CppCodeGenWriteBarrier((&___serverRandom_7), value);
	}

	inline static int32_t get_offset_of_sessionHash_8() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___sessionHash_8)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_sessionHash_8() const { return ___sessionHash_8; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_sessionHash_8() { return &___sessionHash_8; }
	inline void set_sessionHash_8(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___sessionHash_8 = value;
		Il2CppCodeGenWriteBarrier((&___sessionHash_8), value);
	}

	inline static int32_t get_offset_of_pskIdentity_9() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___pskIdentity_9)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_pskIdentity_9() const { return ___pskIdentity_9; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_pskIdentity_9() { return &___pskIdentity_9; }
	inline void set_pskIdentity_9(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___pskIdentity_9 = value;
		Il2CppCodeGenWriteBarrier((&___pskIdentity_9), value);
	}

	inline static int32_t get_offset_of_srpIdentity_10() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___srpIdentity_10)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_srpIdentity_10() const { return ___srpIdentity_10; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_srpIdentity_10() { return &___srpIdentity_10; }
	inline void set_srpIdentity_10(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___srpIdentity_10 = value;
		Il2CppCodeGenWriteBarrier((&___srpIdentity_10), value);
	}

	inline static int32_t get_offset_of_maxFragmentLength_11() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___maxFragmentLength_11)); }
	inline int16_t get_maxFragmentLength_11() const { return ___maxFragmentLength_11; }
	inline int16_t* get_address_of_maxFragmentLength_11() { return &___maxFragmentLength_11; }
	inline void set_maxFragmentLength_11(int16_t value)
	{
		___maxFragmentLength_11 = value;
	}

	inline static int32_t get_offset_of_truncatedHMac_12() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___truncatedHMac_12)); }
	inline bool get_truncatedHMac_12() const { return ___truncatedHMac_12; }
	inline bool* get_address_of_truncatedHMac_12() { return &___truncatedHMac_12; }
	inline void set_truncatedHMac_12(bool value)
	{
		___truncatedHMac_12 = value;
	}

	inline static int32_t get_offset_of_encryptThenMac_13() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___encryptThenMac_13)); }
	inline bool get_encryptThenMac_13() const { return ___encryptThenMac_13; }
	inline bool* get_address_of_encryptThenMac_13() { return &___encryptThenMac_13; }
	inline void set_encryptThenMac_13(bool value)
	{
		___encryptThenMac_13 = value;
	}

	inline static int32_t get_offset_of_extendedMasterSecret_14() { return static_cast<int32_t>(offsetof(SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB, ___extendedMasterSecret_14)); }
	inline bool get_extendedMasterSecret_14() const { return ___extendedMasterSecret_14; }
	inline bool* get_address_of_extendedMasterSecret_14() { return &___extendedMasterSecret_14; }
	inline void set_extendedMasterSecret_14(bool value)
	{
		___extendedMasterSecret_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECURITYPARAMETERS_TF44CC3B939DF8F2A7BA70B4CD9166F51399031DB_H
#ifndef SERVERDHPARAMS_TF568540CC3D6BAD9A1501D67B79FC55CF814CEAF_H
#define SERVERDHPARAMS_TF568540CC3D6BAD9A1501D67B79FC55CF814CEAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.ServerDHParams
struct  ServerDHParams_tF568540CC3D6BAD9A1501D67B79FC55CF814CEAF  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Parameters.DHPublicKeyParameters Org.BouncyCastle.Crypto.Tls.ServerDHParams::mPublicKey
	DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA * ___mPublicKey_0;

public:
	inline static int32_t get_offset_of_mPublicKey_0() { return static_cast<int32_t>(offsetof(ServerDHParams_tF568540CC3D6BAD9A1501D67B79FC55CF814CEAF, ___mPublicKey_0)); }
	inline DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA * get_mPublicKey_0() const { return ___mPublicKey_0; }
	inline DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA ** get_address_of_mPublicKey_0() { return &___mPublicKey_0; }
	inline void set_mPublicKey_0(DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA * value)
	{
		___mPublicKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___mPublicKey_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERDHPARAMS_TF568540CC3D6BAD9A1501D67B79FC55CF814CEAF_H
#ifndef SERVERNAME_T98E795D8EE736F9B4D70AA3A584C31CF972FFDFB_H
#define SERVERNAME_T98E795D8EE736F9B4D70AA3A584C31CF972FFDFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.ServerName
struct  ServerName_t98E795D8EE736F9B4D70AA3A584C31CF972FFDFB  : public RuntimeObject
{
public:
	// System.Byte Org.BouncyCastle.Crypto.Tls.ServerName::mNameType
	uint8_t ___mNameType_0;
	// System.Object Org.BouncyCastle.Crypto.Tls.ServerName::mName
	RuntimeObject * ___mName_1;

public:
	inline static int32_t get_offset_of_mNameType_0() { return static_cast<int32_t>(offsetof(ServerName_t98E795D8EE736F9B4D70AA3A584C31CF972FFDFB, ___mNameType_0)); }
	inline uint8_t get_mNameType_0() const { return ___mNameType_0; }
	inline uint8_t* get_address_of_mNameType_0() { return &___mNameType_0; }
	inline void set_mNameType_0(uint8_t value)
	{
		___mNameType_0 = value;
	}

	inline static int32_t get_offset_of_mName_1() { return static_cast<int32_t>(offsetof(ServerName_t98E795D8EE736F9B4D70AA3A584C31CF972FFDFB, ___mName_1)); }
	inline RuntimeObject * get_mName_1() const { return ___mName_1; }
	inline RuntimeObject ** get_address_of_mName_1() { return &___mName_1; }
	inline void set_mName_1(RuntimeObject * value)
	{
		___mName_1 = value;
		Il2CppCodeGenWriteBarrier((&___mName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERNAME_T98E795D8EE736F9B4D70AA3A584C31CF972FFDFB_H
#ifndef SERVERNAMELIST_T9939760650D7D18A9DAEA1A36EBB286EB8686E9F_H
#define SERVERNAMELIST_T9939760650D7D18A9DAEA1A36EBB286EB8686E9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.ServerNameList
struct  ServerNameList_t9939760650D7D18A9DAEA1A36EBB286EB8686E9F  : public RuntimeObject
{
public:
	// System.Collections.IList Org.BouncyCastle.Crypto.Tls.ServerNameList::mServerNameList
	RuntimeObject* ___mServerNameList_0;

public:
	inline static int32_t get_offset_of_mServerNameList_0() { return static_cast<int32_t>(offsetof(ServerNameList_t9939760650D7D18A9DAEA1A36EBB286EB8686E9F, ___mServerNameList_0)); }
	inline RuntimeObject* get_mServerNameList_0() const { return ___mServerNameList_0; }
	inline RuntimeObject** get_address_of_mServerNameList_0() { return &___mServerNameList_0; }
	inline void set_mServerNameList_0(RuntimeObject* value)
	{
		___mServerNameList_0 = value;
		Il2CppCodeGenWriteBarrier((&___mServerNameList_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERNAMELIST_T9939760650D7D18A9DAEA1A36EBB286EB8686E9F_H
#ifndef SESSIONPARAMETERS_T024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115_H
#define SESSIONPARAMETERS_T024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.SessionParameters
struct  SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Tls.SessionParameters::mCipherSuite
	int32_t ___mCipherSuite_0;
	// System.Byte Org.BouncyCastle.Crypto.Tls.SessionParameters::mCompressionAlgorithm
	uint8_t ___mCompressionAlgorithm_1;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SessionParameters::mMasterSecret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mMasterSecret_2;
	// Org.BouncyCastle.Crypto.Tls.Certificate Org.BouncyCastle.Crypto.Tls.SessionParameters::mPeerCertificate
	Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D * ___mPeerCertificate_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SessionParameters::mPskIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPskIdentity_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SessionParameters::mSrpIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSrpIdentity_5;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SessionParameters::mEncodedServerExtensions
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mEncodedServerExtensions_6;

public:
	inline static int32_t get_offset_of_mCipherSuite_0() { return static_cast<int32_t>(offsetof(SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115, ___mCipherSuite_0)); }
	inline int32_t get_mCipherSuite_0() const { return ___mCipherSuite_0; }
	inline int32_t* get_address_of_mCipherSuite_0() { return &___mCipherSuite_0; }
	inline void set_mCipherSuite_0(int32_t value)
	{
		___mCipherSuite_0 = value;
	}

	inline static int32_t get_offset_of_mCompressionAlgorithm_1() { return static_cast<int32_t>(offsetof(SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115, ___mCompressionAlgorithm_1)); }
	inline uint8_t get_mCompressionAlgorithm_1() const { return ___mCompressionAlgorithm_1; }
	inline uint8_t* get_address_of_mCompressionAlgorithm_1() { return &___mCompressionAlgorithm_1; }
	inline void set_mCompressionAlgorithm_1(uint8_t value)
	{
		___mCompressionAlgorithm_1 = value;
	}

	inline static int32_t get_offset_of_mMasterSecret_2() { return static_cast<int32_t>(offsetof(SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115, ___mMasterSecret_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mMasterSecret_2() const { return ___mMasterSecret_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mMasterSecret_2() { return &___mMasterSecret_2; }
	inline void set_mMasterSecret_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mMasterSecret_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMasterSecret_2), value);
	}

	inline static int32_t get_offset_of_mPeerCertificate_3() { return static_cast<int32_t>(offsetof(SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115, ___mPeerCertificate_3)); }
	inline Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D * get_mPeerCertificate_3() const { return ___mPeerCertificate_3; }
	inline Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D ** get_address_of_mPeerCertificate_3() { return &___mPeerCertificate_3; }
	inline void set_mPeerCertificate_3(Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D * value)
	{
		___mPeerCertificate_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPeerCertificate_3), value);
	}

	inline static int32_t get_offset_of_mPskIdentity_4() { return static_cast<int32_t>(offsetof(SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115, ___mPskIdentity_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPskIdentity_4() const { return ___mPskIdentity_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPskIdentity_4() { return &___mPskIdentity_4; }
	inline void set_mPskIdentity_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPskIdentity_4 = value;
		Il2CppCodeGenWriteBarrier((&___mPskIdentity_4), value);
	}

	inline static int32_t get_offset_of_mSrpIdentity_5() { return static_cast<int32_t>(offsetof(SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115, ___mSrpIdentity_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSrpIdentity_5() const { return ___mSrpIdentity_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSrpIdentity_5() { return &___mSrpIdentity_5; }
	inline void set_mSrpIdentity_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSrpIdentity_5 = value;
		Il2CppCodeGenWriteBarrier((&___mSrpIdentity_5), value);
	}

	inline static int32_t get_offset_of_mEncodedServerExtensions_6() { return static_cast<int32_t>(offsetof(SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115, ___mEncodedServerExtensions_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mEncodedServerExtensions_6() const { return ___mEncodedServerExtensions_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mEncodedServerExtensions_6() { return &___mEncodedServerExtensions_6; }
	inline void set_mEncodedServerExtensions_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mEncodedServerExtensions_6 = value;
		Il2CppCodeGenWriteBarrier((&___mEncodedServerExtensions_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONPARAMETERS_T024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115_H
#ifndef BUILDER_T373D3EDDC040BC8647638E872A52BA6B4ADA8FB2_H
#define BUILDER_T373D3EDDC040BC8647638E872A52BA6B4ADA8FB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder
struct  Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mCipherSuite
	int32_t ___mCipherSuite_0;
	// System.Int16 Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mCompressionAlgorithm
	int16_t ___mCompressionAlgorithm_1;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mMasterSecret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mMasterSecret_2;
	// Org.BouncyCastle.Crypto.Tls.Certificate Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mPeerCertificate
	Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D * ___mPeerCertificate_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mPskIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mPskIdentity_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mSrpIdentity
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mSrpIdentity_5;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SessionParameters_Builder::mEncodedServerExtensions
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mEncodedServerExtensions_6;

public:
	inline static int32_t get_offset_of_mCipherSuite_0() { return static_cast<int32_t>(offsetof(Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2, ___mCipherSuite_0)); }
	inline int32_t get_mCipherSuite_0() const { return ___mCipherSuite_0; }
	inline int32_t* get_address_of_mCipherSuite_0() { return &___mCipherSuite_0; }
	inline void set_mCipherSuite_0(int32_t value)
	{
		___mCipherSuite_0 = value;
	}

	inline static int32_t get_offset_of_mCompressionAlgorithm_1() { return static_cast<int32_t>(offsetof(Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2, ___mCompressionAlgorithm_1)); }
	inline int16_t get_mCompressionAlgorithm_1() const { return ___mCompressionAlgorithm_1; }
	inline int16_t* get_address_of_mCompressionAlgorithm_1() { return &___mCompressionAlgorithm_1; }
	inline void set_mCompressionAlgorithm_1(int16_t value)
	{
		___mCompressionAlgorithm_1 = value;
	}

	inline static int32_t get_offset_of_mMasterSecret_2() { return static_cast<int32_t>(offsetof(Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2, ___mMasterSecret_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mMasterSecret_2() const { return ___mMasterSecret_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mMasterSecret_2() { return &___mMasterSecret_2; }
	inline void set_mMasterSecret_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mMasterSecret_2 = value;
		Il2CppCodeGenWriteBarrier((&___mMasterSecret_2), value);
	}

	inline static int32_t get_offset_of_mPeerCertificate_3() { return static_cast<int32_t>(offsetof(Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2, ___mPeerCertificate_3)); }
	inline Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D * get_mPeerCertificate_3() const { return ___mPeerCertificate_3; }
	inline Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D ** get_address_of_mPeerCertificate_3() { return &___mPeerCertificate_3; }
	inline void set_mPeerCertificate_3(Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D * value)
	{
		___mPeerCertificate_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPeerCertificate_3), value);
	}

	inline static int32_t get_offset_of_mPskIdentity_4() { return static_cast<int32_t>(offsetof(Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2, ___mPskIdentity_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mPskIdentity_4() const { return ___mPskIdentity_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mPskIdentity_4() { return &___mPskIdentity_4; }
	inline void set_mPskIdentity_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mPskIdentity_4 = value;
		Il2CppCodeGenWriteBarrier((&___mPskIdentity_4), value);
	}

	inline static int32_t get_offset_of_mSrpIdentity_5() { return static_cast<int32_t>(offsetof(Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2, ___mSrpIdentity_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mSrpIdentity_5() const { return ___mSrpIdentity_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mSrpIdentity_5() { return &___mSrpIdentity_5; }
	inline void set_mSrpIdentity_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mSrpIdentity_5 = value;
		Il2CppCodeGenWriteBarrier((&___mSrpIdentity_5), value);
	}

	inline static int32_t get_offset_of_mEncodedServerExtensions_6() { return static_cast<int32_t>(offsetof(Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2, ___mEncodedServerExtensions_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mEncodedServerExtensions_6() const { return ___mEncodedServerExtensions_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mEncodedServerExtensions_6() { return &___mEncodedServerExtensions_6; }
	inline void set_mEncodedServerExtensions_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mEncodedServerExtensions_6 = value;
		Il2CppCodeGenWriteBarrier((&___mEncodedServerExtensions_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDER_T373D3EDDC040BC8647638E872A52BA6B4ADA8FB2_H
#ifndef SIGNATUREANDHASHALGORITHM_TDEA4E53D4DC207FA196758B3B4848104689854A9_H
#define SIGNATUREANDHASHALGORITHM_TDEA4E53D4DC207FA196758B3B4848104689854A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.SignatureAndHashAlgorithm
struct  SignatureAndHashAlgorithm_tDEA4E53D4DC207FA196758B3B4848104689854A9  : public RuntimeObject
{
public:
	// System.Byte Org.BouncyCastle.Crypto.Tls.SignatureAndHashAlgorithm::mHash
	uint8_t ___mHash_0;
	// System.Byte Org.BouncyCastle.Crypto.Tls.SignatureAndHashAlgorithm::mSignature
	uint8_t ___mSignature_1;

public:
	inline static int32_t get_offset_of_mHash_0() { return static_cast<int32_t>(offsetof(SignatureAndHashAlgorithm_tDEA4E53D4DC207FA196758B3B4848104689854A9, ___mHash_0)); }
	inline uint8_t get_mHash_0() const { return ___mHash_0; }
	inline uint8_t* get_address_of_mHash_0() { return &___mHash_0; }
	inline void set_mHash_0(uint8_t value)
	{
		___mHash_0 = value;
	}

	inline static int32_t get_offset_of_mSignature_1() { return static_cast<int32_t>(offsetof(SignatureAndHashAlgorithm_tDEA4E53D4DC207FA196758B3B4848104689854A9, ___mSignature_1)); }
	inline uint8_t get_mSignature_1() const { return ___mSignature_1; }
	inline uint8_t* get_address_of_mSignature_1() { return &___mSignature_1; }
	inline void set_mSignature_1(uint8_t value)
	{
		___mSignature_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNATUREANDHASHALGORITHM_TDEA4E53D4DC207FA196758B3B4848104689854A9_H
#ifndef SSL3MAC_T0FA863B419D2DCE41B15503AFFF8C0C4E450DF08_H
#define SSL3MAC_T0FA863B419D2DCE41B15503AFFF8C0C4E450DF08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.Ssl3Mac
struct  Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.IDigest Org.BouncyCastle.Crypto.Tls.Ssl3Mac::digest
	RuntimeObject* ___digest_2;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.Ssl3Mac::padLength
	int32_t ___padLength_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.Ssl3Mac::secret
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___secret_4;

public:
	inline static int32_t get_offset_of_digest_2() { return static_cast<int32_t>(offsetof(Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08, ___digest_2)); }
	inline RuntimeObject* get_digest_2() const { return ___digest_2; }
	inline RuntimeObject** get_address_of_digest_2() { return &___digest_2; }
	inline void set_digest_2(RuntimeObject* value)
	{
		___digest_2 = value;
		Il2CppCodeGenWriteBarrier((&___digest_2), value);
	}

	inline static int32_t get_offset_of_padLength_3() { return static_cast<int32_t>(offsetof(Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08, ___padLength_3)); }
	inline int32_t get_padLength_3() const { return ___padLength_3; }
	inline int32_t* get_address_of_padLength_3() { return &___padLength_3; }
	inline void set_padLength_3(int32_t value)
	{
		___padLength_3 = value;
	}

	inline static int32_t get_offset_of_secret_4() { return static_cast<int32_t>(offsetof(Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08, ___secret_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_secret_4() const { return ___secret_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_secret_4() { return &___secret_4; }
	inline void set_secret_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___secret_4 = value;
		Il2CppCodeGenWriteBarrier((&___secret_4), value);
	}
};

struct Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08_StaticFields
{
public:
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.Ssl3Mac::IPAD
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___IPAD_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.Ssl3Mac::OPAD
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___OPAD_1;

public:
	inline static int32_t get_offset_of_IPAD_0() { return static_cast<int32_t>(offsetof(Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08_StaticFields, ___IPAD_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_IPAD_0() const { return ___IPAD_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_IPAD_0() { return &___IPAD_0; }
	inline void set_IPAD_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___IPAD_0 = value;
		Il2CppCodeGenWriteBarrier((&___IPAD_0), value);
	}

	inline static int32_t get_offset_of_OPAD_1() { return static_cast<int32_t>(offsetof(Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08_StaticFields, ___OPAD_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_OPAD_1() const { return ___OPAD_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_OPAD_1() { return &___OPAD_1; }
	inline void set_OPAD_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___OPAD_1 = value;
		Il2CppCodeGenWriteBarrier((&___OPAD_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSL3MAC_T0FA863B419D2DCE41B15503AFFF8C0C4E450DF08_H
#ifndef SUPPLEMENTALDATAENTRY_T7431C336D03C472BA9D64CEBA9BDBFACFCBDD69D_H
#define SUPPLEMENTALDATAENTRY_T7431C336D03C472BA9D64CEBA9BDBFACFCBDD69D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.SupplementalDataEntry
struct  SupplementalDataEntry_t7431C336D03C472BA9D64CEBA9BDBFACFCBDD69D  : public RuntimeObject
{
public:
	// System.Int32 Org.BouncyCastle.Crypto.Tls.SupplementalDataEntry::mDataType
	int32_t ___mDataType_0;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.SupplementalDataEntry::mData
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mData_1;

public:
	inline static int32_t get_offset_of_mDataType_0() { return static_cast<int32_t>(offsetof(SupplementalDataEntry_t7431C336D03C472BA9D64CEBA9BDBFACFCBDD69D, ___mDataType_0)); }
	inline int32_t get_mDataType_0() const { return ___mDataType_0; }
	inline int32_t* get_address_of_mDataType_0() { return &___mDataType_0; }
	inline void set_mDataType_0(int32_t value)
	{
		___mDataType_0 = value;
	}

	inline static int32_t get_offset_of_mData_1() { return static_cast<int32_t>(offsetof(SupplementalDataEntry_t7431C336D03C472BA9D64CEBA9BDBFACFCBDD69D, ___mData_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mData_1() const { return ___mData_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mData_1() { return &___mData_1; }
	inline void set_mData_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mData_1 = value;
		Il2CppCodeGenWriteBarrier((&___mData_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPLEMENTALDATAENTRY_T7431C336D03C472BA9D64CEBA9BDBFACFCBDD69D_H
#ifndef TLSAEADCIPHER_T14DA167F81DE9D328329AA5AB8198F8B8AD33553_H
#define TLSAEADCIPHER_T14DA167F81DE9D328329AA5AB8198F8B8AD33553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.TlsAeadCipher
struct  TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553  : public RuntimeObject
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsContext Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::context
	RuntimeObject* ___context_0;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::macSize
	int32_t ___macSize_1;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::nonce_explicit_length
	int32_t ___nonce_explicit_length_2;
	// Org.BouncyCastle.Crypto.Modes.IAeadBlockCipher Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::encryptCipher
	RuntimeObject* ___encryptCipher_3;
	// Org.BouncyCastle.Crypto.Modes.IAeadBlockCipher Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::decryptCipher
	RuntimeObject* ___decryptCipher_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::encryptImplicitNonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___encryptImplicitNonce_5;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.TlsAeadCipher::decryptImplicitNonce
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___decryptImplicitNonce_6;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553, ___context_0)); }
	inline RuntimeObject* get_context_0() const { return ___context_0; }
	inline RuntimeObject** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(RuntimeObject* value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}

	inline static int32_t get_offset_of_macSize_1() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553, ___macSize_1)); }
	inline int32_t get_macSize_1() const { return ___macSize_1; }
	inline int32_t* get_address_of_macSize_1() { return &___macSize_1; }
	inline void set_macSize_1(int32_t value)
	{
		___macSize_1 = value;
	}

	inline static int32_t get_offset_of_nonce_explicit_length_2() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553, ___nonce_explicit_length_2)); }
	inline int32_t get_nonce_explicit_length_2() const { return ___nonce_explicit_length_2; }
	inline int32_t* get_address_of_nonce_explicit_length_2() { return &___nonce_explicit_length_2; }
	inline void set_nonce_explicit_length_2(int32_t value)
	{
		___nonce_explicit_length_2 = value;
	}

	inline static int32_t get_offset_of_encryptCipher_3() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553, ___encryptCipher_3)); }
	inline RuntimeObject* get_encryptCipher_3() const { return ___encryptCipher_3; }
	inline RuntimeObject** get_address_of_encryptCipher_3() { return &___encryptCipher_3; }
	inline void set_encryptCipher_3(RuntimeObject* value)
	{
		___encryptCipher_3 = value;
		Il2CppCodeGenWriteBarrier((&___encryptCipher_3), value);
	}

	inline static int32_t get_offset_of_decryptCipher_4() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553, ___decryptCipher_4)); }
	inline RuntimeObject* get_decryptCipher_4() const { return ___decryptCipher_4; }
	inline RuntimeObject** get_address_of_decryptCipher_4() { return &___decryptCipher_4; }
	inline void set_decryptCipher_4(RuntimeObject* value)
	{
		___decryptCipher_4 = value;
		Il2CppCodeGenWriteBarrier((&___decryptCipher_4), value);
	}

	inline static int32_t get_offset_of_encryptImplicitNonce_5() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553, ___encryptImplicitNonce_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_encryptImplicitNonce_5() const { return ___encryptImplicitNonce_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_encryptImplicitNonce_5() { return &___encryptImplicitNonce_5; }
	inline void set_encryptImplicitNonce_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___encryptImplicitNonce_5 = value;
		Il2CppCodeGenWriteBarrier((&___encryptImplicitNonce_5), value);
	}

	inline static int32_t get_offset_of_decryptImplicitNonce_6() { return static_cast<int32_t>(offsetof(TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553, ___decryptImplicitNonce_6)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_decryptImplicitNonce_6() const { return ___decryptImplicitNonce_6; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_decryptImplicitNonce_6() { return &___decryptImplicitNonce_6; }
	inline void set_decryptImplicitNonce_6(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___decryptImplicitNonce_6 = value;
		Il2CppCodeGenWriteBarrier((&___decryptImplicitNonce_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TLSAEADCIPHER_T14DA167F81DE9D328329AA5AB8198F8B8AD33553_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef POLY1305KEYGENERATOR_TB7F0C06B3FACC42A3C8E3D19042E53CC323D87EA_H
#define POLY1305KEYGENERATOR_TB7F0C06B3FACC42A3C8E3D19042E53CC323D87EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Generators.Poly1305KeyGenerator
struct  Poly1305KeyGenerator_tB7F0C06B3FACC42A3C8E3D19042E53CC323D87EA  : public CipherKeyGenerator_tB9597B68C6815C63C622AA3320A0E81BF8568CE1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLY1305KEYGENERATOR_TB7F0C06B3FACC42A3C8E3D19042E53CC323D87EA_H
#ifndef DHKEYGENERATIONPARAMETERS_T7EFE3D05B481CE4EC6E8B4C9E5EF13E970B64078_H
#define DHKEYGENERATIONPARAMETERS_T7EFE3D05B481CE4EC6E8B4C9E5EF13E970B64078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.DHKeyGenerationParameters
struct  DHKeyGenerationParameters_t7EFE3D05B481CE4EC6E8B4C9E5EF13E970B64078  : public KeyGenerationParameters_tDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6
{
public:
	// Org.BouncyCastle.Crypto.Parameters.DHParameters Org.BouncyCastle.Crypto.Parameters.DHKeyGenerationParameters::parameters
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * ___parameters_2;

public:
	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(DHKeyGenerationParameters_t7EFE3D05B481CE4EC6E8B4C9E5EF13E970B64078, ___parameters_2)); }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * get_parameters_2() const { return ___parameters_2; }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 ** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHKEYGENERATIONPARAMETERS_T7EFE3D05B481CE4EC6E8B4C9E5EF13E970B64078_H
#ifndef DHKEYPARAMETERS_T74E59C4811FF2499B6F2F487B9B15DE6E426911C_H
#define DHKEYPARAMETERS_T74E59C4811FF2499B6F2F487B9B15DE6E426911C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.DHKeyParameters
struct  DHKeyParameters_t74E59C4811FF2499B6F2F487B9B15DE6E426911C  : public AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7
{
public:
	// Org.BouncyCastle.Crypto.Parameters.DHParameters Org.BouncyCastle.Crypto.Parameters.DHKeyParameters::parameters
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * ___parameters_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Crypto.Parameters.DHKeyParameters::algorithmOid
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___algorithmOid_2;

public:
	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(DHKeyParameters_t74E59C4811FF2499B6F2F487B9B15DE6E426911C, ___parameters_1)); }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * get_parameters_1() const { return ___parameters_1; }
	inline DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}

	inline static int32_t get_offset_of_algorithmOid_2() { return static_cast<int32_t>(offsetof(DHKeyParameters_t74E59C4811FF2499B6F2F487B9B15DE6E426911C, ___algorithmOid_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_algorithmOid_2() const { return ___algorithmOid_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_algorithmOid_2() { return &___algorithmOid_2; }
	inline void set_algorithmOid_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___algorithmOid_2 = value;
		Il2CppCodeGenWriteBarrier((&___algorithmOid_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHKEYPARAMETERS_T74E59C4811FF2499B6F2F487B9B15DE6E426911C_H
#ifndef DSAKEYPARAMETERS_TBF3E3B9B02243E5C768006D70F4CA496F15E82A6_H
#define DSAKEYPARAMETERS_TBF3E3B9B02243E5C768006D70F4CA496F15E82A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.DsaKeyParameters
struct  DsaKeyParameters_tBF3E3B9B02243E5C768006D70F4CA496F15E82A6  : public AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7
{
public:
	// Org.BouncyCastle.Crypto.Parameters.DsaParameters Org.BouncyCastle.Crypto.Parameters.DsaKeyParameters::parameters
	DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807 * ___parameters_1;

public:
	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(DsaKeyParameters_tBF3E3B9B02243E5C768006D70F4CA496F15E82A6, ___parameters_1)); }
	inline DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807 * get_parameters_1() const { return ___parameters_1; }
	inline DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAKEYPARAMETERS_TBF3E3B9B02243E5C768006D70F4CA496F15E82A6_H
#ifndef ECKEYGENERATIONPARAMETERS_TC5555EDF06F4273B4B637DA006AB968AC7FA7413_H
#define ECKEYGENERATIONPARAMETERS_TC5555EDF06F4273B4B637DA006AB968AC7FA7413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.ECKeyGenerationParameters
struct  ECKeyGenerationParameters_tC5555EDF06F4273B4B637DA006AB968AC7FA7413  : public KeyGenerationParameters_tDA1E9297A6F944BA4FF44DBC3E94089DB7F7D2B6
{
public:
	// Org.BouncyCastle.Crypto.Parameters.ECDomainParameters Org.BouncyCastle.Crypto.Parameters.ECKeyGenerationParameters::domainParams
	ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57 * ___domainParams_2;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Crypto.Parameters.ECKeyGenerationParameters::publicKeyParamSet
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___publicKeyParamSet_3;

public:
	inline static int32_t get_offset_of_domainParams_2() { return static_cast<int32_t>(offsetof(ECKeyGenerationParameters_tC5555EDF06F4273B4B637DA006AB968AC7FA7413, ___domainParams_2)); }
	inline ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57 * get_domainParams_2() const { return ___domainParams_2; }
	inline ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57 ** get_address_of_domainParams_2() { return &___domainParams_2; }
	inline void set_domainParams_2(ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57 * value)
	{
		___domainParams_2 = value;
		Il2CppCodeGenWriteBarrier((&___domainParams_2), value);
	}

	inline static int32_t get_offset_of_publicKeyParamSet_3() { return static_cast<int32_t>(offsetof(ECKeyGenerationParameters_tC5555EDF06F4273B4B637DA006AB968AC7FA7413, ___publicKeyParamSet_3)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_publicKeyParamSet_3() const { return ___publicKeyParamSet_3; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_publicKeyParamSet_3() { return &___publicKeyParamSet_3; }
	inline void set_publicKeyParamSet_3(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___publicKeyParamSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyParamSet_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECKEYGENERATIONPARAMETERS_TC5555EDF06F4273B4B637DA006AB968AC7FA7413_H
#ifndef ECKEYPARAMETERS_T0F4612DCE3141CDAC4A8D33189A966009AF3F1F1_H
#define ECKEYPARAMETERS_T0F4612DCE3141CDAC4A8D33189A966009AF3F1F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.ECKeyParameters
struct  ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1  : public AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7
{
public:
	// System.String Org.BouncyCastle.Crypto.Parameters.ECKeyParameters::algorithm
	String_t* ___algorithm_2;
	// Org.BouncyCastle.Crypto.Parameters.ECDomainParameters Org.BouncyCastle.Crypto.Parameters.ECKeyParameters::parameters
	ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57 * ___parameters_3;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Crypto.Parameters.ECKeyParameters::publicKeyParamSet
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___publicKeyParamSet_4;

public:
	inline static int32_t get_offset_of_algorithm_2() { return static_cast<int32_t>(offsetof(ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1, ___algorithm_2)); }
	inline String_t* get_algorithm_2() const { return ___algorithm_2; }
	inline String_t** get_address_of_algorithm_2() { return &___algorithm_2; }
	inline void set_algorithm_2(String_t* value)
	{
		___algorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&___algorithm_2), value);
	}

	inline static int32_t get_offset_of_parameters_3() { return static_cast<int32_t>(offsetof(ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1, ___parameters_3)); }
	inline ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57 * get_parameters_3() const { return ___parameters_3; }
	inline ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57 ** get_address_of_parameters_3() { return &___parameters_3; }
	inline void set_parameters_3(ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57 * value)
	{
		___parameters_3 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_3), value);
	}

	inline static int32_t get_offset_of_publicKeyParamSet_4() { return static_cast<int32_t>(offsetof(ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1, ___publicKeyParamSet_4)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_publicKeyParamSet_4() const { return ___publicKeyParamSet_4; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_publicKeyParamSet_4() { return &___publicKeyParamSet_4; }
	inline void set_publicKeyParamSet_4(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___publicKeyParamSet_4 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyParamSet_4), value);
	}
};

struct ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1_StaticFields
{
public:
	// System.String[] Org.BouncyCastle.Crypto.Parameters.ECKeyParameters::algorithms
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___algorithms_1;

public:
	inline static int32_t get_offset_of_algorithms_1() { return static_cast<int32_t>(offsetof(ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1_StaticFields, ___algorithms_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_algorithms_1() const { return ___algorithms_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_algorithms_1() { return &___algorithms_1; }
	inline void set_algorithms_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___algorithms_1 = value;
		Il2CppCodeGenWriteBarrier((&___algorithms_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECKEYPARAMETERS_T0F4612DCE3141CDAC4A8D33189A966009AF3F1F1_H
#ifndef ELGAMALKEYPARAMETERS_TE099F61B83BEC92CD2DDFB27273FAA868EEE0DE2_H
#define ELGAMALKEYPARAMETERS_TE099F61B83BEC92CD2DDFB27273FAA868EEE0DE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.ElGamalKeyParameters
struct  ElGamalKeyParameters_tE099F61B83BEC92CD2DDFB27273FAA868EEE0DE2  : public AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7
{
public:
	// Org.BouncyCastle.Crypto.Parameters.ElGamalParameters Org.BouncyCastle.Crypto.Parameters.ElGamalKeyParameters::parameters
	ElGamalParameters_tC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA * ___parameters_1;

public:
	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(ElGamalKeyParameters_tE099F61B83BEC92CD2DDFB27273FAA868EEE0DE2, ___parameters_1)); }
	inline ElGamalParameters_tC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA * get_parameters_1() const { return ___parameters_1; }
	inline ElGamalParameters_tC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(ElGamalParameters_tC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELGAMALKEYPARAMETERS_TE099F61B83BEC92CD2DDFB27273FAA868EEE0DE2_H
#ifndef GOST3410KEYPARAMETERS_TAAD1AD5F82380026117A6E14A1C947AE92951C3F_H
#define GOST3410KEYPARAMETERS_TAAD1AD5F82380026117A6E14A1C947AE92951C3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.Gost3410KeyParameters
struct  Gost3410KeyParameters_tAAD1AD5F82380026117A6E14A1C947AE92951C3F  : public AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7
{
public:
	// Org.BouncyCastle.Crypto.Parameters.Gost3410Parameters Org.BouncyCastle.Crypto.Parameters.Gost3410KeyParameters::parameters
	Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD * ___parameters_1;
	// Org.BouncyCastle.Asn1.DerObjectIdentifier Org.BouncyCastle.Crypto.Parameters.Gost3410KeyParameters::publicKeyParamSet
	DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * ___publicKeyParamSet_2;

public:
	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(Gost3410KeyParameters_tAAD1AD5F82380026117A6E14A1C947AE92951C3F, ___parameters_1)); }
	inline Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD * get_parameters_1() const { return ___parameters_1; }
	inline Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}

	inline static int32_t get_offset_of_publicKeyParamSet_2() { return static_cast<int32_t>(offsetof(Gost3410KeyParameters_tAAD1AD5F82380026117A6E14A1C947AE92951C3F, ___publicKeyParamSet_2)); }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * get_publicKeyParamSet_2() const { return ___publicKeyParamSet_2; }
	inline DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 ** get_address_of_publicKeyParamSet_2() { return &___publicKeyParamSet_2; }
	inline void set_publicKeyParamSet_2(DerObjectIdentifier_tC3F1B6EAC07B07726FEC51CED107FBC369DEF441 * value)
	{
		___publicKeyParamSet_2 = value;
		Il2CppCodeGenWriteBarrier((&___publicKeyParamSet_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410KEYPARAMETERS_TAAD1AD5F82380026117A6E14A1C947AE92951C3F_H
#ifndef RSAKEYPARAMETERS_T57791EDEBAF0AD93C55C7FCF645D7CBB6284D308_H
#define RSAKEYPARAMETERS_T57791EDEBAF0AD93C55C7FCF645D7CBB6284D308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters
struct  RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308  : public AsymmetricKeyParameter_tD6918D64A0FB774AF79E386A85415CE53D5EC0D7
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters::modulus
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___modulus_1;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.RsaKeyParameters::exponent
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___exponent_2;

public:
	inline static int32_t get_offset_of_modulus_1() { return static_cast<int32_t>(offsetof(RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308, ___modulus_1)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_modulus_1() const { return ___modulus_1; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_modulus_1() { return &___modulus_1; }
	inline void set_modulus_1(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___modulus_1 = value;
		Il2CppCodeGenWriteBarrier((&___modulus_1), value);
	}

	inline static int32_t get_offset_of_exponent_2() { return static_cast<int32_t>(offsetof(RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308, ___exponent_2)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_exponent_2() const { return ___exponent_2; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_exponent_2() { return &___exponent_2; }
	inline void set_exponent_2(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___exponent_2 = value;
		Il2CppCodeGenWriteBarrier((&___exponent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAKEYPARAMETERS_T57791EDEBAF0AD93C55C7FCF645D7CBB6284D308_H
#ifndef ABSTRACTTLSCLIENT_TC6DAEE1B4537D4C522014C4971FC936BD5C964F7_H
#define ABSTRACTTLSCLIENT_TC6DAEE1B4537D4C522014C4971FC936BD5C964F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.AbstractTlsClient
struct  AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7  : public AbstractTlsPeer_t26CA3EF032C693BEB7331E2A0972A8620596CC9F
{
public:
	// Org.BouncyCastle.Crypto.Tls.TlsCipherFactory Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mCipherFactory
	RuntimeObject* ___mCipherFactory_0;
	// Org.BouncyCastle.Crypto.Tls.TlsClientContext Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mContext
	RuntimeObject* ___mContext_1;
	// System.Collections.IList Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSupportedSignatureAlgorithms
	RuntimeObject* ___mSupportedSignatureAlgorithms_2;
	// System.Int32[] Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mNamedCurves
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___mNamedCurves_3;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mClientECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mClientECPointFormats_4;
	// System.Byte[] Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mServerECPointFormats
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___mServerECPointFormats_5;
	// System.Int32 Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSelectedCipherSuite
	int32_t ___mSelectedCipherSuite_6;
	// System.Int16 Org.BouncyCastle.Crypto.Tls.AbstractTlsClient::mSelectedCompressionMethod
	int16_t ___mSelectedCompressionMethod_7;

public:
	inline static int32_t get_offset_of_mCipherFactory_0() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mCipherFactory_0)); }
	inline RuntimeObject* get_mCipherFactory_0() const { return ___mCipherFactory_0; }
	inline RuntimeObject** get_address_of_mCipherFactory_0() { return &___mCipherFactory_0; }
	inline void set_mCipherFactory_0(RuntimeObject* value)
	{
		___mCipherFactory_0 = value;
		Il2CppCodeGenWriteBarrier((&___mCipherFactory_0), value);
	}

	inline static int32_t get_offset_of_mContext_1() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mContext_1)); }
	inline RuntimeObject* get_mContext_1() const { return ___mContext_1; }
	inline RuntimeObject** get_address_of_mContext_1() { return &___mContext_1; }
	inline void set_mContext_1(RuntimeObject* value)
	{
		___mContext_1 = value;
		Il2CppCodeGenWriteBarrier((&___mContext_1), value);
	}

	inline static int32_t get_offset_of_mSupportedSignatureAlgorithms_2() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mSupportedSignatureAlgorithms_2)); }
	inline RuntimeObject* get_mSupportedSignatureAlgorithms_2() const { return ___mSupportedSignatureAlgorithms_2; }
	inline RuntimeObject** get_address_of_mSupportedSignatureAlgorithms_2() { return &___mSupportedSignatureAlgorithms_2; }
	inline void set_mSupportedSignatureAlgorithms_2(RuntimeObject* value)
	{
		___mSupportedSignatureAlgorithms_2 = value;
		Il2CppCodeGenWriteBarrier((&___mSupportedSignatureAlgorithms_2), value);
	}

	inline static int32_t get_offset_of_mNamedCurves_3() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mNamedCurves_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_mNamedCurves_3() const { return ___mNamedCurves_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_mNamedCurves_3() { return &___mNamedCurves_3; }
	inline void set_mNamedCurves_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___mNamedCurves_3 = value;
		Il2CppCodeGenWriteBarrier((&___mNamedCurves_3), value);
	}

	inline static int32_t get_offset_of_mClientECPointFormats_4() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mClientECPointFormats_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mClientECPointFormats_4() const { return ___mClientECPointFormats_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mClientECPointFormats_4() { return &___mClientECPointFormats_4; }
	inline void set_mClientECPointFormats_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mClientECPointFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___mClientECPointFormats_4), value);
	}

	inline static int32_t get_offset_of_mServerECPointFormats_5() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mServerECPointFormats_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_mServerECPointFormats_5() const { return ___mServerECPointFormats_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_mServerECPointFormats_5() { return &___mServerECPointFormats_5; }
	inline void set_mServerECPointFormats_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___mServerECPointFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___mServerECPointFormats_5), value);
	}

	inline static int32_t get_offset_of_mSelectedCipherSuite_6() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mSelectedCipherSuite_6)); }
	inline int32_t get_mSelectedCipherSuite_6() const { return ___mSelectedCipherSuite_6; }
	inline int32_t* get_address_of_mSelectedCipherSuite_6() { return &___mSelectedCipherSuite_6; }
	inline void set_mSelectedCipherSuite_6(int32_t value)
	{
		___mSelectedCipherSuite_6 = value;
	}

	inline static int32_t get_offset_of_mSelectedCompressionMethod_7() { return static_cast<int32_t>(offsetof(AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7, ___mSelectedCompressionMethod_7)); }
	inline int16_t get_mSelectedCompressionMethod_7() const { return ___mSelectedCompressionMethod_7; }
	inline int16_t* get_address_of_mSelectedCompressionMethod_7() { return &___mSelectedCompressionMethod_7; }
	inline void set_mSelectedCompressionMethod_7(int16_t value)
	{
		___mSelectedCompressionMethod_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTTLSCLIENT_TC6DAEE1B4537D4C522014C4971FC936BD5C964F7_H
#ifndef DEFAULTTLSCIPHERFACTORY_TFCE3DDE22DA9FF8EB9AF1FF07ECF326DD97F863F_H
#define DEFAULTTLSCIPHERFACTORY_TFCE3DDE22DA9FF8EB9AF1FF07ECF326DD97F863F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.DefaultTlsCipherFactory
struct  DefaultTlsCipherFactory_tFCE3DDE22DA9FF8EB9AF1FF07ECF326DD97F863F  : public AbstractTlsCipherFactory_t95A664781E8103D14B4E9186E35D2826C17C47FB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTLSCIPHERFACTORY_TFCE3DDE22DA9FF8EB9AF1FF07ECF326DD97F863F_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream_ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_3;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_4;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_3)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_3() const { return ____activeReadWriteTask_3; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_3() { return &____activeReadWriteTask_3; }
	inline void set__activeReadWriteTask_3(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_3 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_3), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_4() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_4)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_4() const { return ____asyncActiveSemaphore_4; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_4() { return &____asyncActiveSemaphore_4; }
	inline void set__asyncActiveSemaphore_4(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_4 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_4), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef DHPRIVATEKEYPARAMETERS_T7A930900443BCA929045970B99A92EADD777049C_H
#define DHPRIVATEKEYPARAMETERS_T7A930900443BCA929045970B99A92EADD777049C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters
struct  DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C  : public DHKeyParameters_t74E59C4811FF2499B6F2F487B9B15DE6E426911C
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.DHPrivateKeyParameters::x
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___x_3;

public:
	inline static int32_t get_offset_of_x_3() { return static_cast<int32_t>(offsetof(DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C, ___x_3)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_x_3() const { return ___x_3; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_x_3() { return &___x_3; }
	inline void set_x_3(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___x_3 = value;
		Il2CppCodeGenWriteBarrier((&___x_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHPRIVATEKEYPARAMETERS_T7A930900443BCA929045970B99A92EADD777049C_H
#ifndef DHPUBLICKEYPARAMETERS_TBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA_H
#define DHPUBLICKEYPARAMETERS_TBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.DHPublicKeyParameters
struct  DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA  : public DHKeyParameters_t74E59C4811FF2499B6F2F487B9B15DE6E426911C
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.DHPublicKeyParameters::y
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___y_3;

public:
	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA, ___y_3)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_y_3() const { return ___y_3; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___y_3 = value;
		Il2CppCodeGenWriteBarrier((&___y_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DHPUBLICKEYPARAMETERS_TBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA_H
#ifndef DSAPRIVATEKEYPARAMETERS_T6CFD8006550A801F95DE67946DB700B7D04F6ACE_H
#define DSAPRIVATEKEYPARAMETERS_T6CFD8006550A801F95DE67946DB700B7D04F6ACE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.DsaPrivateKeyParameters
struct  DsaPrivateKeyParameters_t6CFD8006550A801F95DE67946DB700B7D04F6ACE  : public DsaKeyParameters_tBF3E3B9B02243E5C768006D70F4CA496F15E82A6
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.DsaPrivateKeyParameters::x
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___x_2;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(DsaPrivateKeyParameters_t6CFD8006550A801F95DE67946DB700B7D04F6ACE, ___x_2)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_x_2() const { return ___x_2; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___x_2 = value;
		Il2CppCodeGenWriteBarrier((&___x_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAPRIVATEKEYPARAMETERS_T6CFD8006550A801F95DE67946DB700B7D04F6ACE_H
#ifndef DSAPUBLICKEYPARAMETERS_TBC11DDF41C098CB0EA439AEEAEF375C716BC654D_H
#define DSAPUBLICKEYPARAMETERS_TBC11DDF41C098CB0EA439AEEAEF375C716BC654D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.DsaPublicKeyParameters
struct  DsaPublicKeyParameters_tBC11DDF41C098CB0EA439AEEAEF375C716BC654D  : public DsaKeyParameters_tBF3E3B9B02243E5C768006D70F4CA496F15E82A6
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.DsaPublicKeyParameters::y
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___y_2;

public:
	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(DsaPublicKeyParameters_tBC11DDF41C098CB0EA439AEEAEF375C716BC654D, ___y_2)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_y_2() const { return ___y_2; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___y_2 = value;
		Il2CppCodeGenWriteBarrier((&___y_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DSAPUBLICKEYPARAMETERS_TBC11DDF41C098CB0EA439AEEAEF375C716BC654D_H
#ifndef ECPRIVATEKEYPARAMETERS_TC1317EEDAEF16688C98CD41053BB95C6751BBD1F_H
#define ECPRIVATEKEYPARAMETERS_TC1317EEDAEF16688C98CD41053BB95C6751BBD1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters
struct  ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F  : public ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.ECPrivateKeyParameters::d
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___d_5;

public:
	inline static int32_t get_offset_of_d_5() { return static_cast<int32_t>(offsetof(ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F, ___d_5)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_d_5() const { return ___d_5; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_d_5() { return &___d_5; }
	inline void set_d_5(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___d_5 = value;
		Il2CppCodeGenWriteBarrier((&___d_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPRIVATEKEYPARAMETERS_TC1317EEDAEF16688C98CD41053BB95C6751BBD1F_H
#ifndef ECPUBLICKEYPARAMETERS_T84FD118FD3407D284EF46E8358AE185634537702_H
#define ECPUBLICKEYPARAMETERS_T84FD118FD3407D284EF46E8358AE185634537702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters
struct  ECPublicKeyParameters_t84FD118FD3407D284EF46E8358AE185634537702  : public ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1
{
public:
	// Org.BouncyCastle.Math.EC.ECPoint Org.BouncyCastle.Crypto.Parameters.ECPublicKeyParameters::q
	ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 * ___q_5;

public:
	inline static int32_t get_offset_of_q_5() { return static_cast<int32_t>(offsetof(ECPublicKeyParameters_t84FD118FD3407D284EF46E8358AE185634537702, ___q_5)); }
	inline ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 * get_q_5() const { return ___q_5; }
	inline ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 ** get_address_of_q_5() { return &___q_5; }
	inline void set_q_5(ECPoint_tCC0C4447DEFF70FC9D68F5149ECCCC0533CE4399 * value)
	{
		___q_5 = value;
		Il2CppCodeGenWriteBarrier((&___q_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECPUBLICKEYPARAMETERS_T84FD118FD3407D284EF46E8358AE185634537702_H
#ifndef ELGAMALPUBLICKEYPARAMETERS_T65E8DA5C9AF34FDA6E20854D55752CE99F8898FA_H
#define ELGAMALPUBLICKEYPARAMETERS_T65E8DA5C9AF34FDA6E20854D55752CE99F8898FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.ElGamalPublicKeyParameters
struct  ElGamalPublicKeyParameters_t65E8DA5C9AF34FDA6E20854D55752CE99F8898FA  : public ElGamalKeyParameters_tE099F61B83BEC92CD2DDFB27273FAA868EEE0DE2
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.ElGamalPublicKeyParameters::y
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___y_2;

public:
	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(ElGamalPublicKeyParameters_t65E8DA5C9AF34FDA6E20854D55752CE99F8898FA, ___y_2)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_y_2() const { return ___y_2; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___y_2 = value;
		Il2CppCodeGenWriteBarrier((&___y_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELGAMALPUBLICKEYPARAMETERS_T65E8DA5C9AF34FDA6E20854D55752CE99F8898FA_H
#ifndef GOST3410PUBLICKEYPARAMETERS_T084516D277AAE667572C6641A42A212EEDE1E3C5_H
#define GOST3410PUBLICKEYPARAMETERS_T084516D277AAE667572C6641A42A212EEDE1E3C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.Gost3410PublicKeyParameters
struct  Gost3410PublicKeyParameters_t084516D277AAE667572C6641A42A212EEDE1E3C5  : public Gost3410KeyParameters_tAAD1AD5F82380026117A6E14A1C947AE92951C3F
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.Gost3410PublicKeyParameters::y
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___y_3;

public:
	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Gost3410PublicKeyParameters_t084516D277AAE667572C6641A42A212EEDE1E3C5, ___y_3)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_y_3() const { return ___y_3; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___y_3 = value;
		Il2CppCodeGenWriteBarrier((&___y_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOST3410PUBLICKEYPARAMETERS_T084516D277AAE667572C6641A42A212EEDE1E3C5_H
#ifndef RSAPRIVATECRTKEYPARAMETERS_TDF12AEB7CC6881585CF7D1985C2F3656FD2E1040_H
#define RSAPRIVATECRTKEYPARAMETERS_TDF12AEB7CC6881585CF7D1985C2F3656FD2E1040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters
struct  RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040  : public RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308
{
public:
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters::e
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___e_3;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters::p
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___p_4;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters::q
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___q_5;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters::dP
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___dP_6;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters::dQ
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___dQ_7;
	// Org.BouncyCastle.Math.BigInteger Org.BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters::qInv
	BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * ___qInv_8;

public:
	inline static int32_t get_offset_of_e_3() { return static_cast<int32_t>(offsetof(RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040, ___e_3)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_e_3() const { return ___e_3; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_e_3() { return &___e_3; }
	inline void set_e_3(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___e_3 = value;
		Il2CppCodeGenWriteBarrier((&___e_3), value);
	}

	inline static int32_t get_offset_of_p_4() { return static_cast<int32_t>(offsetof(RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040, ___p_4)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_p_4() const { return ___p_4; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_p_4() { return &___p_4; }
	inline void set_p_4(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___p_4 = value;
		Il2CppCodeGenWriteBarrier((&___p_4), value);
	}

	inline static int32_t get_offset_of_q_5() { return static_cast<int32_t>(offsetof(RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040, ___q_5)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_q_5() const { return ___q_5; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_q_5() { return &___q_5; }
	inline void set_q_5(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___q_5 = value;
		Il2CppCodeGenWriteBarrier((&___q_5), value);
	}

	inline static int32_t get_offset_of_dP_6() { return static_cast<int32_t>(offsetof(RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040, ___dP_6)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_dP_6() const { return ___dP_6; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_dP_6() { return &___dP_6; }
	inline void set_dP_6(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___dP_6 = value;
		Il2CppCodeGenWriteBarrier((&___dP_6), value);
	}

	inline static int32_t get_offset_of_dQ_7() { return static_cast<int32_t>(offsetof(RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040, ___dQ_7)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_dQ_7() const { return ___dQ_7; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_dQ_7() { return &___dQ_7; }
	inline void set_dQ_7(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___dQ_7 = value;
		Il2CppCodeGenWriteBarrier((&___dQ_7), value);
	}

	inline static int32_t get_offset_of_qInv_8() { return static_cast<int32_t>(offsetof(RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040, ___qInv_8)); }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * get_qInv_8() const { return ___qInv_8; }
	inline BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 ** get_address_of_qInv_8() { return &___qInv_8; }
	inline void set_qInv_8(BigInteger_t882A86470CD3652F725EAF0F521C5AF9A00F8D91 * value)
	{
		___qInv_8 = value;
		Il2CppCodeGenWriteBarrier((&___qInv_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAPRIVATECRTKEYPARAMETERS_TDF12AEB7CC6881585CF7D1985C2F3656FD2E1040_H
#ifndef DEFAULTTLSCLIENT_T3AEEC6138E8000BF0EB194E242413CB61B9EFF0E_H
#define DEFAULTTLSCLIENT_T3AEEC6138E8000BF0EB194E242413CB61B9EFF0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.DefaultTlsClient
struct  DefaultTlsClient_t3AEEC6138E8000BF0EB194E242413CB61B9EFF0E  : public AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTTLSCLIENT_T3AEEC6138E8000BF0EB194E242413CB61B9EFF0E_H
#ifndef BASEOUTPUTSTREAM_TA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832_H
#define BASEOUTPUTSTREAM_TA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Utilities.IO.BaseOutputStream
struct  BaseOutputStream_tA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Boolean Org.BouncyCastle.Utilities.IO.BaseOutputStream::closed
	bool ___closed_5;

public:
	inline static int32_t get_offset_of_closed_5() { return static_cast<int32_t>(offsetof(BaseOutputStream_tA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832, ___closed_5)); }
	inline bool get_closed_5() const { return ___closed_5; }
	inline bool* get_address_of_closed_5() { return &___closed_5; }
	inline void set_closed_5(bool value)
	{
		___closed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEOUTPUTSTREAM_TA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832_H
#ifndef MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#define MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MemoryStream
struct  MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buffer_5;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_6;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_7;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_8;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_9;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_10;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_11;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_12;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_13;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * ____lastReadTask_14;

public:
	inline static int32_t get_offset_of__buffer_5() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____buffer_5)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buffer_5() const { return ____buffer_5; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buffer_5() { return &____buffer_5; }
	inline void set__buffer_5(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_5), value);
	}

	inline static int32_t get_offset_of__origin_6() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____origin_6)); }
	inline int32_t get__origin_6() const { return ____origin_6; }
	inline int32_t* get_address_of__origin_6() { return &____origin_6; }
	inline void set__origin_6(int32_t value)
	{
		____origin_6 = value;
	}

	inline static int32_t get_offset_of__position_7() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____position_7)); }
	inline int32_t get__position_7() const { return ____position_7; }
	inline int32_t* get_address_of__position_7() { return &____position_7; }
	inline void set__position_7(int32_t value)
	{
		____position_7 = value;
	}

	inline static int32_t get_offset_of__length_8() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____length_8)); }
	inline int32_t get__length_8() const { return ____length_8; }
	inline int32_t* get_address_of__length_8() { return &____length_8; }
	inline void set__length_8(int32_t value)
	{
		____length_8 = value;
	}

	inline static int32_t get_offset_of__capacity_9() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____capacity_9)); }
	inline int32_t get__capacity_9() const { return ____capacity_9; }
	inline int32_t* get_address_of__capacity_9() { return &____capacity_9; }
	inline void set__capacity_9(int32_t value)
	{
		____capacity_9 = value;
	}

	inline static int32_t get_offset_of__expandable_10() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____expandable_10)); }
	inline bool get__expandable_10() const { return ____expandable_10; }
	inline bool* get_address_of__expandable_10() { return &____expandable_10; }
	inline void set__expandable_10(bool value)
	{
		____expandable_10 = value;
	}

	inline static int32_t get_offset_of__writable_11() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____writable_11)); }
	inline bool get__writable_11() const { return ____writable_11; }
	inline bool* get_address_of__writable_11() { return &____writable_11; }
	inline void set__writable_11(bool value)
	{
		____writable_11 = value;
	}

	inline static int32_t get_offset_of__exposable_12() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____exposable_12)); }
	inline bool get__exposable_12() const { return ____exposable_12; }
	inline bool* get_address_of__exposable_12() { return &____exposable_12; }
	inline void set__exposable_12(bool value)
	{
		____exposable_12 = value;
	}

	inline static int32_t get_offset_of__isOpen_13() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____isOpen_13)); }
	inline bool get__isOpen_13() const { return ____isOpen_13; }
	inline bool* get_address_of__isOpen_13() { return &____isOpen_13; }
	inline void set__isOpen_13(bool value)
	{
		____isOpen_13 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_14() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____lastReadTask_14)); }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * get__lastReadTask_14() const { return ____lastReadTask_14; }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 ** get_address_of__lastReadTask_14() { return &____lastReadTask_14; }
	inline void set__lastReadTask_14(Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * value)
	{
		____lastReadTask_14 = value;
		Il2CppCodeGenWriteBarrier((&____lastReadTask_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifndef DIGESTINPUTBUFFER_T117CEE4FCA532842726AF5BFE55FFDFC708B811D_H
#define DIGESTINPUTBUFFER_T117CEE4FCA532842726AF5BFE55FFDFC708B811D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.DigestInputBuffer
struct  DigestInputBuffer_t117CEE4FCA532842726AF5BFE55FFDFC708B811D  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGESTINPUTBUFFER_T117CEE4FCA532842726AF5BFE55FFDFC708B811D_H
#ifndef DIGSTREAM_TD4CE38226FC22D1A410F23711CBC52A3590147F3_H
#define DIGSTREAM_TD4CE38226FC22D1A410F23711CBC52A3590147F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.DigestInputBuffer_DigStream
struct  DigStream_tD4CE38226FC22D1A410F23711CBC52A3590147F3  : public BaseOutputStream_tA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832
{
public:
	// Org.BouncyCastle.Crypto.IDigest Org.BouncyCastle.Crypto.Tls.DigestInputBuffer_DigStream::d
	RuntimeObject* ___d_6;

public:
	inline static int32_t get_offset_of_d_6() { return static_cast<int32_t>(offsetof(DigStream_tD4CE38226FC22D1A410F23711CBC52A3590147F3, ___d_6)); }
	inline RuntimeObject* get_d_6() const { return ___d_6; }
	inline RuntimeObject** get_address_of_d_6() { return &___d_6; }
	inline void set_d_6(RuntimeObject* value)
	{
		___d_6 = value;
		Il2CppCodeGenWriteBarrier((&___d_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGSTREAM_TD4CE38226FC22D1A410F23711CBC52A3590147F3_H
#ifndef SIGNERINPUTBUFFER_TADF941FEEC74D73D7EA670CDF8961A0229C0AFFB_H
#define SIGNERINPUTBUFFER_TADF941FEEC74D73D7EA670CDF8961A0229C0AFFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.SignerInputBuffer
struct  SignerInputBuffer_tADF941FEEC74D73D7EA670CDF8961A0229C0AFFB  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGNERINPUTBUFFER_TADF941FEEC74D73D7EA670CDF8961A0229C0AFFB_H
#ifndef SIGSTREAM_T25F33FC2964511F82619E544B1D0504CA4566513_H
#define SIGSTREAM_T25F33FC2964511F82619E544B1D0504CA4566513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Org.BouncyCastle.Crypto.Tls.SignerInputBuffer_SigStream
struct  SigStream_t25F33FC2964511F82619E544B1D0504CA4566513  : public BaseOutputStream_tA1CB4DF3C39A0E66F3766B324DA65FCEBF73D832
{
public:
	// Org.BouncyCastle.Crypto.ISigner Org.BouncyCastle.Crypto.Tls.SignerInputBuffer_SigStream::s
	RuntimeObject* ___s_6;

public:
	inline static int32_t get_offset_of_s_6() { return static_cast<int32_t>(offsetof(SigStream_t25F33FC2964511F82619E544B1D0504CA4566513, ___s_6)); }
	inline RuntimeObject* get_s_6() const { return ___s_6; }
	inline RuntimeObject** get_address_of_s_6() { return &___s_6; }
	inline void set_s_6(RuntimeObject* value)
	{
		___s_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGSTREAM_T25F33FC2964511F82619E544B1D0504CA4566513_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5100 = { sizeof (SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A), -1, sizeof(SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5100[7] = 
{
	SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A_StaticFields::get_offset_of_SS0_0(),
	SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A_StaticFields::get_offset_of_SS1_1(),
	SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A_StaticFields::get_offset_of_SS2_2(),
	SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A_StaticFields::get_offset_of_SS3_3(),
	SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A_StaticFields::get_offset_of_KC_4(),
	SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A::get_offset_of_wKey_5(),
	SeedEngine_tCB01373AD403AC5EC3DA75A5B81087253068C62A::get_offset_of_forEncryption_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5101 = { sizeof (Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C), -1, sizeof(Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5101[12] = 
{
	Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_StaticFields::get_offset_of_DEFAULT_ROUNDS_0(),
	Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_StaticFields::get_offset_of_sigma_1(),
	Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C_StaticFields::get_offset_of_tau_2(),
	Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C::get_offset_of_rounds_3(),
	Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C::get_offset_of_index_4(),
	Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C::get_offset_of_engineState_5(),
	Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C::get_offset_of_x_6(),
	Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C::get_offset_of_keyStream_7(),
	Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C::get_offset_of_initialised_8(),
	Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C::get_offset_of_cW0_9(),
	Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C::get_offset_of_cW1_10(),
	Salsa20Engine_t5346D9847A884ABE981EC15CAEC9A2E09A6B4E5C::get_offset_of_cW2_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5102 = { sizeof (DHBasicKeyPairGenerator_t8665F4F588356BDFED1CE9232B45BBEAEE2688AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5102[1] = 
{
	DHBasicKeyPairGenerator_t8665F4F588356BDFED1CE9232B45BBEAEE2688AD::get_offset_of_param_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5103 = { sizeof (DHKeyGeneratorHelper_t53E0C55FAA562861BAF57BA31599D85D6F8AD7D6), -1, sizeof(DHKeyGeneratorHelper_t53E0C55FAA562861BAF57BA31599D85D6F8AD7D6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5103[1] = 
{
	DHKeyGeneratorHelper_t53E0C55FAA562861BAF57BA31599D85D6F8AD7D6_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5104 = { sizeof (ECKeyPairGenerator_tEF1C73B021080F994111392005EA8DAF6BB1D36F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5104[4] = 
{
	ECKeyPairGenerator_tEF1C73B021080F994111392005EA8DAF6BB1D36F::get_offset_of_algorithm_0(),
	ECKeyPairGenerator_tEF1C73B021080F994111392005EA8DAF6BB1D36F::get_offset_of_parameters_1(),
	ECKeyPairGenerator_tEF1C73B021080F994111392005EA8DAF6BB1D36F::get_offset_of_publicKeyParamSet_2(),
	ECKeyPairGenerator_tEF1C73B021080F994111392005EA8DAF6BB1D36F::get_offset_of_random_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5105 = { sizeof (Poly1305KeyGenerator_tB7F0C06B3FACC42A3C8E3D19042E53CC323D87EA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5106 = { sizeof (CbcBlockCipherMac_tF3EEE86C592D72AAB46685074C00F63508623048), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5106[5] = 
{
	CbcBlockCipherMac_tF3EEE86C592D72AAB46685074C00F63508623048::get_offset_of_buf_0(),
	CbcBlockCipherMac_tF3EEE86C592D72AAB46685074C00F63508623048::get_offset_of_bufOff_1(),
	CbcBlockCipherMac_tF3EEE86C592D72AAB46685074C00F63508623048::get_offset_of_cipher_2(),
	CbcBlockCipherMac_tF3EEE86C592D72AAB46685074C00F63508623048::get_offset_of_padding_3(),
	CbcBlockCipherMac_tF3EEE86C592D72AAB46685074C00F63508623048::get_offset_of_macSize_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5107 = { sizeof (HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5107[7] = 
{
	HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB::get_offset_of_digest_0(),
	HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB::get_offset_of_digestSize_1(),
	HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB::get_offset_of_blockLength_2(),
	HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB::get_offset_of_ipadState_3(),
	HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB::get_offset_of_opadState_4(),
	HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB::get_offset_of_inputPad_5(),
	HMac_t65D9921BAB421BEF7231F6BE59594F451DC4DFEB::get_offset_of_outputBuf_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5108 = { sizeof (Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5108[22] = 
{
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_cipher_0(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_singleByte_1(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_r0_2(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_r1_3(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_r2_4(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_r3_5(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_r4_6(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_s1_7(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_s2_8(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_s3_9(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_s4_10(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_k0_11(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_k1_12(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_k2_13(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_k3_14(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_currentBlock_15(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_currentBlockOffset_16(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_h0_17(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_h1_18(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_h2_19(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_h3_20(),
	Poly1305_tA7E692335B023AF877C210D99A953921FA39A98E::get_offset_of_h4_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5109 = { sizeof (CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5109[6] = 
{
	CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55::get_offset_of_IV_0(),
	CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55::get_offset_of_cbcV_1(),
	CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55::get_offset_of_cbcNextV_2(),
	CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55::get_offset_of_blockSize_3(),
	CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55::get_offset_of_cipher_4(),
	CbcBlockCipher_t72A1A9225DD378970489A5BEFE255D8B3DDD9E55::get_offset_of_encrypting_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5110 = { sizeof (CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19), -1, sizeof(CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5110[10] = 
{
	CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19_StaticFields::get_offset_of_BlockSize_0(),
	CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19::get_offset_of_cipher_1(),
	CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19::get_offset_of_macBlock_2(),
	CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19::get_offset_of_forEncryption_3(),
	CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19::get_offset_of_nonce_4(),
	CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19::get_offset_of_initialAssociatedText_5(),
	CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19::get_offset_of_macSize_6(),
	CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19::get_offset_of_keyParam_7(),
	CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19::get_offset_of_associatedText_8(),
	CcmBlockCipher_t42A7269184F52AB3243B16F74969015ACD54DA19::get_offset_of_data_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5111 = { sizeof (GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5111[21] = 
{
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_cipher_0(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_multiplier_1(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_exp_2(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_forEncryption_3(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_macSize_4(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_nonce_5(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_initialAssociatedText_6(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_H_7(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_J0_8(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_bufBlock_9(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_macBlock_10(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_S_11(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_S_at_12(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_S_atPre_13(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_counter_14(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_bufOff_15(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_totalLength_16(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_atBlock_17(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_atBlockPos_18(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_atLength_19(),
	GcmBlockCipher_t9822D875AB4C7C702AC8A87312925B346001DDB3::get_offset_of_atLengthPre_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5112 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5113 = { sizeof (SicBlockCipher_tDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5113[5] = 
{
	SicBlockCipher_tDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA::get_offset_of_cipher_0(),
	SicBlockCipher_tDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA::get_offset_of_blockSize_1(),
	SicBlockCipher_tDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA::get_offset_of_counter_2(),
	SicBlockCipher_tDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA::get_offset_of_counterOut_3(),
	SicBlockCipher_tDEF46B24E3D1DFDCCE89E6F9DDE8F3E5A0524FCA::get_offset_of_IV_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5114 = { sizeof (GcmUtilities_t0D3F57F498E8169CC96D3913B9B09C4814078772), -1, sizeof(GcmUtilities_t0D3F57F498E8169CC96D3913B9B09C4814078772_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5114[1] = 
{
	GcmUtilities_t0D3F57F498E8169CC96D3913B9B09C4814078772_StaticFields::get_offset_of_LOOKUP_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5115 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5116 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5117 = { sizeof (Tables1kGcmExponentiator_t5A198F1B1859DE1AEB295E643867A6BB4B87888B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5117[1] = 
{
	Tables1kGcmExponentiator_t5A198F1B1859DE1AEB295E643867A6BB4B87888B::get_offset_of_lookupPowX2_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5118 = { sizeof (Tables8kGcmMultiplier_tDE91C24BB1C4531FC17F3E25C1861733008E2670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5118[2] = 
{
	Tables8kGcmMultiplier_tDE91C24BB1C4531FC17F3E25C1861733008E2670::get_offset_of_H_0(),
	Tables8kGcmMultiplier_tDE91C24BB1C4531FC17F3E25C1861733008E2670::get_offset_of_M_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5119 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5120 = { sizeof (AeadParameters_t04E1AE26A3A54261A1893C79598D845FFE575212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5120[4] = 
{
	AeadParameters_t04E1AE26A3A54261A1893C79598D845FFE575212::get_offset_of_associatedText_0(),
	AeadParameters_t04E1AE26A3A54261A1893C79598D845FFE575212::get_offset_of_nonce_1(),
	AeadParameters_t04E1AE26A3A54261A1893C79598D845FFE575212::get_offset_of_key_2(),
	AeadParameters_t04E1AE26A3A54261A1893C79598D845FFE575212::get_offset_of_macSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5121 = { sizeof (DHKeyGenerationParameters_t7EFE3D05B481CE4EC6E8B4C9E5EF13E970B64078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5121[1] = 
{
	DHKeyGenerationParameters_t7EFE3D05B481CE4EC6E8B4C9E5EF13E970B64078::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5122 = { sizeof (DHKeyParameters_t74E59C4811FF2499B6F2F487B9B15DE6E426911C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5122[2] = 
{
	DHKeyParameters_t74E59C4811FF2499B6F2F487B9B15DE6E426911C::get_offset_of_parameters_1(),
	DHKeyParameters_t74E59C4811FF2499B6F2F487B9B15DE6E426911C::get_offset_of_algorithmOid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5123 = { sizeof (DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5123[7] = 
{
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513::get_offset_of_p_0(),
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513::get_offset_of_g_1(),
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513::get_offset_of_q_2(),
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513::get_offset_of_j_3(),
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513::get_offset_of_m_4(),
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513::get_offset_of_l_5(),
	DHParameters_t15DBCF5CD22F83E806A78FF80CE2853863825513::get_offset_of_validation_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5124 = { sizeof (DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5124[1] = 
{
	DHPrivateKeyParameters_t7A930900443BCA929045970B99A92EADD777049C::get_offset_of_x_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5125 = { sizeof (DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5125[1] = 
{
	DHPublicKeyParameters_tBAD07B04251C37FABE0AAD56DC981D0C4BBD68AA::get_offset_of_y_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5126 = { sizeof (DHValidationParameters_tC30183B3C1E3CAF234F646697CED1210A4EF4C0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5126[2] = 
{
	DHValidationParameters_tC30183B3C1E3CAF234F646697CED1210A4EF4C0E::get_offset_of_seed_0(),
	DHValidationParameters_tC30183B3C1E3CAF234F646697CED1210A4EF4C0E::get_offset_of_counter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5127 = { sizeof (DsaKeyParameters_tBF3E3B9B02243E5C768006D70F4CA496F15E82A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5127[1] = 
{
	DsaKeyParameters_tBF3E3B9B02243E5C768006D70F4CA496F15E82A6::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5128 = { sizeof (DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5128[4] = 
{
	DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807::get_offset_of_p_0(),
	DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807::get_offset_of_q_1(),
	DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807::get_offset_of_g_2(),
	DsaParameters_tD8EB8510ECE0F5F59B9C48B81FF7098DE0D9B807::get_offset_of_validation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5129 = { sizeof (DsaPrivateKeyParameters_t6CFD8006550A801F95DE67946DB700B7D04F6ACE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5129[1] = 
{
	DsaPrivateKeyParameters_t6CFD8006550A801F95DE67946DB700B7D04F6ACE::get_offset_of_x_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5130 = { sizeof (DsaPublicKeyParameters_tBC11DDF41C098CB0EA439AEEAEF375C716BC654D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5130[1] = 
{
	DsaPublicKeyParameters_tBC11DDF41C098CB0EA439AEEAEF375C716BC654D::get_offset_of_y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5131 = { sizeof (DsaValidationParameters_t2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5131[2] = 
{
	DsaValidationParameters_t2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76::get_offset_of_seed_0(),
	DsaValidationParameters_t2C2BC13FBC41AB735FA143D7D3478AFEB9A1AF76::get_offset_of_counter_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5132 = { sizeof (ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5132[5] = 
{
	ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57::get_offset_of_curve_0(),
	ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57::get_offset_of_seed_1(),
	ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57::get_offset_of_g_2(),
	ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57::get_offset_of_n_3(),
	ECDomainParameters_t0960619BC6EBC2C7841671A013C8B7910E71AB57::get_offset_of_h_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5133 = { sizeof (ECKeyGenerationParameters_tC5555EDF06F4273B4B637DA006AB968AC7FA7413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5133[2] = 
{
	ECKeyGenerationParameters_tC5555EDF06F4273B4B637DA006AB968AC7FA7413::get_offset_of_domainParams_2(),
	ECKeyGenerationParameters_tC5555EDF06F4273B4B637DA006AB968AC7FA7413::get_offset_of_publicKeyParamSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5134 = { sizeof (ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1), -1, sizeof(ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5134[4] = 
{
	ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1_StaticFields::get_offset_of_algorithms_1(),
	ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1::get_offset_of_algorithm_2(),
	ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1::get_offset_of_parameters_3(),
	ECKeyParameters_t0F4612DCE3141CDAC4A8D33189A966009AF3F1F1::get_offset_of_publicKeyParamSet_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5135 = { sizeof (ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5135[1] = 
{
	ECPrivateKeyParameters_tC1317EEDAEF16688C98CD41053BB95C6751BBD1F::get_offset_of_d_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5136 = { sizeof (ECPublicKeyParameters_t84FD118FD3407D284EF46E8358AE185634537702), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5136[1] = 
{
	ECPublicKeyParameters_t84FD118FD3407D284EF46E8358AE185634537702::get_offset_of_q_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5137 = { sizeof (ElGamalKeyParameters_tE099F61B83BEC92CD2DDFB27273FAA868EEE0DE2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5137[1] = 
{
	ElGamalKeyParameters_tE099F61B83BEC92CD2DDFB27273FAA868EEE0DE2::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5138 = { sizeof (ElGamalParameters_tC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5138[3] = 
{
	ElGamalParameters_tC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA::get_offset_of_p_0(),
	ElGamalParameters_tC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA::get_offset_of_g_1(),
	ElGamalParameters_tC654BCF89C3C73CAF3815C9F7A64C97D90C9D4DA::get_offset_of_l_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5139 = { sizeof (ElGamalPublicKeyParameters_t65E8DA5C9AF34FDA6E20854D55752CE99F8898FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5139[1] = 
{
	ElGamalPublicKeyParameters_t65E8DA5C9AF34FDA6E20854D55752CE99F8898FA::get_offset_of_y_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5140 = { sizeof (Gost3410KeyParameters_tAAD1AD5F82380026117A6E14A1C947AE92951C3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5140[2] = 
{
	Gost3410KeyParameters_tAAD1AD5F82380026117A6E14A1C947AE92951C3F::get_offset_of_parameters_1(),
	Gost3410KeyParameters_tAAD1AD5F82380026117A6E14A1C947AE92951C3F::get_offset_of_publicKeyParamSet_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5141 = { sizeof (Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5141[4] = 
{
	Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD::get_offset_of_p_0(),
	Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD::get_offset_of_q_1(),
	Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD::get_offset_of_a_2(),
	Gost3410Parameters_t7CA2550E4A0FE7DEDB3DCB2B864B9DF881993CDD::get_offset_of_validation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5142 = { sizeof (Gost3410PublicKeyParameters_t084516D277AAE667572C6641A42A212EEDE1E3C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5142[1] = 
{
	Gost3410PublicKeyParameters_t084516D277AAE667572C6641A42A212EEDE1E3C5::get_offset_of_y_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5143 = { sizeof (Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5143[4] = 
{
	Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98::get_offset_of_x0_0(),
	Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98::get_offset_of_c_1(),
	Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98::get_offset_of_x0L_2(),
	Gost3410ValidationParameters_tFE9E63B9D9DF4A8E281B2A30C3599055545C2B98::get_offset_of_cL_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5144 = { sizeof (KeyParameter_t969556DA4FAE52672608FEC8F8E2D4E4EC8337A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5144[1] = 
{
	KeyParameter_t969556DA4FAE52672608FEC8F8E2D4E4EC8337A2::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5145 = { sizeof (ParametersWithIV_tD591FD016AE64BD6570FEB2A4A98032A8F0F3572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5145[2] = 
{
	ParametersWithIV_tD591FD016AE64BD6570FEB2A4A98032A8F0F3572::get_offset_of_parameters_0(),
	ParametersWithIV_tD591FD016AE64BD6570FEB2A4A98032A8F0F3572::get_offset_of_iv_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5146 = { sizeof (ParametersWithRandom_t70E164400FC8254A14587493D29A4A10292DFFC5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5146[2] = 
{
	ParametersWithRandom_t70E164400FC8254A14587493D29A4A10292DFFC5::get_offset_of_parameters_0(),
	ParametersWithRandom_t70E164400FC8254A14587493D29A4A10292DFFC5::get_offset_of_random_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5147 = { sizeof (ParametersWithSBox_t9DF7CC8A76EBAD30DC1C35CD5E49B7F54A67AC66), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5147[2] = 
{
	ParametersWithSBox_t9DF7CC8A76EBAD30DC1C35CD5E49B7F54A67AC66::get_offset_of_parameters_0(),
	ParametersWithSBox_t9DF7CC8A76EBAD30DC1C35CD5E49B7F54A67AC66::get_offset_of_sBox_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5148 = { sizeof (RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5148[2] = 
{
	RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308::get_offset_of_modulus_1(),
	RsaKeyParameters_t57791EDEBAF0AD93C55C7FCF645D7CBB6284D308::get_offset_of_exponent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5149 = { sizeof (RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5149[6] = 
{
	RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040::get_offset_of_e_3(),
	RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040::get_offset_of_p_4(),
	RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040::get_offset_of_q_5(),
	RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040::get_offset_of_dP_6(),
	RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040::get_offset_of_dQ_7(),
	RsaPrivateCrtKeyParameters_tDF12AEB7CC6881585CF7D1985C2F3656FD2E1040::get_offset_of_qInv_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5150 = { sizeof (CryptoApiRandomGenerator_tF05E7E87DEA4D3ED648264F159B91B54021C26B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5150[1] = 
{
	CryptoApiRandomGenerator_tF05E7E87DEA4D3ED648264F159B91B54021C26B0::get_offset_of_rndProv_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5151 = { sizeof (DigestRandomGenerator_t145115EA116BD2747EB5B1860F1A47A604B1BF59), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5151[5] = 
{
	DigestRandomGenerator_t145115EA116BD2747EB5B1860F1A47A604B1BF59::get_offset_of_stateCounter_0(),
	DigestRandomGenerator_t145115EA116BD2747EB5B1860F1A47A604B1BF59::get_offset_of_seedCounter_1(),
	DigestRandomGenerator_t145115EA116BD2747EB5B1860F1A47A604B1BF59::get_offset_of_digest_2(),
	DigestRandomGenerator_t145115EA116BD2747EB5B1860F1A47A604B1BF59::get_offset_of_state_3(),
	DigestRandomGenerator_t145115EA116BD2747EB5B1860F1A47A604B1BF59::get_offset_of_seed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5152 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5153 = { sizeof (DsaDigestSigner_tF9384890F2D126B4FC63626F0CD56496730BACCE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5153[3] = 
{
	DsaDigestSigner_tF9384890F2D126B4FC63626F0CD56496730BACCE::get_offset_of_digest_0(),
	DsaDigestSigner_tF9384890F2D126B4FC63626F0CD56496730BACCE::get_offset_of_dsaSigner_1(),
	DsaDigestSigner_tF9384890F2D126B4FC63626F0CD56496730BACCE::get_offset_of_forSigning_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5154 = { sizeof (DsaSigner_t1B94B42B08AC7EB937B05FC6DCF361A1BA5BFB66), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5154[3] = 
{
	DsaSigner_t1B94B42B08AC7EB937B05FC6DCF361A1BA5BFB66::get_offset_of_kCalculator_0(),
	DsaSigner_t1B94B42B08AC7EB937B05FC6DCF361A1BA5BFB66::get_offset_of_key_1(),
	DsaSigner_t1B94B42B08AC7EB937B05FC6DCF361A1BA5BFB66::get_offset_of_random_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5155 = { sizeof (ECDsaSigner_t277BCD15E0EC9F82F5399911E2E7BD61D2951AC8), -1, sizeof(ECDsaSigner_t277BCD15E0EC9F82F5399911E2E7BD61D2951AC8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5155[4] = 
{
	ECDsaSigner_t277BCD15E0EC9F82F5399911E2E7BD61D2951AC8_StaticFields::get_offset_of_Eight_0(),
	ECDsaSigner_t277BCD15E0EC9F82F5399911E2E7BD61D2951AC8::get_offset_of_kCalculator_1(),
	ECDsaSigner_t277BCD15E0EC9F82F5399911E2E7BD61D2951AC8::get_offset_of_key_2(),
	ECDsaSigner_t277BCD15E0EC9F82F5399911E2E7BD61D2951AC8::get_offset_of_random_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5156 = { sizeof (GenericSigner_t1303EADD2989EE6C3E682E0CE5D583104B524406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5156[3] = 
{
	GenericSigner_t1303EADD2989EE6C3E682E0CE5D583104B524406::get_offset_of_engine_0(),
	GenericSigner_t1303EADD2989EE6C3E682E0CE5D583104B524406::get_offset_of_digest_1(),
	GenericSigner_t1303EADD2989EE6C3E682E0CE5D583104B524406::get_offset_of_forSigning_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5157 = { sizeof (HMacDsaKCalculator_t13E93BFD5A7F7018A41BDB29E900A2D9CC76A684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5157[3] = 
{
	HMacDsaKCalculator_t13E93BFD5A7F7018A41BDB29E900A2D9CC76A684::get_offset_of_hMac_0(),
	HMacDsaKCalculator_t13E93BFD5A7F7018A41BDB29E900A2D9CC76A684::get_offset_of_K_1(),
	HMacDsaKCalculator_t13E93BFD5A7F7018A41BDB29E900A2D9CC76A684::get_offset_of_V_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5158 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5159 = { sizeof (RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9), -1, sizeof(RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5159[5] = 
{
	RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9::get_offset_of_rsaEngine_0(),
	RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9::get_offset_of_algId_1(),
	RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9::get_offset_of_digest_2(),
	RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9::get_offset_of_forSigning_3(),
	RsaDigestSigner_t41FCA79CA5D0AF546AFA029B417F135C311F75F9_StaticFields::get_offset_of_oidMap_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5160 = { sizeof (AbstractTlsCipherFactory_t95A664781E8103D14B4E9186E35D2826C17C47FB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5161 = { sizeof (AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5161[8] = 
{
	AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7::get_offset_of_mCipherFactory_0(),
	AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7::get_offset_of_mContext_1(),
	AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7::get_offset_of_mSupportedSignatureAlgorithms_2(),
	AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7::get_offset_of_mNamedCurves_3(),
	AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7::get_offset_of_mClientECPointFormats_4(),
	AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7::get_offset_of_mServerECPointFormats_5(),
	AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7::get_offset_of_mSelectedCipherSuite_6(),
	AbstractTlsClient_tC6DAEE1B4537D4C522014C4971FC936BD5C964F7::get_offset_of_mSelectedCompressionMethod_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5162 = { sizeof (AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418), -1, sizeof(AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5162[7] = 
{
	AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418_StaticFields::get_offset_of_counter_0(),
	AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418::get_offset_of_mNonceRandom_1(),
	AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418::get_offset_of_mSecureRandom_2(),
	AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418::get_offset_of_mSecurityParameters_3(),
	AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418::get_offset_of_mClientVersion_4(),
	AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418::get_offset_of_mServerVersion_5(),
	AbstractTlsContext_t6443BAD6549F9D29772C07D635DD73AB72E2D418::get_offset_of_mSession_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5163 = { sizeof (AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5163[3] = 
{
	AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35::get_offset_of_mKeyExchange_0(),
	AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35::get_offset_of_mSupportedSignatureAlgorithms_1(),
	AbstractTlsKeyExchange_t1FBA186A24F2FB58B8F0627EB5547ACD1DEADE35::get_offset_of_mContext_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5164 = { sizeof (AbstractTlsPeer_t26CA3EF032C693BEB7331E2A0972A8620596CC9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5165 = { sizeof (AbstractTlsSigner_t69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5165[1] = 
{
	AbstractTlsSigner_t69E5DAAA91C2B1D2D56A27E2D3683A7347807CEE::get_offset_of_mContext_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5166 = { sizeof (AlertDescription_tA8DB3E28E891DC1EAD532B6131B48917E4C3B2D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5167 = { sizeof (ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5167[3] = 
{
	ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26::get_offset_of_databuf_0(),
	ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26::get_offset_of_skipped_1(),
	ByteQueue_tBA06263CBE5934691B2E38C8E4B5523329D92B26::get_offset_of_available_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5168 = { sizeof (Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D), -1, sizeof(Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5168[2] = 
{
	Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D_StaticFields::get_offset_of_EmptyChain_0(),
	Certificate_t609B55043542EA8131A286ABB40D1471185F0E8D::get_offset_of_mCertificateList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5169 = { sizeof (CertificateRequest_t2D3D094933C7C24EA9ED681B96C590E0488E0105), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5169[3] = 
{
	CertificateRequest_t2D3D094933C7C24EA9ED681B96C590E0488E0105::get_offset_of_mCertificateTypes_0(),
	CertificateRequest_t2D3D094933C7C24EA9ED681B96C590E0488E0105::get_offset_of_mSupportedSignatureAlgorithms_1(),
	CertificateRequest_t2D3D094933C7C24EA9ED681B96C590E0488E0105::get_offset_of_mCertificateAuthorities_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5170 = { sizeof (CertificateStatus_t1F391A4805FD1E6E500F7F5AD46A312FF7057AEA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5170[2] = 
{
	CertificateStatus_t1F391A4805FD1E6E500F7F5AD46A312FF7057AEA::get_offset_of_mStatusType_0(),
	CertificateStatus_t1F391A4805FD1E6E500F7F5AD46A312FF7057AEA::get_offset_of_mResponse_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5171 = { sizeof (Chacha20Poly1305_t3B7CD0A35BE52CEE0E3D54B5792D9A6305E77FF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5171[3] = 
{
	Chacha20Poly1305_t3B7CD0A35BE52CEE0E3D54B5792D9A6305E77FF4::get_offset_of_context_0(),
	Chacha20Poly1305_t3B7CD0A35BE52CEE0E3D54B5792D9A6305E77FF4::get_offset_of_encryptCipher_1(),
	Chacha20Poly1305_t3B7CD0A35BE52CEE0E3D54B5792D9A6305E77FF4::get_offset_of_decryptCipher_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5172 = { sizeof (CipherSuite_t002556B7A727BFF16214821A1AB5E42FFD19447E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5173 = { sizeof (CombinedHash_tA06AFB6B3C4AD80E8570D303A27982F3860D6EDA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5173[3] = 
{
	CombinedHash_tA06AFB6B3C4AD80E8570D303A27982F3860D6EDA::get_offset_of_mContext_0(),
	CombinedHash_tA06AFB6B3C4AD80E8570D303A27982F3860D6EDA::get_offset_of_mMd5_1(),
	CombinedHash_tA06AFB6B3C4AD80E8570D303A27982F3860D6EDA::get_offset_of_mSha1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5174 = { sizeof (DefaultTlsCipherFactory_tFCE3DDE22DA9FF8EB9AF1FF07ECF326DD97F863F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5175 = { sizeof (DefaultTlsClient_t3AEEC6138E8000BF0EB194E242413CB61B9EFF0E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5176 = { sizeof (DeferredHash_t9ADE64D47806150A3D10D105378AE6C23E6B70BF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5176[4] = 
{
	DeferredHash_t9ADE64D47806150A3D10D105378AE6C23E6B70BF::get_offset_of_mContext_0(),
	DeferredHash_t9ADE64D47806150A3D10D105378AE6C23E6B70BF::get_offset_of_mBuf_1(),
	DeferredHash_t9ADE64D47806150A3D10D105378AE6C23E6B70BF::get_offset_of_mHashes_2(),
	DeferredHash_t9ADE64D47806150A3D10D105378AE6C23E6B70BF::get_offset_of_mPrfHashAlgorithm_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5177 = { sizeof (DigestInputBuffer_t117CEE4FCA532842726AF5BFE55FFDFC708B811D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5178 = { sizeof (DigStream_tD4CE38226FC22D1A410F23711CBC52A3590147F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5178[1] = 
{
	DigStream_tD4CE38226FC22D1A410F23711CBC52A3590147F3::get_offset_of_d_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5179 = { sizeof (DigitallySigned_t9DFC1BA43FD4F9AF87418E4E7EF5149EA31ED8C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5179[2] = 
{
	DigitallySigned_t9DFC1BA43FD4F9AF87418E4E7EF5149EA31ED8C7::get_offset_of_mAlgorithm_0(),
	DigitallySigned_t9DFC1BA43FD4F9AF87418E4E7EF5149EA31ED8C7::get_offset_of_mSignature_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5180 = { sizeof (ECBasisType_tF8EDD91568D009AA5CFB6DA005F76AB2B90DB5BB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5181 = { sizeof (ExporterLabel_t64DE4AE70A2960BAF4CF5081306E3B2AB94F13A9), -1, sizeof(ExporterLabel_t64DE4AE70A2960BAF4CF5081306E3B2AB94F13A9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5181[1] = 
{
	ExporterLabel_t64DE4AE70A2960BAF4CF5081306E3B2AB94F13A9_StaticFields::get_offset_of_extended_master_secret_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5182 = { sizeof (HashAlgorithm_tEE6B968B3B1FD999C0F7BCDF55FD8ADD48DC52B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5183 = { sizeof (MaxFragmentLength_t0C5987E0DE30D0E3C97DA86A6A5BDF4DB5AEA55D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5184 = { sizeof (NamedCurve_tC950AF5C8967482E9CB78149686A9B394CB82AE6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5185 = { sizeof (NewSessionTicket_t02775DC80796C90B1E39930FDE0CBADA5B34160D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5185[2] = 
{
	NewSessionTicket_t02775DC80796C90B1E39930FDE0CBADA5B34160D::get_offset_of_mTicketLifetimeHint_0(),
	NewSessionTicket_t02775DC80796C90B1E39930FDE0CBADA5B34160D::get_offset_of_mTicket_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5186 = { sizeof (ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D), -1, sizeof(ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5186[8] = 
{
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields::get_offset_of_SSLv3_0(),
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields::get_offset_of_TLSv10_1(),
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields::get_offset_of_TLSv11_2(),
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields::get_offset_of_TLSv12_3(),
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields::get_offset_of_DTLSv10_4(),
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D_StaticFields::get_offset_of_DTLSv12_5(),
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D::get_offset_of_version_6(),
	ProtocolVersion_t8D17D19D186E01F44206B2B93FCF7454B146D09D::get_offset_of_name_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5187 = { sizeof (RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5187[19] = 
{
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mHandler_0(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mInput_1(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mOutput_2(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mPendingCompression_3(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mReadCompression_4(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mWriteCompression_5(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mPendingCipher_6(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mReadCipher_7(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mWriteCipher_8(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mReadSeqNo_9(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mWriteSeqNo_10(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mBuffer_11(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mHandshakeHash_12(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mReadVersion_13(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mWriteVersion_14(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mRestrictReadVersion_15(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mPlaintextLimit_16(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mCompressedLimit_17(),
	RecordStream_t7308B15B7B671878E7160CCB6F28DDFAB4277A19::get_offset_of_mCiphertextLimit_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5188 = { sizeof (SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5188[15] = 
{
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_entity_0(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_cipherSuite_1(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_compressionAlgorithm_2(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_prfAlgorithm_3(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_verifyDataLength_4(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_masterSecret_5(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_clientRandom_6(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_serverRandom_7(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_sessionHash_8(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_pskIdentity_9(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_srpIdentity_10(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_maxFragmentLength_11(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_truncatedHMac_12(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_encryptThenMac_13(),
	SecurityParameters_tF44CC3B939DF8F2A7BA70B4CD9166F51399031DB::get_offset_of_extendedMasterSecret_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5189 = { sizeof (ServerDHParams_tF568540CC3D6BAD9A1501D67B79FC55CF814CEAF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5189[1] = 
{
	ServerDHParams_tF568540CC3D6BAD9A1501D67B79FC55CF814CEAF::get_offset_of_mPublicKey_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5190 = { sizeof (ServerName_t98E795D8EE736F9B4D70AA3A584C31CF972FFDFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5190[2] = 
{
	ServerName_t98E795D8EE736F9B4D70AA3A584C31CF972FFDFB::get_offset_of_mNameType_0(),
	ServerName_t98E795D8EE736F9B4D70AA3A584C31CF972FFDFB::get_offset_of_mName_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5191 = { sizeof (ServerNameList_t9939760650D7D18A9DAEA1A36EBB286EB8686E9F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5191[1] = 
{
	ServerNameList_t9939760650D7D18A9DAEA1A36EBB286EB8686E9F::get_offset_of_mServerNameList_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5192 = { sizeof (SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5192[7] = 
{
	SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115::get_offset_of_mCipherSuite_0(),
	SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115::get_offset_of_mCompressionAlgorithm_1(),
	SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115::get_offset_of_mMasterSecret_2(),
	SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115::get_offset_of_mPeerCertificate_3(),
	SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115::get_offset_of_mPskIdentity_4(),
	SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115::get_offset_of_mSrpIdentity_5(),
	SessionParameters_t024B59544F7F2D0E9C16B3AEBC04EE4C9B5EC115::get_offset_of_mEncodedServerExtensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5193 = { sizeof (Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5193[7] = 
{
	Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2::get_offset_of_mCipherSuite_0(),
	Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2::get_offset_of_mCompressionAlgorithm_1(),
	Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2::get_offset_of_mMasterSecret_2(),
	Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2::get_offset_of_mPeerCertificate_3(),
	Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2::get_offset_of_mPskIdentity_4(),
	Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2::get_offset_of_mSrpIdentity_5(),
	Builder_t373D3EDDC040BC8647638E872A52BA6B4ADA8FB2::get_offset_of_mEncodedServerExtensions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5194 = { sizeof (SignatureAndHashAlgorithm_tDEA4E53D4DC207FA196758B3B4848104689854A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5194[2] = 
{
	SignatureAndHashAlgorithm_tDEA4E53D4DC207FA196758B3B4848104689854A9::get_offset_of_mHash_0(),
	SignatureAndHashAlgorithm_tDEA4E53D4DC207FA196758B3B4848104689854A9::get_offset_of_mSignature_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5195 = { sizeof (SignerInputBuffer_tADF941FEEC74D73D7EA670CDF8961A0229C0AFFB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5196 = { sizeof (SigStream_t25F33FC2964511F82619E544B1D0504CA4566513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5196[1] = 
{
	SigStream_t25F33FC2964511F82619E544B1D0504CA4566513::get_offset_of_s_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5197 = { sizeof (Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08), -1, sizeof(Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5197[5] = 
{
	Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08_StaticFields::get_offset_of_IPAD_0(),
	Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08_StaticFields::get_offset_of_OPAD_1(),
	Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08::get_offset_of_digest_2(),
	Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08::get_offset_of_padLength_3(),
	Ssl3Mac_t0FA863B419D2DCE41B15503AFFF8C0C4E450DF08::get_offset_of_secret_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5198 = { sizeof (SupplementalDataEntry_t7431C336D03C472BA9D64CEBA9BDBFACFCBDD69D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5198[2] = 
{
	SupplementalDataEntry_t7431C336D03C472BA9D64CEBA9BDBFACFCBDD69D::get_offset_of_mDataType_0(),
	SupplementalDataEntry_t7431C336D03C472BA9D64CEBA9BDBFACFCBDD69D::get_offset_of_mData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5199 = { sizeof (TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5199[7] = 
{
	TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553::get_offset_of_context_0(),
	TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553::get_offset_of_macSize_1(),
	TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553::get_offset_of_nonce_explicit_length_2(),
	TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553::get_offset_of_encryptCipher_3(),
	TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553::get_offset_of_decryptCipher_4(),
	TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553::get_offset_of_encryptImplicitNonce_5(),
	TlsAeadCipher_t14DA167F81DE9D328329AA5AB8198F8B8AD33553::get_offset_of_decryptImplicitNonce_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
