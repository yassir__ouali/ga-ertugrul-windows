﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// CodeStage.AntiCheat.Detectors.InjectionDetector
struct InjectionDetector_tB402442AA73D35344CEF9C14C806583C37BD610D;
// CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector
struct ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601;
// CodeStage.AntiCheat.Detectors.SpeedHackDetector
struct SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE;
// CodeStage.AntiCheat.Detectors.TimeCheatingDetector
struct TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65;
// CodeStage.AntiCheat.Detectors.TimeCheatingDetector/OnlineTimeCallback
struct OnlineTimeCallback_t5AA2B75F108EB462BC1D2C2E38CF960AE3434D34;
// CodeStage.AntiCheat.Detectors.TimeCheatingDetector/TimeCheatingDetectorEventHandler
struct TimeCheatingDetectorEventHandler_t23FE998F4C6E3C8DF608BE057ACD82476B766D29;
// CodeStage.AntiCheat.Detectors.WallHackDetector
struct WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590;
// CodeStage.AntiCheat.Genuine.CodeHash.BaseWorker
struct BaseWorker_tD3B5F0035F406307CFD71F8BCF644A3D81A1036E;
// CodeStage.AntiCheat.Genuine.CodeHash.CodeHashGenerator
struct CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480;
// CodeStage.AntiCheat.Genuine.CodeHash.HashGeneratorResult
struct HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7;
// CodeStage.AntiCheat.Genuine.CodeHash.HashGeneratorResultHandler
struct HashGeneratorResultHandler_tD7EA83E3F9BE89BA5F6FC201F7CF915EE9B31A2E;
// DG.Tweening.Core.DOTweenComponent
struct DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F;
// DG.Tweening.EaseFunction
struct EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691;
// DG.Tweening.Plugins.Core.ITweenPlugin
struct ITweenPlugin_t9F152F90189B4323C827E01980F33F7676AA4CCF;
// DG.Tweening.Sequence
struct Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2;
// DG.Tweening.TweenCallback
struct TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t46CB97F053453C4F41F8AE6213792813E455DDE1;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<CodeStage.AntiCheat.Detectors.TimeCheatingDetector/ErrorKind>
struct Action_1_tF98CA887A7F4745726B4B140A2BEBE05A25F2554;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>
struct List_1_t2E181C700331B7635DE1FD6A98300CEAEFABC4ED;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t57BB69F1AC3759152D9E750F6120000328D367B8;
// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct List_1_tA91E07DF8FDCABF79352C03D79A9D29DE3BBA212;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t028AAE01C4834286B7892F4498364F964CD8B316;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tEFDFBE18E061A6065AB2FF735F1425FB59F919BC;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Threading.Tasks.Task
struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2;
// System.Threading.Tasks.Task`1<CodeStage.AntiCheat.Detectors.TimeCheatingDetector/CheckResult>
struct Task_1_t8134682F4345785A873A6E69078B0B53B9DB89B8;
// System.Threading.Tasks.Task`1<CodeStage.AntiCheat.Detectors.TimeCheatingDetector/OnlineTimeResult>
struct Task_1_t957C3A34CCCE85454E68B7321A390F0720FD14B2;
// System.Type
struct Type_t;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.CharacterController
struct CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MeshRenderer
struct MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t726E134F16701A2671D40BEBE22110DC57156353;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57;
// UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A;
// UnityEngine.RenderTexture
struct RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8;

struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T18E01DC18978C39365422BE25552B43FDE647BA1_H
#define U3CMODULEU3E_T18E01DC18978C39365422BE25552B43FDE647BA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t18E01DC18978C39365422BE25552B43FDE647BA1 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T18E01DC18978C39365422BE25552B43FDE647BA1_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ACTKCONSTANTS_TA3115295C8C5DCBFA0664657820B2216E2B51DAB_H
#define ACTKCONSTANTS_TA3115295C8C5DCBFA0664657820B2216E2B51DAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ACTkConstants
struct  ACTkConstants_tA3115295C8C5DCBFA0664657820B2216E2B51DAB  : public RuntimeObject
{
public:

public:
};

struct ACTkConstants_tA3115295C8C5DCBFA0664657820B2216E2B51DAB_StaticFields
{
public:
	// System.Char[] CodeStage.AntiCheat.Common.ACTkConstants::StringKey
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___StringKey_3;

public:
	inline static int32_t get_offset_of_StringKey_3() { return static_cast<int32_t>(offsetof(ACTkConstants_tA3115295C8C5DCBFA0664657820B2216E2B51DAB_StaticFields, ___StringKey_3)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_StringKey_3() const { return ___StringKey_3; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_StringKey_3() { return &___StringKey_3; }
	inline void set_StringKey_3(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___StringKey_3 = value;
		Il2CppCodeGenWriteBarrier((&___StringKey_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKCONSTANTS_TA3115295C8C5DCBFA0664657820B2216E2B51DAB_H
#ifndef CONTAINERHOLDER_T7F0EC51F32F5808C854B91DCC0CFBADEC4D2A1A9_H
#define CONTAINERHOLDER_T7F0EC51F32F5808C854B91DCC0CFBADEC4D2A1A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ContainerHolder
struct  ContainerHolder_t7F0EC51F32F5808C854B91DCC0CFBADEC4D2A1A9  : public RuntimeObject
{
public:

public:
};

struct ContainerHolder_t7F0EC51F32F5808C854B91DCC0CFBADEC4D2A1A9_StaticFields
{
public:
	// UnityEngine.GameObject CodeStage.AntiCheat.Common.ContainerHolder::container
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___container_1;

public:
	inline static int32_t get_offset_of_container_1() { return static_cast<int32_t>(offsetof(ContainerHolder_t7F0EC51F32F5808C854B91DCC0CFBADEC4D2A1A9_StaticFields, ___container_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_container_1() const { return ___container_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_container_1() { return &___container_1; }
	inline void set_container_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___container_1 = value;
		Il2CppCodeGenWriteBarrier((&___container_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTAINERHOLDER_T7F0EC51F32F5808C854B91DCC0CFBADEC4D2A1A9_H
#ifndef U3CCHECKFORCHEATU3ED__67_T8744D10801D7D4974DA5FB3A21581569AFA87226_H
#define U3CCHECKFORCHEATU3ED__67_T8744D10801D7D4974DA5FB3A21581569AFA87226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<CheckForCheat>d__67
struct  U3CCheckForCheatU3Ed__67_t8744D10801D7D4974DA5FB3A21581569AFA87226  : public RuntimeObject
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<CheckForCheat>d__67::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<CheckForCheat>d__67::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<CheckForCheat>d__67::<>4__this
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCheckForCheatU3Ed__67_t8744D10801D7D4974DA5FB3A21581569AFA87226, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCheckForCheatU3Ed__67_t8744D10801D7D4974DA5FB3A21581569AFA87226, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCheckForCheatU3Ed__67_t8744D10801D7D4974DA5FB3A21581569AFA87226, ___U3CU3E4__this_2)); }
	inline TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKFORCHEATU3ED__67_T8744D10801D7D4974DA5FB3A21581569AFA87226_H
#ifndef U3CFORCECHECKENUMERATORU3ED__58_TB83CC76710450C730B37A01FDEE5E33C17EA64F3_H
#define U3CFORCECHECKENUMERATORU3ED__58_TB83CC76710450C730B37A01FDEE5E33C17EA64F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<ForceCheckEnumerator>d__58
struct  U3CForceCheckEnumeratorU3Ed__58_tB83CC76710450C730B37A01FDEE5E33C17EA64F3  : public RuntimeObject
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<ForceCheckEnumerator>d__58::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<ForceCheckEnumerator>d__58::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<ForceCheckEnumerator>d__58::<>4__this
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CForceCheckEnumeratorU3Ed__58_tB83CC76710450C730B37A01FDEE5E33C17EA64F3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CForceCheckEnumeratorU3Ed__58_tB83CC76710450C730B37A01FDEE5E33C17EA64F3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CForceCheckEnumeratorU3Ed__58_tB83CC76710450C730B37A01FDEE5E33C17EA64F3, ___U3CU3E4__this_2)); }
	inline TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFORCECHECKENUMERATORU3ED__58_TB83CC76710450C730B37A01FDEE5E33C17EA64F3_H
#ifndef U3CCAPTUREFRAMEU3ED__76_T8E49CCA052F5C0D472F1BCD76138069CDBA77C8B_H
#define U3CCAPTUREFRAMEU3ED__76_T8E49CCA052F5C0D472F1BCD76138069CDBA77C8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.WallHackDetector_<CaptureFrame>d__76
struct  U3CCaptureFrameU3Ed__76_t8E49CCA052F5C0D472F1BCD76138069CDBA77C8B  : public RuntimeObject
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.WallHackDetector_<CaptureFrame>d__76::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CodeStage.AntiCheat.Detectors.WallHackDetector_<CaptureFrame>d__76::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CodeStage.AntiCheat.Detectors.WallHackDetector CodeStage.AntiCheat.Detectors.WallHackDetector_<CaptureFrame>d__76::<>4__this
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590 * ___U3CU3E4__this_2;
	// UnityEngine.RenderTexture CodeStage.AntiCheat.Detectors.WallHackDetector_<CaptureFrame>d__76::<previousActive>5__2
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___U3CpreviousActiveU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCaptureFrameU3Ed__76_t8E49CCA052F5C0D472F1BCD76138069CDBA77C8B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCaptureFrameU3Ed__76_t8E49CCA052F5C0D472F1BCD76138069CDBA77C8B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCaptureFrameU3Ed__76_t8E49CCA052F5C0D472F1BCD76138069CDBA77C8B, ___U3CU3E4__this_2)); }
	inline WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CpreviousActiveU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CCaptureFrameU3Ed__76_t8E49CCA052F5C0D472F1BCD76138069CDBA77C8B, ___U3CpreviousActiveU3E5__2_3)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_U3CpreviousActiveU3E5__2_3() const { return ___U3CpreviousActiveU3E5__2_3; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_U3CpreviousActiveU3E5__2_3() { return &___U3CpreviousActiveU3E5__2_3; }
	inline void set_U3CpreviousActiveU3E5__2_3(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___U3CpreviousActiveU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpreviousActiveU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCAPTUREFRAMEU3ED__76_T8E49CCA052F5C0D472F1BCD76138069CDBA77C8B_H
#ifndef U3CINITDETECTORU3ED__71_T1A2D78DA7BC7B2A2FA239173CD78A23A4A1829EC_H
#define U3CINITDETECTORU3ED__71_T1A2D78DA7BC7B2A2FA239173CD78A23A4A1829EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.WallHackDetector_<InitDetector>d__71
struct  U3CInitDetectorU3Ed__71_t1A2D78DA7BC7B2A2FA239173CD78A23A4A1829EC  : public RuntimeObject
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.WallHackDetector_<InitDetector>d__71::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CodeStage.AntiCheat.Detectors.WallHackDetector_<InitDetector>d__71::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CodeStage.AntiCheat.Detectors.WallHackDetector CodeStage.AntiCheat.Detectors.WallHackDetector_<InitDetector>d__71::<>4__this
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInitDetectorU3Ed__71_t1A2D78DA7BC7B2A2FA239173CD78A23A4A1829EC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInitDetectorU3Ed__71_t1A2D78DA7BC7B2A2FA239173CD78A23A4A1829EC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInitDetectorU3Ed__71_t1A2D78DA7BC7B2A2FA239173CD78A23A4A1829EC, ___U3CU3E4__this_2)); }
	inline WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITDETECTORU3ED__71_T1A2D78DA7BC7B2A2FA239173CD78A23A4A1829EC_H
#ifndef BASEWORKER_TD3B5F0035F406307CFD71F8BCF644A3D81A1036E_H
#define BASEWORKER_TD3B5F0035F406307CFD71F8BCF644A3D81A1036E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Genuine.CodeHash.BaseWorker
struct  BaseWorker_tD3B5F0035F406307CFD71F8BCF644A3D81A1036E  : public RuntimeObject
{
public:
	// CodeStage.AntiCheat.Genuine.CodeHash.HashGeneratorResult CodeStage.AntiCheat.Genuine.CodeHash.BaseWorker::<Result>k__BackingField
	HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7 * ___U3CResultU3Ek__BackingField_0;
	// System.Boolean CodeStage.AntiCheat.Genuine.CodeHash.BaseWorker::<IsBusy>k__BackingField
	bool ___U3CIsBusyU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CResultU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BaseWorker_tD3B5F0035F406307CFD71F8BCF644A3D81A1036E, ___U3CResultU3Ek__BackingField_0)); }
	inline HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7 * get_U3CResultU3Ek__BackingField_0() const { return ___U3CResultU3Ek__BackingField_0; }
	inline HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7 ** get_address_of_U3CResultU3Ek__BackingField_0() { return &___U3CResultU3Ek__BackingField_0; }
	inline void set_U3CResultU3Ek__BackingField_0(HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7 * value)
	{
		___U3CResultU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIsBusyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BaseWorker_tD3B5F0035F406307CFD71F8BCF644A3D81A1036E, ___U3CIsBusyU3Ek__BackingField_1)); }
	inline bool get_U3CIsBusyU3Ek__BackingField_1() const { return ___U3CIsBusyU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsBusyU3Ek__BackingField_1() { return &___U3CIsBusyU3Ek__BackingField_1; }
	inline void set_U3CIsBusyU3Ek__BackingField_1(bool value)
	{
		___U3CIsBusyU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEWORKER_TD3B5F0035F406307CFD71F8BCF644A3D81A1036E_H
#ifndef U3CCALCULATIONAWAITERU3ED__20_T1976E62BC5F5EF7115BE28363866365316D26084_H
#define U3CCALCULATIONAWAITERU3ED__20_T1976E62BC5F5EF7115BE28363866365316D26084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Genuine.CodeHash.CodeHashGenerator_<CalculationAwaiter>d__20
struct  U3CCalculationAwaiterU3Ed__20_t1976E62BC5F5EF7115BE28363866365316D26084  : public RuntimeObject
{
public:
	// System.Int32 CodeStage.AntiCheat.Genuine.CodeHash.CodeHashGenerator_<CalculationAwaiter>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CodeStage.AntiCheat.Genuine.CodeHash.CodeHashGenerator_<CalculationAwaiter>d__20::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// CodeStage.AntiCheat.Genuine.CodeHash.CodeHashGenerator CodeStage.AntiCheat.Genuine.CodeHash.CodeHashGenerator_<CalculationAwaiter>d__20::<>4__this
	CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCalculationAwaiterU3Ed__20_t1976E62BC5F5EF7115BE28363866365316D26084, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCalculationAwaiterU3Ed__20_t1976E62BC5F5EF7115BE28363866365316D26084, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCalculationAwaiterU3Ed__20_t1976E62BC5F5EF7115BE28363866365316D26084, ___U3CU3E4__this_2)); }
	inline CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCALCULATIONAWAITERU3ED__20_T1976E62BC5F5EF7115BE28363866365316D26084_H
#ifndef FILEFILTER_T73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28_H
#define FILEFILTER_T73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Genuine.CodeHash.FileFilter
struct  FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28  : public RuntimeObject
{
public:
	// System.Boolean CodeStage.AntiCheat.Genuine.CodeHash.FileFilter::caseSensitive
	bool ___caseSensitive_0;
	// System.Boolean CodeStage.AntiCheat.Genuine.CodeHash.FileFilter::folderRecursive
	bool ___folderRecursive_1;
	// System.Boolean CodeStage.AntiCheat.Genuine.CodeHash.FileFilter::exactFileNameMatch
	bool ___exactFileNameMatch_2;
	// System.Boolean CodeStage.AntiCheat.Genuine.CodeHash.FileFilter::exactFolderMatch
	bool ___exactFolderMatch_3;
	// System.String CodeStage.AntiCheat.Genuine.CodeHash.FileFilter::filterFolder
	String_t* ___filterFolder_4;
	// System.String CodeStage.AntiCheat.Genuine.CodeHash.FileFilter::filterExtension
	String_t* ___filterExtension_5;
	// System.String CodeStage.AntiCheat.Genuine.CodeHash.FileFilter::filterFileName
	String_t* ___filterFileName_6;

public:
	inline static int32_t get_offset_of_caseSensitive_0() { return static_cast<int32_t>(offsetof(FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28, ___caseSensitive_0)); }
	inline bool get_caseSensitive_0() const { return ___caseSensitive_0; }
	inline bool* get_address_of_caseSensitive_0() { return &___caseSensitive_0; }
	inline void set_caseSensitive_0(bool value)
	{
		___caseSensitive_0 = value;
	}

	inline static int32_t get_offset_of_folderRecursive_1() { return static_cast<int32_t>(offsetof(FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28, ___folderRecursive_1)); }
	inline bool get_folderRecursive_1() const { return ___folderRecursive_1; }
	inline bool* get_address_of_folderRecursive_1() { return &___folderRecursive_1; }
	inline void set_folderRecursive_1(bool value)
	{
		___folderRecursive_1 = value;
	}

	inline static int32_t get_offset_of_exactFileNameMatch_2() { return static_cast<int32_t>(offsetof(FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28, ___exactFileNameMatch_2)); }
	inline bool get_exactFileNameMatch_2() const { return ___exactFileNameMatch_2; }
	inline bool* get_address_of_exactFileNameMatch_2() { return &___exactFileNameMatch_2; }
	inline void set_exactFileNameMatch_2(bool value)
	{
		___exactFileNameMatch_2 = value;
	}

	inline static int32_t get_offset_of_exactFolderMatch_3() { return static_cast<int32_t>(offsetof(FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28, ___exactFolderMatch_3)); }
	inline bool get_exactFolderMatch_3() const { return ___exactFolderMatch_3; }
	inline bool* get_address_of_exactFolderMatch_3() { return &___exactFolderMatch_3; }
	inline void set_exactFolderMatch_3(bool value)
	{
		___exactFolderMatch_3 = value;
	}

	inline static int32_t get_offset_of_filterFolder_4() { return static_cast<int32_t>(offsetof(FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28, ___filterFolder_4)); }
	inline String_t* get_filterFolder_4() const { return ___filterFolder_4; }
	inline String_t** get_address_of_filterFolder_4() { return &___filterFolder_4; }
	inline void set_filterFolder_4(String_t* value)
	{
		___filterFolder_4 = value;
		Il2CppCodeGenWriteBarrier((&___filterFolder_4), value);
	}

	inline static int32_t get_offset_of_filterExtension_5() { return static_cast<int32_t>(offsetof(FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28, ___filterExtension_5)); }
	inline String_t* get_filterExtension_5() const { return ___filterExtension_5; }
	inline String_t** get_address_of_filterExtension_5() { return &___filterExtension_5; }
	inline void set_filterExtension_5(String_t* value)
	{
		___filterExtension_5 = value;
		Il2CppCodeGenWriteBarrier((&___filterExtension_5), value);
	}

	inline static int32_t get_offset_of_filterFileName_6() { return static_cast<int32_t>(offsetof(FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28, ___filterFileName_6)); }
	inline String_t* get_filterFileName_6() const { return ___filterFileName_6; }
	inline String_t** get_address_of_filterFileName_6() { return &___filterFileName_6; }
	inline void set_filterFileName_6(String_t* value)
	{
		___filterFileName_6 = value;
		Il2CppCodeGenWriteBarrier((&___filterFileName_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILEFILTER_T73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28_H
#ifndef HASHGENERATORRESULT_T0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7_H
#define HASHGENERATORRESULT_T0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Genuine.CodeHash.HashGeneratorResult
struct  HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7  : public RuntimeObject
{
public:
	// System.String CodeStage.AntiCheat.Genuine.CodeHash.HashGeneratorResult::<CodeHash>k__BackingField
	String_t* ___U3CCodeHashU3Ek__BackingField_0;
	// System.String CodeStage.AntiCheat.Genuine.CodeHash.HashGeneratorResult::<ErrorMessage>k__BackingField
	String_t* ___U3CErrorMessageU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCodeHashU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7, ___U3CCodeHashU3Ek__BackingField_0)); }
	inline String_t* get_U3CCodeHashU3Ek__BackingField_0() const { return ___U3CCodeHashU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCodeHashU3Ek__BackingField_0() { return &___U3CCodeHashU3Ek__BackingField_0; }
	inline void set_U3CCodeHashU3Ek__BackingField_0(String_t* value)
	{
		___U3CCodeHashU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCodeHashU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CErrorMessageU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7, ___U3CErrorMessageU3Ek__BackingField_1)); }
	inline String_t* get_U3CErrorMessageU3Ek__BackingField_1() const { return ___U3CErrorMessageU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CErrorMessageU3Ek__BackingField_1() { return &___U3CErrorMessageU3Ek__BackingField_1; }
	inline void set_U3CErrorMessageU3Ek__BackingField_1(String_t* value)
	{
		___U3CErrorMessageU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorMessageU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHGENERATORRESULT_T0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7_H
#ifndef ABSTWEENPLUGIN_3_TB581A16037BDC1101611BFD35AF866E59472559A_H
#define ABSTWEENPLUGIN_3_TB581A16037BDC1101611BFD35AF866E59472559A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct  ABSTweenPlugin_3_tB581A16037BDC1101611BFD35AF866E59472559A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_TB581A16037BDC1101611BFD35AF866E59472559A_H
#ifndef ABSTWEENPLUGIN_3_T421856DF2C12FA3EB476B9DFF14D0176AC9148EB_H
#define ABSTWEENPLUGIN_3_T421856DF2C12FA3EB476B9DFF14D0176AC9148EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t421856DF2C12FA3EB476B9DFF14D0176AC9148EB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T421856DF2C12FA3EB476B9DFF14D0176AC9148EB_H
#ifndef ABSTWEENPLUGIN_3_TC3773F7BBD824370E35F778AC9CBB6D541CF11B6_H
#define ABSTWEENPLUGIN_3_TC3773F7BBD824370E35F778AC9CBB6D541CF11B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_tC3773F7BBD824370E35F778AC9CBB6D541CF11B6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_TC3773F7BBD824370E35F778AC9CBB6D541CF11B6_H
#ifndef ABSTWEENPLUGIN_3_T18BE0A47C2F614B13A6B50635351658E9D666DA1_H
#define ABSTWEENPLUGIN_3_T18BE0A47C2F614B13A6B50635351658E9D666DA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t18BE0A47C2F614B13A6B50635351658E9D666DA1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T18BE0A47C2F614B13A6B50635351658E9D666DA1_H
#ifndef ABSTWEENPLUGIN_3_T98C01ECFC5E278EFEB1AEF3512475A4604A0AFF5_H
#define ABSTWEENPLUGIN_3_T98C01ECFC5E278EFEB1AEF3512475A4604A0AFF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct  ABSTweenPlugin_3_t98C01ECFC5E278EFEB1AEF3512475A4604A0AFF5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T98C01ECFC5E278EFEB1AEF3512475A4604A0AFF5_H
#ifndef ABSTWEENPLUGIN_3_T9D226D0B072F9EA7690FA7A649856709CE00D725_H
#define ABSTWEENPLUGIN_3_T9D226D0B072F9EA7690FA7A649856709CE00D725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
struct  ABSTweenPlugin_3_t9D226D0B072F9EA7690FA7A649856709CE00D725  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T9D226D0B072F9EA7690FA7A649856709CE00D725_H
#ifndef ABSTWEENPLUGIN_3_TF49BD32818EDBBDC078E7E8BAD2AD193D74B8489_H
#define ABSTWEENPLUGIN_3_TF49BD32818EDBBDC078E7E8BAD2AD193D74B8489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>
struct  ABSTweenPlugin_3_tF49BD32818EDBBDC078E7E8BAD2AD193D74B8489  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_TF49BD32818EDBBDC078E7E8BAD2AD193D74B8489_H
#ifndef ABSTWEENPLUGIN_3_TD282AD5EB6CAA3FCACDF51432ACC1C2E51777D01_H
#define ABSTWEENPLUGIN_3_TD282AD5EB6CAA3FCACDF51432ACC1C2E51777D01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_tD282AD5EB6CAA3FCACDF51432ACC1C2E51777D01  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_TD282AD5EB6CAA3FCACDF51432ACC1C2E51777D01_H
#ifndef ABSTWEENPLUGIN_3_TD58649751AD5C680679FE9F34E72C693082D2950_H
#define ABSTWEENPLUGIN_3_TD58649751AD5C680679FE9F34E72C693082D2950_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct  ABSTweenPlugin_3_tD58649751AD5C680679FE9F34E72C693082D2950  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_TD58649751AD5C680679FE9F34E72C693082D2950_H
#ifndef ABSTWEENPLUGIN_3_TE10F18828A3B58083EDF713736377A9F10B2CEC5_H
#define ABSTWEENPLUGIN_3_TE10F18828A3B58083EDF713736377A9F10B2CEC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct  ABSTweenPlugin_3_tE10F18828A3B58083EDF713736377A9F10B2CEC5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_TE10F18828A3B58083EDF713736377A9F10B2CEC5_H
#ifndef ABSTWEENPLUGIN_3_TEB25032AFD776502392FFF377631C0A006EED8C4_H
#define ABSTWEENPLUGIN_3_TEB25032AFD776502392FFF377631C0A006EED8C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct  ABSTweenPlugin_3_tEB25032AFD776502392FFF377631C0A006EED8C4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_TEB25032AFD776502392FFF377631C0A006EED8C4_H
#ifndef ABSTWEENPLUGIN_3_T68925870D981F1013657936919AE334797F43A6C_H
#define ABSTWEENPLUGIN_3_T68925870D981F1013657936919AE334797F43A6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t68925870D981F1013657936919AE334797F43A6C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T68925870D981F1013657936919AE334797F43A6C_H
#ifndef ABSTWEENPLUGIN_3_TF0F62B2C3E037FA76B2A6352C4C0E6C8B2D668FD_H
#define ABSTWEENPLUGIN_3_TF0F62B2C3E037FA76B2A6352C4C0E6C8B2D668FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct  ABSTweenPlugin_3_tF0F62B2C3E037FA76B2A6352C4C0E6C8B2D668FD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_TF0F62B2C3E037FA76B2A6352C4C0E6C8B2D668FD_H
#ifndef ABSTWEENPLUGIN_3_T50462951CA2C372D1A9BC7C38C6ECC1810944678_H
#define ABSTWEENPLUGIN_3_T50462951CA2C372D1A9BC7C38C6ECC1810944678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct  ABSTweenPlugin_3_t50462951CA2C372D1A9BC7C38C6ECC1810944678  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T50462951CA2C372D1A9BC7C38C6ECC1810944678_H
#ifndef ABSTWEENPLUGIN_3_T59C22DD36F4259810DAE41F464AC73D990C88056_H
#define ABSTWEENPLUGIN_3_T59C22DD36F4259810DAE41F464AC73D990C88056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct  ABSTweenPlugin_3_t59C22DD36F4259810DAE41F464AC73D990C88056  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T59C22DD36F4259810DAE41F464AC73D990C88056_H
#ifndef ABSTWEENPLUGIN_3_T224A02B0B2F704CE5A82C0EC7719C0ADC0B382DD_H
#define ABSTWEENPLUGIN_3_T224A02B0B2F704CE5A82C0EC7719C0ADC0B382DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct  ABSTweenPlugin_3_t224A02B0B2F704CE5A82C0EC7719C0ADC0B382DD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T224A02B0B2F704CE5A82C0EC7719C0ADC0B382DD_H
#ifndef PLUGINSMANAGER_T673F317071E579280C9F5FFB31D751511905BE1E_H
#define PLUGINSMANAGER_T673F317071E579280C9F5FFB31D751511905BE1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PluginsManager
struct  PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E  : public RuntimeObject
{
public:

public:
};

struct PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields
{
public:
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_floatPlugin
	RuntimeObject* ____floatPlugin_0;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_doublePlugin
	RuntimeObject* ____doublePlugin_1;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_intPlugin
	RuntimeObject* ____intPlugin_2;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_uintPlugin
	RuntimeObject* ____uintPlugin_3;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_longPlugin
	RuntimeObject* ____longPlugin_4;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_ulongPlugin
	RuntimeObject* ____ulongPlugin_5;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector2Plugin
	RuntimeObject* ____vector2Plugin_6;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector3Plugin
	RuntimeObject* ____vector3Plugin_7;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector4Plugin
	RuntimeObject* ____vector4Plugin_8;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_quaternionPlugin
	RuntimeObject* ____quaternionPlugin_9;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_colorPlugin
	RuntimeObject* ____colorPlugin_10;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_rectPlugin
	RuntimeObject* ____rectPlugin_11;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_rectOffsetPlugin
	RuntimeObject* ____rectOffsetPlugin_12;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_stringPlugin
	RuntimeObject* ____stringPlugin_13;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector3ArrayPlugin
	RuntimeObject* ____vector3ArrayPlugin_14;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_color2Plugin
	RuntimeObject* ____color2Plugin_15;

public:
	inline static int32_t get_offset_of__floatPlugin_0() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____floatPlugin_0)); }
	inline RuntimeObject* get__floatPlugin_0() const { return ____floatPlugin_0; }
	inline RuntimeObject** get_address_of__floatPlugin_0() { return &____floatPlugin_0; }
	inline void set__floatPlugin_0(RuntimeObject* value)
	{
		____floatPlugin_0 = value;
		Il2CppCodeGenWriteBarrier((&____floatPlugin_0), value);
	}

	inline static int32_t get_offset_of__doublePlugin_1() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____doublePlugin_1)); }
	inline RuntimeObject* get__doublePlugin_1() const { return ____doublePlugin_1; }
	inline RuntimeObject** get_address_of__doublePlugin_1() { return &____doublePlugin_1; }
	inline void set__doublePlugin_1(RuntimeObject* value)
	{
		____doublePlugin_1 = value;
		Il2CppCodeGenWriteBarrier((&____doublePlugin_1), value);
	}

	inline static int32_t get_offset_of__intPlugin_2() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____intPlugin_2)); }
	inline RuntimeObject* get__intPlugin_2() const { return ____intPlugin_2; }
	inline RuntimeObject** get_address_of__intPlugin_2() { return &____intPlugin_2; }
	inline void set__intPlugin_2(RuntimeObject* value)
	{
		____intPlugin_2 = value;
		Il2CppCodeGenWriteBarrier((&____intPlugin_2), value);
	}

	inline static int32_t get_offset_of__uintPlugin_3() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____uintPlugin_3)); }
	inline RuntimeObject* get__uintPlugin_3() const { return ____uintPlugin_3; }
	inline RuntimeObject** get_address_of__uintPlugin_3() { return &____uintPlugin_3; }
	inline void set__uintPlugin_3(RuntimeObject* value)
	{
		____uintPlugin_3 = value;
		Il2CppCodeGenWriteBarrier((&____uintPlugin_3), value);
	}

	inline static int32_t get_offset_of__longPlugin_4() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____longPlugin_4)); }
	inline RuntimeObject* get__longPlugin_4() const { return ____longPlugin_4; }
	inline RuntimeObject** get_address_of__longPlugin_4() { return &____longPlugin_4; }
	inline void set__longPlugin_4(RuntimeObject* value)
	{
		____longPlugin_4 = value;
		Il2CppCodeGenWriteBarrier((&____longPlugin_4), value);
	}

	inline static int32_t get_offset_of__ulongPlugin_5() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____ulongPlugin_5)); }
	inline RuntimeObject* get__ulongPlugin_5() const { return ____ulongPlugin_5; }
	inline RuntimeObject** get_address_of__ulongPlugin_5() { return &____ulongPlugin_5; }
	inline void set__ulongPlugin_5(RuntimeObject* value)
	{
		____ulongPlugin_5 = value;
		Il2CppCodeGenWriteBarrier((&____ulongPlugin_5), value);
	}

	inline static int32_t get_offset_of__vector2Plugin_6() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____vector2Plugin_6)); }
	inline RuntimeObject* get__vector2Plugin_6() const { return ____vector2Plugin_6; }
	inline RuntimeObject** get_address_of__vector2Plugin_6() { return &____vector2Plugin_6; }
	inline void set__vector2Plugin_6(RuntimeObject* value)
	{
		____vector2Plugin_6 = value;
		Il2CppCodeGenWriteBarrier((&____vector2Plugin_6), value);
	}

	inline static int32_t get_offset_of__vector3Plugin_7() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____vector3Plugin_7)); }
	inline RuntimeObject* get__vector3Plugin_7() const { return ____vector3Plugin_7; }
	inline RuntimeObject** get_address_of__vector3Plugin_7() { return &____vector3Plugin_7; }
	inline void set__vector3Plugin_7(RuntimeObject* value)
	{
		____vector3Plugin_7 = value;
		Il2CppCodeGenWriteBarrier((&____vector3Plugin_7), value);
	}

	inline static int32_t get_offset_of__vector4Plugin_8() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____vector4Plugin_8)); }
	inline RuntimeObject* get__vector4Plugin_8() const { return ____vector4Plugin_8; }
	inline RuntimeObject** get_address_of__vector4Plugin_8() { return &____vector4Plugin_8; }
	inline void set__vector4Plugin_8(RuntimeObject* value)
	{
		____vector4Plugin_8 = value;
		Il2CppCodeGenWriteBarrier((&____vector4Plugin_8), value);
	}

	inline static int32_t get_offset_of__quaternionPlugin_9() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____quaternionPlugin_9)); }
	inline RuntimeObject* get__quaternionPlugin_9() const { return ____quaternionPlugin_9; }
	inline RuntimeObject** get_address_of__quaternionPlugin_9() { return &____quaternionPlugin_9; }
	inline void set__quaternionPlugin_9(RuntimeObject* value)
	{
		____quaternionPlugin_9 = value;
		Il2CppCodeGenWriteBarrier((&____quaternionPlugin_9), value);
	}

	inline static int32_t get_offset_of__colorPlugin_10() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____colorPlugin_10)); }
	inline RuntimeObject* get__colorPlugin_10() const { return ____colorPlugin_10; }
	inline RuntimeObject** get_address_of__colorPlugin_10() { return &____colorPlugin_10; }
	inline void set__colorPlugin_10(RuntimeObject* value)
	{
		____colorPlugin_10 = value;
		Il2CppCodeGenWriteBarrier((&____colorPlugin_10), value);
	}

	inline static int32_t get_offset_of__rectPlugin_11() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____rectPlugin_11)); }
	inline RuntimeObject* get__rectPlugin_11() const { return ____rectPlugin_11; }
	inline RuntimeObject** get_address_of__rectPlugin_11() { return &____rectPlugin_11; }
	inline void set__rectPlugin_11(RuntimeObject* value)
	{
		____rectPlugin_11 = value;
		Il2CppCodeGenWriteBarrier((&____rectPlugin_11), value);
	}

	inline static int32_t get_offset_of__rectOffsetPlugin_12() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____rectOffsetPlugin_12)); }
	inline RuntimeObject* get__rectOffsetPlugin_12() const { return ____rectOffsetPlugin_12; }
	inline RuntimeObject** get_address_of__rectOffsetPlugin_12() { return &____rectOffsetPlugin_12; }
	inline void set__rectOffsetPlugin_12(RuntimeObject* value)
	{
		____rectOffsetPlugin_12 = value;
		Il2CppCodeGenWriteBarrier((&____rectOffsetPlugin_12), value);
	}

	inline static int32_t get_offset_of__stringPlugin_13() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____stringPlugin_13)); }
	inline RuntimeObject* get__stringPlugin_13() const { return ____stringPlugin_13; }
	inline RuntimeObject** get_address_of__stringPlugin_13() { return &____stringPlugin_13; }
	inline void set__stringPlugin_13(RuntimeObject* value)
	{
		____stringPlugin_13 = value;
		Il2CppCodeGenWriteBarrier((&____stringPlugin_13), value);
	}

	inline static int32_t get_offset_of__vector3ArrayPlugin_14() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____vector3ArrayPlugin_14)); }
	inline RuntimeObject* get__vector3ArrayPlugin_14() const { return ____vector3ArrayPlugin_14; }
	inline RuntimeObject** get_address_of__vector3ArrayPlugin_14() { return &____vector3ArrayPlugin_14; }
	inline void set__vector3ArrayPlugin_14(RuntimeObject* value)
	{
		____vector3ArrayPlugin_14 = value;
		Il2CppCodeGenWriteBarrier((&____vector3ArrayPlugin_14), value);
	}

	inline static int32_t get_offset_of__color2Plugin_15() { return static_cast<int32_t>(offsetof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields, ____color2Plugin_15)); }
	inline RuntimeObject* get__color2Plugin_15() const { return ____color2Plugin_15; }
	inline RuntimeObject** get_address_of__color2Plugin_15() { return &____color2Plugin_15; }
	inline void set__color2Plugin_15(RuntimeObject* value)
	{
		____color2Plugin_15 = value;
		Il2CppCodeGenWriteBarrier((&____color2Plugin_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLUGINSMANAGER_T673F317071E579280C9F5FFB31D751511905BE1E_H
#ifndef SPECIALPLUGINSUTILS_T7ACBDEA1BEB198E54F55F73804E9244952DFB5CE_H
#define SPECIALPLUGINSUTILS_T7ACBDEA1BEB198E54F55F73804E9244952DFB5CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.SpecialPluginsUtils
struct  SpecialPluginsUtils_t7ACBDEA1BEB198E54F55F73804E9244952DFB5CE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALPLUGINSUTILS_T7ACBDEA1BEB198E54F55F73804E9244952DFB5CE_H
#ifndef STRINGPLUGINEXTENSIONS_TB7BCF7EB9633300CDD2387F467B58782AE28F06B_H
#define STRINGPLUGINEXTENSIONS_TB7BCF7EB9633300CDD2387F467B58782AE28F06B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.StringPluginExtensions
struct  StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B  : public RuntimeObject
{
public:

public:
};

struct StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B_StaticFields
{
public:
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsAll
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___ScrambledCharsAll_0;
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsUppercase
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___ScrambledCharsUppercase_1;
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsLowercase
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___ScrambledCharsLowercase_2;
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsNumerals
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___ScrambledCharsNumerals_3;
	// System.Int32 DG.Tweening.Plugins.StringPluginExtensions::_lastRndSeed
	int32_t ____lastRndSeed_4;

public:
	inline static int32_t get_offset_of_ScrambledCharsAll_0() { return static_cast<int32_t>(offsetof(StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B_StaticFields, ___ScrambledCharsAll_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_ScrambledCharsAll_0() const { return ___ScrambledCharsAll_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_ScrambledCharsAll_0() { return &___ScrambledCharsAll_0; }
	inline void set_ScrambledCharsAll_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___ScrambledCharsAll_0 = value;
		Il2CppCodeGenWriteBarrier((&___ScrambledCharsAll_0), value);
	}

	inline static int32_t get_offset_of_ScrambledCharsUppercase_1() { return static_cast<int32_t>(offsetof(StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B_StaticFields, ___ScrambledCharsUppercase_1)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_ScrambledCharsUppercase_1() const { return ___ScrambledCharsUppercase_1; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_ScrambledCharsUppercase_1() { return &___ScrambledCharsUppercase_1; }
	inline void set_ScrambledCharsUppercase_1(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___ScrambledCharsUppercase_1 = value;
		Il2CppCodeGenWriteBarrier((&___ScrambledCharsUppercase_1), value);
	}

	inline static int32_t get_offset_of_ScrambledCharsLowercase_2() { return static_cast<int32_t>(offsetof(StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B_StaticFields, ___ScrambledCharsLowercase_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_ScrambledCharsLowercase_2() const { return ___ScrambledCharsLowercase_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_ScrambledCharsLowercase_2() { return &___ScrambledCharsLowercase_2; }
	inline void set_ScrambledCharsLowercase_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___ScrambledCharsLowercase_2 = value;
		Il2CppCodeGenWriteBarrier((&___ScrambledCharsLowercase_2), value);
	}

	inline static int32_t get_offset_of_ScrambledCharsNumerals_3() { return static_cast<int32_t>(offsetof(StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B_StaticFields, ___ScrambledCharsNumerals_3)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_ScrambledCharsNumerals_3() const { return ___ScrambledCharsNumerals_3; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_ScrambledCharsNumerals_3() { return &___ScrambledCharsNumerals_3; }
	inline void set_ScrambledCharsNumerals_3(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___ScrambledCharsNumerals_3 = value;
		Il2CppCodeGenWriteBarrier((&___ScrambledCharsNumerals_3), value);
	}

	inline static int32_t get_offset_of__lastRndSeed_4() { return static_cast<int32_t>(offsetof(StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B_StaticFields, ____lastRndSeed_4)); }
	inline int32_t get__lastRndSeed_4() const { return ____lastRndSeed_4; }
	inline int32_t* get_address_of__lastRndSeed_4() { return &____lastRndSeed_4; }
	inline void set__lastRndSeed_4(int32_t value)
	{
		____lastRndSeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGPLUGINEXTENSIONS_TB7BCF7EB9633300CDD2387F467B58782AE28F06B_H
#ifndef SHORTCUTEXTENSIONS_T6551D100BBB9E0A80E6197A1DE743833FF1D0ED8_H
#define SHORTCUTEXTENSIONS_T6551D100BBB9E0A80E6197A1DE743833FF1D0ED8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions
struct  ShortcutExtensions_t6551D100BBB9E0A80E6197A1DE743833FF1D0ED8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHORTCUTEXTENSIONS_T6551D100BBB9E0A80E6197A1DE743833FF1D0ED8_H
#ifndef U3CU3EC__DISPLAYCLASS41_0_TFAA38E06FCDC2A58D29D87FE82C5C07098024512_H
#define U3CU3EC__DISPLAYCLASS41_0_TFAA38E06FCDC2A58D29D87FE82C5C07098024512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions_<>c__DisplayClass41_0
struct  U3CU3Ec__DisplayClass41_0_tFAA38E06FCDC2A58D29D87FE82C5C07098024512  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions_<>c__DisplayClass41_0::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_tFAA38E06FCDC2A58D29D87FE82C5C07098024512, ___target_0)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_0() const { return ___target_0; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS41_0_TFAA38E06FCDC2A58D29D87FE82C5C07098024512_H
#ifndef U3CU3EC__DISPLAYCLASS43_0_T9F7D038FE3DAD3149EA263A82FB93104082BB9A8_H
#define U3CU3EC__DISPLAYCLASS43_0_T9F7D038FE3DAD3149EA263A82FB93104082BB9A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions_<>c__DisplayClass43_0
struct  U3CU3Ec__DisplayClass43_0_t9F7D038FE3DAD3149EA263A82FB93104082BB9A8  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions_<>c__DisplayClass43_0::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_0_t9F7D038FE3DAD3149EA263A82FB93104082BB9A8, ___target_0)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_0() const { return ___target_0; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS43_0_T9F7D038FE3DAD3149EA263A82FB93104082BB9A8_H
#ifndef U3CU3EC__DISPLAYCLASS60_0_T64409C37981CA8094BF58D3393801152AC68A43B_H
#define U3CU3EC__DISPLAYCLASS60_0_T64409C37981CA8094BF58D3393801152AC68A43B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions_<>c__DisplayClass60_0
struct  U3CU3Ec__DisplayClass60_0_t64409C37981CA8094BF58D3393801152AC68A43B  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions_<>c__DisplayClass60_0::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass60_0_t64409C37981CA8094BF58D3393801152AC68A43B, ___target_0)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_0() const { return ___target_0; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS60_0_T64409C37981CA8094BF58D3393801152AC68A43B_H
#ifndef TWEENSETTINGSEXTENSIONS_T61A3DCA4F25E31E73F0104D8DE15B2D9AC78CCCA_H
#define TWEENSETTINGSEXTENSIONS_T61A3DCA4F25E31E73F0104D8DE15B2D9AC78CCCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenSettingsExtensions
struct  TweenSettingsExtensions_t61A3DCA4F25E31E73F0104D8DE15B2D9AC78CCCA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENSETTINGSEXTENSIONS_T61A3DCA4F25E31E73F0104D8DE15B2D9AC78CCCA_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef __STATICARRAYINITTYPESIZEU3D10_T85C6225FBC4C8C59B85DFE15371916C1912F4837_H
#define __STATICARRAYINITTYPESIZEU3D10_T85C6225FBC4C8C59B85DFE15371916C1912F4837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D10
struct  __StaticArrayInitTypeSizeU3D10_t85C6225FBC4C8C59B85DFE15371916C1912F4837 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D10_t85C6225FBC4C8C59B85DFE15371916C1912F4837__padding[10];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D10_T85C6225FBC4C8C59B85DFE15371916C1912F4837_H
#ifndef ACTKBYTE16_TBA7BB660E30157B457EAF59F1313F241EA53C340_H
#define ACTKBYTE16_TBA7BB660E30157B457EAF59F1313F241EA53C340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ACTkByte16
struct  ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340 
{
public:
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b1
	uint8_t ___b1_0;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b2
	uint8_t ___b2_1;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b3
	uint8_t ___b3_2;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b4
	uint8_t ___b4_3;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b5
	uint8_t ___b5_4;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b6
	uint8_t ___b6_5;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b7
	uint8_t ___b7_6;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b8
	uint8_t ___b8_7;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b9
	uint8_t ___b9_8;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b10
	uint8_t ___b10_9;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b11
	uint8_t ___b11_10;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b12
	uint8_t ___b12_11;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b13
	uint8_t ___b13_12;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b14
	uint8_t ___b14_13;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b15
	uint8_t ___b15_14;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte16::b16
	uint8_t ___b16_15;

public:
	inline static int32_t get_offset_of_b1_0() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b1_0)); }
	inline uint8_t get_b1_0() const { return ___b1_0; }
	inline uint8_t* get_address_of_b1_0() { return &___b1_0; }
	inline void set_b1_0(uint8_t value)
	{
		___b1_0 = value;
	}

	inline static int32_t get_offset_of_b2_1() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b2_1)); }
	inline uint8_t get_b2_1() const { return ___b2_1; }
	inline uint8_t* get_address_of_b2_1() { return &___b2_1; }
	inline void set_b2_1(uint8_t value)
	{
		___b2_1 = value;
	}

	inline static int32_t get_offset_of_b3_2() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b3_2)); }
	inline uint8_t get_b3_2() const { return ___b3_2; }
	inline uint8_t* get_address_of_b3_2() { return &___b3_2; }
	inline void set_b3_2(uint8_t value)
	{
		___b3_2 = value;
	}

	inline static int32_t get_offset_of_b4_3() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b4_3)); }
	inline uint8_t get_b4_3() const { return ___b4_3; }
	inline uint8_t* get_address_of_b4_3() { return &___b4_3; }
	inline void set_b4_3(uint8_t value)
	{
		___b4_3 = value;
	}

	inline static int32_t get_offset_of_b5_4() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b5_4)); }
	inline uint8_t get_b5_4() const { return ___b5_4; }
	inline uint8_t* get_address_of_b5_4() { return &___b5_4; }
	inline void set_b5_4(uint8_t value)
	{
		___b5_4 = value;
	}

	inline static int32_t get_offset_of_b6_5() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b6_5)); }
	inline uint8_t get_b6_5() const { return ___b6_5; }
	inline uint8_t* get_address_of_b6_5() { return &___b6_5; }
	inline void set_b6_5(uint8_t value)
	{
		___b6_5 = value;
	}

	inline static int32_t get_offset_of_b7_6() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b7_6)); }
	inline uint8_t get_b7_6() const { return ___b7_6; }
	inline uint8_t* get_address_of_b7_6() { return &___b7_6; }
	inline void set_b7_6(uint8_t value)
	{
		___b7_6 = value;
	}

	inline static int32_t get_offset_of_b8_7() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b8_7)); }
	inline uint8_t get_b8_7() const { return ___b8_7; }
	inline uint8_t* get_address_of_b8_7() { return &___b8_7; }
	inline void set_b8_7(uint8_t value)
	{
		___b8_7 = value;
	}

	inline static int32_t get_offset_of_b9_8() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b9_8)); }
	inline uint8_t get_b9_8() const { return ___b9_8; }
	inline uint8_t* get_address_of_b9_8() { return &___b9_8; }
	inline void set_b9_8(uint8_t value)
	{
		___b9_8 = value;
	}

	inline static int32_t get_offset_of_b10_9() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b10_9)); }
	inline uint8_t get_b10_9() const { return ___b10_9; }
	inline uint8_t* get_address_of_b10_9() { return &___b10_9; }
	inline void set_b10_9(uint8_t value)
	{
		___b10_9 = value;
	}

	inline static int32_t get_offset_of_b11_10() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b11_10)); }
	inline uint8_t get_b11_10() const { return ___b11_10; }
	inline uint8_t* get_address_of_b11_10() { return &___b11_10; }
	inline void set_b11_10(uint8_t value)
	{
		___b11_10 = value;
	}

	inline static int32_t get_offset_of_b12_11() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b12_11)); }
	inline uint8_t get_b12_11() const { return ___b12_11; }
	inline uint8_t* get_address_of_b12_11() { return &___b12_11; }
	inline void set_b12_11(uint8_t value)
	{
		___b12_11 = value;
	}

	inline static int32_t get_offset_of_b13_12() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b13_12)); }
	inline uint8_t get_b13_12() const { return ___b13_12; }
	inline uint8_t* get_address_of_b13_12() { return &___b13_12; }
	inline void set_b13_12(uint8_t value)
	{
		___b13_12 = value;
	}

	inline static int32_t get_offset_of_b14_13() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b14_13)); }
	inline uint8_t get_b14_13() const { return ___b14_13; }
	inline uint8_t* get_address_of_b14_13() { return &___b14_13; }
	inline void set_b14_13(uint8_t value)
	{
		___b14_13 = value;
	}

	inline static int32_t get_offset_of_b15_14() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b15_14)); }
	inline uint8_t get_b15_14() const { return ___b15_14; }
	inline uint8_t* get_address_of_b15_14() { return &___b15_14; }
	inline void set_b15_14(uint8_t value)
	{
		___b15_14 = value;
	}

	inline static int32_t get_offset_of_b16_15() { return static_cast<int32_t>(offsetof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340, ___b16_15)); }
	inline uint8_t get_b16_15() const { return ___b16_15; }
	inline uint8_t* get_address_of_b16_15() { return &___b16_15; }
	inline void set_b16_15(uint8_t value)
	{
		___b16_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKBYTE16_TBA7BB660E30157B457EAF59F1313F241EA53C340_H
#ifndef ACTKBYTE4_T07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE_H
#define ACTKBYTE4_T07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ACTkByte4
struct  ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE 
{
public:
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b1
	uint8_t ___b1_0;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b2
	uint8_t ___b2_1;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b3
	uint8_t ___b3_2;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte4::b4
	uint8_t ___b4_3;

public:
	inline static int32_t get_offset_of_b1_0() { return static_cast<int32_t>(offsetof(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE, ___b1_0)); }
	inline uint8_t get_b1_0() const { return ___b1_0; }
	inline uint8_t* get_address_of_b1_0() { return &___b1_0; }
	inline void set_b1_0(uint8_t value)
	{
		___b1_0 = value;
	}

	inline static int32_t get_offset_of_b2_1() { return static_cast<int32_t>(offsetof(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE, ___b2_1)); }
	inline uint8_t get_b2_1() const { return ___b2_1; }
	inline uint8_t* get_address_of_b2_1() { return &___b2_1; }
	inline void set_b2_1(uint8_t value)
	{
		___b2_1 = value;
	}

	inline static int32_t get_offset_of_b3_2() { return static_cast<int32_t>(offsetof(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE, ___b3_2)); }
	inline uint8_t get_b3_2() const { return ___b3_2; }
	inline uint8_t* get_address_of_b3_2() { return &___b3_2; }
	inline void set_b3_2(uint8_t value)
	{
		___b3_2 = value;
	}

	inline static int32_t get_offset_of_b4_3() { return static_cast<int32_t>(offsetof(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE, ___b4_3)); }
	inline uint8_t get_b4_3() const { return ___b4_3; }
	inline uint8_t* get_address_of_b4_3() { return &___b4_3; }
	inline void set_b4_3(uint8_t value)
	{
		___b4_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKBYTE4_T07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE_H
#ifndef ACTKBYTE8_T833E13ECB9BCA90642862B8F95D336AE37DEA24E_H
#define ACTKBYTE8_T833E13ECB9BCA90642862B8F95D336AE37DEA24E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.ACTkByte8
struct  ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E 
{
public:
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b1
	uint8_t ___b1_0;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b2
	uint8_t ___b2_1;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b3
	uint8_t ___b3_2;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b4
	uint8_t ___b4_3;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b5
	uint8_t ___b5_4;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b6
	uint8_t ___b6_5;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b7
	uint8_t ___b7_6;
	// System.Byte CodeStage.AntiCheat.Common.ACTkByte8::b8
	uint8_t ___b8_7;

public:
	inline static int32_t get_offset_of_b1_0() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b1_0)); }
	inline uint8_t get_b1_0() const { return ___b1_0; }
	inline uint8_t* get_address_of_b1_0() { return &___b1_0; }
	inline void set_b1_0(uint8_t value)
	{
		___b1_0 = value;
	}

	inline static int32_t get_offset_of_b2_1() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b2_1)); }
	inline uint8_t get_b2_1() const { return ___b2_1; }
	inline uint8_t* get_address_of_b2_1() { return &___b2_1; }
	inline void set_b2_1(uint8_t value)
	{
		___b2_1 = value;
	}

	inline static int32_t get_offset_of_b3_2() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b3_2)); }
	inline uint8_t get_b3_2() const { return ___b3_2; }
	inline uint8_t* get_address_of_b3_2() { return &___b3_2; }
	inline void set_b3_2(uint8_t value)
	{
		___b3_2 = value;
	}

	inline static int32_t get_offset_of_b4_3() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b4_3)); }
	inline uint8_t get_b4_3() const { return ___b4_3; }
	inline uint8_t* get_address_of_b4_3() { return &___b4_3; }
	inline void set_b4_3(uint8_t value)
	{
		___b4_3 = value;
	}

	inline static int32_t get_offset_of_b5_4() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b5_4)); }
	inline uint8_t get_b5_4() const { return ___b5_4; }
	inline uint8_t* get_address_of_b5_4() { return &___b5_4; }
	inline void set_b5_4(uint8_t value)
	{
		___b5_4 = value;
	}

	inline static int32_t get_offset_of_b6_5() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b6_5)); }
	inline uint8_t get_b6_5() const { return ___b6_5; }
	inline uint8_t* get_address_of_b6_5() { return &___b6_5; }
	inline void set_b6_5(uint8_t value)
	{
		___b6_5 = value;
	}

	inline static int32_t get_offset_of_b7_6() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b7_6)); }
	inline uint8_t get_b7_6() const { return ___b7_6; }
	inline uint8_t* get_address_of_b7_6() { return &___b7_6; }
	inline void set_b7_6(uint8_t value)
	{
		___b7_6 = value;
	}

	inline static int32_t get_offset_of_b8_7() { return static_cast<int32_t>(offsetof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E, ___b8_7)); }
	inline uint8_t get_b8_7() const { return ___b8_7; }
	inline uint8_t* get_address_of_b8_7() { return &___b8_7; }
	inline void set_b8_7(uint8_t value)
	{
		___b8_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKBYTE8_T833E13ECB9BCA90642862B8F95D336AE37DEA24E_H
#ifndef ONLINETIMERESULT_T7B01E78EE0B7D72C97AD8F2470B481762DECF94D_H
#define ONLINETIMERESULT_T7B01E78EE0B7D72C97AD8F2470B481762DECF94D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeResult
struct  OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D 
{
public:
	// System.Boolean CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeResult::success
	bool ___success_0;
	// System.String CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeResult::error
	String_t* ___error_1;
	// System.Int64 CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeResult::errorResponseCode
	int64_t ___errorResponseCode_2;
	// System.Double CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeResult::onlineSecondsUtc
	double ___onlineSecondsUtc_3;

public:
	inline static int32_t get_offset_of_success_0() { return static_cast<int32_t>(offsetof(OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D, ___success_0)); }
	inline bool get_success_0() const { return ___success_0; }
	inline bool* get_address_of_success_0() { return &___success_0; }
	inline void set_success_0(bool value)
	{
		___success_0 = value;
	}

	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D, ___error_1)); }
	inline String_t* get_error_1() const { return ___error_1; }
	inline String_t** get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(String_t* value)
	{
		___error_1 = value;
		Il2CppCodeGenWriteBarrier((&___error_1), value);
	}

	inline static int32_t get_offset_of_errorResponseCode_2() { return static_cast<int32_t>(offsetof(OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D, ___errorResponseCode_2)); }
	inline int64_t get_errorResponseCode_2() const { return ___errorResponseCode_2; }
	inline int64_t* get_address_of_errorResponseCode_2() { return &___errorResponseCode_2; }
	inline void set_errorResponseCode_2(int64_t value)
	{
		___errorResponseCode_2 = value;
	}

	inline static int32_t get_offset_of_onlineSecondsUtc_3() { return static_cast<int32_t>(offsetof(OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D, ___onlineSecondsUtc_3)); }
	inline double get_onlineSecondsUtc_3() const { return ___onlineSecondsUtc_3; }
	inline double* get_address_of_onlineSecondsUtc_3() { return &___onlineSecondsUtc_3; }
	inline void set_onlineSecondsUtc_3(double value)
	{
		___onlineSecondsUtc_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.Detectors.TimeCheatingDetector/OnlineTimeResult
struct OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D_marshaled_pinvoke
{
	int32_t ___success_0;
	char* ___error_1;
	int64_t ___errorResponseCode_2;
	double ___onlineSecondsUtc_3;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.Detectors.TimeCheatingDetector/OnlineTimeResult
struct OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D_marshaled_com
{
	int32_t ___success_0;
	Il2CppChar* ___error_1;
	int64_t ___errorResponseCode_2;
	double ___onlineSecondsUtc_3;
};
#endif // ONLINETIMERESULT_T7B01E78EE0B7D72C97AD8F2470B481762DECF94D_H
#ifndef STANDALONEWINDOWSWORKER_TF493FB75FD28A9170738009C890FE4D327E8E666_H
#define STANDALONEWINDOWSWORKER_TF493FB75FD28A9170738009C890FE4D327E8E666_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Genuine.CodeHash.StandaloneWindowsWorker
struct  StandaloneWindowsWorker_tF493FB75FD28A9170738009C890FE4D327E8E666  : public BaseWorker_tD3B5F0035F406307CFD71F8BCF644A3D81A1036E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDALONEWINDOWSWORKER_TF493FB75FD28A9170738009C890FE4D327E8E666_H
#ifndef RAWENCRYPTEDVECTOR3_T766A78183852F7A89484E372DBFD15EEF76AB627_H
#define RAWENCRYPTEDVECTOR3_T766A78183852F7A89484E372DBFD15EEF76AB627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3
struct  RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3::x
	int32_t ___x_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3::y
	int32_t ___y_1;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3_RawEncryptedVector3::z
	int32_t ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWENCRYPTEDVECTOR3_T766A78183852F7A89484E372DBFD15EEF76AB627_H
#ifndef RAWENCRYPTEDVECTOR3INT_TFC038D95C234B20C841F481FAB1B435C389129D3_H
#define RAWENCRYPTEDVECTOR3INT_TFC038D95C234B20C841F481FAB1B435C389129D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int_RawEncryptedVector3Int
struct  RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int_RawEncryptedVector3Int::x
	int32_t ___x_0;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int_RawEncryptedVector3Int::y
	int32_t ___y_1;
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int_RawEncryptedVector3Int::z
	int32_t ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWENCRYPTEDVECTOR3INT_TFC038D95C234B20C841F481FAB1B435C389129D3_H
#ifndef COLOR2PLUGIN_T87D37EAA5A6D3CC562863B155AFA3162B5D41743_H
#define COLOR2PLUGIN_T87D37EAA5A6D3CC562863B155AFA3162B5D41743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Color2Plugin
struct  Color2Plugin_t87D37EAA5A6D3CC562863B155AFA3162B5D41743  : public ABSTweenPlugin_3_tB581A16037BDC1101611BFD35AF866E59472559A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR2PLUGIN_T87D37EAA5A6D3CC562863B155AFA3162B5D41743_H
#ifndef COLORPLUGIN_TFE42FCE0666DDBE27D3ED749E5198833F49BBF90_H
#define COLORPLUGIN_TFE42FCE0666DDBE27D3ED749E5198833F49BBF90_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.ColorPlugin
struct  ColorPlugin_tFE42FCE0666DDBE27D3ED749E5198833F49BBF90  : public ABSTweenPlugin_3_tD58649751AD5C680679FE9F34E72C693082D2950
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPLUGIN_TFE42FCE0666DDBE27D3ED749E5198833F49BBF90_H
#ifndef DOUBLEPLUGIN_T516B4EF13BB0F79EA5BEDC15F58823017C8F2418_H
#define DOUBLEPLUGIN_T516B4EF13BB0F79EA5BEDC15F58823017C8F2418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.DoublePlugin
struct  DoublePlugin_t516B4EF13BB0F79EA5BEDC15F58823017C8F2418  : public ABSTweenPlugin_3_t421856DF2C12FA3EB476B9DFF14D0176AC9148EB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEPLUGIN_T516B4EF13BB0F79EA5BEDC15F58823017C8F2418_H
#ifndef FLOATPLUGIN_TBB4C2318EBF50EC6568B3A9CB5AB3A1ACE1BA159_H
#define FLOATPLUGIN_TBB4C2318EBF50EC6568B3A9CB5AB3A1ACE1BA159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.FloatPlugin
struct  FloatPlugin_tBB4C2318EBF50EC6568B3A9CB5AB3A1ACE1BA159  : public ABSTweenPlugin_3_t98C01ECFC5E278EFEB1AEF3512475A4604A0AFF5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPLUGIN_TBB4C2318EBF50EC6568B3A9CB5AB3A1ACE1BA159_H
#ifndef INTPLUGIN_TAA40FA89977341EE1DE1D65ECCA0C1A2D9B05B50_H
#define INTPLUGIN_TAA40FA89977341EE1DE1D65ECCA0C1A2D9B05B50_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.IntPlugin
struct  IntPlugin_tAA40FA89977341EE1DE1D65ECCA0C1A2D9B05B50  : public ABSTweenPlugin_3_tC3773F7BBD824370E35F778AC9CBB6D541CF11B6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPLUGIN_TAA40FA89977341EE1DE1D65ECCA0C1A2D9B05B50_H
#ifndef LONGPLUGIN_T60BD2312BADD30C1AAC88D067CE7864D0CE63B4E_H
#define LONGPLUGIN_T60BD2312BADD30C1AAC88D067CE7864D0CE63B4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.LongPlugin
struct  LongPlugin_t60BD2312BADD30C1AAC88D067CE7864D0CE63B4E  : public ABSTweenPlugin_3_t18BE0A47C2F614B13A6B50635351658E9D666DA1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGPLUGIN_T60BD2312BADD30C1AAC88D067CE7864D0CE63B4E_H
#ifndef COLOROPTIONS_TD57E512D2F4329BEE7EAEE0D1B2C4A0683D288DA_H
#define COLOROPTIONS_TD57E512D2F4329BEE7EAEE0D1B2C4A0683D288DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.ColorOptions
struct  ColorOptions_tD57E512D2F4329BEE7EAEE0D1B2C4A0683D288DA 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.ColorOptions::alphaOnly
	bool ___alphaOnly_0;

public:
	inline static int32_t get_offset_of_alphaOnly_0() { return static_cast<int32_t>(offsetof(ColorOptions_tD57E512D2F4329BEE7EAEE0D1B2C4A0683D288DA, ___alphaOnly_0)); }
	inline bool get_alphaOnly_0() const { return ___alphaOnly_0; }
	inline bool* get_address_of_alphaOnly_0() { return &___alphaOnly_0; }
	inline void set_alphaOnly_0(bool value)
	{
		___alphaOnly_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_tD57E512D2F4329BEE7EAEE0D1B2C4A0683D288DA_marshaled_pinvoke
{
	int32_t ___alphaOnly_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_tD57E512D2F4329BEE7EAEE0D1B2C4A0683D288DA_marshaled_com
{
	int32_t ___alphaOnly_0;
};
#endif // COLOROPTIONS_TD57E512D2F4329BEE7EAEE0D1B2C4A0683D288DA_H
#ifndef FLOATOPTIONS_T7285C3D13285197B6B003786B85DAAD83E654C1B_H
#define FLOATOPTIONS_T7285C3D13285197B6B003786B85DAAD83E654C1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.FloatOptions
struct  FloatOptions_t7285C3D13285197B6B003786B85DAAD83E654C1B 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.FloatOptions::snapping
	bool ___snapping_0;

public:
	inline static int32_t get_offset_of_snapping_0() { return static_cast<int32_t>(offsetof(FloatOptions_t7285C3D13285197B6B003786B85DAAD83E654C1B, ___snapping_0)); }
	inline bool get_snapping_0() const { return ___snapping_0; }
	inline bool* get_address_of_snapping_0() { return &___snapping_0; }
	inline void set_snapping_0(bool value)
	{
		___snapping_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t7285C3D13285197B6B003786B85DAAD83E654C1B_marshaled_pinvoke
{
	int32_t ___snapping_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t7285C3D13285197B6B003786B85DAAD83E654C1B_marshaled_com
{
	int32_t ___snapping_0;
};
#endif // FLOATOPTIONS_T7285C3D13285197B6B003786B85DAAD83E654C1B_H
#ifndef NOOPTIONS_TC312CAA4CCC3BD59EC7E6930F0E08272608A5DE2_H
#define NOOPTIONS_TC312CAA4CCC3BD59EC7E6930F0E08272608A5DE2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.NoOptions
struct  NoOptions_tC312CAA4CCC3BD59EC7E6930F0E08272608A5DE2 
{
public:
	union
	{
		struct
		{
		};
		uint8_t NoOptions_tC312CAA4CCC3BD59EC7E6930F0E08272608A5DE2__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOOPTIONS_TC312CAA4CCC3BD59EC7E6930F0E08272608A5DE2_H
#ifndef RECTOPTIONS_T1C4579A85851F16B3300F373FC807B4913D59A5E_H
#define RECTOPTIONS_T1C4579A85851F16B3300F373FC807B4913D59A5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.RectOptions
struct  RectOptions_t1C4579A85851F16B3300F373FC807B4913D59A5E 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.RectOptions::snapping
	bool ___snapping_0;

public:
	inline static int32_t get_offset_of_snapping_0() { return static_cast<int32_t>(offsetof(RectOptions_t1C4579A85851F16B3300F373FC807B4913D59A5E, ___snapping_0)); }
	inline bool get_snapping_0() const { return ___snapping_0; }
	inline bool* get_address_of_snapping_0() { return &___snapping_0; }
	inline void set_snapping_0(bool value)
	{
		___snapping_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.RectOptions
struct RectOptions_t1C4579A85851F16B3300F373FC807B4913D59A5E_marshaled_pinvoke
{
	int32_t ___snapping_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.RectOptions
struct RectOptions_t1C4579A85851F16B3300F373FC807B4913D59A5E_marshaled_com
{
	int32_t ___snapping_0;
};
#endif // RECTOPTIONS_T1C4579A85851F16B3300F373FC807B4913D59A5E_H
#ifndef UINTOPTIONS_TCD241771582D5159D0129AA82E86590D0A0FC1E6_H
#define UINTOPTIONS_TCD241771582D5159D0129AA82E86590D0A0FC1E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.UintOptions
struct  UintOptions_tCD241771582D5159D0129AA82E86590D0A0FC1E6 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.UintOptions::isNegativeChangeValue
	bool ___isNegativeChangeValue_0;

public:
	inline static int32_t get_offset_of_isNegativeChangeValue_0() { return static_cast<int32_t>(offsetof(UintOptions_tCD241771582D5159D0129AA82E86590D0A0FC1E6, ___isNegativeChangeValue_0)); }
	inline bool get_isNegativeChangeValue_0() const { return ___isNegativeChangeValue_0; }
	inline bool* get_address_of_isNegativeChangeValue_0() { return &___isNegativeChangeValue_0; }
	inline void set_isNegativeChangeValue_0(bool value)
	{
		___isNegativeChangeValue_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.UintOptions
struct UintOptions_tCD241771582D5159D0129AA82E86590D0A0FC1E6_marshaled_pinvoke
{
	int32_t ___isNegativeChangeValue_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.UintOptions
struct UintOptions_tCD241771582D5159D0129AA82E86590D0A0FC1E6_marshaled_com
{
	int32_t ___isNegativeChangeValue_0;
};
#endif // UINTOPTIONS_TCD241771582D5159D0129AA82E86590D0A0FC1E6_H
#ifndef QUATERNIONPLUGIN_T77E2981A5FA8AD92D23F2EB7735F6EA8A3992E1E_H
#define QUATERNIONPLUGIN_T77E2981A5FA8AD92D23F2EB7735F6EA8A3992E1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.QuaternionPlugin
struct  QuaternionPlugin_t77E2981A5FA8AD92D23F2EB7735F6EA8A3992E1E  : public ABSTweenPlugin_3_tE10F18828A3B58083EDF713736377A9F10B2CEC5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONPLUGIN_T77E2981A5FA8AD92D23F2EB7735F6EA8A3992E1E_H
#ifndef RECTOFFSETPLUGIN_T2250B1BA1D4AE2E236ED10428CDD0F1B0B13ADBF_H
#define RECTOFFSETPLUGIN_T2250B1BA1D4AE2E236ED10428CDD0F1B0B13ADBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.RectOffsetPlugin
struct  RectOffsetPlugin_t2250B1BA1D4AE2E236ED10428CDD0F1B0B13ADBF  : public ABSTweenPlugin_3_t68925870D981F1013657936919AE334797F43A6C
{
public:

public:
};

struct RectOffsetPlugin_t2250B1BA1D4AE2E236ED10428CDD0F1B0B13ADBF_StaticFields
{
public:
	// UnityEngine.RectOffset DG.Tweening.Plugins.RectOffsetPlugin::_r
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ____r_0;

public:
	inline static int32_t get_offset_of__r_0() { return static_cast<int32_t>(offsetof(RectOffsetPlugin_t2250B1BA1D4AE2E236ED10428CDD0F1B0B13ADBF_StaticFields, ____r_0)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get__r_0() const { return ____r_0; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of__r_0() { return &____r_0; }
	inline void set__r_0(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		____r_0 = value;
		Il2CppCodeGenWriteBarrier((&____r_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTOFFSETPLUGIN_T2250B1BA1D4AE2E236ED10428CDD0F1B0B13ADBF_H
#ifndef RECTPLUGIN_T85D2E439D5B68F1483299B2730FAC9C22F95DA4F_H
#define RECTPLUGIN_T85D2E439D5B68F1483299B2730FAC9C22F95DA4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.RectPlugin
struct  RectPlugin_t85D2E439D5B68F1483299B2730FAC9C22F95DA4F  : public ABSTweenPlugin_3_tEB25032AFD776502392FFF377631C0A006EED8C4
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTPLUGIN_T85D2E439D5B68F1483299B2730FAC9C22F95DA4F_H
#ifndef STRINGPLUGIN_T5645A3CC39BB2ADAD92EBEFDA75EB3F3C6ABF97D_H
#define STRINGPLUGIN_T5645A3CC39BB2ADAD92EBEFDA75EB3F3C6ABF97D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.StringPlugin
struct  StringPlugin_t5645A3CC39BB2ADAD92EBEFDA75EB3F3C6ABF97D  : public ABSTweenPlugin_3_t9D226D0B072F9EA7690FA7A649856709CE00D725
{
public:

public:
};

struct StringPlugin_t5645A3CC39BB2ADAD92EBEFDA75EB3F3C6ABF97D_StaticFields
{
public:
	// System.Text.StringBuilder DG.Tweening.Plugins.StringPlugin::_Buffer
	StringBuilder_t * ____Buffer_0;
	// System.Collections.Generic.List`1<System.Char> DG.Tweening.Plugins.StringPlugin::_OpenedTags
	List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * ____OpenedTags_1;

public:
	inline static int32_t get_offset_of__Buffer_0() { return static_cast<int32_t>(offsetof(StringPlugin_t5645A3CC39BB2ADAD92EBEFDA75EB3F3C6ABF97D_StaticFields, ____Buffer_0)); }
	inline StringBuilder_t * get__Buffer_0() const { return ____Buffer_0; }
	inline StringBuilder_t ** get_address_of__Buffer_0() { return &____Buffer_0; }
	inline void set__Buffer_0(StringBuilder_t * value)
	{
		____Buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____Buffer_0), value);
	}

	inline static int32_t get_offset_of__OpenedTags_1() { return static_cast<int32_t>(offsetof(StringPlugin_t5645A3CC39BB2ADAD92EBEFDA75EB3F3C6ABF97D_StaticFields, ____OpenedTags_1)); }
	inline List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * get__OpenedTags_1() const { return ____OpenedTags_1; }
	inline List_1_t028AAE01C4834286B7892F4498364F964CD8B316 ** get_address_of__OpenedTags_1() { return &____OpenedTags_1; }
	inline void set__OpenedTags_1(List_1_t028AAE01C4834286B7892F4498364F964CD8B316 * value)
	{
		____OpenedTags_1 = value;
		Il2CppCodeGenWriteBarrier((&____OpenedTags_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGPLUGIN_T5645A3CC39BB2ADAD92EBEFDA75EB3F3C6ABF97D_H
#ifndef UINTPLUGIN_TE4DCBED54C91BABBCA72DC9CD645347DE9F2C58F_H
#define UINTPLUGIN_TE4DCBED54C91BABBCA72DC9CD645347DE9F2C58F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.UintPlugin
struct  UintPlugin_tE4DCBED54C91BABBCA72DC9CD645347DE9F2C58F  : public ABSTweenPlugin_3_tF49BD32818EDBBDC078E7E8BAD2AD193D74B8489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTPLUGIN_TE4DCBED54C91BABBCA72DC9CD645347DE9F2C58F_H
#ifndef ULONGPLUGIN_T96C49ADC71F577E6EFA028F1EEE64261327A328F_H
#define ULONGPLUGIN_T96C49ADC71F577E6EFA028F1EEE64261327A328F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.UlongPlugin
struct  UlongPlugin_t96C49ADC71F577E6EFA028F1EEE64261327A328F  : public ABSTweenPlugin_3_tD282AD5EB6CAA3FCACDF51432ACC1C2E51777D01
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ULONGPLUGIN_T96C49ADC71F577E6EFA028F1EEE64261327A328F_H
#ifndef VECTOR2PLUGIN_T8900F71F5DA563C8D8FA687C9F653A236ECD2F09_H
#define VECTOR2PLUGIN_T8900F71F5DA563C8D8FA687C9F653A236ECD2F09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Vector2Plugin
struct  Vector2Plugin_t8900F71F5DA563C8D8FA687C9F653A236ECD2F09  : public ABSTweenPlugin_3_tF0F62B2C3E037FA76B2A6352C4C0E6C8B2D668FD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2PLUGIN_T8900F71F5DA563C8D8FA687C9F653A236ECD2F09_H
#ifndef VECTOR3ARRAYPLUGIN_T8254C41091DE113A1FB5BDCE91F97600348626EC_H
#define VECTOR3ARRAYPLUGIN_T8254C41091DE113A1FB5BDCE91F97600348626EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Vector3ArrayPlugin
struct  Vector3ArrayPlugin_t8254C41091DE113A1FB5BDCE91F97600348626EC  : public ABSTweenPlugin_3_t59C22DD36F4259810DAE41F464AC73D990C88056
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ARRAYPLUGIN_T8254C41091DE113A1FB5BDCE91F97600348626EC_H
#ifndef VECTOR3PLUGIN_T1B9F5C4A304353A9D68B9182A8DE1DDAE3E2C4F9_H
#define VECTOR3PLUGIN_T1B9F5C4A304353A9D68B9182A8DE1DDAE3E2C4F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Vector3Plugin
struct  Vector3Plugin_t1B9F5C4A304353A9D68B9182A8DE1DDAE3E2C4F9  : public ABSTweenPlugin_3_t50462951CA2C372D1A9BC7C38C6ECC1810944678
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3PLUGIN_T1B9F5C4A304353A9D68B9182A8DE1DDAE3E2C4F9_H
#ifndef VECTOR4PLUGIN_T7DE745F65D4FEBD8FBFECE97FE34F9592612B325_H
#define VECTOR4PLUGIN_T7DE745F65D4FEBD8FBFECE97FE34F9592612B325_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Vector4Plugin
struct  Vector4Plugin_t7DE745F65D4FEBD8FBFECE97FE34F9592612B325  : public ABSTweenPlugin_3_t224A02B0B2F704CE5A82C0EC7719C0ADC0B382DD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4PLUGIN_T7DE745F65D4FEBD8FBFECE97FE34F9592612B325_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#define ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateMachine_0), value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_defaultContextAction_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultContextAction_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
#endif // ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifndef TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#define TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter
struct  TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F 
{
public:
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.TaskAwaiter::m_task
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F, ___m_task_0)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_marshaled_pinvoke
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.TaskAwaiter
struct TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_marshaled_com
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
};
#endif // TASKAWAITER_T0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F_H
#ifndef TASKAWAITER_1_TB004D5745F8C75ED93C95DCDB555E46DE24DF765_H
#define TASKAWAITER_1_TB004D5745F8C75ED93C95DCDB555E46DE24DF765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter`1<CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeResult>
struct  TaskAwaiter_1_tB004D5745F8C75ED93C95DCDB555E46DE24DF765 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.TaskAwaiter`1::m_task
	Task_1_t957C3A34CCCE85454E68B7321A390F0720FD14B2 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_1_tB004D5745F8C75ED93C95DCDB555E46DE24DF765, ___m_task_0)); }
	inline Task_1_t957C3A34CCCE85454E68B7321A390F0720FD14B2 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t957C3A34CCCE85454E68B7321A390F0720FD14B2 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t957C3A34CCCE85454E68B7321A390F0720FD14B2 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKAWAITER_1_TB004D5745F8C75ED93C95DCDB555E46DE24DF765_H
#ifndef SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#define SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR3INT_TA843C5F8C2EB42492786C5AF82C3E1F4929942B4_H
#define VECTOR3INT_TA843C5F8C2EB42492786C5AF82C3E1F4929942B4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3Int
struct  Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 
{
public:
	// System.Int32 UnityEngine.Vector3Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector3Int::m_Y
	int32_t ___m_Y_1;
	// System.Int32 UnityEngine.Vector3Int::m_Z
	int32_t ___m_Z_2;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}

	inline static int32_t get_offset_of_m_Z_2() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4, ___m_Z_2)); }
	inline int32_t get_m_Z_2() const { return ___m_Z_2; }
	inline int32_t* get_address_of_m_Z_2() { return &___m_Z_2; }
	inline void set_m_Z_2(int32_t value)
	{
		___m_Z_2 = value;
	}
};

struct Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields
{
public:
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Zero
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___s_Zero_3;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_One
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___s_One_4;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Up
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___s_Up_5;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Down
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___s_Down_6;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Left
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___s_Left_7;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Right
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___s_Right_8;

public:
	inline static int32_t get_offset_of_s_Zero_3() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields, ___s_Zero_3)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_s_Zero_3() const { return ___s_Zero_3; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_s_Zero_3() { return &___s_Zero_3; }
	inline void set_s_Zero_3(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___s_Zero_3 = value;
	}

	inline static int32_t get_offset_of_s_One_4() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields, ___s_One_4)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_s_One_4() const { return ___s_One_4; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_s_One_4() { return &___s_One_4; }
	inline void set_s_One_4(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___s_One_4 = value;
	}

	inline static int32_t get_offset_of_s_Up_5() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields, ___s_Up_5)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_s_Up_5() const { return ___s_Up_5; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_s_Up_5() { return &___s_Up_5; }
	inline void set_s_Up_5(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___s_Up_5 = value;
	}

	inline static int32_t get_offset_of_s_Down_6() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields, ___s_Down_6)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_s_Down_6() const { return ___s_Down_6; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_s_Down_6() { return &___s_Down_6; }
	inline void set_s_Down_6(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___s_Down_6 = value;
	}

	inline static int32_t get_offset_of_s_Left_7() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields, ___s_Left_7)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_s_Left_7() const { return ___s_Left_7; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_s_Left_7() { return &___s_Left_7; }
	inline void set_s_Left_7(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___s_Left_7 = value;
	}

	inline static int32_t get_offset_of_s_Right_8() { return static_cast<int32_t>(offsetof(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4_StaticFields, ___s_Right_8)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_s_Right_8() const { return ___s_Right_8; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_s_Right_8() { return &___s_Right_8; }
	inline void set_s_Right_8(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___s_Right_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3INT_TA843C5F8C2EB42492786C5AF82C3E1F4929942B4_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T0A0BB8AE918C8DC79883BB225E227F5C7FE0A10A_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T0A0BB8AE918C8DC79883BB225E227F5C7FE0A10A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t0A0BB8AE918C8DC79883BB225E227F5C7FE0A10A  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t0A0BB8AE918C8DC79883BB225E227F5C7FE0A10A_StaticFields
{
public:
	// <PrivateImplementationDetails>___StaticArrayInitTypeSizeU3D10 <PrivateImplementationDetails>::33DC1ECDFFAB5B7840492718281B58ED2B7C1DB2
	__StaticArrayInitTypeSizeU3D10_t85C6225FBC4C8C59B85DFE15371916C1912F4837  ___33DC1ECDFFAB5B7840492718281B58ED2B7C1DB2_0;

public:
	inline static int32_t get_offset_of_U333DC1ECDFFAB5B7840492718281B58ED2B7C1DB2_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t0A0BB8AE918C8DC79883BB225E227F5C7FE0A10A_StaticFields, ___33DC1ECDFFAB5B7840492718281B58ED2B7C1DB2_0)); }
	inline __StaticArrayInitTypeSizeU3D10_t85C6225FBC4C8C59B85DFE15371916C1912F4837  get_U333DC1ECDFFAB5B7840492718281B58ED2B7C1DB2_0() const { return ___33DC1ECDFFAB5B7840492718281B58ED2B7C1DB2_0; }
	inline __StaticArrayInitTypeSizeU3D10_t85C6225FBC4C8C59B85DFE15371916C1912F4837 * get_address_of_U333DC1ECDFFAB5B7840492718281B58ED2B7C1DB2_0() { return &___33DC1ECDFFAB5B7840492718281B58ED2B7C1DB2_0; }
	inline void set_U333DC1ECDFFAB5B7840492718281B58ED2B7C1DB2_0(__StaticArrayInitTypeSizeU3D10_t85C6225FBC4C8C59B85DFE15371916C1912F4837  value)
	{
		___33DC1ECDFFAB5B7840492718281B58ED2B7C1DB2_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T0A0BB8AE918C8DC79883BB225E227F5C7FE0A10A_H
#ifndef CHECKRESULT_T5370C9CFB3349644677B3E4598C26C274743486D_H
#define CHECKRESULT_T5370C9CFB3349644677B3E4598C26C274743486D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_CheckResult
struct  CheckResult_t5370C9CFB3349644677B3E4598C26C274743486D 
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector_CheckResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CheckResult_t5370C9CFB3349644677B3E4598C26C274743486D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHECKRESULT_T5370C9CFB3349644677B3E4598C26C274743486D_H
#ifndef ERRORKIND_T70F1939191471D9D9CD977DC0DD9E55582AE2B44_H
#define ERRORKIND_T70F1939191471D9D9CD977DC0DD9E55582AE2B44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_ErrorKind
struct  ErrorKind_t70F1939191471D9D9CD977DC0DD9E55582AE2B44 
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector_ErrorKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ErrorKind_t70F1939191471D9D9CD977DC0DD9E55582AE2B44, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORKIND_T70F1939191471D9D9CD977DC0DD9E55582AE2B44_H
#ifndef REQUESTMETHOD_T8184DA00E2BEEDEE288C2F1EFE117D515B8F6C35_H
#define REQUESTMETHOD_T8184DA00E2BEEDEE288C2F1EFE117D515B8F6C35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_RequestMethod
struct  RequestMethod_t8184DA00E2BEEDEE288C2F1EFE117D515B8F6C35 
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector_RequestMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RequestMethod_t8184DA00E2BEEDEE288C2F1EFE117D515B8F6C35, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTMETHOD_T8184DA00E2BEEDEE288C2F1EFE117D515B8F6C35_H
#ifndef OBSCUREDVECTOR3INT_T4E47D68D29A1D228D29EC0300213FEB7D891C0BD_H
#define OBSCUREDVECTOR3INT_T4E47D68D29A1D228D29EC0300213FEB7D891C0BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int
struct  ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD 
{
public:
	// System.Int32 CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int::currentCryptoKey
	int32_t ___currentCryptoKey_1;
	// CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int_RawEncryptedVector3Int CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int::hiddenValue
	RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3  ___hiddenValue_2;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int::inited
	bool ___inited_3;
	// UnityEngine.Vector3Int CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int::fakeValue
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___fakeValue_4;
	// System.Boolean CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int::fakeValueActive
	bool ___fakeValueActive_5;

public:
	inline static int32_t get_offset_of_currentCryptoKey_1() { return static_cast<int32_t>(offsetof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD, ___currentCryptoKey_1)); }
	inline int32_t get_currentCryptoKey_1() const { return ___currentCryptoKey_1; }
	inline int32_t* get_address_of_currentCryptoKey_1() { return &___currentCryptoKey_1; }
	inline void set_currentCryptoKey_1(int32_t value)
	{
		___currentCryptoKey_1 = value;
	}

	inline static int32_t get_offset_of_hiddenValue_2() { return static_cast<int32_t>(offsetof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD, ___hiddenValue_2)); }
	inline RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3  get_hiddenValue_2() const { return ___hiddenValue_2; }
	inline RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3 * get_address_of_hiddenValue_2() { return &___hiddenValue_2; }
	inline void set_hiddenValue_2(RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3  value)
	{
		___hiddenValue_2 = value;
	}

	inline static int32_t get_offset_of_inited_3() { return static_cast<int32_t>(offsetof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD, ___inited_3)); }
	inline bool get_inited_3() const { return ___inited_3; }
	inline bool* get_address_of_inited_3() { return &___inited_3; }
	inline void set_inited_3(bool value)
	{
		___inited_3 = value;
	}

	inline static int32_t get_offset_of_fakeValue_4() { return static_cast<int32_t>(offsetof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD, ___fakeValue_4)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_fakeValue_4() const { return ___fakeValue_4; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_fakeValue_4() { return &___fakeValue_4; }
	inline void set_fakeValue_4(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___fakeValue_4 = value;
	}

	inline static int32_t get_offset_of_fakeValueActive_5() { return static_cast<int32_t>(offsetof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD, ___fakeValueActive_5)); }
	inline bool get_fakeValueActive_5() const { return ___fakeValueActive_5; }
	inline bool* get_address_of_fakeValueActive_5() { return &___fakeValueActive_5; }
	inline void set_fakeValueActive_5(bool value)
	{
		___fakeValueActive_5 = value;
	}
};

struct ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD_StaticFields
{
public:
	// UnityEngine.Vector3Int CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int::Zero
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD_StaticFields, ___Zero_0)); }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  get_Zero_0() const { return ___Zero_0; }
	inline Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int
struct ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD_marshaled_pinvoke
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
// Native definition for COM marshalling of CodeStage.AntiCheat.ObscuredTypes.ObscuredVector3Int
struct ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD_marshaled_com
{
	int32_t ___currentCryptoKey_1;
	RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3  ___hiddenValue_2;
	int32_t ___inited_3;
	Vector3Int_tA843C5F8C2EB42492786C5AF82C3E1F4929942B4  ___fakeValue_4;
	int32_t ___fakeValueActive_5;
};
#endif // OBSCUREDVECTOR3INT_T4E47D68D29A1D228D29EC0300213FEB7D891C0BD_H
#ifndef AUTOPLAY_T17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12_H
#define AUTOPLAY_T17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AutoPlay
struct  AutoPlay_t17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12 
{
public:
	// System.Int32 DG.Tweening.AutoPlay::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AutoPlay_t17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOPLAY_T17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12_H
#ifndef AXISCONSTRAINT_T6929580E28515C207FCC805981F73EC49354E48A_H
#define AXISCONSTRAINT_T6929580E28515C207FCC805981F73EC49354E48A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AxisConstraint
struct  AxisConstraint_t6929580E28515C207FCC805981F73EC49354E48A 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisConstraint_t6929580E28515C207FCC805981F73EC49354E48A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCONSTRAINT_T6929580E28515C207FCC805981F73EC49354E48A_H
#ifndef COLOR2_T04D7DADEE8440A02309444A79F7FCD19FAD7313E_H
#define COLOR2_T04D7DADEE8440A02309444A79F7FCD19FAD7313E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Color2
struct  Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E 
{
public:
	// UnityEngine.Color DG.Tweening.Color2::ca
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___ca_0;
	// UnityEngine.Color DG.Tweening.Color2::cb
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___cb_1;

public:
	inline static int32_t get_offset_of_ca_0() { return static_cast<int32_t>(offsetof(Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E, ___ca_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_ca_0() const { return ___ca_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_ca_0() { return &___ca_0; }
	inline void set_ca_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___ca_0 = value;
	}

	inline static int32_t get_offset_of_cb_1() { return static_cast<int32_t>(offsetof(Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E, ___cb_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_cb_1() const { return ___cb_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_cb_1() { return &___cb_1; }
	inline void set_cb_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___cb_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR2_T04D7DADEE8440A02309444A79F7FCD19FAD7313E_H
#ifndef SPECIALSTARTUPMODE_TE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49_H
#define SPECIALSTARTUPMODE_TE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_tE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialStartupMode_tE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALSTARTUPMODE_TE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49_H
#ifndef EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#define EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_TC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E_H
#ifndef LOGBEHAVIOUR_TD1A4AACD65E9C63844373CF59B8E9A576814F813_H
#define LOGBEHAVIOUR_TD1A4AACD65E9C63844373CF59B8E9A576814F813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LogBehaviour
struct  LogBehaviour_tD1A4AACD65E9C63844373CF59B8E9A576814F813 
{
public:
	// System.Int32 DG.Tweening.LogBehaviour::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogBehaviour_tD1A4AACD65E9C63844373CF59B8E9A576814F813, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGBEHAVIOUR_TD1A4AACD65E9C63844373CF59B8E9A576814F813_H
#ifndef LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#define LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T1F864F717700724AC772E79CCF167C3C44D6EE1A_H
#ifndef ROTATEMODE_T7DB9BF2546AEC18F78B34B2B7111AED4416368C7_H
#define ROTATEMODE_T7DB9BF2546AEC18F78B34B2B7111AED4416368C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.RotateMode
struct  RotateMode_t7DB9BF2546AEC18F78B34B2B7111AED4416368C7 
{
public:
	// System.Int32 DG.Tweening.RotateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotateMode_t7DB9BF2546AEC18F78B34B2B7111AED4416368C7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEMODE_T7DB9BF2546AEC18F78B34B2B7111AED4416368C7_H
#ifndef SCRAMBLEMODE_TB2CAB82E2A9149E917AF3B1261C86133CBF83551_H
#define SCRAMBLEMODE_TB2CAB82E2A9149E917AF3B1261C86133CBF83551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ScrambleMode
struct  ScrambleMode_tB2CAB82E2A9149E917AF3B1261C86133CBF83551 
{
public:
	// System.Int32 DG.Tweening.ScrambleMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScrambleMode_tB2CAB82E2A9149E917AF3B1261C86133CBF83551, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRAMBLEMODE_TB2CAB82E2A9149E917AF3B1261C86133CBF83551_H
#ifndef U3CU3EC__DISPLAYCLASS78_0_TFE1008EA46A16601A58722DD5444AC85B0398BC0_H
#define U3CU3EC__DISPLAYCLASS78_0_TFE1008EA46A16601A58722DD5444AC85B0398BC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions_<>c__DisplayClass78_0
struct  U3CU3Ec__DisplayClass78_0_tFE1008EA46A16601A58722DD5444AC85B0398BC0  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions_<>c__DisplayClass78_0::to
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions_<>c__DisplayClass78_0::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass78_0_tFE1008EA46A16601A58722DD5444AC85B0398BC0, ___to_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_to_0() const { return ___to_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass78_0_tFE1008EA46A16601A58722DD5444AC85B0398BC0, ___target_1)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_1() const { return ___target_1; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS78_0_TFE1008EA46A16601A58722DD5444AC85B0398BC0_H
#ifndef TWEENTYPE_T13FCD3DFD7DD8AE49EA4A7BA70695D3936EAF71F_H
#define TWEENTYPE_T13FCD3DFD7DD8AE49EA4A7BA70695D3936EAF71F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenType
struct  TweenType_t13FCD3DFD7DD8AE49EA4A7BA70695D3936EAF71F 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TweenType_t13FCD3DFD7DD8AE49EA4A7BA70695D3936EAF71F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENTYPE_T13FCD3DFD7DD8AE49EA4A7BA70695D3936EAF71F_H
#ifndef UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#define UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T0EBAB258B7D72D4959A3835E554B7958CAFDAF63_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef ASYNCTASKMETHODBUILDER_1_T886883EFB3044B262A59B607B6A7FF6E4F7C91BA_H
#define ASYNCTASKMETHODBUILDER_1_T886883EFB3044B262A59B607B6A7FF6E4F7C91BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<CodeStage.AntiCheat.Detectors.TimeCheatingDetector_CheckResult>
struct  AsyncTaskMethodBuilder_1_t886883EFB3044B262A59B607B6A7FF6E4F7C91BA 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t8134682F4345785A873A6E69078B0B53B9DB89B8 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t886883EFB3044B262A59B607B6A7FF6E4F7C91BA, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t886883EFB3044B262A59B607B6A7FF6E4F7C91BA, ___m_task_2)); }
	inline Task_1_t8134682F4345785A873A6E69078B0B53B9DB89B8 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t8134682F4345785A873A6E69078B0B53B9DB89B8 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t8134682F4345785A873A6E69078B0B53B9DB89B8 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t886883EFB3044B262A59B607B6A7FF6E4F7C91BA_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t8134682F4345785A873A6E69078B0B53B9DB89B8 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t886883EFB3044B262A59B607B6A7FF6E4F7C91BA_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t8134682F4345785A873A6E69078B0B53B9DB89B8 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t8134682F4345785A873A6E69078B0B53B9DB89B8 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t8134682F4345785A873A6E69078B0B53B9DB89B8 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T886883EFB3044B262A59B607B6A7FF6E4F7C91BA_H
#ifndef ASYNCTASKMETHODBUILDER_1_T45F719F86664019FBC618D0C68C40492938FB756_H
#define ASYNCTASKMETHODBUILDER_1_T45F719F86664019FBC618D0C68C40492938FB756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeResult>
struct  AsyncTaskMethodBuilder_1_t45F719F86664019FBC618D0C68C40492938FB756 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t957C3A34CCCE85454E68B7321A390F0720FD14B2 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t45F719F86664019FBC618D0C68C40492938FB756, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t45F719F86664019FBC618D0C68C40492938FB756, ___m_task_2)); }
	inline Task_1_t957C3A34CCCE85454E68B7321A390F0720FD14B2 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t957C3A34CCCE85454E68B7321A390F0720FD14B2 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t957C3A34CCCE85454E68B7321A390F0720FD14B2 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t45F719F86664019FBC618D0C68C40492938FB756_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t957C3A34CCCE85454E68B7321A390F0720FD14B2 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t45F719F86664019FBC618D0C68C40492938FB756_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t957C3A34CCCE85454E68B7321A390F0720FD14B2 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t957C3A34CCCE85454E68B7321A390F0720FD14B2 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t957C3A34CCCE85454E68B7321A390F0720FD14B2 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T45F719F86664019FBC618D0C68C40492938FB756_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef U3CFORCECHECKTASKU3ED__59_T438175677D47B411599FF48D0FFA21895E75F050_H
#define U3CFORCECHECKTASKU3ED__59_T438175677D47B411599FF48D0FFA21895E75F050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<ForceCheckTask>d__59
struct  U3CForceCheckTaskU3Ed__59_t438175677D47B411599FF48D0FFA21895E75F050 
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<ForceCheckTask>d__59::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<CodeStage.AntiCheat.Detectors.TimeCheatingDetector_CheckResult> CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<ForceCheckTask>d__59::<>t__builder
	AsyncTaskMethodBuilder_1_t886883EFB3044B262A59B607B6A7FF6E4F7C91BA  ___U3CU3Et__builder_1;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<ForceCheckTask>d__59::<>4__this
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 * ___U3CU3E4__this_2;
	// System.Runtime.CompilerServices.TaskAwaiter CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<ForceCheckTask>d__59::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CForceCheckTaskU3Ed__59_t438175677D47B411599FF48D0FFA21895E75F050, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CForceCheckTaskU3Ed__59_t438175677D47B411599FF48D0FFA21895E75F050, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t886883EFB3044B262A59B607B6A7FF6E4F7C91BA  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t886883EFB3044B262A59B607B6A7FF6E4F7C91BA * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t886883EFB3044B262A59B607B6A7FF6E4F7C91BA  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CForceCheckTaskU3Ed__59_t438175677D47B411599FF48D0FFA21895E75F050, ___U3CU3E4__this_2)); }
	inline TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3CForceCheckTaskU3Ed__59_t438175677D47B411599FF48D0FFA21895E75F050, ___U3CU3Eu__1_3)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFORCECHECKTASKU3ED__59_T438175677D47B411599FF48D0FFA21895E75F050_H
#ifndef U3CGETONLINETIMECOROUTINEU3ED__49_TD7F6B839D41FBC936A43BDAE662892DB310F8ED8_H
#define U3CGETONLINETIMECOROUTINEU3ED__49_TD7F6B839D41FBC936A43BDAE662892DB310F8ED8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__49
struct  U3CGetOnlineTimeCoroutineU3Ed__49_tD7F6B839D41FBC936A43BDAE662892DB310F8ED8  : public RuntimeObject
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__49::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__49::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__49::url
	String_t* ___url_2;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeCallback CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__49::callback
	OnlineTimeCallback_t5AA2B75F108EB462BC1D2C2E38CF960AE3434D34 * ___callback_3;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_RequestMethod CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__49::method
	int32_t ___method_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeCoroutineU3Ed__49_tD7F6B839D41FBC936A43BDAE662892DB310F8ED8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeCoroutineU3Ed__49_tD7F6B839D41FBC936A43BDAE662892DB310F8ED8, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeCoroutineU3Ed__49_tD7F6B839D41FBC936A43BDAE662892DB310F8ED8, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeCoroutineU3Ed__49_tD7F6B839D41FBC936A43BDAE662892DB310F8ED8, ___callback_3)); }
	inline OnlineTimeCallback_t5AA2B75F108EB462BC1D2C2E38CF960AE3434D34 * get_callback_3() const { return ___callback_3; }
	inline OnlineTimeCallback_t5AA2B75F108EB462BC1D2C2E38CF960AE3434D34 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(OnlineTimeCallback_t5AA2B75F108EB462BC1D2C2E38CF960AE3434D34 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}

	inline static int32_t get_offset_of_method_4() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeCoroutineU3Ed__49_tD7F6B839D41FBC936A43BDAE662892DB310F8ED8, ___method_4)); }
	inline int32_t get_method_4() const { return ___method_4; }
	inline int32_t* get_address_of_method_4() { return &___method_4; }
	inline void set_method_4(int32_t value)
	{
		___method_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETONLINETIMECOROUTINEU3ED__49_TD7F6B839D41FBC936A43BDAE662892DB310F8ED8_H
#ifndef U3CGETONLINETIMECOROUTINEU3ED__50_T35CA5ADEB47F29347B8CC79DED994AB8896D4D3E_H
#define U3CGETONLINETIMECOROUTINEU3ED__50_T35CA5ADEB47F29347B8CC79DED994AB8896D4D3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__50
struct  U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E  : public RuntimeObject
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__50::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__50::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Uri CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__50::uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___uri_2;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_RequestMethod CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__50::method
	int32_t ___method_3;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeCallback CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__50::callback
	OnlineTimeCallback_t5AA2B75F108EB462BC1D2C2E38CF960AE3434D34 * ___callback_4;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeResult CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__50::<result>5__2
	OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D  ___U3CresultU3E5__2_5;
	// UnityEngine.Networking.UnityWebRequest CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeCoroutine>d__50::<wr>5__3
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwrU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_uri_2() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E, ___uri_2)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_uri_2() const { return ___uri_2; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_uri_2() { return &___uri_2; }
	inline void set_uri_2(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___uri_2 = value;
		Il2CppCodeGenWriteBarrier((&___uri_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E, ___method_3)); }
	inline int32_t get_method_3() const { return ___method_3; }
	inline int32_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(int32_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E, ___callback_4)); }
	inline OnlineTimeCallback_t5AA2B75F108EB462BC1D2C2E38CF960AE3434D34 * get_callback_4() const { return ___callback_4; }
	inline OnlineTimeCallback_t5AA2B75F108EB462BC1D2C2E38CF960AE3434D34 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(OnlineTimeCallback_t5AA2B75F108EB462BC1D2C2E38CF960AE3434D34 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}

	inline static int32_t get_offset_of_U3CresultU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E, ___U3CresultU3E5__2_5)); }
	inline OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D  get_U3CresultU3E5__2_5() const { return ___U3CresultU3E5__2_5; }
	inline OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D * get_address_of_U3CresultU3E5__2_5() { return &___U3CresultU3E5__2_5; }
	inline void set_U3CresultU3E5__2_5(OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D  value)
	{
		___U3CresultU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CwrU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E, ___U3CwrU3E5__3_6)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwrU3E5__3_6() const { return ___U3CwrU3E5__3_6; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwrU3E5__3_6() { return &___U3CwrU3E5__3_6; }
	inline void set_U3CwrU3E5__3_6(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwrU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwrU3E5__3_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETONLINETIMECOROUTINEU3ED__50_T35CA5ADEB47F29347B8CC79DED994AB8896D4D3E_H
#ifndef U3CGETONLINETIMETASKU3ED__51_T106FECD8E78B6511C37B819838D037373BBCE489_H
#define U3CGETONLINETIMETASKU3ED__51_T106FECD8E78B6511C37B819838D037373BBCE489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__51
struct  U3CGetOnlineTimeTaskU3Ed__51_t106FECD8E78B6511C37B819838D037373BBCE489 
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__51::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeResult> CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__51::<>t__builder
	AsyncTaskMethodBuilder_1_t45F719F86664019FBC618D0C68C40492938FB756  ___U3CU3Et__builder_1;
	// System.String CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__51::url
	String_t* ___url_2;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_RequestMethod CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__51::method
	int32_t ___method_3;
	// System.Runtime.CompilerServices.TaskAwaiter`1<CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeResult> CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__51::<>u__1
	TaskAwaiter_1_tB004D5745F8C75ED93C95DCDB555E46DE24DF765  ___U3CU3Eu__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeTaskU3Ed__51_t106FECD8E78B6511C37B819838D037373BBCE489, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeTaskU3Ed__51_t106FECD8E78B6511C37B819838D037373BBCE489, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t45F719F86664019FBC618D0C68C40492938FB756  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t45F719F86664019FBC618D0C68C40492938FB756 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t45F719F86664019FBC618D0C68C40492938FB756  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeTaskU3Ed__51_t106FECD8E78B6511C37B819838D037373BBCE489, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeTaskU3Ed__51_t106FECD8E78B6511C37B819838D037373BBCE489, ___method_3)); }
	inline int32_t get_method_3() const { return ___method_3; }
	inline int32_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(int32_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeTaskU3Ed__51_t106FECD8E78B6511C37B819838D037373BBCE489, ___U3CU3Eu__1_4)); }
	inline TaskAwaiter_1_tB004D5745F8C75ED93C95DCDB555E46DE24DF765  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline TaskAwaiter_1_tB004D5745F8C75ED93C95DCDB555E46DE24DF765 * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(TaskAwaiter_1_tB004D5745F8C75ED93C95DCDB555E46DE24DF765  value)
	{
		___U3CU3Eu__1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETONLINETIMETASKU3ED__51_T106FECD8E78B6511C37B819838D037373BBCE489_H
#ifndef U3CGETONLINETIMETASKU3ED__52_T4C3A910AED19F5E424DEE86B8BAF7C49EC433134_H
#define U3CGETONLINETIMETASKU3ED__52_T4C3A910AED19F5E424DEE86B8BAF7C49EC433134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__52
struct  U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134 
{
public:
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__52::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeResult> CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__52::<>t__builder
	AsyncTaskMethodBuilder_1_t45F719F86664019FBC618D0C68C40492938FB756  ___U3CU3Et__builder_1;
	// System.Uri CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__52::uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___uri_2;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_RequestMethod CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__52::method
	int32_t ___method_3;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeResult CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__52::<result>5__2
	OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D  ___U3CresultU3E5__2_4;
	// System.Runtime.CompilerServices.TaskAwaiter CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__52::<>u__1
	TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  ___U3CU3Eu__1_5;
	// UnityEngine.Networking.UnityWebRequest CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__52::<wr>5__3
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwrU3E5__3_6;
	// UnityEngine.Networking.UnityWebRequestAsyncOperation CodeStage.AntiCheat.Detectors.TimeCheatingDetector_<GetOnlineTimeTask>d__52::<asyncOperation>5__4
	UnityWebRequestAsyncOperation_t726E134F16701A2671D40BEBE22110DC57156353 * ___U3CasyncOperationU3E5__4_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t45F719F86664019FBC618D0C68C40492938FB756  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t45F719F86664019FBC618D0C68C40492938FB756 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t45F719F86664019FBC618D0C68C40492938FB756  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_uri_2() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134, ___uri_2)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_uri_2() const { return ___uri_2; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_uri_2() { return &___uri_2; }
	inline void set_uri_2(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___uri_2 = value;
		Il2CppCodeGenWriteBarrier((&___uri_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134, ___method_3)); }
	inline int32_t get_method_3() const { return ___method_3; }
	inline int32_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(int32_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_U3CresultU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134, ___U3CresultU3E5__2_4)); }
	inline OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D  get_U3CresultU3E5__2_4() const { return ___U3CresultU3E5__2_4; }
	inline OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D * get_address_of_U3CresultU3E5__2_4() { return &___U3CresultU3E5__2_4; }
	inline void set_U3CresultU3E5__2_4(OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D  value)
	{
		___U3CresultU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_5() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134, ___U3CU3Eu__1_5)); }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  get_U3CU3Eu__1_5() const { return ___U3CU3Eu__1_5; }
	inline TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F * get_address_of_U3CU3Eu__1_5() { return &___U3CU3Eu__1_5; }
	inline void set_U3CU3Eu__1_5(TaskAwaiter_t0CDE8DBB564F0A0EA55FA6B3D43EEF96BC26252F  value)
	{
		___U3CU3Eu__1_5 = value;
	}

	inline static int32_t get_offset_of_U3CwrU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134, ___U3CwrU3E5__3_6)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwrU3E5__3_6() const { return ___U3CwrU3E5__3_6; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwrU3E5__3_6() { return &___U3CwrU3E5__3_6; }
	inline void set_U3CwrU3E5__3_6(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwrU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwrU3E5__3_6), value);
	}

	inline static int32_t get_offset_of_U3CasyncOperationU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134, ___U3CasyncOperationU3E5__4_7)); }
	inline UnityWebRequestAsyncOperation_t726E134F16701A2671D40BEBE22110DC57156353 * get_U3CasyncOperationU3E5__4_7() const { return ___U3CasyncOperationU3E5__4_7; }
	inline UnityWebRequestAsyncOperation_t726E134F16701A2671D40BEBE22110DC57156353 ** get_address_of_U3CasyncOperationU3E5__4_7() { return &___U3CasyncOperationU3E5__4_7; }
	inline void set_U3CasyncOperationU3E5__4_7(UnityWebRequestAsyncOperation_t726E134F16701A2671D40BEBE22110DC57156353 * value)
	{
		___U3CasyncOperationU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CasyncOperationU3E5__4_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETONLINETIMETASKU3ED__52_T4C3A910AED19F5E424DEE86B8BAF7C49EC433134_H
#ifndef ABSSEQUENTIABLE_TDA1366907669973CC0BB553EF4159D45FC46A757_H
#define ABSSEQUENTIABLE_TDA1366907669973CC0BB553EF4159D45FC46A757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757, ___onStart_3)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSSEQUENTIABLE_TDA1366907669973CC0BB553EF4159D45FC46A757_H
#ifndef DOTWEEN_T6BB48F76E494B12781696AF3D0733CA8DC367E8D_H
#define DOTWEEN_T6BB48F76E494B12781696AF3D0733CA8DC367E8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.DOTween
struct  DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D  : public RuntimeObject
{
public:

public:
};

struct DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields
{
public:
	// System.String DG.Tweening.DOTween::Version
	String_t* ___Version_0;
	// System.Boolean DG.Tweening.DOTween::useSafeMode
	bool ___useSafeMode_1;
	// System.Boolean DG.Tweening.DOTween::showUnityEditorReport
	bool ___showUnityEditorReport_2;
	// System.Single DG.Tweening.DOTween::timeScale
	float ___timeScale_3;
	// System.Boolean DG.Tweening.DOTween::useSmoothDeltaTime
	bool ___useSmoothDeltaTime_4;
	// System.Single DG.Tweening.DOTween::maxSmoothUnscaledTime
	float ___maxSmoothUnscaledTime_5;
	// DG.Tweening.LogBehaviour DG.Tweening.DOTween::_logBehaviour
	int32_t ____logBehaviour_6;
	// System.Boolean DG.Tweening.DOTween::drawGizmos
	bool ___drawGizmos_7;
	// DG.Tweening.UpdateType DG.Tweening.DOTween::defaultUpdateType
	int32_t ___defaultUpdateType_8;
	// System.Boolean DG.Tweening.DOTween::defaultTimeScaleIndependent
	bool ___defaultTimeScaleIndependent_9;
	// DG.Tweening.AutoPlay DG.Tweening.DOTween::defaultAutoPlay
	int32_t ___defaultAutoPlay_10;
	// System.Boolean DG.Tweening.DOTween::defaultAutoKill
	bool ___defaultAutoKill_11;
	// DG.Tweening.LoopType DG.Tweening.DOTween::defaultLoopType
	int32_t ___defaultLoopType_12;
	// System.Boolean DG.Tweening.DOTween::defaultRecyclable
	bool ___defaultRecyclable_13;
	// DG.Tweening.Ease DG.Tweening.DOTween::defaultEaseType
	int32_t ___defaultEaseType_14;
	// System.Single DG.Tweening.DOTween::defaultEaseOvershootOrAmplitude
	float ___defaultEaseOvershootOrAmplitude_15;
	// System.Single DG.Tweening.DOTween::defaultEasePeriod
	float ___defaultEasePeriod_16;
	// DG.Tweening.Core.DOTweenComponent DG.Tweening.DOTween::instance
	DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F * ___instance_17;
	// System.Boolean DG.Tweening.DOTween::isUnityEditor
	bool ___isUnityEditor_18;
	// System.Int32 DG.Tweening.DOTween::maxActiveTweenersReached
	int32_t ___maxActiveTweenersReached_19;
	// System.Int32 DG.Tweening.DOTween::maxActiveSequencesReached
	int32_t ___maxActiveSequencesReached_20;
	// System.Collections.Generic.List`1<DG.Tweening.TweenCallback> DG.Tweening.DOTween::GizmosDelegates
	List_1_tA91E07DF8FDCABF79352C03D79A9D29DE3BBA212 * ___GizmosDelegates_21;
	// System.Boolean DG.Tweening.DOTween::initialized
	bool ___initialized_22;
	// System.Boolean DG.Tweening.DOTween::isQuitting
	bool ___isQuitting_23;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___Version_0)); }
	inline String_t* get_Version_0() const { return ___Version_0; }
	inline String_t** get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(String_t* value)
	{
		___Version_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version_0), value);
	}

	inline static int32_t get_offset_of_useSafeMode_1() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___useSafeMode_1)); }
	inline bool get_useSafeMode_1() const { return ___useSafeMode_1; }
	inline bool* get_address_of_useSafeMode_1() { return &___useSafeMode_1; }
	inline void set_useSafeMode_1(bool value)
	{
		___useSafeMode_1 = value;
	}

	inline static int32_t get_offset_of_showUnityEditorReport_2() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___showUnityEditorReport_2)); }
	inline bool get_showUnityEditorReport_2() const { return ___showUnityEditorReport_2; }
	inline bool* get_address_of_showUnityEditorReport_2() { return &___showUnityEditorReport_2; }
	inline void set_showUnityEditorReport_2(bool value)
	{
		___showUnityEditorReport_2 = value;
	}

	inline static int32_t get_offset_of_timeScale_3() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___timeScale_3)); }
	inline float get_timeScale_3() const { return ___timeScale_3; }
	inline float* get_address_of_timeScale_3() { return &___timeScale_3; }
	inline void set_timeScale_3(float value)
	{
		___timeScale_3 = value;
	}

	inline static int32_t get_offset_of_useSmoothDeltaTime_4() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___useSmoothDeltaTime_4)); }
	inline bool get_useSmoothDeltaTime_4() const { return ___useSmoothDeltaTime_4; }
	inline bool* get_address_of_useSmoothDeltaTime_4() { return &___useSmoothDeltaTime_4; }
	inline void set_useSmoothDeltaTime_4(bool value)
	{
		___useSmoothDeltaTime_4 = value;
	}

	inline static int32_t get_offset_of_maxSmoothUnscaledTime_5() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___maxSmoothUnscaledTime_5)); }
	inline float get_maxSmoothUnscaledTime_5() const { return ___maxSmoothUnscaledTime_5; }
	inline float* get_address_of_maxSmoothUnscaledTime_5() { return &___maxSmoothUnscaledTime_5; }
	inline void set_maxSmoothUnscaledTime_5(float value)
	{
		___maxSmoothUnscaledTime_5 = value;
	}

	inline static int32_t get_offset_of__logBehaviour_6() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ____logBehaviour_6)); }
	inline int32_t get__logBehaviour_6() const { return ____logBehaviour_6; }
	inline int32_t* get_address_of__logBehaviour_6() { return &____logBehaviour_6; }
	inline void set__logBehaviour_6(int32_t value)
	{
		____logBehaviour_6 = value;
	}

	inline static int32_t get_offset_of_drawGizmos_7() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___drawGizmos_7)); }
	inline bool get_drawGizmos_7() const { return ___drawGizmos_7; }
	inline bool* get_address_of_drawGizmos_7() { return &___drawGizmos_7; }
	inline void set_drawGizmos_7(bool value)
	{
		___drawGizmos_7 = value;
	}

	inline static int32_t get_offset_of_defaultUpdateType_8() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultUpdateType_8)); }
	inline int32_t get_defaultUpdateType_8() const { return ___defaultUpdateType_8; }
	inline int32_t* get_address_of_defaultUpdateType_8() { return &___defaultUpdateType_8; }
	inline void set_defaultUpdateType_8(int32_t value)
	{
		___defaultUpdateType_8 = value;
	}

	inline static int32_t get_offset_of_defaultTimeScaleIndependent_9() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultTimeScaleIndependent_9)); }
	inline bool get_defaultTimeScaleIndependent_9() const { return ___defaultTimeScaleIndependent_9; }
	inline bool* get_address_of_defaultTimeScaleIndependent_9() { return &___defaultTimeScaleIndependent_9; }
	inline void set_defaultTimeScaleIndependent_9(bool value)
	{
		___defaultTimeScaleIndependent_9 = value;
	}

	inline static int32_t get_offset_of_defaultAutoPlay_10() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultAutoPlay_10)); }
	inline int32_t get_defaultAutoPlay_10() const { return ___defaultAutoPlay_10; }
	inline int32_t* get_address_of_defaultAutoPlay_10() { return &___defaultAutoPlay_10; }
	inline void set_defaultAutoPlay_10(int32_t value)
	{
		___defaultAutoPlay_10 = value;
	}

	inline static int32_t get_offset_of_defaultAutoKill_11() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultAutoKill_11)); }
	inline bool get_defaultAutoKill_11() const { return ___defaultAutoKill_11; }
	inline bool* get_address_of_defaultAutoKill_11() { return &___defaultAutoKill_11; }
	inline void set_defaultAutoKill_11(bool value)
	{
		___defaultAutoKill_11 = value;
	}

	inline static int32_t get_offset_of_defaultLoopType_12() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultLoopType_12)); }
	inline int32_t get_defaultLoopType_12() const { return ___defaultLoopType_12; }
	inline int32_t* get_address_of_defaultLoopType_12() { return &___defaultLoopType_12; }
	inline void set_defaultLoopType_12(int32_t value)
	{
		___defaultLoopType_12 = value;
	}

	inline static int32_t get_offset_of_defaultRecyclable_13() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultRecyclable_13)); }
	inline bool get_defaultRecyclable_13() const { return ___defaultRecyclable_13; }
	inline bool* get_address_of_defaultRecyclable_13() { return &___defaultRecyclable_13; }
	inline void set_defaultRecyclable_13(bool value)
	{
		___defaultRecyclable_13 = value;
	}

	inline static int32_t get_offset_of_defaultEaseType_14() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultEaseType_14)); }
	inline int32_t get_defaultEaseType_14() const { return ___defaultEaseType_14; }
	inline int32_t* get_address_of_defaultEaseType_14() { return &___defaultEaseType_14; }
	inline void set_defaultEaseType_14(int32_t value)
	{
		___defaultEaseType_14 = value;
	}

	inline static int32_t get_offset_of_defaultEaseOvershootOrAmplitude_15() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultEaseOvershootOrAmplitude_15)); }
	inline float get_defaultEaseOvershootOrAmplitude_15() const { return ___defaultEaseOvershootOrAmplitude_15; }
	inline float* get_address_of_defaultEaseOvershootOrAmplitude_15() { return &___defaultEaseOvershootOrAmplitude_15; }
	inline void set_defaultEaseOvershootOrAmplitude_15(float value)
	{
		___defaultEaseOvershootOrAmplitude_15 = value;
	}

	inline static int32_t get_offset_of_defaultEasePeriod_16() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___defaultEasePeriod_16)); }
	inline float get_defaultEasePeriod_16() const { return ___defaultEasePeriod_16; }
	inline float* get_address_of_defaultEasePeriod_16() { return &___defaultEasePeriod_16; }
	inline void set_defaultEasePeriod_16(float value)
	{
		___defaultEasePeriod_16 = value;
	}

	inline static int32_t get_offset_of_instance_17() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___instance_17)); }
	inline DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F * get_instance_17() const { return ___instance_17; }
	inline DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F ** get_address_of_instance_17() { return &___instance_17; }
	inline void set_instance_17(DOTweenComponent_t48118927332C9BFAE792209EEE42AF786370B05F * value)
	{
		___instance_17 = value;
		Il2CppCodeGenWriteBarrier((&___instance_17), value);
	}

	inline static int32_t get_offset_of_isUnityEditor_18() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___isUnityEditor_18)); }
	inline bool get_isUnityEditor_18() const { return ___isUnityEditor_18; }
	inline bool* get_address_of_isUnityEditor_18() { return &___isUnityEditor_18; }
	inline void set_isUnityEditor_18(bool value)
	{
		___isUnityEditor_18 = value;
	}

	inline static int32_t get_offset_of_maxActiveTweenersReached_19() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___maxActiveTweenersReached_19)); }
	inline int32_t get_maxActiveTweenersReached_19() const { return ___maxActiveTweenersReached_19; }
	inline int32_t* get_address_of_maxActiveTweenersReached_19() { return &___maxActiveTweenersReached_19; }
	inline void set_maxActiveTweenersReached_19(int32_t value)
	{
		___maxActiveTweenersReached_19 = value;
	}

	inline static int32_t get_offset_of_maxActiveSequencesReached_20() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___maxActiveSequencesReached_20)); }
	inline int32_t get_maxActiveSequencesReached_20() const { return ___maxActiveSequencesReached_20; }
	inline int32_t* get_address_of_maxActiveSequencesReached_20() { return &___maxActiveSequencesReached_20; }
	inline void set_maxActiveSequencesReached_20(int32_t value)
	{
		___maxActiveSequencesReached_20 = value;
	}

	inline static int32_t get_offset_of_GizmosDelegates_21() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___GizmosDelegates_21)); }
	inline List_1_tA91E07DF8FDCABF79352C03D79A9D29DE3BBA212 * get_GizmosDelegates_21() const { return ___GizmosDelegates_21; }
	inline List_1_tA91E07DF8FDCABF79352C03D79A9D29DE3BBA212 ** get_address_of_GizmosDelegates_21() { return &___GizmosDelegates_21; }
	inline void set_GizmosDelegates_21(List_1_tA91E07DF8FDCABF79352C03D79A9D29DE3BBA212 * value)
	{
		___GizmosDelegates_21 = value;
		Il2CppCodeGenWriteBarrier((&___GizmosDelegates_21), value);
	}

	inline static int32_t get_offset_of_initialized_22() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___initialized_22)); }
	inline bool get_initialized_22() const { return ___initialized_22; }
	inline bool* get_address_of_initialized_22() { return &___initialized_22; }
	inline void set_initialized_22(bool value)
	{
		___initialized_22 = value;
	}

	inline static int32_t get_offset_of_isQuitting_23() { return static_cast<int32_t>(offsetof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields, ___isQuitting_23)); }
	inline bool get_isQuitting_23() const { return ___isQuitting_23; }
	inline bool* get_address_of_isQuitting_23() { return &___isQuitting_23; }
	inline void set_isQuitting_23(bool value)
	{
		___isQuitting_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEEN_T6BB48F76E494B12781696AF3D0733CA8DC367E8D_H
#ifndef QUATERNIONOPTIONS_T217D095C19651CE87896F40C41802FA82552880B_H
#define QUATERNIONOPTIONS_T217D095C19651CE87896F40C41802FA82552880B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.QuaternionOptions
struct  QuaternionOptions_t217D095C19651CE87896F40C41802FA82552880B 
{
public:
	// DG.Tweening.RotateMode DG.Tweening.Plugins.Options.QuaternionOptions::rotateMode
	int32_t ___rotateMode_0;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.QuaternionOptions::axisConstraint
	int32_t ___axisConstraint_1;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Options.QuaternionOptions::up
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___up_2;

public:
	inline static int32_t get_offset_of_rotateMode_0() { return static_cast<int32_t>(offsetof(QuaternionOptions_t217D095C19651CE87896F40C41802FA82552880B, ___rotateMode_0)); }
	inline int32_t get_rotateMode_0() const { return ___rotateMode_0; }
	inline int32_t* get_address_of_rotateMode_0() { return &___rotateMode_0; }
	inline void set_rotateMode_0(int32_t value)
	{
		___rotateMode_0 = value;
	}

	inline static int32_t get_offset_of_axisConstraint_1() { return static_cast<int32_t>(offsetof(QuaternionOptions_t217D095C19651CE87896F40C41802FA82552880B, ___axisConstraint_1)); }
	inline int32_t get_axisConstraint_1() const { return ___axisConstraint_1; }
	inline int32_t* get_address_of_axisConstraint_1() { return &___axisConstraint_1; }
	inline void set_axisConstraint_1(int32_t value)
	{
		___axisConstraint_1 = value;
	}

	inline static int32_t get_offset_of_up_2() { return static_cast<int32_t>(offsetof(QuaternionOptions_t217D095C19651CE87896F40C41802FA82552880B, ___up_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_up_2() const { return ___up_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_up_2() { return &___up_2; }
	inline void set_up_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___up_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONOPTIONS_T217D095C19651CE87896F40C41802FA82552880B_H
#ifndef STRINGOPTIONS_T58D6011099873A39926A99C00EE24D0D80B231FC_H
#define STRINGOPTIONS_T58D6011099873A39926A99C00EE24D0D80B231FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.StringOptions
struct  StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.StringOptions::richTextEnabled
	bool ___richTextEnabled_0;
	// DG.Tweening.ScrambleMode DG.Tweening.Plugins.Options.StringOptions::scrambleMode
	int32_t ___scrambleMode_1;
	// System.Char[] DG.Tweening.Plugins.Options.StringOptions::scrambledChars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___scrambledChars_2;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::startValueStrippedLength
	int32_t ___startValueStrippedLength_3;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::changeValueStrippedLength
	int32_t ___changeValueStrippedLength_4;

public:
	inline static int32_t get_offset_of_richTextEnabled_0() { return static_cast<int32_t>(offsetof(StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC, ___richTextEnabled_0)); }
	inline bool get_richTextEnabled_0() const { return ___richTextEnabled_0; }
	inline bool* get_address_of_richTextEnabled_0() { return &___richTextEnabled_0; }
	inline void set_richTextEnabled_0(bool value)
	{
		___richTextEnabled_0 = value;
	}

	inline static int32_t get_offset_of_scrambleMode_1() { return static_cast<int32_t>(offsetof(StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC, ___scrambleMode_1)); }
	inline int32_t get_scrambleMode_1() const { return ___scrambleMode_1; }
	inline int32_t* get_address_of_scrambleMode_1() { return &___scrambleMode_1; }
	inline void set_scrambleMode_1(int32_t value)
	{
		___scrambleMode_1 = value;
	}

	inline static int32_t get_offset_of_scrambledChars_2() { return static_cast<int32_t>(offsetof(StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC, ___scrambledChars_2)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_scrambledChars_2() const { return ___scrambledChars_2; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_scrambledChars_2() { return &___scrambledChars_2; }
	inline void set_scrambledChars_2(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___scrambledChars_2 = value;
		Il2CppCodeGenWriteBarrier((&___scrambledChars_2), value);
	}

	inline static int32_t get_offset_of_startValueStrippedLength_3() { return static_cast<int32_t>(offsetof(StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC, ___startValueStrippedLength_3)); }
	inline int32_t get_startValueStrippedLength_3() const { return ___startValueStrippedLength_3; }
	inline int32_t* get_address_of_startValueStrippedLength_3() { return &___startValueStrippedLength_3; }
	inline void set_startValueStrippedLength_3(int32_t value)
	{
		___startValueStrippedLength_3 = value;
	}

	inline static int32_t get_offset_of_changeValueStrippedLength_4() { return static_cast<int32_t>(offsetof(StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC, ___changeValueStrippedLength_4)); }
	inline int32_t get_changeValueStrippedLength_4() const { return ___changeValueStrippedLength_4; }
	inline int32_t* get_address_of_changeValueStrippedLength_4() { return &___changeValueStrippedLength_4; }
	inline void set_changeValueStrippedLength_4(int32_t value)
	{
		___changeValueStrippedLength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC_marshaled_pinvoke
{
	int32_t ___richTextEnabled_0;
	int32_t ___scrambleMode_1;
	uint8_t* ___scrambledChars_2;
	int32_t ___startValueStrippedLength_3;
	int32_t ___changeValueStrippedLength_4;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC_marshaled_com
{
	int32_t ___richTextEnabled_0;
	int32_t ___scrambleMode_1;
	uint8_t* ___scrambledChars_2;
	int32_t ___startValueStrippedLength_3;
	int32_t ___changeValueStrippedLength_4;
};
#endif // STRINGOPTIONS_T58D6011099873A39926A99C00EE24D0D80B231FC_H
#ifndef VECTOR3ARRAYOPTIONS_T3E84666D670017F133C32243F0709A3852F05257_H
#define VECTOR3ARRAYOPTIONS_T3E84666D670017F133C32243F0709A3852F05257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct  Vector3ArrayOptions_t3E84666D670017F133C32243F0709A3852F05257 
{
public:
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.Vector3ArrayOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.Vector3ArrayOptions::snapping
	bool ___snapping_1;
	// System.Single[] DG.Tweening.Plugins.Options.Vector3ArrayOptions::durations
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___durations_2;

public:
	inline static int32_t get_offset_of_axisConstraint_0() { return static_cast<int32_t>(offsetof(Vector3ArrayOptions_t3E84666D670017F133C32243F0709A3852F05257, ___axisConstraint_0)); }
	inline int32_t get_axisConstraint_0() const { return ___axisConstraint_0; }
	inline int32_t* get_address_of_axisConstraint_0() { return &___axisConstraint_0; }
	inline void set_axisConstraint_0(int32_t value)
	{
		___axisConstraint_0 = value;
	}

	inline static int32_t get_offset_of_snapping_1() { return static_cast<int32_t>(offsetof(Vector3ArrayOptions_t3E84666D670017F133C32243F0709A3852F05257, ___snapping_1)); }
	inline bool get_snapping_1() const { return ___snapping_1; }
	inline bool* get_address_of_snapping_1() { return &___snapping_1; }
	inline void set_snapping_1(bool value)
	{
		___snapping_1 = value;
	}

	inline static int32_t get_offset_of_durations_2() { return static_cast<int32_t>(offsetof(Vector3ArrayOptions_t3E84666D670017F133C32243F0709A3852F05257, ___durations_2)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_durations_2() const { return ___durations_2; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_durations_2() { return &___durations_2; }
	inline void set_durations_2(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___durations_2 = value;
		Il2CppCodeGenWriteBarrier((&___durations_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct Vector3ArrayOptions_t3E84666D670017F133C32243F0709A3852F05257_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
	float* ___durations_2;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct Vector3ArrayOptions_t3E84666D670017F133C32243F0709A3852F05257_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
	float* ___durations_2;
};
#endif // VECTOR3ARRAYOPTIONS_T3E84666D670017F133C32243F0709A3852F05257_H
#ifndef VECTOROPTIONS_T385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322_H
#define VECTOROPTIONS_T385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.VectorOptions
struct  VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322 
{
public:
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.VectorOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.VectorOptions::snapping
	bool ___snapping_1;

public:
	inline static int32_t get_offset_of_axisConstraint_0() { return static_cast<int32_t>(offsetof(VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322, ___axisConstraint_0)); }
	inline int32_t get_axisConstraint_0() const { return ___axisConstraint_0; }
	inline int32_t* get_address_of_axisConstraint_0() { return &___axisConstraint_0; }
	inline void set_axisConstraint_0(int32_t value)
	{
		___axisConstraint_0 = value;
	}

	inline static int32_t get_offset_of_snapping_1() { return static_cast<int32_t>(offsetof(VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322, ___snapping_1)); }
	inline bool get_snapping_1() const { return ___snapping_1; }
	inline bool* get_address_of_snapping_1() { return &___snapping_1; }
	inline void set_snapping_1(bool value)
	{
		___snapping_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
#endif // VECTOROPTIONS_T385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef INJECTIONDETECTEDEVENTHANDLER_TC2CA209DEE2B887A8AFFA0E73EBDD507ABF343B1_H
#define INJECTIONDETECTEDEVENTHANDLER_TC2CA209DEE2B887A8AFFA0E73EBDD507ABF343B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.InjectionDetector_InjectionDetectedEventHandler
struct  InjectionDetectedEventHandler_tC2CA209DEE2B887A8AFFA0E73EBDD507ABF343B1  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTIONDETECTEDEVENTHANDLER_TC2CA209DEE2B887A8AFFA0E73EBDD507ABF343B1_H
#ifndef ONLINETIMECALLBACK_T5AA2B75F108EB462BC1D2C2E38CF960AE3434D34_H
#define ONLINETIMECALLBACK_T5AA2B75F108EB462BC1D2C2E38CF960AE3434D34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_OnlineTimeCallback
struct  OnlineTimeCallback_t5AA2B75F108EB462BC1D2C2E38CF960AE3434D34  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONLINETIMECALLBACK_T5AA2B75F108EB462BC1D2C2E38CF960AE3434D34_H
#ifndef TIMECHEATINGDETECTOREVENTHANDLER_T23FE998F4C6E3C8DF608BE057ACD82476B766D29_H
#define TIMECHEATINGDETECTOREVENTHANDLER_T23FE998F4C6E3C8DF608BE057ACD82476B766D29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_TimeCheatingDetectorEventHandler
struct  TimeCheatingDetectorEventHandler_t23FE998F4C6E3C8DF608BE057ACD82476B766D29  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMECHEATINGDETECTOREVENTHANDLER_T23FE998F4C6E3C8DF608BE057ACD82476B766D29_H
#ifndef HASHGENERATORRESULTHANDLER_TD7EA83E3F9BE89BA5F6FC201F7CF915EE9B31A2E_H
#define HASHGENERATORRESULTHANDLER_TD7EA83E3F9BE89BA5F6FC201F7CF915EE9B31A2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Genuine.CodeHash.HashGeneratorResultHandler
struct  HashGeneratorResultHandler_tD7EA83E3F9BE89BA5F6FC201F7CF915EE9B31A2E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHGENERATORRESULTHANDLER_TD7EA83E3F9BE89BA5F6FC201F7CF915EE9B31A2E_H
#ifndef EASEFUNCTION_TAC315FE3B057AC8DA87991C785F2595F3B761691_H
#define EASEFUNCTION_TAC315FE3B057AC8DA87991C785F2595F3B761691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.EaseFunction
struct  EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASEFUNCTION_TAC315FE3B057AC8DA87991C785F2595F3B761691_H
#ifndef TWEEN_T119487E0AB84EF563521F1043116BDBAE68AC437_H
#define TWEEN_T119487E0AB84EF563521F1043116BDBAE68AC437_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tween
struct  Tween_t119487E0AB84EF563521F1043116BDBAE68AC437  : public ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757
{
public:
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject * ___id_6;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject * ___target_7;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_8;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_9;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onPlay_10;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onPause_11;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onRewind_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onUpdate_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onStepComplete_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onComplete_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onKill_16;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_t46CB97F053453C4F41F8AE6213792813E455DDE1 * ___onWaypointChange_17;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_18;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_19;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_20;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_21;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_22;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_23;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_24;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_25;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_26;
	// System.Boolean DG.Tweening.Tween::isRelative
	bool ___isRelative_27;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_28;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691 * ___customEase_29;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_30;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_31;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t * ___typeofT1_32;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t * ___typeofT2_33;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t * ___typeofTPlugOptions_34;
	// System.Boolean DG.Tweening.Tween::active
	bool ___active_35;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_36;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___sequenceParent_37;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_38;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_39;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_40;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_41;
	// System.Boolean DG.Tweening.Tween::playedOnce
	bool ___playedOnce_42;
	// System.Single DG.Tweening.Tween::position
	float ___position_43;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_44;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_45;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_46;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_47;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_48;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_49;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_50;

public:
	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_isBackwards_5() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isBackwards_5)); }
	inline bool get_isBackwards_5() const { return ___isBackwards_5; }
	inline bool* get_address_of_isBackwards_5() { return &___isBackwards_5; }
	inline void set_isBackwards_5(bool value)
	{
		___isBackwards_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___id_6)); }
	inline RuntimeObject * get_id_6() const { return ___id_6; }
	inline RuntimeObject ** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(RuntimeObject * value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_6), value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___target_7)); }
	inline RuntimeObject * get_target_7() const { return ___target_7; }
	inline RuntimeObject ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(RuntimeObject * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((&___target_7), value);
	}

	inline static int32_t get_offset_of_updateType_8() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___updateType_8)); }
	inline int32_t get_updateType_8() const { return ___updateType_8; }
	inline int32_t* get_address_of_updateType_8() { return &___updateType_8; }
	inline void set_updateType_8(int32_t value)
	{
		___updateType_8 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_9() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isIndependentUpdate_9)); }
	inline bool get_isIndependentUpdate_9() const { return ___isIndependentUpdate_9; }
	inline bool* get_address_of_isIndependentUpdate_9() { return &___isIndependentUpdate_9; }
	inline void set_isIndependentUpdate_9(bool value)
	{
		___isIndependentUpdate_9 = value;
	}

	inline static int32_t get_offset_of_onPlay_10() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onPlay_10)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onPlay_10() const { return ___onPlay_10; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onPlay_10() { return &___onPlay_10; }
	inline void set_onPlay_10(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onPlay_10 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_10), value);
	}

	inline static int32_t get_offset_of_onPause_11() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onPause_11)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onPause_11() const { return ___onPause_11; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onPause_11() { return &___onPause_11; }
	inline void set_onPause_11(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onPause_11 = value;
		Il2CppCodeGenWriteBarrier((&___onPause_11), value);
	}

	inline static int32_t get_offset_of_onRewind_12() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onRewind_12)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onRewind_12() const { return ___onRewind_12; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onRewind_12() { return &___onRewind_12; }
	inline void set_onRewind_12(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onRewind_12 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_12), value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onUpdate_13)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onUpdate_13() const { return ___onUpdate_13; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_13), value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onStepComplete_14)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_14), value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onComplete_15)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onComplete_15() const { return ___onComplete_15; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_15), value);
	}

	inline static int32_t get_offset_of_onKill_16() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onKill_16)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onKill_16() const { return ___onKill_16; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onKill_16() { return &___onKill_16; }
	inline void set_onKill_16(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onKill_16 = value;
		Il2CppCodeGenWriteBarrier((&___onKill_16), value);
	}

	inline static int32_t get_offset_of_onWaypointChange_17() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onWaypointChange_17)); }
	inline TweenCallback_1_t46CB97F053453C4F41F8AE6213792813E455DDE1 * get_onWaypointChange_17() const { return ___onWaypointChange_17; }
	inline TweenCallback_1_t46CB97F053453C4F41F8AE6213792813E455DDE1 ** get_address_of_onWaypointChange_17() { return &___onWaypointChange_17; }
	inline void set_onWaypointChange_17(TweenCallback_1_t46CB97F053453C4F41F8AE6213792813E455DDE1 * value)
	{
		___onWaypointChange_17 = value;
		Il2CppCodeGenWriteBarrier((&___onWaypointChange_17), value);
	}

	inline static int32_t get_offset_of_isFrom_18() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isFrom_18)); }
	inline bool get_isFrom_18() const { return ___isFrom_18; }
	inline bool* get_address_of_isFrom_18() { return &___isFrom_18; }
	inline void set_isFrom_18(bool value)
	{
		___isFrom_18 = value;
	}

	inline static int32_t get_offset_of_isBlendable_19() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isBlendable_19)); }
	inline bool get_isBlendable_19() const { return ___isBlendable_19; }
	inline bool* get_address_of_isBlendable_19() { return &___isBlendable_19; }
	inline void set_isBlendable_19(bool value)
	{
		___isBlendable_19 = value;
	}

	inline static int32_t get_offset_of_isRecyclable_20() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isRecyclable_20)); }
	inline bool get_isRecyclable_20() const { return ___isRecyclable_20; }
	inline bool* get_address_of_isRecyclable_20() { return &___isRecyclable_20; }
	inline void set_isRecyclable_20(bool value)
	{
		___isRecyclable_20 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_21() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isSpeedBased_21)); }
	inline bool get_isSpeedBased_21() const { return ___isSpeedBased_21; }
	inline bool* get_address_of_isSpeedBased_21() { return &___isSpeedBased_21; }
	inline void set_isSpeedBased_21(bool value)
	{
		___isSpeedBased_21 = value;
	}

	inline static int32_t get_offset_of_autoKill_22() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___autoKill_22)); }
	inline bool get_autoKill_22() const { return ___autoKill_22; }
	inline bool* get_address_of_autoKill_22() { return &___autoKill_22; }
	inline void set_autoKill_22(bool value)
	{
		___autoKill_22 = value;
	}

	inline static int32_t get_offset_of_duration_23() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___duration_23)); }
	inline float get_duration_23() const { return ___duration_23; }
	inline float* get_address_of_duration_23() { return &___duration_23; }
	inline void set_duration_23(float value)
	{
		___duration_23 = value;
	}

	inline static int32_t get_offset_of_loops_24() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___loops_24)); }
	inline int32_t get_loops_24() const { return ___loops_24; }
	inline int32_t* get_address_of_loops_24() { return &___loops_24; }
	inline void set_loops_24(int32_t value)
	{
		___loops_24 = value;
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_delay_26() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___delay_26)); }
	inline float get_delay_26() const { return ___delay_26; }
	inline float* get_address_of_delay_26() { return &___delay_26; }
	inline void set_delay_26(float value)
	{
		___delay_26 = value;
	}

	inline static int32_t get_offset_of_isRelative_27() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isRelative_27)); }
	inline bool get_isRelative_27() const { return ___isRelative_27; }
	inline bool* get_address_of_isRelative_27() { return &___isRelative_27; }
	inline void set_isRelative_27(bool value)
	{
		___isRelative_27 = value;
	}

	inline static int32_t get_offset_of_easeType_28() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___easeType_28)); }
	inline int32_t get_easeType_28() const { return ___easeType_28; }
	inline int32_t* get_address_of_easeType_28() { return &___easeType_28; }
	inline void set_easeType_28(int32_t value)
	{
		___easeType_28 = value;
	}

	inline static int32_t get_offset_of_customEase_29() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___customEase_29)); }
	inline EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691 * get_customEase_29() const { return ___customEase_29; }
	inline EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691 ** get_address_of_customEase_29() { return &___customEase_29; }
	inline void set_customEase_29(EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691 * value)
	{
		___customEase_29 = value;
		Il2CppCodeGenWriteBarrier((&___customEase_29), value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_30() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___easeOvershootOrAmplitude_30)); }
	inline float get_easeOvershootOrAmplitude_30() const { return ___easeOvershootOrAmplitude_30; }
	inline float* get_address_of_easeOvershootOrAmplitude_30() { return &___easeOvershootOrAmplitude_30; }
	inline void set_easeOvershootOrAmplitude_30(float value)
	{
		___easeOvershootOrAmplitude_30 = value;
	}

	inline static int32_t get_offset_of_easePeriod_31() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___easePeriod_31)); }
	inline float get_easePeriod_31() const { return ___easePeriod_31; }
	inline float* get_address_of_easePeriod_31() { return &___easePeriod_31; }
	inline void set_easePeriod_31(float value)
	{
		___easePeriod_31 = value;
	}

	inline static int32_t get_offset_of_typeofT1_32() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___typeofT1_32)); }
	inline Type_t * get_typeofT1_32() const { return ___typeofT1_32; }
	inline Type_t ** get_address_of_typeofT1_32() { return &___typeofT1_32; }
	inline void set_typeofT1_32(Type_t * value)
	{
		___typeofT1_32 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT1_32), value);
	}

	inline static int32_t get_offset_of_typeofT2_33() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___typeofT2_33)); }
	inline Type_t * get_typeofT2_33() const { return ___typeofT2_33; }
	inline Type_t ** get_address_of_typeofT2_33() { return &___typeofT2_33; }
	inline void set_typeofT2_33(Type_t * value)
	{
		___typeofT2_33 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT2_33), value);
	}

	inline static int32_t get_offset_of_typeofTPlugOptions_34() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___typeofTPlugOptions_34)); }
	inline Type_t * get_typeofTPlugOptions_34() const { return ___typeofTPlugOptions_34; }
	inline Type_t ** get_address_of_typeofTPlugOptions_34() { return &___typeofTPlugOptions_34; }
	inline void set_typeofTPlugOptions_34(Type_t * value)
	{
		___typeofTPlugOptions_34 = value;
		Il2CppCodeGenWriteBarrier((&___typeofTPlugOptions_34), value);
	}

	inline static int32_t get_offset_of_active_35() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___active_35)); }
	inline bool get_active_35() const { return ___active_35; }
	inline bool* get_address_of_active_35() { return &___active_35; }
	inline void set_active_35(bool value)
	{
		___active_35 = value;
	}

	inline static int32_t get_offset_of_isSequenced_36() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isSequenced_36)); }
	inline bool get_isSequenced_36() const { return ___isSequenced_36; }
	inline bool* get_address_of_isSequenced_36() { return &___isSequenced_36; }
	inline void set_isSequenced_36(bool value)
	{
		___isSequenced_36 = value;
	}

	inline static int32_t get_offset_of_sequenceParent_37() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___sequenceParent_37)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_sequenceParent_37() const { return ___sequenceParent_37; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_sequenceParent_37() { return &___sequenceParent_37; }
	inline void set_sequenceParent_37(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___sequenceParent_37 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceParent_37), value);
	}

	inline static int32_t get_offset_of_activeId_38() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___activeId_38)); }
	inline int32_t get_activeId_38() const { return ___activeId_38; }
	inline int32_t* get_address_of_activeId_38() { return &___activeId_38; }
	inline void set_activeId_38(int32_t value)
	{
		___activeId_38 = value;
	}

	inline static int32_t get_offset_of_specialStartupMode_39() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___specialStartupMode_39)); }
	inline int32_t get_specialStartupMode_39() const { return ___specialStartupMode_39; }
	inline int32_t* get_address_of_specialStartupMode_39() { return &___specialStartupMode_39; }
	inline void set_specialStartupMode_39(int32_t value)
	{
		___specialStartupMode_39 = value;
	}

	inline static int32_t get_offset_of_creationLocked_40() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___creationLocked_40)); }
	inline bool get_creationLocked_40() const { return ___creationLocked_40; }
	inline bool* get_address_of_creationLocked_40() { return &___creationLocked_40; }
	inline void set_creationLocked_40(bool value)
	{
		___creationLocked_40 = value;
	}

	inline static int32_t get_offset_of_startupDone_41() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___startupDone_41)); }
	inline bool get_startupDone_41() const { return ___startupDone_41; }
	inline bool* get_address_of_startupDone_41() { return &___startupDone_41; }
	inline void set_startupDone_41(bool value)
	{
		___startupDone_41 = value;
	}

	inline static int32_t get_offset_of_playedOnce_42() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___playedOnce_42)); }
	inline bool get_playedOnce_42() const { return ___playedOnce_42; }
	inline bool* get_address_of_playedOnce_42() { return &___playedOnce_42; }
	inline void set_playedOnce_42(bool value)
	{
		___playedOnce_42 = value;
	}

	inline static int32_t get_offset_of_position_43() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___position_43)); }
	inline float get_position_43() const { return ___position_43; }
	inline float* get_address_of_position_43() { return &___position_43; }
	inline void set_position_43(float value)
	{
		___position_43 = value;
	}

	inline static int32_t get_offset_of_fullDuration_44() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___fullDuration_44)); }
	inline float get_fullDuration_44() const { return ___fullDuration_44; }
	inline float* get_address_of_fullDuration_44() { return &___fullDuration_44; }
	inline void set_fullDuration_44(float value)
	{
		___fullDuration_44 = value;
	}

	inline static int32_t get_offset_of_completedLoops_45() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___completedLoops_45)); }
	inline int32_t get_completedLoops_45() const { return ___completedLoops_45; }
	inline int32_t* get_address_of_completedLoops_45() { return &___completedLoops_45; }
	inline void set_completedLoops_45(int32_t value)
	{
		___completedLoops_45 = value;
	}

	inline static int32_t get_offset_of_isPlaying_46() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isPlaying_46)); }
	inline bool get_isPlaying_46() const { return ___isPlaying_46; }
	inline bool* get_address_of_isPlaying_46() { return &___isPlaying_46; }
	inline void set_isPlaying_46(bool value)
	{
		___isPlaying_46 = value;
	}

	inline static int32_t get_offset_of_isComplete_47() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isComplete_47)); }
	inline bool get_isComplete_47() const { return ___isComplete_47; }
	inline bool* get_address_of_isComplete_47() { return &___isComplete_47; }
	inline void set_isComplete_47(bool value)
	{
		___isComplete_47 = value;
	}

	inline static int32_t get_offset_of_elapsedDelay_48() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___elapsedDelay_48)); }
	inline float get_elapsedDelay_48() const { return ___elapsedDelay_48; }
	inline float* get_address_of_elapsedDelay_48() { return &___elapsedDelay_48; }
	inline void set_elapsedDelay_48(float value)
	{
		___elapsedDelay_48 = value;
	}

	inline static int32_t get_offset_of_delayComplete_49() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___delayComplete_49)); }
	inline bool get_delayComplete_49() const { return ___delayComplete_49; }
	inline bool* get_address_of_delayComplete_49() { return &___delayComplete_49; }
	inline void set_delayComplete_49(bool value)
	{
		___delayComplete_49 = value;
	}

	inline static int32_t get_offset_of_miscInt_50() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___miscInt_50)); }
	inline int32_t get_miscInt_50() const { return ___miscInt_50; }
	inline int32_t* get_address_of_miscInt_50() { return &___miscInt_50; }
	inline void set_miscInt_50(int32_t value)
	{
		___miscInt_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEEN_T119487E0AB84EF563521F1043116BDBAE68AC437_H
#ifndef TWEENCALLBACK_TD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83_H
#define TWEENCALLBACK_TD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenCallback
struct  TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENCALLBACK_TD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef SEQUENCE_TA6FA099F465933113D131DDC0EECB6A19A27BCB2_H
#define SEQUENCE_TA6FA099F465933113D131DDC0EECB6A19A27BCB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Sequence
struct  Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2  : public Tween_t119487E0AB84EF563521F1043116BDBAE68AC437
{
public:
	// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.Sequence::sequencedTweens
	List_1_t57BB69F1AC3759152D9E750F6120000328D367B8 * ___sequencedTweens_51;
	// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable> DG.Tweening.Sequence::_sequencedObjs
	List_1_t2E181C700331B7635DE1FD6A98300CEAEFABC4ED * ____sequencedObjs_52;
	// System.Single DG.Tweening.Sequence::lastTweenInsertTime
	float ___lastTweenInsertTime_53;

public:
	inline static int32_t get_offset_of_sequencedTweens_51() { return static_cast<int32_t>(offsetof(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2, ___sequencedTweens_51)); }
	inline List_1_t57BB69F1AC3759152D9E750F6120000328D367B8 * get_sequencedTweens_51() const { return ___sequencedTweens_51; }
	inline List_1_t57BB69F1AC3759152D9E750F6120000328D367B8 ** get_address_of_sequencedTweens_51() { return &___sequencedTweens_51; }
	inline void set_sequencedTweens_51(List_1_t57BB69F1AC3759152D9E750F6120000328D367B8 * value)
	{
		___sequencedTweens_51 = value;
		Il2CppCodeGenWriteBarrier((&___sequencedTweens_51), value);
	}

	inline static int32_t get_offset_of__sequencedObjs_52() { return static_cast<int32_t>(offsetof(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2, ____sequencedObjs_52)); }
	inline List_1_t2E181C700331B7635DE1FD6A98300CEAEFABC4ED * get__sequencedObjs_52() const { return ____sequencedObjs_52; }
	inline List_1_t2E181C700331B7635DE1FD6A98300CEAEFABC4ED ** get_address_of__sequencedObjs_52() { return &____sequencedObjs_52; }
	inline void set__sequencedObjs_52(List_1_t2E181C700331B7635DE1FD6A98300CEAEFABC4ED * value)
	{
		____sequencedObjs_52 = value;
		Il2CppCodeGenWriteBarrier((&____sequencedObjs_52), value);
	}

	inline static int32_t get_offset_of_lastTweenInsertTime_53() { return static_cast<int32_t>(offsetof(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2, ___lastTweenInsertTime_53)); }
	inline float get_lastTweenInsertTime_53() const { return ___lastTweenInsertTime_53; }
	inline float* get_address_of_lastTweenInsertTime_53() { return &___lastTweenInsertTime_53; }
	inline void set_lastTweenInsertTime_53(float value)
	{
		___lastTweenInsertTime_53 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCE_TA6FA099F465933113D131DDC0EECB6A19A27BCB2_H
#ifndef TWEENER_T9B2A5E94EE6D11F7607E58AE4E37186FF63587C8_H
#define TWEENER_T9B2A5E94EE6D11F7607E58AE4E37186FF63587C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tweener
struct  Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8  : public Tween_t119487E0AB84EF563521F1043116BDBAE68AC437
{
public:
	// System.Boolean DG.Tweening.Tweener::hasManuallySetStartValue
	bool ___hasManuallySetStartValue_51;
	// System.Boolean DG.Tweening.Tweener::isFromAllowed
	bool ___isFromAllowed_52;

public:
	inline static int32_t get_offset_of_hasManuallySetStartValue_51() { return static_cast<int32_t>(offsetof(Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8, ___hasManuallySetStartValue_51)); }
	inline bool get_hasManuallySetStartValue_51() const { return ___hasManuallySetStartValue_51; }
	inline bool* get_address_of_hasManuallySetStartValue_51() { return &___hasManuallySetStartValue_51; }
	inline void set_hasManuallySetStartValue_51(bool value)
	{
		___hasManuallySetStartValue_51 = value;
	}

	inline static int32_t get_offset_of_isFromAllowed_52() { return static_cast<int32_t>(offsetof(Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8, ___isFromAllowed_52)); }
	inline bool get_isFromAllowed_52() const { return ___isFromAllowed_52; }
	inline bool* get_address_of_isFromAllowed_52() { return &___isFromAllowed_52; }
	inline void set_isFromAllowed_52(bool value)
	{
		___isFromAllowed_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENER_T9B2A5E94EE6D11F7607E58AE4E37186FF63587C8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef KEEPALIVEBEHAVIOUR_1_T9EE5C74DE9516E407858EA057A9A286154200A9F_H
#define KEEPALIVEBEHAVIOUR_1_T9EE5C74DE9516E407858EA057A9A286154200A9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.KeepAliveBehaviour`1<CodeStage.AntiCheat.Detectors.InjectionDetector>
struct  KeepAliveBehaviour_1_t9EE5C74DE9516E407858EA057A9A286154200A9F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::keepAlive
	bool ___keepAlive_4;
	// System.Int32 CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::instancesInScene
	int32_t ___instancesInScene_5;

public:
	inline static int32_t get_offset_of_keepAlive_4() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t9EE5C74DE9516E407858EA057A9A286154200A9F, ___keepAlive_4)); }
	inline bool get_keepAlive_4() const { return ___keepAlive_4; }
	inline bool* get_address_of_keepAlive_4() { return &___keepAlive_4; }
	inline void set_keepAlive_4(bool value)
	{
		___keepAlive_4 = value;
	}

	inline static int32_t get_offset_of_instancesInScene_5() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t9EE5C74DE9516E407858EA057A9A286154200A9F, ___instancesInScene_5)); }
	inline int32_t get_instancesInScene_5() const { return ___instancesInScene_5; }
	inline int32_t* get_address_of_instancesInScene_5() { return &___instancesInScene_5; }
	inline void set_instancesInScene_5(int32_t value)
	{
		___instancesInScene_5 = value;
	}
};

struct KeepAliveBehaviour_1_t9EE5C74DE9516E407858EA057A9A286154200A9F_StaticFields
{
public:
	// T CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::<Instance>k__BackingField
	InjectionDetector_tB402442AA73D35344CEF9C14C806583C37BD610D * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t9EE5C74DE9516E407858EA057A9A286154200A9F_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline InjectionDetector_tB402442AA73D35344CEF9C14C806583C37BD610D * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline InjectionDetector_tB402442AA73D35344CEF9C14C806583C37BD610D ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(InjectionDetector_tB402442AA73D35344CEF9C14C806583C37BD610D * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEEPALIVEBEHAVIOUR_1_T9EE5C74DE9516E407858EA057A9A286154200A9F_H
#ifndef KEEPALIVEBEHAVIOUR_1_T0A6BCB14B545073CE530B758EE0391ADD0CE791B_H
#define KEEPALIVEBEHAVIOUR_1_T0A6BCB14B545073CE530B758EE0391ADD0CE791B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.KeepAliveBehaviour`1<CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector>
struct  KeepAliveBehaviour_1_t0A6BCB14B545073CE530B758EE0391ADD0CE791B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::keepAlive
	bool ___keepAlive_4;
	// System.Int32 CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::instancesInScene
	int32_t ___instancesInScene_5;

public:
	inline static int32_t get_offset_of_keepAlive_4() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t0A6BCB14B545073CE530B758EE0391ADD0CE791B, ___keepAlive_4)); }
	inline bool get_keepAlive_4() const { return ___keepAlive_4; }
	inline bool* get_address_of_keepAlive_4() { return &___keepAlive_4; }
	inline void set_keepAlive_4(bool value)
	{
		___keepAlive_4 = value;
	}

	inline static int32_t get_offset_of_instancesInScene_5() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t0A6BCB14B545073CE530B758EE0391ADD0CE791B, ___instancesInScene_5)); }
	inline int32_t get_instancesInScene_5() const { return ___instancesInScene_5; }
	inline int32_t* get_address_of_instancesInScene_5() { return &___instancesInScene_5; }
	inline void set_instancesInScene_5(int32_t value)
	{
		___instancesInScene_5 = value;
	}
};

struct KeepAliveBehaviour_1_t0A6BCB14B545073CE530B758EE0391ADD0CE791B_StaticFields
{
public:
	// T CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::<Instance>k__BackingField
	ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t0A6BCB14B545073CE530B758EE0391ADD0CE791B_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEEPALIVEBEHAVIOUR_1_T0A6BCB14B545073CE530B758EE0391ADD0CE791B_H
#ifndef KEEPALIVEBEHAVIOUR_1_T7797B6DF9E8B20884A95C3C0DF93A75A262D6393_H
#define KEEPALIVEBEHAVIOUR_1_T7797B6DF9E8B20884A95C3C0DF93A75A262D6393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.KeepAliveBehaviour`1<CodeStage.AntiCheat.Detectors.SpeedHackDetector>
struct  KeepAliveBehaviour_1_t7797B6DF9E8B20884A95C3C0DF93A75A262D6393  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::keepAlive
	bool ___keepAlive_4;
	// System.Int32 CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::instancesInScene
	int32_t ___instancesInScene_5;

public:
	inline static int32_t get_offset_of_keepAlive_4() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t7797B6DF9E8B20884A95C3C0DF93A75A262D6393, ___keepAlive_4)); }
	inline bool get_keepAlive_4() const { return ___keepAlive_4; }
	inline bool* get_address_of_keepAlive_4() { return &___keepAlive_4; }
	inline void set_keepAlive_4(bool value)
	{
		___keepAlive_4 = value;
	}

	inline static int32_t get_offset_of_instancesInScene_5() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t7797B6DF9E8B20884A95C3C0DF93A75A262D6393, ___instancesInScene_5)); }
	inline int32_t get_instancesInScene_5() const { return ___instancesInScene_5; }
	inline int32_t* get_address_of_instancesInScene_5() { return &___instancesInScene_5; }
	inline void set_instancesInScene_5(int32_t value)
	{
		___instancesInScene_5 = value;
	}
};

struct KeepAliveBehaviour_1_t7797B6DF9E8B20884A95C3C0DF93A75A262D6393_StaticFields
{
public:
	// T CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::<Instance>k__BackingField
	SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t7797B6DF9E8B20884A95C3C0DF93A75A262D6393_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEEPALIVEBEHAVIOUR_1_T7797B6DF9E8B20884A95C3C0DF93A75A262D6393_H
#ifndef KEEPALIVEBEHAVIOUR_1_T3C0894887B759D2A9C5D6D83B7277680F4A16304_H
#define KEEPALIVEBEHAVIOUR_1_T3C0894887B759D2A9C5D6D83B7277680F4A16304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.KeepAliveBehaviour`1<CodeStage.AntiCheat.Detectors.TimeCheatingDetector>
struct  KeepAliveBehaviour_1_t3C0894887B759D2A9C5D6D83B7277680F4A16304  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::keepAlive
	bool ___keepAlive_4;
	// System.Int32 CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::instancesInScene
	int32_t ___instancesInScene_5;

public:
	inline static int32_t get_offset_of_keepAlive_4() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t3C0894887B759D2A9C5D6D83B7277680F4A16304, ___keepAlive_4)); }
	inline bool get_keepAlive_4() const { return ___keepAlive_4; }
	inline bool* get_address_of_keepAlive_4() { return &___keepAlive_4; }
	inline void set_keepAlive_4(bool value)
	{
		___keepAlive_4 = value;
	}

	inline static int32_t get_offset_of_instancesInScene_5() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t3C0894887B759D2A9C5D6D83B7277680F4A16304, ___instancesInScene_5)); }
	inline int32_t get_instancesInScene_5() const { return ___instancesInScene_5; }
	inline int32_t* get_address_of_instancesInScene_5() { return &___instancesInScene_5; }
	inline void set_instancesInScene_5(int32_t value)
	{
		___instancesInScene_5 = value;
	}
};

struct KeepAliveBehaviour_1_t3C0894887B759D2A9C5D6D83B7277680F4A16304_StaticFields
{
public:
	// T CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::<Instance>k__BackingField
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t3C0894887B759D2A9C5D6D83B7277680F4A16304_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEEPALIVEBEHAVIOUR_1_T3C0894887B759D2A9C5D6D83B7277680F4A16304_H
#ifndef KEEPALIVEBEHAVIOUR_1_TE58BEDAA1527155D4C65C950CD76EE4006CCFB1D_H
#define KEEPALIVEBEHAVIOUR_1_TE58BEDAA1527155D4C65C950CD76EE4006CCFB1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.KeepAliveBehaviour`1<CodeStage.AntiCheat.Detectors.WallHackDetector>
struct  KeepAliveBehaviour_1_tE58BEDAA1527155D4C65C950CD76EE4006CCFB1D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::keepAlive
	bool ___keepAlive_4;
	// System.Int32 CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::instancesInScene
	int32_t ___instancesInScene_5;

public:
	inline static int32_t get_offset_of_keepAlive_4() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_tE58BEDAA1527155D4C65C950CD76EE4006CCFB1D, ___keepAlive_4)); }
	inline bool get_keepAlive_4() const { return ___keepAlive_4; }
	inline bool* get_address_of_keepAlive_4() { return &___keepAlive_4; }
	inline void set_keepAlive_4(bool value)
	{
		___keepAlive_4 = value;
	}

	inline static int32_t get_offset_of_instancesInScene_5() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_tE58BEDAA1527155D4C65C950CD76EE4006CCFB1D, ___instancesInScene_5)); }
	inline int32_t get_instancesInScene_5() const { return ___instancesInScene_5; }
	inline int32_t* get_address_of_instancesInScene_5() { return &___instancesInScene_5; }
	inline void set_instancesInScene_5(int32_t value)
	{
		___instancesInScene_5 = value;
	}
};

struct KeepAliveBehaviour_1_tE58BEDAA1527155D4C65C950CD76EE4006CCFB1D_StaticFields
{
public:
	// T CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::<Instance>k__BackingField
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_tE58BEDAA1527155D4C65C950CD76EE4006CCFB1D_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEEPALIVEBEHAVIOUR_1_TE58BEDAA1527155D4C65C950CD76EE4006CCFB1D_H
#ifndef KEEPALIVEBEHAVIOUR_1_T599F4C31D92484C69D2CF1EE1BFF954D07A1073A_H
#define KEEPALIVEBEHAVIOUR_1_T599F4C31D92484C69D2CF1EE1BFF954D07A1073A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Common.KeepAliveBehaviour`1<CodeStage.AntiCheat.Genuine.CodeHash.CodeHashGenerator>
struct  KeepAliveBehaviour_1_t599F4C31D92484C69D2CF1EE1BFF954D07A1073A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::keepAlive
	bool ___keepAlive_4;
	// System.Int32 CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::instancesInScene
	int32_t ___instancesInScene_5;

public:
	inline static int32_t get_offset_of_keepAlive_4() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t599F4C31D92484C69D2CF1EE1BFF954D07A1073A, ___keepAlive_4)); }
	inline bool get_keepAlive_4() const { return ___keepAlive_4; }
	inline bool* get_address_of_keepAlive_4() { return &___keepAlive_4; }
	inline void set_keepAlive_4(bool value)
	{
		___keepAlive_4 = value;
	}

	inline static int32_t get_offset_of_instancesInScene_5() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t599F4C31D92484C69D2CF1EE1BFF954D07A1073A, ___instancesInScene_5)); }
	inline int32_t get_instancesInScene_5() const { return ___instancesInScene_5; }
	inline int32_t* get_address_of_instancesInScene_5() { return &___instancesInScene_5; }
	inline void set_instancesInScene_5(int32_t value)
	{
		___instancesInScene_5 = value;
	}
};

struct KeepAliveBehaviour_1_t599F4C31D92484C69D2CF1EE1BFF954D07A1073A_StaticFields
{
public:
	// T CodeStage.AntiCheat.Common.KeepAliveBehaviour`1::<Instance>k__BackingField
	CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480 * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(KeepAliveBehaviour_1_t599F4C31D92484C69D2CF1EE1BFF954D07A1073A_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480 * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480 ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480 * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEEPALIVEBEHAVIOUR_1_T599F4C31D92484C69D2CF1EE1BFF954D07A1073A_H
#ifndef ACTKDETECTORBASE_1_T939A5FE28FE28532175DD14FCA3C07F816731308_H
#define ACTKDETECTORBASE_1_T939A5FE28FE28532175DD14FCA3C07F816731308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1<CodeStage.AntiCheat.Detectors.InjectionDetector>
struct  ACTkDetectorBase_1_t939A5FE28FE28532175DD14FCA3C07F816731308  : public KeepAliveBehaviour_1_t9EE5C74DE9516E407858EA057A9A286154200A9F
{
public:
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::autoStart
	bool ___autoStart_8;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::autoDispose
	bool ___autoDispose_9;
	// System.Action CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::CheatDetected
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___CheatDetected_10;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::<IsCheatDetected>k__BackingField
	bool ___U3CIsCheatDetectedU3Ek__BackingField_11;
	// UnityEngine.Events.UnityEvent CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::detectionEvent
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___detectionEvent_12;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::detectionEventHasListener
	bool ___detectionEventHasListener_13;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::started
	bool ___started_14;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::isRunning
	bool ___isRunning_15;

public:
	inline static int32_t get_offset_of_autoStart_8() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t939A5FE28FE28532175DD14FCA3C07F816731308, ___autoStart_8)); }
	inline bool get_autoStart_8() const { return ___autoStart_8; }
	inline bool* get_address_of_autoStart_8() { return &___autoStart_8; }
	inline void set_autoStart_8(bool value)
	{
		___autoStart_8 = value;
	}

	inline static int32_t get_offset_of_autoDispose_9() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t939A5FE28FE28532175DD14FCA3C07F816731308, ___autoDispose_9)); }
	inline bool get_autoDispose_9() const { return ___autoDispose_9; }
	inline bool* get_address_of_autoDispose_9() { return &___autoDispose_9; }
	inline void set_autoDispose_9(bool value)
	{
		___autoDispose_9 = value;
	}

	inline static int32_t get_offset_of_CheatDetected_10() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t939A5FE28FE28532175DD14FCA3C07F816731308, ___CheatDetected_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_CheatDetected_10() const { return ___CheatDetected_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_CheatDetected_10() { return &___CheatDetected_10; }
	inline void set_CheatDetected_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___CheatDetected_10 = value;
		Il2CppCodeGenWriteBarrier((&___CheatDetected_10), value);
	}

	inline static int32_t get_offset_of_U3CIsCheatDetectedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t939A5FE28FE28532175DD14FCA3C07F816731308, ___U3CIsCheatDetectedU3Ek__BackingField_11)); }
	inline bool get_U3CIsCheatDetectedU3Ek__BackingField_11() const { return ___U3CIsCheatDetectedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsCheatDetectedU3Ek__BackingField_11() { return &___U3CIsCheatDetectedU3Ek__BackingField_11; }
	inline void set_U3CIsCheatDetectedU3Ek__BackingField_11(bool value)
	{
		___U3CIsCheatDetectedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_detectionEvent_12() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t939A5FE28FE28532175DD14FCA3C07F816731308, ___detectionEvent_12)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_detectionEvent_12() const { return ___detectionEvent_12; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_detectionEvent_12() { return &___detectionEvent_12; }
	inline void set_detectionEvent_12(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___detectionEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___detectionEvent_12), value);
	}

	inline static int32_t get_offset_of_detectionEventHasListener_13() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t939A5FE28FE28532175DD14FCA3C07F816731308, ___detectionEventHasListener_13)); }
	inline bool get_detectionEventHasListener_13() const { return ___detectionEventHasListener_13; }
	inline bool* get_address_of_detectionEventHasListener_13() { return &___detectionEventHasListener_13; }
	inline void set_detectionEventHasListener_13(bool value)
	{
		___detectionEventHasListener_13 = value;
	}

	inline static int32_t get_offset_of_started_14() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t939A5FE28FE28532175DD14FCA3C07F816731308, ___started_14)); }
	inline bool get_started_14() const { return ___started_14; }
	inline bool* get_address_of_started_14() { return &___started_14; }
	inline void set_started_14(bool value)
	{
		___started_14 = value;
	}

	inline static int32_t get_offset_of_isRunning_15() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t939A5FE28FE28532175DD14FCA3C07F816731308, ___isRunning_15)); }
	inline bool get_isRunning_15() const { return ___isRunning_15; }
	inline bool* get_address_of_isRunning_15() { return &___isRunning_15; }
	inline void set_isRunning_15(bool value)
	{
		___isRunning_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKDETECTORBASE_1_T939A5FE28FE28532175DD14FCA3C07F816731308_H
#ifndef ACTKDETECTORBASE_1_T2B046C275F2284A36CF0207E00EF20D69045339C_H
#define ACTKDETECTORBASE_1_T2B046C275F2284A36CF0207E00EF20D69045339C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1<CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector>
struct  ACTkDetectorBase_1_t2B046C275F2284A36CF0207E00EF20D69045339C  : public KeepAliveBehaviour_1_t0A6BCB14B545073CE530B758EE0391ADD0CE791B
{
public:
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::autoStart
	bool ___autoStart_8;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::autoDispose
	bool ___autoDispose_9;
	// System.Action CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::CheatDetected
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___CheatDetected_10;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::<IsCheatDetected>k__BackingField
	bool ___U3CIsCheatDetectedU3Ek__BackingField_11;
	// UnityEngine.Events.UnityEvent CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::detectionEvent
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___detectionEvent_12;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::detectionEventHasListener
	bool ___detectionEventHasListener_13;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::started
	bool ___started_14;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::isRunning
	bool ___isRunning_15;

public:
	inline static int32_t get_offset_of_autoStart_8() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t2B046C275F2284A36CF0207E00EF20D69045339C, ___autoStart_8)); }
	inline bool get_autoStart_8() const { return ___autoStart_8; }
	inline bool* get_address_of_autoStart_8() { return &___autoStart_8; }
	inline void set_autoStart_8(bool value)
	{
		___autoStart_8 = value;
	}

	inline static int32_t get_offset_of_autoDispose_9() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t2B046C275F2284A36CF0207E00EF20D69045339C, ___autoDispose_9)); }
	inline bool get_autoDispose_9() const { return ___autoDispose_9; }
	inline bool* get_address_of_autoDispose_9() { return &___autoDispose_9; }
	inline void set_autoDispose_9(bool value)
	{
		___autoDispose_9 = value;
	}

	inline static int32_t get_offset_of_CheatDetected_10() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t2B046C275F2284A36CF0207E00EF20D69045339C, ___CheatDetected_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_CheatDetected_10() const { return ___CheatDetected_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_CheatDetected_10() { return &___CheatDetected_10; }
	inline void set_CheatDetected_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___CheatDetected_10 = value;
		Il2CppCodeGenWriteBarrier((&___CheatDetected_10), value);
	}

	inline static int32_t get_offset_of_U3CIsCheatDetectedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t2B046C275F2284A36CF0207E00EF20D69045339C, ___U3CIsCheatDetectedU3Ek__BackingField_11)); }
	inline bool get_U3CIsCheatDetectedU3Ek__BackingField_11() const { return ___U3CIsCheatDetectedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsCheatDetectedU3Ek__BackingField_11() { return &___U3CIsCheatDetectedU3Ek__BackingField_11; }
	inline void set_U3CIsCheatDetectedU3Ek__BackingField_11(bool value)
	{
		___U3CIsCheatDetectedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_detectionEvent_12() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t2B046C275F2284A36CF0207E00EF20D69045339C, ___detectionEvent_12)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_detectionEvent_12() const { return ___detectionEvent_12; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_detectionEvent_12() { return &___detectionEvent_12; }
	inline void set_detectionEvent_12(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___detectionEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___detectionEvent_12), value);
	}

	inline static int32_t get_offset_of_detectionEventHasListener_13() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t2B046C275F2284A36CF0207E00EF20D69045339C, ___detectionEventHasListener_13)); }
	inline bool get_detectionEventHasListener_13() const { return ___detectionEventHasListener_13; }
	inline bool* get_address_of_detectionEventHasListener_13() { return &___detectionEventHasListener_13; }
	inline void set_detectionEventHasListener_13(bool value)
	{
		___detectionEventHasListener_13 = value;
	}

	inline static int32_t get_offset_of_started_14() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t2B046C275F2284A36CF0207E00EF20D69045339C, ___started_14)); }
	inline bool get_started_14() const { return ___started_14; }
	inline bool* get_address_of_started_14() { return &___started_14; }
	inline void set_started_14(bool value)
	{
		___started_14 = value;
	}

	inline static int32_t get_offset_of_isRunning_15() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t2B046C275F2284A36CF0207E00EF20D69045339C, ___isRunning_15)); }
	inline bool get_isRunning_15() const { return ___isRunning_15; }
	inline bool* get_address_of_isRunning_15() { return &___isRunning_15; }
	inline void set_isRunning_15(bool value)
	{
		___isRunning_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKDETECTORBASE_1_T2B046C275F2284A36CF0207E00EF20D69045339C_H
#ifndef ACTKDETECTORBASE_1_T199A0E4E5C8EB0600CCBA8259B58A93884FD66D8_H
#define ACTKDETECTORBASE_1_T199A0E4E5C8EB0600CCBA8259B58A93884FD66D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1<CodeStage.AntiCheat.Detectors.SpeedHackDetector>
struct  ACTkDetectorBase_1_t199A0E4E5C8EB0600CCBA8259B58A93884FD66D8  : public KeepAliveBehaviour_1_t7797B6DF9E8B20884A95C3C0DF93A75A262D6393
{
public:
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::autoStart
	bool ___autoStart_8;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::autoDispose
	bool ___autoDispose_9;
	// System.Action CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::CheatDetected
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___CheatDetected_10;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::<IsCheatDetected>k__BackingField
	bool ___U3CIsCheatDetectedU3Ek__BackingField_11;
	// UnityEngine.Events.UnityEvent CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::detectionEvent
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___detectionEvent_12;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::detectionEventHasListener
	bool ___detectionEventHasListener_13;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::started
	bool ___started_14;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::isRunning
	bool ___isRunning_15;

public:
	inline static int32_t get_offset_of_autoStart_8() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t199A0E4E5C8EB0600CCBA8259B58A93884FD66D8, ___autoStart_8)); }
	inline bool get_autoStart_8() const { return ___autoStart_8; }
	inline bool* get_address_of_autoStart_8() { return &___autoStart_8; }
	inline void set_autoStart_8(bool value)
	{
		___autoStart_8 = value;
	}

	inline static int32_t get_offset_of_autoDispose_9() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t199A0E4E5C8EB0600CCBA8259B58A93884FD66D8, ___autoDispose_9)); }
	inline bool get_autoDispose_9() const { return ___autoDispose_9; }
	inline bool* get_address_of_autoDispose_9() { return &___autoDispose_9; }
	inline void set_autoDispose_9(bool value)
	{
		___autoDispose_9 = value;
	}

	inline static int32_t get_offset_of_CheatDetected_10() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t199A0E4E5C8EB0600CCBA8259B58A93884FD66D8, ___CheatDetected_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_CheatDetected_10() const { return ___CheatDetected_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_CheatDetected_10() { return &___CheatDetected_10; }
	inline void set_CheatDetected_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___CheatDetected_10 = value;
		Il2CppCodeGenWriteBarrier((&___CheatDetected_10), value);
	}

	inline static int32_t get_offset_of_U3CIsCheatDetectedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t199A0E4E5C8EB0600CCBA8259B58A93884FD66D8, ___U3CIsCheatDetectedU3Ek__BackingField_11)); }
	inline bool get_U3CIsCheatDetectedU3Ek__BackingField_11() const { return ___U3CIsCheatDetectedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsCheatDetectedU3Ek__BackingField_11() { return &___U3CIsCheatDetectedU3Ek__BackingField_11; }
	inline void set_U3CIsCheatDetectedU3Ek__BackingField_11(bool value)
	{
		___U3CIsCheatDetectedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_detectionEvent_12() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t199A0E4E5C8EB0600CCBA8259B58A93884FD66D8, ___detectionEvent_12)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_detectionEvent_12() const { return ___detectionEvent_12; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_detectionEvent_12() { return &___detectionEvent_12; }
	inline void set_detectionEvent_12(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___detectionEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___detectionEvent_12), value);
	}

	inline static int32_t get_offset_of_detectionEventHasListener_13() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t199A0E4E5C8EB0600CCBA8259B58A93884FD66D8, ___detectionEventHasListener_13)); }
	inline bool get_detectionEventHasListener_13() const { return ___detectionEventHasListener_13; }
	inline bool* get_address_of_detectionEventHasListener_13() { return &___detectionEventHasListener_13; }
	inline void set_detectionEventHasListener_13(bool value)
	{
		___detectionEventHasListener_13 = value;
	}

	inline static int32_t get_offset_of_started_14() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t199A0E4E5C8EB0600CCBA8259B58A93884FD66D8, ___started_14)); }
	inline bool get_started_14() const { return ___started_14; }
	inline bool* get_address_of_started_14() { return &___started_14; }
	inline void set_started_14(bool value)
	{
		___started_14 = value;
	}

	inline static int32_t get_offset_of_isRunning_15() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t199A0E4E5C8EB0600CCBA8259B58A93884FD66D8, ___isRunning_15)); }
	inline bool get_isRunning_15() const { return ___isRunning_15; }
	inline bool* get_address_of_isRunning_15() { return &___isRunning_15; }
	inline void set_isRunning_15(bool value)
	{
		___isRunning_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKDETECTORBASE_1_T199A0E4E5C8EB0600CCBA8259B58A93884FD66D8_H
#ifndef ACTKDETECTORBASE_1_T0668CF6D7BCF8106C21066FC335852A5F07B0712_H
#define ACTKDETECTORBASE_1_T0668CF6D7BCF8106C21066FC335852A5F07B0712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1<CodeStage.AntiCheat.Detectors.TimeCheatingDetector>
struct  ACTkDetectorBase_1_t0668CF6D7BCF8106C21066FC335852A5F07B0712  : public KeepAliveBehaviour_1_t3C0894887B759D2A9C5D6D83B7277680F4A16304
{
public:
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::autoStart
	bool ___autoStart_8;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::autoDispose
	bool ___autoDispose_9;
	// System.Action CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::CheatDetected
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___CheatDetected_10;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::<IsCheatDetected>k__BackingField
	bool ___U3CIsCheatDetectedU3Ek__BackingField_11;
	// UnityEngine.Events.UnityEvent CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::detectionEvent
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___detectionEvent_12;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::detectionEventHasListener
	bool ___detectionEventHasListener_13;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::started
	bool ___started_14;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::isRunning
	bool ___isRunning_15;

public:
	inline static int32_t get_offset_of_autoStart_8() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t0668CF6D7BCF8106C21066FC335852A5F07B0712, ___autoStart_8)); }
	inline bool get_autoStart_8() const { return ___autoStart_8; }
	inline bool* get_address_of_autoStart_8() { return &___autoStart_8; }
	inline void set_autoStart_8(bool value)
	{
		___autoStart_8 = value;
	}

	inline static int32_t get_offset_of_autoDispose_9() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t0668CF6D7BCF8106C21066FC335852A5F07B0712, ___autoDispose_9)); }
	inline bool get_autoDispose_9() const { return ___autoDispose_9; }
	inline bool* get_address_of_autoDispose_9() { return &___autoDispose_9; }
	inline void set_autoDispose_9(bool value)
	{
		___autoDispose_9 = value;
	}

	inline static int32_t get_offset_of_CheatDetected_10() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t0668CF6D7BCF8106C21066FC335852A5F07B0712, ___CheatDetected_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_CheatDetected_10() const { return ___CheatDetected_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_CheatDetected_10() { return &___CheatDetected_10; }
	inline void set_CheatDetected_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___CheatDetected_10 = value;
		Il2CppCodeGenWriteBarrier((&___CheatDetected_10), value);
	}

	inline static int32_t get_offset_of_U3CIsCheatDetectedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t0668CF6D7BCF8106C21066FC335852A5F07B0712, ___U3CIsCheatDetectedU3Ek__BackingField_11)); }
	inline bool get_U3CIsCheatDetectedU3Ek__BackingField_11() const { return ___U3CIsCheatDetectedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsCheatDetectedU3Ek__BackingField_11() { return &___U3CIsCheatDetectedU3Ek__BackingField_11; }
	inline void set_U3CIsCheatDetectedU3Ek__BackingField_11(bool value)
	{
		___U3CIsCheatDetectedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_detectionEvent_12() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t0668CF6D7BCF8106C21066FC335852A5F07B0712, ___detectionEvent_12)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_detectionEvent_12() const { return ___detectionEvent_12; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_detectionEvent_12() { return &___detectionEvent_12; }
	inline void set_detectionEvent_12(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___detectionEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___detectionEvent_12), value);
	}

	inline static int32_t get_offset_of_detectionEventHasListener_13() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t0668CF6D7BCF8106C21066FC335852A5F07B0712, ___detectionEventHasListener_13)); }
	inline bool get_detectionEventHasListener_13() const { return ___detectionEventHasListener_13; }
	inline bool* get_address_of_detectionEventHasListener_13() { return &___detectionEventHasListener_13; }
	inline void set_detectionEventHasListener_13(bool value)
	{
		___detectionEventHasListener_13 = value;
	}

	inline static int32_t get_offset_of_started_14() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t0668CF6D7BCF8106C21066FC335852A5F07B0712, ___started_14)); }
	inline bool get_started_14() const { return ___started_14; }
	inline bool* get_address_of_started_14() { return &___started_14; }
	inline void set_started_14(bool value)
	{
		___started_14 = value;
	}

	inline static int32_t get_offset_of_isRunning_15() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_t0668CF6D7BCF8106C21066FC335852A5F07B0712, ___isRunning_15)); }
	inline bool get_isRunning_15() const { return ___isRunning_15; }
	inline bool* get_address_of_isRunning_15() { return &___isRunning_15; }
	inline void set_isRunning_15(bool value)
	{
		___isRunning_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKDETECTORBASE_1_T0668CF6D7BCF8106C21066FC335852A5F07B0712_H
#ifndef ACTKDETECTORBASE_1_TE2A24D5EFB3C34378E36E9B26754EDF94D9E2BA3_H
#define ACTKDETECTORBASE_1_TE2A24D5EFB3C34378E36E9B26754EDF94D9E2BA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1<CodeStage.AntiCheat.Detectors.WallHackDetector>
struct  ACTkDetectorBase_1_tE2A24D5EFB3C34378E36E9B26754EDF94D9E2BA3  : public KeepAliveBehaviour_1_tE58BEDAA1527155D4C65C950CD76EE4006CCFB1D
{
public:
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::autoStart
	bool ___autoStart_8;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::autoDispose
	bool ___autoDispose_9;
	// System.Action CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::CheatDetected
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___CheatDetected_10;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::<IsCheatDetected>k__BackingField
	bool ___U3CIsCheatDetectedU3Ek__BackingField_11;
	// UnityEngine.Events.UnityEvent CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::detectionEvent
	UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * ___detectionEvent_12;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::detectionEventHasListener
	bool ___detectionEventHasListener_13;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::started
	bool ___started_14;
	// System.Boolean CodeStage.AntiCheat.Detectors.ACTkDetectorBase`1::isRunning
	bool ___isRunning_15;

public:
	inline static int32_t get_offset_of_autoStart_8() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_tE2A24D5EFB3C34378E36E9B26754EDF94D9E2BA3, ___autoStart_8)); }
	inline bool get_autoStart_8() const { return ___autoStart_8; }
	inline bool* get_address_of_autoStart_8() { return &___autoStart_8; }
	inline void set_autoStart_8(bool value)
	{
		___autoStart_8 = value;
	}

	inline static int32_t get_offset_of_autoDispose_9() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_tE2A24D5EFB3C34378E36E9B26754EDF94D9E2BA3, ___autoDispose_9)); }
	inline bool get_autoDispose_9() const { return ___autoDispose_9; }
	inline bool* get_address_of_autoDispose_9() { return &___autoDispose_9; }
	inline void set_autoDispose_9(bool value)
	{
		___autoDispose_9 = value;
	}

	inline static int32_t get_offset_of_CheatDetected_10() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_tE2A24D5EFB3C34378E36E9B26754EDF94D9E2BA3, ___CheatDetected_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_CheatDetected_10() const { return ___CheatDetected_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_CheatDetected_10() { return &___CheatDetected_10; }
	inline void set_CheatDetected_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___CheatDetected_10 = value;
		Il2CppCodeGenWriteBarrier((&___CheatDetected_10), value);
	}

	inline static int32_t get_offset_of_U3CIsCheatDetectedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_tE2A24D5EFB3C34378E36E9B26754EDF94D9E2BA3, ___U3CIsCheatDetectedU3Ek__BackingField_11)); }
	inline bool get_U3CIsCheatDetectedU3Ek__BackingField_11() const { return ___U3CIsCheatDetectedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsCheatDetectedU3Ek__BackingField_11() { return &___U3CIsCheatDetectedU3Ek__BackingField_11; }
	inline void set_U3CIsCheatDetectedU3Ek__BackingField_11(bool value)
	{
		___U3CIsCheatDetectedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_detectionEvent_12() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_tE2A24D5EFB3C34378E36E9B26754EDF94D9E2BA3, ___detectionEvent_12)); }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * get_detectionEvent_12() const { return ___detectionEvent_12; }
	inline UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F ** get_address_of_detectionEvent_12() { return &___detectionEvent_12; }
	inline void set_detectionEvent_12(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * value)
	{
		___detectionEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___detectionEvent_12), value);
	}

	inline static int32_t get_offset_of_detectionEventHasListener_13() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_tE2A24D5EFB3C34378E36E9B26754EDF94D9E2BA3, ___detectionEventHasListener_13)); }
	inline bool get_detectionEventHasListener_13() const { return ___detectionEventHasListener_13; }
	inline bool* get_address_of_detectionEventHasListener_13() { return &___detectionEventHasListener_13; }
	inline void set_detectionEventHasListener_13(bool value)
	{
		___detectionEventHasListener_13 = value;
	}

	inline static int32_t get_offset_of_started_14() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_tE2A24D5EFB3C34378E36E9B26754EDF94D9E2BA3, ___started_14)); }
	inline bool get_started_14() const { return ___started_14; }
	inline bool* get_address_of_started_14() { return &___started_14; }
	inline void set_started_14(bool value)
	{
		___started_14 = value;
	}

	inline static int32_t get_offset_of_isRunning_15() { return static_cast<int32_t>(offsetof(ACTkDetectorBase_1_tE2A24D5EFB3C34378E36E9B26754EDF94D9E2BA3, ___isRunning_15)); }
	inline bool get_isRunning_15() const { return ___isRunning_15; }
	inline bool* get_address_of_isRunning_15() { return &___isRunning_15; }
	inline void set_isRunning_15(bool value)
	{
		___isRunning_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTKDETECTORBASE_1_TE2A24D5EFB3C34378E36E9B26754EDF94D9E2BA3_H
#ifndef CODEHASHGENERATOR_T8375A0FF3F16EAB1DB67961AD53ADE9A2D184480_H
#define CODEHASHGENERATOR_T8375A0FF3F16EAB1DB67961AD53ADE9A2D184480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Genuine.CodeHash.CodeHashGenerator
struct  CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480  : public KeepAliveBehaviour_1_t599F4C31D92484C69D2CF1EE1BFF954D07A1073A
{
public:
	// CodeStage.AntiCheat.Genuine.CodeHash.HashGeneratorResult CodeStage.AntiCheat.Genuine.CodeHash.CodeHashGenerator::<LastResult>k__BackingField
	HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7 * ___U3CLastResultU3Ek__BackingField_8;
	// UnityEngine.WaitForSeconds CodeStage.AntiCheat.Genuine.CodeHash.CodeHashGenerator::cachedWaitForSeconds
	WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * ___cachedWaitForSeconds_9;
	// CodeStage.AntiCheat.Genuine.CodeHash.BaseWorker CodeStage.AntiCheat.Genuine.CodeHash.CodeHashGenerator::currentWorker
	BaseWorker_tD3B5F0035F406307CFD71F8BCF644A3D81A1036E * ___currentWorker_10;

public:
	inline static int32_t get_offset_of_U3CLastResultU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480, ___U3CLastResultU3Ek__BackingField_8)); }
	inline HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7 * get_U3CLastResultU3Ek__BackingField_8() const { return ___U3CLastResultU3Ek__BackingField_8; }
	inline HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7 ** get_address_of_U3CLastResultU3Ek__BackingField_8() { return &___U3CLastResultU3Ek__BackingField_8; }
	inline void set_U3CLastResultU3Ek__BackingField_8(HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7 * value)
	{
		___U3CLastResultU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastResultU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_cachedWaitForSeconds_9() { return static_cast<int32_t>(offsetof(CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480, ___cachedWaitForSeconds_9)); }
	inline WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * get_cachedWaitForSeconds_9() const { return ___cachedWaitForSeconds_9; }
	inline WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 ** get_address_of_cachedWaitForSeconds_9() { return &___cachedWaitForSeconds_9; }
	inline void set_cachedWaitForSeconds_9(WaitForSeconds_t3E9E78D3BB53F03F96C7F28BA9B9086CD1A5F4E8 * value)
	{
		___cachedWaitForSeconds_9 = value;
		Il2CppCodeGenWriteBarrier((&___cachedWaitForSeconds_9), value);
	}

	inline static int32_t get_offset_of_currentWorker_10() { return static_cast<int32_t>(offsetof(CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480, ___currentWorker_10)); }
	inline BaseWorker_tD3B5F0035F406307CFD71F8BCF644A3D81A1036E * get_currentWorker_10() const { return ___currentWorker_10; }
	inline BaseWorker_tD3B5F0035F406307CFD71F8BCF644A3D81A1036E ** get_address_of_currentWorker_10() { return &___currentWorker_10; }
	inline void set_currentWorker_10(BaseWorker_tD3B5F0035F406307CFD71F8BCF644A3D81A1036E * value)
	{
		___currentWorker_10 = value;
		Il2CppCodeGenWriteBarrier((&___currentWorker_10), value);
	}
};

struct CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480_StaticFields
{
public:
	// CodeStage.AntiCheat.Genuine.CodeHash.HashGeneratorResultHandler CodeStage.AntiCheat.Genuine.CodeHash.CodeHashGenerator::HashGenerated
	HashGeneratorResultHandler_tD7EA83E3F9BE89BA5F6FC201F7CF915EE9B31A2E * ___HashGenerated_7;

public:
	inline static int32_t get_offset_of_HashGenerated_7() { return static_cast<int32_t>(offsetof(CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480_StaticFields, ___HashGenerated_7)); }
	inline HashGeneratorResultHandler_tD7EA83E3F9BE89BA5F6FC201F7CF915EE9B31A2E * get_HashGenerated_7() const { return ___HashGenerated_7; }
	inline HashGeneratorResultHandler_tD7EA83E3F9BE89BA5F6FC201F7CF915EE9B31A2E ** get_address_of_HashGenerated_7() { return &___HashGenerated_7; }
	inline void set_HashGenerated_7(HashGeneratorResultHandler_tD7EA83E3F9BE89BA5F6FC201F7CF915EE9B31A2E * value)
	{
		___HashGenerated_7 = value;
		Il2CppCodeGenWriteBarrier((&___HashGenerated_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEHASHGENERATOR_T8375A0FF3F16EAB1DB67961AD53ADE9A2D184480_H
#ifndef INJECTIONDETECTOR_TB402442AA73D35344CEF9C14C806583C37BD610D_H
#define INJECTIONDETECTOR_TB402442AA73D35344CEF9C14C806583C37BD610D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.InjectionDetector
struct  InjectionDetector_tB402442AA73D35344CEF9C14C806583C37BD610D  : public ACTkDetectorBase_1_t939A5FE28FE28532175DD14FCA3C07F816731308
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTIONDETECTOR_TB402442AA73D35344CEF9C14C806583C37BD610D_H
#ifndef OBSCUREDCHEATINGDETECTOR_T486DBF80B2EE2FE3520DA67CF45A1618352CA601_H
#define OBSCUREDCHEATINGDETECTOR_T486DBF80B2EE2FE3520DA67CF45A1618352CA601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector
struct  ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601  : public ACTkDetectorBase_1_t2B046C275F2284A36CF0207E00EF20D69045339C
{
public:
	// System.Double CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector::doubleEpsilon
	double ___doubleEpsilon_18;
	// System.Single CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector::floatEpsilon
	float ___floatEpsilon_19;
	// System.Single CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector::vector2Epsilon
	float ___vector2Epsilon_20;
	// System.Single CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector::vector3Epsilon
	float ___vector3Epsilon_21;
	// System.Single CodeStage.AntiCheat.Detectors.ObscuredCheatingDetector::quaternionEpsilon
	float ___quaternionEpsilon_22;

public:
	inline static int32_t get_offset_of_doubleEpsilon_18() { return static_cast<int32_t>(offsetof(ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601, ___doubleEpsilon_18)); }
	inline double get_doubleEpsilon_18() const { return ___doubleEpsilon_18; }
	inline double* get_address_of_doubleEpsilon_18() { return &___doubleEpsilon_18; }
	inline void set_doubleEpsilon_18(double value)
	{
		___doubleEpsilon_18 = value;
	}

	inline static int32_t get_offset_of_floatEpsilon_19() { return static_cast<int32_t>(offsetof(ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601, ___floatEpsilon_19)); }
	inline float get_floatEpsilon_19() const { return ___floatEpsilon_19; }
	inline float* get_address_of_floatEpsilon_19() { return &___floatEpsilon_19; }
	inline void set_floatEpsilon_19(float value)
	{
		___floatEpsilon_19 = value;
	}

	inline static int32_t get_offset_of_vector2Epsilon_20() { return static_cast<int32_t>(offsetof(ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601, ___vector2Epsilon_20)); }
	inline float get_vector2Epsilon_20() const { return ___vector2Epsilon_20; }
	inline float* get_address_of_vector2Epsilon_20() { return &___vector2Epsilon_20; }
	inline void set_vector2Epsilon_20(float value)
	{
		___vector2Epsilon_20 = value;
	}

	inline static int32_t get_offset_of_vector3Epsilon_21() { return static_cast<int32_t>(offsetof(ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601, ___vector3Epsilon_21)); }
	inline float get_vector3Epsilon_21() const { return ___vector3Epsilon_21; }
	inline float* get_address_of_vector3Epsilon_21() { return &___vector3Epsilon_21; }
	inline void set_vector3Epsilon_21(float value)
	{
		___vector3Epsilon_21 = value;
	}

	inline static int32_t get_offset_of_quaternionEpsilon_22() { return static_cast<int32_t>(offsetof(ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601, ___quaternionEpsilon_22)); }
	inline float get_quaternionEpsilon_22() const { return ___quaternionEpsilon_22; }
	inline float* get_address_of_quaternionEpsilon_22() { return &___quaternionEpsilon_22; }
	inline void set_quaternionEpsilon_22(float value)
	{
		___quaternionEpsilon_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSCUREDCHEATINGDETECTOR_T486DBF80B2EE2FE3520DA67CF45A1618352CA601_H
#ifndef SPEEDHACKDETECTOR_TCF5D7139EF4D5B1345E222A85570F96A1A8430FE_H
#define SPEEDHACKDETECTOR_TCF5D7139EF4D5B1345E222A85570F96A1A8430FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.SpeedHackDetector
struct  SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE  : public ACTkDetectorBase_1_t199A0E4E5C8EB0600CCBA8259B58A93884FD66D8
{
public:
	// System.Single CodeStage.AntiCheat.Detectors.SpeedHackDetector::interval
	float ___interval_18;
	// System.Single CodeStage.AntiCheat.Detectors.SpeedHackDetector::threshold
	float ___threshold_19;
	// System.Byte CodeStage.AntiCheat.Detectors.SpeedHackDetector::maxFalsePositives
	uint8_t ___maxFalsePositives_20;
	// System.Int32 CodeStage.AntiCheat.Detectors.SpeedHackDetector::coolDown
	int32_t ___coolDown_21;
	// System.Byte CodeStage.AntiCheat.Detectors.SpeedHackDetector::currentFalsePositives
	uint8_t ___currentFalsePositives_22;
	// System.Int32 CodeStage.AntiCheat.Detectors.SpeedHackDetector::currentCooldownShots
	int32_t ___currentCooldownShots_23;
	// System.Int64 CodeStage.AntiCheat.Detectors.SpeedHackDetector::previousReliableTicks
	int64_t ___previousReliableTicks_24;
	// System.Int64 CodeStage.AntiCheat.Detectors.SpeedHackDetector::previousVulnerableEnvironmentTicks
	int64_t ___previousVulnerableEnvironmentTicks_25;
	// System.Int64 CodeStage.AntiCheat.Detectors.SpeedHackDetector::previousVulnerableRealtimeTicks
	int64_t ___previousVulnerableRealtimeTicks_26;

public:
	inline static int32_t get_offset_of_interval_18() { return static_cast<int32_t>(offsetof(SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE, ___interval_18)); }
	inline float get_interval_18() const { return ___interval_18; }
	inline float* get_address_of_interval_18() { return &___interval_18; }
	inline void set_interval_18(float value)
	{
		___interval_18 = value;
	}

	inline static int32_t get_offset_of_threshold_19() { return static_cast<int32_t>(offsetof(SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE, ___threshold_19)); }
	inline float get_threshold_19() const { return ___threshold_19; }
	inline float* get_address_of_threshold_19() { return &___threshold_19; }
	inline void set_threshold_19(float value)
	{
		___threshold_19 = value;
	}

	inline static int32_t get_offset_of_maxFalsePositives_20() { return static_cast<int32_t>(offsetof(SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE, ___maxFalsePositives_20)); }
	inline uint8_t get_maxFalsePositives_20() const { return ___maxFalsePositives_20; }
	inline uint8_t* get_address_of_maxFalsePositives_20() { return &___maxFalsePositives_20; }
	inline void set_maxFalsePositives_20(uint8_t value)
	{
		___maxFalsePositives_20 = value;
	}

	inline static int32_t get_offset_of_coolDown_21() { return static_cast<int32_t>(offsetof(SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE, ___coolDown_21)); }
	inline int32_t get_coolDown_21() const { return ___coolDown_21; }
	inline int32_t* get_address_of_coolDown_21() { return &___coolDown_21; }
	inline void set_coolDown_21(int32_t value)
	{
		___coolDown_21 = value;
	}

	inline static int32_t get_offset_of_currentFalsePositives_22() { return static_cast<int32_t>(offsetof(SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE, ___currentFalsePositives_22)); }
	inline uint8_t get_currentFalsePositives_22() const { return ___currentFalsePositives_22; }
	inline uint8_t* get_address_of_currentFalsePositives_22() { return &___currentFalsePositives_22; }
	inline void set_currentFalsePositives_22(uint8_t value)
	{
		___currentFalsePositives_22 = value;
	}

	inline static int32_t get_offset_of_currentCooldownShots_23() { return static_cast<int32_t>(offsetof(SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE, ___currentCooldownShots_23)); }
	inline int32_t get_currentCooldownShots_23() const { return ___currentCooldownShots_23; }
	inline int32_t* get_address_of_currentCooldownShots_23() { return &___currentCooldownShots_23; }
	inline void set_currentCooldownShots_23(int32_t value)
	{
		___currentCooldownShots_23 = value;
	}

	inline static int32_t get_offset_of_previousReliableTicks_24() { return static_cast<int32_t>(offsetof(SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE, ___previousReliableTicks_24)); }
	inline int64_t get_previousReliableTicks_24() const { return ___previousReliableTicks_24; }
	inline int64_t* get_address_of_previousReliableTicks_24() { return &___previousReliableTicks_24; }
	inline void set_previousReliableTicks_24(int64_t value)
	{
		___previousReliableTicks_24 = value;
	}

	inline static int32_t get_offset_of_previousVulnerableEnvironmentTicks_25() { return static_cast<int32_t>(offsetof(SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE, ___previousVulnerableEnvironmentTicks_25)); }
	inline int64_t get_previousVulnerableEnvironmentTicks_25() const { return ___previousVulnerableEnvironmentTicks_25; }
	inline int64_t* get_address_of_previousVulnerableEnvironmentTicks_25() { return &___previousVulnerableEnvironmentTicks_25; }
	inline void set_previousVulnerableEnvironmentTicks_25(int64_t value)
	{
		___previousVulnerableEnvironmentTicks_25 = value;
	}

	inline static int32_t get_offset_of_previousVulnerableRealtimeTicks_26() { return static_cast<int32_t>(offsetof(SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE, ___previousVulnerableRealtimeTicks_26)); }
	inline int64_t get_previousVulnerableRealtimeTicks_26() const { return ___previousVulnerableRealtimeTicks_26; }
	inline int64_t* get_address_of_previousVulnerableRealtimeTicks_26() { return &___previousVulnerableRealtimeTicks_26; }
	inline void set_previousVulnerableRealtimeTicks_26(int64_t value)
	{
		___previousVulnerableRealtimeTicks_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPEEDHACKDETECTOR_TCF5D7139EF4D5B1345E222A85570F96A1A8430FE_H
#ifndef TIMECHEATINGDETECTOR_T4014D5D2CD0E66A64471222FD6C2266962791B65_H
#define TIMECHEATINGDETECTOR_T4014D5D2CD0E66A64471222FD6C2266962791B65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.TimeCheatingDetector
struct  TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65  : public ACTkDetectorBase_1_t0668CF6D7BCF8106C21066FC335852A5F07B0712
{
public:
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_TimeCheatingDetectorEventHandler CodeStage.AntiCheat.Detectors.TimeCheatingDetector::CheatChecked
	TimeCheatingDetectorEventHandler_t23FE998F4C6E3C8DF608BE057ACD82476B766D29 * ___CheatChecked_21;
	// System.String CodeStage.AntiCheat.Detectors.TimeCheatingDetector::requestUrl
	String_t* ___requestUrl_22;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_RequestMethod CodeStage.AntiCheat.Detectors.TimeCheatingDetector::requestMethod
	int32_t ___requestMethod_23;
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector::timeoutSeconds
	int32_t ___timeoutSeconds_24;
	// System.Single CodeStage.AntiCheat.Detectors.TimeCheatingDetector::interval
	float ___interval_25;
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector::realCheatThreshold
	int32_t ___realCheatThreshold_26;
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector::wrongTimeThreshold
	int32_t ___wrongTimeThreshold_27;
	// System.Boolean CodeStage.AntiCheat.Detectors.TimeCheatingDetector::ignoreSetCorrectTime
	bool ___ignoreSetCorrectTime_28;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_ErrorKind CodeStage.AntiCheat.Detectors.TimeCheatingDetector::<LastError>k__BackingField
	int32_t ___U3CLastErrorU3Ek__BackingField_29;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_CheckResult CodeStage.AntiCheat.Detectors.TimeCheatingDetector::<LastResult>k__BackingField
	int32_t ___U3CLastResultU3Ek__BackingField_30;
	// System.Boolean CodeStage.AntiCheat.Detectors.TimeCheatingDetector::<IsCheckingForCheat>k__BackingField
	bool ___U3CIsCheckingForCheatU3Ek__BackingField_31;
	// System.String CodeStage.AntiCheat.Detectors.TimeCheatingDetector::onlineOfflineDifferencePrefsKey
	String_t* ___onlineOfflineDifferencePrefsKey_32;
	// System.Uri CodeStage.AntiCheat.Detectors.TimeCheatingDetector::cachedUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___cachedUri_33;
	// CodeStage.AntiCheat.Detectors.TimeCheatingDetector_TimeCheatingDetectorEventHandler CodeStage.AntiCheat.Detectors.TimeCheatingDetector::cheatChecked
	TimeCheatingDetectorEventHandler_t23FE998F4C6E3C8DF608BE057ACD82476B766D29 * ___cheatChecked_34;
	// System.Single CodeStage.AntiCheat.Detectors.TimeCheatingDetector::timeElapsed
	float ___timeElapsed_35;
	// System.Boolean CodeStage.AntiCheat.Detectors.TimeCheatingDetector::updateAfterPause
	bool ___updateAfterPause_36;
	// System.Double CodeStage.AntiCheat.Detectors.TimeCheatingDetector::lastOnlineSecondsUtc
	double ___lastOnlineSecondsUtc_37;
	// System.Action`1<CodeStage.AntiCheat.Detectors.TimeCheatingDetector_ErrorKind> CodeStage.AntiCheat.Detectors.TimeCheatingDetector::Error
	Action_1_tF98CA887A7F4745726B4B140A2BEBE05A25F2554 * ___Error_38;
	// System.Action CodeStage.AntiCheat.Detectors.TimeCheatingDetector::CheckPassed
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___CheckPassed_39;
	// System.Int32 CodeStage.AntiCheat.Detectors.TimeCheatingDetector::threshold
	int32_t ___threshold_40;
	// System.String CodeStage.AntiCheat.Detectors.TimeCheatingDetector::timeServer
	String_t* ___timeServer_41;

public:
	inline static int32_t get_offset_of_CheatChecked_21() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___CheatChecked_21)); }
	inline TimeCheatingDetectorEventHandler_t23FE998F4C6E3C8DF608BE057ACD82476B766D29 * get_CheatChecked_21() const { return ___CheatChecked_21; }
	inline TimeCheatingDetectorEventHandler_t23FE998F4C6E3C8DF608BE057ACD82476B766D29 ** get_address_of_CheatChecked_21() { return &___CheatChecked_21; }
	inline void set_CheatChecked_21(TimeCheatingDetectorEventHandler_t23FE998F4C6E3C8DF608BE057ACD82476B766D29 * value)
	{
		___CheatChecked_21 = value;
		Il2CppCodeGenWriteBarrier((&___CheatChecked_21), value);
	}

	inline static int32_t get_offset_of_requestUrl_22() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___requestUrl_22)); }
	inline String_t* get_requestUrl_22() const { return ___requestUrl_22; }
	inline String_t** get_address_of_requestUrl_22() { return &___requestUrl_22; }
	inline void set_requestUrl_22(String_t* value)
	{
		___requestUrl_22 = value;
		Il2CppCodeGenWriteBarrier((&___requestUrl_22), value);
	}

	inline static int32_t get_offset_of_requestMethod_23() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___requestMethod_23)); }
	inline int32_t get_requestMethod_23() const { return ___requestMethod_23; }
	inline int32_t* get_address_of_requestMethod_23() { return &___requestMethod_23; }
	inline void set_requestMethod_23(int32_t value)
	{
		___requestMethod_23 = value;
	}

	inline static int32_t get_offset_of_timeoutSeconds_24() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___timeoutSeconds_24)); }
	inline int32_t get_timeoutSeconds_24() const { return ___timeoutSeconds_24; }
	inline int32_t* get_address_of_timeoutSeconds_24() { return &___timeoutSeconds_24; }
	inline void set_timeoutSeconds_24(int32_t value)
	{
		___timeoutSeconds_24 = value;
	}

	inline static int32_t get_offset_of_interval_25() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___interval_25)); }
	inline float get_interval_25() const { return ___interval_25; }
	inline float* get_address_of_interval_25() { return &___interval_25; }
	inline void set_interval_25(float value)
	{
		___interval_25 = value;
	}

	inline static int32_t get_offset_of_realCheatThreshold_26() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___realCheatThreshold_26)); }
	inline int32_t get_realCheatThreshold_26() const { return ___realCheatThreshold_26; }
	inline int32_t* get_address_of_realCheatThreshold_26() { return &___realCheatThreshold_26; }
	inline void set_realCheatThreshold_26(int32_t value)
	{
		___realCheatThreshold_26 = value;
	}

	inline static int32_t get_offset_of_wrongTimeThreshold_27() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___wrongTimeThreshold_27)); }
	inline int32_t get_wrongTimeThreshold_27() const { return ___wrongTimeThreshold_27; }
	inline int32_t* get_address_of_wrongTimeThreshold_27() { return &___wrongTimeThreshold_27; }
	inline void set_wrongTimeThreshold_27(int32_t value)
	{
		___wrongTimeThreshold_27 = value;
	}

	inline static int32_t get_offset_of_ignoreSetCorrectTime_28() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___ignoreSetCorrectTime_28)); }
	inline bool get_ignoreSetCorrectTime_28() const { return ___ignoreSetCorrectTime_28; }
	inline bool* get_address_of_ignoreSetCorrectTime_28() { return &___ignoreSetCorrectTime_28; }
	inline void set_ignoreSetCorrectTime_28(bool value)
	{
		___ignoreSetCorrectTime_28 = value;
	}

	inline static int32_t get_offset_of_U3CLastErrorU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___U3CLastErrorU3Ek__BackingField_29)); }
	inline int32_t get_U3CLastErrorU3Ek__BackingField_29() const { return ___U3CLastErrorU3Ek__BackingField_29; }
	inline int32_t* get_address_of_U3CLastErrorU3Ek__BackingField_29() { return &___U3CLastErrorU3Ek__BackingField_29; }
	inline void set_U3CLastErrorU3Ek__BackingField_29(int32_t value)
	{
		___U3CLastErrorU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CLastResultU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___U3CLastResultU3Ek__BackingField_30)); }
	inline int32_t get_U3CLastResultU3Ek__BackingField_30() const { return ___U3CLastResultU3Ek__BackingField_30; }
	inline int32_t* get_address_of_U3CLastResultU3Ek__BackingField_30() { return &___U3CLastResultU3Ek__BackingField_30; }
	inline void set_U3CLastResultU3Ek__BackingField_30(int32_t value)
	{
		___U3CLastResultU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CIsCheckingForCheatU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___U3CIsCheckingForCheatU3Ek__BackingField_31)); }
	inline bool get_U3CIsCheckingForCheatU3Ek__BackingField_31() const { return ___U3CIsCheckingForCheatU3Ek__BackingField_31; }
	inline bool* get_address_of_U3CIsCheckingForCheatU3Ek__BackingField_31() { return &___U3CIsCheckingForCheatU3Ek__BackingField_31; }
	inline void set_U3CIsCheckingForCheatU3Ek__BackingField_31(bool value)
	{
		___U3CIsCheckingForCheatU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_onlineOfflineDifferencePrefsKey_32() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___onlineOfflineDifferencePrefsKey_32)); }
	inline String_t* get_onlineOfflineDifferencePrefsKey_32() const { return ___onlineOfflineDifferencePrefsKey_32; }
	inline String_t** get_address_of_onlineOfflineDifferencePrefsKey_32() { return &___onlineOfflineDifferencePrefsKey_32; }
	inline void set_onlineOfflineDifferencePrefsKey_32(String_t* value)
	{
		___onlineOfflineDifferencePrefsKey_32 = value;
		Il2CppCodeGenWriteBarrier((&___onlineOfflineDifferencePrefsKey_32), value);
	}

	inline static int32_t get_offset_of_cachedUri_33() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___cachedUri_33)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_cachedUri_33() const { return ___cachedUri_33; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_cachedUri_33() { return &___cachedUri_33; }
	inline void set_cachedUri_33(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___cachedUri_33 = value;
		Il2CppCodeGenWriteBarrier((&___cachedUri_33), value);
	}

	inline static int32_t get_offset_of_cheatChecked_34() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___cheatChecked_34)); }
	inline TimeCheatingDetectorEventHandler_t23FE998F4C6E3C8DF608BE057ACD82476B766D29 * get_cheatChecked_34() const { return ___cheatChecked_34; }
	inline TimeCheatingDetectorEventHandler_t23FE998F4C6E3C8DF608BE057ACD82476B766D29 ** get_address_of_cheatChecked_34() { return &___cheatChecked_34; }
	inline void set_cheatChecked_34(TimeCheatingDetectorEventHandler_t23FE998F4C6E3C8DF608BE057ACD82476B766D29 * value)
	{
		___cheatChecked_34 = value;
		Il2CppCodeGenWriteBarrier((&___cheatChecked_34), value);
	}

	inline static int32_t get_offset_of_timeElapsed_35() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___timeElapsed_35)); }
	inline float get_timeElapsed_35() const { return ___timeElapsed_35; }
	inline float* get_address_of_timeElapsed_35() { return &___timeElapsed_35; }
	inline void set_timeElapsed_35(float value)
	{
		___timeElapsed_35 = value;
	}

	inline static int32_t get_offset_of_updateAfterPause_36() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___updateAfterPause_36)); }
	inline bool get_updateAfterPause_36() const { return ___updateAfterPause_36; }
	inline bool* get_address_of_updateAfterPause_36() { return &___updateAfterPause_36; }
	inline void set_updateAfterPause_36(bool value)
	{
		___updateAfterPause_36 = value;
	}

	inline static int32_t get_offset_of_lastOnlineSecondsUtc_37() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___lastOnlineSecondsUtc_37)); }
	inline double get_lastOnlineSecondsUtc_37() const { return ___lastOnlineSecondsUtc_37; }
	inline double* get_address_of_lastOnlineSecondsUtc_37() { return &___lastOnlineSecondsUtc_37; }
	inline void set_lastOnlineSecondsUtc_37(double value)
	{
		___lastOnlineSecondsUtc_37 = value;
	}

	inline static int32_t get_offset_of_Error_38() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___Error_38)); }
	inline Action_1_tF98CA887A7F4745726B4B140A2BEBE05A25F2554 * get_Error_38() const { return ___Error_38; }
	inline Action_1_tF98CA887A7F4745726B4B140A2BEBE05A25F2554 ** get_address_of_Error_38() { return &___Error_38; }
	inline void set_Error_38(Action_1_tF98CA887A7F4745726B4B140A2BEBE05A25F2554 * value)
	{
		___Error_38 = value;
		Il2CppCodeGenWriteBarrier((&___Error_38), value);
	}

	inline static int32_t get_offset_of_CheckPassed_39() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___CheckPassed_39)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_CheckPassed_39() const { return ___CheckPassed_39; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_CheckPassed_39() { return &___CheckPassed_39; }
	inline void set_CheckPassed_39(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___CheckPassed_39 = value;
		Il2CppCodeGenWriteBarrier((&___CheckPassed_39), value);
	}

	inline static int32_t get_offset_of_threshold_40() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___threshold_40)); }
	inline int32_t get_threshold_40() const { return ___threshold_40; }
	inline int32_t* get_address_of_threshold_40() { return &___threshold_40; }
	inline void set_threshold_40(int32_t value)
	{
		___threshold_40 = value;
	}

	inline static int32_t get_offset_of_timeServer_41() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65, ___timeServer_41)); }
	inline String_t* get_timeServer_41() const { return ___timeServer_41; }
	inline String_t** get_address_of_timeServer_41() { return &___timeServer_41; }
	inline void set_timeServer_41(String_t* value)
	{
		___timeServer_41 = value;
		Il2CppCodeGenWriteBarrier((&___timeServer_41), value);
	}
};

struct TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65_StaticFields
{
public:
	// UnityEngine.WaitForEndOfFrame CodeStage.AntiCheat.Detectors.TimeCheatingDetector::CachedEndOfFrame
	WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * ___CachedEndOfFrame_19;
	// System.Boolean CodeStage.AntiCheat.Detectors.TimeCheatingDetector::gettingOnlineTime
	bool ___gettingOnlineTime_20;

public:
	inline static int32_t get_offset_of_CachedEndOfFrame_19() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65_StaticFields, ___CachedEndOfFrame_19)); }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * get_CachedEndOfFrame_19() const { return ___CachedEndOfFrame_19; }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA ** get_address_of_CachedEndOfFrame_19() { return &___CachedEndOfFrame_19; }
	inline void set_CachedEndOfFrame_19(WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * value)
	{
		___CachedEndOfFrame_19 = value;
		Il2CppCodeGenWriteBarrier((&___CachedEndOfFrame_19), value);
	}

	inline static int32_t get_offset_of_gettingOnlineTime_20() { return static_cast<int32_t>(offsetof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65_StaticFields, ___gettingOnlineTime_20)); }
	inline bool get_gettingOnlineTime_20() const { return ___gettingOnlineTime_20; }
	inline bool* get_address_of_gettingOnlineTime_20() { return &___gettingOnlineTime_20; }
	inline void set_gettingOnlineTime_20(bool value)
	{
		___gettingOnlineTime_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMECHEATINGDETECTOR_T4014D5D2CD0E66A64471222FD6C2266962791B65_H
#ifndef WALLHACKDETECTOR_T4A00BE755A4156DD2624624E6F674E158EE41590_H
#define WALLHACKDETECTOR_T4A00BE755A4156DD2624624E6F674E158EE41590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CodeStage.AntiCheat.Detectors.WallHackDetector
struct  WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590  : public ACTkDetectorBase_1_tE2A24D5EFB3C34378E36E9B26754EDF94D9E2BA3
{
public:
	// UnityEngine.Vector3 CodeStage.AntiCheat.Detectors.WallHackDetector::rigidPlayerVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rigidPlayerVelocity_23;
	// UnityEngine.WaitForEndOfFrame CodeStage.AntiCheat.Detectors.WallHackDetector::waitForEndOfFrame
	WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * ___waitForEndOfFrame_24;
	// System.Boolean CodeStage.AntiCheat.Detectors.WallHackDetector::checkRigidbody
	bool ___checkRigidbody_25;
	// System.Boolean CodeStage.AntiCheat.Detectors.WallHackDetector::checkController
	bool ___checkController_26;
	// System.Boolean CodeStage.AntiCheat.Detectors.WallHackDetector::checkWireframe
	bool ___checkWireframe_27;
	// System.Boolean CodeStage.AntiCheat.Detectors.WallHackDetector::checkRaycast
	bool ___checkRaycast_28;
	// System.Int32 CodeStage.AntiCheat.Detectors.WallHackDetector::wireframeDelay
	int32_t ___wireframeDelay_29;
	// System.Int32 CodeStage.AntiCheat.Detectors.WallHackDetector::raycastDelay
	int32_t ___raycastDelay_30;
	// UnityEngine.Vector3 CodeStage.AntiCheat.Detectors.WallHackDetector::spawnPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___spawnPosition_31;
	// System.Byte CodeStage.AntiCheat.Detectors.WallHackDetector::maxFalsePositives
	uint8_t ___maxFalsePositives_32;
	// UnityEngine.GameObject CodeStage.AntiCheat.Detectors.WallHackDetector::serviceContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___serviceContainer_33;
	// UnityEngine.GameObject CodeStage.AntiCheat.Detectors.WallHackDetector::solidWall
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___solidWall_34;
	// UnityEngine.GameObject CodeStage.AntiCheat.Detectors.WallHackDetector::thinWall
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___thinWall_35;
	// UnityEngine.Camera CodeStage.AntiCheat.Detectors.WallHackDetector::wfCamera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___wfCamera_36;
	// UnityEngine.MeshRenderer CodeStage.AntiCheat.Detectors.WallHackDetector::foregroundRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___foregroundRenderer_37;
	// UnityEngine.MeshRenderer CodeStage.AntiCheat.Detectors.WallHackDetector::backgroundRenderer
	MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * ___backgroundRenderer_38;
	// UnityEngine.Color CodeStage.AntiCheat.Detectors.WallHackDetector::wfColor1
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___wfColor1_39;
	// UnityEngine.Color CodeStage.AntiCheat.Detectors.WallHackDetector::wfColor2
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___wfColor2_40;
	// UnityEngine.Shader CodeStage.AntiCheat.Detectors.WallHackDetector::wfShader
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___wfShader_41;
	// UnityEngine.Material CodeStage.AntiCheat.Detectors.WallHackDetector::wfMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___wfMaterial_42;
	// UnityEngine.Texture2D CodeStage.AntiCheat.Detectors.WallHackDetector::shaderTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___shaderTexture_43;
	// UnityEngine.Texture2D CodeStage.AntiCheat.Detectors.WallHackDetector::targetTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___targetTexture_44;
	// UnityEngine.RenderTexture CodeStage.AntiCheat.Detectors.WallHackDetector::renderTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___renderTexture_45;
	// System.Int32 CodeStage.AntiCheat.Detectors.WallHackDetector::whLayer
	int32_t ___whLayer_46;
	// System.Int32 CodeStage.AntiCheat.Detectors.WallHackDetector::raycastMask
	int32_t ___raycastMask_47;
	// UnityEngine.Rigidbody CodeStage.AntiCheat.Detectors.WallHackDetector::rigidPlayer
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___rigidPlayer_48;
	// UnityEngine.CharacterController CodeStage.AntiCheat.Detectors.WallHackDetector::charControllerPlayer
	CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * ___charControllerPlayer_49;
	// System.Single CodeStage.AntiCheat.Detectors.WallHackDetector::charControllerVelocity
	float ___charControllerVelocity_50;
	// System.Byte CodeStage.AntiCheat.Detectors.WallHackDetector::rigidbodyDetections
	uint8_t ___rigidbodyDetections_51;
	// System.Byte CodeStage.AntiCheat.Detectors.WallHackDetector::controllerDetections
	uint8_t ___controllerDetections_52;
	// System.Byte CodeStage.AntiCheat.Detectors.WallHackDetector::wireframeDetections
	uint8_t ___wireframeDetections_53;
	// System.Byte CodeStage.AntiCheat.Detectors.WallHackDetector::raycastDetections
	uint8_t ___raycastDetections_54;
	// System.Boolean CodeStage.AntiCheat.Detectors.WallHackDetector::wireframeDetected
	bool ___wireframeDetected_55;
	// UnityEngine.RaycastHit[] CodeStage.AntiCheat.Detectors.WallHackDetector::rayHits
	RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* ___rayHits_56;

public:
	inline static int32_t get_offset_of_rigidPlayerVelocity_23() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___rigidPlayerVelocity_23)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rigidPlayerVelocity_23() const { return ___rigidPlayerVelocity_23; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rigidPlayerVelocity_23() { return &___rigidPlayerVelocity_23; }
	inline void set_rigidPlayerVelocity_23(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rigidPlayerVelocity_23 = value;
	}

	inline static int32_t get_offset_of_waitForEndOfFrame_24() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___waitForEndOfFrame_24)); }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * get_waitForEndOfFrame_24() const { return ___waitForEndOfFrame_24; }
	inline WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA ** get_address_of_waitForEndOfFrame_24() { return &___waitForEndOfFrame_24; }
	inline void set_waitForEndOfFrame_24(WaitForEndOfFrame_t75980FB3F246D6AD36A85CA2BFDF8474E5EEBCCA * value)
	{
		___waitForEndOfFrame_24 = value;
		Il2CppCodeGenWriteBarrier((&___waitForEndOfFrame_24), value);
	}

	inline static int32_t get_offset_of_checkRigidbody_25() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___checkRigidbody_25)); }
	inline bool get_checkRigidbody_25() const { return ___checkRigidbody_25; }
	inline bool* get_address_of_checkRigidbody_25() { return &___checkRigidbody_25; }
	inline void set_checkRigidbody_25(bool value)
	{
		___checkRigidbody_25 = value;
	}

	inline static int32_t get_offset_of_checkController_26() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___checkController_26)); }
	inline bool get_checkController_26() const { return ___checkController_26; }
	inline bool* get_address_of_checkController_26() { return &___checkController_26; }
	inline void set_checkController_26(bool value)
	{
		___checkController_26 = value;
	}

	inline static int32_t get_offset_of_checkWireframe_27() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___checkWireframe_27)); }
	inline bool get_checkWireframe_27() const { return ___checkWireframe_27; }
	inline bool* get_address_of_checkWireframe_27() { return &___checkWireframe_27; }
	inline void set_checkWireframe_27(bool value)
	{
		___checkWireframe_27 = value;
	}

	inline static int32_t get_offset_of_checkRaycast_28() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___checkRaycast_28)); }
	inline bool get_checkRaycast_28() const { return ___checkRaycast_28; }
	inline bool* get_address_of_checkRaycast_28() { return &___checkRaycast_28; }
	inline void set_checkRaycast_28(bool value)
	{
		___checkRaycast_28 = value;
	}

	inline static int32_t get_offset_of_wireframeDelay_29() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___wireframeDelay_29)); }
	inline int32_t get_wireframeDelay_29() const { return ___wireframeDelay_29; }
	inline int32_t* get_address_of_wireframeDelay_29() { return &___wireframeDelay_29; }
	inline void set_wireframeDelay_29(int32_t value)
	{
		___wireframeDelay_29 = value;
	}

	inline static int32_t get_offset_of_raycastDelay_30() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___raycastDelay_30)); }
	inline int32_t get_raycastDelay_30() const { return ___raycastDelay_30; }
	inline int32_t* get_address_of_raycastDelay_30() { return &___raycastDelay_30; }
	inline void set_raycastDelay_30(int32_t value)
	{
		___raycastDelay_30 = value;
	}

	inline static int32_t get_offset_of_spawnPosition_31() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___spawnPosition_31)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_spawnPosition_31() const { return ___spawnPosition_31; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_spawnPosition_31() { return &___spawnPosition_31; }
	inline void set_spawnPosition_31(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___spawnPosition_31 = value;
	}

	inline static int32_t get_offset_of_maxFalsePositives_32() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___maxFalsePositives_32)); }
	inline uint8_t get_maxFalsePositives_32() const { return ___maxFalsePositives_32; }
	inline uint8_t* get_address_of_maxFalsePositives_32() { return &___maxFalsePositives_32; }
	inline void set_maxFalsePositives_32(uint8_t value)
	{
		___maxFalsePositives_32 = value;
	}

	inline static int32_t get_offset_of_serviceContainer_33() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___serviceContainer_33)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_serviceContainer_33() const { return ___serviceContainer_33; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_serviceContainer_33() { return &___serviceContainer_33; }
	inline void set_serviceContainer_33(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___serviceContainer_33 = value;
		Il2CppCodeGenWriteBarrier((&___serviceContainer_33), value);
	}

	inline static int32_t get_offset_of_solidWall_34() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___solidWall_34)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_solidWall_34() const { return ___solidWall_34; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_solidWall_34() { return &___solidWall_34; }
	inline void set_solidWall_34(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___solidWall_34 = value;
		Il2CppCodeGenWriteBarrier((&___solidWall_34), value);
	}

	inline static int32_t get_offset_of_thinWall_35() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___thinWall_35)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_thinWall_35() const { return ___thinWall_35; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_thinWall_35() { return &___thinWall_35; }
	inline void set_thinWall_35(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___thinWall_35 = value;
		Il2CppCodeGenWriteBarrier((&___thinWall_35), value);
	}

	inline static int32_t get_offset_of_wfCamera_36() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___wfCamera_36)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_wfCamera_36() const { return ___wfCamera_36; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_wfCamera_36() { return &___wfCamera_36; }
	inline void set_wfCamera_36(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___wfCamera_36 = value;
		Il2CppCodeGenWriteBarrier((&___wfCamera_36), value);
	}

	inline static int32_t get_offset_of_foregroundRenderer_37() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___foregroundRenderer_37)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_foregroundRenderer_37() const { return ___foregroundRenderer_37; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_foregroundRenderer_37() { return &___foregroundRenderer_37; }
	inline void set_foregroundRenderer_37(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___foregroundRenderer_37 = value;
		Il2CppCodeGenWriteBarrier((&___foregroundRenderer_37), value);
	}

	inline static int32_t get_offset_of_backgroundRenderer_38() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___backgroundRenderer_38)); }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * get_backgroundRenderer_38() const { return ___backgroundRenderer_38; }
	inline MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED ** get_address_of_backgroundRenderer_38() { return &___backgroundRenderer_38; }
	inline void set_backgroundRenderer_38(MeshRenderer_t9D67CA54E83315F743623BDE8EADCD5074659EED * value)
	{
		___backgroundRenderer_38 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundRenderer_38), value);
	}

	inline static int32_t get_offset_of_wfColor1_39() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___wfColor1_39)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_wfColor1_39() const { return ___wfColor1_39; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_wfColor1_39() { return &___wfColor1_39; }
	inline void set_wfColor1_39(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___wfColor1_39 = value;
	}

	inline static int32_t get_offset_of_wfColor2_40() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___wfColor2_40)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_wfColor2_40() const { return ___wfColor2_40; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_wfColor2_40() { return &___wfColor2_40; }
	inline void set_wfColor2_40(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___wfColor2_40 = value;
	}

	inline static int32_t get_offset_of_wfShader_41() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___wfShader_41)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_wfShader_41() const { return ___wfShader_41; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_wfShader_41() { return &___wfShader_41; }
	inline void set_wfShader_41(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___wfShader_41 = value;
		Il2CppCodeGenWriteBarrier((&___wfShader_41), value);
	}

	inline static int32_t get_offset_of_wfMaterial_42() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___wfMaterial_42)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_wfMaterial_42() const { return ___wfMaterial_42; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_wfMaterial_42() { return &___wfMaterial_42; }
	inline void set_wfMaterial_42(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___wfMaterial_42 = value;
		Il2CppCodeGenWriteBarrier((&___wfMaterial_42), value);
	}

	inline static int32_t get_offset_of_shaderTexture_43() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___shaderTexture_43)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_shaderTexture_43() const { return ___shaderTexture_43; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_shaderTexture_43() { return &___shaderTexture_43; }
	inline void set_shaderTexture_43(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___shaderTexture_43 = value;
		Il2CppCodeGenWriteBarrier((&___shaderTexture_43), value);
	}

	inline static int32_t get_offset_of_targetTexture_44() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___targetTexture_44)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_targetTexture_44() const { return ___targetTexture_44; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_targetTexture_44() { return &___targetTexture_44; }
	inline void set_targetTexture_44(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___targetTexture_44 = value;
		Il2CppCodeGenWriteBarrier((&___targetTexture_44), value);
	}

	inline static int32_t get_offset_of_renderTexture_45() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___renderTexture_45)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get_renderTexture_45() const { return ___renderTexture_45; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of_renderTexture_45() { return &___renderTexture_45; }
	inline void set_renderTexture_45(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		___renderTexture_45 = value;
		Il2CppCodeGenWriteBarrier((&___renderTexture_45), value);
	}

	inline static int32_t get_offset_of_whLayer_46() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___whLayer_46)); }
	inline int32_t get_whLayer_46() const { return ___whLayer_46; }
	inline int32_t* get_address_of_whLayer_46() { return &___whLayer_46; }
	inline void set_whLayer_46(int32_t value)
	{
		___whLayer_46 = value;
	}

	inline static int32_t get_offset_of_raycastMask_47() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___raycastMask_47)); }
	inline int32_t get_raycastMask_47() const { return ___raycastMask_47; }
	inline int32_t* get_address_of_raycastMask_47() { return &___raycastMask_47; }
	inline void set_raycastMask_47(int32_t value)
	{
		___raycastMask_47 = value;
	}

	inline static int32_t get_offset_of_rigidPlayer_48() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___rigidPlayer_48)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_rigidPlayer_48() const { return ___rigidPlayer_48; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_rigidPlayer_48() { return &___rigidPlayer_48; }
	inline void set_rigidPlayer_48(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___rigidPlayer_48 = value;
		Il2CppCodeGenWriteBarrier((&___rigidPlayer_48), value);
	}

	inline static int32_t get_offset_of_charControllerPlayer_49() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___charControllerPlayer_49)); }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * get_charControllerPlayer_49() const { return ___charControllerPlayer_49; }
	inline CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E ** get_address_of_charControllerPlayer_49() { return &___charControllerPlayer_49; }
	inline void set_charControllerPlayer_49(CharacterController_t0ED98F461DBB7AC5B189C190153D83D5888BF93E * value)
	{
		___charControllerPlayer_49 = value;
		Il2CppCodeGenWriteBarrier((&___charControllerPlayer_49), value);
	}

	inline static int32_t get_offset_of_charControllerVelocity_50() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___charControllerVelocity_50)); }
	inline float get_charControllerVelocity_50() const { return ___charControllerVelocity_50; }
	inline float* get_address_of_charControllerVelocity_50() { return &___charControllerVelocity_50; }
	inline void set_charControllerVelocity_50(float value)
	{
		___charControllerVelocity_50 = value;
	}

	inline static int32_t get_offset_of_rigidbodyDetections_51() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___rigidbodyDetections_51)); }
	inline uint8_t get_rigidbodyDetections_51() const { return ___rigidbodyDetections_51; }
	inline uint8_t* get_address_of_rigidbodyDetections_51() { return &___rigidbodyDetections_51; }
	inline void set_rigidbodyDetections_51(uint8_t value)
	{
		___rigidbodyDetections_51 = value;
	}

	inline static int32_t get_offset_of_controllerDetections_52() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___controllerDetections_52)); }
	inline uint8_t get_controllerDetections_52() const { return ___controllerDetections_52; }
	inline uint8_t* get_address_of_controllerDetections_52() { return &___controllerDetections_52; }
	inline void set_controllerDetections_52(uint8_t value)
	{
		___controllerDetections_52 = value;
	}

	inline static int32_t get_offset_of_wireframeDetections_53() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___wireframeDetections_53)); }
	inline uint8_t get_wireframeDetections_53() const { return ___wireframeDetections_53; }
	inline uint8_t* get_address_of_wireframeDetections_53() { return &___wireframeDetections_53; }
	inline void set_wireframeDetections_53(uint8_t value)
	{
		___wireframeDetections_53 = value;
	}

	inline static int32_t get_offset_of_raycastDetections_54() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___raycastDetections_54)); }
	inline uint8_t get_raycastDetections_54() const { return ___raycastDetections_54; }
	inline uint8_t* get_address_of_raycastDetections_54() { return &___raycastDetections_54; }
	inline void set_raycastDetections_54(uint8_t value)
	{
		___raycastDetections_54 = value;
	}

	inline static int32_t get_offset_of_wireframeDetected_55() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___wireframeDetected_55)); }
	inline bool get_wireframeDetected_55() const { return ___wireframeDetected_55; }
	inline bool* get_address_of_wireframeDetected_55() { return &___wireframeDetected_55; }
	inline void set_wireframeDetected_55(bool value)
	{
		___wireframeDetected_55 = value;
	}

	inline static int32_t get_offset_of_rayHits_56() { return static_cast<int32_t>(offsetof(WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590, ___rayHits_56)); }
	inline RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* get_rayHits_56() const { return ___rayHits_56; }
	inline RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57** get_address_of_rayHits_56() { return &___rayHits_56; }
	inline void set_rayHits_56(RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57* value)
	{
		___rayHits_56 = value;
		Il2CppCodeGenWriteBarrier((&___rayHits_56), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WALLHACKDETECTOR_T4A00BE755A4156DD2624624E6F674E158EE41590_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6500 = { sizeof (RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627)+ sizeof (RuntimeObject), sizeof(RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6500[3] = 
{
	RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RawEncryptedVector3_t766A78183852F7A89484E372DBFD15EEF76AB627::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6501 = { sizeof (ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD)+ sizeof (RuntimeObject), sizeof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD_marshaled_pinvoke), sizeof(ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6501[6] = 
{
	ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD_StaticFields::get_offset_of_Zero_0(),
	ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD::get_offset_of_currentCryptoKey_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD::get_offset_of_hiddenValue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD::get_offset_of_inited_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD::get_offset_of_fakeValue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ObscuredVector3Int_t4E47D68D29A1D228D29EC0300213FEB7D891C0BD::get_offset_of_fakeValueActive_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6502 = { sizeof (RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3)+ sizeof (RuntimeObject), sizeof(RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6502[3] = 
{
	RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	RawEncryptedVector3Int_tFC038D95C234B20C841F481FAB1B435C389129D3::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6503 = { sizeof (CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480), -1, sizeof(CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6503[4] = 
{
	CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480_StaticFields::get_offset_of_HashGenerated_7(),
	CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480::get_offset_of_U3CLastResultU3Ek__BackingField_8(),
	CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480::get_offset_of_cachedWaitForSeconds_9(),
	CodeHashGenerator_t8375A0FF3F16EAB1DB67961AD53ADE9A2D184480::get_offset_of_currentWorker_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6504 = { sizeof (U3CCalculationAwaiterU3Ed__20_t1976E62BC5F5EF7115BE28363866365316D26084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6504[3] = 
{
	U3CCalculationAwaiterU3Ed__20_t1976E62BC5F5EF7115BE28363866365316D26084::get_offset_of_U3CU3E1__state_0(),
	U3CCalculationAwaiterU3Ed__20_t1976E62BC5F5EF7115BE28363866365316D26084::get_offset_of_U3CU3E2__current_1(),
	U3CCalculationAwaiterU3Ed__20_t1976E62BC5F5EF7115BE28363866365316D26084::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6505 = { sizeof (FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6505[7] = 
{
	FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28::get_offset_of_caseSensitive_0(),
	FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28::get_offset_of_folderRecursive_1(),
	FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28::get_offset_of_exactFileNameMatch_2(),
	FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28::get_offset_of_exactFolderMatch_3(),
	FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28::get_offset_of_filterFolder_4(),
	FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28::get_offset_of_filterExtension_5(),
	FileFilter_t73AB59B0F9AE3CCB7FE4B06EEC64187ECF1EAB28::get_offset_of_filterFileName_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6506 = { sizeof (HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6506[2] = 
{
	HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7::get_offset_of_U3CCodeHashU3Ek__BackingField_0(),
	HashGeneratorResult_t0F9CF4CE29046BF1C2C4A909FA12370F0D5FECE7::get_offset_of_U3CErrorMessageU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6507 = { sizeof (HashGeneratorResultHandler_tD7EA83E3F9BE89BA5F6FC201F7CF915EE9B31A2E), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6508 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6509 = { sizeof (BaseWorker_tD3B5F0035F406307CFD71F8BCF644A3D81A1036E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6509[2] = 
{
	BaseWorker_tD3B5F0035F406307CFD71F8BCF644A3D81A1036E::get_offset_of_U3CResultU3Ek__BackingField_0(),
	BaseWorker_tD3B5F0035F406307CFD71F8BCF644A3D81A1036E::get_offset_of_U3CIsBusyU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6510 = { sizeof (StandaloneWindowsWorker_tF493FB75FD28A9170738009C890FE4D327E8E666), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6511 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6511[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6512 = { sizeof (InjectionDetector_tB402442AA73D35344CEF9C14C806583C37BD610D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6512[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6513 = { sizeof (InjectionDetectedEventHandler_tC2CA209DEE2B887A8AFFA0E73EBDD507ABF343B1), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6514 = { sizeof (ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6514[7] = 
{
	0,
	0,
	ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601::get_offset_of_doubleEpsilon_18(),
	ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601::get_offset_of_floatEpsilon_19(),
	ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601::get_offset_of_vector2Epsilon_20(),
	ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601::get_offset_of_vector3Epsilon_21(),
	ObscuredCheatingDetector_t486DBF80B2EE2FE3520DA67CF45A1618352CA601::get_offset_of_quaternionEpsilon_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6515 = { sizeof (SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6515[11] = 
{
	0,
	0,
	SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE::get_offset_of_interval_18(),
	SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE::get_offset_of_threshold_19(),
	SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE::get_offset_of_maxFalsePositives_20(),
	SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE::get_offset_of_coolDown_21(),
	SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE::get_offset_of_currentFalsePositives_22(),
	SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE::get_offset_of_currentCooldownShots_23(),
	SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE::get_offset_of_previousReliableTicks_24(),
	SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE::get_offset_of_previousVulnerableEnvironmentTicks_25(),
	SpeedHackDetector_tCF5D7139EF4D5B1345E222A85570F96A1A8430FE::get_offset_of_previousVulnerableRealtimeTicks_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6516 = { sizeof (TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65), -1, sizeof(TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6516[26] = 
{
	0,
	0,
	0,
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65_StaticFields::get_offset_of_CachedEndOfFrame_19(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65_StaticFields::get_offset_of_gettingOnlineTime_20(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_CheatChecked_21(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_requestUrl_22(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_requestMethod_23(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_timeoutSeconds_24(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_interval_25(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_realCheatThreshold_26(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_wrongTimeThreshold_27(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_ignoreSetCorrectTime_28(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_U3CLastErrorU3Ek__BackingField_29(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_U3CLastResultU3Ek__BackingField_30(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_U3CIsCheckingForCheatU3Ek__BackingField_31(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_onlineOfflineDifferencePrefsKey_32(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_cachedUri_33(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_cheatChecked_34(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_timeElapsed_35(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_updateAfterPause_36(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_lastOnlineSecondsUtc_37(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_Error_38(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_CheckPassed_39(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_threshold_40(),
	TimeCheatingDetector_t4014D5D2CD0E66A64471222FD6C2266962791B65::get_offset_of_timeServer_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6517 = { sizeof (OnlineTimeCallback_t5AA2B75F108EB462BC1D2C2E38CF960AE3434D34), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6518 = { sizeof (TimeCheatingDetectorEventHandler_t23FE998F4C6E3C8DF608BE057ACD82476B766D29), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6519 = { sizeof (OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D)+ sizeof (RuntimeObject), sizeof(OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6519[4] = 
{
	OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D::get_offset_of_success_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D::get_offset_of_error_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D::get_offset_of_errorResponseCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	OnlineTimeResult_t7B01E78EE0B7D72C97AD8F2470B481762DECF94D::get_offset_of_onlineSecondsUtc_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6520 = { sizeof (CheckResult_t5370C9CFB3349644677B3E4598C26C274743486D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6520[6] = 
{
	CheckResult_t5370C9CFB3349644677B3E4598C26C274743486D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6521 = { sizeof (ErrorKind_t70F1939191471D9D9CD977DC0DD9E55582AE2B44)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6521[7] = 
{
	ErrorKind_t70F1939191471D9D9CD977DC0DD9E55582AE2B44::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6522 = { sizeof (RequestMethod_t8184DA00E2BEEDEE288C2F1EFE117D515B8F6C35)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6522[3] = 
{
	RequestMethod_t8184DA00E2BEEDEE288C2F1EFE117D515B8F6C35::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6523 = { sizeof (U3CGetOnlineTimeCoroutineU3Ed__49_tD7F6B839D41FBC936A43BDAE662892DB310F8ED8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6523[5] = 
{
	U3CGetOnlineTimeCoroutineU3Ed__49_tD7F6B839D41FBC936A43BDAE662892DB310F8ED8::get_offset_of_U3CU3E1__state_0(),
	U3CGetOnlineTimeCoroutineU3Ed__49_tD7F6B839D41FBC936A43BDAE662892DB310F8ED8::get_offset_of_U3CU3E2__current_1(),
	U3CGetOnlineTimeCoroutineU3Ed__49_tD7F6B839D41FBC936A43BDAE662892DB310F8ED8::get_offset_of_url_2(),
	U3CGetOnlineTimeCoroutineU3Ed__49_tD7F6B839D41FBC936A43BDAE662892DB310F8ED8::get_offset_of_callback_3(),
	U3CGetOnlineTimeCoroutineU3Ed__49_tD7F6B839D41FBC936A43BDAE662892DB310F8ED8::get_offset_of_method_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6524 = { sizeof (U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6524[7] = 
{
	U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E::get_offset_of_U3CU3E1__state_0(),
	U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E::get_offset_of_U3CU3E2__current_1(),
	U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E::get_offset_of_uri_2(),
	U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E::get_offset_of_method_3(),
	U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E::get_offset_of_callback_4(),
	U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E::get_offset_of_U3CresultU3E5__2_5(),
	U3CGetOnlineTimeCoroutineU3Ed__50_t35CA5ADEB47F29347B8CC79DED994AB8896D4D3E::get_offset_of_U3CwrU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6525 = { sizeof (U3CGetOnlineTimeTaskU3Ed__51_t106FECD8E78B6511C37B819838D037373BBCE489)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6525[5] = 
{
	U3CGetOnlineTimeTaskU3Ed__51_t106FECD8E78B6511C37B819838D037373BBCE489::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetOnlineTimeTaskU3Ed__51_t106FECD8E78B6511C37B819838D037373BBCE489::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetOnlineTimeTaskU3Ed__51_t106FECD8E78B6511C37B819838D037373BBCE489::get_offset_of_url_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetOnlineTimeTaskU3Ed__51_t106FECD8E78B6511C37B819838D037373BBCE489::get_offset_of_method_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetOnlineTimeTaskU3Ed__51_t106FECD8E78B6511C37B819838D037373BBCE489::get_offset_of_U3CU3Eu__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6526 = { sizeof (U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6526[8] = 
{
	U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134::get_offset_of_uri_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134::get_offset_of_method_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134::get_offset_of_U3CresultU3E5__2_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134::get_offset_of_U3CU3Eu__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134::get_offset_of_U3CwrU3E5__3_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CGetOnlineTimeTaskU3Ed__52_t4C3A910AED19F5E424DEE86B8BAF7C49EC433134::get_offset_of_U3CasyncOperationU3E5__4_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6527 = { sizeof (U3CForceCheckEnumeratorU3Ed__58_tB83CC76710450C730B37A01FDEE5E33C17EA64F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6527[3] = 
{
	U3CForceCheckEnumeratorU3Ed__58_tB83CC76710450C730B37A01FDEE5E33C17EA64F3::get_offset_of_U3CU3E1__state_0(),
	U3CForceCheckEnumeratorU3Ed__58_tB83CC76710450C730B37A01FDEE5E33C17EA64F3::get_offset_of_U3CU3E2__current_1(),
	U3CForceCheckEnumeratorU3Ed__58_tB83CC76710450C730B37A01FDEE5E33C17EA64F3::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6528 = { sizeof (U3CForceCheckTaskU3Ed__59_t438175677D47B411599FF48D0FFA21895E75F050)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6528[4] = 
{
	U3CForceCheckTaskU3Ed__59_t438175677D47B411599FF48D0FFA21895E75F050::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CForceCheckTaskU3Ed__59_t438175677D47B411599FF48D0FFA21895E75F050::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CForceCheckTaskU3Ed__59_t438175677D47B411599FF48D0FFA21895E75F050::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CForceCheckTaskU3Ed__59_t438175677D47B411599FF48D0FFA21895E75F050::get_offset_of_U3CU3Eu__1_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6529 = { sizeof (U3CCheckForCheatU3Ed__67_t8744D10801D7D4974DA5FB3A21581569AFA87226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6529[3] = 
{
	U3CCheckForCheatU3Ed__67_t8744D10801D7D4974DA5FB3A21581569AFA87226::get_offset_of_U3CU3E1__state_0(),
	U3CCheckForCheatU3Ed__67_t8744D10801D7D4974DA5FB3A21581569AFA87226::get_offset_of_U3CU3E2__current_1(),
	U3CCheckForCheatU3Ed__67_t8744D10801D7D4974DA5FB3A21581569AFA87226::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6530 = { sizeof (WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6530[41] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_rigidPlayerVelocity_23(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_waitForEndOfFrame_24(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_checkRigidbody_25(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_checkController_26(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_checkWireframe_27(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_checkRaycast_28(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_wireframeDelay_29(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_raycastDelay_30(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_spawnPosition_31(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_maxFalsePositives_32(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_serviceContainer_33(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_solidWall_34(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_thinWall_35(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_wfCamera_36(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_foregroundRenderer_37(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_backgroundRenderer_38(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_wfColor1_39(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_wfColor2_40(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_wfShader_41(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_wfMaterial_42(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_shaderTexture_43(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_targetTexture_44(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_renderTexture_45(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_whLayer_46(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_raycastMask_47(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_rigidPlayer_48(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_charControllerPlayer_49(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_charControllerVelocity_50(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_rigidbodyDetections_51(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_controllerDetections_52(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_wireframeDetections_53(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_raycastDetections_54(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_wireframeDetected_55(),
	WallHackDetector_t4A00BE755A4156DD2624624E6F674E158EE41590::get_offset_of_rayHits_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6531 = { sizeof (U3CInitDetectorU3Ed__71_t1A2D78DA7BC7B2A2FA239173CD78A23A4A1829EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6531[3] = 
{
	U3CInitDetectorU3Ed__71_t1A2D78DA7BC7B2A2FA239173CD78A23A4A1829EC::get_offset_of_U3CU3E1__state_0(),
	U3CInitDetectorU3Ed__71_t1A2D78DA7BC7B2A2FA239173CD78A23A4A1829EC::get_offset_of_U3CU3E2__current_1(),
	U3CInitDetectorU3Ed__71_t1A2D78DA7BC7B2A2FA239173CD78A23A4A1829EC::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6532 = { sizeof (U3CCaptureFrameU3Ed__76_t8E49CCA052F5C0D472F1BCD76138069CDBA77C8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6532[4] = 
{
	U3CCaptureFrameU3Ed__76_t8E49CCA052F5C0D472F1BCD76138069CDBA77C8B::get_offset_of_U3CU3E1__state_0(),
	U3CCaptureFrameU3Ed__76_t8E49CCA052F5C0D472F1BCD76138069CDBA77C8B::get_offset_of_U3CU3E2__current_1(),
	U3CCaptureFrameU3Ed__76_t8E49CCA052F5C0D472F1BCD76138069CDBA77C8B::get_offset_of_U3CU3E4__this_2(),
	U3CCaptureFrameU3Ed__76_t8E49CCA052F5C0D472F1BCD76138069CDBA77C8B::get_offset_of_U3CpreviousActiveU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6533 = { sizeof (ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340)+ sizeof (RuntimeObject), sizeof(ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6533[16] = 
{
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b3_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b5_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b6_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b7_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b8_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b9_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b10_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b11_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b12_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b13_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b14_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b15_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte16_tBA7BB660E30157B457EAF59F1313F241EA53C340::get_offset_of_b16_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6534 = { sizeof (ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE)+ sizeof (RuntimeObject), sizeof(ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE ), 0, 0 };
extern const int32_t g_FieldOffsetTable6534[4] = 
{
	ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE::get_offset_of_b1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE::get_offset_of_b2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE::get_offset_of_b3_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte4_t07E71C2AD1E7FC28723306EE6AB2EC5ACF1B6BCE::get_offset_of_b4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6535 = { sizeof (ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E)+ sizeof (RuntimeObject), sizeof(ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E ), 0, 0 };
extern const int32_t g_FieldOffsetTable6535[8] = 
{
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E::get_offset_of_b1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E::get_offset_of_b2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E::get_offset_of_b3_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E::get_offset_of_b4_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E::get_offset_of_b5_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E::get_offset_of_b6_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E::get_offset_of_b7_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ACTkByte8_t833E13ECB9BCA90642862B8F95D336AE37DEA24E::get_offset_of_b8_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6536 = { sizeof (ACTkConstants_tA3115295C8C5DCBFA0664657820B2216E2B51DAB), -1, sizeof(ACTkConstants_tA3115295C8C5DCBFA0664657820B2216E2B51DAB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6536[4] = 
{
	0,
	0,
	0,
	ACTkConstants_tA3115295C8C5DCBFA0664657820B2216E2B51DAB_StaticFields::get_offset_of_StringKey_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6537 = { sizeof (ContainerHolder_t7F0EC51F32F5808C854B91DCC0CFBADEC4D2A1A9), -1, sizeof(ContainerHolder_t7F0EC51F32F5808C854B91DCC0CFBADEC4D2A1A9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6537[2] = 
{
	0,
	ContainerHolder_t7F0EC51F32F5808C854B91DCC0CFBADEC4D2A1A9_StaticFields::get_offset_of_container_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6538 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6538[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6539 = { sizeof (U3CPrivateImplementationDetailsU3E_t0A0BB8AE918C8DC79883BB225E227F5C7FE0A10A), -1, sizeof(U3CPrivateImplementationDetailsU3E_t0A0BB8AE918C8DC79883BB225E227F5C7FE0A10A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6539[1] = 
{
	U3CPrivateImplementationDetailsU3E_t0A0BB8AE918C8DC79883BB225E227F5C7FE0A10A_StaticFields::get_offset_of_U333DC1ECDFFAB5B7840492718281B58ED2B7C1DB2_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6540 = { sizeof (__StaticArrayInitTypeSizeU3D10_t85C6225FBC4C8C59B85DFE15371916C1912F4837)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D10_t85C6225FBC4C8C59B85DFE15371916C1912F4837 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6541 = { sizeof (U3CModuleU3E_t18E01DC18978C39365422BE25552B43FDE647BA1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6542 = { sizeof (AutoPlay_t17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6542[5] = 
{
	AutoPlay_t17E2BFFF87FCE2B8BAD2FD23B2B65379B515EC12::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6543 = { sizeof (AxisConstraint_t6929580E28515C207FCC805981F73EC49354E48A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6543[6] = 
{
	AxisConstraint_t6929580E28515C207FCC805981F73EC49354E48A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6544 = { sizeof (Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E)+ sizeof (RuntimeObject), sizeof(Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E ), 0, 0 };
extern const int32_t g_FieldOffsetTable6544[2] = 
{
	Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E::get_offset_of_ca_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Color2_t04D7DADEE8440A02309444A79F7FCD19FAD7313E::get_offset_of_cb_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6545 = { sizeof (TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6546 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6547 = { sizeof (EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6548 = { sizeof (DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D), -1, sizeof(DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6548[24] = 
{
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_Version_0(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_useSafeMode_1(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_showUnityEditorReport_2(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_timeScale_3(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_useSmoothDeltaTime_4(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_maxSmoothUnscaledTime_5(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of__logBehaviour_6(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_drawGizmos_7(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultUpdateType_8(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultTimeScaleIndependent_9(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultAutoPlay_10(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultAutoKill_11(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultLoopType_12(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultRecyclable_13(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultEaseType_14(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultEaseOvershootOrAmplitude_15(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_defaultEasePeriod_16(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_instance_17(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_isUnityEditor_18(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_maxActiveTweenersReached_19(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_maxActiveSequencesReached_20(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_GizmosDelegates_21(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_initialized_22(),
	DOTween_t6BB48F76E494B12781696AF3D0733CA8DC367E8D_StaticFields::get_offset_of_isQuitting_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6549 = { sizeof (Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6549[39] = 
{
	Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6550 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6551 = { sizeof (RotateMode_t7DB9BF2546AEC18F78B34B2B7111AED4416368C7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6551[5] = 
{
	RotateMode_t7DB9BF2546AEC18F78B34B2B7111AED4416368C7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6552 = { sizeof (ScrambleMode_tB2CAB82E2A9149E917AF3B1261C86133CBF83551)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6552[7] = 
{
	ScrambleMode_tB2CAB82E2A9149E917AF3B1261C86133CBF83551::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6553 = { sizeof (LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6553[4] = 
{
	LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6554 = { sizeof (Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6554[3] = 
{
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2::get_offset_of_sequencedTweens_51(),
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2::get_offset_of__sequencedObjs_52(),
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2::get_offset_of_lastTweenInsertTime_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6555 = { sizeof (ShortcutExtensions_t6551D100BBB9E0A80E6197A1DE743833FF1D0ED8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6556 = { sizeof (U3CU3Ec__DisplayClass41_0_tFAA38E06FCDC2A58D29D87FE82C5C07098024512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6556[1] = 
{
	U3CU3Ec__DisplayClass41_0_tFAA38E06FCDC2A58D29D87FE82C5C07098024512::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6557 = { sizeof (U3CU3Ec__DisplayClass43_0_t9F7D038FE3DAD3149EA263A82FB93104082BB9A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6557[1] = 
{
	U3CU3Ec__DisplayClass43_0_t9F7D038FE3DAD3149EA263A82FB93104082BB9A8::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6558 = { sizeof (U3CU3Ec__DisplayClass60_0_t64409C37981CA8094BF58D3393801152AC68A43B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6558[1] = 
{
	U3CU3Ec__DisplayClass60_0_t64409C37981CA8094BF58D3393801152AC68A43B::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6559 = { sizeof (U3CU3Ec__DisplayClass78_0_tFE1008EA46A16601A58722DD5444AC85B0398BC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6559[2] = 
{
	U3CU3Ec__DisplayClass78_0_tFE1008EA46A16601A58722DD5444AC85B0398BC0::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass78_0_tFE1008EA46A16601A58722DD5444AC85B0398BC0::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6560 = { sizeof (TweenSettingsExtensions_t61A3DCA4F25E31E73F0104D8DE15B2D9AC78CCCA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6561 = { sizeof (LogBehaviour_tD1A4AACD65E9C63844373CF59B8E9A576814F813)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6561[4] = 
{
	LogBehaviour_tD1A4AACD65E9C63844373CF59B8E9A576814F813::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6562 = { sizeof (Tween_t119487E0AB84EF563521F1043116BDBAE68AC437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6562[47] = 
{
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_timeScale_4(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_isBackwards_5(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_id_6(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_target_7(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_updateType_8(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_isIndependentUpdate_9(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_onPlay_10(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_onPause_11(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_onRewind_12(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_onUpdate_13(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_onStepComplete_14(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_onComplete_15(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_onKill_16(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_onWaypointChange_17(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_isFrom_18(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_isBlendable_19(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_isRecyclable_20(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_isSpeedBased_21(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_autoKill_22(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_duration_23(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_loops_24(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_loopType_25(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_delay_26(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_isRelative_27(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_easeType_28(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_customEase_29(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_easeOvershootOrAmplitude_30(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_easePeriod_31(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_typeofT1_32(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_typeofT2_33(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_typeofTPlugOptions_34(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_active_35(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_isSequenced_36(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_sequenceParent_37(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_activeId_38(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_specialStartupMode_39(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_creationLocked_40(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_startupDone_41(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_playedOnce_42(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_position_43(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_fullDuration_44(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_completedLoops_45(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_isPlaying_46(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_isComplete_47(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_elapsedDelay_48(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_delayComplete_49(),
	Tween_t119487E0AB84EF563521F1043116BDBAE68AC437::get_offset_of_miscInt_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6563 = { sizeof (Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6563[2] = 
{
	Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8::get_offset_of_hasManuallySetStartValue_51(),
	Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8::get_offset_of_isFromAllowed_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6564 = { sizeof (TweenType_t13FCD3DFD7DD8AE49EA4A7BA70695D3936EAF71F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6564[4] = 
{
	TweenType_t13FCD3DFD7DD8AE49EA4A7BA70695D3936EAF71F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6565 = { sizeof (UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6565[5] = 
{
	UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6566 = { sizeof (Color2Plugin_t87D37EAA5A6D3CC562863B155AFA3162B5D41743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6567 = { sizeof (DoublePlugin_t516B4EF13BB0F79EA5BEDC15F58823017C8F2418), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6568 = { sizeof (LongPlugin_t60BD2312BADD30C1AAC88D067CE7864D0CE63B4E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6569 = { sizeof (UlongPlugin_t96C49ADC71F577E6EFA028F1EEE64261327A328F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6570 = { sizeof (Vector3ArrayPlugin_t8254C41091DE113A1FB5BDCE91F97600348626EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6571 = { sizeof (ColorPlugin_tFE42FCE0666DDBE27D3ED749E5198833F49BBF90), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6572 = { sizeof (IntPlugin_tAA40FA89977341EE1DE1D65ECCA0C1A2D9B05B50), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6573 = { sizeof (QuaternionPlugin_t77E2981A5FA8AD92D23F2EB7735F6EA8A3992E1E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6574 = { sizeof (RectOffsetPlugin_t2250B1BA1D4AE2E236ED10428CDD0F1B0B13ADBF), -1, sizeof(RectOffsetPlugin_t2250B1BA1D4AE2E236ED10428CDD0F1B0B13ADBF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6574[1] = 
{
	RectOffsetPlugin_t2250B1BA1D4AE2E236ED10428CDD0F1B0B13ADBF_StaticFields::get_offset_of__r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6575 = { sizeof (RectPlugin_t85D2E439D5B68F1483299B2730FAC9C22F95DA4F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6576 = { sizeof (UintPlugin_tE4DCBED54C91BABBCA72DC9CD645347DE9F2C58F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6577 = { sizeof (Vector2Plugin_t8900F71F5DA563C8D8FA687C9F653A236ECD2F09), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6578 = { sizeof (Vector4Plugin_t7DE745F65D4FEBD8FBFECE97FE34F9592612B325), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6579 = { sizeof (StringPlugin_t5645A3CC39BB2ADAD92EBEFDA75EB3F3C6ABF97D), -1, sizeof(StringPlugin_t5645A3CC39BB2ADAD92EBEFDA75EB3F3C6ABF97D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6579[2] = 
{
	StringPlugin_t5645A3CC39BB2ADAD92EBEFDA75EB3F3C6ABF97D_StaticFields::get_offset_of__Buffer_0(),
	StringPlugin_t5645A3CC39BB2ADAD92EBEFDA75EB3F3C6ABF97D_StaticFields::get_offset_of__OpenedTags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6580 = { sizeof (StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B), -1, sizeof(StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6580[5] = 
{
	StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B_StaticFields::get_offset_of_ScrambledCharsAll_0(),
	StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B_StaticFields::get_offset_of_ScrambledCharsUppercase_1(),
	StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B_StaticFields::get_offset_of_ScrambledCharsLowercase_2(),
	StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B_StaticFields::get_offset_of_ScrambledCharsNumerals_3(),
	StringPluginExtensions_tB7BCF7EB9633300CDD2387F467B58782AE28F06B_StaticFields::get_offset_of__lastRndSeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6581 = { sizeof (FloatPlugin_tBB4C2318EBF50EC6568B3A9CB5AB3A1ACE1BA159), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6582 = { sizeof (Vector3Plugin_t1B9F5C4A304353A9D68B9182A8DE1DDAE3E2C4F9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6583 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6584 = { sizeof (QuaternionOptions_t217D095C19651CE87896F40C41802FA82552880B)+ sizeof (RuntimeObject), sizeof(QuaternionOptions_t217D095C19651CE87896F40C41802FA82552880B ), 0, 0 };
extern const int32_t g_FieldOffsetTable6584[3] = 
{
	QuaternionOptions_t217D095C19651CE87896F40C41802FA82552880B::get_offset_of_rotateMode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QuaternionOptions_t217D095C19651CE87896F40C41802FA82552880B::get_offset_of_axisConstraint_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QuaternionOptions_t217D095C19651CE87896F40C41802FA82552880B::get_offset_of_up_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6585 = { sizeof (UintOptions_tCD241771582D5159D0129AA82E86590D0A0FC1E6)+ sizeof (RuntimeObject), sizeof(UintOptions_tCD241771582D5159D0129AA82E86590D0A0FC1E6_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6585[1] = 
{
	UintOptions_tCD241771582D5159D0129AA82E86590D0A0FC1E6::get_offset_of_isNegativeChangeValue_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6586 = { sizeof (Vector3ArrayOptions_t3E84666D670017F133C32243F0709A3852F05257)+ sizeof (RuntimeObject), sizeof(Vector3ArrayOptions_t3E84666D670017F133C32243F0709A3852F05257_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6586[3] = 
{
	Vector3ArrayOptions_t3E84666D670017F133C32243F0709A3852F05257::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3ArrayOptions_t3E84666D670017F133C32243F0709A3852F05257::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3ArrayOptions_t3E84666D670017F133C32243F0709A3852F05257::get_offset_of_durations_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6587 = { sizeof (NoOptions_tC312CAA4CCC3BD59EC7E6930F0E08272608A5DE2)+ sizeof (RuntimeObject), sizeof(NoOptions_tC312CAA4CCC3BD59EC7E6930F0E08272608A5DE2 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6588 = { sizeof (ColorOptions_tD57E512D2F4329BEE7EAEE0D1B2C4A0683D288DA)+ sizeof (RuntimeObject), sizeof(ColorOptions_tD57E512D2F4329BEE7EAEE0D1B2C4A0683D288DA_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6588[1] = 
{
	ColorOptions_tD57E512D2F4329BEE7EAEE0D1B2C4A0683D288DA::get_offset_of_alphaOnly_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6589 = { sizeof (FloatOptions_t7285C3D13285197B6B003786B85DAAD83E654C1B)+ sizeof (RuntimeObject), sizeof(FloatOptions_t7285C3D13285197B6B003786B85DAAD83E654C1B_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6589[1] = 
{
	FloatOptions_t7285C3D13285197B6B003786B85DAAD83E654C1B::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6590 = { sizeof (RectOptions_t1C4579A85851F16B3300F373FC807B4913D59A5E)+ sizeof (RuntimeObject), sizeof(RectOptions_t1C4579A85851F16B3300F373FC807B4913D59A5E_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6590[1] = 
{
	RectOptions_t1C4579A85851F16B3300F373FC807B4913D59A5E::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6591 = { sizeof (StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC)+ sizeof (RuntimeObject), sizeof(StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6591[5] = 
{
	StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC::get_offset_of_richTextEnabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC::get_offset_of_scrambleMode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC::get_offset_of_scrambledChars_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC::get_offset_of_startValueStrippedLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t58D6011099873A39926A99C00EE24D0D80B231FC::get_offset_of_changeValueStrippedLength_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6592 = { sizeof (VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322)+ sizeof (RuntimeObject), sizeof(VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable6592[2] = 
{
	VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6593 = { sizeof (SpecialPluginsUtils_t7ACBDEA1BEB198E54F55F73804E9244952DFB5CE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6594 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6595 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6596 = { sizeof (PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E), -1, sizeof(PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6596[16] = 
{
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__floatPlugin_0(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__doublePlugin_1(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__intPlugin_2(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__uintPlugin_3(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__longPlugin_4(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__ulongPlugin_5(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__vector2Plugin_6(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__vector3Plugin_7(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__vector4Plugin_8(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__quaternionPlugin_9(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__colorPlugin_10(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__rectPlugin_11(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__rectOffsetPlugin_12(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__stringPlugin_13(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__vector3ArrayPlugin_14(),
	PluginsManager_t673F317071E579280C9F5FFB31D751511905BE1E_StaticFields::get_offset_of__color2Plugin_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6597 = { sizeof (ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6597[4] = 
{
	ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757::get_offset_of_tweenType_0(),
	ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757::get_offset_of_sequencedPosition_1(),
	ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757::get_offset_of_sequencedEndPosition_2(),
	ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757::get_offset_of_onStart_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6598 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6599 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
